<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 29/05/2018
 * Time: 10:40 AM
 */

namespace App\Providers;

use Illuminate\Contracts\Auth\Authenticatable as UserContract;
use Illuminate\Auth\GenericUser;

class SinoramaUser extends GenericUser implements UserContract
{
    public function __construct(array $attributes)
    {
        $this->attributes = $attributes;
    }

    public function getAuthIdentifierName()
    {
        return 'email';
    }

    /**
     * Get the password for the user.
     */
    public function getAuthPassword()
    {
        return $this->attributes['password'];
    }

}