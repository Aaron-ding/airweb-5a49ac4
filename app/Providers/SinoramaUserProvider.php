<?php

namespace App\Providers;

use Illuminate\Support\Str;
use Illuminate\Contracts\Auth\UserProvider;
use Illuminate\Database\ConnectionInterface;
use Illuminate\Contracts\Hashing\Hasher as HasherContract;
use Illuminate\Contracts\Auth\Authenticatable as UserContract;
use App\Providers\SinoramaUser;
use App\Providers\SinoLogService;

class SinoramaUserProvider implements UserProvider
{
    //  The active database connection.
    protected $conn;

    //  \Illuminate\Contracts\Hashing\Hasher
    protected $hasher;

    //   The table containing the users.
    protected $table;

    public function __construct(ConnectionInterface $conn, HasherContract $hasher, $table)
    {
        $this->conn = $conn;
        $this->table = $table;
        $this->hasher = $hasher;

//        echo "into the constructer!";
//        var_dump($conn);
//        var_dump($table);
    }

    /**
     * Retrieve a user by their unique identifier.
     *
     * @param  mixed $identifier
     * @return \Illuminate\Contracts\Auth\Authenticatable|null
     */
    public function retrieveById($identifier)
    {
        $user = $this->conn->table($this->table)->select('username', 'email', 'password', 'remember_token', 'sinocookie')
            ->where("email", "=", $identifier)
            ->first();
        if(!is_null($user) && !is_array($user)){
            //$user = $user->toArray();
            $user = get_object_vars($user);
        }
        return $this->getSinoUser($user);
    }

    /**
     * Retrieve a user by their unique identifier and "remember me" token.
     *
     * @param  mixed $identifier
     * @param  string $token
     * @return \Illuminate\Contracts\Auth\Authenticatable|null
     */
    public function retrieveByToken($identifier, $token)
    {
        $user = $this->conn->table($this->table)
            ->where('email', $identifier)
            ->where('remember_token', $token)
            ->first();
        if(!is_null($user) && !is_array($user)){
            //$user = $user->toArray();     // for collection
            $user = get_object_vars($user);   //for object
        }
        return $this->getSinoUser($user);
    }

    /**
     * Update the "remember me" token for the given user in storage.
     *
     * @param  \Illuminate\Contracts\Auth\Authenticatable $user
     * @param  string $token
     * @return void
     */
    //   call when logout
    public function updateRememberToken(UserContract $user, $token)
    {
        $this->conn->table($this->table)
            ->where('email', $user->getAuthIdentifier())
            ->update(['remember_token' => $token]);
    }

    /**
     * Retrieve a user by the given credentials.
     *
     * @param  array $credentials
     * @return \Illuminate\Contracts\Auth\Authenticatable|null
     */
    public function retrieveByCredentials(array $credentials)
    {
        // First we will add each credential element to the query as a where clause.
        // Then we can execute the query and, if we found a user, return it in a
        // generic "user" object that will be utilized by the Guard instances.
        $query = $this->conn->table($this->table)->select(
            'username', 'email', 'password', 'remember_token', 'sinocookie');

        foreach ($credentials as $key => $value) {
            if (!Str::contains($key, 'password')) {
                $query->where($key, $value);
            }
        }
        $user = $query->first();

        if(!is_null($user)){
            if(!is_array($user)){
                //$user=$user->toArray();
                $user = get_object_vars($user); // for object
            }
        }
        else {
        //   if user is null, we create a temperary user, for use in attemplogin by sessionguard            -- qiankun
            $user = ['email' => $credentials['email'],
                'username' => 'unknown__',
                'password' => '',
                'remember_token' => '',
                'sinocookie' =>''
            ];
        }

        return $this->getSinoUser($user);
    }

    /**
     * Get the generic user.
     *
     * @param  mixed $user
     * @return \Illuminate\Auth\GenericUser|null
     */
    protected function getSinoUser($user)
    {
        if (!is_null($user)) {
            return new SinoramaUser((array)$user);
        }
    }

    /**
     * Validate a user against the given credentials.
     * @param  \Illuminate\Contracts\Auth\Authenticatable $user
     * @param  array $credentials
     * @return bool
     */

    //   check the user name and password through API on Https://asia.sinaramagroup.com.
    //   Insert the user record into database so we can use it later!!       -- qiankun
    //
    public function validateCredentials(UserContract $user, array $credentials)
    {
        // Run remote login check, return a user if login successed, return null if failed
        $Sinolog = new SinoLogService();

        if($Sinolog->RemoteLogin($credentials)){
            //successsed
            $record = $this->conn->table($this->table)->select('username')
                    ->where('email', $credentials['email'])
                    ->first();

            if (!is_null($record)) {
                $this->conn->table($this->table)
                     ->where('email',  $credentials['email'])
                     ->update(['username' => $Sinolog->getUsername(),
                       'sinocookie' => $Sinolog->getSinocookie()]);
            }
            else {
                // create a record in our database to save login related data
                $this->conn->table($this->table)->insert(
                    ['email' => $credentials['email'],
                     'username' => $Sinolog->getUsername(),
                     'firstname' => ' ',
                     'lastname' => ' ',
                     'password' => bcrypt($credentials['password']),
                     'remember_token' => '',
                     'sinocookie' => $Sinolog->getSinocookie()
                ]);
            }
            return true;
        }
        return false;
        //original:
        //  return $this->hasher->check(
        //    $credentials['password'], $user->getAuthPassword()
        //);
    }

}