<?php

namespace App\Providers;


/**
 * Created by PhpStorm.
 * User: User
 * Date: 28/05/2018
 * Time: 10:59 AM
 */
/**
 * Send a POST requst using cURL
 * @param string $url to request
 * @param array $post values to send
 * @param array $options for cURL
 * @return string
 */
function curl_post($url, array $post=NULL , array $options = array())
{
    $ua = 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US) AppleWebKit/525.13 (KHTML, like Gecko) Chrome/0.A.B.C Safari/525.13';

    $defaults = array(
        CURLOPT_POST => 1,
        CURLOPT_HEADER => 0,
        CURLOPT_URL => $url,
        CURLOPT_FRESH_CONNECT => 1,
        CURLOPT_RETURNTRANSFER => 1,
        CURLOPT_FORBID_REUSE => 1,
        CURLOPT_TIMEOUT => 5,
        CURLOPT_POSTFIELDS => http_build_query($post),
        CURLOPT_USERAGENT=>$ua
    );

    $ch = curl_init();
    curl_setopt_array($ch, ($options + $defaults));
    $result=curl_exec($ch);
    /*
    if( ! $result = curl_exec($ch))
    {
        trigger_error(curl_error($ch));
    }

    if(curl_errno($ch))
        echo 'Curl error: '. curl_error($ch) . '<br>';*/
    curl_close($ch);
    return $result;
}

function curl_get($url, array $options = array())
{
    $ua = 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US) AppleWebKit/525.13 (KHTML, like Gecko) Chrome/0.A.B.C Safari/525.13';


    $defaults = array(
        CURLOPT_HEADER => 1,
        CURLOPT_URL => $url,
        CURLOPT_FRESH_CONNECT => 1,
        CURLOPT_RETURNTRANSFER => 1,
        CURLOPT_FORBID_REUSE => 1,
        CURLOPT_TIMEOUT => 5,
        CURLOPT_USERAGENT=>$ua
    );

    $ch = curl_init();

    curl_setopt_array($ch, ($options + $defaults));

    $result=curl_exec($ch);

    curl_close($ch);
    return $result;
}
