<?php

namespace App\Providers;

class CURL
{
    private static $ua = 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US) AppleWebKit/525.13 (KHTML, like Gecko) Chrome/0.A.B.C Safari/525.13';

    public static function post($url, array $post=NULL , array $options = array())
    {
        $defaults = array(
            CURLOPT_POST => 1,
            CURLOPT_HEADER => 0,
            CURLOPT_URL => $url,
            CURLOPT_FRESH_CONNECT => 1,
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_FORBID_REUSE => 1,
            CURLOPT_TIMEOUT => 5,
            CURLOPT_POSTFIELDS => http_build_query($post),
            CURLOPT_USERAGENT=>self::$ua
        );

        $ch = curl_init();
        curl_setopt_array($ch, ($options + $defaults));
        $result=curl_exec($ch);
        /*
        if( ! $result = curl_exec($ch))  {
            trigger_error(curl_error($ch));
        }

        if(curl_errno($ch))
            echo 'Curl error: '. curl_error($ch) . '<br>';*/
        curl_close($ch);
        return $result;
    }

    public static function get($url, array $options = array())
    {
        $defaults = array(
            CURLOPT_HEADER => 1,
            CURLOPT_URL => $url,
            CURLOPT_FRESH_CONNECT => 1,
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_FORBID_REUSE => 1,
            CURLOPT_TIMEOUT => 5,
            CURLOPT_USERAGENT=>self::$ua
        );

        $ch = curl_init();

        curl_setopt_array($ch, ($options + $defaults));

        $result=curl_exec($ch);

        curl_close($ch);
        return $result;
    }
}