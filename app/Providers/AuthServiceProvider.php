<?php

namespace App\Providers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use App\Providers\SinoramaUserProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        // register self defined UserProvider, will be used in AuthManager     --Kun Qian
        Auth::provider('sinorama', function($app, array $config) {

            $connection = $app['db']->connection();
            // return instance of SinoramaUserProvider
            return new SinoramaUserProvider($connection, $this->app['hash'], $config['table']);
        });

        /*     Self defined session guard is not used.             --Kun Qian
        Auth::extend('sinosession', function ($app, $name, array $config) {
            // Return an instance of SinoramaSessionGuard..

            $guard = new SinoramaSessionGuard($name,Auth::createUserProvider($config['provider']),$app['session.store']);

            if (method_exists($guard, 'setCookieJar')) {
                $guard->setCookieJar($app['cookie']);
            }

            if (method_exists($guard, 'setDispatcher')) {
                $guard->setDispatcher($app['events']);
            }

            if (method_exists($guard, 'setRequest')) {
                $guard->setRequest($app->refresh('request', $guard, 'setRequest'));
            }

            return $guard;
        });

        */
    }
}
