<?php

namespace App\Providers;
use App\Providers\CURL;

class SinoLogService
{
    protected $username;
    protected $sinocookie;

    public function getSinocookie(){
        return $this->sinocookie;
    }

    public function getUsername(){
        return $this->username;
    }

    protected function getJSFeedbackStr($str)
    {
        $line = "";

        //  echo $str.'<br>';
        str_replace('\/', '/', $str);
        $ist = stripos($str, "https://asia.sinorama.ca");
        if ($ist) {
            $ist = $ist + 52;  // 52 is the length to skip in order to get the code.
            $tokenstr = substr($str, $ist);
            $j = strpos($tokenstr, '"');
            if ($j > 0 && $j < 400) {
                $line = substr($tokenstr, 0, $j);
            }
        }
        if ($line != "") {
            $line = 'https://asia.sinorama.ca/asia/index.php/api/uc?time=' . $line;
        }
        return $line;
    }

    protected function readCookie ($str) {
        $m = array();
        preg_match('/Set-Cookie: (.*?);/', $str, $m);
        return (!empty($m[1])) ? $m : false;
    }

    protected function getSinoLoginCookie($jurl)
    {
        $cookiecode = "";
        if ($jurl != "") {
            // server login
            echo $jurl . '<br>';
            $temp = storage_path();
            $cookief = $temp . '/logs/tmp_curl_cookie.tt';
            $options = array(
                CURLOPT_COOKIEJAR => $cookief,
                CURLOPT_COOKIEFILE => $cookief
            );

            //echo implode(" ",$post);
            $resp = CURL::get($jurl, $options);

            $cookies = $this->readCookie($resp);

            foreach ($cookies as $cookie) {
                if (stripos($cookie, "login_info") > -1) {
                    $cookiecode = substr($cookie, 11);
                }
            }
            if (strlen($cookiecode) > 200) {
            //  echo "Error! Bad cookie string !<br>";
                $cookiecode="";
            }
        }
        return $cookiecode;
    }

    public function RemoteLogin(array $credentials){

        // server login
        $url = 'https://asia.sinorama.ca/asia/index.php/asiatour/ucenter/api_loginSyn';
        $post = array(
            'email' => $credentials['email'],
            'password' => $credentials['password']
        );
        $options = array();
        $resp = CURL::post($url, $post, $options);

        $respect = json_decode($resp, true);

        if ($respect["status"] > 0) {

            $jurl = $this->getJSFeedbackStr($respect["js"]);
            $cookiecode = $this->getSinoLoginCookie($jurl);

            $this->username=$respect["userName"];
            $this->sinocookie=$cookiecode;
            return true;
        }
        else {
            session()->flash('loginerror','notmatch');
            return false;
        }
    }

    /* logout logic:
     *  call logout api, no parameter.
     *  get a js url.
     *  read the js url (with cookie:login_info).
     *  api server logout, delete cookie
     */
    public function RemoteLogout($sinocookie){
        $url = 'https://asia.sinorama.ca/asia/index.php/asiatour/ucenter/api_logoutSyn';
        $post = array();
        $options = array();

        $resp = CURL::post($url, $post, $options);
        $respect = json_decode($resp, true);
        $jurl = $this->getJSFeedbackStr($respect["js"]);

        if($jurl!="") {
            $temp = storage_path();
            $cookief = $temp . '/logs/tmp_curl_cookie.tt';
            $options = array(
                CURLOPT_COOKIEJAR => $cookief,
                CURLOPT_COOKIE => 'login_info=' . $sinocookie
            );
            $resp = CURL::get($jurl, $options);

            if(stripos($resp, "login_info=delete") > -1) {
                echo 'logout on api server successed!';
            }
            return true;
        }
        return false;
    }

    /**
     * common user signup, synchronize login to www.sinoramabus.com
     * return status number to indicate whether signup successfully.
     */
    public function RemoteSignup(array $credentials)
    {
        $url = "https://asia.sinorama.ca/asia/index.php/asiatour/ucenter/api_register";
        $post = array(
            'userName' => $credentials['username'],
            'email' => $credentials['email'],
            'password' => $credentials['password'],
        );
        $options = array();
        $resp = CURL::post($url, $post, $options);

        $respect = json_decode($resp, true);

        return $respect["status"];
    }

    public function RemoteChangepassword(array $credentials)
    {
        $url = "https://asia.sinorama.ca/asia/index.php/asiatour/ucenter/api_changePw";
        $post = array(
            'oldPassword' => $credentials['oldpassword'],
            'email' => $credentials['email'],
            'newPassword' => $credentials['newpassword'],
        );
        $options = array();
        $resp = CURL::post($url, $post, $options);

        $respect = json_decode($resp, true);
        return $respect["status"];
    }

    public function RemoteForgetpassword($email){
        $url = "https://asia.sinorama.ca/asia/index.php/asiatour/ucenter/api_changePw";
        $post = array(
            'email'=>$email,
        );
        $options = array();
        $resp = CURL::post($url, $post, $options);
        return $resp;
    }
}