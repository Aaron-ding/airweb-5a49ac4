<?php
/**
 * Created by PhpStorm.
 * User: localUser
 * Date: 1/10/2018
 * Time: 10:36 AM
 */

namespace App\Http\Middleware;

use Closure;


class CrossHttp
{
    public function handle($request, Closure $next) {
        $response = $next($request);
        $response->header('Access-Control-Allow-Origin', '*');
        $response->header('Access-Control-Allow-Headers', 'Origin, Content-Type, Cookie, Accept');
        $response->header('Access-Control-Allow-Methods', 'GET');
         $response->header('Access-Control-Allow-Credentials', 'false');
        return $response;
    }
}