<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 05/06/2018
 * Time: 5:00 PM
 */

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class MyAuthMiddleware
{
    public function handle($request, Closure $next)
    {
        if (auth()->guest()) {
            return redirect('/login');
        }
        return $next($request);
    }
}