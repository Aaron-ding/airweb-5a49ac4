<?php
/**
 * Created by PhpStorm.
 * User: localUser
 * Date: 12/4/2017
 * Time: 5:02 PM
 */

namespace App\Http\Middleware;

use Closure;
use Session;
use App;
use Config;

class Language
{



    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {//var_dump(Session::has('locale'));
        // Make sure current locale exists.
        if(Session::has('locale')){
            $locale=Session::get('locale',Config::get('app.locale'));
            //var_dump($locale);
        }else{
            $locbrowser = substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2);
            switch ($locbrowser){
                case "fr":
                    $locale="fr";
                    break;
                case "zh":
                    $locale="cn";
                    break;
                case "en":
                    $locale="en";
                    break;
                default:
                    $locale=Config::get('app.locale');
                    break;
            }
            Session::put('locale',$locale);
        }
        App::setLocale($locale);

        return $next($request);
    }

}