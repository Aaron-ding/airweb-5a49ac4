<?php

namespace App\Http\Controllers\Auth;

use App\Models\User\User;
//use App\Models\User\Account;
use App\Providers\SinoLogService;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Auth\Events\Registered;
use Jrean\UserVerification\Traits\VerifiesUsers;
use Jrean\UserVerification\Facades\UserVerification;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\URL;



class RegisterController extends Controller
{

    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;
    use VerifiesUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => ['getVerification', 'getVerificationError','resendEmail']]);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'username' => 'required|string|max:100',
            'email' => 'required|string|email|max:180',
            'password' => 'required|string|min:6',
        ]);
    }

    /**  no use
     * Create a new user instance after a valid registration.
     * @param  array  $data
     */
    protected function create(array $data)
    {
        die("don't use create, just for study!!!");

        $user= User::create([
            'username' => $data['username'],
            'firstname' => ' ',
            'lastname' => ' ',
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);
        $user->initJournal('CAD');
        return $user;
    }
    /**
     * Handle a registration request for the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {
        $url=URL::previous();
        if(substr($url,-5)=='login' || substr($url,-8)=='register'){
            $url=$request->session()->get('returl');
            if(!is_null($url)) {
                $this->redirectTo = $url;
            }
        }
        else {
            $this->redirectTo = $url;
        }

        $this->validator($request->all())->validate();

//        $user = $this->create($request->all());

        $Sinolog = new SinoLogService();
        $credentials = $request->only('username', 'email', 'password');

        $status = $Sinolog->RemoteSignup($credentials);
        if($status > 0 ) {

            //
            //  copy from logincontroller,  need refactor           --  qiankun
            if (Auth::guard()->attempt($credentials,false)) {
                $record = DB::table('users')->select('username', 'sinocookie')
                    ->where("email", $request['email'])
                    ->first();

                if (!is_null($record)) {
                    $userdata = get_object_vars($record);
                }

                $user = $this->guard()->user();
                if (!is_null($user)) {
                    $user->__set('username', $userdata['username']);
                    $user->__set('sinocookie', $userdata['sinocookie']);
                }

                $request->session()->put('username', $userdata['username']);
                $request->session()->put('email', $request['email']);

                if (!is_null($userdata['sinocookie']) && $userdata['sinocookie'] != "") {
                    $lifeTime = 3600;
                    setcookie("login_info", $userdata['sinocookie'], time() + $lifeTime, '/', '.sinorama.ca');
                    //                Cookie::queue(Cookie::make("login_info", $userdata['sinocookie'], 60, "/", ".sinorama.ca"));
                }

                $request->session()->regenerate();

                //  Dispatch user registered event
                event(new Registered($user));

                return redirect($this->redirectTo)->with('status',trans('language.Signupsuccess'));
//        UserVerification::generate($user);
//        $this->senactivemail($user);
            }
            return redirect('/')->with('status','Register successed, try login later.');
        }
        else {
            // failed
            if($status ==-3 )
                session()->flash('signuperror','usernameused');
            else if($status ==-6 )
                session()->flash('signuperror','emailused');
            session()->flash('oldusername',$request['username']);
            session()->flash('oldemail',$request['email']);
            return redirect('/register')->with('status',trans('language.generalerror3'));
        }
    }

    public function showRegistrationForm(){
        $url=URL::previous();
        session()->put('returl', $url);
        return view('mobile/login', ['action' => 'register']);
    }

    public function resendEmail(Request $request)
    {
        $user=$request->user();
        if(isset($user)){
            $this->senactivemail($user);
            echo json_encode(["success"]);
            return;
        }
        echo json_encode(["error"]);
        return;
    }

    private function senactivemail($user){
        UserVerification::send($user, 'Active your Sinorama account','no-reply@sinoramagroup.com','SINORAMA');
    }
}