<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\SinoLogService;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\URL;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\DB;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';
    protected $redirectAfterLogout = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    /**
     * Get the login username to be used by the controller.
     * @return string
     */
    public function username()
    {
        return 'email';
    }

    //  Not used
    public function authenticate($email,$password)
    {
        if (Auth::attempt(['email' => $email, 'password' => $password])) {
            // Authentication passed...
            return redirect()->intended('/');
        }
    }

    /**
     * The user has been authenticated.
     * will be call in member function sendLoginResponse()           -- qiankun
     * if you make the response yourself, return true.
     * else return false, go redirect->intended()
    */
    protected function authenticated(Request $request, $user)
    {
        //
    }

    public function login(Request $request)
    {
        $url=URL::previous();
        if(substr($url,-5)=='login' || substr($url,-8)=='register'){
            $url=$request->session()->get('returl');
            if(!is_null($url)) {
                $this->redirectTo = $url;
            }
        }
        else {
            $this->redirectTo = $url;
        }

        $this->validateLogin($request);

        // If the class is using the ThrottlesLogins trait, we can automatically throttle
        // the login attempts for this application. We'll key this by the username and
        // the IP address of the client making these requests into this application.
        if ($this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);

            return $this->sendLockoutResponse($request);
        }

        if ($this->attemptLogin($request)) {

            //   get cookies and username varibles                  -- qiankun
            $record = DB::table('users')->select('username', 'sinocookie')
                ->where("email", $request['email'])
                ->first();
            // data returned from DB:: is object instead of collection  ????????   -- qiankun
            if (!is_null($record)) {
                $userdata = get_object_vars($record);
            }

            // refill the username so that it won't be 'unknown'    -- qiankun
            $user=$this->guard()->user();
            if(!is_null($user)) {
                $user->__set('username', $userdata['username']);
                $user->__set('sinocookie', $userdata['sinocookie']);
            }

            $request->session()->put('username', $userdata['username']);
            $request->session()->put('email', $request['email']);

            if(!is_null($userdata['sinocookie']) && $userdata['sinocookie']!="") {
                $lifeTime =3600;
                setcookie("login_info", $userdata['sinocookie'], time() + $lifeTime, '/', '.sinorama.ca');
//                Cookie::queue(Cookie::make("login_info", $userdata['sinocookie'], 60, "/", ".sinorama.ca"));
            }
            return $this->sendLoginResponse($request);
        }

        // If the login attempt was unsuccessful we will increment the number of attempts
        // to login and redirect the user back to the login form. Of course, when this
        // user surpasses their maximum number of attempts they will get locked out.
        $this->incrementLoginAttempts($request);

        return $this->sendFailedLoginResponse($request);
    }

    public function logout(Request $request)
    {
        if(Auth::check()) {
            $Sinolog = new SinoLogService();
            $user=$this->guard()->user();
            $Sinolog->RemoteLogout($user->__get('sinocookie'));

            $request->session()->forget('username');
            $request->session()->forget('sinocookie');

            $this->guard()->logout();

            $request->session()->forget('email');
            $request->session()->invalidate();

            //        Cookie::queue(Cookie::forget('login_info'));
            setcookie("login_info", "deleted", time() + 1);
        }
        $url=URL::previous();
        if(substr($url,-5)=='login' || substr($url,-8)=='register'){
            return redirect($url);
        }
        return redirect('/');
    }

    public function showLoginForm(Request $request)
    {
        $url=URL::previous();
        $request->session()->put('returl', $url);
        return view('mobile/login', ['action' => 'login']);
    }
}

/**
 * Show the application's login form.
 *
 * @return \Illuminate\Http\Response
 */




/** Don't delete this!!!!
  * Save the original login and logout function for reference!!!!!!!!.

public function login(Request $request)
{
    $this->validateLogin($request);

    if ($this->hasTooManyLoginAttempts($request)) {
        $this->fireLockoutEvent($request);

        return $this->sendLockoutResponse($request);
    }

    if ($this->attemptLogin($request)) {
        return $this->sendLoginResponse($request);
    }

    // If the login attempt was unsuccessful we will increment the number of attempts
    // to login and redirect the user back to the login form. Of course, when this
    // user surpasses their maximum number of attempts they will get locked out.
    $this->incrementLoginAttempts($request);

    return $this->sendFailedLoginResponse($request);
}

public function logout(Request $request)
{
    $this->guard()->logout();

    $request->session()->invalidate();

    return redirect('/');
}
 */
