<?php

namespace App\Http\Controllers\Mobile;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\File;
use App\Http\Controllers\Controller;
use App\Providers\DnodeSyncClient;

class OrderController extends Controller
{
    protected $env;
    protected $connection;

    public function __construct()
    {
        $this->middleware('auth.custom');
    }

    protected function getlocalestrings($locale)
    {
        $fullpath = storage_path('../resources/lang/').$locale."/language.php";
        if(File::exists($fullpath)) {
            $value = File::getRequire($fullpath);
            return json_encode($value);
        }
        else
            return "";
    }

    public function getOrders()
    {
        $userid = session('email');

        $token = md5($userid . 'sinoramaworld-flight12!@');
        //var_dump($token);
        $para = array(
            array(
                "userid" => $userid,
                "token" => $token
            ));

        $response=$this->connection->call('getOrdersByUserId', $para);

        //var_dump($response);
        return $response;
    }

    public function transorderinfo($orderinfo)
    {
        $currency='CAD';
        $userinfos=array();

        foreach($orderinfo as $order) {
            if($order['ticketstatus']!='no booking') {
                $userinfo = array();

                $para = array(
                    array(
                        "orderid" => $order["orderid"],
                    ));

                $response=$this->connection->call('getOrderInfo', $para);
                if(isset($response)&&!is_null($response))
                {
                    if ($response[0][0] == 'success'){
                        $orderinf = $response[0][1];
                        $userinfo["Passengers"]=$orderinf["Passengers"];
                        $userinfo["Contact"]=$orderinf["Contact"];
                    }
                }

                $userinfo["orderid"] = $order["orderid"];
                $tourid = $order["orderid"];
                $date = new \DateTime($order["createdatetime"], new \DateTimeZone('UTC'));
                $userinfo["date"] = strtotime($order["createdatetime"]);
                $date->setTimezone(new \DateTimeZone('America/Toronto'));
                $userinfo["createdatetime"] = $date->format('Y-m-d H:i:s');
                $userinfo["status"] = $order["status"];
                $userinfo["ticketstatus"] = $order["ticketstatus"];
                $userinfo["paystatus"] = $order["paystatus"];
                $userinfo["passengers"] = $order["adult"] + $order["child"] + $order["baby"] + $order["babyinlap"];
                $userinfo["triptype"] = $order["triptype"];
                $userinfo["departuredate"] = $order["departuredate"];
                $userinfo["departuretime"] = strtotime($order["departuredate"]);
                $userinfo["return_departuredate"] = $order["return_departuredate"];
                $userinfo["departcity"] = $order["from"];
                $userinfo["arrivalcity"] = $order["to"];
                $userinfo["currency"] = $order["currency"];
                $userinfo["totalprice"] = intval($order["totalprice"]) / 100;
                $userinfo["PNR"] = $order["PNR"];
                $token = md5('sinoramaWorld_02372785@!#d_air_API' . $order["orderid"]);
                $userinfo["paymenturl"] = '/pay?fullsite=yes&orderid=' . $tourid . '&token=' . $token . '&valid=' . base64_encode($userinfo["date"]);
                $token1 = md5($tourid . 'sinoramaworld-flight12!@');
                //(getSafeEnv()=="dev" =="master" )
                $crudserver = (getSafeEnv()=="master")?"crud.sinorama.ca":"dev.crud.sinorama.ca";

                $userinfo["receipturl"] = "https://" . $crudserver . "/index.php/modules/pnrofqueue/pnrreceipt_after_payment/" . $order["PNR"] . "/" . $tourid . "/" . $token1 . "/html";
                $userinfo["itenerayurl"] = "https://" . $crudserver . "/index.php/modules/pnrofqueue/pnrreceipt_after_booking/" . $order["PNR"] . "/" . $tourid . "/" . $token1 . "/html";
                $userinfo["reorderurl"] = '/search?srccity=' . $order["from"] . '&destcity=' . $order["to"] . '&tripType=' . $order["triptype"] . '&departdate=' . substr($order["departuredate"], 0, 10) . '&returndate=' . substr($order["return_departuredate"], 0, 10) . '&adultcount=' . $order["adult"] . '&childrencount=' . $order["child"] . '&infantsseatcount=' . $order["baby"] . '&infantslapcount=' . $order["babyinlap"] . '&fullsite=yes';
                $userinfo["lasturl"] = "https://" . $crudserver . "/index.php/modules/pnrofqueue/pnrreceipt_after_ticket/" . $order["PNR"] . "/" . $tourid . "/" . $token1 . "/html";

                $userinfo["provider"] = $order["provider"];
                $userinfo["flightsegments"] = $order["flightsegments"];
                $userinfo["itineraries"] = $order["itineraries"];
                if (time() >= ($userinfo["date"]) + 10800) {
                    $userinfo["paymentvalidation"] = "false";
                } else {
                    $userinfo["paymentvalidation"] = "true";
                }

                if (time() < (($userinfo["departuretime"]) - 172800)) {
                    $userinfo["reordervalidation"] = "true";
                } else {
                    $userinfo["reordervalidation"] = "false";
                }

                $userinfos[] = $userinfo;
            }
        }
        return json_encode($userinfos);
    }

    public function orders(Request $request){
        Log::debug('OrderController:orders    '.getSafeEnv());

        $this->env=config('app.env');

        $this->connection = new DnodeSyncClient();

        $response=$this->getOrders();

        if(isset($response)&&!is_null($response))
        {
            if ($response[0][0] == 'success')
            {
                $orderinfo = $this->transorderinfo($response[0][1]);
                $empty = "false";
                $orderlanguagedata=$this->getlocalestrings(session()->get('locale'));
            }
            else if ($response[0][0] == 'error')
            {
                $orderinfo = "";
                $orderlanguagedata = "";
                $empty = "true";
            }
            $this->connection->close();
            return view('mobile/usercentre/orders', ['orderinfo' => $orderinfo, 'langdata'=>$orderlanguagedata,'empty' => $empty]);
        }
        else {
            $this->connection->close();
            return redirect()->back()->with('Error', 'Get Order info failed!');
        }
    }
    public function orderspsginfo(Request $request, $id){
        Log::debug('OrderController:orders    '.getSafeEnv());

        $this->env=config('app.env');

        $this->connection = new DnodeSyncClient();
        $response=$this->getOrders();
        $this->connection->close();

        if(isset($response)&&!is_null($response))
        {
            if ($response[0][0] == 'success')
            {
                $orderinfo = $this->transorderinfo($response[0][1]);
            }
            else if ($response[0][0] == 'error')
            {
                $orderinfo = "";
            }
        }
        else {
            return redirect()->back()->with('Error', 'Get Order info failed!');
        }
    }
}
