<?php

namespace App\Http\Controllers\Mobile;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use App\Http\Controllers\Controller;



class HomeController extends Controller
{
    private $env = '';
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
//        $env = fgets(fopen(getcwd() . "/../current_branch", 'r'));
//        $this->env = trim(preg_replace('/\s\s+/', ' ', $env));
        $this->env = getSafeEnv();
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $request->session()->put("effroute","search");
        if($request->has("querystring")){
            $request->flashOnly(['querystring']);
        }
        $qqstring=$request->session()->get('querystring');
//        $request->session()->forget('savquerystring');
        if($qqstring===null || $qqstring==""){
            return view('mobile/search', ['env'=> $this->env]);
        }else{
            return view('mobile/search',["querystring"=>$qqstring, 'env'=> $this->env]);
        }
    }

    public function searchlist(Request $request)
    {
        if($request->has("querystring")){
            $request->session()->put("effroute","searchlist");
            $request->flashOnly(['querystring']);
            $qqstring=$request->get('querystring');
            $request->session()->put('querystring', $qqstring);
            return view('mobile/list', ['env'=> $this->env]);
        }
        else{
            return view('mobile/search', ['env'=> $this->env]);
        }

    }
/*
    public function searchlist(Request $request)
    {
        if($request->has("querystring")){
            $request->flashOnly(['querystring']);
            $request->session()->put('querystring', $request->get('querystring'));
            return view('mobile/list', ['env'=> $this->env]);
        }


             else{
                 return view('mobile/search', ['env'=> $this->env]);
                 }

    }*/



    public function review(Request $request)
    {
        //var_dump($request->session());
        //exit;
        $request->session()->put("effroute","review");

        // temporary block visit to review page
        return redirect('/')->with('Error','<div style="text-align:left;">'.trans('language.techerror01').'<br><br>'.trans('language.techerror02').'<br>'.trans('language.techerror03').'</div>');

        /*
        if($request->session()->get('querystring')===null || $request->session()->get('querystring')==""){
            return view('mobile/search', ['env'=> $this->env]);
        }else{
            if($request->has("selectdata") && $request->has("returndata")){
                $request->flashOnly(['selectdata','returndata']);
                $options=json_decode($request->session()->get('querystring'));

                $personcount=$options->adultcount+$options->childrencount+$options->infantsseatcount+$options->infantslapcount;
                $adultcount=$options->adultcount;
                $childrencount=$options->childrencount;
                $infantsseatcount=$options->infantsseatcount;
                $infantslapcount=$options->infantslapcount;
                return view('mobile/review',["querystring"=>$request->session()->get('querystring'),"personcount"=>$personcount,"adultcount"=>$adultcount,"childrencount"=>$childrencount,"infantsseatcount"=>$infantsseatcount,"infantslapcount"=>$infantslapcount , 'env'=> $this->env]);
            }
            else if($request->has("selectdata")&& !($request->has("returndata"))){
                $request->flashOnly(['selectdata']);
                $options=json_decode($request->session()->get('querystring'));
                $personcount=$options->adultcount+$options->childrencount+$options->infantsseatcount+$options->infantslapcount;
                $adultcount=$options->adultcount;
                $childrencount=$options->childrencount;
                $infantsseatcount=$options->infantsseatcount;
                $infantslapcount=$options->infantslapcount;
                return view('mobile/review',["querystring"=>$request->session()->get('querystring'),"personcount"=>$personcount,"adultcount"=>$adultcount,"childrencount"=>$childrencount,"infantsseatcount"=>$infantsseatcount,"infantslapcount"=>$infantslapcount , 'env'=> $this->env]);
            }

        }
*/
    }

    public function clean(Request $request){
        $request->session()->put('querystring',null);
        echo "done";
    }

    public function pay(){
        $request->session()->put("effroute","pay");
        return redirect('/')->with('Error','<div style="text-align:left;">'.trans('language.techerror01').'<br><br>'.trans('language.techerror02').'<br>'.trans('language.techerror03').'</div>');
        //return view('mobile/pay',['env'=> $this->env]);
    }

    public function paypal(Request $request){
        /*if(!($request->has('amt')))
        {
            $request->session()->forget('couponcode');
            $request->session()->forget('couponamt');
            return view('mobile/paypal/paypal',['env'=> $this->env]);
        }
        else*/

        if(!$request->has('amt')){
            //var_dump("0");
            $request->session()->put('couponcode',null);
            $request->session()->put('couponamt',null);

        }
        //else
            //var_dump("1");


            //exit;
        return view('mobile/paypal/paypal',['env'=> $this->env]);
    }

    public function ftatos(){
        return view('mobile/atos/ft-atos',['env'=> $this->env]);
    }

    public function atos(){
        return view('mobile/atos/atos',['env'=> $this->env]);
    }

    public function atoscancel(Request $request){
        return view('mobile/atos/atoscancel',['env'=> $this->env,"PNR"=>$request->session()->get('PNR'),"orderid"=>$request->session()->get('orderid')]);
    }

    public function atossuccess(){
        return view('mobile/atos/atossuccess',['env'=> $this->env]);
    }

    public function atosreturn(Request $request){
        return view('mobile/atos/atosreturn',['env'=> $this->env,"PNR"=>$request->session()->get('PNR'),"orderid"=>$request->session()->get('orderid')]);
    }

    public function paypalsuccess(Request $request){
        //var_dump($request->session()->get('couponamt'));
        //exit();

        //return view('mobile/paypal/paypal_success',['env'=> $this->env,'couponcode'=>$request->session()->get('couponcode'),'couponamt'=>$request->session()->get('couponamt')]);
        return view('mobile/paypal/paypal_success',['env'=> $this->env]);
    }

    public function paypalerror(Request $request){
        return view('mobile/paypal/paypal_error',['env'=> $this->env,"PNR"=>$request->session()->get('PNR'),"orderid"=>$request->session()->get('orderid')]);
    }

    public function receipt(){
        return view('mobile/layouts/receipt',['env'=> $this->env]);
    }

    public function loadcoupon(Request $request)
    {
        //var_dump("hello");
//var_dump($request->get("couponcode"));
        //$request->input('couponcode');

        $request->session()->put('couponcode', $request->couponcode);
exit;

    }

    public function search_iframe(Request $request){


        $request->session()->put('locale', 'cn');
        //var_dump($request->session());
        /*
        if($request->has('change_lang'))
        {   if($request->get('chang_lang')=="Chinese")
            $request->session()->put('locale', 'cn');
        else if($request->get('chang_lang')=="English")
            $request->session()->put('locale', 'en');
        }*/
        //$locale=Session::get('locale',Config::get('app.locale'));
        App::setLocale('cn');
        return view('mobile/layouts/iframe',['env'=> $this->env]);
    }

    public function changecurrency(Request $request){
        if ($request->ajax()) {
            $request->session()->put('currency', $request->currency);
            $request->session()->flash('alert-success', ('app.Currency_Change_Success'));
            return(json_encode(['status'=>'Ok','currency'=>$request->currency]));
        }
    }

    public function contactinfo(){
        return view('mobile/usercentre/contactinfo',['env'=> $this->env]);
    }

    public function passengerinfo(){
        return view('mobile/usercentre/passengerinfo',['env'=> $this->env]);
    }

    public function mypoints(){
        return view('mobile/usercentre/mypoints',['env'=> $this->env]);
    }

    public function policy(){
        return view('mobile/policy',['env'=> $this->env]);
    }

}
