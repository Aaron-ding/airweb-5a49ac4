<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Flight\Contract;

class ContractController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    public function getCommissionOfContract(Request $request){
        $contract=new Contract;
        $src=$request->input("src")==null?null:strtoupper($request->input("src"));
        $dest=$request->input("dest")==null?null:strtoupper($request->input("dest"));
        $airline=$request->input("airlinecode")==null?null:strtoupper($request->input("airlinecode"));
        $departuredate=$request->input("departuredate");
        $triptype=$request->input("triptype")=="Select trip type"?null:$request->input("triptype");
        $cabinclass=$request->input("carbinclass")=="Select Cabin class"?null:[$request->input("carbinclass")];

        $contracts=$contract->contractByAirline($src,$dest,$airline,$departuredate,$triptype,$cabinclass,null);
        $rs=[];
        $currcommission=0;
        $currcommissionflag="%";
        foreach ($contracts as $contract) {
            $seatclass = json_decode($contract->seatclass, TRUE);
            if (isset($seatclass) && is_array($seatclass)) {
                foreach ($seatclass as $seatkey => $class) {
                    if(!isset($class["valid"]) || $class["valid"]!=="yes"){
                        continue;
                    }

                    $commissiontypeinfo=json_decode($contract->commissiontype,TRUE);
                    $commissionpercentageinfo=json_decode($contract->commissionpercentage,TRUE);
                    $commissionamount=json_decode($contract->commissionamount,TRUE);
                    if (isset($commissionpercentageinfo[$seatkey]["percentage"])) {
                        if($currcommission<intval($commissionpercentageinfo[$seatkey]["percentage"])){
                            $currcommission= intval($commissionpercentageinfo[$seatkey]["percentage"]);
                        }
                        $currcommissionflag="%";
                    } else {
                        if (intval($commissionamount[$seatkey]["amount"])>$currcommission){
                            $currcommission=intval($commissionamount[$seatkey]["amount"]);
                        }
                        $currcommissionflag="$";
                    }

                }
            }
        }
        if ($currcommissionflag=="%"){
            $rs[]=$currcommission . " " .$currcommissionflag;
        }
        if ($currcommissionflag=="$"){
            $rs[]= $currcommissionflag ." " . $currcommission;
        }
        return $rs;

    }

    public function searchContract(Request $request){

        $contract=new Contract;

        $src=$request->input("src")==null?null:strtoupper($request->input("src"));
        $dest=$request->input("dest")==null?null:strtoupper($request->input("dest"));
        $airline=$request->input("airlinecode")==null?null:strtoupper($request->input("airlinecode"));
        $tourcode=$request->input("tourcode")==null?null:strtoupper($request->input("tourcode"));
        $departuredate=$request->input("departuredate");
        $triptype=$request->input("triptype")=="Select trip type"?null:$request->input("triptype");
        $cabinclass=$request->input("carbinclass")=="Select Cabin class"?null:[$request->input("carbinclass")];

        $request->flashOnly(['src', 'dest',"airlinecode","departuredate","triptype","carbinclass","tourcode"]);





        if($request->has("doquery")){

            $contract=new Contract;

            $contracts=$contract->contractByAirline($src,$dest,$airline,$departuredate,$triptype,$cabinclass,$tourcode);

            return view('flightcontract/flightcontract', [
                "contracts"=>$contracts,
            ]);
        }else{
            return view('flightcontract/flightcontract', [

            ]);
        }

    }

}
