<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\User\User;
use App\Models\Gds\Gds;
use App\Models\Gds\GdsView;


class GdsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function pnrPassngerCard(Request $request)
    {
        $orderid = $request->input('orderid');
        $pnr = $request->input('pnr');
        $provider = $request->input('provider');
        $pnrdata=gds::call("read_pnr", array(
            array(
                "PNR" => $pnr,
                'orderid' => $orderid,
                'provider' => $provider
            )));
        $gdsview=new GdsView();
        return array("passenger"=>$gdsview->getPassengerCard($pnrdata),"internary"=>$gdsview->getInternaryCard($pnrdata));

    }
    public function calculatePNRCommission(Request $request){
        $pnr=$request->input('pnr');
        $provider=$request->input('provider');

        $commission=gds::call("calculate_commission",array(
            array(
                "PNR" => $pnr,
                "provider"=>$provider
            )));
        $gdsview=new GdsView();
        return array("commission"=>$gdsview->getCommissionList($commission[0]));


    }


}
