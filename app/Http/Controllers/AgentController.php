<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User\Agent;
use App\Models\User\User;


class AgentController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function calculateCommission(Request $request)
    {
        $pnr = $request->input("pnrcode");
        $request->flashOnly(['pnrcode','provider']);
        return view('agent/calculatecommission', [
        ]);
    }
    public function issueAirTicekt(Request $request)
    {

    }

    public function application(Request $request){

        if($request->user()->hasPermissionTo('edit_users') && $request->has("approve") && $request->has("email")){
            //do approve
            Agent::where("email",$request->input("email"))->update(["status"=>"normal"]);
            User::where("email",$request->input("email"))->first()->assignRole("Agent");
        }

        if($request->user()->hasPermissionTo('edit_users') && $request->has("deny") && $request->has("email")){
            //do deny
            Agent::where("email",$request->input("email"))->update(["status"=>"denied"]);
        }


        $rs=Agent::where('status','in process')
            ->join('users', 'users.email', '=', 'agents.email')
            ->get();
        return view('agent/application',["result"=>$rs]);
    }


}
