<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\User\User;
use App\Models\flight\Order;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $ordermodel=new Order;
        $orders=$ordermodel->searchOrder();

        return view('home', [
            'journals' => $request->user()->allJournals('app\cash'),
            'points'=> $request->user()->allJournals('app\points'),
            "orders"=>$orders
        ]);
    }
    public function logout()
    {
        Auth::logout();
        return redirect()->intended('/');
    }
}
