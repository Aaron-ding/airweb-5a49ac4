<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\User\User;
use App\Models\Flight\Order;
use App\Models\Gds\Gds;




class OrderController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function recentorders(Request $request)
    {

        $ordermodel=new Order;
        $orders=$ordermodel->searchOrder();
        return view('order/flightorder', [
            'orders' => $orders,
        ]);
    }

    public function querypaypointsinfo(Request $request){
        $rs=array();
        $ordermodel=new Order;
        $orderid=$request->input('orderid');
        $pnr=$request->input('PNR');
        $orders=$ordermodel->searchOrderByID($orderid,$pnr);
        $point=$request->user()->allJournals('app\points',$orders[0]->currency)->first();

        $commission=gds::call("calculate_commission",array(
            array(
                "PNR" => $orders[0]->PNR,
                "provider"=>$orders[0]->provider
            )));
        $rs["maximum"]=$commission[0][0]["jcb"]*100;
        $rs["total"]=$point->balance->getAmount();
        return json_encode($rs);
    }

    public function querypayaccountinfo(Request $request){
        $rs=array();
        $ordermodel=new Order;
        $orderid=$request->input('orderid');
        $pnr=$request->input('PNR');
        $orders=$ordermodel->searchOrderByID($orderid,$pnr);
        $cashaccount=$request->user()->allJournals('app\cash',$orders[0]->currency)->first();
        $rs["maximum"]=$cashaccount->balance->getAmount();
        if($rs["maximum"]>$orders[0]->totalprice) $rs["maximum"]=$orders[0]->totalprice;
        $rs["total"]=$cashaccount->balance->getAmount();
        return json_encode($rs);
    }

    public function orderdetail(Request $request)
    {

        $ordermodel=new Order;
        $orderid=$request->input('orderid');
        $pnr=$request->input('pnr');
        $provider=$request->input('provider');
        $orders=$ordermodel->searchOrderByID($orderid,$pnr);
        $contact=$ordermodel->searchContact($orderid);
        $passengers=$ordermodel->searchPassenger($orderid);
        $internaries=$ordermodel->searchInternary($orderid);
        $payments=$ordermodel->searchPayment($orderid);

        $request->flashOnly(['orderid', 'pnr','provider']);




        return view('order/flightorderdetail', [
            'order' => $orders[0],
            "contact"=>$contact[0],
            "passengers"=>$passengers,
            "internaries"=>$internaries,
            "payments"=>$payments,
            "accountbalance"=>$request->user()->allJournals('app\cash',$orders[0]->currency),
            "pointsbalance"=>$request->user()->allJournals('app\points',$orders[0]->currency),

        ]);
    }

    public function historyorders(Request $request)
    {

        if ($request->has('startdate') && $request->has('enddate')) {
            $min = $request->input("startdate")." 00:00:00";
            $max = $request->input("enddate")." 23:59:59";
            $request->flashOnly(['startdate', 'enddate']);

            $ordermodel=new Order;

            $orders=$ordermodel->searchOrder($min,$max);

            return view('order/flightorder', [
                'orders' => $orders,"showquery"=>"yes"
            ]);
        }else{
            return view('order/flightorder', [
                "showquery"=>"yes"
            ]);
        }
    }

    public function chargeAccountPoints(Request $request){


        $chargepoints=intval($request->input('pointsamt'));
        $chargeaccount=floatval($request->input('cashamt'));

        if($chargepoints>0 || $chargeaccount>0){
            $ordermodel=new Order;
            $orderid=$request->input('orderid');
            $pnr=$request->input('PNR');
            $orders=$ordermodel->searchOrderByID($orderid,$pnr);
            $currency=$orders[0]->currency;
            $checkpayable=1;
            $msg=array();
            $paymentidx=1;
            if(count($orders)==1 && $orders[0]->lastamt>0){
                $orderpayments=$ordermodel->searchPayment($orderid);
                $paymentidx=count($orderpayments);
                foreach ($orderpayments as $payment){
                    if($payment->type=="Points"){
                        //in this case ,wo can not use points again.
                        $checkpayable=0;
                        $msg[]="You have used points to pay this order.You can not pay again.<br>Please contact our coustomer service.";
                    }
                    if($payment->type=="Account Balance"){
                        //in this case ,wo can not use Account balance again.
                        $checkpayable=0;
                        $msg[]="You have used your account balance to pay this order.You can not pay again.<br>Please contact our coustomer service.";
                    }
                }
                if($chargepoints>0){
                    $querypoint=json_decode($this->querypaypointsinfo($request));
                    if($chargepoints>$querypoint->maximum){
                        //charge too much, it is wrong.
                        $checkpayable=0;
                        $msg[]="System error.Please refresh the web page.";
                    }

                }
                if($chargeaccount>0){
                    $queryaccount=json_decode($this->querypayaccountinfo($request));
                    if($chargeaccount>$queryaccount->maximum){
                        //charge too much, it is wrong.
                        $checkpayable=0;
                        $msg[]="System error.Please refresh the web page.";
                    }
                }

            }else{
                $checkpayable=0;
                $msg[]="The order status is wrong. Please refresh the web page.";
            }



            if ($checkpayable){
                //at this point, we have pass all the check

                //step 1 : reduce the account data
                $request->user()->pointDebit($currency,$chargepoints,'',$orderid);
                $request->user()->debit($currency,$chargeaccount,'',$orderid);


                //step 2 : insert into order payment recoerd
                $rec1=[];
                $rec1["orderid"]=$orderid;
                $rec1["idx"]=++$paymentidx;
                $rec1["currency"]=$currency;
                $rec1["amt"]=$chargepoints;
                $rec1["type"]="Points";
                $rec1["status"]= "paying";
                $rec1["memo"]= '';

                $rec2=[];
                $rec2["orderid"]=$orderid;
                $rec2["idx"]=++$paymentidx;
                $rec2["currency"]=$currency;
                $rec2["amt"]=$chargeaccount*100;
                $rec2["type"]="Account Balance";
                $rec2["status"]= "paying";
                $rec2["memo"]= '';
                $ordermodel->addOrderPayment([$rec1,$rec2]);




                //step3 : redeuce the order amount
                $ordermodel->decrementOrderLastamt($orderid,$chargepoints);
                $ordermodel->decrementOrderLastamt($orderid,$chargeaccount*100);

                $orders=$ordermodel->searchOrderByID($orderid,$pnr);

                return json_encode(["success",$orders[0]->currency . " " .$orders[0]->lastamt /100]);

            }else{
                return json_encode($msg);
            }
        }else{
            return json_encode(["success"]);
        }



    }


}
