<?php

namespace App\Http\Controllers\Desktop;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\View;
use Jenssegers\Agent\Agent;





class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        global $lang;
        global $currency;

        $_SESSION["referer"]='';
        define("WEBSITEURL","http://de.air.sinorama.ca");






        global $line;
        global $lang;
        global $branch;

        global $recommandations_YUL;
        global $recommandations_YYZ;
        global $recommandations_YVR;


        View::share('currency', $currency);
        View::share('line', $line);
        View::share('lang', $lang);
        View::share('branch', $branch);

        View::share('recommandations_YUL', $recommandations_YUL);
        View::share('recommandations_YYZ', $recommandations_YYZ);
        View::share('recommandations_YVR', $recommandations_YVR);
        View::share('languagesMap',  array(
            'English' => 'en',
            'French' => 'fr',
            'Chinese' => 'cn'
        ));


    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        View::share('id', 'search');
        return view('desktop/search');
    }
    public function booking(Request $request)
    {
        View::share('id', 'booking');
        return view('desktop/booking');
    }

}
