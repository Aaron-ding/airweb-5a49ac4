<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\User\User;
use App\Models\User\Agent;


class AccountController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function profile(Request $request){
        $user=Auth::user();
        if ($request->has('firstname')) {
            $user->firstname=$request->input("firstname");
        }
        if ($request->has('lastname')) {
            $user->lastname=$request->input("lastname");
        }
        if ($request->has('sex')) {
            $user->sex=$request->input("sex");
        }
        if ($request->has('phone')) {
            $user->phone1=$request->input("phone");
        }
        if ($request->has('preferlang')) {
            $user->lang1=$request->input("preferlang");
        }
        if ($request->has('address')) {
            $user->address1=$request->input("address");
        }
        if ($request->has('city')) {
            $user->city=$request->input("city");
        }
        if ($request->has('province')) {
            $user->province=$request->input("province");
        }
        if ($request->has('country')) {
            $user->country=$request->input("country");
        }
        if ($request->has('postcode')) {
            $user->postcode=$request->input("postcode");
        }
        $user->save();

        $agent=Agent::where('email',$user->email)->first();

        $showagent=null;
        $becomeingagent=null;
        if(!isset($agent)){
            $agent=new Agent;
            $agent->email=$user->email;
            $agent->status="in process";
        }else{
            $showagent="yes";
        }

        $uri = $request->path();

        if(strrpos($uri,"becomeingagent")===false){

        }else{
            $becomeingagent="yes";
            $showagent="yes";
        }


        if ($request->has('license')) {
            $agent->license=$request->input("license");
        }
        if ($request->has('company')) {
            $agent->company=$request->input("company");
        }
        if ($request->has('companyphone')) {
            $agent->companyphone=$request->input("companyphone");
        }
        if ($request->has('companyaddress')) {
            $agent->companyaddress=$request->input("companyaddress");
        }
        if ($request->has('companyprovince')) {
            $agent->companyprovince=$request->input("companyprovince");
        }
        if ($request->has('companycountry')) {
            $agent->companycountry=$request->input("companycountry");
        }

        if(isset($agent->license)){
            $agent->save();
        }




        return view('account/profile',["agent"=>$agent,"showagent"=>$showagent,"becomeingagent"=>$becomeingagent]);
    }

    public function cashpage(Request $request)
    {

        return view('account/cashpage', $this->queryaccount($request,"app\cash"));
    }
    public function pointpage(Request $request)
    {
        return view('account/pointpage', $this->queryaccount($request,"app\points"));
    }

    private function queryaccount(Request $request,$apptype){

        $trans=null;
        $debits=null;
        $credits=null;

        $debitrec=[];
        $creditrec=[];
        $debitsum=0;
        $creditsum=0;

        if ($request->has('startdate') && $request->has('enddate')) {
            $min = $request->input("startdate") . " 00:00:00";
            $max = $request->input("enddate") . " 23:59:59";
            $currency=$request->input("currency");
            $transtype=$request->input("transtype");
            $request->flashOnly(['currency','transtype','startdate', 'enddate']);


            $trans=$request->user()->journalTrans($currency,$apptype,$min,$max,$transtype);

            foreach($trans as $var){
                if(isset($var->debit)){
                    $debitrec[]=$var;
                    $debitsum+=$var->debit;
                }
                if(isset($var->credit)){
                    $creditrec[]=$var;
                    $creditsum+=$var->credit;
                }
            }




        }
        return [
            'journals' => $request->user()->allJournals($apptype),'trans'=>$trans
            ,'debits'=>["rec"=>$debitrec,"count"=>count($debitrec),"amt"=>$debitsum]
            ,'credits'=>["rec"=>$creditrec,"count"=>count($creditrec),"amt"=>$creditsum]
        ];
    }
}
