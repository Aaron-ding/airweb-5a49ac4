<?php
/**
 * Created by PhpStorm.
 * User: localUser
 * Date: 12/4/2017
 * Time: 3:58 PM
 */

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class LocalizationController extends Controller
{
    public function index(Request $request,$locale){
        //set’s application’s locale
        app()->setLocale($locale);

        //Gets the translated message and displays it
        //echo trans('lang.msg');
    }
}