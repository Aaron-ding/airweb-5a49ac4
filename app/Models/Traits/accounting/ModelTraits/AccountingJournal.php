<?php

namespace App\Models\Traits\Accounting\ModelTraits;

use App\Models\Traits\Accounting\Models\Journal;
use Illuminate\Support\Facades\DB;


/**
 * Class AccountingJournal
 * @package Scottlaurent\Accounting\ModelTraits
 */
trait AccountingJournal {


    public function journals()
    {
        return $this->hasMany(Journal::class ,'morphed_id');
    }


    public function journal($currency,$type='app\cash'){
        return $this->initJournal($currency,$type);
    }

    public function journalTrans($currency,$type='app\cash',$min=null,$max=null,$transtype=null){
        $tablerec= Journal::where('morphed_id', $this->id)->where('morphed_type','=',$type)->where('accounting_journals.currency',$currency);
        if(isset($min)){
            $tablerec->where('accounting_journal_transactions.created_at',">=",$min);
        }
        if(isset($min)){
            $tablerec->where('accounting_journal_transactions.created_at',"<=",$max);
        }

        if(isset($transtype)){
            if($transtype=="credit"){
                $tablerec->where(DB::raw(" accounting_journal_transactions.debit "));
            }
            if($transtype=="debit"){
                $tablerec->where(DB::raw(" accounting_journal_transactions.credit "));
            }
        }

        return     $tablerec->join('accounting_journal_transactions', 'accounting_journals.id', '=', 'accounting_journal_transactions.journal_id')->orderBy('accounting_journal_transactions.created_at', 'desc')->get();
    }


    public function debit($currency,$amt,$ref_class=null,$memo=null){
        $this->journal($currency)->debitDollars($amt,$memo,null,$ref_class);
    }
    public function credit($currency,$amt,$ref_class=null,$memo=null){
        $this->journal($currency)->creditDollars($amt,$memo,null,$ref_class);
    }

    public function pointDebit($currency,$amt,$ref_class=null,$memo=null){
        $this->journal($currency,'app\points')->debit($amt,$memo,null,$ref_class);
    }
    public function pointCredit($currency,$amt,$ref_class=null,$memo=null){
        $this->journal($currency,'app\points')->credit($amt,$memo,null,$ref_class);
    }



    public function allJournals($type='app\cash',$currency=null){
        $db= Journal::where('morphed_id', $this->id)->where('morphed_type','=',$type);

        if(isset($currency)){
            $db=$db->where('currency','=',$currency);
        }

        return $db->get();
    }

	
	/**
	 * Initialize a journal for a given model object
	 *
	 * @param string $currency_code
	 * @return Journal
	 * @throws \Exception
	 */
	public function initJournal($currency_code='USD',$type='app\cash') {
        $journal=$this->journals()->where('currency',$currency_code)->where('morphed_type','=',$type)->first();
    	if (!$journal) {
            return $this->journals()->create(['currency'=>$currency_code,'balance'=>0,'morphed_type'=>$type]);
	    }else{
    	    return $journal;
        }
    }
	
}