<?php

namespace App\Models\User;

class Permission extends \Spatie\Permission\Models\Permission
{

    public static function defaultPermissions()
    {
        return [
            'view_users',
            'add_users',
            'edit_users',
            'delete_users',

            'view_roles',
            'add_roles',
            'edit_roles',
            'delete_roles',

            'view_agents',
            'view_flight_contract',
            'view_calculate_commission',
            'view_issue_ticket',

        ];
    }
}
