<?php
namespace App\Models\User;

use Illuminate\Database\Eloquent\Model;

class Agent extends Model
{
    //
    protected $fillable = [
        'license','company',
    ];

}
