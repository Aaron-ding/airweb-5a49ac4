<?php
namespace App\Models\User;

use Illuminate\Notifications\Notifiable;
use Jrean\UserVerification\Traits\UserVerification;
use Jrean\UserVerification\Traits\VerifiesUsers;
use Jrean\UserVerification\Traits\RedirectsUsers;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Permission\Traits\HasRoles;


use App\Models\Traits\Accounting\ModelTraits\AccountingJournal;

class User extends Authenticatable
{
    use Notifiable;
    use AccountingJournal;
    use HasRoles;
    use UserVerification;
    use RedirectsUsers;


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'firstname','lastname', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
}
