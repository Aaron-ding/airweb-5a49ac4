<?php
namespace App\Models\flight;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;



class Order extends Model
{
    private $env;
    public function __construct()
    {
        $this->env=config('app.env');
    }

    public function searchOrder($min=null,$max=null){

        if ($min===null){
            $min=DB::raw('now() - INTERVAL 3 DAY');
        }

        if ($max===null){
            $max=DB::raw('now()');
        }


        $orders = DB::connection('flightdb')->table('t_order')
            /*->where ('channelid','=',$this->env)*/
            ->where('orderid','=','T0M000000000496')
            //->where('createdatetime','>=',$min)
            ->where('createdatetime','<=',$max)
            ->orderBy('createdatetime', 'desc')->get();

        return $orders;
    }

    public function searchOrderByID($orderid,$pnr){
        return DB::connection('flightdb')->table('t_order')
            //->where ('channelid','=',$this->env)
            ->where('orderid','=',$orderid)
            //->where("PNR",'=',$pnr)
            ->get();
    }

    public function searchContact($orderid){
        return DB::connection('flightdb')->table('t_contact')
            ->where('orderid','=',$orderid)->get();

    }
    public function searchPassenger($orderid){
        return DB::connection('flightdb')->table('t_passenger')
            ->where('orderid','=',$orderid)->get();

    }

    public function searchInternary($orderid){
        $recs= DB::connection('flightdb')->table('t_itineraries')
            ->where('orderid','=',$orderid)->get();
      if(isset($recs[0])) {
          return $recs[0]->flightsegments;
      }else{
          return "";
      }


    }

    public function searchPayment($orderid){
        return DB::connection('flightdb')->table('t_order_payment')
            ->where('orderid','=',$orderid)->get();

    }

    public function addOrderPayment($para){
        $para2=[];
        foreach($para as $rec){
            $rec["createtime"]= DB::raw('now()');
            $para2[]=$rec;
        }
        return DB::connection('flightdb')->table('t_order_payment')->insert($para2);
    }

    public function decrementOrderLastamt($orderid,$amt){
        return DB::connection('flightdb')->table('t_order')->where('orderid','=', $orderid)
            ->update(['lastamt' => DB::raw('lastamt-'.$amt)]);
    }


}

