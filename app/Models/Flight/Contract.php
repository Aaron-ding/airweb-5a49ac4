<?php
namespace App\Models\flight;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;


class Contract extends Model
{

    private $env;

    public function __construct()
    {
        $this->env=config('app.env');
    }

    public function contractByAirline($src=null,$dest=null,$airline=null,$departuredate=null,$triptype=null,$cabinclass=null,$tourcode=null){

        $handle=DB::connection('flightdb')->table('t_airfarecontract')
            ->where("channelid","=",$this->env)
            ->where(DB::raw("if(substr(contractdate,13,10)=\"undefined\",'3000-12-31',substr(contractdate,13,10))"),">=",DB::raw("now()"));

        if ($airline!==null){
            $handle=$handle->where("airlinecode",'=',$airline);
        }

        if ($src!==null){
            $handle=$handle->where("original",'like',DB::raw("'%".$src."%'"));
        }

        if ($dest!==null){
            $handle=$handle->where("destination",'like',DB::raw("'%".$dest."%'"));
        }

        if ($tourcode!==null){
            $handle=$handle->where("tourcode",'=',$tourcode);
        }

        $contracts = $handle->get();

        $rs=array();
        foreach ($contracts as $contract){

            if($contract->updatestatus == 'VALID') {
                $validsetkeys=Array();
                $checkcondition = 1;
                if (strlen($contract->seatclass) > 0){
                    $seatclass = json_decode($contract->seatclass, TRUE);
                    if (isset($seatclass) && is_array($seatclass)){
                        $cabinclasspasscheck=0;
                        foreach ($seatclass as $seatkey => $class) {

                            if (strlen($contract->season) > 0) {
                                //check the date range
                                $seasons = json_decode($contract->season, TRUE);
                                $lastseasons=$seasons;
                                if (!isset($seasons["sedate1"])){
                                    $lastseasons=$seasons[$seatkey];
                                }

                                if ($departuredate!==null) {
                                    $checkseason=1;
                                    foreach ($lastseasons as $tmpkey => $seasonitem) {
                                        if ($seasonitem != "" && $seasonitem != "NONE") {
                                            $checkseason=0;
                                            //we need to consider season facts.
                                            $datetodb = strtotime(substr($seasonitem, 12, 10));
                                            $datefromdb = strtotime(substr($seasonitem, 0, 10));
                                            $departure = strtotime($departuredate);

                                            if ($departure >= $datefromdb && $departure <= $datetodb)
                                            {
                                                $checkseason = 1;
                                                break;

                                            }
                                        }
                                    }
                                    if ($checkseason==0) $checkcondition=0;
                                }
                            }

                            if (strlen($contract->blackout) > 0) {
                                //check unexcepted date
                                $blackouts = json_decode($contract->blackout, TRUE);
                                if ($departuredate!==null) {
                                    $checkblackout = 1;
                                    foreach ($blackouts as $tmpkey => $blakeitem) {
                                        if ($blakeitem != "" && $blakeitem != "NONE") {
                                            $checkblackout = 1;
                                            //we need to consider season facts.
                                            $datetodb = strtotime(substr($blakeitem, 12, 10));
                                            $datefromdb = strtotime(substr($blakeitem, 0, 10));
                                            $departure = strtotime($departuredate);

                                            if ($departure >= $datefromdb && $departure <= $datetodb) {
                                                $checkblackout = 0;
                                                break;
                                            }
                                        }
                                    }
                                    if ($checkblackout==0) $checkcondition=0;
                                }
                            }



                            if ($cabinclass!==null){
                                //now we need check cabin class
                                foreach($cabinclass as $cabinrow){
                                    if (strpos(strtolower($class["seatclass"]),strtolower($cabinrow))!==false) {
                                        $cabinclasspasscheck++;
                                    }else{
                                        $seatclass[$seatkey]["valid"]="no";
                                    }
                                }
                            }

                            if($triptype!==null ){
                                $triptypelist=json_decode($contract->triptype,TRUE);
                                if(strpos(strtolower($triptypelist[$seatkey]["triptype"]),strtolower($triptype))===false){
                                    $checkcondition=0;
                                }
                            }

                            $checkfromandto=1;

                            if ($src!=null){
                                $fromlist=json_decode($contract->original,TRUE);
                                if(strpos(strtolower($fromlist[$seatkey]["original"]),strtolower($src))===false){
                                    $checkfromandto=0;
                                }
                            }

                            if ($dest!=null){
                                $tolist=json_decode($contract->destination,TRUE);

                                if(strpos(strtolower($tolist[$seatkey]["destination"]),strtolower($dest))===false){
                                    $checkfromandto=0;
                                }
                            }

                            if($checkfromandto==1) {
                                $validsetkeys[]=$seatkey;
                                if(isset($seatclass[$seatkey]["valid"]) && $seatclass[$seatkey]["valid"]=="no"){
                                    //do nothing
                                }else{
                                    $seatclass[$seatkey]["valid"]="yes";
                                }
                            }




                        }
                    }
                    if ($cabinclass!==null){
                        if($cabinclasspasscheck<count($cabinclass)) $checkcondition=0;
                    }
                }

                if(count($validsetkeys)<=0) $checkcondition=0;

                $contract->seatclass=json_encode($seatclass);

                $contract->validsetkeys=$validsetkeys;

                if ($checkcondition===1) {
                    //yes, this contract is good for this search
                    $rs[]=$contract;
                }

            }
        }

        return $rs;
    }
}

