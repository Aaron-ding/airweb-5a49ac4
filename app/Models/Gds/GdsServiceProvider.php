<?php
/**
 * Created by PhpStorm.
 * User: aaron
 * Date: 2017-07-22
 * Time: 11:55 AM
 */
namespace App\Models\Gds;

use Illuminate\Support\ServiceProvider;

class GdsServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->bind('gds', Dnode::class);
    }
}