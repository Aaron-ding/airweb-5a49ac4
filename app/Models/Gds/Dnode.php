<?php
namespace App\Models\Gds;
/**
 * Created by PhpStorm.
 * User: aaron
 * Date: 2017-07-22
 * Time: 10:42 AM
 */



require_once 'DnodeSyncClient.php';

class Dnode {


    private $env="dev";

    public function __construct()
    {
        $this->env=config('app.env');
        $this->middleware('auth');
    }


    public function call($fun,$para){
        return $this->dnode($fun,$para);
    }
    public function remote_exec($fun,$para){
        return $this->dnode_exec($fun,$para);
    }

    private function dnode($fun,$para){
        $dnode = new \DnodeSyncClient\Dnode();
        $svraddr="";
        if ($this->env=="dev"){
            $svraddr="dev.search.air.sinorama.ca";
        }
        if ($this->env=="master"){
            $svraddr="search.air.sinorama.ca";
        }
        // echo $svraddr."######" . $fun;
        if ($svraddr!=""){
            $connection = $dnode->connect($svraddr, 9998);

            return $connection->call($fun,$para);
        }else{
            echo "I do not know where am I";
            return;
        }
    }



    private  function dnode_exec($fun,$para){
        $dnode = new \DnodeSyncClient\Dnode();
        $svraddr="";
        if ($this->env=="dev"){
            $svraddr="dev.search.air.sinorama.ca";
        }
        if ($this->env=="master"){
            $svraddr="search.air.sinorama.ca";
        }

        if ($svraddr!=""){
            $connection = $dnode->connect($svraddr, 9998);
            return $connection->call("exec",array($para,$fun));
        }else{
            echo "I do not know where am I";
            return;
        }
    }


    private  function dnode_pnrexec($fun,$para){
        $dnode = new \DnodeSyncClient\Dnode();
        $svraddr="";
        if ($this->env=="dev"){
            $svraddr="dev.search.air.sinorama.ca";
        }
        if ($this->env=="master"){
            $svraddr="search.air.sinorama.ca";
        }
        if ($svraddr!=""){
            $connection = $dnode->connect($svraddr, 9998);
            return $connection->call("pnrexec",array($para,$fun));
        }else{
            echo "I do not know where am I";
            return;
        }
    }

}