<?php
/**
 * Created by PhpStorm.
 * User: aaron
 * Date: 2017-07-22
 * Time: 12:48 PM
 */
namespace App\Models\Gds;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\View;


class GdsView extends Model
{
    public function getPassengerCard($pnr){
        $content="";
        $i=0;
        foreach($pnr[0]["SpecialServiceInfo"] as $serviceinfo){
            $passenger=(object)[];
            $passenger->index=++$i;
            if($serviceinfo["Service"][0]["$"]["SSR_Type"]=="DOCS"){
                $name=explode("/",$serviceinfo["Service"][0]["PersonName"][0]["_"]);
                $passenger->firstname=$name[1];
                $passenger->lastname=$name[0];
                $passenger->middle="";
                $text=explode("/",$serviceinfo["Service"][0]["Text"][0]);
                if($text[1]=="P"){
                    $passenger->ctype="Passport";
                }else{
                    continue;
                }
                $passenger->sex=$text[6];
                $passenger->nationality=$text[4];
                $passenger->birthday=$text[5];
                $passenger->type="A";
                $passenger->cno=$text[3];
                $passenger->cexpired=$text[7];
                $passenger->ccountry=$text[2];

                $view = View::make('layouts/parts/passengerdetail', ['passenger' => $passenger]);
                $content .= $view->render();
            }

        }
        return $content;

    }

    public function getInternaryCard($pnr){
        $content="";
        $i=0;
        foreach($pnr[0]["ItineraryInfo"]["ReservationItems"][0]["Item"] as $reserve){
            $internary=array();
            $internary["index"]=++$i;
            $internary["From"]=$reserve["FlightSegment"][0]["OriginLocation"][0]["$"]["LocationCode"];
            $internary["To"]=$reserve["FlightSegment"][0]["DestinationLocation"][0]["$"]["LocationCode"];

            $internary["From_terminal"]="N/A";
            $internary["To_terminal"]="N/A";
            if(isset($reserve["FlightSegment"][0]["OriginLocation"][0]["$"]["Terminal"])) $internary["From_terminal"]=$reserve["FlightSegment"][0]["OriginLocation"][0]["$"]["Terminal"];
            if(isset($reserve["FlightSegment"][0]["DestinationLocation"][0]["$"]["Terminal"])) $internary["To_terminal"]=$reserve["FlightSegment"][0]["DestinationLocation"][0]["$"]["Terminal"];


            $internary["Airline"]=$reserve["FlightSegment"][0]["MarketingAirline"][0]["$"]["Code"];
            $internary["Flightnumber"]=$reserve["FlightSegment"][0]["MarketingAirline"][0]["$"]["FlightNumber"];
            $internary["DepartureDateTime"]=$reserve["FlightSegment"][0]["$"]["DepartureDateTime"];
            $internary["ArrivalDateTime"]=$reserve["FlightSegment"][0]["$"]["ArrivalDateTime"];

            $ElapsedTime=explode(".",$reserve["FlightSegment"][0]["$"]["ElapsedTime"]);
            $internary["ElapsedTimeTotalHours"]=$ElapsedTime[0];
            $internary["ElapsedTimeTotalHoursLeftMins"]=$ElapsedTime[1];
            $internary["MarketingCabin"]=$reserve["FlightSegment"][0]["$"]["ResBookDesigCode"];

            $internary["status"]=$reserve["FlightSegment"][0]["$"]["Status"];

            $view = View::make('layouts/parts/internary', ['internary' => $internary]);
            $content .= $view->render();


        }
        return $content;

    }
    public function getCommissionList($commlist){
        $content="";
        $view = View::make('layouts/parts/commissionlist', ['commission' => $commlist]);
        $content .= $view->render();
        return $content;
    }
}