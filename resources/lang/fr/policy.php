<?php

return [
    'text'=>'<h5>Politique de confidentialité</h5>
<div class="color-gray" style="font-size: 12px;">
La confiance et la fidélité de nos clients sont importantes pour nous. Nous comprenons que les renseignements personnels sont également très importants pour vous. GROUPE SINORAMA s\'engage à protéger vos renseignements personnels. Cette politique de confidentialité explique comment nous traitons et protégeons vos renseignements personnels. En fournissant vos renseignements personnels, vous consentez à la collecte, l\'utilisation, la divulgation, l\'entreposage de vos renseignements personnels conformément à la présente politique de confidentialité. Nous vous recommandons de consulter cette politique régulièrement car cette dernière peut être modifiée de temps en temps.Dans cette politique, GROUPE SINORAMA désigne le groupe de société dont le siège social est Vacances Sinorama Inc., et de toutes ses filiales en Europe et au Canada.<br>
</div>

<h5>Comment recueillons-nous vos renseignements personnels ?</h5>
<div class="color-gray" style="font-size: 12px;">
Nous recueillons vos renseignements personnels directement auprès de vous dans le cadre de la fourniture d\'un produit ou d\'un service ou si vous avez d\'autres rapports avec nous. Par exemple, vos renseignements personnels seront recueillis lorsque vous communiquez avec nous par téléphone, par courrier ou par courriel, visitez notre site Web, remplissez un formulaire d\'inscription en ligne, fournissez des renseignements pour compléter la réservation, vous abonnez à un service fourni par nous ou lorsque vous visitez notre bureau.<br>
</div>

<h5>Quels renseignements personnels recueillons-nous?</h5>
<div class="color-gray" style="font-size: 12px;">
Nous recueillons des renseignements personnels liant aux arrangements de votre voyage ou qui nous permettront de vous proposer des produits et des services.<br>
</div>

<h5>Ce que nous faisons avec vos renseignements personnels ?</h5>
<div class="color-gray" style="font-size: 12px;">
Nous pouvons utiliser vos renseignements personnels pour:<br>
<br>
• vous fournir nos produits et services ;<br>
<br>
• Tenir compte de votre demande pour un produit ou service ;<br>
<br>
• vous fournir d\'informations sur d\'autres produits et services qui peuvent vous intéresser ;<br>
<br>
• Arranger votre voyage avec les fournisseurs (tels que les compagnies aériennes, les agents de voyages, les hôtels et les compagnies d\'assurance) par rapport à la fourniture d\'un produit ou d\'un service ;<br>
<br>
• Effectuer toutes autres tâches administratives et opérationnelles (y compris la gestion des risques, le développement de systèmes et de tests, la formation du personnel, et études de marché ou de satisfaction de la clientèle).<br>
</div>

<h5>Quand divulguons-nous vos renseignements personnels ?</h5>
<div class="color-gray" style="font-size: 12px;">
Nous ne divulguerons vos renseignements personnels qu\'à des fins énoncées dans la présente politique de confidentialité. Nous pouvons divulguer vos renseignements personnels lorsque nous sommes tenus par la loi de le faire.<br>
<br>
Nous divulguerons vos renseignements personnels à des fournisseurs (tel que les compagnies aériennes, les agents de voyages, les hôtels et les compagnies d\'assurance) des produits ou des services que vous avez achetés. Vous connaissez que tous les fournisseurs ne peuvent avoir des politiques équivalentes à notre politique de confidentialité et vous consentez à la divulgation de vos renseignements personnels à cette fin.
<br><br>
Promotions sur notre site web peuvent être parrainées par un tiers ou peuvent être coparrainés. Certaines ou toutes les informations recueillies lors d\'une promotion peuvent être divulguées à la partie qui parraine ou co-commanditaires de la promotion. Nous pouvons divulguer vos renseignements personnels à des tiers lorsque nous donnons en sous-traitance certaines de nos fonctions et activités.<br>
</div>

<h5>Stockage et sécurité des renseignements personnels</h5>
<div class="color-gray" style="font-size: 12px;">
Nous stockons les renseignements personnels dans une combinaison des installations de stockage sécurisées informatiques, des installations à base de papier et d\'autres dossiers.<br>
<br>
Nous nous efforcerons de prendre toute précaution raisonnable pour assurer la sécurité de l\'information dans nos systèmes. Nous utilisons une combinaison de mesures de sécurité pour empêcher l\'accès non autorisé de nos serveurs et à réexaminer régulièrement nos politiques de sécurité.<br>
<br>
Les pages de paiement sur ce site sont cryptées grâce au protocole SSL de système de sécurité internationalement acceptées (Secure Sockets Layer) pour crypter tous les paiements sur le web. Cela signifie que les informations de ces pages sont envoyées à notre serveur de façon sécurisée et ne peut être interceptée.<br>
</div>

<h5>Des flux de données transfrontaliers et de sites web liés</h5>
<div class="color-gray" style="font-size: 12px;">
Nous divulguons vos renseignements personnels à des fournisseurs et autres fournisseurs de biens ou de produits que vous achetez, qui peuvent être à l\'étranger. Nous pouvons également divulguer vos renseignements personnels autorisés dans cette politique de confidentialité pour les entités liées ou les filiales qui peuvent être à l\'étranger. Vous acceptez que les fournisseurs étrangers ne puissent pas avoir mis en place l\'équivalent politique de confidentialité. Notre site web contient un certain nombre de liens vers d\'autres sites web. Nous ne sommes pas responsables pour les politiques de confidentialité des autres sites web. Vous devriez vérifier la politique de confidentialité de tout site web lié.<br>
</div>

<h5>Accès et mise à jour de vos informations personnelles</h5>
<div class="color-gray" style="font-size: 12px;">
Sous réserve de certaines exceptions prévues dans la Loi sur la protection des renseignements personnels, vous pouvez savoir quelles sont les renseignements personnels que nous détenons sur vous. Vous pouvez nous contacter pour en savoir plus sur les renseignements personnels que nous détenons sur vous. Nous aurons besoin de vérifier votre identité avant de vous donner l\'accès.<br>
</div>

<h5>Cookies</h5>
<div class="color-gray" style="font-size: 12px;">
Comme la plupart des sites GROUPE SINORAMA utilise des cookies. Les cookies sont de petits fichiers qu\'un site ou ses fournisseurs de services transfèrent sur le disque dur de votre ordinateur par votre navigateur Web (si vous permettez) qui permet aux sites ou aux systèmes du fournisseur de services de reconnaître votre navigateur et de capturer et retenir certaines informations. Cette information nous permet de vous distinguer des autres utilisateurs du site qui à son tour nous permet de vous offrir une expérience de navigation plus positive. Les cookies utilisés par GROUPE SINORAMA ne sont pas utilisés pour enregistrer des informations personnellement identifiables. Vous trouverez de plus amples renseignements en cookies sur cet article Wikipedia article ou sur www.aboutcookies.org.<br>
<br>
Si vous ne souhaitez pas utiliser un cookie, vous pouvez ajuster les paramètres de votre navigateur pour refuser les cookies, supprimer les cookies existants ou vous avertir quand ils sont utilisés. Pour en savoir plus sur la gestion de vos paramètres de cookies Veuillez vous visiter : www.aboutcookies.org.<br>
</div>

<h5>Informations sensibles</h5>
<div class="color-gray" style="font-size: 12px;">
Nous ne recueillons, n\'utilisons et ne divulguons des informations sensibles qu\'avec votre consentement et selon la nécessité des arrangements de votre voyage. Sinon, nous ne recueillons, n\'utilisons et ne divulguons vos renseignements sauf requis par la loi. Les informations sensibles , telles que définies par la loi , sont les informations sur la race ou l\' origine ethnique d\'une personne , l\'opinion politique , l\'appartenance à une association politique , les croyances ou les affiliations religieuses , les croyances philosophiques , l\'appartenance à une association professionnelle ou commerciale , l\'appartenance à un syndicat , les préférences ou les pratiques sexuelles, casier judiciaire ou information sur la santé .<br>
</div>

<h5>Informations sur les autres voyageurs</h5>
<div class="color-gray" style="font-size: 12px;">
Si vous nous fournissez des informations sur d\'autres personnes voyageant avec vous (par exemple, les membres de la famille), vous nous garantissez que vous êtes autorisé à fournir leurs informations et que vous leur faites connaitre de cette politique de confidentialité et de leurs droits en vertu de cette dernière.<br>
</div>

<h5>Liens externes</h5>
<div class="color-gray" style="font-size: 12px;">
Si une partie de ce site vous relie à d\'autres sites, ces sites ne fonctionnent pas dans cette politique de confidentialité. Nous vous recommandons d\'examiner les déclarations de confidentialité affichées sur ces autres sites Web afin de comprendre leurs modalités de collecte, d\'utilisation et de divulgation de renseignements personnels.<br>
</div>
<br>',
];
