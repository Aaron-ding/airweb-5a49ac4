<?php

return [
    'text'=>'<h5>Privacy Policy</h5>
<div class="color-gray" style="font-size: 12px;">
The confidence and trust of our customers are important to us. We understand that your privacy is very important to you. SINORAMA GROUP is committed to protecting your privacy. This Privacy Policy explains how we deal with and protect your personal information. By providing your personal information, you agree and consent to the collection, use, disclosure, storage and holding of your personal information in accordance with this Privacy Policy. We recommend that you review this policy regularly as we may amend it from time to time.In this policy, SINORAMA GROUP means the group of companies of which the parent company is Vacances Sinorama Inc., and all its affiliates in Europe and Canada.<br>
</div>

<h5>How do we collect your personal information?</h5>
<div class="color-gray" style="font-size: 12px;">
We collect your personal information directly from you in the course of providing a product or service or when you have other dealings with us. For example, your personal information will be collected when you deal with us over the telephone, send us a letter or an email, visit our website, complete an online registration form, provide information to complete a booking, subscribe to a service provided by us or when you visit us.<br>
</div>

<h5>What personal information do we collect?</h5>
<div class="color-gray" style="font-size: 12px;">
We only collect personal information about you that is relevant to your travel arrangements or that will enable us to provide you with our goods and services.<br>
</div>

<h5>What we do with your personal information?</h5>
<div class="color-gray" style="font-size: 12px;">
We may use your personal information to:<br>
<br>
• Provide you with our products and services;<br>
<br>
• Consider your request for a product or service;<br>
<br>
• Provide you with information about other products and services that may be of interest to you;<br>
<br>
• Assist in arrangements with suppliers (such as airlines, tour operators, hotels and insurance providers) in relation to the provision of a product or service;<br>
<br>
• Perform all other administrative and operational tasks (including risk management, systems development and testing, staff training, and market or customer satisfaction research).<br>
</div>

<h5>When do we disclose your personal information?</h5>
<div class="color-gray" style="font-size: 12px;">
We will not disclose your personal information for any purpose other than the purposes contained in this Privacy Policy. We may disclose your personal information where we are required by law to do so.<br>
<br>
We will disclose your personal information to suppliers (such as airlines, tour operators, hotels and insurance providers) of products or services which you have purchased. You acknowledge that not all suppliers may have privacy policies equivalent to our Privacy Policy and you consent to the disclosure of your personal information for this purpose.<br>
<br>
Promotions on our website may be sponsored by a third party or may be co-sponsored. Some or all of the information collected during a promotion may be disclosed to the party which sponsors or co-sponsors the promotion. We may disclose your personal information to third parties when we contract out some of our functions and activities.<br>
</div>

<h5>Storage and security of personal information</h5>
<div class="color-gray" style="font-size: 12px;">
We store personal information in a combination of secure computer storage facilities, paper based facilities and other records.<br>
<br>
We will endeavor to take all reasonable precautions to maintain the security of the information in our systems. We use a combination of security measures to prevent the unauthorized access of our servers and regularly review our security policies.<br>
<br>
The payment pages on this site are encrypted using the internationally accepted security system SSL (Secure Sockets Layer) to encrypt all payments over the web. This means that the information from these pages are sent back to our server in a secure fashion and cannot be intercepted.<br>
</div>

<h5>Trans-border data flows and linked websites</h5>
<div class="color-gray" style="font-size: 12px;">
We will disclose your personal information to travel suppliers and other suppliers of goods or products that you purchase, which may be overseas. We may also disclose your personal information as permitted in this Privacy Policy to related entities or subsidiaries which may be overseas. You accept that overseas suppliers may not have in place equivalent privacy policies. Our website contains a number of links to other websites. We are not responsible for privacy policies of any linked website. You should check the privacy policy of any linked website.<br>

<h5>Accessing and updating your personal information</h5>

Subject to some exceptions contained in the Privacy Act, you can find out what personal information we hold about you. You can contact us to find out the personal information we hold about you. We will need to verify your identity before giving you access.<br>
</div>

<h5>Cookies</h5>
<div class="color-gray" style="font-size: 12px;">
Like most websites SINORAMA GROUP uses cookies. Cookies are small files that a site or its service provider transfers to your computer\'s hard drive through your Web browser (if you allow) that enables the sites or service provider\'s systems to recognize your browser and capture and remember certain information. This information enables us to distinguish you from other users of the website which in turn helps us to provide you with a more positive browsing experience. Cookies used by SINORAMA GROUP are not used to record any personally identifiable information. You can read more about cookies on this Wikipedia article or on www.aboutcookies.org.<br>

If you do not wish to use a cookie, you can adjust your browser settings to reject cookies, delete existing cookies or notify you when they are being used. To find out more about managing your cookie settings then visit: www.aboutcookies.org.<br>
</div>

<h5>What about sensitive information?</h5>
<div class="color-gray" style="font-size: 12px;">
We will only collect, use or disclose sensitive information with your consent and where it is desirable for your travel arrangements. Unless otherwise required by law. Sensitive information, as defined by law, is any information about a person\'s racial or ethnic origin, political opinion, membership of a political association, religious beliefs or affiliations, philosophical beliefs, membership of a professional or trade association, membership of a trade union, sexual preferences or practices, criminal record or health information.<br>
</div>

<h5>Information about other travelers</h5>
<div class="color-gray" style="font-size: 12px;">
If you provide us with information about other people traveling with you (for example, family members), you warrant to us that you are authorized to provide their information and that you will make them aware of this Privacy Policy and their rights under it.<br>
</div>

<h5>External links</h5>
<div class="color-gray" style="font-size: 12px;">
If any part of this website links you to other sites, those sites do not operate under this Privacy Policy. We recommend you examine the privacy statements posted on those other websites to understand their procedures for collecting, using, and disclosing personal information.</div><br>',
];
