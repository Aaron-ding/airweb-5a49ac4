@extends('layouts.mainframe')

@section('extracss')
    <!-- Date Picker css -->
    <link rel="stylesheet"
          href="/bower_components/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css"/>

    <!-- Bootstrap Date-Picker css -->
    <link rel="stylesheet" href="/assets/plugins/bootstrap-datepicker/css/bootstrap-datetimepicker.css"/>
    <link rel="stylesheet" type="text/css" href="/bower_components/bootstrap-daterangepicker/daterangepicker.css"/>

@endsection

@section('content')

    <div class="content-wrapper">
        <!-- Container-fluid starts -->
        <!-- Main content starts -->
        <div class="container-fluid">
            <div class="row" style="margin-bottom:20px;">
                <div class="main-header">
                    <ol class="breadcrumb breadcrumb-title breadcrumb-arrow">
                        <li class="breadcrumb-item"><a href="/home"><i class="icofont icofont-home"></i></a>
                        </li>
                        <li class="breadcrumb-item"><a href="#!">Agent Menu</a>
                        </li>

                        <li class="breadcrumb-item"><a href="#!">Query flight contracts</a>
                        </li>

                    </ol>
                </div>
            </div>
            <form id="searchorder" class="md-float-material" method="GET" action="{{ route('searchcontract') }}">
                {{ csrf_field() }}
                <input type="hidden" name="doquery" value="yes">

                <div class="row" style="">
                    <div class="col-lg-3 col-xs-12">
                        <div class="md-input-wrapper">
                            <input type="text" name="airlinecode" class="md-form-control
                            @if (null!=old('airlinecode'))
                                    md-valid
                            @endif" value="{{old("airlinecode")}}"/>
                            <label>Airline</label>
                        </div>
                    </div>
                    <div class="col-lg-3 col-xs-12">
                        <div class="md-input-wrapper">
                            <input type="text" name="tourcode" class="md-form-control
                            @if (null!=old('tourcode'))
                                    md-valid
                            @endif" value="{{old("tourcode")}}"/>
                            <label>Tour code</label>
                        </div>
                    </div>
                    <div class="col-lg-3 col-xs-12">
                        <div class="md-input-wrapper">
                            <input type="text" name="src" class="md-form-control
                            @if (null!=old('src'))
                                    md-valid
                            @endif" value="{{old("src")}}"/>
                            <label>Departure air port</label>
                        </div>
                    </div>
                    <div class="col-lg-3 col-xs-12">
                        <div class="md-input-wrapper">
                            <input type="text" name="dest" class="md-form-control
                            @if (null!=old('dest'))
                                    md-valid
                            @endif" value="{{old("dest")}}"/>
                            <label>Destination air port</label>
                        </div>
                    </div>

                </div>
                <div class="row" style="margin-bottom: 20px;">
                    <div class="col-lg-3 col-xs-12">

                        <div id="departuredate" class="input-group date input-group-date-custom"
                             style="margin-bottom:10px;">
                            <input type="text" name="departuredate" class="form-control" vallue="">
                            <span class="input-group-addon bg-primary">
                                <i class="icofont icofont-ui-calendar"></i>
                            </span>
                        </div>
                    </div>
                    <div class="col-lg-3 col-xs-12">

                        <div class="md-input-wrapper">
                            <select id="triptype" name="triptype" class="md-form-control">
                                <option>Select trip type</option>
                                <option @if(old("triptype")=="oneway") selected @endif value="oneway">ONE-WAY</option>
                                <option @if(old("triptype")=="roundtrip") selected @endif value="roundtrip">ROUND-TRIP
                                </option>
                                <option @if(old("triptype")=="circletrip") selected @endif value="circletrip">
                                    CIRCLE-TRIP
                                </option>
                                <option @if(old("triptype")=="singleopenjaw") selected @endif value="singleopenjaw">
                                    SINGLE OPEN-JAW
                                </option>
                                <option @if(old("triptype")=="multiopenjaw") selected @endif value="multiopenjaw">MULTI
                                    OPEN-JAW
                                </option>
                                <option @if(old("triptype")=="multitrip") selected @endif value="multitrip">MULTI-TRIP
                                </option>
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-3 col-xs-12">

                        <div class="md-input-wrapper">

                            <select id="carbinclass" name="carbinclass" class="md-form-control">
                                <option>Select Cabin class</option>
                                <option @if(old("carbinclass")=="A") selected @endif>A</option>
                                <option @if(old("carbinclass")=="B") selected @endif>B</option>
                                <option @if(old("carbinclass")=="C") selected @endif>C</option>
                                <option @if(old("carbinclass")=="D") selected @endif>D</option>
                                <option @if(old("carbinclass")=="E") selected @endif>E</option>
                                <option @if(old("carbinclass")=="F") selected @endif>F</option>
                                <option @if(old("carbinclass")=="G") selected @endif>G</option>
                                <option @if(old("carbinclass")=="H") selected @endif>H</option>
                                <option @if(old("carbinclass")=="I") selected @endif>I</option>
                                <option @if(old("carbinclass")=="J") selected @endif>J</option>
                                <option @if(old("carbinclass")=="K") selected @endif>K</option>
                                <option @if(old("carbinclass")=="L") selected @endif>L</option>
                                <option @if(old("carbinclass")=="M") selected @endif>M</option>
                                <option @if(old("carbinclass")=="N") selected @endif>N</option>
                                <option @if(old("carbinclass")=="O") selected @endif>O</option>
                                <option @if(old("carbinclass")=="P") selected @endif>P</option>
                                <option @if(old("carbinclass")=="Q") selected @endif>Q</option>
                                <option @if(old("carbinclass")=="R") selected @endif>R</option>
                                <option @if(old("carbinclass")=="S") selected @endif>S</option>
                                <option @if(old("carbinclass")=="T") selected @endif>T</option>
                                <option @if(old("carbinclass")=="U") selected @endif>U</option>
                                <option @if(old("carbinclass")=="V") selected @endif>V</option>
                                <option @if(old("carbinclass")=="W") selected @endif>W</option>
                                <option @if(old("carbinclass")=="X") selected @endif>x</option>
                                <option @if(old("carbinclass")=="Y") selected @endif>Y</option>
                                <option @if(old("carbinclass")=="Z") selected @endif>Z</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-3 col-xs-12">
                        <button type="submit" class="btn btn-primary waves-effect waves-light" style="width:100%;">
                            QUERY
                        </button>
                    </div>
                </div>
            </form>

            @isset($contracts)
            <div class="row">
                <div class="col-xl-12 col-xs-12">
                    <h4>Total Rows:{{count($contracts)}}</h4>
                </div>
            </div>

            @endisset


            <div class="row">
                @isset($contracts)

                @foreach ($contracts as $contract)
                    <div class="col-xl-4">
                        <div class="card">
                            <div class="card-header" style="padding:10px;padding-bottom:0px;background:#5cb85c">
                                <div class="row" align="left">
                                    <div align="left" style="line-height:20px;" class="col-lg-4 col-xs-12">
                                        <img class="img-renctle "
                                             src="https://air.sinorama.ca/assets/images/logo/{{$contract->airlinecode}}.png"
                                             style="width:40px;" alt="User Image">
                                    </div>
                                    <div class="col-lg-8 col-xs-12">
                                        <h3 class="card-header-text"
                                            style="color:#FFF;font-size: 24px;font-weight: 600;">{{$contract->airlinename}}</h3>
                                    </div>

                                </div>
                            </div>


                            <div class="cd-pricing-body">
                                <ul class="basic-list" style="padding-left:10px;padding-right: 10px;">
                                    <li class="">
                                        <div class="row">
                                            <div class="col-lg-4 col-xs-4">
                                                <h6>Tour Code:</h6>
                                            </div>
                                            <div class="col-lg-8 col-xs-8">
                                                {{$contract->tourcode}}<span style="margin-left:20px;"><a
                                                            target="_blank"
                                                            href="https://crud.sinorama.ca/<?php echo $contract->contractfile; ?>">View raw file</a></span>
                                            </div>
                                        </div>


                                    </li>
                                    <li class="">
                                        <div class="row">
                                            <div class="col-lg-4 col-xs-4">
                                                <h6>IT permit:</h6>
                                            </div>
                                            <div class="col-lg-8 col-xs-8">
                                                {{$contract->itstatus}}
                                            </div>
                                        </div>


                                    </li>
                                    @php
                                        $seatclass = json_decode($contract->seatclass, TRUE);
                                        if (isset($seatclass) && is_array($seatclass)){

                                            foreach ($seatclass as $seatkey => $class) {

                                               if(!isset($class["valid"]) || $class["valid"]!=="yes"){
                                                        continue;
                                               }

                                               $commissiontypeinfo=json_decode($contract->commissiontype,TRUE);
                                               $commissionpercentageinfo=json_decode($contract->commissionpercentage,TRUE);
                                               $commissionamount=json_decode($contract->commissionamount,TRUE);
                                               $triptypeinfo=json_decode($contract->triptype,TRUE);
                                               $rufrom=json_decode($contract->rufrom,TRUE);
                                               $ruto=json_decode($contract->ruto,TRUE);
                                               $blackout=json_decode($contract->blackout,TRUE);
                                               $seasons=json_decode($contract->season,TRUE);

                                    @endphp
                                    <li class="">

                                        <div class="row">
                                            <div class="col-lg-4 col-xs-4">
                                                <h6>From:</h6>
                                            </div>
                                            <div class="col-lg-8 col-xs-8" style="overflow-wrap: break-word;">
                                                <?php echo $rufrom[$seatkey]["from"] ?>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-4 col-xs-4">
                                                <h6>To:</h6>
                                            </div>
                                            <div class="col-lg-8 col-xs-8" style="overflow-wrap: break-word;">
                                                <?php echo $ruto[$seatkey]["to"] ?>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-4 col-xs-4">
                                                <h6>Cabin Class:</h6>
                                            </div>
                                            <div class="col-lg-8 col-xs-8" style="overflow-wrap: break-word;">
                                                <?php echo $class["seatclass"] ?>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-lg-4 col-xs-4">
                                                <h6>Trip type:</h6>
                                            </div>
                                            <div class="col-lg-8 col-xs-8" style="overflow-wrap: break-word;">
                                                <?php echo $triptypeinfo[$seatkey]["triptype"] ?>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-4 col-xs-4">
                                                <h6>Commission:</h6>
                                            </div>
                                            <div class="col-lg-8 col-xs-8">
                                                <?php if (isset($commissionpercentageinfo[$seatkey]["percentage"])) {
                                                    echo $commissionpercentageinfo[$seatkey]["percentage"] . " %";
                                                } else {
                                                    echo "$ " . $commissionamount[$seatkey]["amount"];
                                                }
                                                ?>
                                            </div>
                                        </div>
                                        <?php

                                        if ($blackout["bodate1"] != "" || $blackout["bodate2"] != "" || $blackout["bodate3"] != "" || $blackout["bodate4"] != "" || $blackout["bodate5"] != ""){

                                        ?>
                                        <div class="row">
                                            <div class="col-lg-4 col-xs-4">
                                                <h6>Exception:</h6>
                                            </div>
                                            <div class="col-lg-8 col-xs-8" style="overflow-wrap: break-word;">
                                                <?php foreach ($blackout as $balckoutonkey => $blackitem) {
                                                    if ($blackitem != "") {
                                                        echo $blackitem . "<br>";
                                                    }
                                                } ?>
                                            </div>
                                        </div>
                                        <?php } ?>
                                        <?php
                                        if (strlen($contract->season) > 0) {
                                        $lastseasons = $seasons;
                                        if (!isset($seasons["sedate1"])) {
                                            $lastseasons = $seasons[$seatkey];
                                        }
                                        if ($lastseasons["sedate1"] != "" || $lastseasons["sedate2"] != "" || $lastseasons["sedate3"] != "" || $lastseasons["sedate4"] != "" || $lastseasons["sedate5"] != ""){


                                        ?>
                                        <div class="row">
                                            <div class="col-lg-4 col-xs-4">
                                                <h6>Date range:</h6>
                                            </div>
                                            <div class="col-lg-8 col-xs-8" style="overflow-wrap: break-word;">
                                                <?php foreach ($lastseasons as $sesonkey => $seasonitem) {
                                                    if ($seasonitem != "") {
                                                        echo $seasonitem . "<br>";
                                                    }
                                                } ?>

                                            </div>
                                        </div>
                                        <?php }
                                        }?>

                                    </li>
                                    @php
                                        }
                                    }
                                    @endphp

                                    <li class="">
                                        <div class="row">
                                            <div class="col-lg-12 col-xs-4" style="    overflow-wrap: break-word;">
                                                <?php $text = str_replace('\r\n', '<br>', $contract->remark);echo nl2br($text); ?>
                                            </div>

                                        </div>


                                    </li>

                                </ul>
                            </div>
                        </div>
                    </div>
                @endforeach
                @endisset

            </div>
            <!-- Main content ends -->


            <!-- Container-fluid ends -->
        </div>
    @endsection



    @section('extrascript')

        <!-- Date picker.js -->
            <script src="/assets/plugins/datepicker/js/moment-with-locales.min.js"></script>
            <script src="/bower_components/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js"></script>

            <!-- Bootstrap Datepicker js -->
            <script type="text/javascript"
                    src="/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
            <script src="/assets/plugins/bootstrap-datepicker/js/bootstrap-datetimepicker.min.js"></script>

            <!-- bootstrap range picker -->
            <script type="text/javascript"
                    src="/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>

            <script>
                $(document).ready(function () {
                    $('.input-group-date-custom').datepicker({
                        todayBtn: true,
                        format: 'yyyy-mm-dd',
                        clearBtn: true,
                        keyboardNavigation: false,
                        forceParse: false,
                        todayHighlight: true,

                        defaultViewDate: {
                            year: '2017',
                            month: '01',
                            day: '01'
                        }
                    });
                });

            </script>


@endsection
