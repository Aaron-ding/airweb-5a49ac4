@extends('layouts.mainframe')

@section('extracss')
    <!-- Date Picker css -->
    <link rel="stylesheet" href="/bower_components/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css"/>

    <!-- Bootstrap Date-Picker css -->
    <link rel="stylesheet" href="/assets/plugins/bootstrap-datepicker/css/bootstrap-datetimepicker.css"/>
    <link rel="stylesheet" type="text/css" href="/bower_components/bootstrap-daterangepicker/daterangepicker.css"/>

@endsection


@section('content')
    <div class="content-wrapper">
        <!-- Container-fluid starts -->
        <!-- Main content starts -->
        <div class="container-fluid">

            <div class="row" style="margin-top:150px;margin-left: 15px;margin-right: 15px;">
                <div class="col-md-5">
                    <h3>Create</h3>
                </div>
                <div class="col-md-7 page-action text-right">
                    <a href="{{ route('users.index') }}" class="btn btn-default btn-sm"> <i class="fa fa-arrow-left"></i> Back</a>
                </div>
            </div>

            <div class="row" style="margin-left: 15px;margin-right: 15px;">
                <div class="col-lg-12">
                    {!! Form::open(['route' => ['users.store'] ]) !!}
                        @include('user._form')
                        <!-- Submit Form Button -->
                        {!! Form::submit('Create', ['class' => 'btn btn-primary']) !!}
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection


@section('extrascript')

    <!-- Date picker.js -->
        <script src="/assets/plugins/datepicker/js/moment-with-locales.min.js"></script>
        <script src="/bower_components/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js"></script>

        <!-- Bootstrap Datepicker js -->
        <script type="text/javascript" src="/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
        <script src="/assets/plugins/bootstrap-datepicker/js/bootstrap-datetimepicker.min.js"></script>

        <!-- bootstrap range picker -->
        <script type="text/javascript" src="/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>

@endsection