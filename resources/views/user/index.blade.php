@extends('layouts.mainframe')

@section('extracss')
    <!-- Date Picker css -->
    <link rel="stylesheet" href="/bower_components/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css"/>

    <!-- Bootstrap Date-Picker css -->
    <link rel="stylesheet" href="/assets/plugins/bootstrap-datepicker/css/bootstrap-datetimepicker.css"/>
    <link rel="stylesheet" type="text/css" href="/bower_components/bootstrap-daterangepicker/daterangepicker.css"/>
    <style>
        #pagediv ul li {
            float:left;
            margin-left:5px;

        }
    </style>
@endsection

@section('content')
    <div class="content-wrapper">
        <!-- Container-fluid starts -->
        <!-- Main content starts -->
        <div class="container-fluid">
            <div class="row" style="margin-top:150px;margin-left: 15px;margin-right: 15px;">
                <div class="col-md-3">
                    <h3 class="modal-title">{{ $result->total() }} {{ str_plural('User', $result->count()) }} </h3>
                </div>
                <div class="col-md-6">
                    <form action="/users">
                        <input type="text" name="searchmail" value="{{old("searchmail")}}" maxlength="120" style="vertical-align: middle;height:35px;width:300px;"><button class="btn btn-primary btn-sm" style="margin-left:10px;">Search</button>
                    </form>

                </div>
                <div class="col-md-3 page-action text-right">
                    @can('add_users')
                        <a href="{{ route('users.create') }}" class="btn btn-primary btn-sm"> <i class="glyphicon glyphicon-plus-sign"></i> Create</a>
                    @endcan
                </div>
            </div>

            <div class="result-set" style="margin-left: 15px;margin-right: 15px;">
                <table class="table table-bordered table-striped table-hover"  id="data-table" style="background-color: navajowhite;">
                    <thead>
                    <tr>
                        <th>Id</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Role</th>
                        <th>Created At</th>
                        @can('edit_users', 'delete_users')
                        <th class="text-center">Actions</th>
                        @endcan
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($result as $item)
                        <tr>
                            <td>{{ $item->id }}</td>
                            <td>{{ $item->firstname . " " . $item->lastname }}</td>
                            <td>{{ $item->email }}</td>
                            <td>{{ $item->roles->implode('name', ', ') }}</td>
                            <td>{{ $item->created_at->toFormattedDateString() }}</td>

                            @can('edit_users')
                            <td class="text-center">
                                @include('shared._actions', [
                                    'entity' => 'users',
                                    'id' => $item->id
                                ])
                            </td>
                            @endcan
                        </tr>
                    @endforeach
                    </tbody>
                </table>

                <div id="pagediv" class="text-center">
                    {{ $result->links() }}
                </div>














            </div>
        </div>
    </div>

@endsection


@section('extrascript')

    <!-- Date picker.js -->
    <script src="/assets/plugins/datepicker/js/moment-with-locales.min.js"></script>
    <script src="/bower_components/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js"></script>

    <!-- Bootstrap Datepicker js -->
    <script type="text/javascript" src="/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
    <script src="/assets/plugins/bootstrap-datepicker/js/bootstrap-datetimepicker.min.js"></script>

    <!-- bootstrap range picker -->
    <script type="text/javascript" src="/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>

@endsection
