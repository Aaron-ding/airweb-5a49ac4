
@extends('layouts.mainframe')

@section('extracss')
    <!-- Date Picker css -->
    <link rel="stylesheet" href="/bower_components/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css"/>

    <!-- Bootstrap Date-Picker css -->
    <link rel="stylesheet" href="/assets/plugins/bootstrap-datepicker/css/bootstrap-datetimepicker.css"/>
    <link rel="stylesheet" type="text/css" href="/bower_components/bootstrap-daterangepicker/daterangepicker.css"/>

@endsection

@section('content')

    <div class="content-wrapper">
        <!-- Container-fluid starts -->
        <!-- Main content starts -->
        <div class="container-fluid">
            <div class="row">
                <div class="main-header">
                    <ol class="breadcrumb breadcrumb-title breadcrumb-arrow">
                        <li class="breadcrumb-item"><a href="/home"><i class="icofont icofont-home"></i></a>
                        </li>
                        <li class="breadcrumb-item"><a href="#!">My Orders</a>
                        </li>
                        @isset($showquery)
                            <li class="breadcrumb-item"><a href="#!">History Orders</a>
                            </li>
                        @else
                            <li class="breadcrumb-item"><a href="#!">Recent Orders</a>
                            </li>
                        @endisset

                    </ol>
                </div>
            </div>
            @isset($showquery)
                <nav class="navbar navbar-light bg-faded m-b-30 p-10">
                        <h4>QUERY ORDERS </h4>
                </nav>

            @else
                <nav class="navbar navbar-light bg-faded m-b-30 p-10">
                    <h4>RECENT ORDERS /  <a href="{{ route('flighthistoryorder') }}">查询历史订单<i class="icofont icofont-search" style="padding-left: 10px;"></i></a></h4>
                </nav>
            @endisset

            @isset($showquery)
                <div class="row" style="margin-bottom: 20px;">
                    <div class="col-lg-4 col-xs-12">
                        <label for="reportrange" style="color:#00b9f5;">Specify the query create order date</label>
                        <div id="reportrange" class="f-right" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc; width: 100%; height: 50px; font-size: 22px;">
                            <i class="glyphicon glyphicon-calendar icofont icofont-ui-calendar"></i>
                            <span></span> <b class="caret"></b>
                        </div>
                    </div>
                </div>

                <form id="searchorder" class="md-float-material" method="GET" action="{{ route('flighthistoryorder') }}">
                    {{ csrf_field() }}
                    <input id="startdate" type="hidden" name="startdate" value="{{ old('startdate') }}" >
                    <input id="enddate" type="hidden" name="enddate" value="{{ old('enddate') }}" >
                </form>
            @endisset

            <div class="row">
                @isset($orders)
                    @foreach ($orders as $order)
                        @include('layouts.parts.ordersumary')
                    @endforeach
                @endisset

        </div>
        <!-- Main content ends -->



        <!-- Container-fluid ends -->
    </div>
@endsection



@section('extrascript')

    <!-- Date picker.js -->
        <script src="/assets/plugins/datepicker/js/moment-with-locales.min.js"></script>
        <script src="/bower_components/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js"></script>

        <!-- Bootstrap Datepicker js -->
        <script type="text/javascript" src="/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
        <script src="/assets/plugins/bootstrap-datepicker/js/bootstrap-datetimepicker.min.js"></script>

        <!-- bootstrap range picker -->
        <script type="text/javascript" src="/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
        @isset($showquery)
            <script>
                var start = moment().subtract(29, 'days');
                var end = moment();


                @if (null!=old('enddate'))
                    start=moment(new Date('{{old('startdate')}} 12:00:00'));
                    end=moment(new Date('{{old('enddate')}} 12:00:00'));
                @endif

                var cb=function(startin, endin,submit,showdate) {

                    $('#startdate').val(startin.format('YYYY-MM-DD') );
                    $('#enddate').val(endin.format('YYYY-MM-DD') );
                    if(submit!=="no submit"){
                        //default ,do fomr submit
                        $("#searchorder").submit();
                    }
                    if (showdate==="show date"){
                        $('#reportrange span').html(startin.format('MMMM D, YYYY') + ' - ' + endin.format('MMMM D, YYYY'));
                    }



                }
                $( document ).ready(function() {
                    $('#reportrange').daterangepicker({
                        startDate: start,
                        endDate: end,
                        ranges: {
                            'Today': [moment(), moment()],
                            'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                            'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                            'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                            'This Month': [moment().startOf('month'), moment().endOf('month')],
                            'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                        }
                    },cb);
                });
                @isset($orders)
                    cb(start, end,"no submit","show date");
                @else
                    cb(start, end,"no submit","no show date");
                @endisset
            </script>
        @endisset
        <script>
            function dealorder(action,pnr,orderid,provider){

                window.location = "{{route("orderdetail")}}?pnr="+pnr+"&orderid="+orderid+"&provider="+provider+"&";
            }
        </script>
@endsection
