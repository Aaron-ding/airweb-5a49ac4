
@extends('layouts.mainframe')

@section('extracss')
    <!-- Date Picker css -->
    <link rel="stylesheet" href="/bower_components/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css"/>

    <!-- Bootstrap Date-Picker css -->
    <link rel="stylesheet" href="/assets/plugins/bootstrap-datepicker/css/bootstrap-datetimepicker.css"/>
    <link rel="stylesheet" type="text/css" href="/bower_components/bootstrap-daterangepicker/daterangepicker.css"/>


    <link rel="stylesheet" href="/bower_components/skeuocard/styles/skeuocard.reset.css" />
    <link rel="stylesheet" href="/bower_components/skeuocard/styles/skeuocard.css" />
@endsection

@section('content')

    <div class="content-wrapper">
        <!-- Container-fluid starts -->
        <!-- Main content starts -->
        <div class="container-fluid">
            <div class="row">
                <div class="main-header">
                    <ol class="breadcrumb breadcrumb-title breadcrumb-arrow">
                        <li class="breadcrumb-item"><a href="/home"><i class="icofont icofont-home"></i></a>
                        </li>
                        <li class="breadcrumb-item"><a href="#!">My Orders</a>
                        </li>
                        @isset($showquery)
                            <li class="breadcrumb-item"><a href="#!">History Orders</a>
                            </li>
                        @else
                            <li class="breadcrumb-item"><a href="#!">Recent Orders</a>
                            </li>
                        @endisset
                        <li class="breadcrumb-item"><a href="#!">Order detail</a>
                        </li>

                    </ol>
                </div>
            </div>

            <nav class="navbar navbar-light bg-faded m-b-30 p-10">
                    <h4>Order summary</h4>
            </nav>


            <div class="row" id="ordersummary">
                @isset($order)
                    @include('layouts.parts.ordersumary')
                @endisset
                @include('layouts.parts.contact')
            </div>

            <nav class="navbar navbar-light bg-faded m-b-30 p-10">
                    <h4>Passengers</h4>
            </nav>
            <div class="row" id="passenger">

                @isset($passengers)
                    @for ($i=1;$i<=count($passengers);$i++)
                        @php $passenger=$passengers[$i-1]; @endphp
                        @include('layouts.parts.passengerdetail')
                    @endfor
                @endisset

            </div>
            <nav class="navbar navbar-light bg-faded m-b-30 p-10">
                    <h4>Internay</h4>
            </nav>
            <div class="row" id="internary">

                @isset($internaries)

                    @php $internarie_list=json_decode($internaries,TRUE); @endphp

                    @for ($i=1;$i<=count($internarie_list);$i++)
                        @php $internary=$internarie_list[$i-1]; @endphp
                        @include('layouts.parts.internary')
                    @endfor
                @endisset

            </div>
            <nav class="navbar navbar-light bg-faded m-b-30 p-10">
                    <h4>Payments</h4>
            </nav>
            <div class="row" id="passenger">

                @isset($payments)
                    @for ($i=1;$i<=count($payments);$i++)
                        @php $payment=$payments[$i-1]; @endphp
                        @include('layouts.parts.payment')
                    @endfor
                @endisset
                @include('layouts.parts.paymentform')
            </div>

        </div>
        <!-- Main content ends -->



        <!-- Container-fluid ends -->
    </div>
@endsection



@section('extrascript')

    <!-- Date picker.js -->
        <script src="/assets/plugins/datepicker/js/moment-with-locales.min.js"></script>
        <script src="/bower_components/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js"></script>

        <!-- Bootstrap Datepicker js -->
        <script type="text/javascript" src="/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
        <script src="/assets/plugins/bootstrap-datepicker/js/bootstrap-datetimepicker.min.js"></script>

        <!-- bootstrap range picker -->
        <script type="text/javascript" src="/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>



        <script>

            jQuery( document ).ready( function( $ ) {
                $.ajaxSetup(
                {
                    headers:
                        {
                            'X-CSRF-Token': $('input[name="_token"]').val()
                        }
                });
                $.post(
                    '{{route("passengercard")}}',
                    {
                        "orderid": '{{old("orderid")}}',
                        "pnr": '{{old("pnr")}}',
                        "provider": '{{old("provider")}}',
                        "_token":'{{ csrf_token() }}'
                    },
                    function( data ) {
                        var htmlarray=JSON.parse(data);
                        $( "#passenger").html(htmlarray["passenger"]);
                        $( "#internary").html(htmlarray["internary"]);
                    },
                    'html'
                );


                //prevent the form from actually submitting in browser
                return false;
            } );


            function dealorder(action,pnr,orderid,provider){

                window.location = "{{route("orderdetail")}}?pnr="+pnr+"&orderid="+orderid+"&provider="+provider+"&";
            }
        </script>
@endsection
