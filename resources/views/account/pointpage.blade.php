<style>
    #stopaircode{
        position:relative;
        width:112px;
        height:8px;
        margin:0 10px;
        display: inline-block;
        }
    #stopaircode em{
        position:absolute;
        left:0;
        width:100%;
        text-align:center;}
    #stopaircode em#up{top:-4px;font-size: 14px;font-weight: 300;}
    #stopaircode em#down{bottom:-18px;}
</style>
@extends('layouts.mainframe')

@section('extracss')
    <!-- Date Picker css -->
    <link rel="stylesheet" href="/bower_components/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css"/>

    <!-- Bootstrap Date-Picker css -->
    <link rel="stylesheet" href="/assets/plugins/bootstrap-datepicker/css/bootstrap-datetimepicker.css"/>
    <link rel="stylesheet" type="text/css" href="/bower_components/bootstrap-daterangepicker/daterangepicker.css"/>



@endsection

@section('content')

    @php
        echo "@@";
    @endphp

    <div class="content-wrapper">
        <!-- Container-fluid starts -->
        <!-- Main content starts -->
        <nav class="container-fluid">
            <div class="row">
                <div class="main-header">
                    <ol class="breadcrumb breadcrumb-title breadcrumb-arrow">
                        <li class="breadcrumb-item"><a href="/home"><i class="icofont icofont-home"></i></a>
                        </li>
                        <li class="breadcrumb-item"><a href="#">Sinorama points account</a>
                        </li>

                    </ol>
                </div>
            </div>



            <nav class="navbar navbar-light bg-faded m-b-30 p-10">
                <h4>Sinorama points account balance</h4>
            </nav>
            <div class="row m-b-30 dashboard-header">

            @foreach ($journals as $journal)

                    <div class="col-lg-3 col-sm-6">
                        <div class="dashboard-success bg-success">
                            <div class="sales-success" style="margin-bottom: 60px;">
                                <div class="f-right">
                                    <span style="font-size:16px;">{{$journal->currency}}</span>
                                </div>
                                <div class="f-left">
                                    <h2 class="counter">{{$journal->balance->getAmount()}}</h2>
                                    <span style="font-size:22px;">Points</span>
                                </div>
                            </div>
                            <div class="bg-dark-success">
                                <p class="week-sales">Last update at</p>
                                <p class="total-sales">{{$journal->updated_at}}</p>
                            </div>
                        </div>
                    </div>

            @endforeach
            </div>



            <nav class="navbar navbar-light bg-faded m-b-30 p-10">
                <h4>Sinorama points transactions query</h4>
            </nav>
            <form id="searchorder" class="md-float-material" method="GET" action="{{ route('pointpage') }}">
                <div class="row" style="margin-bottom: 20px;">
                    <div class="col-lg-3 col-xs-12">
                        <label for="reportrange" style="color:#00b9f5;">Specify the query create order date</label>
                        <div id="reportrange" class="f-right" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc; width: 100%; height: 40px; font-size: 18px;">
                            <i class="glyphicon glyphicon-calendar icofont icofont-ui-calendar"></i>
                            <span></span> <b class="caret"></b>
                        </div>
                    </div>
                    <div class="col-lg-3 col-xs-12">
                        <label for="querycurrency" style="color:#00b9f5;">Choose one of your accounts</label>
                        <div id="querycurrency" class="md-input-wrapper" style="margin-top:12px;">
                            <select class="md-form-control" name="currency" onchange="return changequery();">
                                @foreach ($journals as $journal)
                                    <option value="{{$journal->currency}}" @php if($journal->currency==old('currency')) echo "selected"; @endphp>{{$journal->currency}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="col-lg-3 col-xs-12">
                        <label for="querycurrency" style="color:#00b9f5;">Specify transactions type</label>
                        <div id="querycurrency" class="md-input-wrapper" style="margin-top:12px;">
                            <select class="md-form-control" name="transtype" onchange="return changequery();">
                                <option value="">ALL</option>
                                <option value="credit" @php if("credit"==old('transtype')) echo "selected"; @endphp>CREDIT</option>
                                <option value="debit" @php if("debit"==old('transtype')) echo "selected"; @endphp>DEBIT</option>
                            </select>
                        </div>
                    </div>



                </div>



                {{ csrf_field() }}
                <input id="startdate" type="hidden" name="startdate" value="{{ old('startdate') }}" >
                <input id="enddate" type="hidden" name="enddate" value="{{ old('enddate') }}" >
            </form>
            @isset($trans)
                <nav class="navbar navbar-light bg-faded m-b-30 p-10">
                    <div class="row" >
                        <div class="col-md-4 col-sm-12">
                            <h4>Total reccords:{{count($trans)}}</h4>
                        </div>
                        <div class="col-md-4 col-sm-12">
                            <h4>Total CREDIT:<span  style="color:#2196F3">{{$credits["amt"]}} points , {{$credits["count"]}} Rec(s) </span></h4>
                        </div>
                        <div class="col-md-4 col-sm-12">
                            <h4>Total DEBIT:<span  style="color:#4CAF50">{{$debits["amt"]}} points , {{$debits["count"]}} Rec(s)</span> </h4>
                        </div>
                    </div>
                </nav>
                <div class="row">
                    @foreach($trans as $tran)
                        <div class="col-xl-2 col-lg-4 col-md-4 col-sm-6" style="margin-bottom: 20px;">
                            <div class="panel @php
                                        if(isset($tran->credit)) echo "panel-primary";
                                        if(isset($tran->debit)) echo "panel-success";
                                    @endphp ">
                                <div class="panel-heading @php
                                    if(isset($tran->credit)) echo "bg-primary";
                                    if(isset($tran->debit)) echo "bg-success";
                                @endphp">
                                    {{$tran->created_at}}
                                </div>
                                <div class="panel-body">
                                    <div class="row">
                                        <div align="right" class="col-xs-3" style="padding-right: 0px;">
                                            @php
                                                if(isset($tran->credit)) echo "+";
                                                if(isset($tran->debit)) echo "-";
                                            @endphp
                                        </div>
                                        <div class="col-xs-9">
                                            @php
                                                if(isset($tran->credit)) echo $tran->credit;
                                                if(isset($tran->debit)) echo $tran->debit;
                                            @endphp

                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12">
                                            {{$tran->ref_class_id . " ". $tran->ref_class}}
                                        </div>

                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12">
                                            {{$tran->memo}}
                                        </div>

                                    </div>
                                </div>

                            </div>
                        </div>
                    @endforeach

                </div>
            @endisset


        </div>
        <!-- Main content ends -->



        <!-- Container-fluid ends -->
    </div>
@endsection



@section('extrascript')





    <!-- Date picker.js -->
    <script src="/assets/plugins/datepicker/js/moment-with-locales.min.js"></script>
    <script src="/bower_components/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js"></script>

    <!-- Bootstrap Datepicker js -->
    <script type="text/javascript" src="/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
    <script src="/assets/plugins/bootstrap-datepicker/js/bootstrap-datetimepicker.min.js"></script>

    <!-- bootstrap range picker -->
    <script type="text/javascript" src="/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>

    <script>

        function changequery(){
            if($('#reportrange span').html()==""){
                notify('Please specify the date range','top','center','fa fa-comments','danger','','');
                return;
            }else{
                cb(start, end,"submit","show date");
            }
        }

        var start = moment().subtract(29, 'days');
        var end = moment();


        @if (null!=old('enddate'))
            start=moment(new Date('{{old('startdate')}} 12:00:00'));
        end=moment(new Date('{{old('enddate')}} 12:00:00'));
                @endif

        var cb=function(startin, endin,submit,showdate) {

                $('#startdate').val(startin.format('YYYY-MM-DD') );
                $('#enddate').val(endin.format('YYYY-MM-DD') );
                if(submit!=="no submit"){
                    //default ,do fomr submit
                    $("#searchorder").submit();
                }
                if (showdate==="show date"){
                    $('#reportrange span').html(startin.format('MMMM D, YYYY') + ' - ' + endin.format('MMMM D, YYYY'));
                }



            }
        $( document ).ready(function() {
            $('#reportrange').daterangepicker({
                startDate: start,
                endDate: end,
                ranges: {
                    'Today': [moment(), moment()],
                    'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                    'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                    'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                    'This Month': [moment().startOf('month'), moment().endOf('month')],
                    'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                }
            },cb);
        });
        @isset($trans)
            cb(start, end,"no submit","show date");
        @else
            cb(start, end,"no submit","no show date");
        @endisset
    </script>
@endsection
