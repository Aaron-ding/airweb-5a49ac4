
@extends('layouts.mainframe')

@section('extracss')
    <!-- Date Picker css -->
    <link rel="stylesheet" href="/bower_components/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css"/>

    <!-- Bootstrap Date-Picker css -->
    <link rel="stylesheet" href="/assets/plugins/bootstrap-datepicker/css/bootstrap-datetimepicker.css"/>
    <link rel="stylesheet" type="text/css" href="/bower_components/bootstrap-daterangepicker/daterangepicker.css"/>



@endsection

@section('content')
    @php($user=Auth::user())
    <div class="content-wrapper">
        <!-- Container-fluid starts -->
        <div class="container-fluid">

            <div class="row">
                <div class="main-header">
                    <h4>Profile</h4>
                    <ol class="breadcrumb breadcrumb-title breadcrumb-arrow">
                        <li class="breadcrumb-item"><a href="index.html"><i class="icofont icofont-home"></i></a>
                        </li>
                        <li class="breadcrumb-item"><a href="#!">Extras</a>
                        </li>
                        <li class="breadcrumb-item"><a href="profile.html">Profile</a>
                        </li>
                    </ol>
                </div>
            </div>
            <!-- Header end -->
            <div class="row">
                <div class="col-xl-3 col-lg-4">
                    <div class="card faq-left">
                        <div class="social-profile">
                            @php
                                $headimg="undefined.png";
                                if(isset($user->sex) && $user->sex!==""){
                                    $headimg=$user->sex. ".png";
                                }
                            @endphp
                            <img class="img-fluid" src="/assets/images/social/{{$headimg}}" alt="">
                            <div class="profile-hvr m-t-15">
                                <i class="icofont icofont-ui-edit p-r-10 c-pointer"></i>
                                <i class="icofont icofont-ui-delete c-pointer"></i>
                            </div>
                        </div>
                        <div class="card-block">
                            <h4 class="f-18 f-normal m-b-10 txt-primary">{{ucfirst($user->firstname)}} {{ucfirst($user->lastname)}}</h4>

                            @if(null !==$user->roles->pluck('name') && count($user->roles->pluck('name'))>0)
                            <h5 class="f-14">{{$user->roles->pluck('name')}}</h5>
                            @endif
                            <ul>
                                <li class="faq-contact-card">
                                    <i class="icofont icofont-telephone"></i>
                                    {{$user->phone1}}
                                </li>

                                <li class="faq-contact-card">
                                    <i class="icofont icofont-email"></i>
                                    <a href="mailto:{{$user->email}}">{{$user->email}}</a>
                                </li>
                            </ul>


                        </div>
                    </div>
                    <!-- end of card-block -->

                    <!-- end of card -->
                </div>
                <!-- end of col-lg-3 -->
<form id="info" action="{{route("profile")}}" >
                <!-- start col-lg-9 -->
                <div class="col-xl-9 col-lg-8">
                    @isset($becomeingagent)
                        <div class="tab-content">
                            <div class="tab-pane active">
                                <div class="card">
                                    <div class="panel-heading bg-warning">
                                        <h5>To become a Sinorama agent, </h5>
                                        <p style="color:white">
                                            1、Please fill below information.
                                        </p>
                                        <p style="color:white">
                                            2、Click save
                                        </p>
                                        <p style="color:white">
                                            3、After 1 work day, we will give you feedback.
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endisset

                    <!-- Nav tabs -->
                    <div class="tab-header">
                        <ul class="nav nav-tabs md-tabs tab-timeline" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" data-toggle="tab" href="#personal" role="tab">Personal Info</a>
                                <div class="slide"></div>
                            </li>
                            @isset($showagent)
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="#agent" role="tab">Agent Info</a>
                                    <div class="slide"></div>
                                </li>
                            @endisset
                        </ul>
                    </div>
                    <!-- end of tab-header -->

                    <div class="tab-content">
                        <div class="tab-pane active" id="personal" role="tabpanel">
                            <div class="card">
                                <div class="card-header"><h5 class="card-header-text">About Me</h5>
                                    @isset($becomeingagent)
                                    @else
                                        <button id="edit-btn" type="button" class="btn btn-primary waves-effect waves-light f-right" >
                                            <i  class="icofont icofont-edit"></i>
                                        </button>
                                    @endisset
                                </div>
                                <div class="card-block">
                                    <div class="view-info">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div class="general-info">
                                                    <div class="row">
                                                        <div class="col-lg-12 col-xl-6">
                                                            <table class="table m-0">
                                                                <tbody>
                                                                <tr>
                                                                    <th scope="row">First Name</th>
                                                                    <td>{{$user->firstname}}</td>
                                                                </tr>
                                                                <tr>
                                                                    <th scope="row">Last Name</th>
                                                                    <td>{{$user->lastname}}</td>
                                                                </tr>
                                                                <tr>
                                                                    <th scope="row">Gender</th>
                                                                    <td>{{ucfirst($user->sex)}}</td>
                                                                </tr>
                                                                <tr>
                                                                    <th scope="row">Phone Number</th>
                                                                    <td>{{$user->phone1}}</td>
                                                                </tr>
                                                                <tr>
                                                                    <th scope="row">Prefer Language</th>
                                                                    <td>{{$user->lang1}}</td>
                                                                </tr>

                                                                </tbody>
                                                            </table>
                                                        </div>
                                                        <!-- end of table col-lg-6 -->

                                                        <div class="col-lg-12 col-xl-6">
                                                            <table class="table">
                                                                <tbody>

                                                                <tr>
                                                                    <th scope="row">Address</th>
                                                                    <td>{{$user->address1}}</td>
                                                                </tr>
                                                                <tr>
                                                                    <th scope="row">Province</th>
                                                                    <td>{{$user->province}}</td>
                                                                </tr>
                                                                <tr>
                                                                    <th scope="row">City</th>
                                                                    <td>{{$user->city}}</td>
                                                                </tr>
                                                                <tr>
                                                                    <th scope="row">Country</th>
                                                                    <td>{{$user->country}}</td>
                                                                </tr>
                                                                <tr>
                                                                    <th scope="row">Post Code</th>
                                                                    <td>{{$user->postcode}}</td>
                                                                </tr>

                                                                </tbody>
                                                            </table>
                                                        </div>
                                                        <!-- end of table col-lg-6 -->
                                                    </div>
                                                    <!-- end of row -->
                                                </div>
                                                <!-- end of general info -->
                                            </div>
                                            <!-- end of col-lg-12 -->
                                        </div>
                                        <!-- end of row -->
                                    </div>
                                    <!-- end of view-info -->

                                    <div class="edit-info">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div class="general-info">
                                                    <div class="row">
                                                        <div class="col-lg-6">
                                                            <table class="table">
                                                                <tbody>
                                                                <tr>
                                                                    <td>
                                                                        <div class="md-group-add-on">
                                                                         <span class="md-add-on">
                                                                             <i class="icofont icofont-ui-user"></i>
                                                                         </span>
                                                                            <div class="md-input-wrapper">
                                                                                <input name="firstname" type="text" class="md-form-control
                                                                                    @php
                                                                                        if(isset($user->firstname)) echo "md-valid";
                                                                                    @endphp
                                                                                " value="{{$user->firstname}}" maxlength="250"
                                                                               @isset($showagent)
                                                                                        required
                                                                                @endisset
                                                                                >
                                                                                <label>First name @isset($showagent) (<font color="red">*</font>) @endisset </label>
                                                                            </div>
                                                                        </div>
                                                                    </td>

                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <div class="md-group-add-on">
                                                                         <span class="md-add-on">
                                                                             <i class="icofont icofont-ui-user"></i>
                                                                         </span>
                                                                            <div class="md-input-wrapper">
                                                                                <input name="lastname" type="text" class="md-form-control
                                                                                    @php
                                                                                        if(isset($user->lastname)) echo "md-valid";
                                                                                    @endphp
                                                                                " value="{{$user->lastname}}" maxlength="250" @isset($showagent) required @endisset >
                                                                                <label>Last name @isset ($showagent) (<font color="red">*</font>) @endisset </label>
                                                                            </div>
                                                                        </div>
                                                                    </td>

                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <div class="form-radio">
                                                                            <form>
                                                                                <div class="md-group-add-on">
                                                                                  <span class="md-add-on">
                                                                                     <i class="icofont icofont-group-students"></i>
                                                                                  </span>
                                                                                    <div class="radio radiofill radio-inline">
                                                                                        <label>
                                                                                            <input type="radio" name="sex" value="male"
                                                                                            @php
                                                                                            if($user->sex=="male") echo "checked";
                                                                                            @endphp
                                                                                            ><i class="helper"></i> Male
                                                                                        </label>
                                                                                    </div>
                                                                                    <div class="radio radiofill radio-inline">
                                                                                        <label>
                                                                                            <input type="radio" name="sex" value="female"
                                                                                            @php
                                                                                                if($user->sex=="female") echo "checked";
                                                                                            @endphp
                                                                                            ><i class="helper"></i> Female
                                                                                        </label>
                                                                                    </div>
                                                                                </div>
                                                                            </form>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <div class="md-group-add-on">
                                                                         <span class="md-add-on">
                                                                             <i class="icofont icofont-mobile-phone"></i>
                                                                         </span>
                                                                            <div class="md-input-wrapper">
                                                                                <input name="phone" type="text" class="md-form-control
                                                                                @php
                                                                                    if(isset($user->phone1)) echo "md-valid";
                                                                                @endphp
                                                                                " value="{{$user->phone1}}" maxlength="250" @isset($showagent) required @endisset >
                                                                                <label>Phone @isset ($showagent) (<font color="red">*</font>) @endisset </label>
                                                                            </div>
                                                                        </div>
                                                                    </td>

                                                                </tr>

                                                                <tr>
                                                                    <td>
                                                                        <div class="md-group-add-on">
                                                                         <span class="md-add-on">
                                                                             <i class="icofont icofont-users-alt-4"></i>
                                                                         </span>
                                                                            <div class="md-input-wrapper">
                                                                                <select name="preferlang" class="md-form-control">
                                                                                    <option value="">Select Prefer Language</option>
                                                                                    <option value="english"
                                                                                            @php
                                                                                            if($user->lang1=="english") echo "selected";
                                                                                            @endphp
                                                                                    >English</option>
                                                                                    <option value="french"
                                                                                            @php
                                                                                                if($user->lang1=="french") echo "selected";
                                                                                            @endphp
                                                                                    >French</option>
                                                                                    <option value="chinese"
                                                                                            @php
                                                                                                if($user->lang1=="chinese") echo "selected";
                                                                                            @endphp
                                                                                    >Chinese</option>
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                    </td>
                                                                </tr>

                                                                </tbody>
                                                            </table>
                                                        </div>
                                                        <!-- end of table col-lg-6 -->

                                                        <div class="col-lg-6">
                                                            <table class="table">
                                                                <tbody>
                                                                <tr>
                                                                    <td>
                                                                        <div class="md-group-add-on">
                                                                         <span class="md-add-on">
                                                                             <i class="icofont icofont-email"></i>
                                                                         </span>
                                                                            <div class="md-input-wrapper">
                                                                                <input type="text" name="address" class="md-form-control
                                                                                @php
                                                                                    if(isset($user->address1)) echo "md-valid";
                                                                                @endphp
                                                                                " value="{{$user->address1}}" maxlength="250" @isset($showagent) required @endisset >
                                                                                <label>Address @isset ($showagent) (<font color="red">*</font>) @endisset </label>
                                                                            </div>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <div class="md-group-add-on">
                                                                         <span class="md-add-on">
                                                                             <i class="icofont icofont-web"></i>
                                                                         </span>
                                                                            <div class="md-input-wrapper">
                                                                                <input type="text" name="city" class="md-form-control
                                                                                @php
                                                                                    if(isset($user->city)) echo "md-valid";
                                                                                @endphp
                                                                                " value="{{$user->city}}" maxlength="250" @isset($showagent) required @endisset >
                                                                                <label>City @isset ($showagent) (<font color="red">*</font>) @endisset </label>
                                                                            </div>
                                                                        </div>
                                                                    </td>

                                                                </tr>

                                                                <tr>
                                                                    <td>
                                                                        <div class="md-group-add-on">
                                                                         <span class="md-add-on">
                                                                             <i class="icofont icofont-web"></i>
                                                                         </span>
                                                                            <div class="md-input-wrapper">
                                                                                <input type="text" name="province" class="md-form-control
                                                                                @php
                                                                                    if(isset($user->province)) echo "md-valid";
                                                                                @endphp" value="{{$user->province}}" maxlength="250" @isset($showagent) required @endisset >
                                                                                <label>Province @isset ($showagent) (<font color="red">*</font>) @endisset </label>
                                                                            </div>
                                                                        </div>
                                                                    </td>
                                                                </tr>

                                                                <tr>
                                                                    <td>
                                                                        <div class="md-group-add-on">
                                                                         <span class="md-add-on">
                                                                             <i class="icofont icofont-web"></i>
                                                                         </span>
                                                                            <div class="md-input-wrapper">
                                                                                <input type="text" name="country" class="md-form-control
                                                                                @php
                                                                                    if(isset($user->country)) echo "md-valid";
                                                                                @endphp" value="{{$user->country}}" maxlength="250" @isset($showagent) required @endisset >
                                                                                <label>Country @isset ($showagent) (<font color="red">*</font>) @endisset </label>
                                                                            </div>
                                                                        </div>
                                                                    </td>
                                                                </tr>

                                                                <tr>
                                                                    <td>
                                                                        <div class="md-group-add-on">
                                                                         <span class="md-add-on">
                                                                             <i class="icofont icofont-web"></i>
                                                                         </span>
                                                                            <div class="md-input-wrapper">
                                                                                <input type="text" name="postcode" class="md-form-control
                                                                                @php
                                                                                    if(isset($user->postcode)) echo "md-valid";
                                                                                @endphp" value="{{$user->postcode}}" maxlength="250" @isset($showagent) required @endisset >
                                                                                <label>Post Code @isset ($showagent) (<font color="red">*</font>) @endisset </label>
                                                                            </div>
                                                                        </div>
                                                                    </td>
                                                                </tr>

                                                                </tbody>
                                                            </table>

                                                        </div>

                                                        <!-- end of table col-lg-6 -->
                                                    </div>
                                                    <!-- end of row -->
                                                    <div class="text-center">
                                                        <button type="button" id="saveinfo" class="btn btn-primary waves-effect waves-light m-r-20">Save</button>
                                                        <a href="#!" id="edit-cancel" class="btn btn-default waves-effect">Cancel</a>
                                                    </div>
                                                </div>
                                                <!-- end of edit info -->
                                            </div>
                                            <!-- end of col-lg-12 -->
                                        </div>
                                        <!-- end of row -->

                                    </div>
                                    <!-- end of view-info -->
                                </div>
                                <!-- end of card-block -->
                            </div>
                            <!-- end of card-->



                            <!-- end of row of education and experience  -->


                        </div>
                        <!-- end of tab-pane -->
                        @isset($showagent)
                            <div class="tab-pane" id="agent" role="tabpanel">
                                <div class="card">
                                    <div class="card-header"><h5 class="card-header-text">Information of Agent</h5>
                                        @isset($becomeingagent)
                                        @else
                                            <button id="edit-btn" type="button" class="btn btn-primary waves-effect waves-light f-right" >
                                                <i  class="icofont icofont-edit"></i>
                                            </button>
                                         @endisset
                                    </div>
                                    <div class="card-block">
                                        <div class="view-info">
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <div class="general-info">
                                                        <div class="row">
                                                            <div class="col-lg-12 col-xl-6">
                                                                <table class="table m-0">
                                                                    <tbody>
                                                                    <tr>
                                                                        <th scope="row">Agent's License(QPC,TICO,...)</th>
                                                                        <td>{{$agent->license}}</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <th scope="row">Work company</th>
                                                                        <td>{{$agent->company}}</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <th scope="row">Company's phone</th>
                                                                        <td>{{$agent->companyphone}}</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <th scope="row">Company's address</th>
                                                                        <td>{{$agent->companyaddress}}</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <th scope="row">Company's province</th>
                                                                        <td>{{$agent->companyprovince}}</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <th scope="row">Company's country</th>
                                                                        <td>{{$agent->companycountry}}</td>
                                                                    </tr>
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                            <!-- end of table col-lg-6 -->

                                                            <div class="col-lg-12 col-xl-6">
                                                                <table class="table">
                                                                    <tbody>
                                                                    <tr>
                                                                        <th scope="row">Apply at</th>
                                                                        <td>{{$agent->created_at}}</td>
                                                                    </tr>

                                                                    <tr>
                                                                        <th scope="row">Status</th>
                                                                        <td>{{$agent->status}}</td>
                                                                    </tr>


                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                            <!-- end of table col-lg-6 -->
                                                        </div>
                                                        <!-- end of row -->
                                                    </div>
                                                    <!-- end of general info -->
                                                </div>
                                                <!-- end of col-lg-12 -->
                                            </div>
                                            <!-- end of row -->
                                        </div>

                                        <div class="edit-info">
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <div class="general-info">
                                                        <div class="row">
                                                            <div class="col-lg-6">
                                                                <table class="table">
                                                                    <tbody>
                                                                    <tr>
                                                                        <td>
                                                                            <div class="md-group-add-on">
                                                                             <span class="md-add-on">
                                                                                 <i class="icofont icofont-ui-user"></i>
                                                                             </span>
                                                                                <div class="md-input-wrapper">
                                                                                    <input type="text" name="license" class="md-form-control
                                                                                    @php
                                                                                        if(isset($agent->license)) echo "md-valid";
                                                                                    @endphp" value="{{$agent->license}}" maxlength="250" required>
                                                                                    <label>Agent's License(OPC,TICO,...)(<font color="red">*</font>)</label>
                                                                                </div>
                                                                            </div>
                                                                        </td>

                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <div class="md-group-add-on">
                                                                             <span class="md-add-on">
                                                                                 <i class="icofont icofont-group-students"></i>
                                                                             </span>
                                                                                <div class="md-input-wrapper">
                                                                                    <input type="text" name="company" class="md-form-control
                                                                                    @php
                                                                                        if(isset($agent->company)) echo "md-valid";
                                                                                    @endphp" value="{{$agent->company}}" maxlength="250" required>
                                                                                    <label>Work company(<font color="red">*</font>)</label>
                                                                                </div>
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <div class="md-group-add-on">
                                                                             <span class="md-add-on">
                                                                                 <i class="icofont icofont-phone"></i>
                                                                             </span>
                                                                                <div class="md-input-wrapper">
                                                                                    <input type="text" name="companyphone" class="md-form-control
                                                                                    @php
                                                                                        if(isset($agent->companyphone)) echo "md-valid";
                                                                                    @endphp" value="{{$agent->companyphone}}" maxlength="250" required>
                                                                                    <label>Company's Phone(<font color="red">*</font>)</label>
                                                                                </div>
                                                                            </div>
                                                                        </td>
                                                                    </tr>



                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                            <!-- end of table col-lg-6 -->

                                                            <div class="col-lg-6">
                                                                <table class="table">
                                                                    <tbody>
                                                                    <tr>
                                                                        <td>
                                                                            <div class="md-group-add-on">
                                                                             <span class="md-add-on">
                                                                                 <i class="icofont icofont-email"></i>
                                                                             </span>
                                                                                <div class="md-input-wrapper">
                                                                                    <input type="text" name="companyaddress" class="md-form-control
                                                                                    @php
                                                                                        if(isset($agent->companyaddress)) echo "md-valid";
                                                                                    @endphp" value="{{$agent->companyaddress}}" maxlength="250" required>
                                                                                    <label>Company's Address(<font color="red">*</font>)</label>
                                                                                </div>
                                                                            </div>
                                                                        </td>
                                                                    </tr>

                                                                    <tr>
                                                                        <td>
                                                                            <div class="md-group-add-on">
                                                                             <span class="md-add-on">
                                                                                 <i class="icofont icofont-web"></i>
                                                                             </span>
                                                                                <div class="md-input-wrapper">
                                                                                    <input type="text" name="companyprovince" class="md-form-control
                                                                                    @php
                                                                                        if(isset($agent->companyprovince)) echo "md-valid";
                                                                                    @endphp" value="{{$agent->companyprovince}}" maxlength="250" required>
                                                                                    <label>Company's Province(<font color="red">*</font>)</label>
                                                                                </div>
                                                                            </div>
                                                                        </td>
                                                                    </tr>

                                                                    <tr>
                                                                        <td>
                                                                            <div class="md-group-add-on">
                                                                             <span class="md-add-on">
                                                                                 <i class="icofont icofont-web"></i>
                                                                             </span>
                                                                                <div class="md-input-wrapper">
                                                                                    <input type="text" name="companycountry" class="md-form-control
                                                                                    @php
                                                                                        if(isset($agent->companycountry)) echo "md-valid";
                                                                                    @endphp" value="{{$agent->companycountry}}" maxlength="250" required>
                                                                                    <label>Company's Country(<font color="red">*</font>)</label>
                                                                                </div>
                                                                            </div>
                                                                        </td>
                                                                    </tr>

                                                                    </tbody>
                                                                </table>

                                                            </div>

                                                            <!-- end of table col-lg-6 -->
                                                        </div>
                                                        <!-- end of row -->
                                                        <div class="text-center">
                                                            <button type="button" id="saveinfo" class="btn btn-primary waves-effect waves-light m-r-20">Save</button>
                                                            <a href="#!" id="edit-cancel" class="btn btn-default waves-effect">Cancel</a>
                                                        </div>
                                                    </div>
                                                    <!-- end of edit info -->
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        @endisset
                        <!-- end of agent tab-pane -->





</form>

                    </div>
                    <!-- end of main tab content -->
                </div>
            </div>

        </div>
        <!-- Container-fluid ends -->
    </div>

@endsection



@section('extrascript')

    <!-- Date picker.js -->
    <script src="/assets/plugins/datepicker/js/moment-with-locales.min.js"></script>
    <script src="/bower_components/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js"></script>

    <!-- Bootstrap Datepicker js -->
    <script type="text/javascript" src="/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
    <script src="/assets/plugins/bootstrap-datepicker/js/bootstrap-datetimepicker.min.js"></script>

    <!-- bootstrap range picker -->
    <script type="text/javascript" src="/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>

    <script>
        $(document).ready(function() {
            //    Edit information
            $('[id=edit-cancel]').on('click',function(){

                var c=$('[id=edit-btn]').find( "i" );
                c.removeClass('icofont-close');
                c.addClass('icofont-edit');
                $('.view-info').show();
                $('.edit-info').hide();

            });

            @isset($becomeingagent)
                $('.view-info').hide();
            @else
                $('.edit-info').hide();
            @endisset



            $('[id=edit-btn]').on('click', function () {
                var b = $('[id=edit-btn]').find( "i" );
                var edit_class = b.attr('class');
                if (edit_class == 'icofont icofont-edit') {
                    b.removeClass('icofont-edit');
                    b.addClass('icofont-close');
                    $('.view-info').hide();
                    $('.edit-info').show();
                }
                else {
                    b.removeClass('icofont-close');
                    b.addClass('icofont-edit');
                    $('.view-info').show();
                    $('.edit-info').hide();
                }
            });
            var passcheck=1;
            $('[id=saveinfo]').on('click',function(){
                $("#info").find(':input').each(function(){
                    if(typeof $(this).attr('required') === 'undefined') {
                        return;
                    }else{
                        console.log($(this).attr("name")+"="+ $(this).val());

                        if($(this).val()==undefined || $(this).val()==""){
                            $(this).parent('div').addClass('md-input-danger');
                            passcheck=0;
                        }
                    }

                })
                if(1===passcheck){
                    $('#info').submit();
                }else{
                    notify('Please fill all required forms','top','center','fa fa-comments','danger','','');
                    return;
                }

            });
        });
    </script>


@endsection
