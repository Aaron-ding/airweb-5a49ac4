
@extends('desktop.layouts.mainframe')


@section('extracss')
    <style>
        .nav .active a {
            color: #ffffff !important;
            background: #0a528e !important;
        }
        .redSpan span {
            color: #c30d23;
        }
    </style>
@endsection

@section('content')
    <section id="content">
        <div id="filterNav" class="sort-by-section clearfix" style="margin: 0;border-bottom: 1px #ddd solid;width:100%;z-index: 999;position: fixed;right: 0px;top: 0px;padding: 0px;background-color: transparent;display: none;">
            <div class="container" style="background-color: #fff;padding: 20px;">
                <form action="#" method="post">
                    <ul class="sort-bar clearfix block-sm pull-left">
                        <li class="sort-by-name" id="setDepartdateLINav">
                            <div class="selector" style="min-width:190px;padding-top: 5px;">
                                <input type="checkbox" id="setNonStopNav"/><span style="margin-left: 5px;"><?php echo lang('Non Stop'); ?></span>
                            </div>
                        </li>
                    </ul>
                    <ul class="sort-bar clearfix block-sm pull-right">
                        <li class="sort-by-name" id="setDepartdateLINav">
                            <div class="selector" style="min-width:190px;">
                                <select class="full-width" id="setDepartdateNav">
                                    <option selected disabled><?php echo lang('修改出发日期'); ?></option>
                                </select>
                            </div>
                        </li>
                        <li class="sort-by-name" id="setReturndateLINav">
                            <div class="selector" style="min-width:190px;">
                                <select class="full-width" id="setReturndateNav">
                                    <option selected disabled><?php echo lang('修改返程日期'); ?></option>
                                </select>
                            </div>
                        </li>
                        <li class="sort-by-name">
                            <div class="selector" style="min-width:140px;"><select class="full-width userdefine" id="priorityNav"><option value="Price"><?php echo lang('低价格优先'); ?></option><option value="DirectFlights"><?php echo lang('直飞优先'); ?></option><option value="Time"><?php echo lang('按出发时间升序'); ?></option></select></div>
                        </li>
                        <li class="sort-by-name">
                            <div class="selector" style="min-width:140px;"><select class="full-width userdefine" id="maxstopsNav"><option value="99"><?php echo lang('不限制中转次数'); ?></option><option value="1"><?php echo lang('最多1次中转'); ?></option><option value="2"><?php echo lang('最多2次中转'); ?></option><option value="3"><?php echo lang('最多3次中转'); ?></option><option value="4"><?php echo lang('最多4次中转'); ?></option><option value="5"><?php echo lang('最多5次中转'); ?></option></select></div>
                        </li>
                    </ul>
                </form>
            </div>
        </div>
        <div class="container">
            <div id="main" style="min-height: 600px;">
                <div class="search-detail-box container">
                    <ul class="search-tabs clearfix">
                        <li class="active"><a data-toggle="tab" flighttype="roundtrip" href="#flights-return-tab"><?php echo lang('双程（往返）'); ?></a></li>
                        <li><a data-toggle="tab" flighttype="oneway" href="#flights-return-tab" id="langjudge"><?php echo lang('单程'); ?></a></li>
                        <li><a data-toggle="tab" flighttype="multiple" href="#flights-Multiple-tab"><?php echo lang('多程'); ?></a></li>
                    </ul>
                    <div class="visible-mobile">
                        <ul class="search-tabs clearfix" id="mobile-search-tabs">
                            <li class="active"><a flighttype="roundtrip" href="assets/#flights-return-tab"><?php echo lang('双程（往返）'); ?></a></li>
                            <li><a flighttype="oneway" href="assets/#flights-return-tab"><?php echo lang('单程'); ?></a></li>
                            <li><a data-toggle="tab" flighttype="multiple" href="#flights-return-tab"><?php echo lang('多程'); ?></a></li>
                        </ul>
                    </div>
                    <div class="search-tab-content">
                        <div class="tab-pane fade active in" id="flights-return-tab">
                            <form action="#" id="searchform" method="post">
                                <input type="hidden" id="currency" name="currency" value="<?php echo $currency; ?>">
                                <div class="row">
                                    <div class="col-xs-6" style="padding-left: 0px;padding-right: 10px;">
                                        <div class="col-xs-6" style="padding-left: 0px;padding-right: 10px;">
                                            <div class="form-group">
                                                <label for="srccity" placehold="<?php echo lang('出发城市'); ?>"><?php echo lang('出发城市'); ?></label>
                                                <div id="multiple-datasets" style="position:relative;"><a class="typeahead-close" href="#"><i class="soap-icon-close" style="font-size:24px;color:#ccc;position:absolute;right:5px;top:0;z-index:1"></i></a> <input class="typeahead input-text full-width airport" data="" id="srccity" placeholder="<?php echo lang('城市，机场名字'); ?>" required="" style="left:-1px!important" type="text" /></div>
                                            </div>
                                        </div>
                                        <div class="col-xs-6" style="padding: 0px;">
                                            <div class="form-group">
                                                <label for="destcity" placehold="<?php echo lang('目的城市'); ?>"><?php echo lang('目的城市'); ?></label>
                                                <div id="multiple-datasets" style="position:relative;"><a class="typeahead-close" href="#"><i class="soap-icon-close" style="font-size:24px;color:#ccc;position:absolute;right:5px;top:0;z-index:1"></i></a> <input class="typeahead input-text full-width airport" data="" id="destcity" placeholder="<?php echo lang('城市，机场名字'); ?>" required="" style="left:-1px!important" type="text" /></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-6" style="padding: 0px;">
                                        <div class="col-xs-3" style="padding-left: 0px;padding-right: 10px;">
                                            <label for="departdate" placehold="<?php echo lang('出发日期'); ?>"><?php echo lang('出发日期'); ?></label>
                                            <div class="form-group">
                                                <div class="datepicker-wrap"><input class="input-text full-width" date-type="date" id="departdate" picker-type="departure" placeholder="yyyy-mm-dd" required="" type="text" /></div>
                                            </div>
                                        </div>
                                        <div class="col-xs-3" style="padding-left: 0px;padding-right: 10px;">
                                            <label for="returndate" placehold="<?php echo lang('返程日期'); ?>"><?php echo lang('返程日期'); ?></label>
                                            <div class="form-group">
                                                <div class="datepicker-wrap"><input class="input-text full-width" date-type="date" id="returndate" picker-type="departure" placeholder="yyyy-mm-dd" required="" type="text" /></div>
                                            </div>
                                        </div>
                                        <div class="col-xs-6" style="padding: 0px;">
                                            <div class="form-group">
                                                <label for="passenger" placehold="<?php echo lang('Passengers'); ?>"><?php echo lang('Passengers'); ?></label>
                                                <a id="passengers_select" class="pointer">
                                                    <div class="mock-select pass-slide-trigger clearfix">
                                                        <span class="flights-home-pass-total">1</span>
                                                        <span class="flights-home-pass-name pass-slide-trigger"><?php echo lang('Passengers'); ?></span>
                                                        <div class="fa fa-caret-down fa-lg right select-arrow pointer pass-slide-trigger"></div>
                                                    </div>
                                                </a>
                                                <div rel="passengers_select" class="home-pass-select-dropdown" id="home-passengers-container" style="display: none;">
                                                    <div class="hp-select-pax-wrap clearfix">
                                                        <div class="hp-select-pax-left btn-plus-minus">
                                                            <a href="" class="pass-slide-trigger" data-type="adult" data-action="decrement"><span class="fa fa-minus pass-slide-trigger"></span></a>
                                                        </div>
                                                        <div class="hp-select-pax-middle">
                                                            <span id="adultnum" class="passenger-number-sel">1</span>
                                                            <span> <?php echo lang('成人'); ?></span>
                                                        </div>
                                                        <div class="hp-select-pax-right btn-plus-minus">
                                                            <a href="" class="pass-slide-trigger" data-type="adult" data-action="increment"><span class="fa fa-plus pass-slide-trigger"></span></a>
                                                        </div>
                                                    </div>
                                                    <div class="hp-select-pax-wrap clearfix">
                                                        <div class="hp-select-pax-left btn-plus-minus">
                                                            <a href="" class="pass-slide-trigger" data-type="children" data-action="decrement">
                                                                <span class="fa fa-minus pass-slide-trigger"></span>
                                                            </a>
                                                        </div>
                                                        <div class="hp-select-pax-middle">
                                                            <span id="childrennum" class="passenger-number-sel">0</span>
                                                            <span> <?php echo lang('儿童(2-12岁)'); ?></span>
                                                        </div>
                                                        <div class="hp-select-pax-right btn-plus-minus">
                                                            <a href="" class="pass-slide-trigger" data-type="children" data-action="increment"><span class="fa fa-plus pass-slide-trigger"></span></a>
                                                        </div>
                                                    </div>
                                                    <div class="hp-select-pax-wrap clearfix">
                                                        <div class="hp-select-pax-left btn-plus-minus">
                                                            <a href="" class="pass-slide-trigger" data-type="infantsseat" data-action="decrement"><span class="fa fa-minus pass-slide-trigger"></span></a>
                                                        </div>
                                                        <div class="hp-select-pax-middle">
                                                            <span id="infantsseatnum" class="passenger-number-sel">0</span>
                                                            <span> <?php echo lang('婴儿(<2岁，有座位)'); ?></span>
                                                        </div>
                                                        <div class="hp-select-pax-right btn-plus-minus">
                                                            <a href="" class="pass-slide-trigger" data-type="infantsseat" data-action="increment"><span class="fa fa-plus pass-slide-trigger"></span></a>
                                                        </div>
                                                    </div>
                                                    <div class="hp-select-pax-wrap clearfix">
                                                        <div class="hp-select-pax-left btn-plus-minus">
                                                            <a href="" class="pass-slide-trigger" data-type="infantslap" data-action="decrement">
                                                                <span class="fa fa-minus pass-slide-trigger"></span>
                                                            </a>
                                                        </div>
                                                        <div class="hp-select-pax-middle">
                                                            <span id="infantslapnum" class="passenger-number-sel">0</span>
                                                            <span> <?php echo lang('婴儿(<2岁，无座位)'); ?></span>
                                                        </div>
                                                        <div class="hp-select-pax-right btn-plus-minus">
                                                            <a href="" class="pass-slide-trigger" data-type="infantslap" data-action="increment">
                                                                <span class="fa fa-plus pass-slide-trigger"></span>
                                                            </a>
                                                        </div>
                                                    </div>
                                                    <div class="hp-select-pax-done">
                                                        <a href="javascript:void(0);" onclick="tjq('#home-passengers-container').slideUp();" class="pax-select-done"><?php echo lang('完成'); ?></a>
                                                    </div>
                                                    <input type="hidden" id="adultcount" name="adultcount" value="1">
                                                    <input type="hidden" id="childrencount" name="childrencount" value="0">
                                                    <input type="hidden" id="infantsseatcount" name="infantsseatcount" value="0">
                                                    <input type="hidden" id="infantslapcount" name="infantslapcount" value="0">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-6" style="padding-left: 0px;padding-right: 10px;">
                                        <div class="col-xs-6" style="padding-left: 0px;padding-right: 10px;">
                                            <label for="PreferLevel" placehold="<?php echo lang('舱位等级'); ?>"><?php echo lang('舱位等级'); ?></label>
                                            <select class="full-width" id="PreferLevel">
                                                <option value="Y"><?php echo lang('经济舱'); ?></option>
                                                <option value="S"><?php echo lang('豪华经济舱'); ?></option>
                                                <option value="C"><?php echo lang('商务舱'); ?></option>
                                                <option value="J"><?php echo lang('豪华商务舱'); ?></option>
                                                <option value="F"><?php echo lang('头等舱'); ?></option>
                                                <option value="P"><?php echo lang('豪华头等舱'); ?></option>
                                            </select>
                                        </div>
                                        <div class="col-xs-6" style="padding: 0px;">
                                            <label for="preferredAirline" placehold="<?php echo lang('航空公司'); ?>"><?php echo lang('航空公司'); ?></label>
                                            <div id="multiple-datasets" style="position:relative;">
                                                <a class="typeahead-close" href="javascript:void(0);"><i class="soap-icon-close" style="font-size:24px;color:#ccc;position:absolute;right:5px;top:0;z-index:1"></i></a>
                                                <input class="typeahead input-text full-width" data="" id="preferredAirline" placeholder="<?php echo lang('名字，代码'); ?>" style="left:-1px!important" type="text" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-6" style="padding: 0px;">
                                        <div class="col-md-12" style="padding: 0px;">
                                            <label id="search_error" style="text-align: center;color: red;">&nbsp;</label>
                                            <button class="icon-check full-width search-button"><?php echo lang('查找最低价'); ?></button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="tab-pane fade active in" id="flights-Multiple-tab" style="display:none">
                            <form action="#" id="m_searchform" method="post">
                                <input type="hidden" id="currency" name="currency" value="<?php echo $currency; ?>">
                                <div class="row internary" idx="1">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="srccity" placehold="<?php echo lang('出发城市'); ?>"><?php echo lang('出发城市'); ?></label>
                                            <div id="multiple-datasets" style="position:relative;"><a class="typeahead-close" href="#"><i class="soap-icon-close" style="font-size:24px;color:#ccc;position:absolute;right:5px;top:0;z-index:1"></i></a> <input class="typeahead input-text full-width airport" data="" id="srccity" placeholder="<?php echo lang('城市，机场名字'); ?>" required="" style="left:-1px!important" type="text" /></div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="destcity" placehold="<?php echo lang('目的城市'); ?>"><?php echo lang('目的城市'); ?></label>
                                            <div id="multiple-datasets" style="position:relative;"><a class="typeahead-close" href="#"><i class="soap-icon-close" style="font-size:24px;color:#ccc;position:absolute;right:5px;top:0;z-index:1"></i></a> <input class="typeahead input-text full-width airport" data="" id="destcity" placeholder="<?php echo lang('城市，机场名字'); ?>" required="" style="left:-1px!important" type="text" /></div>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <label for="m_departdate_1" placehold="<?php echo lang('出发日期'); ?>"><?php echo lang('出发日期'); ?></label>
                                        <div class="form-group">
                                            <div class="datepicker-wrap"><input class="input-text full-width" date-type="date" id="m_departdate_1" picker-type="departure" placeholder="yyyy-mm-dd" required="" type="text" /></div>
                                        </div>
                                    </div>
                                    <div class="col-md-2">&nbsp;</div>
                                </div>
                                <div class="row internary" idx="2">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="srccity" placehold="<?php echo lang('出发城市'); ?>"><?php echo lang('出发城市'); ?></label>
                                            <div id="multiple-datasets" style="position:relative;"><a class="typeahead-close" href="#"><i class="soap-icon-close" style="font-size:24px;color:#ccc;position:absolute;right:5px;top:0;z-index:999"></i></a> <input class="typeahead input-text full-width airport" data="" id="srccity" placeholder="<?php echo lang('城市，机场名字'); ?>" required="" style="left:-1px!important" type="text" /></div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="destcity" placehold="<?php echo lang('目的城市'); ?>"><?php echo lang('目的城市'); ?></label>
                                            <div id="multiple-datasets" style="position:relative;"><a class="typeahead-close" href="#"><i class="soap-icon-close" style="font-size:24px;color:#ccc;position:absolute;right:5px;top:0;z-index:999"></i></a> <input class="typeahead input-text full-width airport" data="" id="destcity" placeholder="<?php echo lang('城市，机场名字'); ?>" required="" style="left:-1px!important" type="text" /></div>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <label for="m_departdate_2" placehold="<?php echo lang('出发日期'); ?>"><?php echo lang('出发日期'); ?></label>
                                        <div class="form-group">
                                            <div class="datepicker-wrap"><input class="input-text full-width" date-type="date" id="m_departdate_2" picker-type="departure" placeholder="yyyy-mm-dd" required="" type="text" /></div>
                                        </div>
                                    </div>
                                    <div class="col-md-1"><label>&nbsp;</label><button class="button full-width append-button"><?php echo lang('添加'); ?></button></div>
                                    <div class="col-md-1"><label>&nbsp;</label><button class="button full-width append-button" style="display:none"><?php echo lang('删除'); ?></button></div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-xs-4">
                                        <div class="form-group">
                                            <label for="passenger" placehold="<?php echo lang('Passengers'); ?>"><?php echo lang('Passengers'); ?></label>
                                            <a id="passengers_select" class="pointer">
                                                <div class="mock-select pass-slide-trigger clearfix">
                                                    <span class="flights-home-pass-total">1</span>
                                                    <span class="flights-home-pass-name pass-slide-trigger"><?php echo lang('Passengers'); ?></span>
                                                    <div class="fa fa-caret-down fa-lg right select-arrow pointer pass-slide-trigger"></div>
                                                </div>
                                            </a>
                                            <div rel="passengers_select" class="home-pass-select-dropdown" id="home-passengers-container" style="display: none;">
                                                <div class="hp-select-pax-wrap clearfix">
                                                    <div class="hp-select-pax-left btn-plus-minus">
                                                        <a href="" class="pass-slide-trigger" data-type="adult" data-action="decrement"><span class="fa fa-minus pass-slide-trigger"></span></a>
                                                    </div>
                                                    <div class="hp-select-pax-middle">
                                                        <span id="adultnum" class="passenger-number-sel">1</span>
                                                        <span> <?php echo lang('成人'); ?></span>
                                                    </div>
                                                    <div class="hp-select-pax-right btn-plus-minus">
                                                        <a href="" class="pass-slide-trigger" data-type="adult" data-action="increment"><span class="fa fa-plus pass-slide-trigger"></span></a>
                                                    </div>
                                                </div>
                                                <div class="hp-select-pax-wrap clearfix">
                                                    <div class="hp-select-pax-left btn-plus-minus">
                                                        <a href="" class="pass-slide-trigger" data-type="children" data-action="decrement">
                                                            <span class="fa fa-minus pass-slide-trigger"></span>
                                                        </a>
                                                    </div>
                                                    <div class="hp-select-pax-middle">
                                                        <span id="childrennum" class="passenger-number-sel">0</span>
                                                        <span> <?php echo lang('儿童(2-12岁)'); ?></span>
                                                    </div>
                                                    <div class="hp-select-pax-right btn-plus-minus">
                                                        <a href="" class="pass-slide-trigger" data-type="children" data-action="increment"><span class="fa fa-plus pass-slide-trigger"></span></a>
                                                    </div>
                                                </div>
                                                <div class="hp-select-pax-wrap clearfix">
                                                    <div class="hp-select-pax-left btn-plus-minus">
                                                        <a href="" class="pass-slide-trigger" data-type="infantsseat" data-action="decrement"><span class="fa fa-minus pass-slide-trigger"></span></a>
                                                    </div>
                                                    <div class="hp-select-pax-middle">
                                                        <span id="infantsseatnum" class="passenger-number-sel">0</span>
                                                        <span> <?php echo lang('婴儿(<2岁，有座位)'); ?></span>
                                                    </div>
                                                    <div class="hp-select-pax-right btn-plus-minus">
                                                        <a href="" class="pass-slide-trigger" data-type="infantsseat" data-action="increment"><span class="fa fa-plus pass-slide-trigger"></span></a>
                                                    </div>
                                                </div>
                                                <div class="hp-select-pax-wrap clearfix">
                                                    <div class="hp-select-pax-left btn-plus-minus">
                                                        <a href="" class="pass-slide-trigger" data-type="infantslap" data-action="decrement">
                                                            <span class="fa fa-minus pass-slide-trigger"></span>
                                                        </a>
                                                    </div>
                                                    <div class="hp-select-pax-middle">
                                                        <span id="infantslapnum" class="passenger-number-sel">0</span>
                                                        <span> <?php echo lang('婴儿(<2岁，无座位)'); ?></span>
                                                    </div>
                                                    <div class="hp-select-pax-right btn-plus-minus">
                                                        <a href="" class="pass-slide-trigger" data-type="infantslap" data-action="increment">
                                                            <span class="fa fa-plus pass-slide-trigger"></span>
                                                        </a>
                                                    </div>
                                                </div>
                                                <div class="hp-select-pax-done">
                                                    <a href="javascript:void(0);" class="pax-select-done"><?php echo lang('完成'); ?></a>
                                                </div>
                                                <input type="hidden" id="adultcount" name="adultcount" value="1">
                                                <input type="hidden" id="childrencount" name="childrencount" value="0">
                                                <input type="hidden" id="infantsseatcount" name="infantsseatcount" value="0">
                                                <input type="hidden" id="infantslapcount" name="infantslapcount" value="0">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-2">
                                        <label for="PreferLevel" placehold="<?php echo lang('舱位等级'); ?>"><?php echo lang('舱位等级'); ?></label>
                                        <div class="selector"><select class="full-width" id="PreferLevel"><option value="Y"><?php echo lang('经济舱'); ?></option><option value="F"><?php echo lang('商务舱'); ?></option><option value="C"><?php echo lang('头等舱'); ?></option></select></div>
                                    </div>
                                    <div class="col-xs-2">
                                        <label for="preferredAirline" placehold="<?php echo lang('航空公司'); ?>"><?php echo lang('航空公司'); ?></label>
                                        <div id="multiple-datasets" style="position:relative;">
                                            <a class="typeahead-close" href="javascript:void(0);"><i class="soap-icon-close" style="font-size:24px;color:#ccc;position:absolute;right:5px;top:0;z-index:1"></i></a>
                                            <input class="typeahead input-text full-width" data="" id="preferredAirline" placeholder="<?php echo lang('名字，代码'); ?>" style="left:-1px!important" type="text" />
                                        </div>
                                    </div>
                                    <div class="col-xs-2"><label>&nbsp;</label><button class="icon-check full-width search-button"><?php echo lang('查找最低价'); ?></button></div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="clearfix box information-box" style="display:none">
                        <div class="alert alert-notice"><?php echo lang('有错误，请联系客服'); ?></div>
                    </div>
                    <!-- loading... -->
                    <div class="sort-by-section clearfix progress-bar-box" style="display: none;">
                        <div class="progress-bar">
                            <div class="fill" currwidth="0" style="width: 0%;">&nbsp;</div>
                        </div>
                        <p class="text-center"><img src="assets/images/loading.gif" /></p>
                    <!--<p class="text-center" style="margin:0;padding-top:30px;"><?php echo lang('请耐心等待，我们正在处理中'); ?>……</p>--></div>
                    <!-- Sequence -->
                    <div class="sort-by-section clearfix function-button-box" style="display:none;">
                        <h4 class="box-title">【<span id="triptypeshow"></span>】<span id="DepartureCityName"><?php echo lang('北京首都国际机场'); ?></span><img src="assets/images/exchange-depart.jpg" /><span id="ArrivateCityName"><?php echo lang('蒙特里尔机场'); ?></span></h4>
                    </div>
                    <div class="sort-by-section clearfix function-searchResult-loading-box" style="display:none;">
                        <h4 class="box-title"><?php echo lang('机票多日价格数据正在载入中'); ?><img width="25" src="assets/images/loading7_light_blue.gif" /></h4>
                    </div>
                    <div class="sort-by-section clearfix function-roundtrip-advanceSearch-box" style="display:none;">
                        <table class="ffcr-fare ffcr-fare-desktop-table ca-general">
                            <thead>
                            <tr>
                                <th class="emptyCell" colspan="1" rowspan="2"><div style="position:absolute;bottom:10px;left:10px;"><?php echo lang('Outbound'); ?></div> <div style="position:absolute;top:10px;right:10px;"><?php echo lang('Inbound'); ?></div></th>
                            </tr>
                            <tr class="weekDay">

                                <th data="1" class="default-ffcr-ffco" aria-hidden="true">
                                    <div>
                                        <time class="date" datetime="" style="text-transform: uppercase;"></time>
                                    </div>
                                </th>
                                <th data="2" class="default-ffcr-ffco" aria-hidden="true">
                                    <div>
                                        <time class="date" datetime="" style="text-transform: uppercase;"></time>
                                    </div>
                                </th>
                                <th data="3" class="default-ffcr-ffco" aria-hidden="true">
                                    <div>
                                        <time class="date" datetime="" style="text-transform: uppercase;"></time>
                                    </div>
                                </th>
                                <th data="4" class="default-ffcr-ffco" aria-hidden="true">
                                    <div>
                                        <time class="date" datetime="" style="text-transform: uppercase;"></time>
                                    </div>
                                </th>
                                <th data="5" class="default-ffcr-ffco" aria-hidden="true">
                                    <div>
                                        <time class="date" datetime="" style="text-transform: uppercase;"></time>
                                    </div>
                                </th>
                                <th data="6" class="default-ffcr-ffco" aria-hidden="true">
                                    <div><time class="date" datetime="" style="text-transform: uppercase;"></time>
                                    </div>
                                </th>
                                <th data="7" class="default-ffcr-ffco" aria-hidden="true">
                                    <div>
                                        <time class="date" datetime="" style="text-transform: uppercase;"></time>
                                    </div>
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr class="ffcr-first first  ffcr-last">

                                <th class="weekDay default-ffcr-ffco">
                                    <div>
                                        <time class="date" datetime="" style="text-transform: uppercase;"></time>
                                    </div>
                                </th>
                                <td data="1">
                                    <div class="option available" tabindex="0"><span class="price  ffcr-price"><span class="number"></span></span><span class="sr-only">Selected</span>
                                    </div>
                                </td>
                                <td data="2">
                                    <div class="option available" tabindex="0"><span class="price  ffcr-price"><span class="number"></span></span><span class="sr-only">Selected</span>
                                    </div>
                                </td>
                                <td data="3">
                                    <div class="option available" tabindex="0"><span class="price  ffcr-price"><span class="number"></span></span><span class="sr-only">Selected</span>
                                    </div>
                                </td>
                                <td data="4">
                                    <div class="option available" tabindex="0"><span class="price  ffcr-price"><span class="number"></span></span><span class="sr-only">Selected</span>
                                    </div>
                                </td>
                                <td data="5">
                                    <div class="option available selectboxFocus" tabindex="0"><span class="price  ffcr-price"><span class="number"></span></span>
                                    </div>
                                </td>
                                <td data="6">
                                    <div class="option available selectboxFocus" tabindex="0"><span class="price  ffcr-price"><span class="number"></span></span>
                                    </div>
                                </td>
                                <td data="7">
                                    <div class="option available selectboxFocus" tabindex="0"><span class="price  ffcr-price"><span class="number"></span></span>
                                    </div>
                                </td>
                            </tr>
                            <tr class="ffcr-first ffcr-last">
                                <th class="weekDay default-ffcr-ffco">
                                    <div>
                                        <time class="date" datetime="" style="text-transform: uppercase;"></time>
                                    </div>
                                </th>
                                <td data="1">
                                    <div class="option available selectboxFocus" tabindex="0"><span><input type="radio" name="DATE" style="display:none" value="18/10/2016-07/11/2016"></span><span class="sr-only">Outbound 18/10/2016 Inbound 07/11/2016</span><span class="price  ffcr-price"><span class="number"></span></span>
                                    </div>
                                </td>
                                <td data="2">
                                    <div class="option available" tabindex="0"><span><input type="radio" name="DATE" style="display:none" value="18/10/2016-08/11/2016"></span><span class="sr-only">Outbound 18/10/2016 Inbound 08/11/2016</span><span class="price  ffcr-price"><span class="number"></span></span><span class="sr-only">Selected</span><span class="sr-only">Selected</span>
                                    </div>
                                </td>
                                <td data="3">
                                    <div class="option available" tabindex="0"><span><input type="radio" name="DATE" style="display:none" value="18/10/2016-09/11/2016"></span><span class="sr-only">Outbound 18/10/2016 Inbound 09/11/2016</span><span class="price  ffcr-price"><span class="number"></span></span><span class="sr-only">Selected</span><span class="sr-only">Selected</span>
                                    </div>
                                </td>
                                <td data="4">
                                    <div class="option available" tabindex="0"><span><input type="radio" name="DATE" style="display:none" value="18/10/2016-10/11/2016"></span><span class="sr-only">Outbound 18/10/2016 Inbound 10/11/2016</span><span class="price  ffcr-price"><span class="number"></span></span><span class="sr-only">Selected</span>
                                    </div>
                                </td>
                                <td data="5">
                                    <div class="option available" tabindex="0"><span><input type="radio" name="DATE" style="display:none" value="18/10/2016-11/11/2016"></span><span class="sr-only">Outbound 18/10/2016 Inbound 11/11/2016</span><span class="price  ffcr-price"><span class="number"></span></span><span class="sr-only">Selected</span>
                                    </div>
                                </td>
                                <td data="6">
                                    <div class="option available selectboxFocus" tabindex="0"><span><input type="radio" name="DATE" style="display:none" value="18/10/2016-12/11/2016"></span><span class="sr-only">Outbound 18/10/2016 Inbound 12/11/2016</span><span class="price  ffcr-price"><span class="number"></span></span>
                                    </div>
                                </td>
                                <td data="7">
                                    <div class="option available selectboxFocus" tabindex="0"><span><input type="radio" name="DATE" style="display:none" value="18/10/2016-13/11/2016"></span><span class="sr-only">Outbound 18/10/2016 Inbound 13/11/2016</span><span class="price  ffcr-price"><span class="number"></span></span>
                                    </div>
                                </td>
                            </tr>
                            <tr class="ffcr-first ffcr-last">
                                <th class="weekDay default-ffcr-ffco">
                                    <div>
                                        <time class="date" datetime="" style="text-transform: uppercase;"></time>
                                    </div>
                                </th>
                                <td data="1">
                                    <div class="option available selectboxFocus" tabindex="0"><span><input type="radio" name="DATE" style="display:none" value="19/10/2016-07/11/2016"></span><span class="sr-only">Outbound 19/10/2016 Inbound 07/11/2016</span><span class="price  ffcr-price"><span class="number"></span></span>
                                    </div>
                                </td>
                                <td data="2">
                                    <div class="option available" tabindex="0"><span><input type="radio" name="DATE" style="display:none" value="19/10/2016-08/11/2016"></span><span class="sr-only">Outbound 19/10/2016 Inbound 08/11/2016</span><span class="price  ffcr-price"><span class="number"></span></span><span class="sr-only">Selected</span>
                                    </div>
                                </td>
                                <td data="3">
                                    <div class="option available" tabindex="0"><span><input type="radio" name="DATE" style="display:none" value="19/10/2016-09/11/2016"></span><span class="sr-only">Outbound 19/10/2016 Inbound 09/11/2016</span><span class="price  ffcr-price"><span class="number"></span></span><span class="sr-only">Selected</span><span class="sr-only">Selected</span>
                                    </div>
                                </td>
                                <td data="4">
                                    <div class="option available" tabindex="0"><span><input type="radio" name="DATE" style="display:none" value="19/10/2016-10/11/2016"></span><span class="sr-only">Outbound 19/10/2016 Inbound 10/11/2016</span><span class="price  ffcr-price"><span class="number"></span></span><span class="sr-only">Selected</span><span class="sr-only">Selected</span>
                                    </div>
                                </td>
                                <td data="5">
                                    <div class="option available selectboxFocus" tabindex="0"><span><input type="radio" name="DATE" style="display:none" value="19/10/2016-11/11/2016"></span><span class="sr-only">Outbound 19/10/2016 Inbound 11/11/2016</span><span class="price  ffcr-price"><span class="number"></span></span>
                                    </div>
                                </td>
                                <td data="6">
                                    <div class="option available selectboxFocus" tabindex="0"><span><input type="radio" name="DATE" style="display:none" value="19/10/2016-12/11/2016"></span><span class="sr-only">Outbound 19/10/2016 Inbound 12/11/2016</span><span class="price  ffcr-price"><span class="number"></span></span>
                                    </div>
                                </td>
                                <td data="7">
                                    <div class="option available selectboxFocus" tabindex="0"><span><input type="radio" name="DATE" style="display:none" value="19/10/2016-13/11/2016"></span><span class="sr-only">Outbound 19/10/2016 Inbound 13/11/2016</span><span class="price  ffcr-price"><span class="number"></span></span>
                                    </div>
                                </td>
                            </tr>
                            <tr class="ffcr-first ffcr-last">
                                <th class="weekDay default-ffcr-ffco">
                                    <div>
                                        <time class="date" datetime="" style="text-transform: uppercase;"></time>
                                    </div>
                                </th>
                                <td data="1">
                                    <div class="option available selectboxFocus" tabindex="0"><span><input type="radio" name="DATE" style="display:none" value="20/10/2016-07/11/2016"></span><span class="sr-only">Outbound 20/10/2016 Inbound 07/11/2016</span><span class="price  ffcr-price"><span class="number"></span></span>
                                    </div>
                                </td>
                                <td data="2">
                                    <div class="option available selectboxFocus" tabindex="0"><span><input type="radio" name="DATE" style="display:none" value="20/10/2016-08/11/2016"></span><span class="sr-only">Outbound 20/10/2016 Inbound 08/11/2016</span><span class="price  ffcr-price"><span class="number"></span></span>
                                    </div>
                                </td>
                                <td data="3">
                                    <div class="option available selectboxFocus" tabindex="0"><span><input type="radio" name="DATE" style="display:none" value="20/10/2016-09/11/2016"></span><span class="sr-only">Outbound 20/10/2016 Inbound 09/11/2016</span><span class="price  ffcr-price"><span class="number"></span></span>
                                    </div>
                                </td>
                                <td data="4">
                                    <div class="option available selectboxFocus" tabindex="0"><span><input type="radio" name="DATE" style="display:none" value="20/10/2016-10/11/2016" checked="checked"></span><span class="sr-only">Outbound 20/10/2016 Inbound 10/11/2016</span><span class="price  ffcr-price"><span class="number"></span></span>
                                    </div>
                                </td>
                                <td data="5">
                                    <div class="option available selectboxFocus" tabindex="0"><span><input type="radio" name="DATE" style="display:none" value="20/10/2016-11/11/2016"></span><span class="sr-only">Outbound 20/10/2016 Inbound 11/11/2016</span><span class="price  ffcr-price"><span class="number"></span></span>
                                    </div>
                                </td>
                                <td data="6">
                                    <div class="option available selectboxFocus" tabindex="0"><span><input type="radio" name="DATE" style="display:none" value="20/10/2016-12/11/2016"></span><span class="sr-only">Outbound 20/10/2016 Inbound 12/11/2016</span><span class="price  ffcr-price"><span class="number"></span></span>
                                    </div>
                                </td>
                                <td data="7">
                                    <div class="option available selectboxFocus" tabindex="0"><span><input type="radio" name="DATE" style="display:none" value="20/10/2016-13/11/2016"></span><span class="sr-only">Outbound 20/10/2016 Inbound 13/11/2016</span><span class="price  ffcr-price"><span class="number"></span></span>
                                    </div>
                                </td>
                            </tr>
                            <tr class="ffcr-first ffcr-last">
                                <th class="weekDay default-ffcr-ffco">
                                    <div>
                                        <time class="date" datetime="" style="text-transform: uppercase;"></time>
                                    </div>
                                </th>
                                <td data="1">
                                    <div class="option available selectboxFocus" tabindex="0"><span><input type="radio" name="DATE" style="display:none" value="21/10/2016-07/11/2016"></span><span class="sr-only">Outbound 21/10/2016 Inbound 07/11/2016</span><span class="price  ffcr-price"><span class="number"></span></span>
                                    </div>
                                </td>
                                <td data="2">
                                    <div class="option available selectboxFocus" tabindex="0"><span><input type="radio" name="DATE" style="display:none" value="21/10/2016-08/11/2016"></span><span class="sr-only">Outbound 21/10/2016 Inbound 08/11/2016</span><span class="price  ffcr-price"><span class="number"></span></span>
                                    </div>
                                </td>
                                <td data="3">
                                    <div class="option available selectboxFocus" tabindex="0"><span><input type="radio" name="DATE" style="display:none" value="21/10/2016-09/11/2016"></span><span class="sr-only">Outbound 21/10/2016 Inbound 09/11/2016</span><span class="price  ffcr-price"><span class="number"></span></span>
                                    </div>
                                </td>
                                <td data="4">
                                    <div class="option available selectboxFocus" tabindex="0"><span><input type="radio" name="DATE" style="display:none" value="21/10/2016-10/11/2016"></span><span class="sr-only">Outbound 21/10/2016 Inbound 10/11/2016</span><span class="price  ffcr-price"><span class="number"></span></span>
                                    </div>
                                </td>
                                <td data="5">
                                    <div class="option available selectboxFocus" tabindex="0"><span><input type="radio" name="DATE" style="display:none" value="21/10/2016-11/11/2016"></span><span class="sr-only">Outbound 21/10/2016 Inbound 11/11/2016</span><span class="price  ffcr-price"><span class="number"></span></span>
                                    </div>
                                </td>
                                <td data="6">
                                    <div class="option available selectboxFocus" tabindex="0"><span><input type="radio" name="DATE" style="display:none" value="21/10/2016-12/11/2016"></span><span class="sr-only">Outbound 21/10/2016 Inbound 12/11/2016</span><span class="price  ffcr-price"><span class="number"></span></span>
                                    </div>
                                </td>
                                <td data="7">
                                    <div class="option available selectboxFocus" tabindex="0"><span><input type="radio" name="DATE" style="display:none" value="21/10/2016-13/11/2016"></span><span class="sr-only">Outbound 21/10/2016 Inbound 13/11/2016</span><span class="price  ffcr-price"><span class="number"></span></span>
                                    </div>
                                </td>
                            </tr>
                            <tr class="ffcr-first ffcr-last">
                                <th class="weekDay default-ffcr-ffco">
                                    <div>
                                        <time class="date" datetime="" style="text-transform: uppercase;"></time>
                                    </div>
                                </th>
                                <td data="1">
                                    <div class="option lowest  available selectboxFocus" tabindex="0"><span><input type="radio" name="DATE" style="display:none" value="22/10/2016-07/11/2016"></span><span class="sr-only">Outbound 22/10/2016 Inbound 07/11/2016</span><span class="price  ffcr-price"><span class="number"></span></span>
                                    </div>
                                </td>
                                <td data="2">
                                    <div class="option lowest  available selectboxFocus" tabindex="0"><span><input type="radio" name="DATE" style="display:none" value="22/10/2016-08/11/2016"></span><span class="sr-only">Outbound 22/10/2016 Inbound 08/11/2016</span><span class="price  ffcr-price"><span class="number"></span></span>
                                    </div>
                                </td>
                                <td data="3">
                                    <div class="option lowest  available selectboxFocus" tabindex="0"><span><input type="radio" name="DATE" style="display:none" value="22/10/2016-09/11/2016"></span><span class="sr-only">Outbound 22/10/2016 Inbound 09/11/2016</span><span class="price  ffcr-price"><span class="number"></span></span>
                                    </div>
                                </td>
                                <td data="4">
                                    <div class="option lowest  available selectboxFocus" tabindex="0"><span><input type="radio" name="DATE" style="display:none" value="22/10/2016-10/11/2016"></span><span class="sr-only">Outbound 22/10/2016 Inbound 10/11/2016</span><span class="price  ffcr-price"><span class="number"></span></span>
                                    </div>
                                </td>
                                <td data="5">
                                    <div class="option lowest  available selectboxFocus" tabindex="0"><span><input type="radio" name="DATE" style="display:none" value="22/10/2016-11/11/2016"></span><span class="sr-only">Outbound 22/10/2016 Inbound 11/11/2016</span><span class="price  ffcr-price"><span class="number"></span></span>
                                    </div>
                                </td>
                                <td data="6">
                                    <div class="option lowest  available selectboxFocus" tabindex="0"><span><input type="radio" name="DATE" style="display:none" value="22/10/2016-12/11/2016"></span><span class="sr-only">Outbound 22/10/2016 Inbound 12/11/2016</span><span class="price  ffcr-price"><span class="number"></span></span>
                                    </div>
                                </td>
                                <td data="7">
                                    <div class="option lowest  available selectboxFocus" tabindex="0"><span><input type="radio" name="DATE" style="display:none" value="22/10/2016-13/11/2016"></span><span class="sr-only">Outbound 22/10/2016 Inbound 13/11/2016</span><span class="price  ffcr-price"><span class="number"></span></span>
                                    </div>
                                </td>
                            </tr>
                            <tr class="ffcr-first  last ffcr-last">
                                <th class="weekDay default-ffcr-ffco">
                                    <div>
                                        <time class="date" datetime="" style="text-transform: uppercase;"></time>
                                    </div>
                                </th>
                                <td data="1">
                                    <div class="option lowest  available selectboxFocus" tabindex="0"><span><input type="radio" name="DATE" style="display:none" value="23/10/2016-07/11/2016"></span><span class="sr-only">Outbound 23/10/2016 Inbound 07/11/2016</span><span class="price  ffcr-price"><span class="number"></span></span>
                                    </div>
                                </td>
                                <td data="2">
                                    <div class="option lowest  available selectboxFocus" tabindex="0"><span><input type="radio" name="DATE" style="display:none" value="23/10/2016-08/11/2016"></span><span class="sr-only">Outbound 23/10/2016 Inbound 08/11/2016</span><span class="price  ffcr-price"><span class="number"></span></span>
                                    </div>
                                </td>
                                <td data="3">
                                    <div class="option lowest  available selectboxFocus" tabindex="0"><span><input type="radio" name="DATE" style="display:none" value="23/10/2016-09/11/2016"></span><span class="sr-only">Outbound 23/10/2016 Inbound 09/11/2016</span><span class="price  ffcr-price"><span class="number"></span></span>
                                    </div>
                                </td>
                                <td data="4">
                                    <div class="option lowest  available selectboxFocus" tabindex="0"><span><input type="radio" name="DATE" style="display:none" value="23/10/2016-10/11/2016"></span><span class="sr-only">Outbound 23/10/2016 Inbound 10/11/2016</span><span class="price  ffcr-price"><span class="number"></span></span>
                                    </div>
                                </td>
                                <td data="5">
                                    <div class="option lowest  available selectboxFocus" tabindex="0"><span><input type="radio" name="DATE" style="display:none" value="23/10/2016-11/11/2016"></span><span class="sr-only">Outbound 23/10/2016 Inbound 11/11/2016</span><span class="price  ffcr-price"><span class="number"></span></span>
                                    </div>
                                </td>
                                <td data="6">
                                    <div class="option lowest  available selectboxFocus" tabindex="0"><span><input type="radio" name="DATE" style="display:none" value="23/10/2016-12/11/2016"></span><span class="sr-only">Outbound 23/10/2016 Inbound 12/11/2016</span><span class="price  ffcr-price"><span class="number"></span></span>
                                    </div>
                                </td>
                                <td data="7">
                                    <div class="option lowest  available selectboxFocus" tabindex="0"><span><input type="radio" name="DATE" style="display:none" value="23/10/2016-13/11/2016"></span><span class="sr-only">Outbound 23/10/2016 Inbound 13/11/2016</span><span class="price  ffcr-price"><span class="number"></span></span>
                                    </div>
                                </td>
                            </tr>
                            </tbody>
                            <tfoot>
                            <tr>
                                <td colspan="9" class="hidden-md hidden-lg">
                                    <div class="booking-legend">
                                        <p><strong><?php echo lang('Legend'); ?>:</strong><span><span class="legendLabel"><span class="lowest-price-small hidden-md hidden-lg">Lowest</span><span class="sprite lowest-price-triangle hidden-xs hidden-sm"></span>: <?php echo lang('Lowest price'); ?> </span><span class="legendLabel"><strong> C$</strong>: <?php echo lang('Canadian Dollar'); ?> </span></span></p><p> <?php echo lang('flight_ticket_legend'); ?> </p>
                                    </div>
                                </td>
                                <td colspan="9" class="hidden-sm hidden-xs">
                                    <div>
                                        <p><strong><?php echo lang('Legend'); ?>:</strong>&nbsp; <span class="lowest-price-small-ffcr-ffco-legend hidden-lg">Lowest</span><span class="sprite lowest-price-triangle"></span>: <?php echo lang('Lowest price'); ?> &nbsp;&nbsp;&nbsp;    </p><p> <?php echo lang('flight_ticket_legend'); ?> </p>
                                    </div>
                                </td>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                    <div class="sort-by-section clearfix function-oneway-advanceSearch-box" style="display:none;">
                        <div class="ffco-desktop-caption clearfix hidden-xs">
                            <button class="btn pull-left selectboxFocus" type="button" aria-pressed="true" id="btnPrevWeek"><strong><?php echo lang('Previous 7 days'); ?></strong></button>
                            <button class="btn pull-right selectboxFocus" type="button" aria-pressed="true" id="btnNextWeek"><strong><?php echo lang('Next 7 days'); ?></strong></button>
                        </div>
                        <table class="ffcr-fare ffcr-fare-desktop-table ca-general">
                            <thead>
                            <tr class="weekDay">
                                <th class="default-ffcr-ffco first" data="1">
                                    <div>
                                        <time class="day" datetime="" style="text-transform: uppercase;"></time>
                                    </div>
                                </th>
                                <th class="default-ffcr-ffco" data="2">
                                    <div>
                                        <time class="day" datetime="" style="text-transform: uppercase;"></time>
                                    </div>
                                </th>
                                <th class="default-ffcr-ffco" data="3">
                                    <div>
                                        <time class="day" datetime="" style="text-transform: uppercase;"></time>
                                    </div>
                                </th>
                                <th class="default-ffcr-ffco" data="4">
                                    <div>
                                        <time class="day" datetime="" style="text-transform: uppercase;"></time>
                                    </div>
                                </th>
                                <th class="default-ffcr-ffco" data="5">
                                    <div>
                                        <time class="day" datetime="" style="text-transform: uppercase;"></time>
                                    </div>
                                </th>
                                <th class="default-ffcr-ffco" data="6">
                                    <div>
                                        <time class="day" datetime="" style="text-transform: uppercase;"></time>
                                    </div>
                                </th>
                                <th class="default-ffcr-ffco" data="7">
                                    <div>
                                        <time class="day" datetime="" style="text-transform: uppercase;"></time>
                                    </div>
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr class="ffcr-first first ffcr-last">
                                <td data="1">
                                    <div class="option available" tabindex="0"><span class="price  ffcr-price"><span class="number"></span></span><span class="sr-only">Selected</span>
                                    </div>
                                </td>
                                <td data="2">
                                    <div class="option available" tabindex="0"><span class="price  ffcr-price"><span class="number"></span></span><span class="sr-only">Selected</span>
                                    </div>
                                </td>
                                <td data="3">
                                    <div class="option available" tabindex="0"><span class="price  ffcr-price"><span class="number"></span></span><span class="sr-only">Selected</span>
                                    </div>
                                </td>
                                <td data="4">
                                    <div class="option available" tabindex="0"><span class="price  ffcr-price"><span class="number"></span></span><span class="sr-only">Selected</span>
                                    </div>
                                </td>
                                <td data="5">
                                    <div class="option available" tabindex="0"><span class="price  ffcr-price"><span class="number"></span></span><span class="sr-only">Selected</span>
                                    </div>
                                </td>
                                <td data="6">
                                    <div class="option available" tabindex="0"><span class="price  ffcr-price"><span class="number"></span></span><span class="sr-only">Selected</span>
                                    </div>
                                </td>
                                <td data="7">
                                    <div class="option available" tabindex="0"><span class="price  ffcr-price"><span class="number"></span></span><span class="sr-only">Selected</span>
                                    </div>
                                </td>
                            </tr>
                            <!--
                            <tr class="ffco-first ffco-last">
                                <td class="ffco-first first ffco-last" data="1">
                                    <div class="option available ffco-list selectboxFocus" tabindex="0">
                                        <span class="date" aria-hidden="true"><time datetime=""></time></span>
                                        <span class="sr-only"></span>
                                        <span class="price  promo-class"><span class="currency"><strong></strong></span><strong></strong></span>
                                    </div>
                                </td>
                                <td class="ffco-first ffco-last" data="2">
                                    <div class="option available ffco-list selectboxFocus" tabindex="0">
                                        <span class="date" aria-hidden="true"><time datetime=""></time></span>
                                        <span class="sr-only"></span>
                                        <span class="price  promo-class"><span class="currency"><strong></strong></span><strong></strong></span>
                                    </div>
                                </td>
                                <td class="ffco-first ffco-last" data="3">
                                    <div class="option available ffco-list selectboxFocus" tabindex="0">
                                        <span class="date" aria-hidden="true"><time datetime=""></time></span>
                                        <span class="sr-only"></span>
                                        <span class="price  promo-class"><span class="currency"><strong></strong></span><strong></strong></span>
                                    </div>
                                </td>
                                <td class="ffco-first ffco-last" data="4">
                                    <div class="option available ffco-list selectboxFocus" tabindex="0">
                                        <span class="date" aria-hidden="true"><time datetime=""></time></span>
                                        <span class="sr-only"></span>
                                        <span class="price  promo-class"><span class="currency"><strong></strong></span><strong></strong></span>
                                    </div>
                                </td>
                                <td class="ffco-first ffco-last" data="5">
                                    <div class="option available ffco-list selectboxFocus" tabindex="0">
                                        <span class="date" aria-hidden="true"><time datetime=""></time></span>
                                        <span class="sr-only"></span>
                                        <span class="price  promo-class"><span class="currency"><strong></strong></span><strong></strong></span>
                                    </div>
                                </td>
                                <td class="ffco-first ffco-last" data="6">
                                    <div class="option available ffco-list selectboxFocus" tabindex="0">
                                        <span class="date" aria-hidden="true"><time datetime=""></time></span>
                                        <span class="sr-only"></span>
                                        <span class="price  promo-class"><span class="currency"><strong></strong></span><strong></strong></span>
                                    </div>
                                </td>
                                <td class="ffco-first last ffco-last" data="7">
                                    <div class="option available ffco-list selectboxFocus" tabindex="0">
                                        <span class="date" aria-hidden="true"><time datetime=""></time></span>
                                        <span class="sr-only"></span>
                                        <span class="price  promo-class"><span class="currency"><strong></strong></span><strong></strong></span>
                                    </div>
                                </td>
                            </tr>
                            <tr class="ffco-first last ffco-last">
                                <td class="ffco-first first ffco-last" data="1">
                                    <div class="option available ffco-list selectboxFocus" tabindex="0">
                                        <span class="date" aria-hidden="true">
                                            <time datetime=""></time>
                                        </span>
                                        <span class="sr-only"></span>
                                        <span class="price  promo-class">
                                            <span class="currency"><strong></strong></span><strong></strong>
                                        </span>
                                    </div>
                                </td>
                                <td class="ffco-first ffco-last" data="2">
                                    <div class="option available ffco-list selectboxFocus" tabindex="0">
                                        <span class="date" aria-hidden="true">
                                            <time datetime=""></time>
                                        </span>
                                        <span class="sr-only"></span>
                                        <span class="price  promo-class">
                                            <span class="currency"><strong></strong></span><strong></strong>
                                        </span>
                                    </div>
                                </td>
                                <td class="ffco-first ffco-last" data="3">
                                    <div class="option available ffco-list selectboxFocus" tabindex="0">
                                        <span class="date" aria-hidden="true">
                                            <time datetime=""></time>
                                        </span>
                                        <span class="sr-only"></span>
                                        <span class="price  promo-class">
                                            <span class="currency"><strong></strong></span><strong></strong>
                                        </span>
                                    </div>
                                </td>
                                <td class="ffco-first ffco-last" data="4">
                                    <div class="option available ffco-list selectboxFocus" tabindex="0">
                                        <span class="date" aria-hidden="true">
                                            <time datetime=""></time>
                                        </span>
                                        <span class="sr-only"></span>
                                        <span class="price  promo-class">
                                            <span class="currency"><strong></strong></span><strong></strong>
                                        </span>
                                    </div>
                                </td>
                                <td class="ffco-first ffco-last" data="5">
                                    <div class="option available ffco-list selectboxFocus" tabindex="0">
                                        <span class="date" aria-hidden="true">
                                            <time datetime=""></time>
                                        </span>
                                                            <span class="sr-only"></span>
                                        <span class="price  promo-class">
                                            <span class="currency"><strong></strong></span><strong></strong>
                                        </span>
                                    </div>
                                </td>
                                <td class="ffco-first ffco-last" data="6">
                                    <div class="option available ffco-list selectboxFocus" tabindex="0">
                                        <span class="date" aria-hidden="true"><time datetime=""></time></span>
                                        <span class="sr-only"></span>
                                        <span class="price  promo-class"><span class="currency"><strong></strong></span><strong></strong></span>
                                    </div>
                                </td>
                                <td class="ffco-first last ffco-last" data="7">
                                    <div class="option available ffco-list selectboxFocus" tabindex="0">
                                        <span class="date" aria-hidden="true"><time datetime=""></time></span>
                                        <span class="sr-only"></span>
                                        <span class="price  promo-class"><span class="currency"><strong></strong></span><strong></strong></span>
                                    </div>
                                </td>
                            </tr>
                            -->
                            </tbody>
                            <tfoot>
                            <tr>
                                <td colspan="7" class="hidden-md hidden-lg">
                                    <div class="booking-legend">
                                        <p><strong><?php echo lang('Legend'); ?>:</strong><span><span class="legendLabel"><span class="lowest-price-small hidden-md hidden-lg">Lowest</span><span class="sprite lowest-price-triangle hidden-xs hidden-sm"></span>: <?php echo lang('Lowest price'); ?> </span></span></p><p> <?php echo lang('flight_ticket_legend'); ?> </p>
                                    </div>
                                </td>
                                <td colspan="7" class="hidden-sm hidden-xs">
                                    <div>
                                        <p><strong><?php echo lang('Legend'); ?>:</strong>&nbsp; <span class="lowest-price-small-ffcr-ffco-legend hidden-lg">Lowest</span><span class="sprite lowest-price-triangle"></span>: <?php echo lang('Lowest price'); ?> &nbsp;&nbsp;&nbsp;    </p><p> <?php echo lang('flight_ticket_legend'); ?> </p>
                                    </div>
                                </td>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                    <div id="filterTab" class="sort-by-section clearfix function-button-box" style="display:none;margin: 0;border-bottom: 1px #ddd solid;">
                        <form action="#" id="extraoption" method="post">
                            <ul class="sort-bar clearfix block-sm pull-left">
                                <li class="sort-by-name" id="setDepartdateLINav">
                                    <div class="selector" style="min-width:190px;padding-top: 5px;">
                                        <input type="checkbox" id="setNonStop"/><span style="margin-left: 5px;"><?php echo lang('Non Stop'); ?></span>
                                    </div>
                                </li>
                            </ul>
                            <ul class="sort-bar clearfix block-sm pull-right">
                                <li class="sort-by-name" id="setDepartdateLI">
                                    <div class="selector" style="min-width:190px;">
                                        <select class="full-width" id="setDepartdate">
                                            <option selected disabled><?php echo lang('修改出发日期'); ?></option>
                                        </select>
                                    </div>
                                </li>
                                <li class="sort-by-name" id="setReturndateLI">
                                    <div class="selector" style="min-width:190px;">
                                        <select class="full-width" id="setReturndate">
                                            <option selected disabled><?php echo lang('修改返程日期'); ?></option>
                                        </select>
                                    </div>
                                </li>
                                <li class="sort-by-name">
                                    <div class="selector" style="min-width:140px;"><select class="full-width userdefine" id="priority"><option value="Price"><?php echo lang('低价格优先'); ?></option><option value="DirectFlights"><?php echo lang('直飞优先'); ?></option><option value="Time"><?php echo lang('按出发时间升序'); ?></option></select></div>
                                </li>
                                <li class="sort-by-name">
                                    <div class="selector" style="min-width:140px;"><select class="full-width userdefine" id="maxstops"><option value="99"><?php echo lang('不限制中转次数'); ?></option><option value="1"><?php echo lang('最多1次中转'); ?></option><option value="2"><?php echo lang('最多2次中转'); ?></option><option value="3"><?php echo lang('最多3次中转'); ?></option><option value="4"><?php echo lang('最多4次中转'); ?></option><option value="5"><?php echo lang('最多5次中转'); ?></option></select></div>
                                </li>
                            </ul>
                        </form>
                    </div>
                    <!-- searchresultlist -->
                    <div id="searchresultlist" class="listing-style flight">
                        <article class="box" id="ft_item_template" style="display:none" data-price="">
                            <div style="position: absolute;padding: 5px;"><img id="provider" src="assets/images/sabre.jpg" style="width: 20px;display: <?php if ($line == 'dev') { echo 'block'; } else { echo 'none'; } ?>"></div>
                            <div class="details col-sm-12">
                                <div class="details-wrapper">
                                    <h4 id="mark-depature" style="display:none"><?php echo lang('去程'); ?></h4>
                                    <div class="first-row">
                                        <div>
                                            <img alt="" class="airimg" src="assets/images/mail-logo.png" />
                                        </div>
                                        <div class="time">
                                            <h4 class="box-title  col-sm-3">
                                                <div class="icon"><i class="soap-icon-plane-right takeoff-effect"></i></div>
                                                <span id="DepartureDate"></span>, <span id="DepartureTime"></span>
                                            </h4>
                                            <h4 class="box-title col-sm-4">
                                                <div class="icon"><i class="soap-icon-plane-right landing-effect"></i></div>
                                                <span id="ArrivalDate"></span>, <span id="ArrivalTime"></span>
                                            </h4>
                                            <h4 class="box-title col-sm-5">
                                                <div class="icon"><i class="soap-icon-clock"></i></div>
                                                <span id="ElapsedTimeTotalHours"></span><?php echo lang('小时'); ?><span id="ElapsedTimeTotalHoursLeftMins"></span><?php echo lang('分钟'); ?>
                                            </h4>
                                        </div>
                                        <div>
                                            <span class="price"><span id="TotalFareCurrency"></span><span id="TotalFare"></span><span id="TotalPeople">/<?php echo lang('人'); ?></span></span>
                                            <?php echo lang('总价格'); ?>：<span id="TotalFareTotal"></span>
                                        </div>
                                    </div>
                                    <div class="second-row">
                                        <div><span id="FlightName"></span> <!--<span id="FlightNumber"></span>--></div>
                                        <div><a class="button btn-mini"><span id="stops"></span></a> <span id="stopcitylist"> <span id="stopaircode"> <em id="up">UC12345</em> <em id="down"><a class="soap-popupbox" href="#flighttypeDetail" onload="loadflighttypedetail">777-3850</a></em> </span> </span> <a class="soap-popupbox" href="#flightDetail" onload="loadflightdetail">&nbsp;&nbsp;&nbsp;<?php echo lang('查看详情'); ?><i class="fa fa-caret-down"></i></a></div>
                                        <div class="action">
                                            <a href="javascript:void(0);" class="button btn-small full-width gobutton"></a>
                                            <div id="loadingseat" style="display:none;"><i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i> <span class="sr-only">Loading...</span></div>
                                            <div class="red-color" id="nomoreseat" style="display:none;"><?php echo lang('很抱歉，位置售罄！'); ?></div>
                                        </div>
                                    </div>
                                <!--
                                <div class="third-row">
                                    <div><img id="provider" src="assets/images/sabre.jpg" style="display: <?php if ($line == 'dev') { echo 'block'; } else { echo 'none'; } ?>"></div>
                                    <div><span class="seatsLeft"><?php echo lang('Only '); ?><span id="seatsLeft"></span><?php echo lang(' seats left!'); ?></span></div>
                                    <div class="action">
                                        <a href="javascript:void(0);" class="button btn-small full-width gobutton"></a>
                                        <div id="loadingseat" style="display:none;"><i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i> <span class="sr-only">Loading...</span></div>
                                        <div class="red-color" id="nomoreseat" style="display:none;"><?php echo lang('很抱歉，位置售罄！'); ?></div>
                                    </div>
                                </div>
                                -->
                                </div>
                                <div class="panel-collapse collapse in" id="ft_item_template_return" style="height: auto;">
                                    <div class="panel-content ">
                                        <hr />
                                        <div class="details-wrapper">
                                            <h4 id="mark-arrival" style="display:none"><?php echo lang('回程'); ?></h4>
                                            <div class="first-row">
                                                <div><img alt="" class="airimg" src="assets/images/mail-logo.png" /><!--<a class="button btn-mini"><span id="airCompany"></span></a>--></div>
                                                <div class="time">
                                                    <h4 class="box-title col-sm-4">
                                                        <div class="icon"><i class="soap-icon-plane-right takeoff-effect"></i></div>
                                                        <span id="DepartureDate"></span>, <span id="DepartureTime"></span>
                                                    </h4>
                                                    <h4 class="box-title col-sm-4">
                                                        <div class="icon"><i class="soap-icon-plane-right landing-effect"></i></div>
                                                        <span id="ArrivalDate"></span>, <span id="ArrivalTime"></span>
                                                    </h4>
                                                    <h4 class="box-title">
                                                        <div class="icon"><i class="soap-icon-clock"></i></div>
                                                        <span id="ElapsedTimeTotalHours"></span><?php echo lang('小时'); ?><span id="ElapsedTimeTotalHoursLeftMins"></span><?php echo lang('分钟'); ?>
                                                    </h4>
                                                </div>
                                                <div>&nbsp;</div>
                                            </div>
                                            <div class="second-row">
                                                <div><span id="FlightName"></span></div>
                                                <div><a class="button btn-mini"><span id="stops"></span></a> <span id="stopcitylist"></span><a class="soap-popupbox" href="#flightDetail" onload="loadflightdetail">&nbsp;&nbsp;&nbsp;<?php echo lang('查看详情'); ?><i class="fa fa-caret-down"></i></a></div>
                                                <div class="action">
                                                    <a href="javascript:void(0);" class="button btn-small full-width orderbutton" data="" data-from="" href=""><?php echo lang('预定'); ?></a>
                                                    <div id="loadingseat" style="display:none;"><i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i> <span class="sr-only">Loading...</span></div>
                                                    <div class="red-color" id="nomoreseat" style="display:none;"><?php echo lang('很抱歉，位置售罄！'); ?></div>
                                                </div>
                                            </div>
                                        <!--
                                        <div class="third-row">
                                            <div><img id="provider" src="assets/images/sabre.jpg" style="display: <?php if ($line == 'dev') { echo 'block'; } else { echo 'none'; } ?>"></div>
                                            <div><span class="seatsLeft"><?php echo lang('Only '); ?><span id="seatsLeft"></span><?php echo lang(' seats left!'); ?></span></div>
                                            <div class="action"><a href="javascript:void(0);" class="button btn-small full-width orderbutton" data="" data-from="" href=""><?php echo lang('预定'); ?></a>
                                                <div id="loadingseat" style="display:none;"><i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i> <span class="sr-only">Loading...</span></div>
                                                <div class="red-color" id="nomoreseat" style="display:none;"><?php echo lang('很抱歉，位置售罄！'); ?></div>
                                            </div>
                                        </div>
                                        -->
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </article>
                    </div>
                    <!-- <a class="button uppercase full-width btn-large button-loading" style="display:none">load more listings</a> --></div>
                <div class="search-detail-box container" style="background-color: #fff;">
                    <div class="clearfix" style="padding: 0px 15px;">
                        <div class="row">
                            <?php if ($lang != 'Chinese') { ?>
                            <a href="https://air.sinorama.ca/index.php?id=search&srccity=YUL&destcity=SHA&tripType=roundtrip&departdate=2017-09-14&returndate=2017-10-22&adultcount=1&childrencount=0&infantsseatcount=0&infantslapcount=0&fullsite=yes"><img src="assets/images/tj<?php echo $languagesMap[$lang]; ?>.png" /></a>
                            <?php } else { ?>
                            <a href="javascript:void(0);"><img src="assets/images/7dcn.png?v=20170809" /></a>
                            <?php } ?>
                        </div>
                    </div>
                </div>
                <div class="search-detail-box container" style="background-color: #fff;">
                    <div class="clearfix" style="padding: 0px 15px;">
                        <div class="row">
                            <div class="col-xs-12" style="text-align: center;padding: 15px;background: #0a528e;color: #fff;font-size: 20px;font-weight: bold;">
                                <span><?php echo lang('OUR BEST DEALS'); ?></span>
                            </div>
                        </div>
                        <div style="margin-top: 20px;">
                            <ul class="nav nav-tabs" role="tablist">
                                <li role="presentation" class="active">
                                    <a href="#montreal" aria-controls="home" role="tab" data-toggle="tab" style="background-color: #eee;font-size: 16px;font-weight: bold;"><?php echo lang('Montreal'); ?></a>
                                </li>
                                <li role="presentation">
                                    <a href="#toronto" aria-controls="profile" role="tab" data-toggle="tab" style="background-color: #eee;font-size: 16px;font-weight: bold;"><?php echo lang('Toronto'); ?></a>
                                </li>
                                <li role="presentation">
                                    <a href="#vancouver" aria-controls="messages" role="tab" data-toggle="tab" style="background-color: #eee;font-size: 16px;font-weight: bold;"><?php echo lang('Vancouver'); ?></a>
                                </li>
                            </ul>
                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane active" id="montreal">
                                    <div class="row" style="margin: 5px 1px 0px 1px;font-size: 14px;padding: 15px 5px;border-bottom: 1px solid #dadada;background-color: #eee;">
                                        <div class="col-xs-2">
                                            <span><?php echo lang('FROM'); ?></span>
                                        </div>
                                        <div class="col-xs-2">
                                            <span><?php echo lang('TO'); ?></span>
                                        </div>
                                        <div class="col-xs-2">
                                            <span><?php echo lang('DEPARTING'); ?></span>
                                        </div>
                                        <div class="col-xs-2">
                                            <span><?php echo lang('RETURNING'); ?></span>
                                        </div>
                                        <div class="col-xs-2">
                                            <span><?php echo lang('AVG. PRICE'); ?></span>
                                        </div>
                                        <div class="col-xs-2">
                                            <span>&nbsp;</span>
                                        </div>
                                    </div>
                                    <?php if ($lang == 'Chinese') { ?>
                                    <div class="row redSpan" style="padding: 15px 5px;border-bottom: 1px solid #dadada;">
                                        <div class="col-xs-2" style="padding-top: 7px;">
                                            <span>蒙特利尔（加航）</span>
                                        </div>
                                        <div class="col-xs-2" style="padding-top: 7px;">
                                            <span> 北京/上海（加航）</span>
                                        </div>
                                        <div class="col-xs-2" style="padding-top: 7px;">
                                            <span>2017-08-24</span>
                                        </div>
                                        <div class="col-xs-2" style="padding-top: 7px;">
                                            <span>2018-06-14</span>
                                        </div>
                                        <div class="col-xs-2" style="padding-top: 7px;">
                                            <span class="price" style="float: left; text-align: left;font-size: 20px;">
                                                <span style="font-size: 14px;">C$</span><span>698</span>
                                            </span>
                                        </div>
                                        <div class="col-xs-2">
                                            <div class="col-xs-12">
                                                <a href="https://air.sinorama.ca/index.php?id=flighttickets&change_lang=Chinese&fullsite=yes" target="_blank" class="full-width" style="padding:10px 47px;background: #0a528e;color: #fff;text-transform: uppercase;font-weight: bold;cursor: pointer;">现在预订</a>
                                            </div>
                                        </div>
                                    </div>
                                    <?php } ?>
                                    <?php
                                    foreach ($recommandations_YUL as $recommandation_YUL) {
                                    ?>
                                    <div class="row <?php if ($recommandation_YUL['to']['code'] == 'PEK' || $recommandation_YUL['to']['code'] == 'PVG') { echo 'redSpan'; } ?>" style="padding: 15px 5px;border-bottom: 1px solid #dadada;">
                                        <div class="col-xs-2" style="padding-top: 7px;">
                                            <span><?php echo lang($recommandation_YUL['from']['desc']); ?></span>
                                        </div>
                                        <div class="col-xs-2" style="padding-top: 7px;">
                                            <span><?php echo lang($recommandation_YUL['to']['desc']); ?></span>
                                        </div>
                                        <div class="col-xs-2" style="padding-top: 7px;">
                                            <span><?php echo $recommandation_YUL['departing']; ?></span>
                                        </div>
                                        <div class="col-xs-2" style="padding-top: 7px;">
                                            <span><?php echo $recommandation_YUL['returning']; ?></span>
                                        </div>
                                        <div class="col-xs-2" style="padding-top: 7px;">
                                            <span class="price" style="float: left; text-align: left;font-size: 20px;">
                                                <span style="font-size: 14px;">C$</span><span><?php echo $recommandation_YUL['price']; ?></span>
                                            </span>
                                        </div>
                                        <div class="col-xs-2">
                                            <div class="col-xs-12">
                                                <button class="bookit-button full-width" style="background: #0a528e;color: #fff;text-transform: uppercase;font-weight: bold;cursor: pointer;" data-from="<?php echo lang($recommandation_YUL['from']['code']); ?>" data-to="<?php echo lang($recommandation_YUL['to']['code']); ?>" data-departing="<?php echo $recommandation_YUL['departing']; ?>" data-returning="<?php echo $recommandation_YUL['returning']; ?>"><?php echo lang('Book It'); ?></button>
                                            </div>
                                        </div>
                                    </div>
                                    <?php
                                    }
                                    ?>
                                </div>
                                <div role="tabpanel" class="tab-pane" id="toronto">
                                    <div class="row" style="margin: 5px 1px 0px 1px;font-size: 14px;padding: 15px 5px;border-bottom: 1px solid #dadada;background-color: #eee;">
                                        <div class="col-xs-2">
                                            <span><?php echo lang('FROM'); ?></span>
                                        </div>
                                        <div class="col-xs-2">
                                            <span><?php echo lang('TO'); ?></span>
                                        </div>
                                        <div class="col-xs-2">
                                            <span><?php echo lang('DEPARTING'); ?></span>
                                        </div>
                                        <div class="col-xs-2">
                                            <span><?php echo lang('RETURNING'); ?></span>
                                        </div>
                                        <div class="col-xs-2">
                                            <span><?php echo lang('AVG. PRICE'); ?></span>
                                        </div>
                                        <div class="col-xs-2">
                                            <span>&nbsp;</span>
                                        </div>
                                    </div>
                                    <?php
                                    foreach ($recommandations_YYZ as $recommandation_YYZ) {
                                    ?>
                                    <div class="row" style="padding: 15px 5px;border-bottom: 1px solid #dadada;">
                                        <div class="col-xs-2" style="padding-top: 7px;">
                                            <span><?php echo lang($recommandation_YYZ['from']['desc']); ?></span>
                                        </div>
                                        <div class="col-xs-2" style="padding-top: 7px;">
                                            <span><?php echo lang($recommandation_YYZ['to']['desc']); ?></span>
                                        </div>
                                        <div class="col-xs-2" style="padding-top: 7px;">
                                            <span><?php echo $recommandation_YYZ['departing']; ?></span>
                                        </div>
                                        <div class="col-xs-2" style="padding-top: 7px;">
                                            <span><?php echo $recommandation_YYZ['returning']; ?></span>
                                        </div>
                                        <div class="col-xs-2" style="padding-top: 7px;">
                                            <span class="price" style="float: left; text-align: left;font-size: 20px;">
                                                <span style="font-size: 14px;">C$</span><span><?php echo $recommandation_YYZ['price']; ?></span>
                                            </span>
                                        </div>
                                        <div class="col-xs-2">
                                            <div class="col-xs-12">
                                                <button class="bookit-button full-width" style="background: #0a528e;color: #fff;text-transform: uppercase;font-weight: bold;cursor: pointer;" data-from="<?php echo lang($recommandation_YYZ['from']['code']); ?>" data-to="<?php echo lang($recommandation_YYZ['to']['code']); ?>" data-departing="<?php echo $recommandation_YYZ['departing']; ?>" data-returning="<?php echo $recommandation_YYZ['returning']; ?>"><?php echo lang('Book It'); ?></button>
                                            </div>
                                        </div>
                                    </div>
                                    <?php
                                    }
                                    ?>
                                </div>
                                <div role="tabpanel" class="tab-pane" id="vancouver">
                                    <div class="row" style="margin: 5px 1px 0px 1px;font-size: 14px;padding: 15px 5px;border-bottom: 1px solid #dadada;background-color: #eee;">
                                        <div class="col-xs-2">
                                            <span><?php echo lang('FROM'); ?></span>
                                        </div>
                                        <div class="col-xs-2">
                                            <span><?php echo lang('TO'); ?></span>
                                        </div>
                                        <div class="col-xs-2">
                                            <span><?php echo lang('DEPARTING'); ?></span>
                                        </div>
                                        <div class="col-xs-2">
                                            <span><?php echo lang('RETURNING'); ?></span>
                                        </div>
                                        <div class="col-xs-2">
                                            <span><?php echo lang('AVG. PRICE'); ?></span>
                                        </div>
                                        <div class="col-xs-2">
                                            <span>&nbsp;</span>
                                        </div>
                                    </div>
                                    <?php
                                    foreach ($recommandations_YVR as $recommandation_YVR) {
                                    ?>
                                    <div class="row" style="padding: 15px 5px;border-bottom: 1px solid #dadada;">
                                        <div class="col-xs-2" style="padding-top: 7px;">
                                            <span><?php echo lang($recommandation_YVR['from']['desc']); ?></span>
                                        </div>
                                        <div class="col-xs-2" style="padding-top: 7px;">
                                            <span><?php echo lang($recommandation_YVR['to']['desc']); ?></span>
                                        </div>
                                        <div class="col-xs-2" style="padding-top: 7px;">
                                            <span><?php echo $recommandation_YVR['departing']; ?></span>
                                        </div>
                                        <div class="col-xs-2" style="padding-top: 7px;">
                                            <span><?php echo $recommandation_YVR['returning']; ?></span>
                                        </div>
                                        <div class="col-xs-2" style="padding-top: 7px;">
                                            <span class="price" style="float: left; text-align: left;font-size: 20px;">
                                                <span style="font-size: 14px;">C$</span><span><?php echo $recommandation_YVR['price']; ?></span>
                                            </span>
                                        </div>
                                        <div class="col-xs-2">
                                            <div class="col-xs-12">
                                                <button class="bookit-button full-width" style="background: #0a528e;color: #fff;text-transform: uppercase;font-weight: bold;cursor: pointer;" data-from="<?php echo lang($recommandation_YVR['from']['code']); ?>" data-to="<?php echo lang($recommandation_YVR['to']['code']); ?>" data-departing="<?php echo $recommandation_YVR['departing']; ?>" data-returning="<?php echo $recommandation_YVR['returning']; ?>"><?php echo lang('Book It'); ?></button>
                                            </div>
                                        </div>
                                    </div>
                                    <?php
                                    }
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- soldflight+calander -->
            <!--
			<div class="search-detail-box container">
				<div class="row">
					<div class="col-md-3">
						<h5><?php echo lang('机票大数据'); ?></h5>
						<ul class="salelist">
							<li class="active" data="YUL" id="YUL"><?php echo lang('蒙特利尔——中国'); ?><i class="fa fa-caret-right"></i></li>
							<li data="YYZ" id="YYZ"><?php echo lang('多伦多——中国'); ?><i class="fa fa-caret-right" style="display:none;"></i></li>
							<li data="YVR" id="YVR"><?php echo lang('温哥华——中国'); ?><i class="fa fa-caret-right" style="display:none;"></i></li>
						</ul>
					</div>
					<div class="col-md-9">
						<div id="hotel-main-content">
							<p><font color="red">*</font><?php echo lang('以下价格仅供分析参考，非最终机票售价。具体价格请以预定过程报价为准。'); ?></p>
							<div class="row" style="display:flex;">
								<div class="selector col-md-4"><select class="full-width" id="select-month"> </select></div>
								<div class="selector col-md-4"><select class="full-width" id="select-departurecity" readonly="readonly"><option value="YUL"><?php echo lang('蒙特利尔'); ?></option><option value="YYZ"><?php echo lang('多伦多'); ?></option><option value="YVR"><?php echo lang('温哥华'); ?></option></select></div>
								<div class="selector col-md-4"><select class="full-width" id="select-arrivalcity"><option value="PEK"><?php echo lang('北京'); ?></option><option value="PVG"><?php echo lang('上海'); ?></option><option value="HKG"><?php echo lang('香港'); ?></option></select></div>
								<div class="selector col-md-4"><select class="full-width" id="select-time"><option value="14"><?php echo lang('两星期（14天）往返'); ?></option><option value="30"><?php echo lang('一个月（30天）往返'); ?></option><option value="90"><?php echo lang('三个月（90天）往返'); ?></option><option value="180"><?php echo lang('六个月（180天）往返'); ?></option></select></div>
							</div>
							<h6 style="margin: 30px 0 0 0;"><?php echo lang('按出发月份价格走势图分析'); ?> <select id="chardatatype"><option value="min"><?php echo lang('最低价格'); ?></option><option value="avg"><?php echo lang('平均价格'); ?></option><option value="max"><?php echo lang('最高价格'); ?></option></select></h6>
							<div class="row" id="price_chart_list">&nbsp;</div>
							<div class="row" id="price_table_list">&nbsp;</div>
						</div>
					</div>
				</div>
			</div>
			-->
            </div>
        </div>
    </section>
    <form action="{{route("booking")}}?" method="post" name="orderform">
        {{csrf_field()}}
        <input name="request" type="hidden" /> <input name="personcount" type="hidden" />
    </form>

@endsection



@section('extrascript')


@endsection
