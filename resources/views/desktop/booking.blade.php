

@extends('desktop.layouts.mainframe')


@section('extracss')
    <style>
        .ui-datepicker .ui-datepicker-prev{
            display:none
        }
        .ui-datepicker .ui-datepicker-next{
            display:none
        }
    </style>
@endsection

@section('content')
    <section id="content">
        <div class="container">
            <div class="row" style="position: relative;">
                <div class="sort-by-section clearfix box progress-bar-box">
                    <div id="main" class="col-sm-12 col-md-12">
                        <div class="progress-bar">
                            <div class="fill" style="width: 30%;"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="booking-content" class="row" style="position: relative;display: none;">
                <div id="main" class="col-sm-8 col-md-8">
                    <div class="booking-section travelo-box">
                        <form id="bookingform" class="booking-form">
                            <div class="explanation">
                                <font color="red" >*</font><?php echo lang('请阅读《'); ?><a href="#flightDetail" onload="loadtermsonly" class="soap-popupbox"><?php echo lang('订票须知'); ?></a>》。<br />
                                <font color="red" >*</font><?php echo lang('请确保您所填写的信息和您的旅行证件一致。'); ?><br />
                                <font color="red" >*</font><?php echo lang('以下信息如果没有特殊说明，均为必填项目。'); ?>
                            </div>
                            <div class="person-information" person-index="1" person-type="ADT">
                                <h3><?php echo lang('乘客'); ?></h3>
                                <div class="form-group row">
                                    <div class="col-sm-6 col-md-4">
                                        <label for="firstname" placehold="<?php echo lang('名字'); ?>"></label>
                                        <input id="firstname" type="text" class="input-text full-width" required value="" placeholder="" />
                                    </div>
                                    <div class="col-sm-6 col-md-4">
                                        <label for="middlename" placehold="<?php echo lang('中间名'); ?>"></label>
                                        <input id="middlename" type="text" class="input-text full-width" value="" placeholder="<?php echo lang('不是必填项目'); ?>" />
                                    </div>
                                    <div class="col-sm-6 col-md-4">
                                        <label for="lastname" placehold="<?php echo lang('姓'); ?>"></label>
                                        <input id="lastname" type="text" class="input-text full-width" required value="" placeholder="" />
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-6 col-md-4">
                                        <label for="gender" placehold="<?php echo lang('乘客性别'); ?>"></label>
                                        <input required id="gender" required name="gender" value="M" type="radio" ng-model="contact.gender" class="ng-pristine ng-untouched ng-valid"><?php echo lang('男'); ?>　　
                                        <input required name="gender" value="F" type="radio" ng-model="contact.gender" class="ng-pristine ng-untouched ng-valid"><?php echo lang('女'); ?>
                                    </div>
                                    <div class="col-sm-6 col-md-4">
                                        <label for="birthday_1" placehold="<?php echo lang('出生日期'); ?>"></label>

                                        <div class="datepicker-wrap">
                                            <input vid="birthday" id="birthday_1" required type="text" class="input-text full-width " picker-type="birthday" date-type="date" placeholder="<?php echo lang('年-月-日'); ?>" />
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-md-4">
                                        <label for="nationality_1" placehold="<?php echo lang('国籍'); ?>"></label>
                                        <div id="multiple-datasets" style="position:relative;">
                                            <a class="typeahead-close" href="javascript:void(0);"><i class="soap-icon-close" style="font-size:24px;color:#ccc;position:absolute;right:5px;top:0;z-index:999"></i></a>
                                            <input vid="nationality" id="nationality_1" type="text" class="input-text full-width" data="" required value="" placeholder="" />
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-6 col-md-4">
                                        <label for="passport" placehold="<?php echo lang('护照号码'); ?>"></label>
                                        <input id="passport" type="text" class="input-text full-width" required value="" placeholder="" />
                                    </div>
                                    <div class="col-sm-6 col-md-4">
                                        <label for="passportexpired_1" placehold="<?php echo lang('护照有效期'); ?>"></label>
                                        <div class="datepicker-wrap" >
                                            <input vid="passportexpired" id="passportexpired_1" type="text" required class="input-text full-width" picker-type="birthday" min-date=0 max-date=30 date-type="date" placeholder="<?php echo lang('年-月-日'); ?>" />
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-md-4">
                                        <label for="passportcountry_1" placehold="<?php echo lang('护照签发国家'); ?>"></label>
                                        <div id="multiple-datasets" style="position:relative;"><a class="typeahead-close" href="javascript:void(0);"><i class="soap-icon-close" style="font-size:24px;color:#ccc;position:absolute;right:5px;top:0;z-index:1"></i></a> <input vid="passportcountry" id="passportcountry_1" type="text" class="input-text full-width" data="" required value="" placeholder="" /></div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-12 col-md-12">
                                        <a id="langjudge" style="display: none;"><?php echo lang('单程'); ?></a>
                                        <a id="showFlyerMealSSR" href="javascript:void(0);" style="color: #006699;"><?php echo lang('Frequent Flyer & Meal Requests & Special assistance (optional)'); ?> <span class="fa fa-angle-down"></span></a>
                                    </div>
                                </div>
                                <div style="display: none;">
                                    <div id="flyermeal" class="form-group row">
                                        <div id="flyerplan" class="col-sm-6 col-md-4" style="display: none;margin-bottom: 0px;">
                                            <label for="flyernumber" placehold=""></label>
                                            <input id="flyernumber" type="text" class="input-text full-width" value="" placeholder="" />
                                        </div>
                                        <div class="col-sm-12 col-md-12">
                                            <span><?php echo lang('We share program details with the airline, though we cannot guarantee point awards.'); ?></span>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-sm-6 col-md-4">
                                            <label for="mealpreference" placehold="<?php echo lang('Meal Request'); ?>"></label>
                                            <div class="selector">
                                                <select id="mealpreference" class="full-width">
                                                    @include("desktop.layouts.ft.booking_meal_codes_" . $lang)
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-sm-6 col-md-6" style="margin-bottom: 0px;">
                                            <label for="ssr" placehold="<?php echo lang('Special assistance'); ?>"></label>
                                            <div class="selector">
                                                <select id="ssr" class="full-width">
                                                    @include("desktop.layouts.ft.booking_ssr_codes_" . $lang)
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-sm-12 col-md-12">
                                            <span><?php echo lang('Please contact the airline to confirm special assistance requests.'); ?></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <div class="person-information" person-index="contact">
                                <h3><?php echo lang('联系人信息'); ?></h3>
                                <div class="form-group row">
                                    <div class="col-sm-6 col-md-4">
                                        <label>&nbsp;</label>
                                        <div class="selector">
                                            <select id="contactindex" class="full-width">
                                                <option value="0"><?php echo lang('联系人不是乘客'); ?></option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-md-4">
                                        <label for="contactfirstname" placehold="<?php echo lang('名字'); ?>"></label>
                                        <input type="text" id="contactfirstname" class="input-text full-width" required value="" placeholder="" />
                                    </div>
                                    <div class="col-sm-6 col-md-4">
                                        <label for="contactlastname" placehold="<?php echo lang('姓'); ?>"></label>
                                        <input type="text" id="contactlastname" class="input-text full-width" required value="" placeholder="" />
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-6 col-md-4">
                                        <label for="email" placehold="<?php echo lang('电子邮箱(EMAIL)'); ?>"></label>
                                        <input type="text" id="email" class="input-text full-width" required value="" placeholder="" />
                                    </div>
                                    <div class="col-sm-6 col-md-4">
                                        <label for="callingCode" placehold="<?php echo lang('国家电话区号'); ?>"></label>
                                        <div id="multiple-datasets" style="position:relative;"><a class="typeahead-close" href="javascript:void(0);"><i class="soap-icon-close" style="font-size:24px;color:#ccc;position:absolute;right:5px;top:0;z-index:1"></i></a> <input vid="callingCode" id="callingCode" type="text" class="input-text full-width" data="" required value="" placeholder="" /></div>
                                    </div>
                                    <div class="col-sm-6 col-md-4">
                                        <label for="tel1" placehold="<?php echo lang('联系电话'); ?>"></label>
                                        <input type="text" id="tel1" class="input-text full-width" required value="" placeholder="" maxlength="15"/>
                                    </div>
                                    <div class="col-sm-6 col-md-4" style="display: none;">
                                        <label for="tel2" placehold="<?php echo lang('联系电话'); ?>2"></label>
                                        <input type="text" id="tel2" class="input-text full-width" required value="" placeholder="" />
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-6 col-md-8">
                                        <label for="address" placehold="<?php echo lang('账单地址'); ?>"></label>
                                        <input type="text" id="address" class="input-text full-width" required value="" placeholder="" />
                                    </div>
                                    <div class="col-sm-6 col-md-4">
                                        <label for="city" placehold="<?php echo lang('城市'); ?>"></label>
                                        <input type="text" id="city" class="input-text full-width" required value="" placeholder="" />
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-6 col-md-4">
                                        <label for="province" placehold="<?php echo lang('省/州'); ?>"></label>
                                        <input type="text" id="province" class="input-text full-width" required value="" placeholder="" />
                                    </div>
                                    <div class="col-sm-6 col-md-4">
                                        <label for="country" placehold="<?php echo lang('国家'); ?>"></label>
                                        <div id="multiple-datasets" style="position:relative;"><a class="typeahead-close" href="javascript:void(0);"><i class="soap-icon-close" style="font-size:24px;color:#ccc;position:absolute;right:5px;top:0;z-index:1"></i></a> <input vid="country" id="country" type="text" class="input-text full-width" data="" required value="" placeholder="" /></div>
                                    </div>
                                    <div class="col-sm-6 col-md-4">
                                        <label for="postcode" placehold="<?php echo lang('邮编'); ?>"></label>
                                        <input type="text" id="postcode" class="input-text full-width" required value="" placeholder="" />
                                    </div>
                                </div>
                            </div>
                            <div class="Terms and Conditions">
                                <div class="form-group row">
                                    <div class="col-sm-offset-1 col-sm-10 col-md-10">
                                        <div class="progress" id="booking-progress" style="display: none;">
                                            <div class="bootstrap-progress-bar progress-bar-striped active" role="progressbar"
                                                 aria-valuenow="5" aria-valuemin="0" aria-valuemax="100" style="width:5%">
                                                5%
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-md-5">
                                        <a href="#flightDetail" onload="loaduserterms" onload="checkbookfrom" class="button btn-small full-width orderbutton soap-popupbox" style="display:none"><?php echo lang('提交订单并且支付'); ?></a>

                                        <a href="javascript:void(0);" id="submityorder" class="button btn-small full-width orderbutton" ><?php echo lang('提交订单'); ?></a>
                                        <div id="doingbookoing" style="display:none;">
                                            <i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i>
                                            <span class="sr-only">Loading...</span>
                                        </div>

                                        <a href="javascript:void(0);" id="modifyorder" class="button btn-small full-width orderbutton" style="display:none"><?php echo lang('修改信息'); ?></a>
                                    </div>
                                    <div id="book-submit-errors" class="col-sm-6 col-md-7 red-color">

                                    </div>
                                </div>
                            </div>
                        </form>
                        <div class="clearfix box information-box" style="display:none">
                            <div id="ticketFeeUpdated" class="alert alert-notice" style="display: none;"><?php echo lang('Price_Change'); ?></div>
                            <div id="ticketbooked" class="alert alert-notice"></div>
                        </div>
                        <div id="couponForm" style="display: none;">
                            <hr>
                            <h3><?php echo lang('优惠券'); ?></h3>
                            <div class="row">
                                <div class="col-sm-4 col-md-4"> <label for="couoncode" placehold="<?php echo lang('优惠券号'); ?>"></label><input type="text" id="couoncode" name="couoncode" class="input-text full-width" value="" placeholder="" data-stripe="number"/>

                                </div>
                                <div class="col-sm-3 col-md-3" style="margin-top:28px"><a href="javascript:void(0);" class="button btn-small full-width orderbutton" onclick="vlidateCoupon();"><?php echo lang('验证优惠券'); ?></a>
                                </div>
                                <div class="col-sm-5 col-md-5" style="margin-top:28px">
                                    <span id="coupon_info_success" style="color:green;display:none"><i class="fa fa-check"></i><?php echo lang('验证成功，可抵扣'); ?><span id="coupon_amt">CAD 50</span></span>
                                    <span id="coupon_info" style="display:none"><i class="fa fa-times"></i><?php echo lang('验证失败'); ?><span id="coupon_extra_error"></span></span>
                                </div>
                            </div>
                        </div>
                        <div id="payment" style="display: none;">
                            <hr>
                            <div class="search-tab-content" style="background:#fff">
                                <ul class="search-tabs clearfix">
                                    <li class="active"><a href="#payment-credit" data-toggle="tab"><?php echo lang('pay with credit card'); ?></a></li>
                                    <li><a href="#payment-debit" data-toggle="tab"><?php echo lang('pay in store(cash or debit card)'); ?></a></li>
                                </ul>
                                <div class="tab-pane fade active in" id="payment-credit">
                                    <div id="atos" style="display: <?php if ($currency == 'EUR') { echo 'block;'; } else { echo 'none;'; } ?>">
                                        <iframe id="atosIframe" src="" width="100%"></iframe>
                                        <div>
                                            <span id="paymentmsg" class="red-color"></span>
                                        </div>
                                    </div>
                                    <div id="go_to_payment" style="display: <?php if ($currency != 'EUR') { echo 'block;'; } else { echo 'none;'; } ?>">
                                        <div class="card-information" >
                                        <!--<h3><?php echo lang('支付信息'); ?></h3>-->
                                            <h4 class="credit-payment" style="margin-top: 0px;"><img src="assets/images/visa.png" style="margin-left:0px;"/><img src="assets/images/mastercard.png" /><img src="assets/images/ae.png" /></h4>
                                            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                                                <?php if ($currency == 'CADUSDEUR') { ?>
                                                <div class="panel panel-default">
                                                    <div class="panel-heading" role="tab" id="headingOne">
                                                        <h4 class="panel-title">
                                                            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne" id="airlineSystem">
                                                                <?php echo lang('Charged By Airline System'); ?>
                                                            </a>
                                                        </h4>
                                                    </div>
                                                    <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                                                        <div class="panel-body">
                                                            <form id="ASpaymentform" class="booking-form">
                                                                <div class="row">
                                                                    <div class="col-sm-6 col-md-6">
                                                                        <div class="form-group row">
                                                                            <div class="col-sm-12 col-md-12">
                                                                                <label for="cardnumber" placehold="<?php echo lang('信用卡号'); ?>"></label>
                                                                                <input type="text" id="cardnumber" name="number" class="input-text full-width" required value="" placeholder="" data-stripe="number"/>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group row">
                                                                            <div class="col-sm-12 col-md-12">
                                                                                <label for="cardname" placehold="<?php echo lang('持卡人姓名'); ?>"></label>
                                                                                <input type="text" id="cardname" name="name" class="input-text full-width" required value="" placeholder="" />
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group row">
                                                                            <div class="col-sm-6 col-md-6">

                                                                                <label for="seccode" placehold="<?php echo lang('安全号(CVC)'); ?>"></label>
                                                                                <input type="text" id="seccode" class="input-text full-width" required value="" placeholder="" name="cvc" data-stripe="cvc" />
                                                                            </div>
                                                                            <div class="col-sm-6 col-md-6">
                                                                                <label for="expireddate" placehold="<?php echo lang('有效期'); ?>"></label>
                                                                                <input type="text" id="expireddate" class="input-text full-width" required value="" placeholder="MM/YY" name="expiry" />
                                                                                <input type="hidden" data-stripe="exp-month" />
                                                                                <input type="hidden" data-stripe="exp-year" />
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-sm-6 col-md-6">
                                                                        <div id="AS-card-wrapper" class="card-wrapper" style="padding-top:20px;"></div>
                                                                    </div>
                                                                </div>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                                <?php } ?>
                                                <div class="panel panel-default">
                                                    <div class="panel-heading" role="tab" id="headingTwo">
                                                        <h4 class="panel-title">
                                                        <!--
                                                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="<?php if ($currency == 'CADUSDEUR') { echo 'false'; } else { echo 'true'; } ?>" aria-controls="collapseTwo" id="stripe">
                                                            <?php echo lang('Pay by Stripe'); ?>
                                                                </a>
-->
                                                            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="<?php if ($currency == 'CADUSDEUR') { echo 'false'; } else { echo 'true'; } ?>" aria-controls="collapseTwo" id="stripe">
                                                                <?php echo lang('Pay by Stripe'); ?>
                                                            </a>
                                                        </h4>
                                                    </div>
                                                    <div id="collapseTwo" class="panel-collapse collapse <?php if ($currency != 'CADUSDEUR') { echo 'in'; } ?>" role="tabpanel" aria-labelledby="headingTwo">
                                                        <div class="panel-body">
                                                            <div id="interchangefeeNotice" style="padding-bottom: 15px;font-size: 14px;color: #c30d23;"><?php echo lang('我们需要额外收取3%的手续费。'); ?></div>
                                                            <form id="stripepaymentform" class="booking-form">
                                                                <div class="row">
                                                                    <div class="col-sm-6 col-md-6">
                                                                        <div class="form-group row">
                                                                            <div class="col-sm-12 col-md-12">
                                                                                <label for="cardnumber" placehold="<?php echo lang('信用卡号'); ?>"></label>
                                                                                <input type="text" id="cardnumber" name="number" class="input-text full-width" required value="" placeholder="" data-stripe="number"/>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group row">
                                                                            <div class="col-sm-12 col-md-12">
                                                                                <label for="cardname" placehold="<?php echo lang('持卡人姓名'); ?>"></label>
                                                                                <input type="text" id="cardname" name="name" class="input-text full-width" required value="" placeholder="" />
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group row">
                                                                            <div class="col-sm-6 col-md-6">

                                                                                <label for="seccode" placehold="<?php echo lang('安全号(CVC)'); ?>"></label>
                                                                                <input type="text" id="seccode" class="input-text full-width" required value="" placeholder="" name="cvc" data-stripe="cvc" />
                                                                            </div>
                                                                            <div class="col-sm-6 col-md-6">
                                                                                <label for="expireddate" placehold="<?php echo lang('有效期'); ?>"></label>
                                                                                <input type="text" id="expireddate" class="input-text full-width" required value="" placeholder="MM/YY" name="expiry" />
                                                                                <input type="hidden" data-stripe="exp-month" />
                                                                                <input type="hidden" data-stripe="exp-year" />
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-sm-6 col-md-6">
                                                                        <div id="stripe-card-wrapper" class="card-wrapper" style="padding-top:20px;"></div>
                                                                    </div>
                                                                </div>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="Terms and Conditions">
                                            <div class="form-group row">
                                                <div class="col-sm-offset-1 col-sm-10 col-md-10">
                                                    <div class="progress" id="payment-progress" style="display: none;">
                                                        <div class="bootstrap-progress-bar progress-bar-striped active" role="progressbar"
                                                             aria-valuenow="5" aria-valuemin="0" aria-valuemax="100" style="width:5%">
                                                            5%
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6 col-md-5">

                                                    <a href="javascript:void(0);" id="submitorder" class="button btn-small full-width orderbutton"><?php echo lang('提交支付'); ?></a>
                                                    <div id="doingpayment" style="display:none;">
                                                        <i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i>
                                                        <span class="sr-only">Loading...</span>
                                                    </div>
                                                </div>

                                                <div id="submit-errors" class="col-sm-6 col-md-7 red-color">

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="tab-pane fade" id="payment-debit">
                                    <div id="go_to_payment">
                                        <ol class="storeinfo"  type="1">
                                            <li><a href="javascript:void(0);" id="printbookingreceipt" style="margin-left: 0px;"><?php echo lang('点击'); ?><?php echo lang('这里'); ?><?php echo lang('打印预留位置收据'); ?>,</a> <?php echo lang('或者'); ?><a href="javascript:void(0);" id="emailbookingconfirmation" style="margin-left: 0px;"><?php echo lang('点击'); ?><?php echo lang('这里'); ?><?php echo lang('发送到您的邮箱'); ?></a></li>
                                            <li><?php echo lang('持收据到我们门店支付机票费用，门店地址如下'); ?>：
                                                <ul>
                                                    <li>Montréal： 998 Boulevard Saint-Laurent,suite 518 Montréal, Québec H2Z 9Y9</li>
                                                    <li>Scheduel： Mon. - Fri. 10:00 - 18:00 / Sat. - Sun. 10:00 - 17:00</li>
                                                    <li>Telephone： 1-888-579-5519</li>
                                                </ul>
                                                <hr>
                                                <ul>
                                                    <li>Toronto: 998 Boulevard Saint-Laurent,suite 518 Montréal, Québec H2Z 9Y9</li>
                                                    <li>Scheduel： Mon. - Fri. 10:00 - 18:00 / Sat. - Sun. 10:00 - 17:00</li>
                                                    <li>Telephone： 1-888-579-5519</li>
                                                </ul>
                                                <hr>
                                                <ul>
                                                    <li>Vancouver: 998 Boulevard Saint-Laurent,suite 518 Montréal, Québec H2Z 9Y9</li>
                                                    <li>Scheduel： Mon. - Fri. 10:00 - 18:00 / Sat. - Sun. 10:00 - 17:00</li>
                                                    <li>Telephone： 1-888-579-5519</li>
                                                </ul>
                                            </li>
                                        </ol>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="bookingsuccess" class="col-sm-8 col-md-8" style="display:none;height:900px">
                    <div class="booking-information travelo-box">
                        <h3><?php echo lang('预定确认'); ?></h3>
                        <hr />
                        <div class="booking-confirmation clearfix">
                            <i class="soap-icon-recommend icon circle"></i>
                            <div class="message">
                                <h4 class="main-message"><?php echo lang('非常感谢您选择华景！您的预定已经成功提交！'); ?></h4>
                                <p><?php echo lang('我们已经发送一封确认邮件到您所填写的邮箱，您的电子客票也将随后发送到您的邮箱，请注意查收。再次感谢！'); ?>.</p>
                            </div>
                        </div>
                        <hr />
                        <h3><?php echo lang('预定信息'); ?><small><a href='javascript:void(0);' id="printbookingconfirmation"><?php echo lang('点击，打印预定确认单'); ?></a></small></h3>
                        <dl class="term-description">
                            <dt><?php echo lang('乘客信息号（PNR）：'); ?></dt><dd><span id="PNR"></span><span id="orderid" style="display: none"></span></dd>
                            <dt><?php echo lang('大人'); ?>：</dt><dd><span id="adultcount"></span><?php echo lang('人'); ?></dd>
                            <dt><?php echo lang('小孩'); ?>：</dt><dd><span id="childcount"></span><?php echo lang('人'); ?></dd>
                            <dt><?php echo lang('婴儿'); ?>：</dt><dd><span id="ifantcount"></span><?php echo lang('人'); ?></dd>
                        </dl>
                    </div>
                    <iframe id="printIframe" src="" width="0" height="0" style="display:none"></iframe>
                </div>
                <div id="bookingerror" class="col-sm-8 col-md-8" style="display:none;height:900px">
                    <div class="booking-information travelo-box">
                        <div class="booking-confirmation clearfix">
                            <img src="assets/images/error.jpg" />
                            <span id="extra_error" style="padding-left: 20px;color:red;"></span><a class="rechoose" href="javascript:void(0);" onclick="searchagain();" ><?php echo lang('重新选择'); ?></a>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4 col-md-4 siderbar" id="editInfo">
                    <div class="booking-details travelo-box">
                        <h4><?php echo lang('预定详情'); ?><small><a href="javascript:void(0);" onclick="searchagain();" ><?php echo lang('修改订单'); ?></a></small></h4>
                        <article class="flight-booking-details">
                            <figure class="clearfix">
                                <a href="javascript:void(0);"><img class="airimg" class="middle-item" alt="" src="http://placehold.it/75x75"></a>
                                <div class="travel-title">
                                    <h5 class="box-title"><span id="departureCity"></span><img src="assets/images/exchange-depart.jpg" /><span id="arriveCity"></span><small><span id="triptype"></span><br><br><span id="adultcount"></span><?php echo lang('大人，'); ?><span id="childcount"></span><?php echo lang('小孩，'); ?><span id="ifantcount"></span><?php echo lang('婴儿'); ?></small></h5>
                                    <!-- <a href="assets/flight-detailed.html" class="button">EDIT</a> -->
                                </div>
                            </figure>
                        </article>
                        <dl class="other-details">
                            <dt class="feature" id="langjudge1"><?php echo lang('机票费:'); ?></dt>
                            <dd class="value"><span id="basefare"></span></dd>
                            <dt class="feature"><?php echo lang('税和其它费用:'); ?></dt>
                            <dd class="value"><span id="taxfee"></span></dd>
                            <dt class="feature"><?php echo lang('信用卡手续费:'); ?></dt>
                            <dd class="value"><span id="interchangefee">C$0.00</span></dd>
                            <dt class="total-price"><?php echo lang('总费用'); ?></dt>
                            <dd class="total-price-value"><span id="totalfare"></span></dd>
                        </dl>
                        <article></article>
                        <h4><?php echo lang('行程'); ?><small><a href="javascript:void(0);" onclick="searchagain();" ><?php echo lang('修改行程'); ?></a></small><small><a href="#flightDetail" onload="loadflightdetail" class="soap-popupbox" ><?php echo lang('查看详细行程'); ?></a></small></h4>
                        <article class="flight-booking-details">
                            <div class="details">
                                <div class="constant-column-3 timing clearfix" style="display:none;">
                                    <div class="check-in">
                                        <label></label>
                                        <span></span>
                                    </div>
                                    <div class="duration text-center">
                                        <i class="soap-icon-clock"></i>
                                        <span></span>
                                    </div>
                                    <div class="check-out">
                                        <label></label>
                                        <span></span>
                                    </div>
                                </div>
                            </div>
                        </article>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <div class="payment-opacity-overlay" style="display: none;">
        <div class="container">
            <div class="popup-wrapper">
                <div id="paymentoptions">
                    <a href="javascript:void(0);" class="button btn-small" id="paymentsuccess"><?php echo lang('支付成功'); ?></a>
                    <a href="javascript:void(0);" class="button btn-small" style="margin-left: 20px;" id="paymentfailed"><?php echo lang('遇到问题'); ?></a>
                </div>
            </div>
        </div>
    </div>
    <form id="searchagain" action="/flight_front/index.php?id=search&" method="post">
        <input type="hidden" name="srccity" />
        <input type="hidden" name="destcity" />
        <input type="hidden" name="departdate" />
        <input type="hidden" name="returndate" />
        <input type="hidden" name="adultcount" />
        <input type="hidden" name="childrencount" />
        <input type="hidden" name="infantsseatcount" />
        <input type="hidden" name="infantslapcount" />
        <input type="hidden" name="tripType" />
        <input type="hidden" name="PreferLevel" />
        <input type="hidden" name="preferredAirline" />
        <input type="hidden" name="segments" />
        <input type="hidden" name="noflash" value="yes" />
    </form>

@endsection



@section('extrascript')


@endsection
