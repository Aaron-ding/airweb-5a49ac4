<?php

global $lang_array;
$lang_array = array();

function lang($key) {
	global $lang_array;
	global $lang;
    if (isset($lang_array[$key])) {
		return $lang_array[$key];		
    } else {
		return $key;	
		
        
    }
}

function getLangPackage($lang)
{
	$langhash=array();
	$langhash["Chinese"]="zh_cn";
	$langhash["Chinese_traditional"]="zh_cn";
	$langhash["English"]="en";
	$langhash["French"]="fr";
	$langhash["Japanese"]="jp";
	$langhash["Spanish"]="sp";
	$langhash["Korean"]="kr";
	return $langhash[$lang];
}

function curPageURL() {

	$pageURL = 'http';
	if (isset($_SERVER["HTTPS"]) && $_SERVER["HTTPS"] == "on") {
		$pageURL .= "s";
	}
	$pageURL .= "://";
	if ($_SERVER["SERVER_PORT"] != "80") {
		$pageURL .= $_SERVER["SERVER_NAME"] . ":" . $_SERVER["SERVER_PORT"] . $_SERVER["REQUEST_URI"];
	} else {
		$pageURL .= $_SERVER["SERVER_NAME"] . $_SERVER["REQUEST_URI"];
	}

	if (substr($pageURL, -1) == '/') {
		$pageURL .= '?';
	}

	if (isset($_GET["change_lang"])){
		$removestring="&change_lang=" . $_GET["change_lang"] . "&";
		$tmpurl= str_replace($removestring,"",$pageURL);
		return $tmpurl;
	} elseif (isset($_GET["change_currency"])) {
		$removestring = "&change_currency=" . $_GET["change_currency"] . "&";
		$tmpurl = str_replace($removestring, "", $pageURL);
		return $tmpurl;
	} else {
		return $pageURL;
	}
}

function get_domain($url) {
    $pieces = parse_url($url);
    $domain = isset($pieces['host']) ? $pieces['host'] : '';
    if (preg_match('/(?P<domain>[a-z0-9][a-z0-9\-]{1,63}\.[a-z\.]{2,6})$/i', $domain, $regs)) {
        return $regs['domain'];
    }
    return false;
}

if(!isset($_SESSION)){
    session_start();
}

$lang = "English";
if (isset($_COOKIE["sinorama_user_language"])){
	$lang=$_COOKIE["sinorama_user_language"];
}
    if (isset($_SESSION["lang_flight"])){
	$lang=$_SESSION["lang_flight"];
}

if (isset($_REQUEST["change_lang"])) {
    $lang = $_REQUEST["change_lang"];
}
$_SESSION["lang_flight"]=$lang;

$currency = "CAD";

if (isset($_SESSION["currency"])){
	$currency=$_SESSION["currency"];
}

if (isset($_REQUEST["change_currency"])) {
	$currency = $_REQUEST["change_currency"];
}

$_SESSION["currency"]=$currency;

if (!isset($_SESSION["referer"]) && !empty($_SERVER["HTTP_REFERER"])) {
    $referer = parse_url($_SERVER["HTTP_REFERER"], PHP_URL_HOST);
    if ($referer) {
        $_SESSION["referer"] = $referer;
    }
}

require_once(getLangPackage($lang) . '.php');

