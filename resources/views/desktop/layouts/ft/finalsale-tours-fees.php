<?php

$singleFees = array(
    'CAD' => 1150,
    'USD' => 1150,
    'EUR' => 1150
);
$adultDeposits = array(
    'CAD' => 1200,
    'USD' => 1200,
    'EUR' => 1200
);
$childDeposits = array(
    'CAD' => 0,
    'USD' => 0,
    'EUR' => 0
);
$adultTourfees = array(
    'CAD' => array(
        'E' => 0,
        'S' => 399,
        'T' => 499
    ),
    'USD' => array(
        'E' => 0,
        'S' => 399,
        'T' => 499
    ),
    'EUR' => array(
        'E' => 0,
        'S' => 399,
        'T' => 499
    )
);
$childTourfees = array(
    'CAD' => array(
        'E' => 1999,
        'S' => 2498,
        'T' => 2598
    ),
    'USD' => array(
        'E' => 1999,
        'S' => 2498,
        'T' => 2598
    ),
    'EUR' => array(
        'E' => 1999,
        'S' => 2498,
        'T' => 2598
    )
);
$ticketsTipfees = array(
    'CAD' => 1067,
    'USD' => 1067,
    'EUR' => 750
);