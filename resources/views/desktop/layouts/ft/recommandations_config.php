<?php
$recommandations_YUL = array(
    array(
        'from' => array(
            'code' => 'YUL',
            'desc' => 'Montreal'
        ),
        'to' => array(
            'code' => 'PEK',
            'desc' => 'Beijing'
        ),
        'departing' => '2017-08-28',
        'returning' => '2017-10-16',
        'price' => '648'
    ),
    array(
        'from' => array(
            'code' => 'YUL',
            'desc' => 'Montreal'
        ),
        'to' => array(
            'code' => 'PVG',
            'desc' => 'Shanghai'
        ),
        'departing' => '2017-09-12',
        'returning' => '2017-10-16',
        'price' => '590'
    ),
    array(
        'from' => array(
            'code' => 'YUL',
            'desc' => 'Montreal'
        ),
        'to' => array(
            'code' => 'CAN',
            'desc' => 'Guangzhou'
        ),
        'departing' => '2017-09-11',
        'returning' => '2017-10-17',
        'price' => '788'
    ),
    array(
        'from' => array(
            'code' => 'YUL',
            'desc' => 'Montreal'
        ),
        'to' => array(
            'code' => 'HKG',
            'desc' => 'Hong Kong'
        ),
        'departing' => '2017-09-14',
        'returning' => '2017-10-18',
        'price' => '725'
    ),
    array(
        'from' => array(
            'code' => 'YUL',
            'desc' => 'Montreal'
        ),
        'to' => array(
            'code' => 'TPE',
            'desc' => 'Taipei'
        ),
        'departing' => '2017-10-11',
        'returning' => '2017-11-09',
        'price' => '806'
    ),
    array(
        'from' => array(
            'code' => 'YUL',
            'desc' => 'Montreal'
        ),
        'to' => array(
            'code' => 'BKK',
            'desc' => 'Bangkok'
        ),
        'departing' => '2017-09-11',
        'returning' => '2017-09-28',
        'price' => '912'
    ),
    array(
        'from' => array(
            'code' => 'YUL',
            'desc' => 'Montreal'
        ),
        'to' => array(
            'code' => 'SGN',
            'desc' => 'Ho Chi Minh'
        ),
        'departing' => '2017-09-11',
        'returning' => '2017-09-28',
        'price' => '902'
    ),
    array(
        'from' => array(
            'code' => 'YUL',
            'desc' => 'Montreal'
        ),
        'to' => array(
            'code' => 'SIN',
            'desc' => 'Singapore'
        ),
        'departing' => '2017-09-19',
        'returning' => '2017-09-28',
        'price' => '910'
    ),
    array(
        'from' => array(
            'code' => 'YUL',
            'desc' => 'Montreal'
        ),
        'to' => array(
            'code' => 'NRT',
            'desc' => 'Tokyo'
        ),
        'departing' => '2017-09-25',
        'returning' => '2017-10-04',
        'price' => '1278'
    ),
    array(
        'from' => array(
            'code' => 'YUL',
            'desc' => 'Montreal'
        ),
        'to' => array(
            'code' => 'ICN',
            'desc' => 'Seoul'
        ),
        'departing' => '2017-09-14',
        'returning' => '2017-09-22',
        'price' => '1123'
    ),
    array(
        'from' => array(
            'code' => 'YUL',
            'desc' => 'Montreal'
        ),
        'to' => array(
            'code' => 'JFK',
            'desc' => 'New York'
        ),
        'departing' => '2017-08-26',
        'returning' => '2017-08-30',
        'price' => '236'
    ),
    array(
        'from' => array(
            'code' => 'YUL',
            'desc' => 'Montreal'
        ),
        'to' => array(
            'code' => 'MCO',
            'desc' => 'Orlando'
        ),
        'departing' => '2017-09-12',
        'returning' => '2017-09-19',
        'price' => '266'
    ),
    array(
        'from' => array(
            'code' => 'YUL',
            'desc' => 'Montreal'
        ),
        'to' => array(
            'code' => 'LAX',
            'desc' => 'Los Angeles'
        ),
        'departing' => '2017-09-12',
        'returning' => '2017-09-19',
        'price' => '378'
    ),
    array(
        'from' => array(
            'code' => 'YUL',
            'desc' => 'Montreal'
        ),
        'to' => array(
            'code' => 'YVR',
            'desc' => 'Vancouver'
        ),
        'departing' => '2017-09-21',
        'returning' => '2017-10-05',
        'price' => '450'
    ),
    array(
        'from' => array(
            'code' => 'YUL',
            'desc' => 'Montreal'
        ),
        'to' => array(
            'code' => 'YYZ',
            'desc' => 'Toronto'
        ),
        'departing' => '2017-09-13',
        'returning' => '2017-09-21',
        'price' => '172'
    )
);

$recommandations_YYZ = array(
    array(
        'from' => array(
            'code' => 'YYZ',
            'desc' => 'Toronto'
        ),
        'to' => array(
            'code' => 'PEK',
            'desc' => 'Beijing'
        ),
        'departing' => '2017-09-11',
        'returning' => '2017-10-05',
        'price' => '725'
    ),
    array(
        'from' => array(
            'code' => 'YYZ',
            'desc' => 'Toronto'
        ),
        'to' => array(
            'code' => 'PVG',
            'desc' => 'Shanghai'
        ),
        'departing' => '2017-09-10',
        'returning' => '2017-10-05',
        'price' => '662'
    ),
    array(
        'from' => array(
            'code' => 'YYZ',
            'desc' => 'Toronto'
        ),
        'to' => array(
            'code' => 'CAN',
            'desc' => 'Guangzhou'
        ),
        'departing' => '2017-09-11',
        'returning' => '2017-10-17',
        'price' => '688'
    ),
    array(
        'from' => array(
            'code' => 'YYZ',
            'desc' => 'Toronto'
        ),
        'to' => array(
            'code' => 'HKG',
            'desc' => 'Hong Kong'
        ),
        'departing' => '2017-09-14',
        'returning' => '2017-10-18',
        'price' => '718'
    ),
    array(
        'from' => array(
            'code' => 'YYZ',
            'desc' => 'Toronto'
        ),
        'to' => array(
            'code' => 'TPE',
            'desc' => 'Taipei'
        ),
        'departing' => '2017-10-11',
        'returning' => '2017-11-09',
        'price' => '798'
    ),
    array(
        'from' => array(
            'code' => 'YYZ',
            'desc' => 'Toronto'
        ),
        'to' => array(
            'code' => 'BKK',
            'desc' => 'Bangkok'
        ),
        'departing' => '2017-09-11',
        'returning' => '2017-09-28',
        'price' => '1088'
    ),
    array(
        'from' => array(
            'code' => 'YYZ',
            'desc' => 'Toronto'
        ),
        'to' => array(
            'code' => 'SGN',
            'desc' => 'Ho Chi Minh'
        ),
        'departing' => '2017-09-11',
        'returning' => '2017-09-28',
        'price' => '878'
    ),
    array(
        'from' => array(
            'code' => 'YYZ',
            'desc' => 'Toronto'
        ),
        'to' => array(
            'code' => 'SIN',
            'desc' => 'Singapore'
        ),
        'departing' => '2017-09-19',
        'returning' => '2017-09-28',
        'price' => '958'
    ),
    array(
        'from' => array(
            'code' => 'YYZ',
            'desc' => 'Toronto'
        ),
        'to' => array(
            'code' => 'NRT',
            'desc' => 'Tokyo'
        ),
        'departing' => '2017-09-25',
        'returning' => '2017-10-04',
        'price' => '1148'
    ),
    array(
        'from' => array(
            'code' => 'YYZ',
            'desc' => 'Toronto'
        ),
        'to' => array(
            'code' => 'ICN',
            'desc' => 'Seoul'
        ),
        'departing' => '2017-09-14',
        'returning' => '2017-09-22',
        'price' => '888'
    ),
    array(
        'from' => array(
            'code' => 'YYZ',
            'desc' => 'Toronto'
        ),
        'to' => array(
            'code' => 'JFK',
            'desc' => 'New York'
        ),
        'departing' => '2017-08-26',
        'returning' => '2017-08-30',
        'price' => '248'
    ),
    array(
        'from' => array(
            'code' => 'YYZ',
            'desc' => 'Toronto'
        ),
        'to' => array(
            'code' => 'MCO',
            'desc' => 'Orlando'
        ),
        'departing' => '2017-09-12',
        'returning' => '2017-09-19',
        'price' => '262'
    ),
    array(
        'from' => array(
            'code' => 'YYZ',
            'desc' => 'Toronto'
        ),
        'to' => array(
            'code' => 'LAX',
            'desc' => 'Los Angeles'
        ),
        'departing' => '2017-09-12',
        'returning' => '2017-09-19',
        'price' => '368'
    ),
    array(
        'from' => array(
            'code' => 'YYZ',
            'desc' => 'Toronto'
        ),
        'to' => array(
            'code' => 'YVR',
            'desc' => 'Vancouver'
        ),
        'departing' => '2017-09-21',
        'returning' => '2017-10-05',
        'price' => '562'
    )
);

$recommandations_YVR = array(
    array(
        'from' => array(
            'code' => 'YVR',
            'desc' => 'Vancouver'
        ),
        'to' => array(
            'code' => 'PEK',
            'desc' => 'Beijing'
        ),
        'departing' => '2017-09-11',
        'returning' => '2017-10-05',
        'price' => '560'
    ),
    array(
        'from' => array(
            'code' => 'YVR',
            'desc' => 'Vancouver'
        ),
        'to' => array(
            'code' => 'PVG',
            'desc' => 'Shanghai'
        ),
        'departing' => '2017-09-10',
        'returning' => '2017-10-05',
        'price' => '562'
    ),
    array(
        'from' => array(
            'code' => 'YVR',
            'desc' => 'Vancouver'
        ),
        'to' => array(
            'code' => 'CAN',
            'desc' => 'Guangzhou'
        ),
        'departing' => '2017-09-11',
        'returning' => '2017-10-17',
        'price' => '562'
    ),
    array(
        'from' => array(
            'code' => 'YVR',
            'desc' => 'Vancouver'
        ),
        'to' => array(
            'code' => 'HKG',
            'desc' => 'Hong Kong'
        ),
        'departing' => '2017-09-14',
        'returning' => '2017-10-18',
        'price' => '528'
    ),
    array(
        'from' => array(
            'code' => 'YVR',
            'desc' => 'Vancouver'
        ),
        'to' => array(
            'code' => 'TPE',
            'desc' => 'Taipei'
        ),
        'departing' => '2017-10-11',
        'returning' => '2017-11-09',
        'price' => '578'
    ),
    array(
        'from' => array(
            'code' => 'YVR',
            'desc' => 'Vancouver'
        ),
        'to' => array(
            'code' => 'BKK',
            'desc' => 'Bangkok'
        ),
        'departing' => '2017-09-11',
        'returning' => '2017-09-28',
        'price' => '708'
    ),
    array(
        'from' => array(
            'code' => 'YVR',
            'desc' => 'Vancouver'
        ),
        'to' => array(
            'code' => 'SGN',
            'desc' => 'Ho Chi Minh'
        ),
        'departing' => '2017-09-11',
        'returning' => '2017-09-28',
        'price' => '748'
    ),
    array(
        'from' => array(
            'code' => 'YVR',
            'desc' => 'Vancouver'
        ),
        'to' => array(
            'code' => 'SIN',
            'desc' => 'Singapore'
        ),
        'departing' => '2017-09-19',
        'returning' => '2017-09-28',
        'price' => '688'
    ),
    array(
        'from' => array(
            'code' => 'YVR',
            'desc' => 'Vancouver'
        ),
        'to' => array(
            'code' => 'NRT',
            'desc' => 'Tokyo'
        ),
        'departing' => '2017-09-25',
        'returning' => '2017-10-04',
        'price' => '668'
    ),
    array(
        'from' => array(
            'code' => 'YVR',
            'desc' => 'Vancouver'
        ),
        'to' => array(
            'code' => 'ICN',
            'desc' => 'Seoul'
        ),
        'departing' => '2017-09-14',
        'returning' => '2017-09-22',
        'price' => '968'
    ),
    array(
        'from' => array(
            'code' => 'YVR',
            'desc' => 'Vancouver'
        ),
        'to' => array(
            'code' => 'JFK',
            'desc' => 'New York'
        ),
        'departing' => '2017-08-26',
        'returning' => '2017-08-30',
        'price' => '448'
    ),
    array(
        'from' => array(
            'code' => 'YVR',
            'desc' => 'Vancouver'
        ),
        'to' => array(
            'code' => 'MCO',
            'desc' => 'Orlando'
        ),
        'departing' => '2017-09-12',
        'returning' => '2017-09-19',
        'price' => '288'
    ),
    array(
        'from' => array(
            'code' => 'YVR',
            'desc' => 'Vancouver'
        ),
        'to' => array(
            'code' => 'LAX',
            'desc' => 'Los Angeles'
        ),
        'departing' => '2017-09-12',
        'returning' => '2017-09-19',
        'price' => '248'
    ),
    array(
        'from' => array(
            'code' => 'YVR',
            'desc' => 'Vancouver'
        ),
        'to' => array(
            'code' => 'YYZ',
            'desc' => 'Toronto'
        ),
        'departing' => '2017-09-13',
        'returning' => '2017-09-21',
        'price' => '478'
    )
);