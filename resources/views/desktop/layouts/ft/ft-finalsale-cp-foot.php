<script type="text/javascript">

var request = new Request();

tjq(document).ready(function () {

    tjq('#passengercount').change(function() {
        updateCoupons();
        updatePassengers();
    });

    tjq('#submityorder').click(function (e) {
        e.preventDefault();
        tjq('#book-submit-errors').html('');

        if (!tjq('#checkTerms').is(':checked')) {
            tjq('#book-submit-errors').html('<i class="fa fa-times"></i><?php echo lang("请阅读本次活动规则，并选择已阅读。") ?> ');
            return
        }

        passchecked = true;

        tjq.each(tjq('#bookingform').find("input,select"), function () {
            if (tjq(this).is('[required]') && tjq(this).is(':visible')) {

                var val = request.getValFromObj(tjq(this));
                var showerrorobject = tjq(this);
                if (tjq(this).attr('groupid') != undefined) {
                    showerrorobject = tjq('#' + tjq(this).attr('groupid'));
                }

                if (val == undefined || val == "" || val.length == 0) {
                    passchecked = false;
                    request.showRequired(showerrorobject);
                } else {
                    request.removeRequired(showerrorobject);
                }
            }
        });

        if (passchecked) {
            if (!request.isEmail(tjq('#email').val())) {
                passchecked = false;
                request.showRequired(tjq('#email'));
            } else {
                request.removeRequired(tjq('#email'));
            }
        }

        if (!passchecked) {
            tjq('#book-submit-errors').html('<i class="fa fa-times"></i><?php echo lang("输入信息有误，请检查!") ?> ');
            return;
        } else {
            tjq(this).prop('disabled', true);
            tjq(this).hide();
            tjq('#doingbookoing').show();

            var data = {};
            data['passport'] = tjq.trim(tjq('#passport').val());
            data['email'] = tjq.trim(tjq('#email').val());
            data['tel'] = tjq.trim(tjq('#tel').val());
            data['srccity'] = tjq.trim(tjq('#srccity').val());
            data['departdate'] = tjq.trim(tjq('#departdate').val());
            data['destcity'] = tjq.trim(tjq('#destcity').val());
            data['returndate'] = tjq.trim(tjq('#returndate').val());
            data['passengercount'] = tjq.trim(tjq('#passengercount').val());

            var coupons = [];
            var passengers = [];

            for (var i = 1; i <= data['passengercount']; i++) {
                coupons.push(tjq.trim(tjq('#coupon_'+i).val().toUpperCase()));

                var passenger = {};
                passenger['firstname'] = tjq.trim(tjq('#firstname_'+i).val());
                passenger['middlename'] = tjq.trim(tjq('#middlename_'+i).val());
                passenger['lastname'] = tjq.trim(tjq('#lastname_'+i).val());
                passenger['gender'] = tjq.trim(tjq('input[name=gender_'+i+']:checked').val());
                passenger['birthday'] = tjq.trim(tjq('#birthday_'+i).val());
                passenger['passport'] = tjq.trim(tjq('#passport_'+i).val());
                passenger['aeroplan'] = tjq.trim(tjq('#aeroplan_'+i).val());

                passengers.push(passenger);
            }

            data['coupons'] = coupons;
            data['passengers'] = passengers;

            FT_c_finalsale_redeemCoupons(data, callback);
        }
    });
});

function callback(result) {

    tjq('#submityorder').hide();
    tjq('#submityorder').prop('disabled', true);
    
    var msg = '';

    if (result == 'success') {
        tjq('#doingbookoing').hide();
        tjq('#book-submit-errors').html('<i class="fa fa-check"></i><?php echo lang("非常感谢您选择华景！您的兑换请求已经成功提交，稍后机票专员会联系您。") ?>');

        //window.location.href = 'index.php?id=ft-finalsale-order&orderId=' + orderid + '&fullsite=yes&change_lang=Chinese&';
    } else {
        tjq('#doingbookoing').hide();
        tjq('#submityorder').prop('disabled', false);
        tjq('#submityorder').show();
        
        if (result == 'verifyBuyerError') {
            msg = '<?php echo lang("购买人信息验证失败，请您确认购买人信息，谢谢。") ?>';
        } else if (result == 'verifyBuyerCouponsNoneError') {
            msg = '<?php echo lang("兑换请求失败，该购买人已无可用兑换券。") ?>';
        } else if (result == 'verifyBuyerCouponsNotEnoughError') {
            msg = '<?php echo lang("兑换请求失败，该购买人已无足够可用兑换券。") ?>';
        } else if (result == 'redeemedCouponsError') {
            msg = '<?php echo lang("兑换请求失败，兑换券信息错误。") ?>';
        } else {
            msg = '<?php echo lang("兑换请求失败，兑换券信息错误。") ?>';
        }

        tjq('#book-submit-errors').html('<i class="fa fa-times"></i>'+msg);
    }
}

function updateCoupons() {
    var passengercount = tjq('#passengercount').val();
    var couponsHtml = '';

    for (i = 1; i <= passengercount; i++) {
        couponsHtml += '<div class="col-sm-6 col-md-4"><label for="coupon_'+i+'" placehold="*兑换券码'+i+'">*兑换券码'+i+'</label>';
        couponsHtml += '<input class="input-text full-width" id="coupon_'+i+'" placeholder="" required="" type="text" value=""></div>';
    }

    tjq('#coupons').html('');
    tjq('#coupons').html(couponsHtml);
}

function updatePassengers() {
    var passengercount = tjq('#passengercount').val();
    var couponsHtml = '';

    for (i = 1; i <= passengercount; i++) {
        couponsHtml += '<div class="person-information"><h3>乘客'+i+'</h3>';
        couponsHtml += '<div class="form-group row">';
        couponsHtml += '<div class="col-sm-6 col-md-4">';
        couponsHtml += '<label for="firstname_'+i+'" placehold="*<?php echo lang('名字'); ?>">*<?php echo lang('名字'); ?></label>';
        couponsHtml += '<input class="input-text full-width" id="firstname_'+i+'" placeholder="" required="" type="text" value="" />';
        couponsHtml += '</div>';
        couponsHtml += '<div class="col-sm-6 col-md-4">';
        couponsHtml += '<label for="middlename_'+i+'" placehold="<?php echo lang('中间名'); ?>"><?php echo lang('中间名'); ?></label>';
        couponsHtml += '<input class="input-text full-width" id="middlename_'+i+'" placeholder="不是必填项目" type="text" value="" />';
        couponsHtml += '</div>';
        couponsHtml += '<div class="col-sm-6 col-md-4">';
        couponsHtml += '<label for="lastname_'+i+'" placehold="*<?php echo lang('姓'); ?>">*<?php echo lang('姓'); ?></label>';
        couponsHtml += '<input class="input-text full-width" id="lastname_'+i+'" placeholder="" required="" type="text" value="" />';
        couponsHtml += '</div>';
        couponsHtml += '</div>';

        couponsHtml += '<div class="form-group row">';
        couponsHtml += '<div class="col-sm-6 col-md-4">';
        couponsHtml += '<label for="gender_'+i+'" placehold="*<?php echo lang('乘客性别'); ?>">*<?php echo lang('乘客性别'); ?></label>';
        couponsHtml += '<input class="ng-pristine ng-untouched ng-valid" id="gender_'+i+'" name="gender_'+i+'" ng-model="contact.gender" required="" type="radio" value="M" />　男　　 <input class="ng-pristine ng-untouched ng-valid" name="gender_'+i+'" ng-model="contact.gender" required="" type="radio" value="F" />　女';
        couponsHtml += '</div>';
        couponsHtml += '<div class="col-sm-6 col-md-4">';
        couponsHtml += '<label for="birthday_'+i+'" placehold="*<?php echo lang('出生日期'); ?>">*<?php echo lang('出生日期'); ?></label>';
        couponsHtml += '<div class="datepicker-wrap"><input class="input-text full-width " date-type="date" id="birthday_'+i+'" picker-type="birthday" placeholder="年/月/日" required="" type="text" vid="birthday_'+i+'" /></div>';
        couponsHtml += '</div>';
        couponsHtml += '<div class="col-sm-6 col-md-4">';
        couponsHtml += '<label for="passport_'+i+'" placehold="*<?php echo lang('护照号码'); ?>">*<?php echo lang('护照号码'); ?></label>';
        couponsHtml += '<input class="input-text full-width" id="passport_'+i+'" placeholder="" required="" type="text" value="" />';
        couponsHtml += '</div>';
        couponsHtml += '</div>';

        couponsHtml += '<div class="form-group row">';
        couponsHtml += '<div class="col-sm-6 col-md-4">';
        couponsHtml += '<label for="aeroplan_'+i+'" placehold="*<?php echo lang('Aeroplan号码'); ?>">*<?php echo lang('Aeroplan号码'); ?></label>';
        couponsHtml += '<input class="input-text full-width" id="aeroplan_'+i+'" placeholder="" required="" type="text" value="" />';
        couponsHtml += '</div>';
        couponsHtml += '</div>';
        couponsHtml += '</div>';
    }

    tjq('#passengers').html('');
    tjq('#passengers').html(couponsHtml);

    bindDatepickers();
}

function bindDatepickers() {

    // datepicker
    tjq('.datepicker-wrap input').each(function() {
        var pickertype=tjq(this).attr('picker-type');
        switch(pickertype){
            case "birthday":
                var maxDate = tjq(this).attr("max-date");
                if (typeof maxDate == "undefined") {
                    maxDate = 0;
                }
                var minDate=tjq(this).attr("min-date");
                if (typeof minDate == "undefined") {
                    minDate = 100;
                }

                var start=new Date();
                start.setFullYear(parseInt(start.getFullYear())+parseInt(maxDate));


                tjq(this).datepicker({
                    showOn: 'both',
                    buttonImage: 'assets/images/icon/blank.png',
                    buttonText: '',
                    buttonImageOnly: false,
                    changeYear: true,
                    yearRange: "-"+minDate+":+"+maxDate,
                    changeMonth: true,
                    hideIfNoPrevNext: true,
                    /*showOtherMonths: true,*/
                    //numberOfMonths: [1, 2],
                    /*showButtonPanel: true,*/
                    dateFormat: "yy-mm-dd",
                    dayNamesMin: dayNamesMin,
                    monthNames: monthNames,
                    monthNamesShort: monthNamesShort,
                    dayNames: dayNames,
                    dayNamesShort: dayNamesShort,
                    prevText:'hello',
                    beforeShow: function(input, inst) {
                        var themeClass = tjq(input).parent().attr("class").replace("datepicker-wrap", "");
                        tjq('#ui-datepicker-div').attr("class", "");
                        tjq('#ui-datepicker-div').addClass("ui-datepicker ui-widget ui-widget-content ui-helper-clearfix ui-corner-all ");
                        tjq('#ui-datepicker-div').addClass(themeClass);

                    },
                    onSelect:function(dateText,inst) {
                        return true;
                    }


                });

                return;
            case "departure":
                initDepartureDate(this);
            default:
                return;
        };

    });
}
</script>