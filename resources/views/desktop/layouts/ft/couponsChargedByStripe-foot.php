<script src="assets/js/card.js"></script>
<script type="text/javascript" src="https://js.stripe.com/v2/"></script>
<script type="text/javascript">
var stripeErrors = {
    'EndTransactionLLSRQ': "<?php echo lang('processing_error'); ?>",
    'TravelItineraryReadRQ': "<?php echo lang('processing_error'); ?>",
    'CreditVerificationRQ': "<?php echo lang('CreditVerificationRQ_Failed'); ?>",
    'AddRemarkLLSRQ_Creditcard': "<?php echo lang('processing_error'); ?>",
    'invalid_coupon': "<?php echo lang('invalid_coupon'); ?>",
    'coupon_processing_error': "<?php echo lang('coupon_processing_error'); ?>",
    'invalid_number': "<?php echo lang('invalid_number'); ?>",
    'invalid_expiry_month': "<?php echo lang('invalid_expiry_month'); ?>",
    'invalid_expiry_year': "<?php echo lang('invalid_expiry_year'); ?>",
    'invalid_cvc': "<?php echo lang('invalid_cvc'); ?>",
    'invalid_swipe_data': "<?php echo lang('invalid_swipe_data'); ?>",
    'incorrect_number': "<?php echo lang('incorrect_number'); ?>",
    'expired_card': "<?php echo lang('expired_card'); ?>",
    'incorrect_cvc': "<?php echo lang('incorrect_cvc'); ?>",
    'incorrect_zip': "<?php echo lang('incorrect_zip'); ?>",
    'card_declined': "<?php echo lang('card_declined'); ?>",
    'missing': "<?php echo lang('missing'); ?>",
    'processing_error': "<?php echo lang('processing_error'); ?>"
};
var request = new Request();

tjq(document).ready(function () {

    var card = new Card({
        form: document.querySelector('form[id=payform]'),
        container: '.card-wrapper'
    });
    
    tjq('#paysubmit').click(function (e) {
        e.preventDefault();
        tjq('#book-submit-errors').html('');

        if (!tjq('#checkTerms').is(':checked')) {
            tjq('#book-submit-errors').html('<i class="fa fa-times"></i><?php echo lang("请检查待付款信息，并选择已检查。") ?> ');
            return
        }

        passchecked = true;

        tjq.each(tjq('#payform').find("input,select"), function () {
            if (tjq(this).is('[required]') && tjq(this).is(':visible')) {

                var val = request.getValFromObj(tjq(this));
                var showerrorobject = tjq(this);
                if (tjq(this).attr('groupid') != undefined) {
                    showerrorobject = tjq('#' + tjq(this).attr('groupid'));
                }

                if (val == undefined || val == "" || val.length == 0) {
                    passchecked = false;
                    request.showRequired(showerrorobject);
                } else {
                    request.removeRequired(showerrorobject);
                }
            }
        });

        if (!passchecked) {
            tjq('#book-submit-errors').html('<i class="fa fa-times"></i><?php echo lang("输入信息有误，请检查!") ?> ');
            return;
        } else {
            var expirelist = tjq('#expireddate').val().split("/");
            if (expirelist.length != 2) {
                request.showRequired(tjq('#expireddate'));
                return;
            }

            <?php if($GLOBALS["branch"]=="master"){ ?>
            Stripe.setPublishableKey('pk_live_B5SZw46G2GUaRIWOD1s1YjdW');
            <?php } ?>
            <?php if($GLOBALS["branch"]=="dev"){ ?>
            Stripe.setPublishableKey('pk_test_XEL2ac8zWkzRTSF9NQPfqJ49');
            <?php } ?>

            if (!Stripe.card.validateCardNumber(tjq('#cardnumber').val())) {
                tjq('#book-submit-errors').html('<i class="fa fa-times"></i>' + stripeErrors['invalid_number']);
                return;
            }

            if (!Stripe.card.validateExpiry(expirelist[0].trim(), expirelist[1].trim())) {
                tjq('#book-submit-errors').html('<i class="fa fa-times"></i><?php echo lang("有效期"); ?><?php echo lang("格式错误。"); ?> ');
                return;
            }

            if (!Stripe.card.validateCVC(tjq('#seccode').val())) {
                tjq('#book-submit-errors').html('<i class="fa fa-times"></i>' + stripeErrors['invalid_cvc']);
                return;
            }

            tjq('input[data-stripe=exp-month]').val(expirelist[0].trim());
            tjq('input[data-stripe=exp-year]').val(expirelist[1].trim());

            tjq(this).prop('disabled', true);
            tjq(this).hide();
            tjq('#doingbookoing').show();

            Stripe.card.createToken(tjq('#payform'), function stripeResponseHandler(status, response) {
                if (response.error) {
                    // Show the errors on the form
                    console.log(response.error.message);
                    tjq('#book-submit-errors').html('<i class="fa fa-times"></i><?php echo lang("您的信用卡验证不通过，请检查信用卡信息!") ?> ');
                    tjq('#doingbookoing').hide();
                    tjq('#submityorder').prop('disabled', false);
                    tjq('#submityorder').show();
                } else {
                    var data = {};
                    data['orderid'] = tjq('#orderId').val();
                    
                    var token = response.id;
                    data['stripeToken'] = token;

                    FT_c_finalsale_doBooking_chargeByStripe(data, successBooking);
                }
            });
        }
    });
});

function successBooking(data) {
    if (data[0] != 'error') {
        tjq('#doingbookoing').hide();
        tjq('#book-submit-errors').html('<i class="fa fa-check"></i><?php echo lang("非常感谢您选择华景！您的订单已经成功支付！") ?>');

        var result = data.split('|');
        var orderId = result[0];
        var token = result[1];

        window.location.href = 'index.php?id=flighttickets-order&orderId=' + orderId + '&token=' + token + '&fullsite=yes&change_lang=Chinese&';
    } else {
        tjq('#doingbookoing').hide();
        tjq('#submityorder').prop('disabled', false);
        tjq('#submityorder').show();
        
        tjq('#book-submit-errors').html('<i class="fa fa-times"></i>' + stripeErrors[data[1]]);
    }
}
</script>