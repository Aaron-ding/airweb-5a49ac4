<!--<script type="text/javascript" src="assets/js/bootstrap-typeahead.js"></script>-->
<script type="text/javascript" src="assets/js/typeahead.bundle.js"></script>
<script type="text/javascript" src="assets/js/airport.js"></script>
<script src="assets/components/bootstrap-modal/js/bootstrap-modal.js"></script>
<script src="assets/components/bootstrap-modal/js/bootstrap-modalmanager.js"></script>
<script src="assets/js/ui-modals.js"></script>

<script type="text/javascript"> 
    var request = new Request();
    var cal = new Calendar(); 
    
    <?php if ($_POST["tripType"]=="multiple"){ ?>
        var post={};
        post["srccity"]=JSON.parse('<?php echo $_POST["srccity"]; ?>');
        post["tripType"]="multiple";
        post["noflash"]="<?php echo $_POST["noflash"]; ?>";        
        post["adultcount"]="<?php echo $_POST["adultcount"]; ?>";
        post["childrencount"]="<?php echo $_POST["childrencount"]; ?>";
        post["ifantcount"]="<?php echo $_POST["ifantcount"]; ?>";
        post["cabinlevel"]="<?php echo $_POST["cabinlevel"]; ?>";
         
    <?php }else{ ?>
        var post=JSON.parse('<?php echo json_encode($_POST); ?>');
    <?php }?>
        
    function getNMonth(step,fromdate){
        var now = new Date();
        if(fromdate !=undefined && typeof fromdate.getMonth === 'function'){
                now=fromdate;
        }
		
        if (now.getMonth() == 11) {
            var current = new Date(now.getFullYear() + step, 0, 1);
        } else {
            var current = new Date(now.getFullYear(), now.getMonth() + step, 1);
        }
		
        var tmpmonth="0"+(current.getMonth()+1);
        var tmpday="0"+(current.getDate()+1);
        return Array(current.getFullYear(),tmpmonth.substr(tmpmonth.length-2),current.getMonth()+1,tmpday.substr(tmpday.length-2),current.getDate()+1,current);
    }
	
    function getNDay(step,fromdate){        
        var now = new Date();
        if(fromdate !=undefined && typeof fromdate.getMonth === 'function'){
                now=fromdate;
        }			
        var current=new Date(now.setTime( now.getTime() + (step) * 86400000 ));     var monthlist=[1,2,3,4,5,6,7,8,9,10,11,12]
         var days = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
           if ((((1900+current.getYear()) % 4 == 0) && ((1900+current.getYear() % 100 != 0)) || ((1900+current.getYear() % 400 == 0))))
            days[1] = 29;
        else
            days[1] = 28;
        for(var i=0;i<monthlist.length;++i)
        {if (monthlist[i]==(current.getMonth()+1))
            {if (days[i]<(current.getDate()+1))
                { var tmpday="0"+(current.getDate()+1-days[i]);
                    var tmpmonth="0"+(current.getMonth()+2);}
            else {var tmpday="0"+(current.getDate()+1);
            var tmpmonth="0"+(current.getMonth()+1);}
			}}
        
        
        return Array(current.getFullYear(),tmpmonth.substr(tmpmonth.length-2),current.getMonth()+1,tmpday.substr(tmpday.length-2),current.getDate()+1,getNDay);
    }
    
    tjq(document).ready(function () {

        /*
        tjq(".min-price-label").html("$" + tjq("#price-range").slider("values", 0));
        tjq(".max-price-label").html("$" + tjq("#price-range").slider("values", 1));
        */
        function convertTimeToHHMM(t) {
            var minutes = t % 60;
            var hour = (t - minutes) / 60;
            var timeStr = (hour + "").lpad("0", 2) + ":" + (minutes + "").lpad("0", 2);
            var date = new Date("2014/01/01 " + timeStr + ":00");
            var hhmm = date.toLocaleTimeString(navigator.language, {hour: '2-digit', minute: '2-digit'});
            return hhmm;
        }
        tjq("#flight-times").slider({
            range: true,
            min: 0,
            max: 1440,
            step: 5,
            values: [360, 1200],
            slide: function (event, ui) {

                tjq(".start-time-label").html(convertTimeToHHMM(ui.values[0]));
                tjq(".end-time-label").html(convertTimeToHHMM(ui.values[1]));
            }
        });
        tjq(".start-time-label").html(convertTimeToHHMM(tjq("#flight-times").slider("values", 0)));
        tjq(".end-time-label").html(convertTimeToHHMM(tjq("#flight-times").slider("values", 1)));
        tjq("a[flighttype]").on('click', function () {
            switch (tjq(this).attr('flighttype')) {
                case "roundtrip":
                    tjq("div#flights-return-tab").show();
                    tjq("div#flights-Multiple-tab").hide();                    
                    tjq("label[for=returndate]").show();
                    tjq("#returndate").show();
                    break;
                case "oneway":
                    tjq("div#flights-return-tab").show();
                    tjq("div#flights-Multiple-tab").hide();                    
                    tjq("label[for=returndate]").hide();
                    tjq("#returndate").hide();
                    break;
                case "multiple":
                    tjq("div#flights-Multiple-tab").show();
                    tjq("div#flights-return-tab").hide();
                    break;    
                default:
                    return;
            }
        });


        tjq('.userdefine').on('change',function() {
            var ftype=tjq('.search-tabs:visible').children('li.active').children('a').attr('flighttype');
            if (ftype=="multiple"){
                tjq('#m_searchform').find('.search-button').click();
            }else{
                tjq('#searchform').find('.search-button').click();
            }
        });
        //tjq(window).on('load', initSearchForm);
        initSearchForm();
        var monthNames=['<?php echo lang("一月") ?>','<?php echo lang("二月") ?>','<?php echo lang("三月") ?>','<?php echo lang("四月") ?>','<?php echo lang("五月") ?>','<?php echo lang("六月") ?>','<?php echo lang("七月") ?>','<?php echo lang("八月") ?>','<?php echo lang("九月") ?>','<?php echo lang("十月") ?>','<?php echo lang("十一月") ?>','<?php echo lang("十二月") ?>'];
        //generate low price table
        tjq("#select-month").append('<option value="ALL"><?php echo lang("显示所有月份")?></option>');
        var current_date = new Date();
        var current_year_month = (1900 + current_date.getYear()) + "-" + (current_date.getMonth() + 1);
		if('<?php echo lang("英文") ?>'!="English")
		   {for(var i=0;i<12;i++){
            var tmpdate=getNMonth(i);
            tjq("#select-month").append('<option value="'+tmpdate[0]+'-'+tmpdate[1]+'">'+tmpdate[0]+'年'+tmpdate[1]+'月</option>');
        }} else{for(var i=0;i<12;i++){
            var tmpdate=getNMonth(i);
            tjq("#select-month").append('<option value="'+tmpdate[0]+'-'+tmpdate[1]+'">'+monthNames[tmpdate[1]-1]+". "+tmpdate[0]+'</option>');
        }}
        //build select-month option list
        tjq("#select-month").find("[value='" + current_year_month + "']").prop("selected", "selected");
        
        
        
        
        changePriceByMonth();
        

        tjq("#select-month").change(function() {
            changePriceByMonth();
        });
        tjq("#chardatatype").change(function() {
            changePriceByMonth();
        });
        
        
        tjq("#select-time").change(function() {
            changePriceByMonth();
        });
        tjq("#select-arrivalcity").change(function() {
            changePriceByMonth();
        });
        tjq("#select-departurecity").change(function() {
            tjq('ul.salelist').children().removeClass('active');
            tjq('ul.salelist').find('i').hide();
            var sel=tjq('ul.salelist').children('[data='+tjq(this).val()+']');
            tjq(sel).addClass('active');
            tjq(sel).children().show();	
            changePriceByMonth();
        });
        tjq('ul.salelist').children().click(function() {
                tjq('select[id=select-departurecity]').val(tjq(this).attr('data')).change();
                changePriceByMonth();
        });

    });
    
    function initSearchForm(){
        if (post!=undefined){	
            if (post.srccity!=undefined){
                if (post.tripType!=undefined && post.tripType=="oneway"){
                        //tjq('ul.search-tabs').children().removeClass('active');
                        tjq('a[flighttype=oneway]').click();
                }
                if (post.tripType!=undefined && post.tripType=="multiple"){
                        tjq('ul.search-tabs').children().removeClass('active');
                        tjq('a[flighttype=multiple]').parent('li').addClass('active');
                        tjq("div#flights-Multiple-tab").show();
                        tjq("div#flights-return-tab").hide();
                        tjq( window ).load(function() {  // Run code
                            post.srccity.forEach(function(ele,idx){                        
                                if (idx>=2){
                                    tjq("button:contains('<?php echo lang("添加"); ?>')").last().trigger("click");
                                }
                                tjq('div.internary[idx='+(idx+1)+']').each(function(){
                                    var srcctiy=tjq(this).find('#srccity');
                                    tjq(srcctiy).typeahead('val', getDisplayName(ele.origin));
                                    tjq(srcctiy).attr('data',ele.origin);
                                    tjq(srcctiy).siblings('input').attr('data', ele.origin);

                                    var destcity=tjq(this).find('#destcity');
                                    tjq(destcity).typeahead('val', getDisplayName(ele.destination));                                                
                                    tjq(destcity).attr('data',ele.destination);
                                    tjq(destcity).siblings('input').attr('data', ele.destination);                                
                                    tjq(this).find('input[id^="m_departdate"]').val(ele.departuredate);

                                });

                            });
                        });
                        
                        tjq("div#flights-Multiple-tab").find('#adultcount').val(post.adultcount);
                        tjq("div#flights-Multiple-tab").find('#childrencount').val(post.childrencount);
                        tjq("div#flights-Multiple-tab").find('#ifantcount').val(post.ifantcount);
                        if (typeof post.cabinlevel!= undefined && post.cabinlevel!=undefined && post.cabinlevel!=""){
                            tjq("div#flights-Multiple-tab").find('#PreferLevel').val(post.cabinlevel);
                        }
                        
                         
                }else{
                    tjq('#srccity').typeahead('val', getDisplayName(post.srccity));
                    tjq('#srccity').attr('data',post.srccity);
                    tjq('#srccity').siblings('input').attr('data', post.srccity);

                    tjq('#destcity').typeahead('val', getDisplayName(post.destcity));                                                
                    tjq('#destcity').attr('data',post.destcity);
                    tjq('#destcity').siblings('input').attr('data', post.destcity);

                    tjq('#departdate').val(post.departdate);
                    tjq('#returndate').val(post.returndate);

                    tjq('#adultcount').val(post.adultcount);
                    tjq('#childrencount').val(post.childrencount);
                    tjq('#ifantcount').val(post.ifantcount);
                    if (typeof post.cabinlevel!= undefined && post.cabinlevel!=undefined && post.cabinlevel!=""){
                        tjq('#PreferLevel').val(post.cabinlevel);
                    }
                }
                if (post.noflash==undefined || post.noflash!="yes"){
                    var ftype=tjq('.search-tabs:visible').children('li.active').children('a').attr('flighttype');
                    if (ftype=="multiple"){
                        tjq('#m_searchform').find('.search-button').click();
                    }else{
                        tjq('#searchform').find('.search-button').click();
                    }
                }
            }
        }
    }

    function FT_p_loadlist(departuredata, returndata) {
        if (departuredata == "NotFound" || departuredata == undefined) {
            FT_p_error_report("<?php echo lang("没有找到航班信息") ?>");
            return;
        }

        var listbox = tjq('#ft_item_template').parent();
        var currprocess = 50;

        var step = Math.floor(50 / Object.keys(departuredata).length);
        request.showProcessBar(currprocess);

        for (var key in departuredata) {
            if (departuredata.hasOwnProperty(key)) {
                var value = departuredata[key];
                var template = tjq('#ft_item_template').clone();


                tjq(template).find('#ft_item_template_return').remove();
                tjq(template).addClass('ticketItem');
                tjq(template).css("display", "table");
                //console.log(value);
                for (var item in value) {
                    tjq(template).find('#' + item).html(value[item]);
                }

                var totalfee=parseFloat(value["TotalFare"]);
                var totalperson=parseInt(request.adultcount)+parseInt(request.childrencount)+parseInt(request.ifantcount);
                var avgfee=Math.round(totalfee*100/totalperson)/100;
                tjq(template).find('#TotalFare').html(avgfee);
                tjq(template).find('#TotalFareTotal').html(request.getCurrencyShow(value['TotalFareCurrency'])+value["TotalFare"]);

                if (value['stops']=="0"){
                    tjq(template).find('#stops').html("<?php echo lang("直飞") ?>");
                }else{
                    tjq(template).find('#stops').html(value['stops']+"<?php echo lang("中转")?>");
                }
 
                var airCompanylist=value['airCompany'];
                if(typeof airCompanylist=="string"){					
                    airCompanylist=[airCompanylist];
                }                
                tjq(template).find('#FlightName').html('');
                for(var m=0;m<airCompanylist.length;m++){
                    var ele=airCompanylist[m];
                    var br="<br>";
                    if (m==0){
                        br="";                        
                    }					
                    tjq(template).find('#FlightName').append(br+'<a href="#flighttypeDetail" onload="loadWikipedia" wkey="'+getAirlinesNameByCode(ele)+'" class="soap-popupbox">'+getAirlinesNameByCode(ele)+'</a>');
                };
                tjq(template).find('#TotalFareCurrency').html(request.getCurrencyShow(value['TotalFareCurrency']));
                //airline logo
                var go_airstring = value["airline_logo"];
                if (go_airstring.indexOf(",")>0){
                    go_airstring="unitflight";
                }
                tjq(template).find('.airimg').replaceWith('<img alt="" src="assets/images/logo/' + go_airstring + '.png" />');

                //var stopcitylist=JSON.parse(value['stopcitylist']);
                var stopcitystring='';
                var goSeg=JSON.parse(value['Segment']);
                var i=0;
                tjq.each(goSeg,function(idx,val){

                    stopcitystring +=getCityNameByCode(val["From"]);
                    if(i<(goSeg.length)){
                        stopcitystring +='<span id="stopaircode"><em id="up">'+val["Airline"]+val["Flightnumber"]+'</em><em id="down"><a href="#flighttypeDetail" onload="loadWikipedia" wkey="'+getairplanemodelsByCode(val["AirEquipType"])+'" class="soap-popupbox">'+getairplanemodelsByCode(val["AirEquipType"])+'</a></em></span>';
                    }
                    if (i==(goSeg.length-1)){
                        stopcitystring +=getCityNameByCode(val["To"]);
                    }
                    i++;
                });

                tjq(template).find('#stopcitylist').html(stopcitystring);
                if (request.tripType=="multiple"){
                    tjq(template).find('.gobutton').attr('data',JSON.stringify(value));
                }else{
                    tjq(template).find('.gobutton').attr('data',request.concat(JSON.stringify(value),""));
                }

                if (request.tripType=="roundtrip" ||  request.tripType=="multiple"){

                    //build return information
                    var returncount=0;
                    tjq.each(value["RETURN_LIST"], function (key_r, value_r) {
                        var return_template = tjq('#ft_item_template_return').clone();
                        tjq(return_template).removeAttr('id');
                        if (key_r != 0) {
                            tjq(return_template).find('.alert-notice').remove();
                        }
                        //console.log(returndata[value_r]);
                        for (var item in returndata[value_r]) {
                            tjq(return_template).find('#' + item).html(returndata[value_r][item]);
                        }
                        if (returndata[value_r]['stops']=="0"){
                            tjq(return_template).find('#stops').html("<?php echo lang("直飞") ?>");
                        }else{
                            tjq(return_template).find('#stops').html(returndata[value_r]['stops']+"<?php echo lang("中转")?>");
                        }
                        var airCompanylist=returndata[value_r]['airCompany'];
                        if(typeof airCompanylist=="string"){					
                            airCompanylist=[airCompanylist];
                        } 
						
                        tjq(return_template).find('#FlightName').html('');
                        for(var m=0;m<airCompanylist.length;m++){
                            var ele=airCompanylist[m];
                                var br="<br>";
                    		if (m==0){
                                    br="";                        
                    		}	
                            tjq(return_template).find('#FlightName').append(br+'<a href="#flighttypeDetail" onload="loadWikipedia" wkey="'+getAirlinesNameByCode(ele)+'" class="soap-popupbox">'+getAirlinesNameByCode(ele)+"</a>");
                        }    
                        var returnSeg=JSON.parse(returndata[value_r]['Segment']);
                        tjq(return_template).hide();


                        var return_airstring = returndata[value_r]["airline_logo"];
                        if (return_airstring.indexOf(",")>0){
                            return_airstring="unitflight";
                        }
                        tjq(return_template).find('.airimg').replaceWith('<img alt="" src="assets/images/logo/' + return_airstring + '.png" />');
                        var airstring=go_airstring;
                        if (return_airstring!=go_airstring){
                            airstring="multi";
                        }
                        request.airline_logo=airstring;
                        if (request.tripType=="multiple"){
                            tjq(return_template).find('.orderbutton').attr('data',JSON.stringify(returndata[value_r]));
                        }else{
                            tjq(return_template).find('.orderbutton').attr('data',request.concat(JSON.stringify(value),JSON.stringify(returndata[value_r])));                            
                        }
                        tjq(return_template).find('.orderbutton').attr('data-from','sabre');
                        returncount++;
                        tjq(template).find('.details').append(return_template);

                        //stopcitylist=JSON.parse(returndata[value_r]['stopcitylist']);
                        stopcitystring='';
                        var i=0;
                        tjq.each(returnSeg,function(idx,val){
                               stopcitystring +=getCityNameByCode(val["From"]);
                               if(i<(returnSeg.length)){
                                stopcitystring +='<span id="stopaircode"><em id="up">'+val["Airline"]+val["Flightnumber"]+'</em><em id="down"><a href="#flighttypeDetail" onload="loadWikipedia" wkey="'+getairplanemodelsByCode(val["AirEquipType"])+'" class="soap-popupbox">'+getairplanemodelsByCode(val["AirEquipType"])+'</a></em></span>';
                               }
                               if (i==(returnSeg.length-1)){
                                stopcitystring +=getCityNameByCode(val["To"]);
                               }
                               i++;
                        });

                        tjq(return_template).find('#stopcitylist').html(stopcitystring);
                        //airline logo


                    });


                    tjq(template).find('.gobutton').html( '<?php echo lang("选择回程")?>（'+returncount +'）');
                    tjq(template).find('.panel-collapse').hover(function() {

                        tjq(this).find('div hr').css('border-color','#c30d23');
                        tjq(this).next().find('div hr').css('border-color','#c30d23');

                        //tjq( this ).fadeOut( 100 );

                    },function(){
                        tjq(this).find('div hr').css('border-color','#eee');
                        tjq(this).next().find('div hr').css('border-color','#eee');
                       // tjq( this ).fadeIn( 500 );
                    });
                }

                tjq(listbox).append(template);

            }
            currprocess += step;
            setTimeout(function () {
                request.showProcessBar(currprocess);
            }, 200);


        }
        if (request.tripType=="multiple" ){
            tjq(".gobutton").hide();
            tjq('.orderbutton').hide();
            tjq('article.ticketItem').each(function(){
               tjq(this).find(".orderbutton").last().show();
            });
            
             tjq('.orderbutton').click(function (event) {
                    event.preventDefault();
                    tjq(this).hide();                    
                    tjq(this).siblings('div#loadingseat').show();
                    var opobj=this;                    
                    var para2=Array();
                    tjq(this).parents('article.box').find("a[data]").each(function(){
                        var tmpraw=JSON.parse(tjq(this).attr('data'));
                        tmpraw["personcount"]=request.getPersoncount();
                        para2.push(tmpraw);
                    });                    
                    
                    FT_c_checkSeats(para2,function(avai){                        
                        if (avai==1){
                            request.mulit_dconccat(para2);                            
                            tjq('input[name=request]').val(request.toJSONString());
                            tjq('input[name=personcount]').val(request.getPersoncount());
                            var actionurl=tjq('form[name=orderform]').attr('action')+"<?php echo $GLOBALS["fullsite"] ?>";
                            tjq('form[name=orderform]').attr('action',actionurl);
                            tjq('form[name=orderform]').submit();
                        }else{
                            tjq(opobj).siblings('div#loadingseat').hide();
                            tjq(opobj).siblings('div#nomoreseat').show();
                        }
                    });    
                                                               
            });
            tjq('.panel-collapse').fadeIn();
            
        }
        
        if (request.tripType=="roundtrip" ){
            tjq(".gobutton").show();
            tjq('.orderbutton').show();
            tjq(".gobutton").click(function (event) {
                event.preventDefault();
                //console.log(tjq(this).parents('.details').children('.panel-collapse'));
                var currentopen=tjq(this).parents('.details').children('.panel-collapse').is(":visible");
                tjq('.panel-collapse').hide();                                                
                tjq('h4[id^=mark-]').hide();                                                
                if (!currentopen){
                    tjq(this).parents('article[id=ft_item_template]').find('h4[id^=mark-]').show();
                    tjq(this).parents('.details').children('.panel-collapse').fadeIn();
                }
                tjq('html, body').animate({
                    scrollTop: tjq(this).offset().top - 100
                }, 1000);
            });

            tjq('.orderbutton').click(function (event) {
                    event.preventDefault();
                    tjq(this).hide();                    
                    tjq(this).siblings('div#loadingseat').show();
                    var opobj=this;
                    var para2=Array();
                    request.dconcat(tjq(this).attr('data'));                     
                    var tmpraw=JSON.parse(request.goRaw);
                    tmpraw["personcount"]=request.getPersoncount();
                    para2.push(tmpraw);
                    var tmpraw=JSON.parse(request.returnRaw);
                    tmpraw["personcount"]=request.getPersoncount();
                    para2.push(tmpraw);                    
                    //para2["go"]=JSON.parse(request.goKey);
                    //para2["return"]=JSON.parse(request.returnKey);
console.log(para2);                    
                    FT_c_checkSeats(para2,function(avai){                        
                        if (avai==1){
                            tjq('input[name=request]').val(request.toJSONString());
                            tjq('input[name=personcount]').val(request.getPersoncount());
                            var actionurl=tjq('form[name=orderform]').attr('action')+"<?php echo $GLOBALS["fullsite"] ?>";
                            tjq('form[name=orderform]').attr('action',actionurl);
                            tjq('form[name=orderform]').submit();
                        }else{
                            tjq(opobj).siblings('div#loadingseat').hide();
                            tjq(opobj).siblings('div#nomoreseat').show();
                        }
                    });
                    
                    
                                                               
            });
        }
        if (request.tripType=="oneway"){
            tjq(".gobutton").show();
            tjq('.orderbutton').show();
            tjq(".gobutton").html('<?php echo lang(" 预定")?>');
            tjq(".gobutton").click(function (event) {
                event.preventDefault();
                tjq(this).hide();                    
                tjq(this).siblings('div#loadingseat').show();
                var opobj=this;
                var para2=Array();
                request.dconcat(tjq(this).attr('data')); 
                var tmpraw=JSON.parse(request.goRaw);
                tmpraw["personcount"]=request.getPersoncount();
                para2.push(tmpraw);              
                FT_c_checkSeats(para2,function(avai){                        
                    if (avai==1){
                        tjq('input[name=request]').val(request.toJSONString());
                        tjq('input[name=personcount]').val(request.getPersoncount());
                        tjq('form[name=orderform]').submit();
                    }else{
                        tjq(opobj).siblings('div#loadingseat').hide();
                        tjq(opobj).siblings('div#nomoreseat').show();
                    }
                });
               
            });
        }

        tjq('.function-button-box').show();
        tjq('.button-loading').show();
        setTimeout(function () {
            request.showProcessBar(100);
        }, 1000);
        tjq('.myBtn').click(function (event) {
            tjq('#myModal').show();
        });


    }
    function FT_p_error_report(errorMsg, errorLevel, showtype) {
        tjq('.information-box').show();
        tjq('.information-box div').html(errorMsg);
        request.showProcessBar(100);
    }
    tjq("button:contains('<?php echo lang("添加"); ?>')").click(function(event){
        event.preventDefault();
        var currobj=tjq('.internary').last();
        var curridx=tjq(currobj).attr('idx');        
        var copyobj=tjq(currobj).clone(true,true);
        var copyidx=parseInt(curridx)+1;
        tjq(copyobj).attr('idx',copyidx);
        tjq(copyobj).find('input').val('');        
        tjq(copyobj).find('span.twitter-typeahead').remove();
        tjq(copyobj).find('.typeahead-close').each(function( index ) {
           var tmpid="srccity";
           if(index==1){
               tmpid="destcity";
           }
           tjq(this).after('<input type="text" required id="'+tmpid+'" class="typeahead input-text full-width airport" style="left:-1px!important" data="" placeholder="<?php echo lang("城市，机场名字");?>" />');
        });
        
        tjq(copyobj).find('.datepicker-wrap').empty();
        tjq(copyobj).find('.datepicker-wrap').html('<input id="m_departdate_'+copyidx+'" required type="text" class="input-text full-width" picker-type="departure"  date-type="date" placeholder="yyyy-mm-dd" />');
        initDepartureDate(tjq(copyobj).find("input[id^='m_departdate_']"));
        
        initAirportInput(copyobj);
        tjq(currobj).after(tjq(copyobj));        
       
        tjq("button:contains('<?php echo lang("删除"); ?>')").unbind('click').click(function(){
            if (tjq("button:contains('<?php echo lang("删除"); ?>')").length>1){
                var delcurrobj=tjq(this).closest('.row');
                var prevobj=tjq(delcurrobj).prev();
                tjq(delcurrobj).remove();
                tjq("button:contains('<?php echo lang("添加"); ?>')").last().show();
                
            }
            if (tjq("button:contains('<?php echo lang("删除"); ?>')").length==1){
                tjq("button:contains('<?php echo lang("删除"); ?>')").first().hide();
            }
        });
        tjq("button:contains('<?php echo lang("添加"); ?>')").hide();
        tjq("button:contains('<?php echo lang("添加"); ?>')").last().show();
        tjq("button:contains('<?php echo lang("删除"); ?>')").show();  
    });
    tjq('.search-button').click(function (event) {
        event.preventDefault();
        tjq('.information-box').hide();
        
        if (false == request.checkForm(tjq(this).closest('form').attr('id'))) {
            return;
        }

        options=request.getSearchOption();

console.log(options);
        if (request.tripType==="multiple"){
            var tmpstr='<h4 class="box-title">【<?php echo lang('多程')?>】</h4>';
            tjq('div.function-button-box').first().html('');
            var i=0;
            tjq('div.internary').each(function(){
                if (i>0){
                    tmpstr +='<br>'
                }else{
                    tmpstr +='<h4 class="box-title pull-left">'
                }
                tmpstr +=getAirportNameByCode(tjq(this).find('input#srccity').val())+'<img src="assets/images/exchange-oneway.jpg">'+getAirportNameByCode(tjq(this).find('input#destcity').val());
                i++;
            });
            tmpstr +="</h4>"
            tjq('div.function-button-box').first().html(tmpstr);
        }
        if (request.tripType==="roundtrip"){
            tjq('div.function-button-box').first().html('');
            var tmpstr='<h4 class="box-title">【<?php echo lang('往返')?>】'+getAirportNameByCode(request.srccity)+'<img src="assets/images/exchange-full.jpg">'+getAirportNameByCode(request.destcity)+'</h4>';
            tjq('div.function-button-box').first().html(tmpstr);
        }
        if (request.tripType==="oneway"){
            tjq('div.function-button-box').first().html('');
            var tmpstr='<h4 class="box-title">【<?php echo lang('单程')?>】'+getAirportNameByCode(request.srccity)+'<img src="assets/images/exchange-oneway.jpg">'+getAirportNameByCode(request.destcity)+'</h4>';
            tjq('div.function-button-box').first().html(tmpstr);
        }
/*
else{
            tjq('#DepartureCityName').html(getAirportNameByCode(request.srccity));
            tjq('#ArrivateCityName').html(getAirportNameByCode(request.destcity));
            if (request.tripType==="roundtrip"){
                tjq('#triptypeshow').text("<?php echo lang('往返')?>");
                tjq('#DepartureCityName').parent().children('img').replaceWith('<img src="assets/images/exchange-full.jpg">');
            }else{
                tjq('#triptypeshow').text("<?php echo lang('单程')?>");
                tjq('#DepartureCityName').parent().children('img').replaceWith('<img src="assets/images/exchange-oneway.jpg">');
            }
        }
        */
        tjq('.ticketItem').remove();
        tjq('.function-button-box').hide();
        tjq('.button-loading').hide();
        request.showProcessBar(0);
        request.showProcessBar(10);

        FT_c_doSearch(options, FT_p_loadlist, FT_p_error_report);

    });

    function loadflightdetail(obj)
    {
        var data=request.getDconcat(tjq(obj).parents('.second-row').find('div.action').children('a').attr('data'));


        var markid=tjq(obj).parents('div.details-wrapper').children('h4[id^=mark-]').attr('id');
        var contenttype="all";
        var internary=data[0];

        switch(markid){
            case 'mark-depature':
                contenttype="go";
                break;
            case 'mark-arrival':
                contenttype="return";
                internary=data[1];
                if(internary==undefined){
                    internary=data[0];
                }
                break;    

        }
        loadInternaryDetail(contenttype,internary);

    }

    function loadWikipedia(obj){
        var wkey=tjq(obj).attr('wkey');                                        
        tjq('#flighttypeDetail').find('iframe').attr('src', 'https://dev.crud.sinorama.ca/getWikiContent.php?keyword='+wkey+'&');                                                                                
        tjq('#soap-popupbox').click(function(e) {  
               tjq('#flighttypeDetail').find('iframe').attr('src','');
        });

    }

    function changePriceByMonth(){
        tjq('#price_chart_list').html('<div class="col-sm-12 col-md-12"><p class="text-center" ><img src="assets/images/loading.gif" width="50%" height="50%" /></p></div>');
        //tjq('#price_chart_list').append('<div class="col-sm-6 col-md-6"><p class="text-center" ><img src="assets/images/loading.gif" width="50%" height="50%" /></p></div>');
        tjq('#price_table_list').html('<div class="calendar col-sm-12 col-md-12"><p class="text-center" ><img src="assets/images/loading.gif" width="50%" height="50%" /></p></div>');
        var currmonth=tjq('#select-month').val();
        var fromcity=tjq('#select-departurecity').val();
        var tocity=tjq('#select-arrivalcity').val();
        var duration=tjq('#select-time').val();

        tjq.getJSON( "getLowPriceList.php?currmonth="+currmonth+"&fromcity="+fromcity+"&tocity="+tocity+"&duration="+duration+"&", function(lowpricelist) {
                tjq('#price_chart_list').html('');

                drawChart2(lowpricelist);  
                if (currmonth!="ALL"){                                                
                    tjq('#price_table_list').html('<div class="calendar col-sm-12 col-md-12">'+getOnmonthTable(currmonth,lowpricelist)+'</div>');
                }else{
                    tjq('#price_table_list').html('');
                    tjq("#select-month option").each(function()
                    {
                        var tmpmonth=tjq(this).val();
                        if (tmpmonth!="ALL"){
                            tjq('#price_table_list').append('<div class="calendar col-sm-6 col-md-6">'+getOnmonthTable(tmpmonth,lowpricelist)+'</div>');
                        }
                    });
                }
                
                tjq('td.available a').hover(function(){
                    var currdate=tjq(this).attr('startdate');                    
                    tjq(this).attr('start-date',currdate.substring(4,6)+'<?php echo lang(" 月")?>'+currdate.substring(6,8)+'<?php echo lang(" 日")?>');
                    var step=tjq('#select-time').val();					
                    var enddate=getNDay(step,new Date(currdate.substring(0,4)+"-"+currdate.substring(4,6)+"-"+currdate.substring(6,8)));					
                    tjq(this).attr('end-date',enddate[1]+'<?php echo lang(" 月")?>'+enddate[3]+'<?php echo lang(" 日")?>');
                });
                tjq('td.available a').click(function(event ){
                    event.preventDefault();
                    var currdate=tjq(this).attr('startdate');
                    var step=tjq('#select-time').val();		
                    tjq('a[flighttype=roundtrip]').click();
                    var enddate=getNDay(step,new Date(currdate.substring(0,4)+"-"+currdate.substring(4,6)+"-"+currdate.substring(6,8)));
                    tjq('#srccity').typeahead('val', getDisplayName(tjq('#select-departurecity').val()));
                    tjq('#srccity').attr('data',tjq('#select-departurecity').val());
                    tjq('#srccity').siblings('input').attr('data', tjq('#select-departurecity').val());

                    tjq('#destcity').typeahead('val', getDisplayName(tjq('#select-arrivalcity').val()));                                                
                    tjq('#destcity').attr('data',tjq('#select-arrivalcity').val());
                    tjq('#destcity').siblings('input').attr('data', tjq('#select-arrivalcity').val());

                    tjq('#departdate').val(currdate.substring(0,4)+"-"+currdate.substring(4,6)+"-"+currdate.substring(6,8));
                    var enddate=getNDay(step,new Date(currdate.substring(0,4)+"-"+currdate.substring(4,6)+"-"+currdate.substring(6,8)));
                    tjq('#returndate').val(enddate[0]+"-"+enddate[1]+"-"+enddate[3]);
                    tjq('html, body').animate({scrollTop : 0},800);
                    var ftype=tjq('.search-tabs:visible').children('li.active').children('a').attr('flighttype');
                    if (ftype=="multiple"){
                        tjq('#m_searchform').find('.search-button').click();
                    }else{
                        tjq('#searchform').find('.search-button').click();
                    }

                });

        });
    }
    function getOnmonthTable(month,lowpricelist){
        var currmonth=month;
        var unavailable_days = [];        
        var price_arr = {};
        tjq(lowpricelist).each(function(){
            if (this.fromdate.substring(0,7)==currmonth){
                if (parseInt(this.totalfare)>0){
                        price_arr[parseInt(this.fromdate.substring(8,10))]="$"+parseInt(this.totalfare)/100;
                }
            }
        });
        var current_date=new Date(currmonth+"-15");   
        cal.generateHTML(current_date.getMonth(), (1900 + current_date.getYear()), unavailable_days, price_arr);                                        
        return cal.getHTML();
    }


</script>

<script>
function drawChart1(){
    tjq('#price_chart_list').append('<div class="col-sm-6 col-md-6"><canvas id="myChart1"></canvas></div>');
    var ctx = document.getElementById("myChart1");
    var myChart = new Chart(ctx, {
        type: 'bar',
        data: {
            labels: ["Jan", "Feb", "Mar", "April", "May", "June"],
            datasets: [{
                label: '# of Votes',
                data: [12, 19, 3, 5, 2, 3],
                backgroundColor: [
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(255, 206, 86, 0.2)',
                    'rgba(75, 192, 192, 0.2)',
                    'rgba(153, 102, 255, 0.2)',
                    'rgba(255, 159, 64, 0.2)'
                ],
                borderColor: [
                    'rgba(255,99,132,1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)',
                    'rgba(75, 192, 192, 1)',
                    'rgba(153, 102, 255, 1)',
                    'rgba(255, 159, 64, 1)'
                ],
                borderWidth: 1
            }]
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero:true
                    }
                }]
            }
        }
    });
}
</script>
<script>
function drawChart2(lowpricelist){
    tjq('#price_chart_list').append('<div class="col-sm-12 col-md-12"><canvas id="myChart2" style="height:250px!important"></canvas></div>');
    var ctx = document.getElementById("myChart2");
    var labels = new Array;
    var data=new Array;
    var datatype=tjq('#chardatatype').val();
    tjq("#select-month option").each  ( function() {
        if (tjq(this).val()!="ALL"){
            var currentmonth=tjq(this).val();
            var tmpval=0;
            var tmpcount=0;
            tjq(lowpricelist).each(function(){
                if (new Date(this.fromdate)> new Date() && this.fromdate.substring(0,7)==currentmonth){
                    if (parseInt(this.totalfare)>0){
                            tmpcount++;
                            if (datatype=="avg"){
                                tmpval+=parseInt(this.totalfare)/100;
                            }
                            if (datatype=="min"){
                                if (tmpval==0 || tmpval>parseInt(this.totalfare)/100){
                                    tmpval=parseInt(this.totalfare)/100;
                                }
                                
                            }
                            if (datatype=="max"){
                                if (tmpval==0 || tmpval<parseInt(this.totalfare)/100){
                                    tmpval=parseInt(this.totalfare)/100;
                                }
                            }
                    }
                }
            });
            if (datatype=="avg"){
                data.push(tmpval/tmpcount);
            }else{
                if (tmpval>0){
                    data.push(tmpval);
                }
            }
            labels.push ( tjq(this).text());
        }
    });
    var myChart = new Chart(ctx, {
        type: 'line',
        data: {
            labels: labels,
            datasets: [{
                label: tjq('#select-departurecity option:selected').text()+'< = >'+tjq('#select-arrivalcity option:selected').text()+tjq('#select-time').val()+'<?php echo lang("天往返") ?>'+tjq('#chardatatype option:selected').text()+"<?php echo lang('走势图') ?>",
                data: data,
                backgroundColor: [
                    'rgba(255, 99, 132, 0.2)'
                ],
                borderColor: [
                    'rgba(255,99,132,1)'
                ],
                borderWidth: 1
            }]
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero:false
                    }
                }]
            }
        }
    });
}
</script>