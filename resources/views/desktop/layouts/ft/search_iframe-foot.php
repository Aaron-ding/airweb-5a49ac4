<!--<script type="text/javascript" src="assets/js/bootstrap-typeahead.js"></script>-->
<script type="text/javascript" src="assets/js/typeahead.bundle.js"></script>
<script type="text/javascript" src="assets/js/airport.js"></script>
<script type="text/javascript" src="assets/js/airlines.js?v=20170529"></script>
<script src="assets/components/bootstrap-modal/js/bootstrap-modal.js"></script>
<script src="assets/components/bootstrap-modal/js/bootstrap-modalmanager.js"></script>
<script src="assets/js/ui-modals.js"></script>
<script type="text/javascript" src="assets/js/clipboard.min.js"></script>

<script type="text/javascript">
    var lang = '<?php echo $lang; ?>';

    var request = new Request();
    var cal = new Calendar();

    Array.prototype.unique = function() {
        var unique = [];
        for (var i = 0; i < this.length; i++) {
            if (unique.indexOf(this[i]) == -1) {
                unique.push(this[i]);
            }
        }
        return unique;
    };

    function getNMonth(step,fromdate){
        var now = new Date();
        if(fromdate !=undefined && typeof fromdate.getMonth === 'function'){
                now=fromdate;
        }
		
        if (now.getMonth() == 11) {
            var current = new Date(now.getFullYear() + step, 0, 1);
        } else {
            var current = new Date(now.getFullYear(), now.getMonth() + step, 1);
        }
		
        var tmpmonth="0"+(current.getMonth()+1);
        var tmpday="0"+(current.getDate()+1);
        return Array(current.getFullYear(),tmpmonth.substr(tmpmonth.length-2),current.getMonth()+1,tmpday.substr(tmpday.length-2),current.getDate()+1,current);
    }
	
    function getNDay(step,fromdate){        
        var now = new Date();
        if(fromdate !=undefined && typeof fromdate.getMonth === 'function'){
                now=fromdate;
        }			
        var current=new Date(now.setTime( now.getTime() + (step) * 86400000 ));     var monthlist=[1,2,3,4,5,6,7,8,9,10,11,12]
         var days = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
           if ((((1900+current.getYear()) % 4 == 0) && ((1900+current.getYear() % 100 != 0)) || ((1900+current.getYear() % 400 == 0))))
            days[1] = 29;
        else
            days[1] = 28;
        for(var i=0;i<monthlist.length;++i)
        {if (monthlist[i]==(current.getMonth()+1))
            {if (days[i]<(current.getDate()+1))
                { var tmpday="0"+(current.getDate()+1-days[i]);
                    var tmpmonth="0"+(current.getMonth()+2);}
            else {var tmpday="0"+(current.getDate()+1);
            var tmpmonth="0"+(current.getMonth()+1);}
			}}
        
        
        return Array(current.getFullYear(),tmpmonth.substr(tmpmonth.length-2),current.getMonth()+1,tmpday.substr(tmpday.length-2),current.getDate()+1,getNDay);
    }
    
    tjq(document).ready(function () {
        tjq('.bookit-button').click(function () {
            var from = tjq(this).data('from');
            var to = tjq(this).data('to');
            var departing = tjq(this).data('departing');
            var returning = tjq(this).data('returning');

            var url = 'index.php?id=search&srccity='+from+'&destcity='+to+'&tripType=roundtrip&departdate='+departing+'&returndate='+returning+'&adultcount=1&childrencount=0&infantsseatcount=0&infantslapcount=0&fullsite=yes';
            window.parent.location = url;
        });

        tjq('#m_searchform #passengers_select, #searchform #passengers_select').click(function () {
            if (tjq(this).next().is(':visible')) {
                tjq(this).next().slideUp();
            } else {
                tjq(this).next().slideDown();
            }
        });

        tjq('.hp-select-pax-done').click(function () {
            tjq(this).parent().slideUp();
        });

        tjq('a.pass-slide-trigger').click(function (e) {
            e.preventDefault();

            var type = tjq(this).data('type');
            var action = tjq(this).data('action');
            var qty = parseInt(tjq(this).parent().parent().find('.passenger-number-sel').text());
            var total = parseInt(tjq(this).parent().parent().parent().parent().find('.flights-home-pass-total').text());
            if (action == 'decrement') {
                if ((type != 'adult' && qty > 0) || (type == 'adult' && qty > 1)) {
                    qty--;
                    total--;
                }
            } else {
                if (total < 6) {
                    qty++;
                    total++;
                }
            }

            tjq(this).parent().parent().find('.passenger-number-sel').text(qty);
            tjq(this).parent().parent().parent().parent().find('.flights-home-pass-total').text(total);
            tjq(this).parent().parent().parent().find('#' + type + 'count').val(qty);
        });

        function convertTimeToHHMM(t) {
            var minutes = t % 60;
            var hour = (t - minutes) / 60;
            var timeStr = (hour + "").lpad("0", 2) + ":" + (minutes + "").lpad("0", 2);
            var date = new Date("2014/01/01 " + timeStr + ":00");
            var hhmm = date.toLocaleTimeString(navigator.language, {hour: '2-digit', minute: '2-digit'});
            return hhmm;
        }
        tjq("#flight-times").slider({
            range: true,
            min: 0,
            max: 1440,
            step: 5,
            values: [360, 1200],
            slide: function (event, ui) {

                tjq(".start-time-label").html(convertTimeToHHMM(ui.values[0]));
                tjq(".end-time-label").html(convertTimeToHHMM(ui.values[1]));
            }
        });
        tjq(".start-time-label").html(convertTimeToHHMM(tjq("#flight-times").slider("values", 0)));
        tjq(".end-time-label").html(convertTimeToHHMM(tjq("#flight-times").slider("values", 1)));
        tjq("a[flighttype]").on('click', function () {
            switch (tjq(this).attr('flighttype')) {
                case "roundtrip":
                    tjq('#searchform input[name="tripType"]').val('roundtrip');
                    tjq("div#flights-return-tab").show();
                    tjq("div#flights-Multiple-tab").hide();                    
                    tjq("label[for=returndate]").show();
                    tjq("#departdate").parent().parent().parent().removeClass('col-xs-6').addClass('col-xs-3');
                    tjq("#returndate").parent().parent().parent().show();
                    tjq('#setReturndateLI').show();
                    tjq('#setDepartdateLI').show();
                    break;
                case "oneway":
                    tjq('#searchform input[name="tripType"]').val('oneway');
                    tjq("div#flights-return-tab").show();
                    tjq("div#flights-Multiple-tab").hide();                    
                    tjq("label[for=returndate]").hide();
                    tjq("#returndate").parent().parent().parent().hide();
                    tjq("#departdate").parent().parent().parent().removeClass('col-xs-3').addClass('col-xs-6');
                    tjq('#setReturndateLI').hide();
                    tjq('#setDepartdateLI').show();
                    break;
                case "multiple":
                    tjq("div#flights-Multiple-tab").show();
                    tjq("div#flights-return-tab").hide();
                    tjq('#setReturndateLI').hide();
                    tjq('#setDepartdateLI').hide();
                    break;    
                default:
                    return;
            }
        });
    });

    function initSetDateSelect() {
        var departdate = tjq('#departdate').val();
        var returndate = tjq('#returndate').val();
        var departdateString = new Date(departdate);
        var returndateString = new Date(returndate);

        tjq('#setDepartdate').html('<option selected="" disabled=""><?php echo lang('修改出发日期'); ?></option>');

        var departdateStart = departdateString.getTime() - (3 * 86400000);
        var departdateEnd = departdateString.getTime() + (3 * 86400000);
        for (departdateTime = departdateStart; departdateTime <= departdateEnd; departdateTime += 86400000) {
            var departdateFormated = new Date(departdateTime).toISOString().substring(0, 10);
            var disabled = '',
                current = '',
                style = '';
            if (departdateFormated == departdate) {
                disabled = ' disabled';
                current = ' (<?php echo lang('当前'); ?>)';
                style = ' style="background: rgba(0,0,0,0.3);"';
            }

            tjq('#setDepartdate').append('<option'+disabled+style+'>'+departdateFormated+current+'</option>');
        }

        tjq('#setReturndate').html('<option selected="" disabled=""><?php echo lang('修改返程日期'); ?></option>');

        var returndateStart = returndateString.getTime() - (3 * 86400000);
        var returndateEnd = returndateString.getTime() + (3 * 86400000);
        for (returndateTime = returndateStart; returndateTime <= returndateEnd; returndateTime += 86400000) {
            var returndateFormated = new Date(returndateTime).toISOString().substring(0, 10);
            var disabled = '',
                current = '',
                style = '';
            if (returndateFormated == returndate) {
                disabled = ' disabled';
                current = ' (<?php echo lang('当前'); ?>)';
                style = ' style="background: rgba(0,0,0,0.3);"';
            }

            tjq('#setReturndate').append('<option'+disabled+style+'>'+returndateFormated+current+'</option>');
        }
    }

    function toDate(dateStr) {
        var parts = dateStr.split("-");
        return new Date(parts[1]+'/'+parts[2]+'/'+parts[0]);
    }

    function generateMonthWeek(dateStr) {
        var date = toDate(dateStr);
        var day = date.getDate();
        var monthIndex = date.getMonth();
        var weekIndex = date.getDay();

        if (lang == 'English') {
            var month = monthNames[monthIndex];
            var week = weekNames_EN[weekIndex];
            var monthWeek = week + '<br>' + day + ' ' + month;
        } else if (lang == 'Chinese') {
            var month = monthIndex + 1;
            var week = weekNames_CN[weekIndex];
            var monthWeek = week + '<br>' + month + '月' + day + '日';
        }

        return monthWeek;
    }

    tjq("button:contains('<?php echo lang("添加"); ?>')").click(function(event){
        event.preventDefault();
        var currobj=tjq('.internary').last();
        var curridx=tjq(currobj).attr('idx');        
        var copyobj=tjq(currobj).clone(true,true);
        var copyidx=parseInt(curridx)+1;
        tjq(copyobj).attr('idx',copyidx);
        tjq(copyobj).find('input').val('');        
        tjq(copyobj).find('span.twitter-typeahead').remove();
        tjq(copyobj).find('.typeahead-close').each(function( index ) {
           var tmpid="srccity";
           if(index==1){
               tmpid="destcity";
           }
           tjq(this).after('<input type="text" required id="'+tmpid+'" class="typeahead input-text full-width airport" style="left:-1px!important" data="" placeholder="<?php echo lang("城市，机场名字");?>" />');
        });
        
        tjq(copyobj).find('.datepicker-wrap').empty();
        tjq(copyobj).find('.datepicker-wrap').html('<input id="m_departdate_'+copyidx+'" required type="text" class="input-text full-width" picker-type="departure"  date-type="date" placeholder="yyyy-mm-dd" />');
        initDepartureDate(tjq(copyobj).find("input[id^='m_departdate_']"));
        
        initAirportInput(copyobj);
        tjq(currobj).after(tjq(copyobj));        
       
        tjq("button:contains('<?php echo lang("删除"); ?>')").unbind('click').click(function(){
            if (tjq("button:contains('<?php echo lang("删除"); ?>')").length>1){
                var delcurrobj=tjq(this).closest('.row');
                var prevobj=tjq(delcurrobj).prev();
                tjq(delcurrobj).remove();
                tjq("button:contains('<?php echo lang("添加"); ?>')").last().show();
                
            }
            if (tjq("button:contains('<?php echo lang("删除"); ?>')").length==1){
                tjq("button:contains('<?php echo lang("删除"); ?>')").first().hide();
            }
        });
        tjq("button:contains('<?php echo lang("添加"); ?>')").hide();
        tjq("button:contains('<?php echo lang("添加"); ?>')").last().show();
        tjq("button:contains('<?php echo lang("删除"); ?>')").show();  
    });

    tjq('.search-button').click(function (event) {
        event.preventDefault();

        var form = tjq(this).closest('form').attr('id');
        if (false == request.checkForm(form)) {
            return;
        }

        var srccity = tjq('#' + form + ' #srccity').attr('data');
        var destcity = tjq('#' + form + ' #destcity').attr('data');

        tjq('#' + form + ' input[name="srccity"]').val(srccity);
        tjq('#' + form + ' input[name="destcity"]').val(destcity);

        tjq('#' + form).submit();
    });
</script>