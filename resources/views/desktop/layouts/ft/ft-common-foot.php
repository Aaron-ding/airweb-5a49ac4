<!-- 机型详细弹层 -->
<div id="flighttypeDetail" class="travelo-login-box travelo-box">
    <div>
       <button type="button" data-dismiss="modal" onclick="tjq('#soap-popupbox').click();"><i class="soap-icon-close" style=" font-size: 24px;"></i></button>
       <span class="pull-right"><img src="assets/images/wikipedia.jpg" /></span>
    </div>
    <iframe src="" style="width:100%;min-height:600px;overflow-y:scroll;overflow-x:hidden"></iframe>
</div>

<!-- 机票详细单弹层 -->
<div id="flightDetail" class="travelo-login-box travelo-box">
    <div><button type="button" data-dismiss="modal" onclick="tjq('#soap-popupbox').click();"><i class="soap-icon-close" style=" font-size: 24px;"></i></button></div>
    <div>
	<div id="orderdetails">
	<h4 style="display:none">
       <span id="triptype"></span>
       <span id="DepartureCityName"><?php echo lang('北京首都国际机场') ?></span>
       <img src="assets/images/exchange-depart.jpg" />
       <span id="ArrivateCityName"><?php echo lang('蒙特利尔机场') ?></span>
       <span class="red-color">
             <i class="soap-icon-clock" style="padding-left:20px"></i>
             <span id="ElapsedTimeTotalHours"></span><?php echo lang('小时') ?>
             <span id="ElapsedTimeTotalHoursLeftMins"></span><?php echo lang('分钟') ?>
       </span>
    </h4>
	<div id="internarylist" style="display:none">
        <dl id="template" style="display:none">
            <dt><img alt="" src="assets/images/logo/OZ.png"></dt>
            <dd>
               <ul id="first_row">
                 <li></li>
                 <li id="arrow-right"><i class="fa fa-long-arrow-right"></i><br />KE 854,77W,U<?php echo lang('舱') ?><br /></li>       
                 <li></li>          
               </ul>
            </dd>
        </dl>
        <h5 style="display: none;">layover 12h 46m stop Vancouver (YVR)</h5>
        
        <p id="notice" style="display:none">
            <b>Bag fees</b>
            Baggage fees when purchased at the airport (Prices may be cheaper if purchased online with Air Canada)<br>
            Carry on: No fee<br>
            1st checked bag: No fee up to 23 kg<br>
            2nd checked bag: No fee up to 23 kg
        </p>
    </div>
	</div>	
		<div id="userterms" style="display:none">
		<h4><?php echo lang('订票须知') ?></h4>
		<p>•	<?php echo lang('所有价格以出票时的即时价格为准。') ?><br />
        •	<?php echo lang('一般订位只能保留24小时，某些特定的航空公司（如西捷航空和其他美国航空公司,国航等亚洲航空公司）会在晚上自动取消没有出票的预订。') ?><br />
        •	<?php echo lang('请正确、详细的输入您的联系信息并保持通讯畅通，以便机票预订员能及时地与您联系；如联系人、联系电话等资料有误，此订单将被视为无效订单作取消处理。收到你的订单后，我们会在工作时间内尽快联系你确认出票。为了方便联系，我们建议你提供加拿大或美国的电话。如因联系不上你而导致无法确认出票，我们将保留取消预订的权利。') ?><br />
        •	<?php echo lang ('我司不支持三家航空公司在同一张机票上的预订。如果你预订上了，我司保留拒绝出票的权利。') ?><br />
        •	<?php echo lang ('请确认旅客信息（包括名字，性别，出生年月日）必须与他的旅行时护照上的信息一致。如预订姓名与证件姓名不一致将无法登机，所造成的损失由预订人自行承担。以上信息的缺失都有可能造成无法出票。请确保旅行结束日比证件有效期至少早6个月。') ?><br />
        •	<?php echo lang ('务必在出票前确保持有行程所需的有效签证及旅行证件。') ?><br />
        •	<?php echo lang ('我们的工作时间是加拿大东岸时间周一到周五10:00 到18:00, 周六日 11:00到16:00联系电话 1-866-255-2188。') ?><br />
		•	<?php echo lang ('单程票限制：1）只有长期留学生签证、移民签证、一年以上的长期工作签证等签证类型才可以购买单程票，其他商务和旅游类型的短期签证必须购买往返票；否则会由于目的地国家的相关政策限制，您将无法办理乘机和出境手续，请您务必确认核实后再购买单程票。2）华景国际旅行网仅提供票务信息，任何临时发生的航班取消或变动请直接与相关航空公司联系，华景国际旅行网不承担责任。') ?><br/>
		•	<?php echo lang('重复订单：请勿提交重复订单（同一位登机人相同时间段两张以上的机票订单），如提交重复订单导致了重复出票，所产生的退票损失将由预订人全部承担。') ?><br />	
	    •	<?php echo lang('修改订单：如果您需要修改订单，请致电1-866-255-2188，由机票预订员进行修改。') ?><br />
        •	<?php echo lang('必须按照顺序使用机票。') ?>
        </p>	
		<button id="agreeandpay"><?php echo lang('我同意，并确认上述行程') ?></button><button onclick="tjq('#soap-popupbox').click();"><?php echo lang("我不同意") ?></button>
	</div>
    </div>
</div>
<script>
var language = '<?php echo $lang; ?>';
var error={};
error["10001"]="<?php echo lang('必填') ?>";
error["10002"]="<?php echo lang('请输入所有信息') ?>";
error["10003"]="<?php echo lang('日期不能小于上一程日期') ?>";
error["10004"]="<?php echo lang('婴儿乘客不能多于成人乘客') ?>";
var dayNamesMin=['<?php echo lang("日") ?>','<?php echo lang("一") ?>','<?php echo lang("二") ?>','<?php echo lang("三") ?>','<?php echo lang("四") ?>','<?php echo lang("五") ?>','<?php echo lang("六") ?>'];
var monthNames=['<?php echo lang("一月") ?>','<?php echo lang("二月") ?>','<?php echo lang("三月") ?>','<?php echo lang("四月") ?>','<?php echo lang("五月") ?>','<?php echo lang("六月") ?>','<?php echo lang("七月") ?>','<?php echo lang("八月") ?>','<?php echo lang("九月") ?>','<?php echo lang("十月") ?>','<?php echo lang("十一月") ?>','<?php echo lang("十二月") ?>'];
var monthNamesShort=['<?php echo lang("一月") ?>','<?php echo lang("二月") ?>','<?php echo lang("三月") ?>','<?php echo lang("四月") ?>','<?php echo lang("五月") ?>','<?php echo lang("六月") ?>','<?php echo lang("七月") ?>','<?php echo lang("八月") ?>','<?php echo lang("九月") ?>','<?php echo lang("十月") ?>','<?php echo lang("十一月") ?>','<?php echo lang("十二月") ?>'];
var daynames=dayNames=['<?php echo lang("星期日") ?>','<?php echo lang("星期一") ?>','<?php echo lang("星期二") ?>','<?php echo lang("星期三") ?>','<?php echo lang("星期四") ?>','<?php echo lang("星期五") ?>','<?php echo lang("星期六") ?>'];
var dayNamesShort=['<?php echo lang("周日") ?>','<?php echo lang("周一") ?>','<?php echo lang("周二") ?>','<?php echo lang("周三") ?>','<?php echo lang("周四") ?>','<?php echo lang("周五") ?>','<?php echo lang("周六") ?>'];

	function getCalSpan(year,month)
{
 if ("<?php echo lang('英文')?>" !='English')

	return '<span class="calendar-month">'+year+'年'+month+'月</span><table><thead><tr>';
else
	 return '<span class="calendar-month">'+monthNames[month-1]+'. '+year+'</span><table><thead><tr>';
	
}	
function  loadInternaryDetail(contenttype,data,clear)
{
    var modal=tjq('div#flightDetail');

    var detail=data;
    if (typeof data=="string"){    
        detail=JSON.parse(data);
    }
    var triptype="";
    tjq('#orderdetails').show();
    switch(contenttype){
        case "go":
            triptype="【<?php echo lang('去程') ?>】";
		
            break;
        case "return":
            triptype="【<?php echo lang('回程') ?>】";
            break;
    }
    if (detail.internaryTitle!=undefined){
        triptype="【"+detail.internaryTitle+"】";
    }
    if (clear == undefined || clear!="no"){
        tjq(modal).find('[notemplate=yes]').remove();
    }
    tjq('#userterms').hide();


    var modal_head=tjq('#orderdetails').children('h4:not([notemplate])').clone();
    tjq(modal_head).find('#triptype').html(triptype);
    tjq(modal_head).find('#DepartureCityName').html(getAirportNameByCode(detail.DepartureCity));
    tjq(modal_head).find('#ArrivateCityName').html(getAirportNameByCode(detail.ArrivateCity));
    tjq(modal_head).find('#ElapsedTimeTotalHours').html(detail.ElapsedTimeTotalHours);
    tjq(modal_head).find('#ElapsedTimeTotalHoursLeftMins').html(detail.ElapsedTimeTotalHoursLeftMins);
    tjq(modal_head).attr('notemplate','yes');
    tjq(modal_head).css("display","");
    if (request.tripType=="roundtrip"){

    }else{
        tjq(modal_head).children('img').replaceWith('<img src="assets/images/exchange-oneway.jpg" />');
    }
    tjq('#orderdetails').append(modal_head);
	
	
    var segs=JSON.parse(detail.Segment);    
    tjq(segtemplate).attr('notemplate','yes');
    tjq(segtemplate).css("display","");
	
	
    var internarytemplate=tjq(modal).find('div#internarylist:not([notemplate])').clone();
    tjq(internarytemplate).attr('notemplate','yes');
    tjq(internarytemplate).css("display","");
	
    var segtemplate=tjq(modal).find('div#internarylist:not([notemplate])').children('dl#template').clone();

    var segcount=0;
    var lastarrivetime=undefined;
    var lastcity=undefined;
    var lastteminnal=undefined;
    tjq.each(segs,function(idx,seg){
        segcount++;
        var departdate=new Date(seg.DepartureDateTime+"Z");
        if (lastarrivetime!=undefined){
            var stoptemplate=tjq(modal).find('div#internarylist:not([notemplate=yes])').children('h5').clone();
            tjq(stoptemplate).attr('notemplate','yes');
            tjq(stoptemplate).css("display","");
            var diff  = departdate.getTime() - lastarrivetime.getTime();
            var diffhours= Math.floor(diff/1000/60/60);
            var diffminutes= Math.floor(diff/1000/60) % 60;
            var chanageairport="";
            var changeterminal="";
            if (lastcity!=seg.From){
                chanageairport="，<?php echo lang('需要转到另一个机场') ?>";
            }
            if (chanageairport=="" && lastteminnal!=seg.From_TerminalID){
                changeterminal="，<?php echo lang('需要转到另一个航站楼') ?>";
            }

            tjq(stoptemplate).html('<?php echo lang("在") ?>'+getCityNameByCode(seg.From)+'<?php echo lang("的中转时间") ?>：'+diffhours+"<?php echo lang('时') ?>"+diffminutes+"<?php echo lang('分') ?>"+chanageairport+changeterminal);
            tjq(internarytemplate).append(stoptemplate);
        }
        
        var tmpseg=tjq(segtemplate).clone();        
        tjq(tmpseg).attr('notemplate','yes');
        tjq(tmpseg).css("display","");
        
        tjq(tmpseg).find('dt img').replaceWith('<img alt="" src="assets/images/logo/'+seg.Airline+'.png">');

        tjq(tmpseg).find('ul#first_row li:nth-child(1)').html(request.formatDateYMD(departdate)+ ' ' + request.formatAMPM(departdate)+'<br>'+getAirportNameByCode(seg.From)+'('+seg.From+')<br><?php echo lang("航站楼") ?>：'+seg.From_TerminalID);
		var flightdetail=seg.Airline+" "+seg.Flightnumber+","+seg.AirEquipType+","+seg.MarketingCabin+" <?php echo lang('舱') ?>";
		if (seg.Airline != seg.OperatingAirline) {
            if (language  == 'Chinese') {
                var operatingAirlineDetail = "<?php echo lang('由') ?>" + seg.OperatingAirline + "<?php echo lang('运营') ?>";
            } else {
                var operatingAirlineDetail = "<?php echo lang('Operated by ') ?>" + seg.OperatingAirline;
            }
        } else {
            var operatingAirlineDetail = '';
        }
        tjq(tmpseg).find('ul#first_row li:nth-child(2)').html(seg.ElapsedTimeTotalHours+'<?php echo lang("时") ?>'+seg.ElapsedTimeTotalHoursLeftMins+'<?php echo lang("分") ?><br /><i class="fa fa-long-arrow-right"></i><br>'+flightdetail+'<br />'+operatingAirlineDetail);
        
        var arrivedate=new Date(seg.ArrivalDateTime+"Z");
        tjq(tmpseg).find('ul#first_row li:nth-child(3)').html(request.formatDateYMD(arrivedate)+ ' ' + request.formatAMPM(arrivedate)+'<br>'+getAirportNameByCode(seg.To)+'('+seg.To+')<br><?php echo lang("航站楼") ?>：'+seg.To_TerminalID);

        lastarrivetime=arrivedate;
        lastcity=seg.To;
        lastteminnal=seg.To_TerminalID;
        tjq(internarytemplate).append(tmpseg);
    });
	tjq('#orderdetails').append(internarytemplate);
    
    
}
</script>
<footer id="footer">
                <div class="footer-wrapper"  <?php if (isset($_SESSION["showhead"]) && $_SESSION["showhead"]=="no"){ ?> style="display:none" <?php } ?>>
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-6 col-md-3" id="newsletter">
                                <h3><?php echo lang("更多优惠信息") ?></h3>
                                <span><?php echo lang("请输入您的邮箱地址") ?></span>
                                <div>
                                    <input type="text" class="input-text" placeholder="<?php echo lang("您的邮箱地址") ?>" />
                                    <button type="submit" id="newsletter_submit" onclick="loadView('vp-get-bus-mtl');"><?php echo lang("订阅") ?></button>
                                </div>
                            </div>
                            <div class="col-sm-6 col-md-3">
                                <h3><?php echo lang("版权所有") ?></h3>
                             © 2005-2017 Sinorama [ OPC:702569 ] <?php echo lang("您所看到的价格只在当前页面有效，若离开该页面，价格可能会有变动。") ?>
                            </div>
                            <div class="col-sm-12 col-md-6">
                                <div class="col-sm-6 col-md-3">
                                    <h3><?php echo lang("地址") ?></h3>
                                    998, boul. St-Laurent, Suite 518, POX.008 Montreal, QC, Canada H2Z 9Y9
                                </div>
                                <div class="col-sm-6 col-md-9">
                                    <h3><?php echo lang("相关讯息") ?></h3>
									
                                    <ul class="row">
										<!--
                                        <li class="col-sm-6 col-md-6"><a href="assets/#">Politica de Privacidad</a></li>
                                        <li class="col-sm-6 col-md-6"><a href="assets/#">Condiciones de Uso</a></li>
                                        <li class="col-sm-6 col-md-6"><a href="assets/#">Terminos Condiciones</a></li>
                                        <li class="col-sm-6 col-md-6"><a href="assets/#">Acerca de Nosotros</a></li>
                                        <li class="col-sm-6 col-md-6"><a href="assets/#">Comentarios de los clientes</a></li>
                                        <li class="col-sm-6 col-md-6"><a href="assets/#">Agente Iniciar sesión</a></li>
-->
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </footer>
        </div>

        <!-- Javascript -->
        <script type="text/javascript" src="assets/js/jquery-1.11.1.min.js"></script>
        <script type="text/javascript" src="assets/js/jquery.noconflict.js"></script>
        <script type="text/javascript" src="assets/js/modernizr.2.7.1.min.js"></script>
        <script type="text/javascript" src="assets/js/jquery-migrate-1.2.1.min.js"></script>
        <script type="text/javascript" src="assets/js/jquery.placeholder.js"></script>
        <script type="text/javascript" src="assets/js/jquery-ui.1.10.4.min.js"></script>

        <!-- Twitter Bootstrap -->
        <script type="text/javascript" src="assets/js/bootstrap.js"></script>


        <!-- load revolution slider scripts -->
        <script type="text/javascript" src="assets/components/revolution_slider/js/jquery.themepunch.plugins.min.js"></script>
        <script type="text/javascript" src="assets/components/revolution_slider/js/jquery.themepunch.revolution.min.js"></script>

        <!-- load BXSlider scripts -->
        <script type="text/javascript" src="assets/components/jquery.bxslider/jquery.bxslider.min.js"></script>

        <!-- load FlexSlider scripts -->
        <script type="text/javascript" src="assets/components/flexslider/jquery.flexslider-min.js"></script>

        <!-- Google Map Api -->
        <script src="http://maps.googleapis.com/maps/api/js?v=3.exp&amp;sensor=false"></script>

        <script type="text/javascript" src="assets/js/calendar.js"></script>

        <!-- parallax -->
        <script type="text/javascript" src="assets/js/jquery.stellar.min.js"></script>

        <!-- waypoint -->
        <script type="text/javascript" src="assets/js/waypoints.min.js"></script>

        <script type="text/javascript" src="assets/js/moment.js"></script>
        



        <!-- load page Javascript -->
        
        <script type="text/javascript" src="assets/js/scripts.js"></script>
        <script type="text/javascript" src="assets/js/Myrequest.js?v=201705251"></script>


		<!-- <script type="text/javascript" src="assets/js/cover.js"></script> -->		
		<script src="http://<?php echo $GLOBALS["search_server"]; ?>:9999/air_api.js?v=201703303"></script>
		<script src="http://<?php echo $GLOBALS["crud_server"]; ?>/getAirlines.php?v=20170529"></script>
		<script src="http://<?php echo $GLOBALS["crud_server"]; ?>/getAirplanemodels.php"></script>
		<script src="http://<?php echo $GLOBALS["crud_server"]; ?>/getAirportCode.php"></script>


        <?php include( $id . '-foot.php'); ?>

		<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-17008322-14', 'auto');
  ga('send', 'pageview');

</script>

		<script>
			 tjq(document).ready(function () {
				tjq.each(tjq("label[placehold]"), function () {
                    tjq(this).html(tjq(this).attr("placehold"));
                });
			 });
			function checkForm(formid){
				
                var passchecked = true;
                tjq.each(tjq('#'+formid).find("input"), function () {
                if (tjq(this).is('[required]') && tjq(this).is(':visible')) {
                                                        var val = tjq(this).val();
                                                        if (tjq(this).is("[data]")) {
                                                            val = tjq(this).attr('data');
                                                        }
                                                        if (val == "" || val.length == 0) {
                                                            passchecked = false;
                                                            request.showRequired(tjq(this));
                                                        } else {
                                                            request.removeRequired(tjq(this));
                                                        }
                                                    }
                                                });
			}
		</script>

        <script type="text/javascript" src="assets/js/theme-scripts.js?v=20170525"></script>

		<script type="text/javascript">
		 
        tjq(document).ready(function() {
            
          
            
            tjq(".goto-writereview-pane").click(function(e) {
                e.preventDefault();
                tjq('#hotel-features .tabs a[href="#hotel-write-review"]').tab('show')
            });
            
            // editable rating
            tjq(".editable-rating.five-stars-container").each(function() {
                var oringnal_value = tjq(this).data("original-stars");
                if (typeof oringnal_value == "undefined") {
                    oringnal_value = 0;
                } else {
                    //oringnal_value = 10 * parseInt(oringnal_value);
                }
                tjq(this).slider({
                    range: "min",
                    value: oringnal_value,
                    min: 0,
                    max: 5,
                    slide: function( event, ui ) {
                        
                    }
                });
            });
        });
		/* 		
        tjq('a[href="#map-tab"]').on('shown.bs.tab', function (e) {
            var center = panorama.getPosition();
            google.maps.event.trigger(map, "resize");
            map.setCenter(center);
        });
        tjq('a[href="#steet-view-tab"]').on('shown.bs.tab', function (e) {
            fenway = panorama.getPosition();
            panoramaOptions.position = fenway;
            panorama = new google.maps.StreetViewPanorama(document.getElementById('steet-view-tab'), panoramaOptions);
            map.setStreetView(panorama);
        });
        var map = null;
        var panorama = null;
        var fenway = new google.maps.LatLng(48.855702, 2.292577);
        var mapOptions = {
            center: fenway,
            zoom: 12
        };
        var panoramaOptions = {
            position: fenway,
            pov: {
                heading: 34,
                pitch: 10
            }
        };
        function initialize() {
            tjq("#map-tab").height(tjq("#hotel-main-content").width() * 0.6);
            map = new google.maps.Map(document.getElementById('map-tab'), mapOptions);
            panorama = new google.maps.StreetViewPanorama(document.getElementById('steet-view-tab'), panoramaOptions);
            map.setStreetView(panorama);
        }
        google.maps.event.addDomListener(window, 'load', initialize);
*/
	function change_language(taget_lang){
		tjq.get("/flight_front/index.php?id=change_lang&change_lang="+taget_lang+"&", function( data ) {
           location.reload();
	    })
	}
    </script>
	
    


    </body>
</html>