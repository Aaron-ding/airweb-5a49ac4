<?php

$singleFees = array(
    'CAD' => 1630,
    'USD' => 1630,
    'EUR' => 1630
);
$adultDeposits = array(
    'CAD' => 1200,
    'USD' => 1200,
    'EUR' => 1200
);
$childDeposits = array(
    'CAD' => 0,
    'USD' => 0,
    'EUR' => 0
);
$adultTourfees = array(
    'CAD' => array(
        'E' => 0,
        'S' => 399,
        'T' => 499
    ),
    'USD' => array(
        'E' => 0,
        'S' => 399,
        'T' => 499
    ),
    'EUR' => array(
        'E' => 0,
        'S' => 399,
        'T' => 499
    )
);
$childTourfees = array(
    'CAD' => array(
        'E' => 1999,
        'S' => 2698,
        'T' => 2798
    ),
    'USD' => array(
        'E' => 1999,
        'S' => 2698,
        'T' => 2798
    ),
    'EUR' => array(
        'E' => 1999,
        'S' => 2698,
        'T' => 2798
    )
);
$ticketsTipfees = array(
    'CAD' => 1362,
    'USD' => 1362,
    'EUR' => 959
);