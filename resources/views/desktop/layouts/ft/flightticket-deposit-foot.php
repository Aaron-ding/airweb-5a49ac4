<script type="text/javascript">
var currency = '<?php echo $currency; ?>';
var request = new Request();

tjq(document).ready(function () {
    
    tjq('#submityorder').click(function (e) {
        e.preventDefault();
        tjq('#book-submit-errors').html('');

        if (!tjq('#checkTerms').is(':checked')) {
            tjq('#book-submit-errors').html('<i class="fa fa-times"></i><?php echo lang("Please confirm the information.") ?> ');
            return
        }

        passchecked = true;

        tjq.each(tjq('#bookingform').find("input,select"), function () {
            if (tjq(this).is('[required]') && tjq(this).is(':visible')) {

                var val = request.getValFromObj(tjq(this));
                var showerrorobject = tjq(this);
                if (tjq(this).attr('groupid') != undefined) {
                    showerrorobject = tjq('#' + tjq(this).attr('groupid'));
                }

                if (val == undefined || val == "" || val.length == 0) {
                    passchecked = false;
                    request.showRequired(showerrorobject);
                } else {
                    request.removeRequired(showerrorobject);
                }
            }
        });

        if (passchecked) {
            if (!request.isEmail(tjq('#email').val())) {
                passchecked = false;
                request.showRequired(tjq('#email'));
            } else {
                request.removeRequired(tjq('#email'));
            }
        }

        if (!passchecked) {
            tjq('#book-submit-errors').html('<i class="fa fa-times"></i><?php echo lang("输入信息有误，请检查!") ?> ');
            return;
        } else {
            tjq(this).prop('disabled', true);
            tjq(this).hide();
            tjq('#doingbookoing').show();

            var data = {};
            data['quantity'] = tjq('#quantity').val();
            data['fileno'] = tjq.trim(tjq('#fileno').val());
            data['agencyname'] = tjq.trim(tjq('#agencyname').val());
            data['agentname'] = tjq.trim(tjq('#agentname').val());
            data['email'] = tjq.trim(tjq('#email').val());
            data['tel'] = tjq.trim(tjq('#tel').val());
            data['currency'] = currency;

            FT_c_flightticket_deposit(data, successBooking);
        }
    });
    
    tjq('#quantity').change(function () {
        updateFee();
    });

    updateFee();
});

var currencyPrix = '<?php echo $currencyPrix; ?>';
var $deposit = '<?php echo $deposit; ?>';

function updateFee() {
    var quantity = tjq('#quantity').val();
    tjq('#qty').text(quantity);

    var total = parseInt($deposit) * parseInt(quantity);
    tjq('#totalfare').text(currencyPrix + total);
}

function successBooking(data) {
    if (data[0] != 'error') {
        tjq('#doingbookoing').hide();
        tjq('#book-submit-errors').html('<i class="fa fa-check"></i><?php echo lang("Your order has been submitted!") ?>');

        var result = data.split('|');
        var orderId = result[0];
        var token = result[1];

        window.location.href = 'index.php?id=flightticket-deposit-order&orderId=' + orderId + '&token=' + token + '&fullsite=yes&change_lang=English&';
    } else {
        tjq('#doingbookoing').hide();
        tjq('#submityorder').prop('disabled', false);
        tjq('#submityorder').show();
        
        tjq('#book-submit-errors').html('<i class="fa fa-times"></i>' + data[1]);
    }
}
</script>