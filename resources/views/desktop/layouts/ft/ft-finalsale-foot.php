<script type="text/javascript" src="assets/js/typeahead.bundle.js"></script>
<script type="text/javascript" src="assets/js/airport.js"></script>
<script type="text/javascript">

var request = new Request();

tjq(document).ready(function () {

    tjq('#submityorder').click(function (e) {
        e.preventDefault();
        tjq('#book-submit-errors').html('');

        if (!tjq('#checkTerms').is(':checked')) {
            tjq('#book-submit-errors').html('<i class="fa fa-times"></i><?php echo lang("请阅读本次活动规则，并选择已阅读。") ?> ');
            return
        }

        passchecked = true;

        tjq.each(tjq('#bookingform').find("input,select"), function () {
            if (tjq(this).is('[required]') && tjq(this).is(':visible')) {

                var val = request.getValFromObj(tjq(this));
                var showerrorobject = tjq(this);
                if (tjq(this).attr('groupid') != undefined) {
                    showerrorobject = tjq('#' + tjq(this).attr('groupid'));
                }

                if (val == undefined || val == "" || val.length == 0) {
                    passchecked = false;
                    request.showRequired(showerrorobject);
                } else {
                    request.removeRequired(showerrorobject);
                }
            }
        });

        if (passchecked) {
            if (!request.isEmail(tjq('#email').val())) {
                passchecked = false;
                request.showRequired(tjq('#email'));
            } else {
                request.removeRequired(tjq('#email'));
            }
        }

        if (!passchecked) {
            tjq('#book-submit-errors').html('<i class="fa fa-times"></i><?php echo lang("输入信息有误，请检查!") ?> ');
            return;
        } else {
            tjq(this).prop('disabled', true);
            tjq(this).hide();
            tjq('#doingbookoing').show();

            var data = {};
            data['firstname'] = tjq('#firstname').val();
            data['middlename'] = tjq('#middlename').val();
            data['lastname'] = tjq('#lastname').val();
            data['passport'] = tjq('#passport').val();
            data['email'] = tjq('#email').val();
            data['tel'] = tjq('#tel').val();
            data['address'] = tjq('#address').val();
            data['city'] = tjq('#city').val();
            data['country'] = tjq('#country').val();
            data['postalcode'] = tjq('#postalcode').val();
            data['couponquantity'] = tjq('#couponquantity').val();
            data['remarks'] = tjq('#remarks').val();
            
            request.booking = data;

            FT_c_finalsale_doBooking(request, successBooking, errorBooking);
        }
    });
    
    tjq('#couponquantity').change(function () {
        updateFee();
    });

    updateFee();
});

function updateFee() {
    var currencyShow = tjq('#currencyShow').val();
    var quantity = tjq('#couponquantity').val();
    var basefee = tjq('#basefee').val();

    var basefee = parseInt(basefee);
    var totalfee = parseInt(basefee) * parseInt(quantity);

    tjq('#basefare').text(currencyShow + basefee);
    tjq('#quantity').text(quantity);
    tjq('#totalfare').text(currencyShow + totalfee);
}

function successBooking(data) {
    if (data[0] != 'error') {
        tjq('#doingbookoing').hide();
        tjq('#book-submit-errors').html('<i class="fa fa-check"></i><?php echo lang("非常感谢您选择华景！您的预定已经成功提交！") ?>');

        var result = data.split('|');
        var orderId = result[0];
        var token = result[1];

        window.location.href = 'index.php?id=ft-finalsale-order&orderId=' + orderId + '&token=' + token + '&fullsite=yes&change_lang=Chinese&';
    } else {
        tjq('#doingbookoing').hide();
        tjq('#submityorder').prop('disabled', false);
        tjq('#submityorder').show();
        
        tjq('#book-submit-errors').html('<i class="fa fa-times"></i>' + data[1]);
    }
}

function errorBooking(msg) {
    console.log(msg);
}
</script>