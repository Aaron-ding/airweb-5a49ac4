<!DOCTYPE html>
<!--[if IE 8]>          <html class="ie ie8"> <![endif]-->
<!--[if IE 9]>          <html class="ie ie9"> <![endif]-->
<!--[if gt IE 9]><!-->  <html>
    <!--<![endif]-->
<?php global $id; ?>
    <head>
        <!-- Page Title -->
        <title><?php echo lang('portal_title') ?></title>

        <!-- Meta Tags -->
        <meta charset="utf-8">
        <meta name="keywords" content="<?php echo lang('portal_keywords') ?>" />
        <meta name="description" content="<?php echo lang('portal_description') ?>">
        <meta name="author" content="sinorama">

        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <!-- Theme Styles -->
        <link rel="stylesheet" href="assets/css/bootstrap.min.css">
        <link rel="stylesheet" href="assets/css/font-awesome.min.css">
        <link href="https://fonts.googleapis.com/css?family=Lato:300,400,500,700" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="assets/css/animate.min.css">

        <!-- Current Page Styles -->
        <link rel="stylesheet" type="text/css" href="assets/components/revolution_slider/css/settings.css" media="screen" />
        <link rel="stylesheet" type="text/css" href="assets/components/revolution_slider/css/style.css?v=20161212" media="screen" />
        <link rel="stylesheet" type="text/css" href="assets/components/jquery.bxslider/jquery.bxslider.css" media="screen" />
        <link rel="stylesheet" type="text/css" href="assets/components/flexslider/flexslider.css" media="screen" />
        <style>
			<?php 			
			if ($GLOBALS["branch"]=="dev"){ ?>
					section#content{				
					
					background:url(/flight_front/assets/images/fttest_bg.jpg) repeat!important;}
			<?php } ?>
		</style>	
		
		<!-- chart js -->
		<script src="assets/js/Chart.min.js"></script>
		
        <!-- Main Style -->
        <link id="main-style" rel="stylesheet" href="assets/css/style.css?v=20170524">

        <!-- Custom Styles -->
        <link rel="stylesheet" href="assets/css/custom.css">

        <!-- Updated Styles -->
        <link rel="stylesheet" href="assets/css/updates.css">

        <!-- Responsive Styles -->
        <link rel="stylesheet" href="assets/css/responsive.css?v=20170109">

        <!-- CSS for IE -->
                    <!--[if lte IE 9]>
                                            <link rel="stylesheet" type="text/css" href="assets/css/ie.css" />
                                        <![endif]-->
        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
                    <!--[if lt IE 9]>
                                          <script type='text/javascript' src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
                                          <script type='text/javascript' src="http://cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.js"></script>
                                        <![endif]-->
    </head>
    <body>
        <div id="page-wrapper">
            <header id="header-normal" class="navbar-static-top" <?php if (isset($_SESSION["showhead"]) && $_SESSION["showhead"]=="no"){ ?> style="display:none" <?php } ?>>
                <div class="topnav">
                    <div class="container">
                        <h1 class="logo navbar-brand pull-left">
                            <a href="<?php echo WEBSITEURL; ?>" title="Travelo - home">
                                <img src="assets/images/mail-logo.png" alt="SINORAMA HTML5 Template" />
                            </a>
                        </h1>
                        <?php if ($id != 'ft-finalsale-order' && $id != 'ft-finalsale' && $id != 'ft-finalsale-cp') { ?>
                        <ul class="quick-menu pull-right">
                            <!-- <li class="telephone">514-666-0888</li> -->
                            <li class="ribbon">
								<?php if ($lang == "English") { ?>
                                            <a href="javascript:void(0);" id="langDisplay">English</a>
                                 <?php } ?>
                                 <?php if ($lang == "French") { ?>
                                            <a href="javascript:void(0);" id="langDisplay">Français</a>
                                 <?php } ?>
								<?php if ($lang == "Chinese") { ?>
                                            <a href="javascript:void(0);" id="langDisplay">中文</a>
                                 <?php } ?>
                                
                                <ul class="menu mini">
									<?php if ($lang == "Chinese") { ?>
                                    	<li class="active"><a href="javascript:void(0);" title="中文">中文</a></li>
									<?php }else{?>
										<li> <a href="javascript:change_language('Chinese');" title="中文">中文</a></li>
									<?php } ?>
									<?php if ($lang == "English") { ?>
										<li class="active"><a href="javascript:void(0);" title="English">English</a></li>
									<?php }else{?>
									<li> <a href="javascript:change_language('English');" title="English">English</a></li>
									<?php } ?>
                                    <?php if ($lang == "French") { ?>
                                        <li class="active"><a href="javascript:void(0);" title="Français">Français</a></li>
                                    <?php }else{?>
                                        <li> <a href="javascript:change_language('French');" title="Français">Français</a></li>
                                    <?php } ?>
									
                                    <!-- <li><a href="assets/#" title="French">French</a></li>
                                    
									-->	
                                </ul>
                            </li>
                            <li class="ribbon currency">
                                <?php if ($currency == "CAD") { ?>
                                    <a href="javascript:void(0);" title="CAD">CAD</a>
                                <?php } elseif ($currency == "USD") { ?>
                                    <a href="javascript:void(0);" title="USD">USD</a>
                                <?php } elseif ($currency == "EUR") { ?>
                                    <a href="javascript:void(0);" title="EUR">EUR</a>
                                <?php } ?>
                                <?php if ($id == 'search') { ?>
                                <ul class="menu mini">
                                    <li <?php if ($currency == "CAD") { echo 'class="active"'; } ?>><a href="javascript:void(0);" data="CAD" title="CAD">CAD</a></li>
                                    <li <?php if ($currency == "USD") { echo 'class="active"'; } ?>><a href="javascript:void(0);" data="USD" title="USD">USD</a></li>
                                    <li <?php if ($currency == "EUR") { echo 'class="active"'; } ?>><a href="javascript:void(0);" data="EUR" title="EUR">EUR</a></li>
                                </ul>
                                <?php } ?>
                            </li>
                            <li style="margin-left: 30px;"><a href="javascript:void(0);" style="font-size: 24px;"><i class="fa fa-phone"></i>&nbsp;1-866-255-2188</a></li>
                            <!-- <li><a href="assets/#travelo-login" class="soap-popupbox">LOGIN</a></li> -->
                            <!-- <li><a href="assets/#travelo-signup" class="soap-popupbox">SIGNUP</a></li> -->
                        </ul>
                        <?php } else { ?>
                            <ul class="quick-menu pull-right">
                                <li class="telephone">1-866-255-2188</li>
                            </ul>
                        <?php } ?>
                    </div>
                </div>
                <div id="travelo-signup" class="travelo-signup-box travelo-box">
                    <div class="login-social">
                        <a href="assets/#" class="button login-facebook"><i class="soap-icon-facebook"></i>Login with Facebook</a>
                        <a href="assets/#" class="button login-googleplus"><i class="soap-icon-googleplus"></i>Login with Google+</a>
                    </div>
                    <div class="seperator"><label>OR</label></div>
                    <div class="simple-signup">
                        <div class="text-center signup-email-section">
                            <a href="assets/#" class="signup-email"><i class="soap-icon-letter"></i>Sign up with Email</a>
                        </div>
                        <p class="description">By signing up, I agree to Travelo's Terms of Service, Privacy Policy, Guest Refund olicy, and Host Guarantee Terms.</p>
                    </div>
                    <div class="email-signup">
                        <form>
                            <div class="form-group">
                                <input type="text" class="input-text full-width" placeholder="first name">
                            </div>
                            <div class="form-group">
                                <input type="text" class="input-text full-width" placeholder="last name">
                            </div>
                            <div class="form-group">
                                <input type="text" class="input-text full-width" placeholder="email address">
                            </div>
                            <div class="form-group">
                                <input type="password" class="input-text full-width" placeholder="password">
                            </div>
                            <div class="form-group">
                                <input type="password" class="input-text full-width" placeholder="confirm password">
                            </div>
                            <div class="form-group">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox"> Tell me about Travelo news
                                    </label>
                                </div>
                            </div>
                            <div class="form-group">
                                <p class="description">By signing up, I agree to Travelo's Terms of Service, Privacy Policy, Guest Refund Policy, and Host Guarantee Terms.</p>
                            </div>
                            <button type="submit" class="full-width btn-medium">SIGNUP</button>
                        </form>
                    </div>
                    <div class="seperator"></div>
                    <p>Already a Travelo member? <a href="assets/#travelo-login" class="goto-login soap-popupbox">Login</a></p>
                </div>
                <div id="travelo-login" class="travelo-login-box travelo-box">
                    <div class="login-social">
                        <a href="assets/#" class="button login-facebook"><i class="soap-icon-facebook"></i>Login with Facebook</a>
                        <a href="assets/#" class="button login-googleplus"><i class="soap-icon-googleplus"></i>Login with Google+</a>
                    </div>
                    <div class="seperator"><label>OR</label></div>
                    <form>
                        <div class="form-group">
                            <input type="text" class="input-text full-width" placeholder="email address">
                        </div>
                        <div class="form-group">
                            <input type="password" class="input-text full-width" placeholder="password">
                        </div>
                        <div class="form-group">
                            <a href="assets/#" class="forgot-password pull-right">Forgot password?</a>
                            <div class="checkbox checkbox-inline">
                                <label>
                                    <input type="checkbox"> Remember me
                                </label>
                            </div>
                        </div>
                    </form>
                    <div class="seperator"></div>
                    <p>Don't have an account? <a href="assets/#travelo-signup" class="goto-signup soap-popupbox">Sign up</a></p>
                </div>
            </header>