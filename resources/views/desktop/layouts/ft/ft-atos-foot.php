<script type="text/javascript">
    tjq(document).ready(function () {

        tjq('#atosToPay').click(function (e) {
            e.preventDefault();

            tjq(window.parent.document).find('.payment-opacity-overlay').show();
            tjq('#atos_payform').submit();
        });

        tjq('input[name=atos_payment_method]').click(function () {
            tjq('input[name=atos_payment]').val(tjq(this).val());
        });
    });
</script>