<option value="">None</option>
<option value="BLND">Blind With Guide Dog</option>
<option value="DEAF">Deaf With Guide Dog</option>
<option value="WCHS">Wheelchair - Cannot Ascend/Descend Stairs</option>
<option value="WCHR">Wheelchair - Can Walk and Ascend/Descend Stairs</option>
<option value="WCHC">Wheelchair - Immobile</option>