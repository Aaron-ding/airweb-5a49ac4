<script type="text/javascript" src="https://js.stripe.com/v2/"></script>
<script type="text/javascript" src="assets/js/jquery.maskedinput.min.js"></script>
<!-- 预订页右边栏随滚动条滚动 -->
<script type="text/javascript">

    var request = new Request();
    request.fromJSONString('<?php echo addslashes($_REQUEST["request"]); ?>');
    var personcount =<?php echo $_REQUEST["personcount"]; ?>;
    var currentyear =<?php echo date("Y"); ?>;
    var maxexpiredyear = 30;
    tjq(document).ready(function () {
        tjq('#tel1').mask("999-999-9999");
        tjq('#tel2').mask("999-999-9999");

        tjq(window).scroll(function () {
            var top = tjq(window).scrollTop() + 0;
            var right = tjq(window).scrollLeft() + 0;
            var lastbottom = tjq("#footer").offset().top - 40;
            var stickyTop = tjq("#editInfo").offset().top + tjq("#editInfo").outerHeight();
            var maintop = tjq("#content").offset().top + 40;
            var editfixtop = tjq(window).height() - tjq("#editInfo").outerHeight();
            var scrollBottom = tjq(window).scrollTop() + tjq(window).height();

            if (stickyTop >= lastbottom && stickyTop < (scrollBottom - editfixtop + maintop)) {
                top2 = lastbottom - tjq("#editInfo").outerHeight() - maintop;
                tjq("#editInfo").css({right: right + "px", top: top2 + "px"});
            } else {
                tjq("#editInfo").css({right: right + "px", top: top + "px"});
            }
        });
        request.showProcessBar(50);

        console.log(request);
        var goKey = JSON.parse(request.goKey);
        var returnKey = JSON.parse(request.returnKey);
        if (request.tripType == "multiple") {            
            var interarys=JSON.parse(request.srccity);            
            var internary_template=tjq('h5.box-title').children().not('small').clone();
            interarys.forEach(function(ele,idx){
                if (idx==0){
                    tjq('h5.box-title').find('#departureCity').html(getCityNameByCode(ele.origin));
                    tjq('h5.box-title').find('img').replaceWith('<img src="assets/images/exchange-oneway.jpg">');
                    tjq('h5.box-title').find('#arriveCity').html(getCityNameByCode(ele.destination));
                }else{
                    var tmpinternary=internary_template.clone();                    
                    tjq(tmpinternary).filter('#departureCity').html(getCityNameByCode(ele.origin));
                    tjq(tmpinternary).filter('img').replaceWith('<img src="assets/images/exchange-oneway.jpg">');
                    tjq(tmpinternary).filter('#arriveCity').html(getCityNameByCode(ele.destination));
                    tjq('h5.box-title').children('small').before('<br>');
                    tjq('h5.box-title').children('small').before(tmpinternary);
                }
            });
            
        }else{
            tjq('#departureCity').html(getCityNameByCode(request.srccity));
            tjq('#arriveCity').html(getCityNameByCode(request.destcity));
        }
        tjq('.airimg').replaceWith('<img alt="" src="assets/images/logo/' + request.airline_logo + '.png" />');
        tjq('span#adultcount').text(request.adultcount);
        tjq('span#childcount').text(request.childrencount);
        tjq('span#ifantcount').text(request.ifantcount);
        var triptype = "";
        if (request.tripType == "roundtrip") {
            tjq('h5.box-title').children('img').replaceWith('<img src="assets/images/exchange-full.jpg">');
            triptype = "<?php echo lang('往返') ?>";
        }
        if (request.tripType == "oneway") {
            triptype = "<?php echo lang('单程') ?>";
            tjq('h5.box-title').children('img').replaceWith('<img src="assets/images/exchange-oneway.jpg">');
        }
        if (request.tripType == "multiple") {
            triptype = "<?php echo lang('多程') ?>";
            tjq('h5.box-title').children('img').replaceWith('<img src="assets/images/exchange-oneway.jpg">');
        }
        
        tjq('#triptype').text(triptype);
        tjq('#basefare').text(request.currencyShow + request.basefee);
        tjq('#taxfee').text(request.currencyShow + request.taxfee);
        tjq('#totalfare').text(request.currencyShow + request.totalfee);

        var detail_template = tjq('.constant-column-3.timing.clearfix').clone();
        var goseglist = JSON.parse(request.goKey);
        var returnlist = JSON.parse(request.returnKey);
	
        tjq.each(goseglist.concat(returnlist), function (segidx, segval) {
			if (segval==""){return;}
            var tmptemplate = tjq(detail_template).clone();
            tjq(tmptemplate).css("display", "");
            tjq(tmptemplate).find('div.check-in').children('label').text(getCityNameByCode(segval.From));
            tjq(tmptemplate).find('div.check-out').children('label').text(getCityNameByCode(segval.To));
            
            var departdate = new Date(segval.DepartureDateTime+"Z");
            tjq(tmptemplate).find('div.check-in').children('span').html(request.formatDateYMD(departdate) + '<br>' + request.formatAMPM(departdate));            
            var arrivedate = new Date(segval.ArrivalDateTime+"Z");
            tjq(tmptemplate).find('div.check-out').children('span').html(request.formatDateYMD(arrivedate) + '<br>' + request.formatAMPM(arrivedate));
            tjq(tmptemplate).find('div.duration').children('span').html(segval.ElapsedTimeTotalHours + "<?php echo lang('时') ?>" + segval.ElapsedTimeTotalHoursLeftMins + "<?php echo lang('分') ?>");
            tjq('.constant-column-3.timing.clearfix').parent().append(tmptemplate);
        });




        for (var i = 2; i <= personcount; i++) {
            var newperson = tjq('[person-index=1]').clone();
            tjq(newperson).attr('person-index', i);
            var tmphead = tjq(newperson).children('h3').html();
            tjq(newperson).children('h3').html(tmphead + i);
            tjq(newperson).find("[name=gender]").removeAttr("checked");
            tjq(newperson).find("[name=gender]").removeAttr("disabled");
            tjq(newperson).find("[name=gender]").attr("name", "gender" + i);
            tjq(newperson).find("#birthday_1").attr('vid', "birthday");
            tjq(newperson).find("#birthday_1").attr('id', "birthday_" + i);
            tjq(newperson).find("label[for=birthday_1]").attr("for", "birthday_" + i);
            tjq(newperson).find("#passportexpired_1").attr('vid', "passportexpired");
            tjq(newperson).find("#passportexpired_1").attr('id', "passportexpired_" + i);
            tjq(newperson).find("label[for=passportexpired_1]").attr("for", "passportexpired_" + i);
            tjq('[person-index=' + (i - 1) + ']').after(newperson);
        }
        for (var i = 1; i <= personcount; i++) {
            tjq('#contactindex')
                    .append(tjq("<option></option>")
                            .attr("value", i)
                            .text("<?php echo lang('联系人为乘客') ?>" + i));
        }
        var tmphead = tjq('[person-index=1]').children('h3').html();
        tjq('[person-index=1]').children('h3').html(tmphead + "1");
        request.showProcessBar(10);
        setTimeout(function () {

            request.showProcessBar(100);
            tjq('#booking-content').show();
        }, 1000);


        for (var i = 0; i < maxexpiredyear; i++) {
            var tmpyear = currentyear + i;
            tjq('#expireddateyear')
                    .append(tjq("<option></option>")
                            .attr("value", tmpyear)
                            .text(tmpyear));
        }


        searchInit();

    });
    function searchInit() {
        var searchform = tjq('form[id=searchagain]');
        searchform.find("input[name=srccity]").val(request.srccity);
        searchform.find("input[name=destcity]").val(request.destcity);
        searchform.find("input[name=departdate]").val(request.departdate);
        searchform.find("input[name=returndate]").val(request.returndate);
        searchform.find("input[name=adultcount]").val(request.adultcount);
        searchform.find("input[name=childrencount]").val(request.childrencount);
        searchform.find("input[name=ifantcount]").val(request.ifantcount);
        searchform.find("input[name=tripType]").val(request.tripType);
        searchform.find("input[name=cabinlevel]").val(request.cabinlevel);
    }
    
    
    tjq('#submityorder').click(function (e) {
        e.preventDefault();
        tjq('#book-submit-errors').html('');        
        if (false == request.checkForm('bookingform')) {
            tjq('#book-submit-errors').html('<i class="fa fa-times"></i><?php echo lang("输入信息有误，请检查!") ?> ');
            return;
        }
        //check passger type count
        var acount = 0;
        var kcount = 0;
        var icount = 0;
        var adultunder18 = 0;
        tjq.each(tjq('select[id=passengertype]'), function () {
            if (tjq(this).val() == "A") {
                if (parseInt(tjq(this).attr('age')) <= 18) {
                    adultunder18++;
                }
                acount++;
            }
            if (tjq(this).val() == "K") {
                kcount++;
            }
            if (tjq(this).val() == "I") {
                icount++;
            }
        });
        if (adultunder18 >= request.adultcount) {
            tjq('#book-submit-errors').html('<i class="fa fa-times"></i><?php echo lang("根据航空公司的规定，年龄小于18岁的乘客不能独立出行") ?> ');
            return;
        }

        if (acount != request.adultcount || kcount != request.childrencount || icount != request.ifantcount) {
            tjq('#book-submit-errors').html('<i class="fa fa-times"></i> <?php echo lang("乘客年龄和询价的乘客类型不符，请检查或重新查询") ?>');
            return;
        }

        var orderobj=this;
        tjq('#agreeandpay').click(function () {
            
            var rs = {};
            var personlist = new Array();
            tjq.each(tjq('.person-information'), function () {
                var personindex = tjq(this).attr('person-index');
                var personobj = tjq(this);
                var person = {};
                person['index'] = personindex;
                tjq.each(tjq(this).find("input,select"), function () {
                    var id = tjq(this).attr('id');
                    if (typeof id == undefined || id == false) {
                        return;
                    }
                    var vid = tjq(this).attr('vid');
                    if (typeof vid !== typeof undefined && vid !== false) {
                        id = vid;
                    }
                    var val = request.getValFromObj(tjq(this));

                    if (id !== undefined && val != undefined) {
                        if (personindex == "contact") {
                            rs[id] = val;
                        } else {
                            person[id] = val;
                        }
                    }

                });
                if (personindex != "contact") {
                    personlist.push(person);
                    rs["passengers"] = personlist;
                }
            });
            
            
            request.booking = rs;            
            console.log(request);
/*
            tjq('#booking-content').hide();
            request.showProcessBar(0);
            
            request.showProcessBar(10);
            
  */          
            tjq('#soap-popupbox').click();
            tjq(orderobj).hide();
            tjq('#doingbookoing').show();
            FT_c_doBooking(request, successBooking, errorBooking);
        });
        tjq('a[onload=loaduserterms]')[0].click();
        return;
    });
    
    
    
    

    function successBooking(msg) {
        console.log(msg);
        if (msg[0]!=undefined && msg[0] == "error") {
            /*
            tjq('extra_error').html('');
            tjq('#booking-content').show();
            tjq('div#main').fadeOut();
            tjq('#bookingerror').height(parseInt(tjq('#editInfo').height()) + 40);
            tjq('#bookingerror').fadeIn();
            tjq('#editInfo').find('a').hide();
            if (msg.length >= 2) {
                tjq('#extra_error').html(msg[1]);
            }

            request.showProcessBar(100);
            */
            tjq('#doingbookoing').hide();
            tjq('#modifyorder').html('<?php echo lang("重新选择"); ?>');
            tjq('#modifyorder').click(function(){
                searchagain();
            });
            tjq('#modifyorder').show();
            if (msg.length >= 2) {
                tjq('#book-submit-errors').html(msg[1]);
            }
        } else {
            /*
            tjq('#PNR').text(msg[0]);
            tjq('#adultcount').text(request["adultcount"]);
            tjq('#childcount').text(request["childrencount"]);
            tjq('#ifantcount').text(request["ifantcount"]);
            tjq('#booking-content').show();
            tjq('div#main').fadeOut();
            tjq('#bookingsuccess').height(parseInt(tjq('#editInfo').height()) + 40);
            tjq('#bookingsuccess').fadeIn();
            tjq('#editInfo').find('a').hide();
            request.showProcessBar(100);
            */
            
            tjq('#submitorder').click(function (e) {
                event.preventDefault();    
                tjq('#submit-errors').html('');
                if (false == request.checkForm('paymentform')) {
                    tjq('#submit-errors').html('<i class="fa fa-times"></i><?php echo lang("输入信息有误，请检查!") ?> ');
                    return;
                }                
                var parentbutton=this;
                //validate coupon
                tjq(parentbutton).hide();
                tjq('#doingpayment').show();
                vlidateCoupon(function () {
                    //start pay by stripe
                    
                    var form = tjq('#paymentform');
                    var expirelist = tjq('#expireddate').val().split("/");
                    if (expirelist.length != 2) {
                        request.showRequired(tjq('#expireddate'));
                        return;
                    }
                    tjq('input[data-stripe=exp-month]').val(expirelist[0].trim());
                    tjq('input[data-stripe=exp-year]').val(expirelist[1].trim());
                                <?php if($GLOBALS["branch"]=="master"){ ?>
                        Stripe.setPublishableKey('pk_live_B5SZw46G2GUaRIWOD1s1YjdW');
                                <?php } ?>
                                <?php if($GLOBALS["branch"]=="dev"){ ?>
                        Stripe.setPublishableKey('pk_test_XEL2ac8zWkzRTSF9NQPfqJ49');
                                <?php } ?>	

                    // Disable the submit button to prevent repeated clicks
                    tjq(form).find('button').prop('disabled', true);
                    Stripe.card.createToken(tjq(form), function stripeResponseHandler(status, response) {
                        var form = tjq('#paymentform');
                        if (response.error) {
                            // Show the errors on the form
                            console.log(response.error.message);
                            //tjq(form).find('.payment-errors').text(response.error.message);
                            tjq('#submit-errors').html('<i class="fa fa-times"></i> <?php echo lang("您的信用卡验证不通过，请检查信用卡信息!") ?>');
                            tjq(form).find('button').prop('disabled', false);
                            tjq(parentbutton).show();
                            tjq('#doingpayment').hide();
                        } else {
                            var token = response.id;
                            
                            msg.booking["stripeToken"]=token;                            
                            FT_c_charge_ticket(msg,function(chargers){
                                tjq('#PNR').text(msg.PNR);
                                tjq('#adultcount').text(request["adultcount"]);
                                tjq('#childcount').text(request["childrencount"]);
                                tjq('#ifantcount').text(request["ifantcount"]);
                                tjq('#booking-content').show();
                                tjq('div#main').fadeOut();
                                tjq('#bookingsuccess').height(parseInt(tjq('#editInfo').height()) + 40);
                                tjq('#bookingsuccess').fadeIn();
                                tjq('#editInfo').find('a').hide();            
                                console.log(chargers);
                            });
                        }

                    });
                    return false;
                    //end pay by stripe
                });
            });
            
            tjq('#doingbookoing').hide();
            tjq('#bookingform input').attr('readonly', 'readonly');
            tjq('.information-box div').html("<?php echo lang("已经为您保留了位置，请尽快付款，超过规定时间，位置将会被释放!"); ?>");
            tjq('.information-box').show();            
            tjq('#go_to_payment').fadeIn("slow");
            tjq('#editInfo').find('a').hide();

        }
        window.onbeforeunload = function () {
            window.location.replace("/ft/index.php?id=search");
            return false;
        }

    }
    function errorBooking(msg) {
        request.showProcessBar(100);
        console.log(msg);
    }
    tjq('#contactindex').change(
            function () {
                var personidx = tjq(this).val();
                if (personidx == 0) {
                    tjq('#contactfirstname').val('');
                    tjq('#contactlastname').val('');
                } else {

                    tjq('#contactfirstname').val(tjq('div[person-index=' + personidx + ']').find('#firstname').val());
                    tjq('#contactlastname').val(tjq('div[person-index=' + personidx + ']').find('#lastname').val());
                }

            }
    );

</script>
<script src="assets/js/card.js"></script>
<script>
    tjq(document).ready(function () {
        var card = new Card({
            form: document.querySelector('form[id=paymentform]'),
            container: '.card-wrapper'
        });
        tjq('#couoncode').on('input', function (e) {
            tjq('[id^=coupon_info]').hide();
        });

    });
    function searchagain() {
	var actionurl=tjq('form[id=searchagain]').attr('action')+"<?php echo $GLOBALS["fullsite"] ?>";
        tjq('form[id=searchagain]').attr('action',actionurl);
        tjq('form[id=searchagain]').attr('action');		
        tjq('form[id=searchagain]').submit();
    }

    function loaduserterms()
    {
        loadflightdetail();
        tjq('#userterms').find('button').show();
        tjq('#userterms').show();
    }
    function loadtermsonly()
    {
        tjq('#userterms').find('button').hide();
        tjq('#orderdetails').hide();
        tjq('#userterms').show();
    }
    function loadflightdetail()
    {
        if (request.tripType == "multiple") {
            var rawlist=JSON.parse(request.goRaw);
            rawlist.forEach(function(ele,idx){
                ele.internaryTitle="行程"+(idx+1);
                if (idx==0){
                    loadInternaryDetail('multiple', ele);
                }else{
                    loadInternaryDetail('multiple', ele, 'no');
                }
            });
        }else{   
            loadInternaryDetail('go', request.goRaw);
            if (request.tripType == "roundtrip") {
                loadInternaryDetail('return', request.returnRaw, 'no');
            }
        }    
    }


    function vlidateCoupon(f) {
        tjq('[id^=coupon_info]').hide();
        tjq('#coupon_extra_error').html("");
        tjq('#coupon_amt').html('');
        if (tjq('#couoncode').val().length > 0) {
            var rs = "success"


            var req = tjq.ajax({
                type: "GET",
                url: "https://booking.sinoramabus.com/website/../quan/coupon_valid.php",
                dataType: "jsonp",
                timeout: 10000,
                data: {coupon_code: tjq('#couoncode').val(), owner: "", tour: "AIRTICKET"}

                //data:{ coupon_code: "TENDBDNXERREP", owner: "zhengdongquan@hotmail.com", owner: "zhengdongquan@hotmail.com",tour:"AIRTICKET",callback:"validcounpon_callback" }


            });
            req.success(function (data) {
                if (data != undefined && typeof data == "object" && data.error == undefined) {
                    tjq('#coupon_amt').html(data.currency + ' ' + data.amt);
                    tjq('#coupon_info_success').show();
                    if (typeof f == "function") {
                        f();
                    }
                } else {
                    tjq('#coupon_info').show();
                    if (typeof f == "function") {
                        tjq('#submit-errors').html('<i class="fa fa-times"></i> <?php echo lang("您输入了代金券，但验证不通过") ?>');
                    }
                }
            });

            req.error(function (data) {
                tjq('#coupon_info').show();
                if (typeof f == "function") {
                    tjq('#submit-errors').html('<i class="fa fa-times"></i> <?php echo lang("您输入了代金券，但验证不通过") ?>');
                }
            });

        } else {
            if (f == undefined) {
                tjq('#coupon_extra_error').html("--<?php echo lang('请输入代金券号') ?>");
                tjq('#coupon_info').show();
            } else {
                f();
            }
        }

    }

    function printbookingconfirmation(e)
    {
        if (tjq("span#PNR").text().length > 0) {
            var PDF = tjq('#printIframe');
            tjq(PDF).attr('src', 'https://<?php  echo $GLOBALS["crud_server"]; ?>/index.php/modules/pnrofqueue/pnrreceipt/' + tjq("span#PNR").text());
        }
    }
    
    
    
    
</script>