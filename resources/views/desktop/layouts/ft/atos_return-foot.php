<script type="text/javascript">

tjq(document).ready(function () {
    countdown();
});

var timeout = 10;

function countdown() {
    var timeleft = tjq("#timeleft");
    timeleft.html('('+timeout+')');

    if (timeout == 0) {
        window.opener=null;
        window.open('','_self');
        window.close();
    } else {
        setTimeout("countdown()", 1000);
    }

    timeout--;
}
</script>