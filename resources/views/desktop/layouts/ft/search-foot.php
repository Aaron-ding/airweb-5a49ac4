<!--<script type="text/javascript" src="assets/js/bootstrap-typeahead.js"></script>-->
<script type="text/javascript" src="assets/js/typeahead.bundle.js"></script>
<script type="text/javascript" src="assets/js/airport.js?v=20170405"></script>
<script type="text/javascript" src="assets/js/airlines.js?v=20170529"></script>
<script src="assets/components/bootstrap-modal/js/bootstrap-modal.js"></script>
<script src="assets/components/bootstrap-modal/js/bootstrap-modalmanager.js"></script>
<script src="assets/js/ui-modals.js"></script>
<script type="text/javascript" src="assets/js/clipboard.min.js"></script>

<script type="text/javascript">
    var lang = '<?php echo $lang; ?>';
    var itineraries;
    var dates = [];
    var highlightDates = [];

    var monthNames = [
        "January", "February", "March",
        "April", "May", "June", "July",
        "August", "September", "October",
        "November", "December"
    ];

    var weekNames_EN = [
        "SUNDAY", "MONDAY", "TUESDAY", "WEDNESDAY",
        "THURSDAY", "FRIDAY", "SATURDAY"
    ];

    var weekNames_CN = [
        "星期日", "星期一", "星期二", "星期三",
        "星期四", "星期五", "星期六"
    ];

    var request = new Request();
    var cal = new Calendar();

    var requestData = <?php echo json_encode($_REQUEST); ?>;
    if (requestData.src != undefined && requestData.src != '') {
        request.src = requestData.src;
    }

    var referer = '<?php echo $_SESSION["referer"]; ?>';
    if (referer != undefined && referer != '') {
        request.referer = referer;
    }

    var post=<?php echo json_encode($_REQUEST); ?>;
    if (post.source != undefined && post.source == 'changeItinerary') {
        if (post.returndate != undefined && post.returndate != '') {
            post.tripType = 'roundtrip';
        } else {
            post.tripType = 'oneway';
        }
    }

    Array.prototype.unique = function() {
        var unique = [];
        for (var i = 0; i < this.length; i++) {
            if (unique.indexOf(this[i]) == -1) {
                unique.push(this[i]);
            }
        }
        return unique;
    };

    function getNMonth(step,fromdate){
        var now = new Date();
        if(fromdate !=undefined && typeof fromdate.getMonth === 'function'){
                now=fromdate;
        }
		
        if (now.getMonth() == 11) {
            var current = new Date(now.getFullYear() + step, 0, 1);
        } else {
            var current = new Date(now.getFullYear(), now.getMonth() + step, 1);
        }
		
        var tmpmonth="0"+(current.getMonth()+1);
        var tmpday="0"+(current.getDate()+1);
        return Array(current.getFullYear(),tmpmonth.substr(tmpmonth.length-2),current.getMonth()+1,tmpday.substr(tmpday.length-2),current.getDate()+1,current);
    }
	
    function getNDay(step,fromdate){        
        var now = new Date();
        if(fromdate !=undefined && typeof fromdate.getMonth === 'function'){
                now=fromdate;
        }			
        var current=new Date(now.setTime( now.getTime() + (step) * 86400000 ));     var monthlist=[1,2,3,4,5,6,7,8,9,10,11,12]
         var days = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
           if ((((1900+current.getYear()) % 4 == 0) && ((1900+current.getYear() % 100 != 0)) || ((1900+current.getYear() % 400 == 0))))
            days[1] = 29;
        else
            days[1] = 28;
        for(var i=0;i<monthlist.length;++i)
        {if (monthlist[i]==(current.getMonth()+1))
            {if (days[i]<(current.getDate()+1))
                { var tmpday="0"+(current.getDate()+1-days[i]);
                    var tmpmonth="0"+(current.getMonth()+2);}
            else {var tmpday="0"+(current.getDate()+1);
            var tmpmonth="0"+(current.getMonth()+1);}
			}}
        
        
        return Array(current.getFullYear(),tmpmonth.substr(tmpmonth.length-2),current.getMonth()+1,tmpday.substr(tmpday.length-2),current.getDate()+1,getNDay);
    }

    tjq(document).ready(function () {
        tjq('.bookit-button').click(function () {
            var from = tjq(this).data('from');
            var to = tjq(this).data('to');
            var departing = tjq(this).data('departing');
            var returning = tjq(this).data('returning');

            var url = 'index.php?id=search&srccity='+from+'&destcity='+to+'&tripType=roundtrip&departdate='+departing+'&returndate='+returning+'&adultcount=1&childrencount=0&infantsseatcount=0&infantslapcount=0&fullsite=yes';
            window.location.href = url;
        });

        tjq('#m_searchform #passengers_select, #searchform #passengers_select').click(function () {
            if (tjq(this).next().is(':visible')) {
                tjq(this).next().slideUp();
            } else {
                tjq(this).next().slideDown();
            }
        });

        tjq('.hp-select-pax-done').click(function () {
            tjq(this).parent().slideUp();
        });

        tjq('a.pass-slide-trigger').click(function (e) {
            e.preventDefault();

            var type = tjq(this).data('type');
            var action = tjq(this).data('action');
            var qty = parseInt(tjq(this).parent().parent().find('.passenger-number-sel').text());
            var total = parseInt(tjq(this).parent().parent().parent().parent().find('.flights-home-pass-total').text());
            if (action == 'decrement') {
                if ((type != 'adult' && qty > 0) || (type == 'adult' && qty > 1)) {
                    qty--;
                    total--;
                }
            } else {
                if (total < 6) {
                    qty++;
                    total++;
                }
            }

            tjq(this).parent().parent().find('.passenger-number-sel').text(qty);
            tjq(this).parent().parent().parent().parent().find('.flights-home-pass-total').text(total);
            tjq(this).parent().parent().parent().find('#' + type + 'count').val(qty);
        });

        var clipboard = new Clipboard('.gobutton, .orderbutton');
        clipboard.on('success', function(e) {
            tjq(e.trigger).after('<span style="color:red;">复制成功</span>');
            e.clearSelection();
        });

        tjq('tr.ffcr-first td div').mouseover(function() {
            tjq(this).addClass('hover');
        }).mouseleave(function() {
            tjq(this).removeClass('hover');
        });

        tjq('.function-roundtrip-advanceSearch-box tr.ffcr-first td').click(function () {
            var date = tjq(this).data('date').split('+');
            var departureDate = date[0];
            var returnDate = date[1];

            tjq('#departdate').val(departureDate);
            tjq('#returndate').val(returnDate);
            submitSearchForm();
        });

        tjq('#btnPrevWeek').click(function() {
            var departureDate = tjq('#departdate').val();
            var date = toDate(departureDate);
            var departureday = date.getTime();
            var settime = departureday - (7 * 86400000);
            var setdate = new Date(settime);
            var today = new Date();
            var nowday = today.getTime();
            if (settime < nowday) {
                setdate = today;
            }

            var setDay = setdate.getDate();
            if (setDay <= 9) {
                setDay = '0' + setDay;
            }

            tjq('#departdate').val(setdate.getFullYear()+'-'+(setdate.getMonth()+1)+'-'+setDay);
            submitSearchForm();
        });

        tjq('#btnNextWeek').click(function() {
            var departureDate = tjq('#departdate').val();
            var date = toDate(departureDate);
            var departureday = date.getTime();
            var settime = departureday + (7 * 86400000);
            var setdate = new Date(settime);

            var setDay = setdate.getDate();
            if (setDay <= 9) {
                setDay = '0' + setDay;
            }

            tjq('#departdate').val(setdate.getFullYear()+'-'+(setdate.getMonth()+1)+'-'+setDay);
            submitSearchForm();
        });

        tjq('.function-oneway-advanceSearch-box tr.ffcr-first td').click(function () {
            var departureDate = tjq(this).data('date');

            tjq('#departdate').val(departureDate);
            submitSearchForm();
        });

        function convertTimeToHHMM(t) {
            var minutes = t % 60;
            var hour = (t - minutes) / 60;
            var timeStr = (hour + "").lpad("0", 2) + ":" + (minutes + "").lpad("0", 2);
            var date = new Date("2014/01/01 " + timeStr + ":00");
            var hhmm = date.toLocaleTimeString(navigator.language, {hour: '2-digit', minute: '2-digit'});
            return hhmm;
        }
        tjq("#flight-times").slider({
            range: true,
            min: 0,
            max: 1440,
            step: 5,
            values: [360, 1200],
            slide: function (event, ui) {

                tjq(".start-time-label").html(convertTimeToHHMM(ui.values[0]));
                tjq(".end-time-label").html(convertTimeToHHMM(ui.values[1]));
            }
        });
        tjq(".start-time-label").html(convertTimeToHHMM(tjq("#flight-times").slider("values", 0)));
        tjq(".end-time-label").html(convertTimeToHHMM(tjq("#flight-times").slider("values", 1)));
        tjq("a[flighttype]").on('click', function () {
            switch (tjq(this).attr('flighttype')) {
                case "roundtrip":
                    tjq("div#flights-return-tab").show();
                    tjq("div#flights-Multiple-tab").hide();                    
                    tjq("label[for=returndate]").show();
                    tjq("#departdate").parent().parent().parent().removeClass('col-xs-6').addClass('col-xs-3');
                    tjq("#returndate").parent().parent().parent().show();
                    tjq('#setReturndateLI, #setReturndateLINav').show();
                    tjq('#setDepartdateLI, #setDepartdateLINav').show();
                    break;
                case "oneway":
                    tjq("div#flights-return-tab").show();
                    tjq("div#flights-Multiple-tab").hide();                    
                    tjq("label[for=returndate]").hide();
                    tjq("#returndate").parent().parent().parent().hide();
                    tjq("#departdate").parent().parent().parent().removeClass('col-xs-3').addClass('col-xs-6');
                    tjq('#setReturndateLI, #setReturndateLINav').hide();
                    tjq('#setDepartdateLI, #setDepartdateLINav').show();
                    break;
                case "multiple":
                    tjq("div#flights-Multiple-tab").css("display", "block");
                    tjq("div#flights-return-tab").hide();
                    tjq('#setReturndateLI, #setReturndateLINav').hide();
                    tjq('#setDepartdateLI, #setDepartdateLINav').hide();
                    break;    
                default:
                    return;
            }
        });

        tjq('#setDepartdate, #setDepartdateNav').change(function() {
            var setDepartdate = tjq(this).val();
            tjq('#departdate').val(setDepartdate);

            submitSearchForm();
        });

        tjq('#setReturndate, #setReturndateNav').change(function() {
            var returndate = tjq(this).val();
            tjq('#returndate').val(returndate);

            submitSearchForm();
        });

        tjq('.userdefine').on('change',function() {
            var id = tjq(this).attr('id');
            if (id.indexOf('Nav') != '-1') {
                if (tjq('#' + id.replace('Nav', '')).val() != tjq(this).val()) {
                    tjq('#' + id.replace('Nav', '')).val(tjq(this).val()).change();
                }
            } else {
                tjq('#' + id + 'Nav').val(tjq(this).val()).change();
            }

            submitSearchForm();
        });

        //tjq(window).on('load', initSearchForm);
        initSearchForm();
        var monthNames=['<?php echo lang("一月") ?>','<?php echo lang("二月") ?>','<?php echo lang("三月") ?>','<?php echo lang("四月") ?>','<?php echo lang("五月") ?>','<?php echo lang("六月") ?>','<?php echo lang("七月") ?>','<?php echo lang("八月") ?>','<?php echo lang("九月") ?>','<?php echo lang("十月") ?>','<?php echo lang("十一月") ?>','<?php echo lang("十二月") ?>'];
        //generate low price table
        tjq("#select-month").append('<option value="ALL"><?php echo lang("显示所有月份")?></option>');
        var current_date = new Date();
        var current_year_month = (1900 + current_date.getYear()) + "-" + (current_date.getMonth() + 1);
		if('<?php echo lang("英文") ?>'!="English")
		   {for(var i=0;i<12;i++){
            var tmpdate=getNMonth(i);
            tjq("#select-month").append('<option value="'+tmpdate[0]+'-'+tmpdate[1]+'">'+tmpdate[0]+'年'+tmpdate[1]+'月</option>');
        }} else{for(var i=0;i<12;i++){
            var tmpdate=getNMonth(i);
            tjq("#select-month").append('<option value="'+tmpdate[0]+'-'+tmpdate[1]+'">'+monthNames[tmpdate[1]-1]+". "+tmpdate[0]+'</option>');
        }}
        //build select-month option list
        tjq("#select-month").find("[value='" + current_year_month + "']").prop("selected", "selected");
        /*
        changePriceByMonth();

        tjq("#select-month").change(function() {
            changePriceByMonth();
        });
        tjq("#chardatatype").change(function() {
            changePriceByMonth();
        });

        tjq("#select-time").change(function() {
            changePriceByMonth();
        });
        tjq("#select-arrivalcity").change(function() {
            changePriceByMonth();
        });
        tjq("#select-departurecity").change(function() {
            tjq('ul.salelist').children().removeClass('active');
            tjq('ul.salelist').find('i').hide();
            var sel=tjq('ul.salelist').children('[data='+tjq(this).val()+']');
            tjq(sel).addClass('active');
            tjq(sel).children().show();	
            changePriceByMonth();
        });
        tjq('ul.salelist').children().click(function() {
                tjq('select[id=select-departurecity]').val(tjq(this).attr('data')).change();
                changePriceByMonth();
        });
        */

        tjq('.currency .menu li a').click(function(e) {
            e.preventDefault();

            if (!tjq(this).parent().hasClass('active')) {
                var currency = tjq(this).attr('data');

                tjq.get("<?php echo WEBSITEURL; ?>index.php?id=change_currency&change_currency="+currency+"&");
                tjq('.currency a:first').text(currency);
                tjq('.currency .menu li').removeClass('active');
                tjq(this).parent().addClass('active');
                tjq('#searchform input[name=currency], #m_searchform input[name=currency]').val(currency);
                submitSearchForm();
            }
        });

        tjq('#departdate').datepicker({
            showOn: 'both',
            buttonImage: 'assets/images/icon/blank.png',
            buttonText: '',
            buttonImageOnly: false,
            changeYear: false,
            numberOfMonths: [1, 2],
            minDate: 0,
            dateFormat: "yy-mm-dd",
            dayNamesMin: dayNamesMin,
            monthNames:monthNames,
            monthNamesShort: monthNamesShort,
            dayNames: dayNames,
            dayNamesShort: dayNamesShort,
            beforeShowDay: function (date) {
                updateHighlightDates();

                var returndateObj = tjq('#returndate');
                var dateFormated = stringToDate(date);
                if (dates.length > 0 && tjq.inArray(dateFormated, dates) != '-1') {
                    return [true, 'ui-state-active', ''];
                } else if (highlightDates.length > 0 && tjq.inArray(dateFormated, highlightDates) != '-1') {
                    return [true, 'ui-state-highlight', ''];
                } else {
                    return [true];
                }
            },
            beforeShow: function(input, inst) {
                var themeClass = tjq(input).parent().attr("class").replace("datepicker-wrap", "");
                tjq('#ui-datepicker-div').attr("class", "");
                tjq('#ui-datepicker-div').addClass("ui-datepicker ui-datepicker-multi ui-widget ui-widget-content ui-helper-clearfix ui-corner-all ");
                tjq('#ui-datepicker-div').addClass(themeClass);
            },
            onSelect:function(dateText,inst) {
                var returndateObj = tjq('#returndate');
                var minDate = tjq(this).datepicker('getDate');
                returndateObj.datepicker('option', 'minDate', minDate);
                if (returndateObj.datepicker('getDate') == null) {
                    returndateObj.datepicker('setDate', minDate);
                }
            }
        });

        tjq('#returndate').datepicker({
            showOn: 'both',
            buttonImage: 'assets/images/icon/blank.png',
            buttonText: '',
            buttonImageOnly: false,
            changeYear: false,
            numberOfMonths: [1, 2],
            minDate: 0,
            dateFormat: "yy-mm-dd",
            dayNamesMin: dayNamesMin,
            monthNames:monthNames,
            monthNamesShort: monthNamesShort,
            dayNames: dayNames,
            dayNamesShort: dayNamesShort,
            beforeShowDay: function (date) {
                updateHighlightDates();

                var dateFormated = stringToDate(date);
                if (dates.length > 0 && tjq.inArray(dateFormated, dates) != '-1') {
                    return [true, 'ui-state-active', ''];
                } else if (highlightDates.length > 0 && tjq.inArray(dateFormated, highlightDates) != '-1') {
                    return [true, 'ui-state-highlight', ''];
                } else {
                    return [true];
                }
            },
            beforeShow: function(input, inst) {
                var themeClass = tjq(input).parent().attr("class").replace("datepicker-wrap", "");
                tjq('#ui-datepicker-div').attr("class", "");
                tjq('#ui-datepicker-div').addClass("ui-datepicker ui-datepicker-multi ui-widget ui-widget-content ui-helper-clearfix ui-corner-all ");
                tjq('#ui-datepicker-div').addClass(themeClass);
            },
            onSelect:function(dateText,inst) {
            }
        });

        tjq(window).on('scroll', function () {
            var top = tjq(document).scrollTop();
            var filterTab = tjq('#filterTab').offset().top - tjq(window).scrollTop();
            if (top > 0 && filterTab < 0) {
                tjq("#filterNav").slideDown(300);
            } else if (filterTab > 0) {
                tjq("#filterNav").slideUp(200);
            }
        });

        tjq('#setNonStop, #setNonStopNav').click(function() {
            var id = tjq(this).attr('id');
            if (id.indexOf('Nav') != '-1') {
                tjq('#' + id.replace('Nav', '')).prop( "checked", tjq(this).prop('checked'));
            } else {
                tjq('#' + id + 'Nav').prop( "checked", tjq(this).prop('checked'));
            }

            filterItinerary();
        });
    });

    function filterItinerary() {

        var nonStop = tjq('#setNonStop').prop('checked');
        var rows = tjq('#searchresultlist article');
        if (nonStop) {
            rows.each(function(index, row){
                if (tjq(row).attr('data-directflights') != undefined && tjq(row).attr('data-directflights') != '0') {
                    tjq(row).hide();
                }
            });
        } else {
            tjq('#searchresultlist .ticketItem').show();
        }
    }

    function updateHighlightDates() {
        var departdate,returndate;
        dates = highlightDates = [];

        var departdateOriginal = tjq('#departdate').datepicker('getDate');
        if (departdateOriginal != null) {
            departdate = stringToDate(departdateOriginal);
            dates.push(departdate);
        }

        var returndateObj = tjq('#returndate');
        if (returndateObj.is(':visible')) {
            var returndateOriginal = returndateObj.datepicker('getDate');
            if (returndateOriginal != null) {
                returndate = stringToDate(returndateOriginal);
                if (returndate != departdate) {
                    dates.push(returndate);
                }
            }

            if (departdate != null && returndate != null && returndate != departdate) {
                highlightDates = getDates(departdateOriginal, returndateOriginal);
            }
        }
    }

    function stringToDate(str) {
        var m = str.getMonth(), d = str.getDate(), y = str.getFullYear();

        return y + '-' + ('0'+(m+1)).slice(-2) + '-' + ('0'+d).slice(-2);
    }

    Date.prototype.addDays = function(days) {
        var dat = new Date(this.valueOf())
        dat.setDate(dat.getDate() + days);
        return dat;
    }

    function getDates(startDate, stopDate) {
        var dateArray = new Array();
        var currentDate = startDate.addDays(1);
        while (currentDate < stopDate) {
            dateArray.push(stringToDate(currentDate))
            currentDate = currentDate.addDays(1);
        }
        return dateArray;
    }

    function initSetDateSelect() {

        var departdate = tjq('#departdate').val();
        var returndate = tjq('#returndate').val();
        var departdateString = new Date(departdate);
        var returndateString = new Date(returndate);

        tjq('#setDepartdate, #setDepartdateNav').html('<option selected="" disabled=""><?php echo lang('修改出发日期'); ?></option>');

        var departdateStart = departdateString.getTime() - (3 * 86400000);
        var departdateEnd = departdateString.getTime() + (3 * 86400000);
        for (departdateTime = departdateStart; departdateTime <= departdateEnd; departdateTime += 86400000) {
            var departdateFormated = new Date(departdateTime).toISOString().substring(0, 10);
            var disabled = '',
                current = '',
                style = '';
            if (departdateFormated == departdate) {
                disabled = ' disabled';
                current = ' (<?php echo lang('当前'); ?>)';
                style = ' style="background: rgba(0,0,0,0.3);"';
            }

            tjq('#setDepartdate, #setDepartdateNav').append('<option'+disabled+style+'>'+departdateFormated+current+'</option>');
        }

        tjq('#setReturndate, #setReturndateNav').html('<option selected="" disabled=""><?php echo lang('修改返程日期'); ?></option>');

        var returndateStart = returndateString.getTime() - (3 * 86400000);
        var returndateEnd = returndateString.getTime() + (3 * 86400000);
        for (returndateTime = returndateStart; returndateTime <= returndateEnd; returndateTime += 86400000) {
            var returndateFormated = new Date(returndateTime).toISOString().substring(0, 10);
            var disabled = '',
                current = '',
                style = '';
            if (returndateFormated == returndate) {
                disabled = ' disabled';
                current = ' (<?php echo lang('当前'); ?>)';
                style = ' style="background: rgba(0,0,0,0.3);"';
            }

            tjq('#setReturndate, #setReturndateNav').append('<option'+disabled+style+'>'+returndateFormated+current+'</option>');
        }
    }

    function submitSearchForm() {
        var ftype=tjq('.search-tabs:visible').children('li.active').children('a').attr('flighttype');
        if (ftype=="multiple"){
            tjq('#m_searchform').find('.search-button').click();
        }else{
            tjq('#searchform').find('.search-button').click();
        }
    }

    function initSearchForm(){
        if (post!=undefined){
            if (post.tripType!=undefined && post.tripType=="oneway"){
                tjq('a[flighttype=oneway]').click();
            }
            if (post.tripType!=undefined && post.tripType=="multiple"){
                tjq('a[flighttype=multiple]').click();
            }
            if (post.srccity!=undefined){
                if (post.tripType!=undefined && post.tripType=="multiple"){
                    tjq( window ).load(function() {  // Run code
                        post.srccity.forEach(function(ele,idx){
                            if (idx>=2){
                                tjq("button:contains('<?php echo lang("添加"); ?>')").last().trigger("click");
                            }
                            tjq('div.internary[idx='+(idx+1)+']').each(function(){
                                var srcctiy=tjq(this).find('#srccity');
                                tjq(srcctiy).typeahead('val', getDisplayName(ele.origin));
                                tjq(srcctiy).attr('data',ele.origin);
                                tjq(srcctiy).siblings('input').attr('data', ele.origin);

                                var destcity=tjq(this).find('#destcity');
                                tjq(destcity).typeahead('val', getDisplayName(ele.destination));
                                tjq(destcity).attr('data',ele.destination);
                                tjq(destcity).siblings('input').attr('data', ele.destination);
                                tjq(this).find('input[id^="m_departdate"]').val(ele.departuredate);

                            });

                        });
                    });

                    if (typeof post.PreferLevel!= undefined && post.PreferLevel!=undefined && post.PreferLevel!=""){
                        tjq("div#flights-Multiple-tab").find('#PreferLevel').val(post.PreferLevel);
                    }

                    tjq("div#flights-Multiple-tab").find('#preferredAirline').typeahead('val', getDisplayName(post.preferredAirline));
                    tjq("div#flights-Multiple-tab").find('#preferredAirline').attr('data',post.preferredAirline);
                    tjq("div#flights-Multiple-tab").find('#preferredAirline').siblings('input').attr('data', post.preferredAirline);
                }else{
                    tjq('#srccity').typeahead('val', getDisplayName(post.srccity));
                    tjq('#srccity').attr('data',post.srccity);
                    tjq('#srccity').siblings('input').attr('data', post.srccity);

                    tjq('#destcity').typeahead('val', getDisplayName(post.destcity));                                                
                    tjq('#destcity').attr('data',post.destcity);
                    tjq('#destcity').siblings('input').attr('data', post.destcity);

                    tjq('#departdate').val(post.departdate);
                    tjq('#returndate').val(post.returndate);
                    if (typeof post.PreferLevel!= undefined && post.PreferLevel!=undefined && post.PreferLevel!=""){
                        tjq('#PreferLevel').val(post.PreferLevel);
                    }

                    tjq('#preferredAirline').typeahead('val', getDisplayName(post.preferredAirline));
                    tjq('#preferredAirline').attr('data',post.preferredAirline);
                    tjq('#preferredAirline').siblings('input').attr('data', post.preferredAirline);
                }

                tjq('#searchform #adultcount, #m_searchform #adultcount').val(post.adultcount);
                tjq('#searchform #adultnum, #m_searchform #adultnum').text(post.adultcount);
                tjq('#searchform #childrencount, #m_searchform #childrencount').val(post.childrencount);
                tjq('#searchform #childrennum, #m_searchform #childrennum').text(post.childrencount);
                tjq('#searchform #infantsseatcount, #m_searchform #infantsseatcount').val(post.infantsseatcount);
                tjq('#searchform #infantsseatnum, #m_searchform #infantsseatnum').text(post.infantsseatcount);
                tjq('#searchform #infantslapcount, #m_searchform #infantslapcount').val(post.infantslapcount);
                tjq('#searchform #infantslapnum, #m_searchform #infantslapnum').text(post.infantslapcount);
                var passengersnum = parseInt(post.adultcount)+parseInt(post.childrencount)+parseInt(post.infantsseatcount)+parseInt(post.infantslapcount);
                tjq('.flights-home-pass-total').text(passengersnum);

                if (post.noflash==undefined || post.noflash!="yes"){
                    var ftype=tjq('.search-tabs:visible').children('li.active').children('a').attr('flighttype');
                    if (ftype=="multiple"){
                        tjq('#m_searchform').find('.search-button').click();
                    }else{
                        tjq('#searchform').find('.search-button').click();
                    }
                }
            }
        }
    }

    function FT_p_loadOnewayAdvancedSearchlist(departuredata, returndata) {
        if (departuredata == "NotFound" || departuredata == undefined) {
            return;
        }

        var departureDateData = [];
        var advancedSearchData = {};
        var lowestPrice = '';
        for (var key in departuredata) {
            if (departuredata.hasOwnProperty(key)) {
                var departureValue = departuredata[key];
                var totalfee = parseFloat(departureValue['TotalFare']);
                var totalperson = parseInt(request.adultcount) + parseInt(request.childrencount) + parseInt(request.ifantcount);
                var avgfee = Math.round(totalfee * 100 / totalperson) / 100;
                if (!lowestPrice || lowestPrice > avgfee) {
                    lowestPrice = avgfee;
                }
            }
        }

        for (var key in departuredata) {
            if (departuredata.hasOwnProperty(key)) {
                var departureValue = departuredata[key];
                var departureDateTime = departureValue['DepartureDateTime'];
                var departureDate = departureDateTime.substring(0, 10);
                var totalfee=parseFloat(departureValue['TotalFare']);
                var totalperson = parseInt(request.adultcount) + parseInt(request.childrencount) + parseInt(request.ifantcount);
                var avgfee = Math.round(totalfee * 100 / totalperson) / 100;
                departureDateData.push(departureDate);

                var currencyPrix = request.getCurrencyShow(departureValue['TotalFareCurrency']);

                if (avgfee == lowestPrice) {
                    var dateLowestPrice = true;
                } else {
                    var dateLowestPrice = false;
                }

                advancedSearchData[departureDate] = {'price': currencyPrix + ' ' + avgfee, 'lowestPrice': dateLowestPrice};
            }
        }

        departureDateData = departureDateData.unique().sort();

        var departdateStr = tjq('#departdate').val();
        var departdate = toDate(departdateStr);
        var prev7daysOfDepartdate = departdate.getTime() - (4 * 86400000);
        var today = new Date();
        var nowday = today.getTime();
        if (prev7daysOfDepartdate <= nowday) {
            tjq('#btnPrevWeek').hide();
        } else {
            tjq('#btnPrevWeek').show();
        }

        tjq('.function-oneway-advanceSearch-box tr.weekDay th').removeData('date').find('time').html('');

        tjq('.function-oneway-advanceSearch-box tr.weekDay th').each(function (key) {
            if (departureDateData[key] != undefined) {
                var monthWeek = generateMonthWeek(departureDateData[key]);

                var departdate = tjq('#departdate').val();
                if (departdate == departureDateData[key]) {
                    tjq(this).removeClass('default-ffcr-ffco').addClass('selected-ffcr-ffco');
                } else {
                    tjq(this).removeClass('selected-ffcr-ffco').addClass('default-ffcr-ffco');
                }

                tjq(this).data('date', departureDateData[key]);
                tjq(this).find('time').html(monthWeek);
            } else {
                tjq(this).find('time').html('-<br>-');
            }
        });

        tjq('.function-oneway-advanceSearch-box table tbody .lowest-price-triangle').remove();

        tjq('.function-oneway-advanceSearch-box tr.ffcr-first td').each(function () {
            var dataIndex = tjq(this).attr('data');
            var departureDate = tjq('.function-oneway-advanceSearch-box tr.weekDay th[data='+dataIndex+']').data('date');

            if (departureDate != undefined) {
                var requestDepartureDate = tjq('#departdate').val();
                if (requestDepartureDate == departureDate) {
                    tjq(this).find('div').addClass('active');
                } else {
                    tjq(this).find('div').removeClass('active');
                }

                tjq(this).data('date', departureDate);

                if (advancedSearchData[departureDate] == undefined) {
                    tjq(this).find('.number').text('-');
                } else {
                    tjq(this).find('.number').text(advancedSearchData[departureDate]['price']);
                    if (advancedSearchData[departureDate]['lowestPrice']) {
                        tjq(this).find('.number').after('<span class="sprite lowest-price-triangle"></span>');
                    }
                }
            } else {
                tjq(this).find('.number').text('-');
            }
        });

        tjq('.function-searchResult-loading-box').hide();
        tjq('.function-oneway-advanceSearch-box').show();
    }

    function FT_p_loadRoundtripAdvancedSearchlist(departuredata, returndata) {
        if (departuredata == "NotFound" || departuredata == undefined) {
            return;
        }

        var departureDateData = [];
        var returnDateData = [];
        var advancedSearchData = {};
        var lowestPrice = '';
        for (var key in departuredata) {
            if (departuredata.hasOwnProperty(key)) {
                var departureValue = departuredata[key];
                var totalfee = parseFloat(departureValue['TotalFare']);
                var totalperson = parseInt(request.adultcount) + parseInt(request.childrencount) + parseInt(request.ifantcount);
                var avgfee = Math.round(totalfee * 100 / totalperson) / 100;
                if (!lowestPrice || lowestPrice > avgfee) {
                    lowestPrice = avgfee;
                }
            }
        }

        for (var key in departuredata) {
            if (departuredata.hasOwnProperty(key)) {
                var departureValue = departuredata[key];
                var departureDateTime = departureValue['DepartureDateTime'];
                var departureDate = departureDateTime.substring(0, 10);
                var totalfee=parseFloat(departureValue['TotalFare']);
                var totalperson = parseInt(request.adultcount) + parseInt(request.childrencount) + parseInt(request.ifantcount);
                var avgfee = Math.round(totalfee * 100 / totalperson) / 100;
                departureDateData.push(departureDate);

                var currencyPrix = request.getCurrencyShow(departureValue['TotalFareCurrency']);

                var returnList = departureValue["RETURN_LIST"];
                for (var key_r in returnList) {
                    if (!isNaN(key_r)) {
                        var value_r = returnList[key_r];
                        var returnValue = returndata[value_r];
                        if (returnValue.hasOwnProperty('DepartureDateTime')) {
                            var returnDateTime = returnValue['DepartureDateTime'];
                            var returnDate = returnDateTime.substring(0, 10);
                            returnDateData.push(returnDate);

                            if (avgfee == lowestPrice) {
                                var dateLowestPrice = true;
                            } else {
                                var dateLowestPrice = false;
                            }

                            advancedSearchData[departureDate + '+' + returnDate] = {'price': currencyPrix + ' ' + avgfee, 'lowestPrice': dateLowestPrice};
                        }
                    }
                }
            }
        }

        departureDateData = departureDateData.unique().sort();
        returnDateData = returnDateData.unique().sort();

        tjq('th.weekDay').removeData('date').find('time').html('');
        tjq('tr.weekDay th').removeData('date').find('time').html('');

        tjq('th.weekDay').each(function (key) {
            if (departureDateData[key] != undefined) {
                var monthWeek = generateMonthWeek(departureDateData[key]);

                var departureDate = tjq('#departdate').val();
                if (departureDate == departureDateData[key]) {
                    tjq(this).removeClass('default-ffcr-ffco').addClass('selected-ffcr-ffco');
                } else {
                    tjq(this).removeClass('selected-ffcr-ffco').addClass('default-ffcr-ffco');
                }

                tjq(this).data('date', departureDateData[key]);
                tjq(this).find('time').html(monthWeek);
                tjq(this).parent().show();
            } else {
                tjq(this).parent().hide();
            }
        });

        tjq('tr.weekDay th').each(function (key) {
            if (returnDateData[key] != undefined) {
                var monthWeek = generateMonthWeek(returnDateData[key]);

                var returnDate = tjq('#returndate').val();
                if (returnDate == returnDateData[key]) {
                    tjq(this).removeClass('default-ffcr-ffco').addClass('selected-ffcr-ffco');
                } else {
                    tjq(this).removeClass('selected-ffcr-ffco').addClass('default-ffcr-ffco');
                }

                tjq(this).data('date', returnDateData[key]);
                tjq(this).find('time').html(monthWeek);
            }
        });

        tjq('table tbody .lowest-price-triangle').remove();

        tjq('.ffcr-first td').each(function () {
            var dataIndex = tjq(this).attr('data');
            var returnDate = tjq('tr.weekDay th[data='+dataIndex+']').data('date');
            var departureDate = tjq(this).parent().find('th.weekDay').data('date');

            if (departureDate != undefined && returnDate != undefined) {
                var requestDepartureDate = tjq('#departdate').val();
                var requestReturnDate = tjq('#returndate').val();
                if (requestDepartureDate == departureDate && requestReturnDate == returnDate) {
                    tjq(this).find('div').addClass('active');
                } else {
                    tjq(this).find('div').removeClass('active');
                }

                tjq(this).data('date', departureDate + '+' + returnDate);

                if (advancedSearchData[departureDate + '+' + returnDate] == undefined) {
                    tjq(this).find('.number').text('-');
                } else {
                    tjq(this).find('.number').text(advancedSearchData[departureDate + '+' + returnDate]['price']);
                    if (advancedSearchData[departureDate + '+' + returnDate]['lowestPrice']) {
                        tjq(this).find('.number').after('<span class="sprite lowest-price-triangle"></span>');
                    }
                }
            } else {
                tjq(this).find('.number').text('-');
            }
        });

        tjq('.function-searchResult-loading-box').hide();
        tjq('.function-roundtrip-advanceSearch-box').show();
    }

    function toDate(dateStr) {
        var parts = dateStr.split("-");
        return new Date(parts[1]+'/'+parts[2]+'/'+parts[0]);
    }

    function generateMonthWeek(dateStr) {
        var date = toDate(dateStr);
        var day = date.getDate();
        var monthIndex = date.getMonth();
        var weekIndex = date.getDay();

        if (lang == 'English') {
            var month = monthNames[monthIndex];
            var week = weekNames_EN[weekIndex];
            var monthWeek = week + '<br>' + day + ' ' + month;
        } else if (lang == 'Chinese') {
            var month = monthIndex + 1;
            var week = weekNames_CN[weekIndex];
            var monthWeek = week + '<br>' + month + '月' + day + '日';
        }

        return monthWeek;
    }

    function sortItinerary() {

        var priority = tjq('#priority').val();
        var rows = tjq('#searchresultlist article:visible');
        var priorityMap = {
            'Price': ['DirectFlights', 'Time'],
            'DirectFlights': ['Price', 'Time'],
            'Time': ['Price', 'DirectFlights']
        };

        rows.sort(function(a, b) {
            var data = {
                'Price': {},
                'DirectFlights': {},
                'Time': {}
            };

            data['Price']['a'] = parseFloat(tjq(a).attr('data-price'));
            data['Price']['b'] = parseFloat(tjq(b).attr('data-price'));
            data['DirectFlights']['a'] = parseInt(tjq(a).attr('data-directflights'));
            data['DirectFlights']['b'] = parseInt(tjq(b).attr('data-directflights'));
            data['Time']['a'] = parseInt(tjq(a).attr('data-time'));
            data['Time']['b'] = parseInt(tjq(b).attr('data-time'));

            if (data[priority]['a'] > data[priority]['b']) {
                return 1;
            } else if (data[priority]['a'] < data[priority]['b']) {
                return -1;
            }

            if (data[priorityMap[priority][0]]['a'] > data[priorityMap[priority][0]]['b']) {
                return 1;
            } else if (data[priorityMap[priority][0]]['a'] < data[priorityMap[priority][0]]['b']) {
                return -1;
            }

            if (data[priorityMap[priority][1]]['a'] > data[priorityMap[priority][1]]['b']) {
                return 1;
            } else if (data[priorityMap[priority][1]]['a'] < data[priorityMap[priority][1]]['b']) {
                return -1;
            }

            return 0;
        });

        rows.each(function(index, row){
            rows.parent().append(row);
        });
    }

    function FT_p_loadlist(departuredata, returndata) {
        if (departuredata == "NotFound" || departuredata == undefined) {
            FT_p_error_report("<?php echo lang("没有找到航班信息") ?>");
            return;
        }

        var listbox = tjq('#ft_item_template').parent();
        var currprocess = 50;

        var step = Math.floor(50 / Object.keys(departuredata).length);
        request.showProcessBar(currprocess);

        for (var key in departuredata) {
            if (departuredata.hasOwnProperty(key)) {
                var value = departuredata[key];
                if ((itineraries[value['MD5']] == undefined) || (itineraries[value['MD5']] != undefined && parseFloat(itineraries[value['MD5']]) > parseFloat(value['TotalFare']))) {
                    if (itineraries[value['MD5']] != undefined) {
                        tjq("article[data-md5='" + value['MD5'] + "']").remove();
                    }

                    itineraries[value['MD5']] = value['TotalFare'];

                    var template = tjq('#ft_item_template').clone();

                    tjq(template).find('#ft_item_template_return').remove();
                    tjq(template).addClass('ticketItem');
                    tjq(template).css("display", "table");
                    //console.log(value);
                    for (var item in value) {
                        tjq(template).find('#' + item).html(value[item]);
                    }

                    tjq(template).find('#seatsLeft').parent().hide();

                    var totalfee = parseFloat(value["TotalFare"]);
                    var totalperson = parseInt(request.adultcount) + parseInt(request.childrencount) + parseInt(request.ifantcount);
                    var avgfee = Math.round(totalfee * 100 / totalperson) / 100;
                    tjq(template).find('#TotalFare').html(avgfee);
                    tjq(template).attr('data-price', avgfee);
                    tjq(template).find('#TotalFareTotal').html(request.getCurrencyShow(value['TotalFareCurrency']) + value["TotalFare"]);

                    tjq(template).attr('data-MD5', value['MD5']);
                    tjq(template).attr('data-directflights', value['stops']);
                    var DepartureDateTimeDate = new Date(value['DepartureDateTime']);
                    tjq(template).attr('data-time', DepartureDateTimeDate.getTime() / 1000);

                    tjq(template).find('#provider').attr('src', 'assets/images/' + value["provider"] + '.jpg');

                    if (value['stops'] == "0") {
                        tjq(template).find('#stops').html("<?php echo lang("直飞") ?>");
                    } else {
                        tjq(template).find('#stops').html(value['stops'] + "<?php echo lang("中转")?>");
                    }

                    var airCompanylist = value['airCompany'];
                    if (typeof airCompanylist == "string") {
                        airCompanylist = [airCompanylist];
                    }
                    tjq(template).find('#FlightName').html('');
                    for (var m = 0; m < airCompanylist.length; m++) {
                        var ele = airCompanylist[m];
                        var br = "<br>";
                        if (m == 0) {
                            br = "";
                        }
                        tjq(template).find('#FlightName').append(br + '<a href="#flighttypeDetail" onload="loadWikipedia" wkey="' + getAirlinesNameByCode(ele) + '" class="soap-popupbox">' + getAirlinesNameByCode(ele) + '</a>');
                    }
                    tjq(template).find('#TotalFareCurrency').html(request.getCurrencyShow(value['TotalFareCurrency']));
                    //airline logo
                    var go_airstring = value["airline_logo"];
                    if (go_airstring.indexOf(",") > 0) {
                        go_airstring = "unitflight";
                    }
                    tjq(template).find('.airimg').replaceWith('<img alt="" src="assets/images/logo/' + go_airstring + '.png" />');

                    //var stopcitylist=JSON.parse(value['stopcitylist']);
                    var stopcitystring = '';
                    var goSeg = JSON.parse(value['Segment']);
                    var i = 0;
                    tjq.each(goSeg, function (idx, val) {

                        stopcitystring += getCityNameByCode(val["From"]);
                        if (i < (goSeg.length)) {
                            stopcitystring += '<span id="stopaircode"><em id="up">' + val["Airline"] + val["Flightnumber"] + '</em><em id="down"><a href="#flighttypeDetail" onload="loadWikipedia" wkey="' + getairplanemodelsByCode(val["AirEquipType"]) + '" class="soap-popupbox">' + getairplanemodelsByCode(val["AirEquipType"]) + '</a></em></span>';
                        }
                        if (i == (goSeg.length - 1)) {
                            stopcitystring += getCityNameByCode(val["To"]);
                        }
                        i++;
                    });

                    tjq(template).find('#stopcitylist').html(stopcitystring);
                    if (request.tripType == "multiple") {
                        tjq(template).find('.gobutton').attr('data', JSON.stringify(value));
                    } else {
                        tjq(template).find('.gobutton').attr('data', request.concat(JSON.stringify(value), ""));
                        if (post.source != undefined && post.source == 'changeItinerary' && request.tripType == 'oneway') {
                            tjq(template).find('.gobutton').attr('data-clipboard-text', value['Segment']);
                        }
                    }

                    if (request.tripType == "roundtrip" || request.tripType == "multiple") {

                        //build return information
                        var returncount = 0;
                        tjq.each(value["RETURN_LIST"], function (key_r, value_r) {
                            var return_template = tjq('#ft_item_template_return').clone();
                            tjq(return_template).removeAttr('id');
                            if (key_r != 0) {
                                tjq(return_template).find('.alert-notice').remove();
                            }
                            //console.log(returndata[value_r]);
                            for (var item in returndata[value_r]) {
                                tjq(return_template).find('#' + item).html(returndata[value_r][item]);
                            }

                            tjq(return_template).find('#seatsLeft').parent().hide();
                            if (returndata[value_r]['stops'] == "0") {
                                tjq(return_template).find('#stops').html("<?php echo lang("直飞") ?>");
                            } else {
                                tjq(return_template).find('#stops').html(returndata[value_r]['stops'] + "<?php echo lang("中转")?>");
                            }
                            var airCompanylist = returndata[value_r]['airCompany'];
                            if (typeof airCompanylist == "string") {
                                airCompanylist = [airCompanylist];
                            }

                            tjq(return_template).find('#FlightName').html('');
                            for (var m = 0; m < airCompanylist.length; m++) {
                                var ele = airCompanylist[m];
                                var br = "<br>";
                                if (m == 0) {
                                    br = "";
                                }
                                tjq(return_template).find('#FlightName').append(br + '<a href="#flighttypeDetail" onload="loadWikipedia" wkey="' + getAirlinesNameByCode(ele) + '" class="soap-popupbox">' + getAirlinesNameByCode(ele) + "</a>");
                            }
                            var returnSeg = JSON.parse(returndata[value_r]['Segment']);
                            tjq(return_template).hide();


                            var return_airstring = returndata[value_r]["airline_logo"];
                            if (return_airstring.indexOf(",") > 0) {
                                return_airstring = "unitflight";
                            }
                            tjq(return_template).find('.airimg').replaceWith('<img alt="" src="assets/images/logo/' + return_airstring + '.png" />');
                            var airstring = go_airstring;
                            if (return_airstring != go_airstring) {
                                airstring = "multi";
                            }
                            request.airline_logo = airstring;
                            if (request.tripType == "multiple") {
                                tjq(return_template).find('.orderbutton').attr('data', JSON.stringify(returndata[value_r]));
                            } else {
                                tjq(return_template).find('.orderbutton').attr('data', request.concat(JSON.stringify(value), JSON.stringify(returndata[value_r])));
                                if (post.source != undefined && post.source == 'changeItinerary') {
                                    tjq(return_template).find('.orderbutton').attr('data-clipboard-text', request.concat(value['Segment'], returndata[value_r]['Segment']));
                                }
                            }

                            tjq(return_template).find('.orderbutton').attr('data-from', 'sabre');
                            returncount++;
                            tjq(template).find('.details').append(return_template);

                            //stopcitylist=JSON.parse(returndata[value_r]['stopcitylist']);
                            stopcitystring = '';
                            var i = 0;
                            tjq.each(returnSeg, function (idx, val) {
                                stopcitystring += getCityNameByCode(val["From"]);
                                if (i < (returnSeg.length)) {
                                    stopcitystring += '<span id="stopaircode"><em id="up">' + val["Airline"] + val["Flightnumber"] + '</em><em id="down"><a href="#flighttypeDetail" onload="loadWikipedia" wkey="' + getairplanemodelsByCode(val["AirEquipType"]) + '" class="soap-popupbox">' + getairplanemodelsByCode(val["AirEquipType"]) + '</a></em></span>';
                                }
                                if (i == (returnSeg.length - 1)) {
                                    stopcitystring += getCityNameByCode(val["To"]);
                                }
                                i++;
                            });

                            tjq(return_template).find('#stopcitylist').html(stopcitystring);
                            //airline logo


                        });


                        tjq(template).find('.gobutton').html('<?php echo lang("选择回程")?>（' + returncount + '）');
                        tjq(template).find('.panel-collapse').hover(function () {

                            tjq(this).find('div hr').css('border-color', '#c30d23');
                            tjq(this).next().find('div hr').css('border-color', '#c30d23');

                            //tjq( this ).fadeOut( 100 );

                        }, function () {
                            tjq(this).find('div hr').css('border-color', '#eee');
                            tjq(this).next().find('div hr').css('border-color', '#eee');
                            // tjq( this ).fadeIn( 500 );
                        });
                    }

                    if (request.tripType=="multiple" ){
                        tjq(template).find('.orderbutton').click(function (event) {
                            event.preventDefault();
                            tjq(this).hide();
                            tjq(this).siblings('div#loadingseat').show();
                            var opobj=this;
                            var para2=Array();
                            tjq(this).parents('article.box').find("a[data]").each(function(){
                                var tmpraw=JSON.parse(tjq(this).attr('data'));
                                tmpraw["personcount"]=request.getPersoncount();
                                para2.push(tmpraw);
                            });

                            FT_c_checkSeats(para2,function(avai){
                                if (avai==1){
                                    request.mulit_dconccat(para2);
                                    tjq('input[name=request]').val(request.toJSONString());
                                    tjq('input[name=personcount]').val(request.getPersoncount());
                                    var actionurl=tjq('form[name=orderform]').attr('action')+"<?php echo $GLOBALS["fullsite"] ?>";
                                    tjq('form[name=orderform]').attr('action',actionurl);
                                    tjq('form[name=orderform]').submit();
                                }else{
                                    tjq(opobj).siblings('div#loadingseat').hide();
                                    tjq(opobj).siblings('div#nomoreseat').show();
                                }
                            });

                        });
                    }

                    if (request.tripType=="roundtrip" ){
                        tjq(template).find('.gobutton').click(function (event) {
                            event.preventDefault();
                            //console.log(tjq(this).parents('.details').children('.panel-collapse'));
                            var currentopen=tjq(this).parents('.details').children('.panel-collapse').is(":visible");
                            tjq('.panel-collapse').hide();
                            tjq('h4[id^=mark-]').hide();
                            if (!currentopen){
                                tjq(this).parents('article[id=ft_item_template]').find('h4[id^=mark-]').show();
                                tjq(this).parents('.details').children('.panel-collapse').fadeIn();
                            }
                            tjq('html, body').animate({
                                scrollTop: tjq(this).offset().top - 100
                            }, 1000);
                        });

                        tjq(template).find('.orderbutton').click(function (event) {
                            event.preventDefault();
                            tjq(this).hide();
                            tjq(this).siblings('div#loadingseat').show();
                            var opobj=this;
                            var para2=Array();
                            request.dconcat(tjq(this).attr('data'));
                            var tmpraw=JSON.parse(request.goRaw);
                            tmpraw["personcount"]=request.getPersoncount();
                            para2.push(tmpraw);
                            var tmpraw=JSON.parse(request.returnRaw);
                            tmpraw["personcount"]=request.getPersoncount();
                            para2.push(tmpraw);
                            //para2["go"]=JSON.parse(request.goKey);
                            //para2["return"]=JSON.parse(request.returnKey);

                            FT_c_checkSeats(para2,function(avai){
                                if (avai==1){
                                    tjq('input[name=request]').val(request.toJSONString());
                                    tjq('input[name=personcount]').val(request.getPersoncount());
                                    var actionurl=tjq('form[name=orderform]').attr('action')+"<?php echo $GLOBALS["fullsite"] ?>";
                                    tjq('form[name=orderform]').attr('action',actionurl);
                                    tjq('form[name=orderform]').submit();
                                }else{
                                    tjq(opobj).siblings('div#loadingseat').hide();
                                    tjq(opobj).siblings('div#nomoreseat').show();
                                }
                            });
                        });
                    }

                    if (request.tripType=="oneway"){
                        tjq(template).find('.gobutton').click(function (event) {
                            event.preventDefault();
                            tjq(this).hide();
                            tjq(this).siblings('div#loadingseat').show();
                            var opobj=this;
                            var para2=Array();
                            request.dconcat(tjq(this).attr('data'));
                            var tmpraw=JSON.parse(request.goRaw);
                            tmpraw["personcount"]=request.getPersoncount();
                            para2.push(tmpraw);
                            FT_c_checkSeats(para2,function(avai){
                                if (avai==1){
                                    tjq('input[name=request]').val(request.toJSONString());
                                    tjq('input[name=personcount]').val(request.getPersoncount());
                                    tjq('form[name=orderform]').submit();
                                }else{
                                    tjq(opobj).siblings('div#loadingseat').hide();
                                    tjq(opobj).siblings('div#nomoreseat').show();
                                }
                            });

                        });
                    }

                    tjq(listbox).append(template);
                }
            }
            currprocess += step;
            setTimeout(function () {
                request.showProcessBar(currprocess);
            }, 200);
        }

        sortItinerary();

        if (request.tripType=="multiple" ){
            tjq(".gobutton").hide();
            tjq('.orderbutton').hide();
            tjq('article.ticketItem').each(function () {
                tjq(this).find(".orderbutton").last().show();
            });
            tjq('.panel-collapse').fadeIn();
            if (post.source != undefined && post.source == 'changeItinerary') {
                tjq('article.box.ticketItem').each(function(){
                    var segments=Array();
                    tjq(this).find("a[data]").each(function(){
                        var tmpraw=JSON.parse(tjq(this).attr('data'));
                        segments.push(tmpraw['Segment']);
                    });
                    tjq(this).find('.orderbutton').attr('data-clipboard-text', segments.join('}@@{'));
                });
                tjq(".orderbutton").unbind().html('<?php echo lang("复制行程信息")?>');
            } else {
                tjq(".orderbutton").html('<?php echo lang(" 预定")?>');
            }
        }
        
        if (request.tripType=="roundtrip" ){
            tjq(".gobutton").show();
            tjq('.orderbutton').show();
            if (post.source != undefined && post.source == 'changeItinerary') {
                tjq(".orderbutton").unbind().html('<?php echo lang("复制行程信息")?>');
            } else {
                tjq(".orderbutton").html('<?php echo lang(" 预定")?>');
            }
        }

        if (request.tripType=="oneway"){
            tjq(".gobutton").show();
            tjq('.orderbutton').show();
            if (post.source != undefined && post.source == 'changeItinerary') {
                tjq(".gobutton").unbind().html('<?php echo lang("复制行程信息")?>');
            } else {
                tjq(".gobutton").html('<?php echo lang(" 预定")?>');
            }
        }

        tjq('.function-button-box').show();
        tjq('.button-loading').show();
        setTimeout(function () {
            request.showProcessBar(100);
        }, 1000);
        tjq('.myBtn').click(function (event) {
            tjq('#myModal').show();
        });
    }

    function FT_p_error_report(errorMsg, errorLevel, showtype) {
        tjq('.information-box').show();
        tjq('.information-box div').html(errorMsg);
        request.showProcessBar(100);
    }
    tjq("button:contains('<?php echo lang("添加"); ?>')").click(function(event){
        event.preventDefault();
        var currobj=tjq('.internary').last();
        var curridx=tjq(currobj).attr('idx');        
        var copyobj=tjq(currobj).clone(true,true);
        var copyidx=parseInt(curridx)+1;
        tjq(copyobj).attr('idx',copyidx);
        tjq(copyobj).find('input').val('');        
        tjq(copyobj).find('span.twitter-typeahead').remove();
        tjq(copyobj).find('.typeahead-close').each(function( index ) {
           var tmpid="srccity";
           if(index==1){
               tmpid="destcity";
           }
           tjq(this).after('<input type="text" required id="'+tmpid+'" class="typeahead input-text full-width airport" style="left:-1px!important" data="" placeholder="<?php echo lang("城市，机场名字");?>" />');
        });
        
        tjq(copyobj).find('.datepicker-wrap').empty();
        tjq(copyobj).find('.datepicker-wrap').html('<input id="m_departdate_'+copyidx+'" required type="text" class="input-text full-width" picker-type="departure"  date-type="date" placeholder="yyyy-mm-dd" />');
        initDepartureDate(tjq(copyobj).find("input[id^='m_departdate_']"));
        
        initAirportInput(copyobj);
        tjq(currobj).after(tjq(copyobj));        
       
        tjq("button:contains('<?php echo lang("删除"); ?>')").unbind('click').click(function(){
            if (tjq("button:contains('<?php echo lang("删除"); ?>')").length>1){
                var delcurrobj=tjq(this).closest('.row');
                var prevobj=tjq(delcurrobj).prev();
                tjq(delcurrobj).remove();
                tjq("button:contains('<?php echo lang("添加"); ?>')").last().show();
                
            }
            if (tjq("button:contains('<?php echo lang("删除"); ?>')").length==1){
                tjq("button:contains('<?php echo lang("删除"); ?>')").first().hide();
            }
        });
        tjq("button:contains('<?php echo lang("添加"); ?>')").hide();
        tjq("button:contains('<?php echo lang("添加"); ?>')").last().show();
        tjq("button:contains('<?php echo lang("删除"); ?>')").show();  
    });

    tjq('.search-button').click(function (event) {
        event.preventDefault();
        tjq('.information-box').hide();
        tjq('.function-roundtrip-advanceSearch-box, .function-oneway-advanceSearch-box').hide();

        
        if (false == request.checkForm(tjq(this).closest('form').attr('id'))) {
            return;
        }

        options=request.getSearchOption();

        if (request.tripType !="multiple") {
            tjq('.function-searchResult-loading-box').show();
        }

        if (request.tripType==="multiple"){
            var tmpstr='<h4 class="box-title">【<?php echo lang('多程')?>】</h4>';
            tjq('div.function-button-box').first().html('');
            var i=0;
            tjq('div.internary').each(function(){
                if (i>0){
                    tmpstr +='<br>'
                }else{
                    tmpstr +='<h4 class="box-title pull-left">'
                }
                tmpstr +=getAirportNameByCode(tjq(this).find('input#srccity').val())+'<img src="assets/images/exchange-oneway.jpg">'+getAirportNameByCode(tjq(this).find('input#destcity').val());
                i++;
            });
            tmpstr +="</h4>"
            tjq('div.function-button-box').first().html(tmpstr);
        }
        if (request.tripType==="roundtrip"){
            tjq('div.function-button-box').first().html('');
            var tmpstr='<h4 class="box-title">【<?php echo lang('往返')?>】'+getAirportNameByCode(request.srccity)+'<img src="assets/images/exchange-full.jpg">'+getAirportNameByCode(request.destcity)+'</h4>';
            tjq('div.function-button-box').first().html(tmpstr);
        }
        if (request.tripType==="oneway"){
            tjq('div.function-button-box').first().html('');
            var tmpstr='<h4 class="box-title">【<?php echo lang('单程')?>】'+getAirportNameByCode(request.srccity)+'<img src="assets/images/exchange-oneway.jpg">'+getAirportNameByCode(request.destcity)+'</h4>';
            tjq('div.function-button-box').first().html(tmpstr);
        }

        tjq('.ticketItem').remove();
        tjq('.function-button-box').hide();
        tjq('.button-loading').hide();
        request.showProcessBar(0);
        request.showProcessBar(10);

        initSetDateSelect();

        if (request.tripType==="roundtrip") {
            FT_c_doAdvancedSearch(options, FT_p_loadRoundtripAdvancedSearchlist, FT_p_error_report);
        } else if (request.tripType==="oneway") {
            FT_c_doAdvancedSearch(options, FT_p_loadOnewayAdvancedSearchlist, FT_p_error_report);
        }

        itineraries = {};
        console.log(options);
        FT_c_doSearch(options, FT_p_loadlist, FT_p_error_report);

    });

    function loadflightdetail(obj)
    {
        var data=request.getDconcat(tjq(obj).parents('.second-row').find('div.action').children('a').attr('data'));


        var markid=tjq(obj).parents('div.details-wrapper').children('h4[id^=mark-]').attr('id');
        var contenttype="all";
        var internary=data[0];

        switch(markid){
            case 'mark-depature':
                contenttype="go";
                break;
            case 'mark-arrival':
                contenttype="return";
                internary=data[1];
                if(internary==undefined){
                    internary=data[0];
                }
                break;    

        }
        loadInternaryDetail(contenttype,internary);

    }

    function loadWikipedia(obj){
        var wkey=tjq(obj).attr('wkey');                                        
        tjq('#flighttypeDetail').find('iframe').attr('src', 'http://dev.crud.sinorama.ca/getWikiContent.php?keyword='+wkey+'&');                                                                                
        tjq('#soap-popupbox').click(function(e) {  
               tjq('#flighttypeDetail').find('iframe').attr('src','');
        });

    }

    function changePriceByMonth(){
        tjq('#price_chart_list').html('<div class="col-sm-12 col-md-12"><p class="text-center" ><img src="assets/images/loading.gif" width="50%" height="50%" /></p></div>');
        //tjq('#price_chart_list').append('<div class="col-sm-6 col-md-6"><p class="text-center" ><img src="assets/images/loading.gif" width="50%" height="50%" /></p></div>');
        tjq('#price_table_list').html('<div class="calendar col-sm-12 col-md-12"><p class="text-center" ><img src="assets/images/loading.gif" width="50%" height="50%" /></p></div>');
        var currmonth=tjq('#select-month').val();
        var fromcity=tjq('#select-departurecity').val();
        var tocity=tjq('#select-arrivalcity').val();
        var duration=tjq('#select-time').val();

        tjq.getJSON( "getLowPriceList.php?currmonth="+currmonth+"&fromcity="+fromcity+"&tocity="+tocity+"&duration="+duration+"&", function(lowpricelist) {
                tjq('#price_chart_list').html('');

                drawChart2(lowpricelist);  
                if (currmonth!="ALL"){                                                
                    tjq('#price_table_list').html('<div class="calendar col-sm-12 col-md-12">'+getOnmonthTable(currmonth,lowpricelist)+'</div>');
                }else{
                    tjq('#price_table_list').html('');
                    tjq("#select-month option").each(function()
                    {
                        var tmpmonth=tjq(this).val();
                        if (tmpmonth!="ALL"){
                            tjq('#price_table_list').append('<div class="calendar col-sm-6 col-md-6">'+getOnmonthTable(tmpmonth,lowpricelist)+'</div>');
                        }
                    });
                }
                
                tjq('td.available a').hover(function(){
                    var currdate=tjq(this).attr('startdate');                    
                    tjq(this).attr('start-date',currdate.substring(4,6)+'<?php echo lang(" 月")?>'+currdate.substring(6,8)+'<?php echo lang(" 日")?>');
                    var step=tjq('#select-time').val();					
                    var enddate=getNDay(step,new Date(currdate.substring(0,4)+"-"+currdate.substring(4,6)+"-"+currdate.substring(6,8)));					
                    tjq(this).attr('end-date',enddate[1]+'<?php echo lang(" 月")?>'+enddate[3]+'<?php echo lang(" 日")?>');
                });
                tjq('td.available a').click(function(event ){
                    event.preventDefault();
                    var currdate=tjq(this).attr('startdate');
                    var step=tjq('#select-time').val();		
                    tjq('a[flighttype=roundtrip]').click();
                    var enddate=getNDay(step,new Date(currdate.substring(0,4)+"-"+currdate.substring(4,6)+"-"+currdate.substring(6,8)));
                    tjq('#srccity').typeahead('val', getDisplayName(tjq('#select-departurecity').val()));
                    tjq('#srccity').attr('data',tjq('#select-departurecity').val());
                    tjq('#srccity').siblings('input').attr('data', tjq('#select-departurecity').val());

                    tjq('#destcity').typeahead('val', getDisplayName(tjq('#select-arrivalcity').val()));                                                
                    tjq('#destcity').attr('data',tjq('#select-arrivalcity').val());
                    tjq('#destcity').siblings('input').attr('data', tjq('#select-arrivalcity').val());

                    tjq('#departdate').val(currdate.substring(0,4)+"-"+currdate.substring(4,6)+"-"+currdate.substring(6,8));
                    var enddate=getNDay(step,new Date(currdate.substring(0,4)+"-"+currdate.substring(4,6)+"-"+currdate.substring(6,8)));
                    tjq('#returndate').val(enddate[0]+"-"+enddate[1]+"-"+enddate[3]);
                    tjq('html, body').animate({scrollTop : 0},800);
                    var ftype=tjq('.search-tabs:visible').children('li.active').children('a').attr('flighttype');
                    if (ftype=="multiple"){
                        tjq('#m_searchform').find('.search-button').click();
                    }else{
                        tjq('#searchform').find('.search-button').click();
                    }

                });

        });
    }
    function getOnmonthTable(month,lowpricelist){
        var currmonth=month;
        var unavailable_days = [];        
        var price_arr = {};
        tjq(lowpricelist).each(function(){
            if (this.fromdate.substring(0,7)==currmonth){
                if (parseInt(this.totalfare)>0){
                        price_arr[parseInt(this.fromdate.substring(8,10))]="$"+parseInt(this.totalfare)/100;
                }
            }
        });
        var current_date=new Date(currmonth+"-15");   
        cal.generateHTML(current_date.getMonth(), (1900 + current_date.getYear()), unavailable_days, price_arr);                                        
        return cal.getHTML();
    }


</script>

<script>
function drawChart1(){
    tjq('#price_chart_list').append('<div class="col-sm-6 col-md-6"><canvas id="myChart1"></canvas></div>');
    var ctx = document.getElementById("myChart1");
    var myChart = new Chart(ctx, {
        type: 'bar',
        data: {
            labels: ["Jan", "Feb", "Mar", "April", "May", "June"],
            datasets: [{
                label: '# of Votes',
                data: [12, 19, 3, 5, 2, 3],
                backgroundColor: [
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(255, 206, 86, 0.2)',
                    'rgba(75, 192, 192, 0.2)',
                    'rgba(153, 102, 255, 0.2)',
                    'rgba(255, 159, 64, 0.2)'
                ],
                borderColor: [
                    'rgba(255,99,132,1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)',
                    'rgba(75, 192, 192, 1)',
                    'rgba(153, 102, 255, 1)',
                    'rgba(255, 159, 64, 1)'
                ],
                borderWidth: 1
            }]
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero:true
                    }
                }]
            }
        }
    });
}
</script>
<script>
function drawChart2(lowpricelist){
    tjq('#price_chart_list').append('<div class="col-sm-12 col-md-12"><canvas id="myChart2" style="height:250px!important"></canvas></div>');
    var ctx = document.getElementById("myChart2");
    var labels = new Array;
    var data=new Array;
    var datatype=tjq('#chardatatype').val();
    tjq("#select-month option").each  ( function() {
        if (tjq(this).val()!="ALL"){
            var currentmonth=tjq(this).val();
            var tmpval=0;
            var tmpcount=0;
            tjq(lowpricelist).each(function(){
                if (new Date(this.fromdate)> new Date() && this.fromdate.substring(0,7)==currentmonth){
                    if (parseInt(this.totalfare)>0){
                            tmpcount++;
                            if (datatype=="avg"){
                                tmpval+=parseInt(this.totalfare)/100;
                            }
                            if (datatype=="min"){
                                if (tmpval==0 || tmpval>parseInt(this.totalfare)/100){
                                    tmpval=parseInt(this.totalfare)/100;
                                }
                                
                            }
                            if (datatype=="max"){
                                if (tmpval==0 || tmpval<parseInt(this.totalfare)/100){
                                    tmpval=parseInt(this.totalfare)/100;
                                }
                            }
                    }
                }
            });
            if (datatype=="avg"){
                data.push(tmpval/tmpcount);
            }else{
                if (tmpval>0){
                    data.push(tmpval);
                }
            }
            labels.push ( tjq(this).text());
        }
    });
    var myChart = new Chart(ctx, {
        type: 'line',
        data: {
            labels: labels,
            datasets: [{
                label: tjq('#select-departurecity option:selected').text()+'< = >'+tjq('#select-arrivalcity option:selected').text()+tjq('#select-time').val()+'<?php echo lang("天往返") ?>'+tjq('#chardatatype option:selected').text()+"<?php echo lang('走势图') ?>",
                data: data,
                backgroundColor: [
                    'rgba(255, 99, 132, 0.2)'
                ],
                borderColor: [
                    'rgba(255,99,132,1)'
                ],
                borderWidth: 1
            }]
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero:false
                    }
                }]
            }
        }
    });
}
</script>