<script type="text/javascript">
var currency = '<?php echo $currency; ?>';
var request = new Request();

tjq(document).ready(function () {
    
    tjq('#submityorder').click(function (e) {
        e.preventDefault();
        tjq('#book-submit-errors').html('');

        if (!tjq('#checkTerms').is(':checked')) {
            tjq('#book-submit-errors').html('<i class="fa fa-times"></i><?php echo lang("请阅读本次活动细则，并选择已阅读。") ?> ');
            return
        }

        passchecked = true;

        tjq.each(tjq('#bookingform').find("input,select"), function () {
            if (tjq(this).is('[required]') && tjq(this).is(':visible')) {

                var val = request.getValFromObj(tjq(this));
                var showerrorobject = tjq(this);
                if (tjq(this).attr('groupid') != undefined) {
                    showerrorobject = tjq('#' + tjq(this).attr('groupid'));
                }

                if (val == undefined || val == "" || val.length == 0) {
                    passchecked = false;
                    request.showRequired(showerrorobject);
                } else {
                    request.removeRequired(showerrorobject);
                }
            }
        });

        if (passchecked) {
            if (!request.isEmail(tjq('#email').val())) {
                passchecked = false;
                request.showRequired(tjq('#email'));
            } else {
                request.removeRequired(tjq('#email'));
            }
        }

        if (!passchecked) {
            tjq('#book-submit-errors').html('<i class="fa fa-times"></i><?php echo lang("输入信息有误，请检查!") ?> ');
            return;
        } else {
            tjq(this).prop('disabled', true);
            tjq(this).hide();
            tjq('#doingbookoing').show();

            var data = {};
            data['routecode'] = tjq('#routecode').val();
            data['cabin'] = tjq('input[name=cabin]:checked').val();
            data['adult'] = tjq('#adult').val();
            data['departdate'] = tjq.trim(tjq('#departdate').val());
            data['schedule'] = tjq.trim(tjq('#schedule').val());
            data['remarks'] = tjq.trim(tjq('#remarks').val());
            data['firstname'] = tjq.trim(tjq('#firstname').val());
            data['middlename'] = tjq.trim(tjq('#middlename').val());
            data['lastname'] = tjq.trim(tjq('#lastname').val());
            data['passport'] = tjq.trim(tjq('#passport').val());
            data['email'] = tjq.trim(tjq('#email').val());
            data['tel'] = tjq.trim(tjq('#tel').val());
            data['address'] = tjq.trim(tjq('#address').val());
            data['city'] = tjq.trim(tjq('#city').val());
            data['country'] = tjq.trim(tjq('#country').val());
            data['postalcode'] = tjq.trim(tjq('#postalcode').val());
            data['currency'] = currency;

            var passengers = [];

            var passengercount = parseInt(data['adult']);
            for (var i = 1; i <= passengercount; i++) {
                var passenger = {};
                passenger['firstname'] = tjq.trim(tjq('#firstname_'+i).val());
                passenger['middlename'] = tjq.trim(tjq('#middlename_'+i).val());
                passenger['lastname'] = tjq.trim(tjq('#lastname_'+i).val());
                passenger['gender'] = tjq.trim(tjq('input[name=gender_'+i+']:checked').val());
                passenger['birthday'] = tjq.trim(tjq('#birthday_'+i).val());
                passenger['passport'] = tjq.trim(tjq('#passport_'+i).val());
                passenger['passportexpired'] = tjq.trim(tjq('#passportexpired_'+i).val());
                passenger['passportcountry'] = tjq.trim(tjq('#passportcountry_'+i).val());

                passengers.push(passenger);
            }

            data['passengers'] = passengers;

            FT_c_finalsale_tours(data, successBooking);
        }
    });
    
    tjq('#adult').change(function () {
        updateFee();
        updatePassengers();
    });

    tjq('input[name=cabin]').change(function () {
        updateFee();
    });

    updateFee();
});

function updatePassengers() {
    var adult = tjq('#adult').val();
    var passengercount = parseInt(adult);
    var passengerHtml = '';

    for (i = 1; i <= passengercount; i++) {
        passengerHtml += '<div class="person-information"><h3>客人'+i+'</h3>';
        passengerHtml += '<div class="form-group row">';
        passengerHtml += '<div class="col-sm-6 col-md-4">';
        passengerHtml += '<label for="firstname_'+i+'" placehold="*<?php echo lang('名字'); ?>">*<?php echo lang('名字'); ?></label>';
        passengerHtml += '<input class="input-text full-width" id="firstname_'+i+'" placeholder="" required="" type="text" value="" />';
        passengerHtml += '</div>';
        passengerHtml += '<div class="col-sm-6 col-md-4">';
        passengerHtml += '<label for="middlename_'+i+'" placehold="<?php echo lang('中间名'); ?>"><?php echo lang('中间名'); ?></label>';
        passengerHtml += '<input class="input-text full-width" id="middlename_'+i+'" placeholder="不是必填项目" type="text" value="" />';
        passengerHtml += '</div>';
        passengerHtml += '<div class="col-sm-6 col-md-4">';
        passengerHtml += '<label for="lastname_'+i+'" placehold="*<?php echo lang('姓'); ?>">*<?php echo lang('姓'); ?></label>';
        passengerHtml += '<input class="input-text full-width" id="lastname_'+i+'" placeholder="" required="" type="text" value="" />';
        passengerHtml += '</div>';
        passengerHtml += '</div>';

        passengerHtml += '<div class="form-group row">';
        passengerHtml += '<div class="col-sm-6 col-md-4">';
        passengerHtml += '<label for="gender_'+i+'" placehold="*<?php echo lang('性别'); ?>">*<?php echo lang('性别'); ?></label>';
        passengerHtml += '<input class="ng-pristine ng-untouched ng-valid" id="gender_'+i+'" name="gender_'+i+'" ng-model="contact.gender" required="" type="radio" value="M" />　男　　 <input class="ng-pristine ng-untouched ng-valid" name="gender_'+i+'" ng-model="contact.gender" required="" type="radio" value="F" />　女';
        passengerHtml += '</div>';
        passengerHtml += '<div class="col-sm-6 col-md-4">';
        passengerHtml += '<label for="birthday_'+i+'" placehold="*<?php echo lang('出生日期'); ?>">*<?php echo lang('出生日期'); ?></label>';
        passengerHtml += '<div class="datepicker-wrap"><input class="input-text full-width " date-type="date" id="birthday_'+i+'" picker-type="birthday" placeholder="年/月/日" required="" type="text" vid="birthday" /></div>';
        passengerHtml += '</div>';
        passengerHtml += '<div class="col-sm-6 col-md-4">';
        passengerHtml += '<label for="passport_'+i+'" placehold="*<?php echo lang('护照号码'); ?>">*<?php echo lang('护照号码'); ?></label>';
        passengerHtml += '<input class="input-text full-width" id="passport_'+i+'" placeholder="" required="" type="text" value="" />';
        passengerHtml += '</div>';
        passengerHtml += '</div>';

        passengerHtml += '<div class="form-group row">';
        passengerHtml += '<div class="col-sm-6 col-md-4">';
        passengerHtml += '<label for="passportexpired_'+i+'" placehold="*<?php echo lang('护照有效期'); ?>">*<?php echo lang('护照有效期'); ?></label>';
        passengerHtml += '<div class="datepicker-wrap"><input class="input-text full-width " date-type="date" id="passportexpired_'+i+'" picker-type="birthday" placeholder="年/月/日" required="" type="text" min-date="0" max-date="30" vid="passportexpired" /></div>';
        passengerHtml += '</div>';
        passengerHtml += '<div class="col-sm-6 col-md-4">';
        passengerHtml += '<label for="passportcountry_'+i+'" placehold="*<?php echo lang('护照签发地'); ?>">*<?php echo lang('护照签发地'); ?></label>';
        passengerHtml += '<input class="input-text full-width" id="passportcountry_'+i+'" placeholder="" required="" type="text" value="" />';
        passengerHtml += '</div>';
        passengerHtml += '</div>';
        passengerHtml += '</div>';
    }

    tjq('#passengers').html('');
    tjq('#passengers').html(passengerHtml);

    bindDatepickers();
}

function bindDatepickers() {

    // datepicker
    tjq('.datepicker-wrap input').each(function() {
        var pickertype=tjq(this).attr('picker-type');
        switch(pickertype){
            case "birthday":
                var maxDate = tjq(this).attr("max-date");
                if (typeof maxDate == "undefined") {
                    maxDate = 0;
                }
                var minDate=tjq(this).attr("min-date");
                if (typeof minDate == "undefined") {
                    minDate = 100;
                }

                var start=new Date();
                start.setFullYear(parseInt(start.getFullYear())+parseInt(maxDate));


                tjq(this).datepicker({
                    showOn: 'both',
                    buttonImage: 'assets/images/icon/blank.png',
                    buttonText: '',
                    buttonImageOnly: false,
                    changeYear: true,
                    yearRange: "-"+minDate+":+"+maxDate,
                    changeMonth: true,
                    hideIfNoPrevNext: true,
                    /*showOtherMonths: true,*/
                    //numberOfMonths: [1, 2],
                    /*showButtonPanel: true,*/
                    dateFormat: "yy-mm-dd",
                    dayNamesMin: dayNamesMin,
                    monthNames: monthNames,
                    monthNamesShort: monthNamesShort,
                    dayNames: dayNames,
                    dayNamesShort: dayNamesShort,
                    prevText:'hello',
                    beforeShow: function(input, inst) {
                        var themeClass = tjq(input).parent().attr("class").replace("datepicker-wrap", "");
                        tjq('#ui-datepicker-div').attr("class", "");
                        tjq('#ui-datepicker-div').addClass("ui-datepicker ui-widget ui-widget-content ui-helper-clearfix ui-corner-all ");
                        tjq('#ui-datepicker-div').addClass(themeClass);

                    },
                    onSelect:function(dateText,inst) {
                        return true;
                    }


                });

                return;
            case "departure":
                initDepartureDate(this);
            default:
                return;
        };

    });
}

var currencyPrix = '<?php echo $currencyPrix; ?>';
var singleFee = <?php echo $singleFee; ?>;
var tourfee = '<?php echo json_encode($tourfee); ?>';
tourfee = jQuery.parseJSON(tourfee);
var tipfee = <?php echo $tipfee; ?>;

function updateFee() {
    var adult = tjq('#adult').val();

    var cabin = tjq('input[name=cabin]:checked').val();
    var adultFee = tourfee[cabin];

    if (adult == 1) {
        var single = singleFee;
    } else {
        var single = 0;
    }
    
    var adultTotal = (parseInt(adultFee) + single + tipfee) * parseInt(adult);
    
    tjq('#singlefee').text(currencyPrix + single + '/人');
    tjq('#tourfee').text(currencyPrix + adultFee + '/人');
    tjq('#adultQuantity').text(adult);

    var total = adultTotal;

    tjq('#totalfare').text(currencyPrix + total);
}

function successBooking(data) {
    if (data[0] != 'error') {
        tjq('#doingbookoing').hide();
        tjq('#book-submit-errors').html('<i class="fa fa-check"></i><?php echo lang("非常感谢您选择华景！您的预定已经成功提交！") ?>');

        var result = data.split('|');
        var orderId = result[0];
        var token = result[1];

        window.location.href = 'index.php?id=finalsale-tours-order&orderId=' + orderId + '&token=' + token + '&fullsite=yes&change_lang=Chinese&';
    } else {
        tjq('#doingbookoing').hide();
        tjq('#submityorder').prop('disabled', false);
        tjq('#submityorder').show();
        
        tjq('#book-submit-errors').html('<i class="fa fa-times"></i>' + data[1]);
    }
}
</script>