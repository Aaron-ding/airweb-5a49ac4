<?php
$ip = '';
if (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
    $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
} elseif (!empty($_SERVER['HTTP_CLIENT_IP'])) {
    $ip = $_SERVER['HTTP_CLIENT_IP'];
} else {
    $ip = $_SERVER['REMOTE_ADDR'];
}
?>
<script type="text/javascript" src="https://js.stripe.com/v2/"></script>
<script type="text/javascript" src="assets/js/jquery.maskedinput.min.js"></script>
<script type="text/javascript" src="assets/js/typeahead.bundle.js"></script>
<script type="text/javascript" src="https://crud.sinorama.ca/getCountryCode.php?v=201704201"></script>
<script type="text/javascript" src="assets/js/countryCode.js?v=201701254"></script>
<script type="text/javascript" src="assets/js/callingCodes.js?v=20170420"></script>
<!-- 预订页右边栏随滚动条滚动 -->
<script type="text/javascript">

    var bookingErrors = {
        'soldout': '<?php echo lang('您所选择的机票已经售完，请重新选择！'); ?>',
        'systemException': '<?php echo lang('系统有异常，请联系我们的客服，1-866-255-2188'); ?>'
    };
    var stripeErrors = {
        'PNR_Save': "<?php echo lang('processing_error'); ?>",
        'PNR_Retrieve': "<?php echo lang('processing_error'); ?>",
        'FOP_CreateFormOfPayment': "<?php echo lang('CreditVerificationRQ_Failed'); ?>",
        'EndTransactionLLSRQ': "<?php echo lang('processing_error'); ?>",
        'TravelItineraryReadRQ': "<?php echo lang('processing_error'); ?>",
        'CreditVerificationRQ': "<?php echo lang('CreditVerificationRQ_Failed'); ?>",
        'AddRemarkLLSRQ_Creditcard': "<?php echo lang('processing_error'); ?>",
        'invalid_coupon': "<?php echo lang('invalid_coupon'); ?>",
        'coupon_processing_error': "<?php echo lang('coupon_processing_error'); ?>",
        'invalid_number': "<?php echo lang('invalid_number'); ?>",
        'invalid_expiry_month': "<?php echo lang('invalid_expiry_month'); ?>",
        'invalid_expiry_year': "<?php echo lang('invalid_expiry_year'); ?>",
        'invalid_cvc': "<?php echo lang('invalid_cvc'); ?>",
        'invalid_swipe_data': "<?php echo lang('invalid_swipe_data'); ?>",
        'incorrect_number': "<?php echo lang('incorrect_number'); ?>",
        'expired_card': "<?php echo lang('expired_card'); ?>",
        'incorrect_cvc': "<?php echo lang('incorrect_cvc'); ?>",
        'incorrect_zip': "<?php echo lang('incorrect_zip'); ?>",
        'card_declined': "<?php echo lang('card_declined'); ?>",
        'missing': "<?php echo lang('missing'); ?>",
        'processing_error': "<?php echo lang('processing_error'); ?>"
    };
    var request = new Request();
    request.fromJSONString('<?php echo addslashes($_REQUEST["request"]); ?>');
    request.clientIp = '<?php echo $ip; ?>';
    var personcount =<?php echo $_REQUEST["personcount"]; ?>;
    var currentyear =<?php echo date("Y"); ?>;
    var maxexpiredyear = 30;
    tjq(document).ready(function () {
        //tjq('#tel1').mask("999-999-9999");
        //tjq('#tel2').mask("999-999-9999");
        tjq('#callingCode').typeahead('val', '<?php echo lang('Canada (+1)'); ?>');
        tjq('#callingCode').attr('data', '<?php echo lang('Canada (+1)'); ?>');
        tjq('#callingCode').siblings('input').attr('data', '<?php echo lang('Canada (+1)'); ?>');

        tjq('#showFlyerMealSSR').click(function () {
            tjq(this).parent().parent().next().toggle();

            if (tjq(this).find('span').attr('class') == 'fa fa-angle-down') {
                tjq(this).find('span').attr('class', 'fa fa-angle-up');
            } else {
                tjq(this).find('span').attr('class', 'fa fa-angle-down');
            }
        });

        tjq(window).scroll(function () {
            var top = tjq(window).scrollTop() + 0;
            var right = tjq(window).scrollLeft() + 0;
            var lastbottom = tjq("#footer").offset().top - 40;
            var stickyTop = tjq("#editInfo").offset().top + tjq("#editInfo").outerHeight();
            var maintop = tjq("#content").offset().top + 40;
            var editfixtop = tjq(window).height() - tjq("#editInfo").outerHeight();
            var scrollBottom = tjq(window).scrollTop() + tjq(window).height();

            if (stickyTop >= lastbottom && stickyTop < (scrollBottom - editfixtop + maintop)) {
                top2 = lastbottom - tjq("#editInfo").outerHeight() - maintop;
                tjq("#editInfo").css({right: right + "px", top: top2 + "px"});
            } else {
                tjq("#editInfo").css({right: right + "px", top: top + "px"});
            }
        });
        request.showProcessBar(50);

        var goKey = JSON.parse(request.goKey);
        var returnKey = JSON.parse(request.returnKey);
        if (request.tripType == "multiple") {            
            var interarys=JSON.parse(request.internarys);
            var internary_template=tjq('h5.box-title').children().not('small').clone();
            interarys.forEach(function(ele,idx){
                if (idx==0){
                    tjq('h5.box-title').find('#departureCity').html(getCityNameByCode(ele.origin));
                    tjq('h5.box-title').find('img').replaceWith('<img src="assets/images/exchange-oneway.jpg">');
                    tjq('h5.box-title').find('#arriveCity').html(getCityNameByCode(ele.destination));
                }else{
                    var tmpinternary=internary_template.clone();                    
                    tjq(tmpinternary).filter('#departureCity').html(getCityNameByCode(ele.origin));
                    tjq(tmpinternary).filter('img').replaceWith('<img src="assets/images/exchange-oneway.jpg">');
                    tjq(tmpinternary).filter('#arriveCity').html(getCityNameByCode(ele.destination));
                    tjq('h5.box-title').children('small').before('<br>');
                    tjq('h5.box-title').children('small').before(tmpinternary);
                }
            });
            
        }else{
            tjq('#departureCity').html(getCityNameByCode(request.srccity));
            tjq('#arriveCity').html(getCityNameByCode(request.destcity));
        }

        tjq('.airimg').replaceWith('<img alt="" src="assets/images/logo/' + request.airline_logo + '.png" />');
        tjq('span#adultcount').text(request.adultcount);
        tjq('span#childcount').text(request.childrencount);
        tjq('span#ifantcount').text(request.ifantcount);
        var triptype = "";
        if (request.tripType == "roundtrip") {
            tjq('h5.box-title').children('img').replaceWith('<img src="assets/images/exchange-full.jpg">');
            triptype = "<?php echo lang('往返') ?>";
        }
        if (request.tripType == "oneway") {
            triptype = "<?php echo lang('单程') ?>";
            tjq('h5.box-title').children('img').replaceWith('<img src="assets/images/exchange-oneway.jpg">');
        }
        if (request.tripType == "multiple") {
            triptype = "<?php echo lang('多程') ?>";
            tjq('h5.box-title').children('img').replaceWith('<img src="assets/images/exchange-oneway.jpg">');
        }
        
        tjq('#triptype').text(triptype);
        tjq('#basefare').text(request.currencyShow + request.basefee);
        tjq('#taxfee').text(request.currencyShow + request.taxfee);
        tjq('#totalfare').text(request.currencyShow + request.totalfee);

        var detail_template = tjq('.constant-column-3.timing.clearfix').clone();
        var goseglist = JSON.parse(request.goKey);
        var returnlist = JSON.parse(request.returnKey);
        var airlines = [];
	
        tjq.each(goseglist.concat(returnlist), function (segidx, segval) {
			if (segval==""){return;}
            var tmptemplate = tjq(detail_template).clone();
            tjq(tmptemplate).css("display", "");
            tjq(tmptemplate).find('div.check-in').children('label').text(getCityNameByCode(segval.From));
            tjq(tmptemplate).find('div.check-out').children('label').text(getCityNameByCode(segval.To));
            
            var departdate = new Date(segval.DepartureDateTime+"Z");
            tjq(tmptemplate).find('div.check-in').children('span').html(request.formatDateYMD(departdate) + '<br>' + request.formatAMPM(departdate));            
            var arrivedate = new Date(segval.ArrivalDateTime+"Z");
            tjq(tmptemplate).find('div.check-out').children('span').html(request.formatDateYMD(arrivedate) + '<br>' + request.formatAMPM(arrivedate));
            tjq(tmptemplate).find('div.duration').children('span').html(segval.ElapsedTimeTotalHours + "<?php echo lang('时') ?>" + segval.ElapsedTimeTotalHoursLeftMins + "<?php echo lang('分') ?>");
            tjq('.constant-column-3.timing.clearfix').parent().append(tmptemplate);

            if (airlines.indexOf(segval.Airline) == '-1') {
                airlines.push(segval.Airline);
            }
        });

        for (var i = 1; i <= airlines.length; i++) {
            var airline = airlines[i-1];
            var airlineName = getAirlinesNameByCode(airline);
            var newflyer = tjq('#flyerplan').clone();
            tjq(newflyer).find("#flyernumber").attr('id', "flyernumber_" + airline);
            tjq(newflyer).find("label[for=flyernumber]").attr('placehold', airlineName);
            tjq(newflyer).find("label[for=flyernumber]").attr("for", "flyernumber_" + airline);
            tjq(newflyer).attr('id', "flyerplan_" + airline);
            tjq(newflyer).show();
            tjq('#flyermeal').prepend(newflyer);
        }
        tjq('#flyerplan').remove();

        var adultcount = parseInt(request.adultcount);
        var childrencount = parseInt(request.childrencount);
        var infantsseatcount = parseInt(request.infantsseatcount);
        var infantslapcount = parseInt(request.infantslapcount);
        for (var i = 2; i <= personcount; i++) {
            var newperson = tjq('[person-index=1]').clone();
            if (i <= adultcount) {
                var persontype = 'ADT';
                if (i <= infantslapcount) {
                    var persontitle = '<?php echo lang('adult_with_infantinlap_title'); ?>';
                } else {
                    var persontitle = '<?php echo lang('adult_title'); ?>';
                }
            } else if (i > adultcount && i <= (adultcount + childrencount)) {
                var persontype = 'CNN';
                var persontitle = '<?php echo lang('child_title'); ?>';
            } else if (i > (adultcount + childrencount) && i <= (adultcount + childrencount + infantsseatcount)) {
                var persontype = 'INS';
                var persontitle = '<?php echo lang('infant_seat_title'); ?>';
            } else {
                var persontype = 'INF';
                var persontitle = '<?php echo lang('infant_lap_title'); ?>';
            }
            tjq(newperson).attr('person-type', persontype);
            tjq(newperson).attr('person-index', i);
            var tmphead = tjq(newperson).children('h3').html();
            tjq(newperson).children('h3').html(tmphead + i + ' - ' + persontitle);
            tjq(newperson).find("[name=gender]").removeAttr("checked");
            tjq(newperson).find("[name=gender]").removeAttr("disabled");
            tjq(newperson).find("[name=gender]").attr("name", "gender" + i);
            tjq(newperson).find("#birthday_1").attr('vid', "birthday");
            tjq(newperson).find("#birthday_1").attr('id', "birthday_" + i);
            tjq(newperson).find("label[for=birthday_1]").attr("for", "birthday_" + i);
            tjq(newperson).find("#passportexpired_1").attr('vid', "passportexpired");
            tjq(newperson).find("#passportexpired_1").attr('id', "passportexpired_" + i);
            tjq(newperson).find("label[for=passportexpired_1]").attr("for", "passportexpired_" + i);
            tjq(newperson).find("#passportcountry_1").attr('vid', "passportcountry");
            tjq(newperson).find("#passportcountry_1").attr('id', "passportcountry_" + i);
            tjq(newperson).find("label[for=passportcountry_1]").attr("for", "passportcountry_" + i);
            tjq(newperson).find("#nationality_1").attr('vid', "nationality");
            tjq(newperson).find("#nationality_1").attr('id', "nationality_" + i);
            tjq(newperson).find("label[for=nationality_1]").attr("for", "nationality_" + i);

            tjq(newperson).find('#showFlyerMealSSR').click(function () {
                tjq(this).parent().parent().next().toggle();

                if (tjq(this).find('span').attr('class') == 'fa fa-angle-down') {
                    tjq(this).find('span').attr('class', 'fa fa-angle-up');
                } else {
                    tjq(this).find('span').attr('class', 'fa fa-angle-down');
                }
            });

            tjq('[person-index=' + (i - 1) + ']').after(newperson);
        }

        for (var i = 1; i <= adultcount; i++) {
            tjq('#contactindex')
                    .append(tjq("<option></option>")
                            .attr("value", i)
                            .text("<?php echo lang('联系人为乘客') ?>" + i));
        }
        var tmphead = tjq('[person-index=1]').children('h3').html();
        if (infantslapcount > 0) {
            var firstAdultTitle = tmphead + "1 - " + '<?php echo lang('adult_with_infantinlap_title'); ?>';
        } else {
            var firstAdultTitle = tmphead + "1 - " + '<?php echo lang('adult_title'); ?>';
        }
        tjq('[person-index=1]').children('h3').html(firstAdultTitle);

        tjq.mask.definitions['a'] = "[1-2]";
        tjq.mask.definitions['b'] = "[0-1]";
        tjq.mask.definitions['c'] = "[0-3]";
        tjq("input[id^=birthday]").mask("a999-b9-c9");
        tjq("input[id^=passportexpired]").mask("a999-b9-c9");

        initCountryInput();

        request.showProcessBar(10);
        setTimeout(function () {

            request.showProcessBar(100);
            tjq('#booking-content').show();
        }, 1000);
        
        for (var i = 0; i < maxexpiredyear; i++) {
            var tmpyear = currentyear + i;
            tjq('#expireddateyear')
                    .append(tjq("<option></option>")
                            .attr("value", tmpyear)
                            .text(tmpyear));
        }

        searchInit();

        var rsToken = getCookie('rsToken');
        console.log(rsToken);
        if (rsToken) {
            FT_c_getPassengersFromSession(rsToken, getPassengersSuccess);
        }


        tjq('#printbookingconfirmation, #printbookingreceipt').click(function (e) {
            e.preventDefault();

            if (tjq("span#PNR").text().length > 0 && tjq("span#orderid").text().length > 0) {
                window.open('https://<?php  echo $GLOBALS["crud_server"]; ?>/index.php/modules/pnrofqueue/pnrreceipt_customer/' + tjq("span#PNR").text() + '/' + tjq("span#orderid").text());
            }
        });

        tjq('#emailbookingconfirmation').click(function (e) {
            e.preventDefault();

            if (tjq("span#PNR").text().length > 0 && tjq("span#orderid").text().length > 0) {
                var para = {};
                para['PNR'] = tjq("span#PNR").text();
                para['orderid'] = tjq("span#orderid").text();
                FT_c_emailBookingConfirmation(para, function (rs) {
                    if (rs != undefined && rs[0] == 'success') {
                        alert('<?php echo lang('已将预定确认单发送到您的邮箱，请查收'); ?>');
                    } else {
                        alert('<?php echo lang('服务器未响应，请稍后再试，谢谢'); ?>');
                    }
                });
            }
        });

        tjq('#submityorder').click(function (e) {
            e.preventDefault();
            tjq('#book-submit-errors').html('');
            if (false == request.checkForm('bookingform')) {
                tjq('#book-submit-errors').html('<i class="fa fa-times"></i><?php echo lang("输入信息有误，请检查!") ?> ');
                return;
            }

            //check passger type
            var checkPassengerType = true;
            tjq.each(tjq('.person-information'), function () {
                var checktype = true;
                var persontype = tjq(this).attr('person-type');
                var dateText = tjq(this).find('input[id^=birthday_]').val();
                var departdate=request.getLastDepardate();
                var age=getAge(dateText,departdate);
                if (age >= 12 && persontype != 'ADT') {
                    checktype = false;
                }

                if ((age>=2 && age<12) && persontype != 'CNN'){
                    checktype = false;
                }

                if (age<2 && (persontype != 'INF' && persontype != 'INS')){
                    checktype = false;
                }

                if (!checktype) {
                    checkPassengerType = false;
                    request.showRequired(tjq(this).find('input[id^=birthday_]'));
                } else {
                    request.removeRequired(tjq(this).find('input[id^=birthday_]'));
                }
            });

            if (false) {
                tjq('#book-submit-errors').html('<i class="fa fa-times"></i><?php echo lang("根据航空公司的规定，年龄小于18岁的乘客不能独立出行") ?> ');
                return;
            }

            if (!checkPassengerType) {
                tjq('#book-submit-errors').html('<i class="fa fa-times"></i> <?php echo lang("乘客年龄和询价的乘客类型不符，请检查或重新查询") ?>');
                return;
            }

            var orderobj=this;
            tjq('#agreeandpay').unbind().click(function () {

                var rs = {};
                var personlist = new Array();
                tjq.each(tjq('.person-information'), function () {
                    var persontype = tjq(this).attr('person-type');
                    var personindex = tjq(this).attr('person-index');
                    var personobj = tjq(this);
                    var person = {};
                    person['flyernumbers'] = [];
                    person['index'] = personindex;
                    person['passengertype'] = persontype;
                    tjq.each(tjq(this).find("input,select"), function () {
                        var id = tjq(this).attr('id');
                        if (typeof id == 'undefined' || id == false) {
                            return;
                        }
                        var vid = tjq(this).attr('vid');
                        if (typeof vid !== typeof undefined && vid !== false) {
                            id = vid;
                        }
                        var val = request.getValFromObj(tjq(this));

                        if (id !== undefined && val != undefined) {
                            if (personindex == "contact") {
                                rs[id] = val;
                            } else {
                                person[id] = val;

                                if (id.indexOf('flyernumber_') != '-1' && val != '') {
                                    var airline = id.substr(-2);
                                    person['flyernumbers'].push(airline + val);
                                }
                            }
                        }

                    });
                    if (personindex != "contact") {
                        personlist.push(person);
                        rs["passengers"] = personlist;
                    }
                });
                var matches = rs['callingCode'].match(/(.*)\(\+(\d+)\)/);
                rs['tel'] = matches[2] + '-' + rs['tel1'];
                console.log(rs['tel']);

                request.booking = rs;

                var rsToken = getCookie('rsToken');
                if (rsToken) {
                    request.booking['rsToken'] = rsToken;
                }
                /*
                 tjq('#booking-content').hide();
                 request.showProcessBar(0);

                 request.showProcessBar(10);

                 */
                tjq('#soap-popupbox').click();
                tjq(orderobj).hide();
                //tjq('#doingbookoing').show();
                tjq('#booking-progress').show();
                FT_c_storePassengersToSession(request, storePassengersSuccess);
                console.log(request);
                FT_c_doBooking(request, successBooking, function (value) {
                    tjq('#booking-progress').children('div').attr('aria-valuenow', value).css('width', value + '%').text(value + '%');
                });
            });
            tjq('a[onload=loaduserterms]')[0].click();
            return;
        });

        tjq('#airlineSystem').click(function () {
            tjq(this).parent().find('a').removeClass('active');
            tjq(this).addClass('active');
            tjq('#totalfare').text(request.currencyShow + request.totalfee);
            tjq('#interchangefee').text(request.currencyShow + '0.00');
        });

        tjq('#stripe').click(function () {
            tjq(this).parent().find('a').removeClass('active');
            tjq(this).addClass('active');
            updateInterchangefee();
        });

        <?php if ($currency != 'CAD') { ?>
        updateInterchangefee();
        <?php } ?>
    });

    function updateInterchangefee() {
        tjq('#totalfare').text(request.currencyShow + (request.totalfee * 100 + Math.round(request.totalfee * 3)) / 100);
        tjq('#interchangefee').text(request.currencyShow + Math.round(request.totalfee * 3) / 100);
    }
    function searchInit() {
        var searchform = tjq('form[id=searchagain]');
        searchform.find("input[name=srccity]").val(request.srccity);
        searchform.find("input[name=destcity]").val(request.destcity);
        searchform.find("input[name=departdate]").val(request.departdate);
        searchform.find("input[name=returndate]").val(request.returndate);
        searchform.find("input[name=adultcount]").val(request.adultcount);
        searchform.find("input[name=childrencount]").val(request.childrencount);
        searchform.find("input[name=infantsseatcount]").val(request.infantsseatcount);
        searchform.find("input[name=infantslapcount]").val(request.infantslapcount);
        searchform.find("input[name=tripType]").val(request.tripType);
        searchform.find("input[name=PreferLevel]").val(request.PreferLevel);
    }

    //取cookies函数
    function getCookie(name){
        var arr = document.cookie.match(new RegExp("(^| )"+name+"=([^;]*)(;|$)"));
        if(arr != null) return unescape(arr[2]); return null;
    }
    
    //设置cookie
    function setCookie(name,value){
        var exp = new Date();
        exp.setTime(exp.getTime() + 1*60*60*1000);//有效期1小时
        document.cookie = name + "="+ escape (value) + ";expires=" + exp.toGMTString();
    }

    function storePassengersSuccess(rsToken) {
        if (rsToken != 'error') {
            setCookie('rsToken', rsToken);
        }
    }

    function getPassengersSuccess(resp) {
        if (resp != 'error') {
            for (var key in resp.d) {
                if (tjq.inArray(key, ['contactindex', 'contactfirstname', 'contactlastname', 'email', 'tel1', 'tel2', 'address', 'city', 'province', 'country', 'postcode', 'callingCode']) >= 0) {
                    if (key == 'country' || key == 'callingCode') {
                        tjq('div[person-index=contact]').find('#' + key).attr('data', resp.d[key]);
                        tjq('div[person-index=contact]').find('#' + key).prev().attr('data', resp.d[key]);
                        tjq('div[person-index=contact]').find('#' + key).typeahead('val', resp.d[key]);
                    } else {
                        tjq('div[person-index=contact]').find('#' + key).val(resp.d[key]);
                    }
                } else {
                    var underlinePos = key.lastIndexOf('_');
                    if (underlinePos >= 0) {
                        var personIndex = key.substring(underlinePos + 1, key.length);
                        var inputVal = key.substring(0, underlinePos);

                        if (inputVal == 'gender') {
                            if (personIndex > 1) {
                                tjq('div[person-index=' + personIndex + ']').find('input[name="gender' + personIndex + '"]').filter('[value="' + resp.d[key] + '"]').attr('checked', true);
                            } else {
                                tjq('div[person-index=' + personIndex + ']').find('input[name="gender"]').filter('[value="' + resp.d[key] + '"]').attr('checked', true);
                            }
                        } else if (inputVal == 'birthday' || inputVal == 'passportexpired') {
                            tjq('div[person-index=' + personIndex + ']').find('#' + key).datepicker('setDate', resp.d[key]);
                        } else if (inputVal == 'passengertype' || inputVal == 'mealpreference' || inputVal == 'ssr') {
                            tjq('div[person-index=' + personIndex + ']').find('#' + inputVal).val(resp.d[key]).change();
                        } else if (inputVal == 'passportcountry' || inputVal == 'nationality') {
                            tjq('div[person-index=' + personIndex + ']').find('#' + key).attr('data', resp.d[key]);
                            tjq('div[person-index=' + personIndex + ']').find('#' + key).prev().attr('data', resp.d[key]);
                            tjq('div[person-index=' + personIndex + ']').find('#' + key).typeahead('val', resp.d[key]);
                        } else {
                            tjq('div[person-index=' + personIndex + ']').find('#' + inputVal).val(resp.d[key]);
                        }
                    }
                }
            }
        }
    }

    function successBooking(msg) {
        if (msg!=undefined && msg[0] == "error") {
            /*
            tjq('extra_error').html('');
            tjq('#booking-content').show();
            tjq('div#main').fadeOut();
            tjq('#bookingerror').height(parseInt(tjq('#editInfo').height()) + 40);
            tjq('#bookingerror').fadeIn();
            tjq('#editInfo').find('a').hide();
            if (msg.length >= 2) {
                tjq('#extra_error').html(msg[1]);
            }

            request.showProcessBar(100);
            */
            tjq('#booking-progress').hide();
            tjq('#doingbookoing').hide();
            tjq('#modifyorder').html('<?php echo lang("重新选择"); ?>');
            tjq('#modifyorder').click(function(){
                searchagain();
            });
            tjq('#modifyorder').show();
            console.log(msg)
            if (msg.length >= 2) {
                tjq('#book-submit-errors').html(bookingErrors[msg[1]]);
            }
        } else {
            /*
             tjq('#PNR').text(msg[0]);
             tjq('#adultcount').text(request["adultcount"]);
             tjq('#childcount').text(request["childrencount"]);
             tjq('#ifantcount').text(request["ifantcount"]);
             tjq('#booking-content').show();
             tjq('div#main').fadeOut();
             tjq('#bookingsuccess').height(parseInt(tjq('#editInfo').height()) + 40);
             tjq('#bookingsuccess').fadeIn();
             tjq('#editInfo').find('a').hide();
             request.showProcessBar(100);
             */
            tjq('#PNR').text(msg.PNR);
            tjq('#orderid').text(msg.orderid);

            if (msg['updateFee']) {
                updateTicketFee(msg);
            }

            tjq('#paymentsuccess').click(function () {
                tjq('.payment-opacity-overlay').hide();

                FT_c_flights_ticketing_atos(msg, function (results) {
                    if (results[0] == 'error') {
                        if (results[1] == 'noPayment') {
                            tjq('#paymentmsg').text('<?php echo lang("订单支付失败"); ?>');
                        }
                    } else if (results[0] == 'success') {
                        tjq('#PNR').text(msg.PNR);
                        tjq('#adultcount').text(request["adultcount"]);
                        tjq('#childcount').text(request["childrencount"]);
                        tjq('#ifantcount').text(request["ifantcount"]);
                        tjq('#booking-content').show();
                        tjq('div#main').fadeOut();
                        tjq('#bookingsuccess').height(parseInt(tjq('#editInfo').height()) + 40);
                        tjq('#bookingsuccess').fadeIn();
                        tjq('#editInfo').find('a').hide();
                        tjq('.flight-booking-details').find('a').show();
                    }
                });
            });

            tjq('#paymentfailed').click(function () {
                tjq('.payment-opacity-overlay').hide();

                FT_c_flights_ticketing_atos(msg, function (results) {
                    if (results[0] == 'error') {
                        if (results[1] == 'noPayment') {
                            tjq('#paymentmsg').text('<?php echo lang("订单支付失败"); ?>');
                        }
                    } else if (results[0] == 'success') {
                        tjq('#PNR').text(msg.PNR);
                        tjq('#adultcount').text(request["adultcount"]);
                        tjq('#childcount').text(request["childrencount"]);
                        tjq('#ifantcount').text(request["ifantcount"]);
                        tjq('#booking-content').show();
                        tjq('div#main').fadeOut();
                        tjq('#bookingsuccess').height(parseInt(tjq('#editInfo').height()) + 40);
                        tjq('#bookingsuccess').fadeIn();
                        tjq('#editInfo').find('a').hide();
                        tjq('.flight-booking-details').find('a').show();
                    }
                });
            });

            tjq('#submitorder').click(function (e) {
                e.preventDefault();
                tjq('#submit-errors').html('');

                if(!tjq('#airlineSystem').hasClass('collapsed')) {
                    var formId = 'ASpaymentform';
                } else if (!tjq('#stripe').hasClass('collapsed')) {
                    var formId = 'stripepaymentform';
                }

                if (false == request.checkForm(formId)) {
                    tjq('#submit-errors').html('<i class="fa fa-times"></i><?php echo lang("输入信息有误，请检查!"); ?> ');
                    return;
                }

                var form = tjq('#' + formId);
                var expirelist = form.find('#expireddate').val().split("/");
                if (expirelist.length != 2) {
                    request.showRequired(form.find('#expireddate'));
                    return;
                }

                //start pay by stripe
                <?php if($GLOBALS["branch"] == "master"){ ?>
                Stripe.setPublishableKey('pk_live_B5SZw46G2GUaRIWOD1s1YjdW');
                <?php } ?>
                <?php if($GLOBALS["branch"] == "dev"){ ?>
                Stripe.setPublishableKey('pk_test_XEL2ac8zWkzRTSF9NQPfqJ49');
                <?php } ?>

                if (!Stripe.card.validateCardNumber(form.find('#cardnumber').val())) {
                    tjq('#submit-errors').html('<i class="fa fa-times"></i>' + stripeErrors['invalid_number']);
                    return;
                }

                var creditcardType = getCreditcardType(form.find('#cardnumber').val());
                if (creditcardType == '') {
                    tjq('#submit-errors').html('<i class="fa fa-times"></i>' + stripeErrors['invalid_number']);
                    return;
                }

                if (!Stripe.card.validateExpiry(expirelist[0].trim(), expirelist[1].trim())) {
                    tjq('#submit-errors').html('<i class="fa fa-times"></i><?php echo lang("有效期"); ?><?php echo lang("格式错误。"); ?> ');
                    return;
                }

                if (!Stripe.card.validateCVC(form.find('#seccode').val())) {
                    tjq('#submit-errors').html('<i class="fa fa-times"></i>' + stripeErrors['invalid_cvc']);
                    return;
                }

                form.find('input[data-stripe=exp-month]').val(expirelist[0].trim());
                form.find('input[data-stripe=exp-year]').val(expirelist[1].trim());

                var parentbutton = this;
                //validate coupon
                tjq(parentbutton).hide();
                //tjq('#doingpayment').show();
                tjq('#payment-progress').show();
                vlidateCoupon(function () {
                    // Disable the submit button to prevent repeated clicks
                    form.find('button').prop('disabled', true);
                    msg['couoncode'] = tjq('#couoncode').val();

                    var cardNumber = form.find('#cardnumber').val().trim();
                    if (expirelist[1].trim().length == 2) {
                        var expYear = '20' + expirelist[1].trim();
                    } else {
                        var expYear = expirelist[1].trim();
                    }

                    msg['booking']["card"] = {
                        'name': form.find('#cardname').val().trim(),
                        'number': cardNumber,
                        'last4': cardNumber.substring(cardNumber.length-4),
                        'exp_year': expYear,
                        'exp_month': expirelist[0].trim(),
                        'brand': creditcardType,
                        'seccode': form.find('#seccode').val().trim()
                    };

                    if(!tjq('#airlineSystem').hasClass('collapsed')) {
                        msg['creditcardProviders'] = 'airlineSystem';
                    } else if (!tjq('#stripe').hasClass('collapsed')) {
                        msg['creditcardProviders'] = 'stripe';
                    }

                    FT_c_charge_ticket(msg, function (chargers) {
                        if (chargers != undefined && chargers[0] != 'error') {
                            tjq('#PNR').text(msg.PNR);
                            tjq('#adultcount').text(request["adultcount"]);
                            tjq('#childcount').text(request["childrencount"]);
                            tjq('#ifantcount').text(request["ifantcount"]);
                            tjq('#booking-content').show();
                            tjq('div#main').fadeOut();
                            tjq('#bookingsuccess').height(parseInt(tjq('#editInfo').height()) + 40);
                            tjq('#bookingsuccess').fadeIn();
                            tjq('#editInfo').find('a').hide();
                            tjq('.flight-booking-details').find('a').show();
                        } else {
                            tjq('#submit-errors').html('<i class="fa fa-times"></i> ' + stripeErrors[chargers[1]]);
                            tjq(form).find('button').prop('disabled', false);
                            tjq(parentbutton).show();
                            tjq('#doingpayment').hide();
                            tjq('#payment-progress').hide();
                        }
                    }, function (value) {
                        tjq('#payment-progress').children('div').attr('aria-valuenow', value).css('width', value + '%').text(value + '%');
                    });

                    return false;
                });
            });

            tjq('#booking-progress').hide();
            tjq('#doingbookoing').hide();
            tjq('#bookingform input').attr('readonly', 'readonly');
            tjq('#ticketbooked').html("<?php echo lang("已经为您保留了位置，请尽快付款，超过规定时间，位置将会被释放!"); ?>");
            tjq('.information-box').show();

            if (tjq("span#PNR").text().length > 0) {
                var atos = tjq('#atosIframe');
                tjq(atos).attr('src', '<?php  echo WEBSITEURL; ?>index.php?id=ft-atos&PNR=' + tjq("span#PNR").text());
            }

            tjq('#couponForm').show();
            tjq('#payment').fadeIn("slow");
            tjq('#editInfo').find('a').hide();
            tjq('.flight-booking-details').find('a').show();
        }
        window.onbeforeunload = function () {
            window.location.replace("/ft/index.php?id=search");
            return false;
        }

    }

    tjq('#contactindex').change(
            function () {
                var personidx = tjq(this).val();
                if (personidx == 0) {
                    tjq('#contactfirstname').val('');
                    tjq('#contactlastname').val('');
                } else {

                    tjq('#contactfirstname').val(tjq('div[person-index=' + personidx + ']').find('#firstname').val());
                    tjq('#contactlastname').val(tjq('div[person-index=' + personidx + ']').find('#lastname').val());
                }

            }
    );

    function getAge(dateString,departdate) {
        var today = new Date(departdate);
        var birthDate = new Date(dateString);
        var age = today.getFullYear() - birthDate.getFullYear();
        var m = today.getMonth() - birthDate.getMonth();
        if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate()))
        {
            age--;
        }
        return age;
    }

    function isNumber(n) {
        return !isNaN(parseFloat(n)) && isFinite(n);
    }

    function updateTicketFee(para) {
        request.totalfee = para['totalfee'];
        request.basefee = para['basefee'];
        request.taxfee = para['taxfee'];

        tjq('#basefare').text(request.currencyShow + request.basefee);
        tjq('#taxfee').text(request.currencyShow + request.taxfee);
        tjq('#totalfare').text(request.currencyShow + request.totalfee);

        var ticketFeeUpdated = tjq('#ticketFeeUpdated').text();
        ticketFeeUpdated = ticketFeeUpdated.replace(/%%FORMERFEE%%/i, request.currencyShow + para['formertotalfee']);
        ticketFeeUpdated = ticketFeeUpdated.replace(/%%CURRENTFEE%%/i, request.currencyShow + request.totalfee);
        tjq('#ticketFeeUpdated').text(ticketFeeUpdated).show();
    }
</script>
<script src="assets/js/card.js"></script>
<script>
    tjq(document).ready(function () {
        var card = new Card({
            form: document.querySelector('form[id=stripepaymentform]'),
            container: '#stripe-card-wrapper'
        });
        var AScard = new Card({
            form: document.querySelector('form[id=ASpaymentform]'),
            container: '#AS-card-wrapper'
        });
        tjq('#couoncode').on('input', function (e) {
            tjq('[id^=coupon_info]').hide();
        });

    });
    function searchagain() {
	var actionurl=tjq('form[id=searchagain]').attr('action')+"<?php echo $GLOBALS["fullsite"] ?>";
        tjq('form[id=searchagain]').attr('action',actionurl);
        tjq('form[id=searchagain]').attr('action');		
        tjq('form[id=searchagain]').submit();
    }

    function loaduserterms()
    {
        loadflightdetail();
        tjq('#userterms').find('button').show();
        tjq('#userterms').show();
    }
    function loadtermsonly()
    {
        tjq('#userterms').find('button').hide();
        tjq('#orderdetails').hide();
        tjq('#userterms').show();
    }
    function loadflightdetail()
    {
        if (request.tripType == "multiple") {
            var rawlist=JSON.parse(request.goRaw);
            rawlist.forEach(function(ele,idx){
                ele.internaryTitle="行程"+(idx+1);
                if (idx==0){
                    loadInternaryDetail('multiple', ele);
                }else{
                    loadInternaryDetail('multiple', ele, 'no');
                }
            });
        }else{   
            loadInternaryDetail('go', request.goRaw);
            if (request.tripType == "roundtrip") {
                loadInternaryDetail('return', request.returnRaw, 'no');
            }
        }    
    }

    function getCreditcardType(number) {
        // visa
        var re = new RegExp("^4");
        if (number.match(re) != null)
            return "VI";

        // Mastercard
        re = new RegExp("^5[1-5]");
        if (number.match(re) != null)
            return "CA";

        // AMEX
        re = new RegExp("^3[47]");
        if (number.match(re) != null)
            return "AX";

        return "";
    }

    function vlidateCoupon(f) {
        tjq('[id^=coupon_info]').hide();
        tjq('#coupon_extra_error').html("");
        tjq('#coupon_amt').html('');
        if (tjq('#couoncode').val().length > 0) {
            var couoncode = tjq('#couoncode').val();
            FT_c_validateCoupon(couoncode, function(result) {
                if (result[0] == 'success') {
                    tjq('#coupon_amt').html(result[1].currency + ' ' + result[1].amt);
                    tjq('#coupon_info_success').show();
                    if (typeof f == "function") {
                        f();
                    }
                } else {
                    tjq('#coupon_info').show();
                    if (typeof f == "function") {
                        tjq('#submit-errors').html('<i class="fa fa-times"></i> <?php echo lang("您输入了优惠券，但验证不通过") ?>');
                        tjq('#submitorder').show();
                        tjq('#doingpayment').hide();
                    }
                }
            });
        } else {
            if (f == undefined) {
                tjq('#coupon_extra_error').html("--<?php echo lang('请输入优惠券号') ?>");
                tjq('#coupon_info').show();
            } else {
                f();
            }
        }

    }
</script>