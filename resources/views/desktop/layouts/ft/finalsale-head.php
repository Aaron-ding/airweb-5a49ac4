<?php
if (isset($_REQUEST) && isset($_REQUEST['routecode'])) {
    $routecode = $_REQUEST['routecode'];
} else {
    $routecode = 'CN-JSZJ';
}
?>
<!DOCTYPE html>
<!--[if IE 8]>          <html class="ie ie8"> <![endif]-->
<!--[if IE 9]>          <html class="ie ie9"> <![endif]-->
<!--[if gt IE 9]><!-->  <html>
    <!--<![endif]-->
    <head>
        <!-- Page Title -->
        <title><?php echo lang('华景机票') ?></title>

        <!-- Meta Tags -->
        <meta charset="utf-8">
        <meta name="keywords" content="机票,特价机票,便宜机票,加航,国航,亚洲旅游" />
        <meta name="description" content="提供专业的机票服务，提供特价机票，提供便宜机票">
        <meta name="author" content="sinorama">

        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <!-- Theme Styles -->
        <link rel="stylesheet" href="assets/css/bootstrap.min.css">
        <link rel="stylesheet" href="assets/css/font-awesome.min.css">
        <link href="http://fonts.googleapis.com/css?family=Lato:300,400,500,700" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="assets/css/animate.min.css">

        <!-- Current Page Styles -->
        <link rel="stylesheet" type="text/css" href="assets/components/revolution_slider/css/settings.css" media="screen" />
        <link rel="stylesheet" type="text/css" href="assets/components/revolution_slider/css/style.css?v=20160819" media="screen" />
        <link rel="stylesheet" type="text/css" href="assets/components/jquery.bxslider/jquery.bxslider.css" media="screen" />
        <link rel="stylesheet" type="text/css" href="assets/components/flexslider/flexslider.css" media="screen" />
        <style>
			<?php 			
			if ($GLOBALS["branch"]=="dev"){ ?>
					section#content{				
					
					background:url(/flight_front/assets/images/fttest_bg.jpg) repeat!important;}
			<?php } ?>
		</style>	
		
		<!-- chart js -->
		<script src="assets/js/Chart.min.js"></script>
		
        <!-- Main Style -->
        <link id="main-style" rel="stylesheet" href="assets/css/style.css?v=20160808">

        <!-- Custom Styles -->
        <link rel="stylesheet" href="assets/css/custom.css">

        <!-- Updated Styles -->
        <link rel="stylesheet" href="assets/css/updates.css">

        <!-- Responsive Styles -->
        <link rel="stylesheet" href="assets/css/responsive.css">

        <!-- CSS for IE -->
                    <!--[if lte IE 9]>
                                            <link rel="stylesheet" type="text/css" href="assets/css/ie.css" />
                                        <![endif]-->
        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
                    <!--[if lt IE 9]>
                                          <script type='text/javascript' src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
                                          <script type='text/javascript' src="http://cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.js"></script>
                                        <![endif]-->
    </head>
    <body>
        <div id="page-wrapper">
            <header id="header-normal" class="navbar-static-top" <?php if (isset($_SESSION["showhead"]) && $_SESSION["showhead"]=="no"){ ?> style="display:none" <?php } ?>>
                <div class="topnav">
                    <div class="container">
                        <h1 class="logo navbar-brand pull-left">
                            <a href="javascript:void(0);" title="Travelo - home">
                                <img src="assets/images/mail-logo.png" alt="SINORAMA HTML5 Template" />
                            </a>
                        </h1>
                        <ul class="quick-menu pull-right">
                            <li class="telephone">1-866-255-2188</li>
                            <?php if ($id == 'finalsale-tours') { ?>
                            <li class="ribbon currency">
                                <a href="javascript:void(0);" title="">
                                    <?php if ($currency == "CAD") { ?>
                                        <?php echo lang('加元'); ?> <?php echo lang('C$'); ?>
                                    <?php } ?>
                                    <?php if ($currency == "EUR") { ?>
                                        <?php echo lang('欧元'); ?> <?php echo lang('€'); ?>
                                    <?php } ?>
                                </a>
                                <ul class="menu mini">
                                    <li<?php if ($currency == "CAD") { echo ' class="active"'; } ?>><a href="<?php echo WEBSITEURL; ?>index.php?id=finalsale-tours&routecode=<?php echo $routecode; ?>&fullsite=yes&change_lang=Chinese&change_currency=CAD&" title="<?php echo lang('加元'); ?>"><?php echo lang('加元'); ?> <?php echo lang('C$'); ?></a></li>
                                    <li<?php if ($currency == "EUR") { echo ' class="active"'; } ?>><a href="<?php echo WEBSITEURL; ?>index.php?id=finalsale-tours&routecode=<?php echo $routecode; ?>&fullsite=yes&change_lang=Chinese&change_currency=EUR&" title="<?php echo lang('欧元'); ?>"><?php echo lang('欧元'); ?> <?php echo lang('€'); ?></a></li>
                                </ul>
                            </li>
                            <?php } ?>
                        </ul>
                    </div>
                </div>
                <div id="travelo-signup" class="travelo-signup-box travelo-box">
                    <div class="login-social">
                        <a href="assets/#" class="button login-facebook"><i class="soap-icon-facebook"></i>Login with Facebook</a>
                        <a href="assets/#" class="button login-googleplus"><i class="soap-icon-googleplus"></i>Login with Google+</a>
                    </div>
                    <div class="seperator"><label>OR</label></div>
                    <div class="simple-signup">
                        <div class="text-center signup-email-section">
                            <a href="assets/#" class="signup-email"><i class="soap-icon-letter"></i>Sign up with Email</a>
                        </div>
                        <p class="description">By signing up, I agree to Travelo's Terms of Service, Privacy Policy, Guest Refund olicy, and Host Guarantee Terms.</p>
                    </div>
                    <div class="email-signup">
                        <form>
                            <div class="form-group">
                                <input type="text" class="input-text full-width" placeholder="first name">
                            </div>
                            <div class="form-group">
                                <input type="text" class="input-text full-width" placeholder="last name">
                            </div>
                            <div class="form-group">
                                <input type="text" class="input-text full-width" placeholder="email address">
                            </div>
                            <div class="form-group">
                                <input type="password" class="input-text full-width" placeholder="password">
                            </div>
                            <div class="form-group">
                                <input type="password" class="input-text full-width" placeholder="confirm password">
                            </div>
                            <div class="form-group">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox"> Tell me about Travelo news
                                    </label>
                                </div>
                            </div>
                            <div class="form-group">
                                <p class="description">By signing up, I agree to Travelo's Terms of Service, Privacy Policy, Guest Refund Policy, and Host Guarantee Terms.</p>
                            </div>
                            <button type="submit" class="full-width btn-medium">SIGNUP</button>
                        </form>
                    </div>
                    <div class="seperator"></div>
                    <p>Already a Travelo member? <a href="assets/#travelo-login" class="goto-login soap-popupbox">Login</a></p>
                </div>
                <div id="travelo-login" class="travelo-login-box travelo-box">
                    <div class="login-social">
                        <a href="assets/#" class="button login-facebook"><i class="soap-icon-facebook"></i>Login with Facebook</a>
                        <a href="assets/#" class="button login-googleplus"><i class="soap-icon-googleplus"></i>Login with Google+</a>
                    </div>
                    <div class="seperator"><label>OR</label></div>
                    <form>
                        <div class="form-group">
                            <input type="text" class="input-text full-width" placeholder="email address">
                        </div>
                        <div class="form-group">
                            <input type="password" class="input-text full-width" placeholder="password">
                        </div>
                        <div class="form-group">
                            <a href="assets/#" class="forgot-password pull-right">Forgot password?</a>
                            <div class="checkbox checkbox-inline">
                                <label>
                                    <input type="checkbox"> Remember me
                                </label>
                            </div>
                        </div>
                    </form>
                    <div class="seperator"></div>
                    <p>Don't have an account? <a href="assets/#travelo-signup" class="goto-signup soap-popupbox">Sign up</a></p>
                </div>
            </header>