<?php

$singleFees = array(
    'CAD' => 290,
    'USD' => 290,
    'EUR' => 200
);
$tourfees = array(
    'CAD' => array(
        'E' => 0,
        'S' => 399,
        'T' => 499
    ),
    'USD' => array(
        'E' => 0,
        'S' => 399,
        'T' => 499
    ),
    'EUR' => array(
        'E' => 0,
        'S' => 399,
        'T' => 499
    )
);
$tipfees = array(
    'CAD' => 60,
    'USD' => 60,
    'EUR' => 40
);
$title = '399起，长江三峡游轮5天超值豪华游';
$cabinDesc = array(
    'S' => '长江三峡游轮（四楼标准间）',
    'T' => '长江三峡游轮（小套间）'
);