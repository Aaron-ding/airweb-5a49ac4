<script type="text/javascript" src="assets/js/typeahead.bundle.js"></script>
<script type="text/javascript" src="https://crud.sinorama.ca/getCountryCode.php?v=201704201"></script>
<script type="text/javascript" src="assets/js/countryCode.js?v=201701254"></script>
<script type="text/javascript" src="assets/js/airport.js"></script>
<script type="text/javascript">

var request = new Request();

tjq(document).ready(function () {
    initCountryInput();

    tjq('#passengercount').change(function() {
        updatePassengers();
    });

    tjq('#submityorder').click(function (e) {
        e.preventDefault();
        tjq('#book-submit-errors').html('');

        if (!tjq('#checkTerms').is(':checked')) {
            tjq('#book-submit-errors').html('<i class="fa fa-times"></i><?php echo lang("请阅读本次活动规则，并选择已阅读。") ?> ');
            return
        }

        passchecked = true;

        tjq.each(tjq('#bookingform').find("input,select"), function () {
            if (tjq(this).is('[required]') && tjq(this).is(':visible')) {

                var val = request.getValFromObj(tjq(this));
                var showerrorobject = tjq(this);
                if (tjq(this).attr('groupid') != undefined) {
                    showerrorobject = tjq('#' + tjq(this).attr('groupid'));
                }

                if (val == undefined || val == "" || val.length == 0) {
                    passchecked = false;
                    request.showRequired(showerrorobject);
                } else {
                    request.removeRequired(showerrorobject);
                }
            }
        });

        if (passchecked) {
            if (!request.isEmail(tjq('#email').val())) {
                passchecked = false;
                request.showRequired(tjq('#email'));
            } else {
                request.removeRequired(tjq('#email'));
            }
        }

        if (!passchecked) {
            tjq('#book-submit-errors').html('<i class="fa fa-times"></i><?php echo lang("输入信息有误，请检查!") ?> ');
            return;
        } else {
            tjq(this).prop('disabled', true);
            tjq(this).hide();
            tjq('#doingbookoing').show();

            var data = {};
            data['firstname'] = tjq('#firstname').val();
            data['middlename'] = tjq('#middlename').val();
            data['lastname'] = tjq('#lastname').val();
            data['email'] = tjq('#email').val();
            data['tel'] = tjq('#tel').val();
            data['address'] = tjq('#address').val();
            data['city'] = tjq('#city').val();
            data['country'] = tjq('#country').attr('data');
            data['postalcode'] = tjq('#postalcode').val();
            data['remarks'] = tjq('#remarks').val();
            data['srccity'] = tjq.trim(tjq('#srccity').val());
            data['departdate'] = tjq.trim(tjq('#departdate').val());
            data['destcity'] = tjq.trim(tjq('#destcity').val());
            data['returndate'] = tjq.trim(tjq('#returndate').val());
            data['passengercount'] = tjq.trim(tjq('#passengercount').val());

            var passengers = [];

            for (var i = 1; i <= data['passengercount']; i++) {
                var passenger = {};
                passenger['firstname'] = tjq.trim(tjq('#firstname_'+i).val());
                passenger['middlename'] = tjq.trim(tjq('#middlename_'+i).val());
                passenger['lastname'] = tjq.trim(tjq('#lastname_'+i).val());
                passenger['gender'] = tjq.trim(tjq('input[name=gender_'+i+']:checked').val());
                passenger['birthday'] = tjq.trim(tjq('#birthday_'+i).val());
                passenger['nationality'] = tjq.trim(tjq('#nationality_'+i).attr('data'));
                passenger['passport'] = tjq.trim(tjq('#passport_'+i).val());
                passenger['passportexpired'] = tjq.trim(tjq('#passportexpired_'+i).val());
                passenger['passportcountry'] = tjq.trim(tjq('#passportcountry_'+i).attr('data'));
                passenger['aeroplan'] = tjq.trim(tjq('#aeroplan_'+i).val());

                passengers.push(passenger);
            }

            data['passengers'] = passengers;

            request.booking = data;

            FT_c_bookFlighttickets(request, successBooking, errorBooking);
        }
    });
    
    tjq('#passengercount').change(function () {
        updateFee();
    });

    updateFee();
});

function updateFee() {
    var currencyShow = tjq('#currencyShow').val();
    var quantity = tjq('#passengercount').val();
    var basefee = tjq('#basefee').val();

    var basefee = parseInt(basefee);
    var taxfee = Math.round(basefee * 0.03 * 100) / 100;
    var totalfee = (parseInt(basefee) + taxfee) * parseInt(quantity);

    tjq('#basefare').text(currencyShow + basefee + '/人');
    tjq('#intercharge').text(currencyShow + taxfee + '/人');
    tjq('#quantity').text(quantity);
    tjq('#totalfare').text(currencyShow + totalfee);
}

function updatePassengers() {
    var passengercount = tjq('#passengercount').val();
    var couponsHtml = '';

    for (var i = 1; i <= passengercount; i++) {
        couponsHtml += '<div class="person-information"><h3>乘客'+i+'</h3>';
        couponsHtml += '<div class="form-group row">';
        couponsHtml += '<div class="col-sm-6 col-md-4">';
        couponsHtml += '<label for="firstname_'+i+'" placehold="*<?php echo lang('名字'); ?>">*<?php echo lang('名字'); ?></label>';
        couponsHtml += '<input class="input-text full-width" id="firstname_'+i+'" placeholder="" required="" type="text" value="" />';
        couponsHtml += '</div>';
        couponsHtml += '<div class="col-sm-6 col-md-4">';
        couponsHtml += '<label for="middlename_'+i+'" placehold="<?php echo lang('中间名'); ?>"><?php echo lang('中间名'); ?></label>';
        couponsHtml += '<input class="input-text full-width" id="middlename_'+i+'" placeholder="不是必填项目" type="text" value="" />';
        couponsHtml += '</div>';
        couponsHtml += '<div class="col-sm-6 col-md-4">';
        couponsHtml += '<label for="lastname_'+i+'" placehold="*<?php echo lang('姓'); ?>">*<?php echo lang('姓'); ?></label>';
        couponsHtml += '<input class="input-text full-width" id="lastname_'+i+'" placeholder="" required="" type="text" value="" />';
        couponsHtml += '</div>';
        couponsHtml += '</div>';

        couponsHtml += '<div class="form-group row">';
        couponsHtml += '<div class="col-sm-6 col-md-4">';
        couponsHtml += '<label for="gender_'+i+'" placehold="*<?php echo lang('乘客性别'); ?>">*<?php echo lang('乘客性别'); ?></label>';
        couponsHtml += '<input class="ng-pristine ng-untouched ng-valid" id="gender_'+i+'" name="gender_'+i+'" ng-model="contact.gender" required="" type="radio" value="M" />　男　　 <input class="ng-pristine ng-untouched ng-valid" name="gender_'+i+'" ng-model="contact.gender" required="" type="radio" value="F" />　女';
        couponsHtml += '</div>';
        couponsHtml += '<div class="col-sm-6 col-md-4">';
        couponsHtml += '<label for="birthday_'+i+'" placehold="*<?php echo lang('出生日期'); ?>">*<?php echo lang('出生日期'); ?></label>';
        couponsHtml += '<div class="datepicker-wrap"><input class="input-text full-width " date-type="date" id="birthday_'+i+'" picker-type="birthday" placeholder="年-月-日" required="" type="text" vid="birthday_'+i+'" /></div>';
        couponsHtml += '</div>';
        couponsHtml += '<div class="col-sm-6 col-md-4">';
        couponsHtml += '<label for="nationality_'+i+'"" placehold="*<?php echo lang('国籍'); ?>">*<?php echo lang('国籍'); ?></label>';
        couponsHtml += '<div id="multiple-datasets" style="position:relative;">';
        couponsHtml += '<a class="typeahead-close" href="javascript:void(0);"><i class="soap-icon-close" style="font-size:24px;color:#ccc;position:absolute;right:5px;top:0;z-index:999"></i></a>';
        couponsHtml += '<input vid="nationality" id="nationality_'+i+'"" type="text" class="input-text full-width" data="" required value="" placeholder="" />';
        couponsHtml += '</div>';
        couponsHtml += '</div>';
        couponsHtml += '<div class="col-sm-6 col-md-4">';
        couponsHtml += '<label for="passport_'+i+'" placehold="*<?php echo lang('护照号码'); ?>">*<?php echo lang('护照号码'); ?></label>';
        couponsHtml += '<input class="input-text full-width" id="passport_'+i+'" placeholder="" required="" type="text" value="" />';
        couponsHtml += '</div>';
        couponsHtml += '<div class="col-sm-6 col-md-4">';
        couponsHtml += '<label for="passportexpired_'+i+'" placehold="*<?php echo lang('护照有效期'); ?>">*<?php echo lang('护照有效期'); ?></label>';
        couponsHtml += '<div class="datepicker-wrap"><input class="input-text full-width " date-type="date" id="passportexpired_'+i+'" min-date=0 max-date=30 picker-type="birthday" placeholder="年-月-日" required="" type="text" vid="passportexpired" /></div>';
        couponsHtml += '</div>';
        couponsHtml += '<div class="col-sm-6 col-md-4">';
        couponsHtml += '<label for="passportcountry_'+i+'"" placehold="*<?php echo lang('护照签发国家'); ?>">*<?php echo lang('护照签发国家'); ?></label>';
        couponsHtml += '<div id="multiple-datasets" style="position:relative;">';
        couponsHtml += '<a class="typeahead-close" href="javascript:void(0);"><i class="soap-icon-close" style="font-size:24px;color:#ccc;position:absolute;right:5px;top:0;z-index:999"></i></a>';
        couponsHtml += '<input vid="passportcountry" id="passportcountry_'+i+'"" type="text" class="input-text full-width" data="" required value="" placeholder="" />';
        couponsHtml += '</div>';
        couponsHtml += '</div>';
        couponsHtml += '<div class="col-sm-6 col-md-4">';
        couponsHtml += '<label for="aeroplan_'+i+'" placehold="*<?php echo lang('Aeroplan号码'); ?>">*<?php echo lang('Aeroplan号码'); ?></label>';
        couponsHtml += '<input class="input-text full-width" id="aeroplan_'+i+'" placeholder="" required="" type="text" value="" />';
        couponsHtml += '</div>';
        couponsHtml += '</div>';
        couponsHtml += '</div>';
    }

    tjq('#passengers').html('');
    tjq('#passengers').html(couponsHtml);

    bindDatepickers();
    initCountryInput();
}

function successBooking(data) {
    if (data[0] != 'error') {
        tjq('#doingbookoing').hide();
        tjq('#book-submit-errors').html('<i class="fa fa-check"></i><?php echo lang("非常感谢您选择华景！您的预定已经成功提交！") ?>');

        var result = data.split('|');
        var orderId = result[0];
        var token = result[1];

        window.location.href = 'index.php?id=flighttickets-order&orderId=' + orderId + '&token=' + token + '&fullsite=yes&change_lang=Chinese&';
    } else {
        tjq('#doingbookoing').hide();
        tjq('#submityorder').prop('disabled', false);
        tjq('#submityorder').show();
        
        tjq('#book-submit-errors').html('<i class="fa fa-times"></i>' + data[1]);
    }
}

function errorBooking(msg) {
    console.log(msg);
}

function bindDatepickers() {

    // datepicker
    tjq('.datepicker-wrap input').each(function() {
        var pickertype=tjq(this).attr('picker-type');
        switch(pickertype){
            case "birthday":
                var maxDate = tjq(this).attr("max-date");
                if (typeof maxDate == "undefined") {
                    maxDate = 0;
                }
                var minDate=tjq(this).attr("min-date");
                if (typeof minDate == "undefined") {
                    minDate = 100;
                }

                var start=new Date();
                start.setFullYear(parseInt(start.getFullYear())+parseInt(maxDate));


                tjq(this).datepicker({
                    showOn: 'both',
                    buttonImage: 'assets/images/icon/blank.png',
                    buttonText: '',
                    buttonImageOnly: false,
                    changeYear: true,
                    yearRange: "-"+minDate+":+"+maxDate,
                    changeMonth: true,
                    hideIfNoPrevNext: true,
                    /*showOtherMonths: true,*/
                    //numberOfMonths: [1, 2],
                    /*showButtonPanel: true,*/
                    dateFormat: "yy-mm-dd",
                    dayNamesMin: dayNamesMin,
                    monthNames: monthNames,
                    monthNamesShort: monthNamesShort,
                    dayNames: dayNames,
                    dayNamesShort: dayNamesShort,
                    prevText:'hello',
                    beforeShow: function(input, inst) {
                        var themeClass = tjq(input).parent().attr("class").replace("datepicker-wrap", "");
                        tjq('#ui-datepicker-div').attr("class", "");
                        tjq('#ui-datepicker-div').addClass("ui-datepicker ui-widget ui-widget-content ui-helper-clearfix ui-corner-all ");
                        tjq('#ui-datepicker-div').addClass(themeClass);

                    },
                    onSelect:function(dateText,inst) {
                        return true;
                    }


                });

                return;
            case "departure":
                initDepartureDate(this);
            default:
                return;
        };

    });
}
</script>