@include('layouts.parts.config')
        <!DOCTYPE html>
<html lang="en">
<head>
    @include('layouts.parts.template_head')
    @yield('extracss')

</head>
<body>

@yield('content')
@include('layouts.parts.oldbrowser')
@include('layouts.parts.footer_script')
@yield('extrascript')





</body>

</html>
