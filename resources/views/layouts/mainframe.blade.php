
@include('layouts.parts.config')
<!DOCTYPE html>
<html lang="en">
<head>
    @include('layouts.parts.template_head')
    @yield('extracss')

</head>
<body class="horizontal-fixed fixed " >
<div class="wrapper">
    <div class="loader-bg">
        <div class="loader-bar">
        </div>
    </div>
    @include('layouts.parts.header')
    @yield('content')

</div>

@include('layouts.parts.oldbrowser')

@include('layouts.parts.footer_script')


@yield('extrascript')





</body>

</html>


