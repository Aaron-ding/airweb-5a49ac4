<div class="col-xl-4">
    <div class="card">
        <div class="card-header" style="padding:10px;padding-bottom:0px;background:#5cb85c">
            <div class="row">
                <div class="col-lg-12 col-xs-12">
                    <h3 class="card-header-text"  style="color:#FFF;font-size: 24px;font-weight: 600;">STEP 2: Commission list </h3>
                </div>
            </div>
        </div>
        <div class="cd-pricing-body">
            <ul class="basic-list" style="padding-left:10px;padding-right: 10px;">
                @php $totalcommission=0; @endphp
                @for($i=0;$i<count($commission);$i++)

                <li>
                    <div class="row">
                        <div class="col-lg-4 col-xs-4">
                            Desc:
                        </div>
                        <div class="col-lg-8 col-xs-8">
                            {{$commission[$i]["desc"]}}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-4 col-xs-4">
                            Currency:
                        </div>
                        <div class="col-lg-8 col-xs-8">
                            {{$commission[$i]["currency"]}}
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-4 col-xs-4">
                            Passenger:
                        </div>
                        <div class="col-lg-8 col-xs-8">
                            {{intval($commission[$i]["count"])}} {{$commission[$i]["passengertype"]}}
                        </div>
                    </div>


                    <div class="row">
                        <div class="col-lg-4 col-xs-4">
                            Base fare:
                        </div>
                        <div class="col-lg-8 col-xs-8">
                            {{$commission[$i]["basefare"]}}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-4 col-xs-4">
                            Total fare:
                        </div>
                        <div class="col-lg-8 col-xs-8">
                            {{$commission[$i]["totalprice"]}}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-4 col-xs-4">
                            Commission:
                        </div>
                        <div class="col-lg-8 col-xs-8">
                            @php
                                $comm=$commission[$i]["jcb"];
                                if($commission[$i]["db"]>$comm) $comm=$commission[$i]["db"];
                                if($commission[$i]["itx"]>$comm) $comm=$commission[$i]["itx"];
                                $totalcommission+=$comm*intval($commission[$i]["count"])+$commission[$i]["incentive"];
                            @endphp
                            {{$comm}} * {{intval($commission[$i]["count"])}} = {{$comm*intval($commission[$i]["count"])}}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-4 col-xs-4">
                            Incentive:
                        </div>
                        <div class="col-lg-8 col-xs-8">
                            {{$commission[$i]["incentive"]}}
                        </div>
                    </div>
                </li>
                @endfor
                <li>
                    <h3 style="    padding-top: 20px;
    padding-bottom: 20px;">Total Commission:${{$totalcommission}}</h3>
                </li>
            </ul>
        </div>
    </div>
</div>
