@php
/**
 * config.php
 *
 * Author: phoenixcoded
 *
 * Configuration file. It contains variables used in the template as well as the primary navigation array from which the navigation is created
 *
 */

/* Template variables */
global $template;
$template = array(
    'title'                     => 'Able Pro Responsive Bootstrap 4 Admin Template by Phoenixcoded',
    'author'                    => 'Phoenixcoded',
    'description'               => 'Phoenixcoded',
    'keywords'                  => ', Responsive, Landing, Bootstrap, App, Template, Mobile, iOS, Android, apple, creative app',
    'active_page'               => basename($_SERVER['PHP_SELF'])
);
global $primary_nav;
if(basename($_SERVER['PHP_SELF']) != 'mega-menu.php') {
    $primary_nav = array(

        array(
            'name' => 'Home',
            'icon' => 'icon-speedometer',
            'url' => 'https://mo.air.sinorama.ca/',
            'sub' => 0
        ),

        array(
            'name' => 'Dashboard',
            'icon' => 'icon-speedometer',
            'url' => '/home',
            'sub' => 0
        ),

        array(
            'name' => 'My Orders',
            'sub-title1' => 'Manage transtractions',
            'icon' => 'icon-user-follow',
            'sub' => array(
                array(
                    'name' => 'Recent orders',
                    'url' => '/order/flightorder'
                ),
                array(
                    'name' => 'History orders',
                    'url' => '/order/flighthistoryorder'
                )

            ),
        ),
        array(
            'name' => 'My Balance',
            //'opt' => '<span class="label label-success menu-caption">New</span>',
            'sub-title1' => 'Charts &amp; Maps',
            'icon' => 'icon-user-follow',
            'sub' => array(
                array(
                    'name' => 'Points',
                    'url' => '/account/pointpage'
                ),
                array(
                    'name' => 'Cash',
                    'url' => '/account/cashpage'
                )

            ),
        ),
        array(
            'name' => 'Agent menu',
            'sub-title1' => 'Pages',
            'icon' => 'icon-user-follow',
            'permission'=>'view_agents',
            'sub' => array(
                array(
                    'name' => 'Become an agent',
                    'url' => '/registeragent'
                ),
                array(
                    'name' => 'Calculate commission',
                    'permission'=>'view_calculate_commission',
                    'url' => '/agent/calculatecommission'
                ),
                array(
                    'name' => 'Issue air ticket',
                    'permission'=>'view_issue_ticket',
                    'url' => '/agent/issueairticekt'
                ),
                array(
                    'name' => 'All agent orders',
                    'permission'=>'view_agents',
                    'url' => '/allagentorders'
                ),
                array(
                    'name' => 'Query contract',
                    'permission'=>'view_flight_contract',
                    'url' => '/contract/search'
                ),
            ),

        ),

        array(
            'name' => 'Administrator',
            //'opt' => '<span class="label label-success menu-caption">New</span>',
            'sub-title1' => 'Users,Roles',
            'icon' => 'icon-user-follow',
            'permission'=>'edit_users',
            'sub' => array(
                array(
                    'name' => 'Users',
                    'permission'=>'edit_users',
                    'url' => '/users'
                ),
                array(
                    'name' => 'Roles',
                    'permission'=>'edit_users',
                    'url' => '/roles'
                ),
                array(
                    'name' => 'Agent application',
                    'permission'=>'edit_users',
                    'url' => '/agent/applicationlist'
                )

            ),
        ),
        array(
            'name' => 'Logout',
            'url' => '/logout',
            'sub' => 0
        )


    );
}
if(basename($_SERVER['PHP_SELF']) == 'mega-menu.php'){
    $primary_nav = array(
        array(
            'name' => 'Dashboard',
            'icon' => 'icon-speedometer',
            'url' => '/home',
            'sub' => 0
        ),

        array(
            'name' => 'My Orders',
            'sub-title1' => 'Manage transtractions',
            'icon' => 'icon-user-follow',
            'sub' => array(
                array(
                    'name' => 'Recent orders',
                    'url' => '/order/flightorder'
                ),
                array(
                    'name' => 'History orders',
                    'url' => '/historyorders'
                )

            ),
        ),
        array(
            'name' => 'My Balance',
            //'opt' => '<span class="label label-success menu-caption">New</span>',
            'sub-title1' => 'Charts &amp; Maps',
            'icon' => 'icon-user-follow',
            'sub' => array(
                array(
                    'name' => 'Points',
                    'url' => '/points'
                ),
                array(
                    'name' => 'Cash',
                    'url' => '/cash'
                ),
                array(
                    'name' => 'Balance roll',
                    'url' => '/balanceroll'
                )

            ),
        ),
        array(
            'name' => 'Agent menu',
            'sub-title1' => 'Pages',
            'icon' => 'icon-user-follow',
            'sub' => array(
                array(
                    'name' => 'Become an agent',
                    'url' => '/registeragent'
                ),
                array(
                    'name' => 'Import order from GDS',
                    'url' => '/importorder'
                ),
                array(
                    'name' => 'Recent orders',
                    'url' => '/recentorders'
                ),
                array(
                    'name' => 'All agent orders',
                    'url' => '/allagentorders'
                ),
                array(
                    'name' => 'Query contract',
                    'url' => '/querycontract'
                ),
            ),
        ),

    );
}


@endphp