<div class="col-xl-4">

    <div class="card b-l-success">

        <div class="card-header" style="padding:10px;vertical-align: middle" >
            <h3 style="font-size: 20px;font-weight: 600;">Pay By {{$payment->type}}</h3>
        </div>
        <div class="card-block">
            <div class="row">

                <div class="col-lg-4 col-xs-4">
                    <b>Currency:</b>
                </div>
                <div class="col-lg-8 col-xs-8">
                    {{$payment->currency}}
                </div>

                <div class="col-lg-4 col-xs-4">
                    <b>Amount:</b>
                </div>
                <div class="col-lg-8 col-xs-8">
                    {{$payment->amt/100}}
                </div>
                <div class="col-lg-4 col-xs-4">
                    <b>Paid at:</b>
                </div>
                <div class="col-lg-8 col-xs-8">
                    {{$payment->createtime}}
                </div>

            </div>

        </div>

        <!-- end of card-block -->


    </div>
</div>

