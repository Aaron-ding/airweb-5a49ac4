



<!-- Required Fremwork -->
<script src="/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

<!-- waves effects.js -->
<script src="/bower_components/Waves/dist/waves.min.js"></script>

<!-- Scrollbar JS-->
<script src="/bower_components/jquery-slimscroll/jquery.slimscroll.js"></script>
<script src="/bower_components/jquery.nicescroll/dist/jquery.nicescroll.min.js"></script>

<!--classic JS-->
<script src="/bower_components/classie/classie.js"></script>

<!-- notification -->
<script src="/assets/plugins/notification/js/bootstrap-growl.min.js"></script>



<!-- custom js -->
<script type="text/javascript" src="/assets/js/main.min.js"></script>
<script type="text/javascript" src="/assets/pages/elements.js"></script>
<script src="/assets/js/menu-horizontal.min.js"></script>




<script>
    $(document).ready(function() {
        @foreach ($errors->all() as $error)
            notify('{{$error}}','top','center','fa fa-comments','danger','','');
        @endforeach
    });
</script>