<div class="col-xl-4">


    <div class="card b-l-primary">

        <div class="card-header" style="padding:10px;vertical-align: middle" >

            <h3 class="card-header-text" style="font-size: 20px;font-weight: 600;"><i class="icon-plane" style="margin-right:15px;"></i>{{$order->from}}<span id="stopaircode"><em id="up">{{$order->triptype}}</em>--------------</span>{{$order->to}}</h3>
            <span class="label label-default f-right m-t-5" style="font-size:18px;">${{$order->totalprice/100}}</span><span class="f-right" style="margin-top:5px;">{{$order->currency}} </span>
        </div>
        <div class="card-block">
            <div class="row">

                <div class="col-lg-4 col-xs-4">
                    <b>Order ID:</b>
                </div>
                <div class="col-lg-8 col-xs-8">
                    {{$order->orderid}}
                </div>

                <div class="col-lg-4 col-xs-4">
                    <b>PNR:</b>
                </div>
                <div class="col-lg-8 col-xs-8">
                    {{$order->PNR}}&nbsp;
                </div>



                <div class="col-lg-4 col-xs-5">
                    <b>Departure at:</b>
                </div>
                <div class="col-lg-8 col-xs-7">
                    {{$order->departuredate}}
                </div>


                <div class="col-lg-4 col-xs-4">
                    <b>Status:</b>
                </div>
                <div class="col-lg-8 col-xs-8">
                    {{$order->ticketstatus." , ". $order->paystatus}}
                </div>


                <div class="col-lg-4 col-xs-4">
                    <b>Passenger(s):</b>
                </div>
                <div class="col-lg-8 col-xs-8">
                    <b>{{$order->adult}} Adult(s)</b>
                    @if ($order->child>0)
                        <b>{{$order->child}} Child(ren)</b>
                    @endif
                    @if ($order->baby>0)
                        <b>{{$order->baby}} Enfat(s)</b>
                    @endif
                </div>



                <div class="col-lg-4 col-xs-4">
                    <b>Create at:</b>
                </div>
                <div class="col-lg-8 col-xs-8">
                    {{$order->createdatetime}}
                </div>



            </div>

        </div>

        <!-- end of card-block -->

        <div class="card-footer" style="padding:10px;">

            <div class="task-list-table f-left">
                <img class="img-renctle " src="https://air.sinorama.ca/assets/images/logo/{{$order->vendor}}.png" style="width:30px;">
            </div>
            <div class="task-board">
                <button type="button" class="btn btn-primary btn-mini  waves-effect waves-light " data-toggle="tooltip" data-placement="top" title="" data-original-title="Make a payment" onclick="dealorder('payit','{{$order->PNR}}','{{$order->orderid}}','{{$order->provider}}');">Pay it </button>
                <button type="button" class="btn btn-info btn-mini  waves-effect waves-light"  data-toggle="tooltip" data-placement="top" title="" data-original-title="Get receipt" onclick="dealorder('invoice','{{$order->PNR}}','{{$order->orderid}}','{{$order->provider}}');">Receipt</button>
                <button type="button" class="btn btn-warning btn-mini  waves-effect waves-light "  data-toggle="tooltip" data-placement="top" title="" data-original-title="Get the internary" onclick="dealorder('detail','{{$order->PNR}}','{{$order->orderid}}','{{$order->provider}}');">Get details</button>

                <!-- end of seconadary -->
            </div>
            <!-- end of pull-right class -->
        </div>
        <!-- end of card-footer -->
    </div>
    <!-- end of card -->

</div>