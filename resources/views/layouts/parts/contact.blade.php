<div class="col-xl-4">

    <div class="card b-l-warning">

        <div class="card-header" style="padding:10px;vertical-align: middle" >
            <h3 class="card-header-text" style="font-size: 20px;font-weight: 600;"><i class="icon-phone" style="margin-right:15px;"></i>Contact information</h3>
        </div>
        <div class="card-block">
            <div class="row">

                <div class="col-lg-4 col-xs-4">
                    <b>Contact:</b>
                </div>
                <div class="col-lg-8 col-xs-8">
                    {{$contact->firstname}} {{$contact->lastname}}
                </div>

                <div class="col-lg-4 col-xs-4">
                    <b>Email:</b>
                </div>
                <div class="col-lg-8 col-xs-8">
                    {{$contact->email}}
                </div>
                <div class="col-lg-4 col-xs-4">
                    <b>Phone:</b>
                </div>
                <div class="col-lg-8 col-xs-8">
                    {{$contact->tel1}}
                    @if($contact->tel2!="")
                        {{$contact->tel2}}
                    @endif
                </div>


                <div class="col-lg-4 col-xs-4">
                    <b>Address:</b>
                </div>
                <div class="col-lg-8 col-xs-8">
                    {{$contact->address . "," . $contact->city . "," . $contact->province . "," . $contact->country . "," . $contact->postcode}}
                </div>
            </div>

        </div>

        <!-- end of card-block -->


    </div>
</div>