@php
    global $template;
@endphp
<title> {{$template['title']}}</title>

<!-- HTML5 Shim and Respond.js IE9 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->

<!-- Meta -->
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta name="description" content="{{$template['description'] }}">
<meta name="keywords" content="{{$template['keywords']}}">
<meta name="author" content="{{$template['author'] }}?>">
<meta name="csrf-token" content="{{ csrf_token() }}">
<!-- Favicon icon -->
<link rel="shortcut icon" href="/assets/images/favicon.png" type="image/x-icon">
<link rel="icon" href="/assets/images/favicon.ico" type="image/x-icon">

<!-- Google font-->
<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">

<!-- iconfont -->
<link rel="stylesheet" type="text/css" href="/assets/icon/icofont/css/icofont.css">

<!-- simple line icon -->
<link rel="stylesheet" type="text/css" href="/assets/icon/simple-line-icons/css/simple-line-icons.css">

<!-- Material Icon -->
<link rel="stylesheet" type="text/css" href="/assets/icon/material-design/css/material-design-iconic-font.min.css">


<!-- Required Fremwork -->
<link rel="stylesheet" type="text/css" href="/bower_components/bootstrap/dist/css/bootstrap.min.css">

<!-- ion icon css -->
<link rel="stylesheet" type="text/css" href="/assets/icon/ion-icon/css/ionicons.min.css">


<!-- Style.css -->
<link rel="stylesheet" type="text/css" href="/assets/css/main.css">

<!-- Responsive.css-->
<link rel="stylesheet" type="text/css" href="/assets/css/responsive.css">

<!--color css-->
<link rel="stylesheet" type="text/css" href="/assets/css/color/color-1.min.css" id="color"/>

<!--color css-->
<link rel="stylesheet" type="text/css" href="/my.css" id="color"/>


<!-- Required Jqurey -->
<script src="/bower_components/jquery/dist/jquery.min.js"></script>
<script src="/bower_components/jquery-ui/jquery-ui.min.js"></script>
<script src="/bower_components/tether/dist/js/tether.min.js"></script>