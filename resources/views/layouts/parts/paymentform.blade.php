

<div class="col-xl-4">
    <div class="card" style="    background-color: transparent;
    box-shadow: none;">
        <a class="btn btn-primary btn-add-task waves-effect waves-light" href="#" data-toggle="modal"
           data-target="#paymentMethod" style="font-size:25px;"><i class="icofont icofont-plus"></i> Add A Payment</a>
    </div>
</div>




<div class="modal fade" id="paymentMethod" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true"
     style="display: none;">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title" id="modalLabel">Pay for {{$order->orderid}}</h4>
            </div>
            <div class="modal-body">
                <h5>Balance due : <span id="balancedue">{{$order->currency}} {{$order->lastamt/100}}</span></h5>


                @isset($pointsbalance)
                    @php
                        $pointspay=null;
                        $accountpay=null;
                        foreach($payments as $pay){
                            if($pay->type=="Points"){
                                $pointspay=$pay;
                            }
                            if($pay->type=="Account Balance"){
                                $accountpay=$pay;
                            }
                        }
                    @endphp
                    <div class="row">
                        <div class="col-lg-12 col-xs-12">
                            @isset($pointspay)
                                <input type="checkbox"  value="yes" checked onclick="return false;">
                            @else
                                <input id="usepoints" type="checkbox" name="usepoints" value="yes">
                            @endisset

                                Use My Sinorama points to pay
                        </div>
                    </div>
                    @isset($pointspay)
                        <div id="showpoints" class="row" style="margin-left:10px">
                            <div id="paypointscontent" class="col-lg-12 col-xs-12">
                                Use <input type="text"  value="{{$pointspay->amt}}" style=" width: 80px;" readonly> points = {{$order->currency}} <span id="equalamout">{{$pointspay->amt/100}}</span>
                            </div>
                        </div>
                    @else
                        <div id="showpoints" class="row" style="margin-left:10px;display:none">
                            <div class="col-lg-12 col-xs-12" id="loadingpointscontent">
                                <div class="preloader3 loader-block" style="height: 0px;margin-top: 20px;">
                                    <div class="circ1 bg-primary"></div>
                                    <div class="circ2 bg-primary"></div>
                                    <div class="circ3 bg-primary"></div>
                                    <div class="circ4 bg-primary"></div>

                                </div>
                            </div>
                            <div id="paypointscontent" class="col-lg-12 col-xs-12">
                                Use <input id="pointsamt" type="text" name="pointsamt" value="-1" style=" width: 80px;"> points = {{$order->currency}} <span id="equalamout">0</span><br>(Total <span id="totalpoints"></span> points , Maximum usage <span id="maxpoints"></span> points)
                            </div>

                        </div>
                    @endisset
                @endisset

                @isset($pointsbalance)



                    <div class="row">
                        <div class="col-lg-12 col-xs-12">
                            @isset($accountpay)
                                <input type="checkbox" checked onclick="return false;">
                            @else
                                <input id="useaccount" type="checkbox" name="useaccount" value="yes">
                            @endisset
                            Use My account balance to pay
                        </div>
                    </div>

                    @isset($accountpay)
                        <div id="showaccount" class="row" style="margin-left:10px;margin-bottom:20px;">
                            Use $<input type="text"  value="{{$accountpay->amt/100}}" style=" width: 80px;" readonly>
                        </div>
                    @else

                        <div id="showaccount" class="row" style="margin-left:10px;margin-bottom: 20px;display:none">
                            <div class="col-lg-12 col-xs-12" id="loadingpayaccount">
                                <div class="preloader3 loader-block" style="height: 0px;margin-top: 20px;">
                                    <div class="circ1 bg-primary"></div>
                                    <div class="circ2 bg-primary"></div>
                                    <div class="circ3 bg-primary"></div>
                                    <div class="circ4 bg-primary"></div>

                                </div>
                            </div>
                            <div id="payaccount" class="col-lg-12 col-xs-12">
                                Use $<input type="text" id="cashamt" name="cashamt" value="-1" style=" width: 80px;">  (Total balance $<span id="cashtotal"></span> )
                            </div>

                        </div>


                     @endisset


                @endisset


            <!--<input class="form-control save_task_todo" placeholder="Add todo">-->
                <div class="row" align="center">
                    <div id="loadinggif" class="loader animation-start" style="display:none;left:50px!important;">
                        <span class="circle delay-1 size-2"></span>
                        <span class="circle delay-2 size-4"></span>
                        <span class="circle delay-3 size-6"></span>
                        <span class="circle delay-4 size-7"></span>
                        <span class="circle delay-5 size-7"></span>
                        <span class="circle delay-6 size-6"></span>
                        <span class="circle delay-7 size-4"></span>
                        <span class="circle delay-8 size-2"></span>
                    </div>
                    <div id="countdownfresh" style="display:none;">This page will auto refresh after <span id="countdownsec" style="font-size: 20px;font-weight: 400;color:red;">5</span> seconds.....</div>
                </div>


                <div class="form-radio" id="choosemethod" style="margin-top: 10px;">
                        <div id="divpaybycreditcard" class="radio"
                                @isset($accountpay)
                                    style="display:none;"
                                @endisset>
                            <label>
                                <input type="radio" name="paymethod" value="paybycreditcard"><i class="helper"></i>Pay by Credit Card<br><i class="icofont icofont-visa" style="font-size:40px;margin-right:10px;"></i><i class="icofont icofont-mastercard" style="font-size:40px;margin-right:10px;"></i><i class="icofont icofont-american-express" style="font-size:40px;margin-right:10px;"></i>
                            </label>
                        </div>
                        <div class="radio">
                            <label>
                                <input type="radio" name="paymethod" value="paybycash"><i class="helper"></i>Pay by Cash / Debit card ( Pay in store only )<br><i class="zmdi zmdi-store" style="font-size:30px;margin-right: 10px"></i>
                            </label>
                        </div>
                        <div class="radio">
                            <label>
                                <input type="radio" name="paymethod" value="paybydebitcard"><i class="helper"></i>Pay by e-transfer( INTERAC only )<br>
                                <image style="    width: 105px;height: 35px;" src="/assets/images/interac-etransfer.png"></image>
                            </label>
                        </div>
                        <div class="radio">
                            <label>
                                <input type="radio" name="paymethod" value="paybystripe"><i class="helper"></i>Pay by Stripe ( 3% extra fee )<br><i class="icofont icofont-visa" style="font-size:40px;margin-right:10px;"></i><i class="icofont icofont-mastercard" style="font-size:40px;margin-right:10px;"></i><i class="icofont icofont-american-express" style="font-size:40px;margin-right:10px;"></i>
                            </label>
                        </div>
                </div>



                <div class="row paypage" id="inputcreditcard" align="center" style="display: none">
                    <div class="credit-card-input no-js" id="skeuocard">
                        <p class="no-support-warning">
                            Either you have Javascript disabled, or you're using an unsupported browser, amigo! That's why you're seeing this old-school credit card input form instead of a fancy new Skeuocard. On the other hand, at least you know it gracefully degrades...
                        </p>
                        <label for="cc_type">Card Type</label>
                        <select name="cc_type">
                            <option value="">...</option>
                            <option value="visa">Visa</option>
                            <option value="discover">Discover</option>
                            <option value="mastercard">MasterCard</option>
                            <option value="maestro">Maestro</option>
                            <option value="jcb">JCB</option>
                            <option value="unionpay">China UnionPay</option>
                            <option value="amex">American Express</option>
                            <option value="dinersclubintl">Diners Club</option>
                        </select>
                        <label for="cc_number">Card Number</label>
                        <input type="text" name="cc_number" id="cc_number" placeholder="XXXX XXXX XXXX XXXX" maxlength="19" size="19">
                        <label for="cc_exp_month">Expiration Month</label>
                        <input type="text" name="cc_exp_month" id="cc_exp_month" placeholder="00">
                        <label for="cc_exp_year">Expiration Year</label>
                        <input type="text" name="cc_exp_year" id="cc_exp_year" placeholder="00">
                        <label for="cc_name">Cardholder's Name</label>
                        <input type="text" name="cc_name" id="cc_name" placeholder="John Doe">
                        <label for="cc_cvc">Card Validation Code</label>
                        <input type="text" name="cc_cvc" id="cc_cvc" placeholder="123" maxlength="3" size="3">
                    </div>
                </div>


                <div class="row paypage" id="paybycash" style="display: none">
                    <div class="col-lg-12 col-xs-12">
                        Click<a href="">here</a> to download the booking confirmation
                        <br>OR<br>
                        Click<a href="">here</a> to send you a booking confirmation email
                        <h6 style="margin-top: 30px;"> Go to one of our office with your booking confirmation</h6>
                        <ul>
                            <li style="margin-top: 30px;">
                                <h6>Montreal office</h6>
                                <p>Address:998 Boulevard Saint-Laurent,suite 518 Montréal, Québec H2Z 9Y9</p>
                                <p>Scheduel:Mon. - Fri. 10:00 - 18:00 / Sat. - Sun. 10:00 - 17:00</p>
                                <p>Telephone:1-866-255-2188</p>
                            </li>
                            <li style="margin-top: 30px;">
                                <h6>Toronto office</h6>
                                <p>Address:7077 Kennedy Road, #201, Markham ON L3R 0N8</p>
                                <p>Scheduel:Mon. - Fri. 10:00 - 18:00 / Sat. - Sun. 10:00 - 17:00</p>
                                <p>Telephone:1-888-577-9918</p>
                            </li>
                            <li style="margin-top: 30px;">
                                <h6>Vancover office</h6>
                                <p>Address: 8091 Westminster Hwy, #200 Richmond BC V6X 1A7</p>
                                <p>Scheduel:Mon. - Fri. 10:00 - 18:00 / Sat. - Sun. 10:00 - 17:00</p>
                                <p>Telephone:1-855-666-8088</p>
                            </li>
                        </ul>


                    </div>
                </div>




                <div class="row paypage" id="paybydebitcard" style="display: none">
                    <div class="col-lg-12 col-xs-12">
                        <ul>
                            <li style="margin-top:20px;">
                                1、Send your money to this email: <span style="color:red;font-weight: 400">etransfer@sinoramagroup.com</span>
                            </li>
                            <li style="margin-top:20px;">
                                2、Input the order number:<span style="color:red;font-weight: 400">{{$order->orderid}}</span> in the memo.
                            </li>
                            <li style="margin-top:20px;">
                                3、Send a notice email to <span style="color:red;font-weight: 400">service@sinoramagroup.com</span>
                            </li>
                            <li style="margin-top:20px;">
                                4、Give us the password when we contact you.
                            </li>
                        </ul>




                    </div>

                </div>




            </div>
            <div class="modal-footer">
                <button type="button" id="afterchoosemethod" class="save_btn btn btn-primary" onclick="return gotopaygate();"><span class="m-r-10">Next</span><i class="icofont icofont-bubble-right"></i></button>
                <button type="button" id="gobacktochoosemethod" class="btn btn-default" style="display:none" onclick="return gobacktochoosemethod();"><i class="icofont icofont-bubble-left"></i><span class="m-l-10">Backward</span></button>
                <button type="button" id="payit" class="save_btn btn btn-primary" style="display:none"  onclick="return payit();">Pay it</button>

                <button type="button" id="done" class="btn btn-primary close_btn" style="display:none"  >Done</button>
                <button type="button" class="btn btn-default close_btn" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<script src="/bower_components/skeuocard/javascripts/skeuocard.min.js"></script>
<script src="/bower_components/skeuocard/javascripts/vendor/cssua.min.js"></script>
<script src="https://dev.search.air.sinorama.ca:9999/air_api.js?v=201703303"></script>
<script type="text/javascript" src="https://js.stripe.com/v2/"></script>
<script>
    var stripeErrors = {
        'PNR_Save': "An error occurred while processing the card.",
        'PNR_Retrieve': "An error occurred while processing the card.",
        'FOP_CreateFormOfPayment': "An error occurred while obtaining credit card approval.",
        'EndTransactionLLSRQ': "An error occurred while processing the card.",
        'TravelItineraryReadRQ': "An error occurred while processing the card.",
        'CreditVerificationRQ': "An error occurred while obtaining credit card approval.",
        'AddRemarkLLSRQ_Creditcard': "An error occurred while processing the card.",
        'invalid_coupon': "The coupon is not valid.",
        'coupon_processing_error': "An error occurred while processing the coupon.",
        'invalid_number': "The card number is not a valid credit card number.",
        'invalid_expiry_month': "The card's expiration month is invalid.",
        'invalid_expiry_year': "The card's expiration year is invalid.",
        'invalid_cvc': "The card's security code is invalid.",
        'invalid_swipe_data': "The card's swipe data is invalid.",
        'incorrect_number': "The card number is incorrect.",
        'expired_card': "The card has expired.",
        'incorrect_cvc': "The card's security code is incorrect.",
        'incorrect_zip': "The card's zip code failed validation.",
        'card_declined': "The card was declined.",
        'missing': "There is no card on a customer that is being charged.",
        'processing_error': "An error occurred while processing the card."
    };

        function gotopaygate(){


            //change points and account balance firstly
            if ($("input[name='paymethod']").is(':checked') && choosemethod!=undefined && choosemethod!=""){
                var chargepoints=0;
                var chargeaccount=0;

                if($("#usepoints").length>0 && $("#usepoints").prop("checked") == true){
                    if($('#pointsamt').length>0 && $('#pointsamt').val()!="" && parseInt($('#pointsamt').val()) >0){

                        chargepoints=$('#pointsamt').val();
                    }
                }
                if($("#useaccount").length>0 && $("#useaccount").prop("checked") == true){
                    if($('#cashamt').length>0 && $('#cashamt').val()!="" && parseFloat($('#cashamt').val()) >0){
                        chargeaccount=$('#cashamt').val();
                    }
                }
                $('#loadinggif').show();
                $.ajaxSetup(
                    {
                        headers:
                            {
                                'X-CSRF-Token': $('input[name="_token"]').val()
                            }
                });

                $.post( "{{route("changeaccountpoints")}}", { PNR: "{{$order->PNR}}",orderid:"{{$order->orderid}}","_token":'{{ csrf_token() }}',"pointsamt":chargepoints,"cashamt":chargeaccount}, function( data ) {
                    $('#loadinggif').hide();
                    if(data[0]=="success"){
                        if(data[1]!=undefined){
                            $('#balancedue').text(data[1]);
                        }
                        if($("#usepoints").length>0 && $("#usepoints").prop("checked") == true) {
                            $('#usepoints').attr("id", "usepointsreadonly");
                            $('#usepointsreadonly').click(function () {
                                return false;
                            });
                        }else{
                            $("#usepoints").hide();
                        }
                        if($("#useaccount").length>0 && $("#useaccount").prop("checked") == true){
                            $('#useaccount').attr("id", "useaccountreadonly");
                            $('#useaccountreadonly').click(function () {
                                return false;
                            });
                        }else{
                            $("#useaccount").hide();
                        }
                        var choosemethod=$("input[name='paymethod']:checked").val();

                        $('#choosemethod').hide();
                        $('#'+choosemethod).show();
                        $('#gobacktochoosemethod').show();
                        $('#afterchoosemethod').hide();
                        if (choosemethod=="paybycreditcard" ||  choosemethod=="paybystripe"){
                            $('#payit').show();
                            $('#inputcreditcard').show();
                            var card=new Skeuocard($("#skeuocard"));
                        }
                        if (choosemethod=="paybycash" ||  choosemethod=="paybydebitcard"){
                            $('#done').show();
                        }


                    }else{
                        $.each(data,function(item){
                            notify(item,'top','center','fa fa-comments','danger','','');
                        });
                    }


                }, "json");
            }else{
                notify('Please choose a payment method!','top','center','fa fa-comments','danger','','');
            }








        }

        function gobacktochoosemethod(){
            $('.paypage').hide();
            $('#choosemethod').show();
            $('#gobacktochoosemethod').hide();
            $('#payit').hide();
            $('#done').hide();
            $('#inputcreditcard').hide();
            $('#afterchoosemethod').show();
            $('#loadinggif').hide();
            $("#usepoints").show();
            $("#useaccount").show();

        }

        function vlidateCoupon(f) {

            if ($('#couoncode').val()!=undefined && $('#couoncode').val().length > 0) {
                var couoncode = $('#couoncode').val();
                FT_c_validateCoupon(couoncode, function(result) {
                    if (result[0] == 'success') {
                        $('#coupon_amt').html(result[1].currency + ' ' + result[1].amt);
                        $('#coupon_info_success').show();
                        if (typeof f == "function") {
                            f();;
                        }
                    } else {
                        $('#coupon_info').show();
                        if (typeof f == "function") {
                            $('#submit-errors').html('<i class="fa fa-times"></i> Your cash Coupon is not valid');
                            $('#submitorder').show();
                            $('#doingpayment').hide();
                        }
                    }
                });
            } else {
                if (f == undefined) {
                    notify('Please enter the cash coupon code','top','center','fa fa-comments','danger','','');
                } else {
                    f();
                }
            }

        }
        function getCreditcardType(ctype) {
            // visa

            if (ctype=="visa")
                return "VI";

            // Mastercard
            if (ctype=="mastercard")
                return "CA";

            // AMEX
            if (ctype=="amex")
                return "AX";

            return "";
        }

        function payit(){

            Stripe.setPublishableKey('pk_test_XEL2ac8zWkzRTSF9NQPfqJ49');

            if (!Stripe.card.validateCardNumber($('[name="cc_number"]').val())) {
                notify(stripeErrors['invalid_number'],'top','center','fa fa-comments','danger','','');
                return;
            }

            var creditcardType = getCreditcardType($('[name="cc_type"]').val());
            if (creditcardType == '') {
                notify(stripeErrors['invalid_number'],'top','center','fa fa-comments','danger','','');
                return;
            }

            if (!Stripe.card.validateExpiry($('[name="cc_exp_month"]').val(), $('[name="cc_exp_year"]').val())) {
                notify('Expiration date is not valid.','top','center','fa fa-comments','danger','','');
                return;
            }

            if (!Stripe.card.validateCVC($('[name="cc_cvc"]').val())) {
                notify(stripeErrors['invalid_cvc'],'top','center','fa fa-comments','danger','','');
                return;
            }
            $('#loadinggif').show();
            $('#inputcreditcard').hide();
            $('#payit').hide();
            $('#gobacktochoosemethod').hide();

            vlidateCoupon(function () {
                var msg=jQuery.parseJSON('@php $order->status=htmlentities($order->status); echo json_encode($order); @endphp');
                msg["couoncode"]="";
                msg["chargeamt"]='{{$order->lastamt}}';
                msg['booking']={};


                var choosemethod=$("input[name='paymethod']:checked").val();
                var cardNumber=$('[name="cc_number"]').val();
                msg['booking']["card"] = {
                    'name': $('[name="cc_name"]').val().trim(),
                    'number': cardNumber,
                    'last4': cardNumber.substring(cardNumber.length-4),
                    'exp_year': $('[name="cc_exp_year"]').val().trim(),
                    'exp_month': $('[name="cc_exp_month"]').val().trim(),
                    'brand': creditcardType,
                    'seccode': $('[name="cc_cvc"]').val().trim()
                };
                if (choosemethod=="paybycreditcard" ){
                    msg['creditcardProviders'] = 'airlineSystem';
                } else if (choosemethod=="paybystripe") {
                    msg['creditcardProviders'] = 'stripe';
                }
                FT_c_charge_ticket(msg, function (chargers) {
                    function countdownfreshpage(){
                        $('#loadinggif').hide();
                        $('#countdownfresh').show();
                        var time = 5

                        setInterval( function() {

                            time--;
                            if(time>=0){
                                $('#countdownsec').html(time);
                            }
                            if (time === 0) {

                                location.reload();
                            }


                        }, 1000 );
                    }

                    if (chargers != undefined && chargers[0] != 'error') {
                        //pay success
                        notify("Congratulations!<br>You have paid success!",'top','center','fa fa-comments','success','','');
                        countdownfreshpage();
                    } else {
                        //pay error

                        err="System error!<br>Plase contact us!";
                        if (stripeErrors[chargers[1]]!="" && stripeErrors[chargers[1]]!=undefined){
                            err=stripeErrors[chargers[1]];
                        }
                        notify(err,'top','center','fa fa-comments','danger','','');
                        countdownfreshpage();

                    }
                }, function (value) {
                    //tjq('#payment-progress').children('div').attr('aria-valuenow', value).css('width', value + '%').text(value + '%');
                    console.log(value);
                });

                return false;

            });

        }
    var maximumusepoints=0;
    var maximumcash=0;

    $( document ).ready(function() {
        $("#usepoints").change(function () {
            if (this.checked) {
                $('#showpoints').show();
                $('#loadingpointscontent').show();
                $('#paypointscontent').hide();

                $.ajaxSetup(
                    {
                        headers:
                            {
                                'X-CSRF-Token': $('input[name="_token"]').val()
                            }
                    });
                $.post( "{{route("querypaypointsinfo")}}", { PNR: "{{$order->PNR}}",orderid:"{{$order->orderid}}","_token":'{{ csrf_token() }}'}, function( data ) {
                    $('#totalpoints').text(data.total);
                    $('#maxpoints').text(data.maximum);
                    $('#loadingpointscontent').hide();
                    $('#paypointscontent').show();
                    maximumusepoints=parseInt(data.maximum);
                    if(data.total<data.maximum) maximumusepoints=data.total;
                    $('#equalamout').text(maximumusepoints/100);
                    $('[name=pointsamt]').val(data.maximum);
                }, "json");
            } else {
                $('#showpoints').hide();
            }
        });

        $("#useaccount").change(function () {
            if (this.checked) {
                $('#showaccount').show();
                $('#loadingpayaccount').show();
                $('#payaccount').hide();

                $.ajaxSetup(
                    {
                        headers:
                            {
                                'X-CSRF-Token': $('input[name="_token"]').val()
                            }
                    });
                $.post( "{{route("querypayaccountinfo")}}", { PNR: "{{$order->PNR}}",orderid:"{{$order->orderid}}","_token":'{{ csrf_token() }}'}, function( data ) {
                    $('#totalaccount').text(data.total);
                    $('#loadingpayaccount').hide();
                    $('#payaccount').show();
                        maximumcash=data.maximum/100;
                    $('[name=cashamt]').val(maximumcash);
                    $('#cashtotal').text(data.total/100);
                }, "json");

                //disable credit payment. because at this case, wo can only issue ticket by cash
                $("input[type=radio][value=paybycreditcard]").prop('checked', false);
                $("#divpaybycreditcard").hide();
            } else {
                $('#showaccount').hide();
                $("#divpaybycreditcard").show();
            }
        });

        $('input[name=pointsamt]').on('input',function(e){
            var currentamt=0;
            if($(this).val()!=undefined && $(this).val()!=""){
                currentamt=parseInt($(this).val());
            }
            if(currentamt>maximumusepoints) currentamt=maximumusepoints;
            $(this).val(currentamt);
            $('#equalamout').text(currentamt/100);
        });

        $('input[name=pointsamt]').keydown(function (e) {
            // Allow: backspace, delete, tab, escape, enter and .
            if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110]) !== -1 ||
                // Allow: Ctrl+A, Command+A
                (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||
                // Allow: home, end, left, right, down, up
                (e.keyCode >= 35 && e.keyCode <= 40)) {
                // let it happen, don't do anything
                return;
            }
            // Ensure that it is a number and stop the keypress
            if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                e.preventDefault();
            }
        });


        $('input[name=cashamt]').on('input',function(e){
            var currentamt=0.00;
            if($(this).val()!=undefined && $(this).val()!=""){
                currentamt=parseFloat($(this).val());
            }
            if(currentamt>maximumcash){
                $(this).val(maximumcash);
            }


        });

        $('input[name=cashamt]').keydown(function (e) {
            // Allow: backspace, delete, tab, escape, enter and .
            if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110]) !== -1 ||
                // Allow: Ctrl+A, Command+A
                (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||
                // Allow: home, end, left, right, down, up
                (e.keyCode >= 35 && e.keyCode <= 40)) {
                // let it happen, don't do anything
                return;
            }
            var val=$(this).val();
            var dotidx=val.indexOf('.');
            // Ensure that it is a number and stop the keypress
            if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105) && (e.keyCode!=190)) {
                e.preventDefault();
            }

            if(e.which == 190 &&  dotidx >=0){
                e.preventDefault();
            }
            if(e.which == 190 &&  val.length==0){
                e.preventDefault();
            }
            if(dotidx>=0 && (val.length-dotidx)>2){
                e.preventDefault();
            }
        });




    });


</script>