<div class="col-xl-4">

    <div class="card b-l-info">

        <div class="card-header" style="padding:10px;vertical-align: middle" >
            <h3 class="card-header-text" style="font-size: 20px;font-weight: 600;"><i class="icon-people" style="margin-right:15px;"></i>Passenger @isset($i){{$i}}@endisset @isset($passenger->index){{$passenger->index}}@endisset--
                @if($passenger->type=='A')
                    {{"Adult"}}
                @endif
                @if($passenger->type=='C')
                    {{"Child"}}
                @endif
                @if($passenger->type=='I')
                    {{"Infant"}}
                @endif
            </h3>
        </div>
        <div class="card-block">
            <div class="row">

                <div class="col-lg-4 col-xs-4">
                    <b>First name:</b>
                </div>
                <div class="col-lg-8 col-xs-8">
                    {{$passenger->firstname}}
                </div>

                <div class="col-lg-4 col-xs-5">
                    <b>Middle name:</b>
                </div>
                <div class="col-lg-8 col-xs-7">
                    {{$passenger->middle}}&nbsp;
                </div>



                <div class="col-lg-4 col-xs-4">
                    <b>Last name:</b>
                </div>
                <div class="col-lg-8 col-xs-8">
                    {{$passenger->lastname}}
                </div>


                <div class="col-lg-4 col-xs-4">
                    <b>Gender:</b>
                </div>
                <div class="col-lg-8 col-xs-8">
                    {{$passenger->sex}}
                </div>


                <div class="col-lg-4 col-xs-4">
                    <b>Citizenship:</b>
                </div>
                <div class="col-lg-8 col-xs-8">
                    {{$passenger->nationality}}
                </div>


                <div class="col-lg-4 col-xs-4">
                    <b>Birthday:</b>
                </div>
                <div class="col-lg-8 col-xs-8">
                    {{$passenger->birthday}}
                </div>


                <div class="col-lg-4 col-xs-4">
                    <b>{{ucfirst($passenger->ctype)}}:</b>
                </div>
                <div class="col-lg-8 col-xs-8">
                    {{$passenger->cno}}
                </div>


                <div class="col-lg-4 col-xs-4">
                    <b>Expried:</b>
                </div>
                <div class="col-lg-8 col-xs-8">
                    {{$passenger->cexpired}}
                </div>


                <div class="col-lg-4 col-xs-4">
                    <b>Issue country:</b>
                </div>
                <div class="col-lg-8 col-xs-8">
                    {{$passenger->ccountry}}
                </div>






            </div>

        </div>

        <!-- end of card-block -->


    </div>









</div>








