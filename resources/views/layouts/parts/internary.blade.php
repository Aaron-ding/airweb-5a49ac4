<div class="col-xl-4">

    <div class="card b-l-danger">

        <div class="card-header" style="padding:10px;vertical-align: middle" >
            <h3 class="card-header-text" style="font-size: 20px;font-weight: 600;"><span class="badge" style="
    width: 40px;
    height: 40px;
    font-size: 20px;
    padding-top: 10px;
    background: #ff5252;
    border-radius: 50%;
    color: white;
}
">@isset($i) {{$i}} @endisset  @isset($internary["index"]){{$internary["index"]}}@endisset</span><img class="img-renctle " src="https://air.sinorama.ca/assets/images/logo/{{$internary["Airline"]}}.png" style="width:40px;margin-right:10px;margin-left:10px;">{{$internary["From"]}}<span id="stopaircode"><em id="up">{{$internary["Airline"]. $internary["Flightnumber"]}}</em>--------------</span>{{$internary["To"]}}</h3>
        </div>
        <div class="card-block">
            <div class="row">

                <div class="col-lg-4 col-xs-4">
                    <b>Status:</b>
                </div>
                <div class="col-lg-8 col-xs-8">
                    @isset($internary["status"])
                        {{$internary["status"]}}
                    @else
                        <span style="float:left">updating</span>
                        <div class="preloader3 loader-block" style="height: 0px;margin-top: 20px;">
                            <div class="circ1 bg-primary"></div>
                            <div class="circ2 bg-primary"></div>
                            <div class="circ3 bg-primary"></div>
                            <div class="circ4 bg-primary"></div>

                        </div>

                    @endisset
                </div>

                <div class="col-lg-4 col-xs-4">
                    <b>Departure at:</b>
                </div>
                <div class="col-lg-8 col-xs-8">
                    {{$internary["DepartureDateTime"]}}
                    @if(isset($internary["From_terminal"]) && $internary["From_terminal"]!="N/A")
                        <p>{{$internary["From_terminal"]}}</p>
                    @endif
                </div>



                <div class="col-lg-4 col-xs-4">
                    <b>Arrive at:</b>
                </div>
                <div class="col-lg-8 col-xs-8">
                    {{$internary["ArrivalDateTime"]}}
                    @if(isset($internary["To_terminal"]) && $internary["To_terminal"]!="N/A")
                        <p>{{$internary["To_terminal"]}}</p>
                    @endif
                </div>


                <div class="col-lg-4 col-xs-4">
                    <b>Total time:</b>
                </div>
                <div class="col-lg-8 col-xs-8">
                    {{$internary["ElapsedTimeTotalHours"]}}Hour{{$internary["ElapsedTimeTotalHoursLeftMins"]}}Mins
                </div>

                <div class="col-lg-4 col-xs-4">
                    <b>Cabin class:</b>
                </div>
                <div class="col-lg-8 col-xs-8">
                    {{$internary["MarketingCabin"]}}
                </div>
            </div>

        </div>

        <!-- end of card-block -->


    </div>
</div>
