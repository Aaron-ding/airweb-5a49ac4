<!-- Navbar-->
@php global $template;global $primary_nav; @endphp
<header class="main-header-top hidden-print">
    <a href="https://mo.air.sinoramgroup.com/" class="logo"><img class="img-fluid able-logo" src="/assets/images/logo.png" alt="Theme-logo"></a>
    <nav class="navbar navbar-static-top">
        <!-- Sidebar toggle button-->
        <a href="#!"  class="hidden-md-up" style="    position: absolute;
    top: 14px;
    color: #1b8bf9;
    font-size: 14px;
    left: 20px;
    background: white;
    padding: 1px;
    padding-left: 3px;
    padding-right: 3px;
    font-weight: 600;">EN</a>
        <a href="#!"  class="hidden-md-up" style="    position: absolute;
    font-size: 14px;
    color: white;
    top: 15px;
    left: 60px;">CN</a>
        <a href="#!"  class="hidden-md-up" style="    position: absolute;
    font-size: 14px;
    color: white;
    top: 15px;
    left: 90px;">FR</a>
        <!-- Navbar Right Menu-->
        <div class="navbar-custom-menu">
            <ul class="top-nav">
                <!--Notification Menu-->
                @php
                    $headimg="undefined.png";
                    if(isset(Auth::user()->sex) && Auth::user()->sex!==""){
                        $headimg=Auth::user()->sex. ".jpeg";
                    }
                @endphp


                <!--
                <li class="dropdown pc-rheader-submenu message-notification search-toggle">
                    <a href="#!" id="morphsearch-search" class="drop icon-circle txt-white">
                        <i class="icofont icofont-search-alt-1"></i>
                    </a>
                </li>
                <li class="dropdown notification-menu">
                    <a href="#!" data-toggle="dropdown" aria-expanded="false" class="dropdown-toggle">
                        <i class="icon-bell"></i>
                        <span class="badge badge-danger header-badge">9</span>
                    </a>
                    <ul class="dropdown-menu">
                        <li class="not-head">You have <b class="text-primary">4</b> new notifications.</li>
                        <li class="bell-notification">
                            <a href="javascript:;" class="media">
                    <span class="media-left media-icon">
                    <img class="img-circle" src="/assets/images/avatar-1.png" alt="User Image">
                  </span>
                                <div class="media-body"><span class="block">Lisa sent you a mail</span><span class="text-muted block-time">2min ago</span></div></a>
                        </li>
                        <li class="bell-notification">
                            <a href="javascript:;" class="media">
                    <span class="media-left media-icon">
                    <img class="img-circle" src="/assets/images/avatar-2.png" alt="User Image">
                  </span>
                                <div class="media-body"><span class="block">Server Not Working</span><span class="text-muted block-time">20min ago</span></div></a>
                        </li>
                        <li class="bell-notification">
                            <a href="javascript:;" class="media"><span class="media-left media-icon">
                    <img class="img-circle" src="/assets/images/avatar-3.png" alt="User Image">
                  </span>
                                <div class="media-body"><span class="block">Transaction xyz complete</span><span class="text-muted block-time">3 hours ago</span></div></a>
                        </li>
                        <li class="not-footer">
                            <a href="#!">See all notifications.</a>
                        </li>
                    </ul>
                </li>
                -->

                <!-- window screen -->
                <li class="dropdown hidden-md-down">

                    <a href="#!" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" class="dropdown-toggle drop icon-circle drop-image">
                        <span>English<i class=" icofont icofont-simple-down"></i></span>

                    </a>
                    <ul class="dropdown-menu settings-menu">

                        <li><a href="#!"><i class="icon-user"></i> French</a></li>

                        <li class="p-0">
                            <div class="dropdown-divider m-0"></div>
                        </li>
                        <li><a href="#!"><i class="icon-logout"></i>Chinese</a></li>

                    </ul>
                </li>

                <li class="pc-rheader-submenu hidden-md-down">
                    <a href="#!" class="drop icon-circle" onclick="javascript:toggleFullScreen()">
                        <i class="icon-size-fullscreen"></i>
                    </a>

                </li>
                <!-- User Menu-->
                <li class="dropdown hidden-md-down">

                    <a href="#!" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" class="dropdown-toggle drop icon-circle drop-image">
                        <span><img class="img-circle " src="/assets/images/social/{{$headimg}}" style="width:40px;" alt="User Image"></span>
                        <span>{{ucfirst(Auth::user()->firstname)}} {{ucfirst(Auth::user()->lastname)}} <i class=" icofont icofont-simple-down"></i></span>

                    </a>
                    <ul class="dropdown-menu settings-menu">

                        <li><a href="{{route("profile")}}"><i class="icon-user"></i> Profile</a></li>

                        <li class="p-0">
                            <div class="dropdown-divider m-0"></div>
                        </li>
                        <li><a href="/logout"><i class="icon-logout"></i> Logout</a></li>

                    </ul>
                </li>
                <li>

                </li>
                <a href="#!" data-toggle="offcanvas" class="sidebar-toggle hidden-md-up"></a>
            </ul>


            <!-- search end -->
        </div>
    </nav>
</header>
<!-- Side-Nav-->
<aside class="main-sidebar hidden-print ">
    <section class="sidebar <?php if($template['active_page']== 'form-elements-materialize.php' || $template['active_page']== 'form-elements-advance.php' ){ echo "m-b-0" ;}?>" id="sidebar-scroll">
        <div class="user-panel">
            <div class="f-left image"><img src="/assets/images/social/{{$headimg}}"  class="img-circle"></div>
            <div class="f-left info">
                <p>{{ucfirst(Auth::user()->firstname)}} {{ucfirst(Auth::user()->lastname)}}</p>
                @php
                $rolename=Auth::user()->roles->pluck('name');
                if(count($rolename)>0){
                    echo '<p class="designation">'+$rolename[0]+'<i class="icofont icofont-caret-down m-l-5"></i></p>';
                }
                @endphp

            </div>
        </div>
        <!-- sidebar profile Menu-->
        <ul class="nav sidebar-menu extra-profile-list">
            <li>
                <a class="waves-effect waves-dark" href="{{route("profile")}}">
                    <i class="icon-user"></i>
                    <span class="menu-text">View Profile</span>
                    <span class="selected"></span>
                </a>
            </li>

            <li>
                <a class="waves-effect waves-dark" href="/logout">
                    <i class="icon-logout"></i>
                    <span class="menu-text">Logout</span>
                    <span class="selected"></span>
                </a>
            </li>
        </ul>
        <!-- Sidebar Menu-->
        <?php
        

        if ($primary_nav) { ?>
        <ul class="sidebar-menu">
            <?php foreach( $primary_nav as $key => $link ) {
            $link_class = '';
            $li_active  = '';
            $menu_link  = '';

            if(isset($link["permission"])){
                if(!Auth::user()->hasPermissionTo($link["permission"])){
                    continue;
                }
            }


            // Get 1st level link's vital info
            $url        = (isset($link['url']) && $link['url']) ? $link['url'] : '#!';
            $active     = (isset($link['url']) && ($template['active_page'] == $link['url'])) ? ' active ' : '';
            $icon       = (isset($link['icon']) && $link['icon']) ? '<i class="' . $link['icon'] . '"></i>' : '';
            // Check if the link has a submenu

            if (isset($link['sub']) && $link['sub']) {
                // Since it has a submenu, we need to check if we have to add the class active
                // to its parent li element (only if a 2nd or 3rd level link is active)
                foreach ($link['sub'] as $sub_link) {
                    if (in_array($template['active_page'], $sub_link)) {
                        $li_active = ' class="active"';
                        break;
                    }
                    // 3rd level links
                    if (isset($sub_link['sub']) && $sub_link['sub']) {
                        foreach ($sub_link['sub'] as $sub2_link) {
                            if (in_array($template['active_page'], $sub2_link)) {
                                $li_active = ' class="active "';
                                break;
                            }
                        }
                    }
                }
            }


            for($n=1; $n<=10; $n++) {
                if (isset($link['sub'.$n]) && $link['sub'.$n]) {
                    foreach ($link['sub'.$n] as $sub_link) {
                        if (in_array($template['active_page'], $sub_link)) {
                            $li_active = ' class="active"';
                            break;
                        }
                    }
                }
            }

            ?>


            <li<?php echo $li_active; ?>>
                <a href="<?php echo $url; ?>"><?php if (isset($link['sub']) && $link['sub']) { // if the link has a submenu ?><?php } echo $icon; ?><span><?php echo $link['name']; ?></span> <?php if (isset($link['opt']) && $link['opt']) { // If the header has options set ?><?php echo $link['opt']; ?><?php } ?><?php if($link['sub'] != 0){?> <i class="icon-arrow-down"></i> <?php }?></a>
                <?php if (isset($link['sub']) && $link['sub']) { // if the link has a submenu ?>
                <ul class="treeview-menu">
                    <?php foreach ($link['sub'] as $sub_link) {
                    if(isset($sub_link["permission"])){
                        if(!Auth::user()->hasPermissionTo($sub_link["permission"])){
                            continue;
                        }
                    }
                    $link_class = '';
                    $li_active = '';
                    $submenu_link = '';

                    // Get 2nd level link's vital info
                    $url        = (isset($sub_link['url']) && $sub_link['url']) ? $sub_link['url'] : '#';
                    $active     = (isset($sub_link['url']) && ($template['active_page'] == $sub_link['url'])) ? 'active' : '';

                    // Check if the link has a submenu
                    if (isset($sub_link['sub']) && $sub_link['sub']) {
                        // Since it has a submenu, we need to check if we have to add the class active
                        // to its parent li element (only if a 3rd level link is active)
                        foreach ($sub_link['sub'] as $sub2_link) {
                            if (in_array($template['active_page'], $sub2_link)) {
                                $li_active = ' class="active treeview"';
                                break;
                            }
                        }
                    }
                    ?>

                    <li<?php echo $li_active; ?>>
                        <a href="<?php echo $url; ?>"class="waves-effect waves-dark <?php echo $active; ?> "><i class="icon-arrow-right"></i><?php echo $sub_link['name']; ?><?php if (isset($sub_link['opt']) && $sub_link['opt']) { // If the header has options set ?><?php echo $sub_link['opt']; ?><?php } ?><?php if($sub_link['name'] != 'Social'){ if (isset($sub_link['sub']) && $sub_link['sub']) { ?> <i class="icofont icofont-caret-right"></i> <?php }}?><?php if($sub_link['name'] == 'Social'){?> <i class="icofont icofont-caret-right"></i> <?php } ?></a>
                        <?php if (isset($sub_link['sub']) && $sub_link['sub']) { ?>
                        <ul class="treeview-menu">
                            <?php foreach ($sub_link['sub'] as $sub2_link) {
                            $link_class = '';
                            $li_active = '';
                            $submenu_link = '';
                            // Get 3rd level link's vital info
                            $url    = (isset($sub2_link['url']) && $sub2_link['url']) ? $sub2_link['url'] : '#';
                            $active = (isset($sub2_link['url']) && ($template['active_page'] == $sub2_link['url'])) ? ' active' : '';
                            $submenu_link = 'waves-effect waves-dark';
                            if ($submenu_link || $active) {
                                $link_class = ' class="'. $submenu_link . $active .'"';
                            }
                            ?>
                            <li>
                                <a  href="<?php echo $url; ?>"<?php echo $link_class; ?> ><i class="icon-arrow-right"></i><?php echo $sub2_link['name']; ?><?php if (isset($sub2_link['sub']) && $sub2_link['sub']) { ?><i class="icofont icofont-caret-right"></i><?php }?></a>
                                <?php if (isset($sub_link['sub']) && $sub_link['sub']) {
                                if($sub2_link['name'] == 'Level Three' && $sub2_link['opt'] == 'Level Three') { ?>
                                <ul class="treeview-menu">
                                    <?php foreach ($sub2_link['sub'] as $sub4_link){
                                    // Get 4rd level link's vital info
                                    $url    = (isset($sub4_link['url']) && $sub4_link['url']) ? $sub4_link['url'] : '#';
                                    $active = (isset($sub4_link['url']) && ($template['active_page'] == $sub4_link['url'])) ? ' class="active"' : '';
                                    ?>
                                    <li>
                                        <a href="<?php echo $url; ?>"<?php echo $active ?>><i class="icon-arrow-right"></i><?php echo $sub4_link['name']; ?></a>
                                    </li>
                                    <?php }?>
                                </ul>
                                <?php }?>
                                <?php }?>
                            </li>
                            <?php } ?>
                        </ul>
                        <?php } ?>
                    </li>
                    <?php } ?>
                </ul>
                <?php } ?>

            </li>

            <?php } ?>
            <li>
                @php
                    $rolename=Auth::user()->roles->pluck('name');
                    $show_become_agent=true;
                    if(count($rolename)>0){
                        $show_become_agent=null;
                    }
                @endphp
                @isset($show_become_agent)
                    <form action="{{route("becomeingagent")}}">
                        <div>
                            <button type="submit" class="btn btn-success waves-effect waves-light m-r-30 js-dynamic-enable hidden-md-up"  style="margin-top: 20px;
    margin-left: 7px;">Become a Sinorama agent</button>
                            <button type="submit" class="btn btn-success waves-effect waves-light m-r-30 js-dynamic-enable hidden-md-down" >Become a Sinorama agent</button>
                        </div>

                    </form>
                @endisset

            </li>
        </ul>
        <?php }?>
    </section>
</aside>
