<style>
    #stopaircode{
        position:relative;
        width:112px;
        height:8px;
        margin:0 10px;
        display: inline-block;
        }
    #stopaircode em{
        position:absolute;
        left:0;
        width:100%;
        text-align:center;}
    #stopaircode em#up{top:-4px;font-size: 14px;font-weight: 300;}
    #stopaircode em#down{bottom:-18px;}
</style>
@extends('layouts.mainframe')

@section('extracss')
    <!-- Chartlist chart css -->
    <link rel="stylesheet" href="/bower_components/chartist/dist/chartist.css" type="text/css" media="all">

    <!-- Weather css -->
    <link href="assets/css/svg-weather.css" rel="stylesheet">

    <!-- Echart js -->
    <script src="assets/plugins/charts/echarts/js/echarts-all.js"></script>
@endsection

@section('content')


    <nav class="content-wrapper">
        <!-- Container-fluid starts -->
        <!-- Main content starts -->
        <nav class="container-fluid">
            <div class="row">
                <div class="main-header">
                    <ol class="breadcrumb breadcrumb-title breadcrumb-arrow">
                        <li class="breadcrumb-item"><a href="/home"><i class="icofont icofont-home"></i></a>
                        </li>
                        <li class="breadcrumb-item"><a href="#">DASHBOARD</a>
                        </li>

                    </ol>
                </div>
            </div>

            <nav class="navbar navbar-light bg-faded m-b-30 p-10">
                <h4>SPECIAL OFFERS <span style="font-size:16px;">( 0 records )</span></h4>
            </nav>

            <nav id="recentorders" class="navbar navbar-light bg-faded m-b-30 p-10">
                <h4 >RECENT ORDERS <span style="font-size:16px;">( {{count($orders)}} records )</span></h4>
            </nav>

            <div id="displayRecentorder" class="row" style="display: none">
                @isset($orders)
                    @foreach ($orders as $order)
                        @include('layouts.parts.ordersumary')
                    @endforeach
                @endisset
            </div>



            <nav id="cashAccount" class="navbar navbar-light bg-faded m-b-30 p-10">
                <h4>CASH ACCOUNT <span style="font-size:16px;">( {{count($journals)}} records )</span></h4>
            </nav>
            <div id="displayCashAccount" class="row m-b-30 dashboard-header" style="display: none">

            @foreach ($journals as $journal)

                    <div class="col-lg-3 col-sm-6">
                        <div class="dashboard-primary bg-primary">
                            <div class="sales-primary">
                                <i class="ion-social-usd"></i>
                                <div class="f-right">
                                    <h2 class="counter">{{$journal->balance->getAmount()/100}}</h2>
                                    <span>{{$journal->currency}}</span>
                                </div>
                            </div>
                            <div class="bg-dark-primary">
                                <p class="week-sales">LAST UPDATE AT</p>
                                <p class="total-sales">{{$journal->updated_at}}</p>
                            </div>
                        </div>
                    </div>

            @endforeach
            </div>
            <!-- 4-blocks row start -->

            <nav class="navbar navbar-light bg-faded m-b-30 p-10">
                <h4>POINTS <span style="font-size:16px;">( {{count($points)}} records )</span></h4>
            </nav>
            <div class="row m-b-30 dashboard-header">

                @foreach ($points as $journal)

                    <div class="col-lg-3 col-sm-6">
                        <div class="dashboard-success bg-success">
                            <div class="sales-success" style="margin-bottom: 60px;">
                                <div class="f-right">
                                    <span style="font-size:16px;">{{$journal->currency}}</span>
                                </div>
                                <div class="f-left">
                                    <h2 class="counter">{{$journal->balance->getAmount()}}</h2>
                                    <span style="font-size:22px;">Points</span>
                                </div>
                            </div>
                            <div class="bg-dark-success">
                                <p class="week-sales">Last update at</p>
                                <p class="total-sales">{{$journal->updated_at}}</p>
                            </div>
                        </div>
                    </div>

                @endforeach
            </div>


        </div>
        <!-- Main content ends -->



        <!-- Container-fluid ends -->
    </div>
@endsection



@section('extrascript')

    <!-- Sparkline charts -->
    <script src="/bower_components/jquery-sparkline/dist/jquery.sparkline.js"></script>

    <!-- Counter js  -->
    <script src="/bower_components/waypoints/lib/jquery.waypoints.min.js"></script>
    <script src="assets/plugins/countdown/js/jquery.counterup.js"></script>

    <!-- custom js -->
    <script type="text/javascript" src="assets/pages/dashboard.js"></script>
    <script>
        function dealorder(action,pnr,orderid,provider){

            window.location = "{{route("orderdetail")}}?pnr="+pnr+"&orderid="+orderid+"&provider="+provider+"&";
        }

        $("#recentorders").click(function () {
            $('#displayRecentorder').show();
        });

        $("#cashAccount").click(function () {
            $('#displayCashAccount').show();
        });
    </script>
@endsection
