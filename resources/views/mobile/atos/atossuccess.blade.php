@extends('mobile.layouts.mainframe')
@include('mobile.layouts.Myatos')

<?php
/**
 * Created by PhpStorm.
 * User: localUser
 * Date: 12/20/2017
 * Time: 4:39 PM
 */



$_SESSION["showhead"]="yes";
$fullsite="";


global $lang, $currency;

//echo (isset($_POST)&&isset($_POST["data"]));

if (isset($_POST) && isset($_POST["DATA"])) {
    //include('functions/Myatos.php');

    $myatos = new Myatos();
    $myatos->decodeResponse($_POST);

    $orderId = strtoupper($myatos->responsedata["order_id"]);
    $amount = $myatos->responsedata["amount"];
    $currency = strtoupper($myatos->responsedata["currency_code"]);
    $response_code = $myatos->responsedata["response_code"];
    $error = $myatos->responsedata["error"];

    //echo $myatos->isSuccess();

    if ($myatos->isSuccess() && $response_code == "00") {
        try {
            $db = new PDO('mysql:host=sinoramamasterdb.cugbitmg8jvq.us-west-2.rds.amazonaws.com;dbname=fts;charset=utf8', 'flight', 'flight');
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . PHP_EOL;
            die();
        }

        $orderQuery = $db->prepare("SELECT * FROM t_order WHERE orderid = ?");
        $orderQuery->execute(array($orderId));
        $order = $orderQuery->fetch(PDO::FETCH_ASSOC);

        if ($amount == $order['totalprice'] && $currency == '978' && $order['currency'] == 'EUR') {
            $result = $db->exec("UPDATE t_order SET paystatus='paid',modifydatetime=now() WHERE orderid='" . $order['orderid'] . "'");
            if ($result) {
                $InsertResult = $db->exec("INSERT into t_order_payment values('" . $order['orderid'] . "', 1, '" . $order['currency'] . "', " . $order['totalprice'] . ", 'Credit Card', 'paid', now(), 'unknown#atos')");
                $msg = "1";
            }
        } else {
            $msg="2";
        }

        $db = null;
    } else {
        $msg="3";
    }

    $token = md5('sinoramaWorld_02372785@!#d_air_API' . $order['orderid']);
    if($env== 'master')
    {
        $paymenturl = 'https://mo.air.sinorama.ca/pay?fullsite=yes&orderid='.$order['orderid'].'&token='.$token.'&valid='.base64_encode(time());
        $receipturl='https://mo.air.sinorama.ca/receipt?orderid='.$order['orderid'];
        $homeurl='https://mo.air.sinorama.ca/';
    }
    else
    {
        $paymenturl = 'https://dev.mo.air.sinorama.ca/pay?fullsite=yes&orderid='.$order['orderid'].'&token='.$token.'&valid='.base64_encode(time());
        $receipturl='https://dev.mo.air.sinorama.ca/receipt?orderid='.$order['orderid'];
        $homeurl='https://dev.mo.air.sinorama.ca/';
    }

}


?>
@section('content')
    <!-- Views -->
    <div class="views">
        <!-- Your main view, should have "view-main" class -->
        <div class="view view-main">

            <!-- Pages container, because we use fixed-through navbar and toolbar, it has additional appropriate classes-->
            <div class="pages toolbar-through">
                <!-- Page, "data-page" contains page name -->
                <div data-page="index" class="page search">
                    <!-- Scrollable page content -->
                    <div class="page-content">
                        <div class="navbar navbar_list">
                            <div class="navbar-inner">
                                <!-- We need cool sliding animation on title element, so we have additional "sliding" class -->
                                <div class="left logo_text">
                                    <a href="/" class="external">
                                        <img src="../dist/img/logi_u4.png" style="margin-top: 5px">
                                    </a>
                                </div>
                                <!-- right
                                <div class="right  text-right">
                                    <span>
                                        <a href="#" data-panel="right" class="open-panel">
                                        <img src="../dist/img/u107.png" width="17">
                                        </a>
                                    </span>
                                </div> -->
                            </div>
                        </div>


                        <div class="card" id="h1" style="display: none">
                            <div class="card-content card-content-padding">@lang('language.OrderSucess')</div>
                            <div class="card-footer">
                                <a href="#" class="link" id="receipt">@lang('language.Receipt')</a>
                                <a href="#" class="link" id="reorder">@lang('language.Home')</a>
                            </div>
                        </div>

                        <div class="card" id="h2" style="display: none">
                            <div class="card-content card-content-padding">@lang('language.errorinfo')</div>
                            <div class="card-footer">
                                <a href="#" class="link" id="repay">@lang('language.Repay')</a>
                                <a href="#" class="link" id="reorder">@lang('language.Reorder')</a>
                            </div>
                        </div>

                        <div class="card" id="h3" style="display: none">
                            <div class="card-content card-content-padding">@lang('language.systemerror')</div>
                            <div class="card-footer">
                                <a href="#" class="link" id="repay">@lang('language.Repay')</a>
                                <a href="#" class="link" id="reorder">@lang('language.Reorder')</a>
                            </div>
                        </div>

                        <div class="toolbar">
                            <div class="toolbar-inner">
                                <!-- Toolbar links -->
                            <!--  <a href="tel:1-866-255-2188" class="external"><img src="dist/img/phone_u6.png"></a>
                <a href="mailto:service@sinoramagroup.com" class="external"><img src="dist/img/u115.png" height="15" style="margin-right: 3px"></a>
                <a href="#" class="link"><img src="../dist/img/u145.png" height="17" style="margin-right: 3px"></a>
                -->
                                <a href="tel:1-866-255-2188" class="external">
                                    <img src="dist/img/phone_u6.png" height="12" style="padding-right: 5px">1-866-255-2188
                                </a>

                                <a href="mailto:service@sinoramagroup.com" class="external">
                                    <img src="dist/img/u115.png" height="12" style="margin-left: 0px; padding-right: 5px">service@sinoramagroup.com
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
<script type="text/javascript">

    tjq(document).ready(function () {
        //countdown();
        tjq('#orderid').text('<?php echo $order['orderid']; ?>');
        var paymenturl='<?php echo $paymenturl ?>';
        var receipturl='<?php echo $receipturl?>';
        var homeurl='<?php echo $homeurl ?>';
        document.getElementById("receipt").onclick = function () {
            location.href=receipturl;
        };

        document.getElementById("repay").onclick = function () {
            location.href=paymenturl;
        };

        document.getElementById("reorder").onclick = function () {
            location.href = homeurl;
        };


        <?php if($msg=='1') {?>
        tjq('#h1').css('display','');
        window.setTimeout(redirctreceipt,7000);
        <?php }  ?>

        <?php if($msg=='2') {?>
        tjq('#h2').css('display','');
        window.setTimeout(redirctpay,7000);
        <?php } ?>

        <?php if($msg=='3') {?>
        tjq('#h3').css('display','');
        window.setTimeout(redirctpay,7000);
        <?php } ?>



    });

    function redirctpay(){
        window.location=paymenturl;
    }

    function redirctreceipt(){
        window.location=receipturl;
    }



    var timeout = 10;

    function countdown() {
        var timeleft = tjq("#timeleft");
        timeleft.html('('+timeout+')');

        if (timeout == 0) {
            window.opener=null;
            window.open('','_self');
            window.close();
        } else {
            setTimeout("countdown()", 1000);
        }

        timeout--;
    }
</script>
