@extends('mobile.layouts.mainframe')

<?php


$_SESSION["showhead"]="no";
$fullsite="";


global $lang, $currency;

if (isset($_REQUEST) && isset($_REQUEST['PNR'])) {
    $PNR = strtoupper($_REQUEST['PNR']);
} else {
    $PNR = '';
}

if (!empty($PNR)) {
try {
    $db = new PDO('mysql:host=sinoramamasterdb.cugbitmg8jvq.us-west-2.rds.amazonaws.com;dbname=fts;charset=utf8', 'flight', 'flight');
} catch (PDOException $e) {
    print "Error!: " . $e->getMessage() . PHP_EOL;
    die();
}

$orderQuery = $db->prepare("SELECT * FROM t_order WHERE PNR = ?");
$orderQuery->execute(array($PNR));
$order = $orderQuery->fetch(PDO::FETCH_ASSOC);

$db = null;

$token = md5($order['orderid'] . 'sinoramaworld-flight12!@');
if($env== 'master')
    $action='https://mo.air.sinorama.ca/atos?';
else  $action='https://dev.mo.air.sinorama.ca/atos?';


?>
<div class="person-information ">
    <h3> @lang('language.cardtype')</h3>
    <div class="form-group row">
        <div class="col-xs-12 col-md-12">
            <label class="radio-inline col-xs-2 col-md-2">
                <input type="radio" value="visa" name="atos_payment_method" id="payment_visa" class="grey " checked="">
                <img src="https://air.sinorama.ca/assets/images/visa.png">
            </label>
            <label class="radio-inline col-xs-2 col-md-2">
                <input type="radio" value="mastercard" name="atos_payment_method" class="grey ">
                <img src="https://air.sinorama.ca/assets/images/mastercard.png">
            </label>
            <label class="radio-inline col-xs-2 col-md-2 ">
                <input type="radio" value="cb" name="atos_payment_method">
                <img width="42" height="25" src="https://booking.sinoramabus.com/website/asset/img/payment/logo/CB.gif">
            </label>
        </div>
    </div>
</div>
<div class="person-information ">
    <div class="form-group row">
        <div class="col-sm-6 col-md-5">
            <a class="button button-round active"  href="javascript:void(0);" id="atosToPay"> @lang('language.submit')</a>
        </div>
        <form id="atos_payform" method="POST" action="<?php echo $action; ?>" target="_parent">
            <input type="hidden" name="orderId" value="<?php echo $order['orderid']; ?>">
            <input type="hidden" name="token" value="<?php echo $token; ?>">
            <input type="hidden" name="lang" value="<?php echo $lang; ?>">
            <input type="hidden" name="atos_payment" value="visa">
            {{csrf_field()}}
        </form>
    </div>
</div>
<?php
}

?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(function () {

        tjq('#atosToPay').click(function (e) {
            e.preventDefault();

            //tjq(window.parent.document).find('.payment-opacity-overlay').show();
            tjq('#atos_payform').submit();
        });

        tjq('input[name=atos_payment_method]').click(function () {
            tjq('input[name=atos_payment]').val(tjq(this).val());
        });
    });
</script>

