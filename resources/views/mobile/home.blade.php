
<!-- 111 -->
<!-- Views -->
<div class="views">
    <!-- Your main view, should have "view-main" class -->
    <div class="view view-main">

        <!-- Pages container, because we use fixed-through navbar and toolbar, it has additional appropriate classes-->
        <div class="pages">
            <!-- Page, "data-page" contains page name -->
            <div data-page="index" class="page">
                <!-- Scrollable page content -->
                <div class="page-content">
                    <!-- Top Navbar-->
                    <div class="navbar">
                        <div class="navbar-inner">
                            <!-- We need cool sliding animation on title element, so we have additional "sliding" class -->
                            <div class="left logo_text">
                                <img src="dist/img/logi_u4.png" height="auto"><br><br><br>

                            </div>
                            <!-- right -->
                            <div class="right logo_text text-right">
                                <span style="font-size:14px"><img src="dist/img/phone_u6.png"> 1-866-255-2188</span>
                                <span> <br></span>
                                <span> <br></span>
                                <span><img src="dist/img/u35.png" width="18"> </span>
                            </div>
                        </div>
                    </div>
                    <div class="content-block">
                        <div class="content-block-title"><img src="dist/img/u25.png" height="auto">&nbsp;<strong>Search Results</strong></div>
                    </div>
                    <div class="content-block-search">
                        <p class="buttons-row">
                            <a href="#tab1" class="button active">Return</a>
                            <a href="#tab2" class="button">One way</a>                        </p>
                    </div>

                    <div class="content-block">
                        <div class="content-block-title">Flying from</div>
                        <div class="list-block inset">
                            <ul>
                                <li>
                                    <div class="item-content">
                                        <div class="item-inner">
                                            <div class="item-input">
                                                <input type="text" placeholder="Enter City or Airport">
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                        <div class="content-block-title">Flying to</div>
                        <div class="list-block inset">
                            <ul>
                                <li>
                                    <div class="item-content">
                                        <div class="item-inner">
                                            <div class="item-input">
                                                <input type="text" placeholder="YUL-Montreal  International Airport">
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                        <div class="row">
                            <div class="col-50">
                                <div class="content-block-title">Departing</div>
                                <div class="list-block inset">
                                    <ul>
                                        <li>
                                            <div class="item-content">
                                                <div class="item-inner">
                                                    <div class="item-input">
                                                        <input type="text" placeholder="yyyy-mm-dd" readonly
                                                               id="calendar-Departing">
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-50">
                                <div class="content-block-title">Returning</div>
                                <div class="list-block inset">
                                    <ul>
                                        <li>
                                            <div class="item-content">
                                                <div class="item-inner">
                                                    <div class="item-input">
                                                        <input type="text" placeholder="yyyy-mm-dd" readonly
                                                               id="calendar-Returning">
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-50">
                                <div class="content-block-title">Traveller(s)</div>
                                <div class="list-block inset">
                                    <ul>
                                        <li>
                                            <div class="item-content">
                                                <div class="item-inner">
                                                    <div class="item-input">
                                                        <select>
                                                            <option selected="" value="2 adult">2 adult</option>
                                                            <option value="0 children(2-12)">0 children(2-12)</option>
                                                            <option value="0 infant(<2)">0 infant(&lt;2)</option>
                                                            <option value="0 infangt(>2)">0 infangt(&gt;2)</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-50">
                                <div class="content-block-title">Class</div>
                                <div class="list-block inset">
                                    <ul>
                                        <li>
                                            <div class="item-content">
                                                <div class="item-inner">
                                                    <div class="item-input">
                                                        <select>
                                                            <option selected="" value="Economy">Economy</option>
                                                            <option value="Premium Economy">Premium Economy</option>
                                                            <option value="Premium Business">Premium Business</option>
                                                            <option value="Business">Business</option>
                                                            <option value="Premium First">Premium First</option>
                                                            <option value="Frist">Frist</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <label class="label-switch">
                            <input type="checkbox">
                            <div class="checkbox"></div>
                        </label> Nonstop
                        <p><a href="#" class="button active">Search <i class="f7-icons size-14">search_strong</i></a></p>
                    </div>
                    <div class="card">
                        <div class="card-content">
                            <div class="card-content-inner">
                                <img src="https://framework7.cn/Public/i/logo-new.png" height="60">这里放图片 </div>
                        </div>
                    </div><br>
                    <!-- Newsletter -->
                    <div class="content-block">
                        <div class="content-block-title">Newsletter</div>
                        <div class="list-block inset">
                            <div class="row">
                                <div class="col-70">
                                    <div class="list-block">
                                        <ul>
                                            <li>
                                                <div class="item-content">
                                                    <div class="item-inner">
                                                        <div class="item-input">
                                                            <input type="text" placeholder="Enter City or Airport">
                                                        </div>
                                                    </div>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-30">
                                    <a href="#" class="button button-mid active">Submit</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Bottom Toolbar-->
                    <div class="toolbar">
                        <div class="toolbar-inner">
                            <!-- Toolbar links -->
                            <a href="#" class="link">Contact Us</a>
                            <a href="#" class="link">Helps</a>
                            <a href="#" class="link">Terms&Privacy Policy</a>
                            <a href="#" class="link">Copyright</a>                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
