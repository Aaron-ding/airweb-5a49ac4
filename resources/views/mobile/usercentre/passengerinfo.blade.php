
@include('mobile.layouts.DnodeSyncClient')
<?php
$action=(isset($_GET['action'])) ? $_GET['action'] : 'get';
$data= (isset($_POST['data'])) ? $_POST['data'] : '';
$email=session('email');

if($env== 'master')
    $url='https://mo.air.sinorama.ca/';
else
    $url='https://dev.mo.air.sinorama/';

if(!isset($email)){
    header("Location: ".$url);
    exit();
}

//case "get" all passenger
if($action=="get") {
    $para = array(
        array(
            "userid" => $email,
        ));

    $response = dnode("getFrequentPassengers", $para);
    //var_dump($response);
    if ($response[0][0] == "success") {
        $result = $response[0][1];
        if ($result == "No_records") {
            $record = "no";

        }
    }

}

//case "get" one passenger
if($action=="getone") {
    $para = array(
        array(
            "userid" => $email,
            'passengerindex'=>$data
        ));

    $response = dnode("getFrequentPassengers", $para);
    //var_dump($response);
    if ($response[0][0] == "success") {
        $result2 = $response[0][1];
        echo json_encode($result2);

        if ($result2 == "No_records") {
            $record = "no";
            echo json_encode(["error"=>"no record"]);
        }
    }
    else
        echo json_encode(["error"=>"failed"]);
    exit;

}

//case add
else if($action=="add"){
    $para = array(
        array(
            "userid" => $email,
            'firstname'=> $data["firstname"],
            'middlename'=> $data["middlename"],
            'lastname'=> $data["lastname"],
            'gender'=> $data["gender"],
            'birthday'=> $data["birthday"],
            'nationality'=> $data["nationality"],
            'documentnumber'=> $data["documentnumber"],
            'documentvalidity'=> $data["documentvalidity"],
            'issuingcountry'=> $data["issuingcountry"]
        ));

    $response = dnode("addFrequentPassengers", $para);


    if($response[0][0]=="success"){
        echo json_encode(["id"=>$response[0][1]]);
    }
    else
        echo json_encode(["error"=>"failed"]);


    exit;

}


//case "update"
else if($action=="update"){
    $para = array(
        array(
            "userid" => $email,
            'passengerindex' => $data["passengerindex"],
            'firstname'=> $data["firstname"],
            'middlename'=> $data["middlename"],
            'lastname'=> $data["lastname"],
            'gender'=> $data["gender"],
            'birthday'=> $data["birthday"],
            'nationality'=> $data["nationality"],
            'documentnumber'=> $data["documentnumber"],
            'documentvalidity'=> $data["documentvalidity"],
            'issuingcountry'=> $data["issuingcountry"]
        ));
    $response = dnode("updateFrequentPassengers", $para);

    //var_dump($response);
    if($response[0][0]=="success"){
        echo json_encode(["id"=>$response[0][1]]);
    }
    else
        echo json_encode(["error"=>"failed"]);
    exit;
}

//case delete
else if($action=="delete"){
    $para = array(
        array(
            "userid" => $email,
            "passengerindex" => $data
        ));
    $response = dnode("deleteFrequentPassengers", $para);


    if($response[0][0]=="success"){
        echo json_encode(["success"=>$response[0][0]]);
    }
    else
        echo json_encode(["error"=>"failed"]);
    exit;

}





function dnode($fun,$para){
    $dnode = new \DnodeSyncClient\Dnode();

    $env=getSafeEnv();
    $svraddr = ($env=='master'? "":"dev.")  . 'search.air.sinorama.ca';
    // echo $svraddr."######" . $fun;
    if ($svraddr!=""){
        $connection = $dnode->connect($svraddr, 9998);
        $result=$connection->call($fun,$para);
        $connection->close();

        return $result;
    }else{
        echo "I do not know where am I";
        return;
    }
}



?>



@extends('mobile.layouts.mainframe')

@section('extracss')
    <link rel="stylesheet" href="dist/css/search-app.css">
@endsection

@section('content')
    <!-- Views -->
    <div class="views">
        <!-- Your main view, should have "view-main" class -->
        <div class="view view-main">

        @include('mobile.layouts.parts.navbar')

        <!-- Pages container, because we use fixed-through navbar and toolbar, it has additional appropriate classes-->
            <div class="pages toolbar-through navbar-through">
                <!-- Page, "data-page" contains page name -->
                <div data-page="index" class="page search">
                    <!-- Scrollable page content -->
                    <div class="page-content">

                        <div class="card"  xmlns="http://www.w3.org/1999/html">
                            <div class="row no-gutter">
                                <div class="col-90 left-block">
                                    <div class="row">
                                        <div> <a href="javascript:void(0);" id="addpassenger" style="color: #007aff; font-weight: 600;">+ @lang("language.addPassenger")
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div id="searchresultlist">
                            <?php if(!isset($record)){
                            foreach ($result as $passenger){ ?>

                            <div id="ft_item_template" class="card" data-index="<?php echo $passenger["passengerindex"]?>" xmlns="http://www.w3.org/1999/html">
                                <div class="row no-gutter">
                                    <div class="col-100 left-block">
                                        <div class="row">
                                            <div class="col-65"> <b>@lang("language.name"):</b>
                                                <span id="name"> <?php echo $passenger["firstname"]." ".$passenger["middlename"]." ".$passenger["lastname"]?></span>
                                            </div>
                                            <div class="col-35"> <b>@lang("language.gender"):</b>
                                                <span id="gender"> <?php
                                                    if($passenger["gender"]=="F") echo trans("language.Female");
                                                    else if($passenger["gender"]=="M") echo trans("language.Male");?></span>
                                            </div>
                                        </div>
                                        <div class="row" style="padding-top: 10px">
                                            <div class="col-75">
                                                <b>@lang("language.Birthday"):</b><span id="slbirthday"><?php echo $passenger["birthday"] ?></span>
                                            </div>
                                            <div class="col-25">
                                                <a id="modifybutton" onclick="edit('<?php echo $passenger["passengerindex"] ?>')" href="#" >
                                                    <i class="icon f7-icons" style="padding-right: 10px">
                                                        compose
                                                    </i>
                                                </a>
                                                <a id="deletebutton" onclick="deleteinfo('<?php echo $passenger["passengerindex"] ?>')" href="#" >
                                                    <i class="icon f7-icons">trash</i>
                                                </a>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <!-- left
                                <div class="row no-gutter" style="padding-bottom: 3px;">

                                    <div class="col-55 line-three">

                                    </div>-->
                            <!-- button
                                    <div class="col-20 right-btn" style="padding-right: 5px;">
                                        <a id="modifybutton" onclick="edit('<?php echo $passenger["passengerindex"] ?>')" href="#" class="button button-fill color-blue" style="font-size: 12px" >@lang("language.modify")</a>
                                    </div>

                                    <div class="col-20 right-btn" style="padding-right: 5px;">
                                        <a id="deletebutton" onclick="deleteinfo('<?php echo $passenger["passengerindex"] ?>')" href="#" class="button active gobutton" style="font-size: 12px" >@lang("language.delete")</a>
                                    </div>
                                </div>-->
                            </div>
                            <?php  }  ?>
                            <?php }  ?>
                            <?php if(isset($record)){ ?>


                            <div id="ft_item_template" class="card"  xmlns="http://www.w3.org/1999/html">
                                <div class="row no-gutter">
                                    <div class="col-60 left-block">
                                        <div class="row">
                                            <div> No History </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php } ?>



                            <div class="popup popup-passenger-1 person-information" style="background: #efeff4"  dataidx="1" >
                                <div class="page ">
                                    <div class="page-content">
                                        <div class="navbar" style="position: fixed; top: 0; left: 0; z-index: 999;">
                                            <div class="navbar-inner">
                                                <div class="left sliding">
                                                    <a href="#" class="link close-popup">
                                                        <i class="f7-icons">chevron_left</i>
                                                        <span class="">@lang('language.Close')</span>
                                                    </a>
                                                </div>
                                                <div class="right sliding">
                                                    <a href="#" class="link modify-popup">
                                                        <span >@lang('language.Save')</span>
                                                        <i class="f7-icons">chevron_right</i></a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card travellers-card" id="bookingform" style="font-size:18px;margin-top: 50px;">
                                            <div class="card-content">
                                                <div class="list-block inline-labels">
                                                    <ul id="rgneditpassengerinfo">
                                                        <!-- Text inputs -->
                                                        <li>
                                                            <div class="item-content item-input">
                                                                <div class="item-media">
                                                                    <i class="icon f7-icons">person</i>
                                                                </div>
                                                                <div class="item-inner" style="margin-left: 15px;">
                                                                    <div class="item-title item-label" style="font-size:15px;color: #565656;">@lang('language.CFirstName')</div>
                                                                    <div class="item-input-wrap">
                                                                        <input name="firstname" id="firstname" type="text" required style="height:30px;padding-left:10px;font-size:15px;"   placeholder="@lang('language.FirstName')" >
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </li>
                                                        <li>
                                                            <div class="item-content item-input">
                                                                <div class="item-media">
                                                                    <i class="icon f7-icons">person</i>
                                                                </div>
                                                                <div class="item-inner" style="margin-left: 15px;">
                                                                    <div class="item-title item-label" style="font-size:15px;color: #565656;">@lang('language.CMiddleName')</div>
                                                                    <div class="item-input-wrap">
                                                                        <input name="middlename" id="middlename" type="text"  style="height:30px;padding-left:10px;font-size:15px;"  placeholder="@lang('language.MiddleName')">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </li>
                                                        <li>
                                                            <div class="item-content item-input">
                                                                <div class="item-media">
                                                                    <i class="icon f7-icons">person</i>
                                                                </div>
                                                                <div class="item-inner" style="margin-left: 15px;">
                                                                    <div class="item-title item-label" style="font-size:15px;color: #565656;">@lang('language.CLastName')</div>
                                                                    <div class="item-input-wrap">
                                                                        <input name="lastname" id="lastname" type="text" style="height:30px;padding-left:10px;font-size:15px;" required placeholder="@lang('language.LastName') ">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </li>
                                                        <!-- gender Select -->
                                                        <li>
                                                            <div class="item-content item-input">
                                                                <div class="item-media">
                                                                    <i class="icon f7-icons">person</i>
                                                                </div>
                                                                <div class="item-inner" style="margin-left: 15px;">
                                                                    <div class="item-title item-label" style="font-size:15px;color: #565656;">@lang('language.Sex')</div>
                                                                    <div class="item-input-wrap" style="width:60%;">
                                                                        <div>
                                                                            <select name="gender" required style="font-size:15px;padding-right: 15px;">
                                                                                <option value="M">@lang('language.Male')</option>
                                                                                <option value="F">@lang('language.Female')</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </li>

                                                        <!-- Date -->
                                                        <li>
                                                            <div class="item-content item-input">
                                                                <div class="item-media"><i class="icon f7-icons">calendar</i>
                                                                </div>
                                                                <div class="item-inner">
                                                                    <div class="item-title label" id="birthflag" style="width: 40%; font-size:15px; color: #565656;">@lang('language.Birthday')</div>
                                                                    <div class="item-input-wrap">
                                                                        <input name="birthday" id="birthday" type="date" required style="height:30px;padding-left:10px;font-size:15px;" placeholder="@lang('language.Birthday')" value="1999-04-30" >
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </li>

                                                        <li>
                                                            <div class="item-content">
                                                                <div class="item-media">
                                                                    <i class="icon f7-icons">world</i>
                                                                </div>

                                                                <div class="item-inner">
                                                                    <div class="item-input" style="margin-bottom: 0px; border: 0px; height: 30px;margin-left: -14px;">
                                                                        <a href="#" id="autocomplete-nationality-1" class="item-link item-content autocomplete-opener">

                                                                            <input name="nationality" popupid="autocomplete-nationality-1"  style="height:30px;" type="hidden">
                                                                            <div class="item-inner popuptext">
                                                                                <div class="item-title" style="color: #565656; font-size:15px;margin-right:0px;margin-left: 0px;">@lang('language.Nationality')
                                                                                </div>
                                                                                <div class="item-after" style="font-size: 15px"></div>
                                                                            </div>
                                                                        </a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </li>



                                                        <!-- Passport Number-->
                                                        <li>
                                                            <div class="item-content item-input">
                                                                <div class="item-media">
                                                                    <i class="icon f7-icons">document_text</i>
                                                                </div>
                                                                <div class="item-inner" style="margin-left: 12px;">
                                                                    <div class="item-title item-label" style="color: #565656;font-size:15px;padding-left:10px;">@lang('language.PassportNumber')</div>
                                                                    <div class="item-input-wrap">
                                                                        <input name="passport" type="text" id="passport"
                                                                               placeholder="@lang('language.PassportNumber')" style="height:30px;padding-left:10px;font-size:15px;">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </li>

                                                        <!-- passport validity -->
                                                        <li>
                                                            <div class="item-content item-input">
                                                                <div class="item-media">
                                                                    <i class="icon f7-icons">document_text</i>
                                                                </div>
                                                                <div class="item-inner" style="margin-left:20px;">
                                                                    <div class="item-title item-label" style="color: #565656;font-size:15px;">@lang('language.ValidityPassport')</div>
                                                                    <div class="item-input-wrap">
                                                                        <input name="passportexpired"  type="text" style="height:30px;padding-left:10px;font-size:15px;"
                                                                               placeholder="@lang('language.ValidityPassport')"
                                                                               readonly id="passportvalidity-1">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </li>



                                                        <!-- Issuing country Select -->
                                                        <li>
                                                            <div class="item-content">
                                                                <div class="item-media">
                                                                    <i class="icon f7-icons">world</i>
                                                                </div>

                                                                <div class="item-inner">
                                                                    <div class="item-input" style="margin-bottom: 0px; border: 0px; height: 30px;margin-left: -15px;">
                                                                        <a href="#" id="autocomplete-issuing-1" class="item-link item-content autocomplete-opener">

                                                                            <input name="passportcountry" popupid="autocomplete-issuing-1"  style="height:30px;" type="hidden">
                                                                            <div class="item-inner popuptext">
                                                                                <div class="item-title" style="color: #565656; font-size:15px;margin-right:0px;margin-left: 0px;">@lang('language.IssuingCountry')
                                                                                </div>
                                                                                <div class="item-after" style="font-size: 15px"></div>
                                                                            </div>
                                                                        </a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </li>
                                                        <div style="display: none"><input id="passengerindex"></div>
                                                        <div style="display: none"><input id="action"></div>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="index-footer" style="padding-left:15px; font-size: 13px; color: #007aff; padding-top: 20px">
                                                <span>
                                                    <i class="icon f7-icons" style="font-size: 20px">info</i>
                                                </span>
                                            <span id="info"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>



                        </div>
                        <div class="index-footer" style="padding-left:13px; font-size: 12px; color: grey;padding-top: 20px;padding-right:14px;">
                            <span id="information"></span>
                        </div>
                    </div>
                </div>
            </div>

            <div class="toolbar">
                <div class="toolbar-inner">
                    <!-- Toolbar links -->
                    <a href="tel:1-866-255-2188" class="external">
                        <img src="dist/img/phone_u6.png" height="12" style="padding-right: 5px">1-866-255-2188
                    </a>

                    <a href="mailto:service@sinoramagroup.com" class="external">
                        <img src="dist/img/u115.png" height="12" style="margin-left: 0px;padding-right: 5px">service@sinoramagroup.com</a>

                    <!--
                    <a href="#" class="link"><img src="../dist/img/u145.png" height="17" style="margin-right: 3px"></a>
                    -->



                </div>
            </div>
        </div>
    </div>
    <input type="hidden" name="_token" id="csrf_token" value="{{csrf_token()}}">
@endsection

@section('extrascript')



    <script>
        tjq(document).ready(function() {
            var lang="@lang('language.Lang')";
            if(lang=='English') {
                monthNames = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
                dayNamesShort=['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];
            }
            else if(lang=="Chinese")
            {
                monthNames = ['一月', '二月', '三月', '四月', '五月', '六月', '七月', '八月' , '九月' , '十月', '十一月', '十二月'];
                dayNamesShort=['日', '一', '二', '三', '四', '五', '六'];
            }
            else if(lang=="French")
            {
                monthNames = ["Janvier", "Février", "Mars","Avril", "Mai", "Juin", "Juillet","Août", "Septembre", "Octobre","Novembre", "Décembre"];
                dayNamesShort=["DIM", "LUN", "MAR", "MER", "JEU", "VEN", "SAM"];
            }

            var nationality1 = myApp.autocomplete({
                openIn: 'popup', //open in popup
                opener: $$('#autocomplete-nationality-1'),//link that opens autocomplete
                backOnSelect: true, //go back after we select somethin,
                source: function (autocomplete, query, render) {
                    var results = [];
                    if (query.length === 0) {
                        render(results);
                        return;
                    }// Render items by passing array with result items
                    render(searchcountry(query));
                },
                onChange: function (autocomplete, value) {
                    // Add item text value to item-after

                    $$('#autocomplete-nationality-1').find('.item-after').text(value[0]);
                    $$('#autocomplete-nationality-1').css('background-color', '#FFF');
                    // Add item value to input value
                    $$('#autocomplete-nationality-1').find('input').val(value[0].substr(0, 2));
                },
                onOpen:function(p){
                    tjq('.autocomplete-popup').find('.close-popup').on('click',function(e){
                        e.stopPropagation();
                        nationality1.close();
                        return false;
                    })
                }
            });

            var issuecountry1 = myApp.autocomplete({
                openIn: 'popup', //open in popup
                opener: $$('#autocomplete-issuing-1'),//link that opens autocomplete
                backOnSelect: true, //go back after we select somethin,
                source: function (autocomplete, query, render) {
                    var results = [];
                    if (query.length === 0) {
                        render(results);
                        return;
                    }// Render items by passing array with result items
                    render(searchcountry(query));
                },
                onChange: function (autocomplete, value) {
                    // Add item text value to item-after

                    $$('#autocomplete-issuing-1').find('.item-after').text(value[0]);
                    $$('#autocomplete-issuing-1').css('background-color', '#FFF');
                    // Add item value to input value
                    $$('#autocomplete-issuing-1').find('input').val(value[0].substr(0, 2));
                },
                onOpen:function(p){
                    tjq('.autocomplete-popup').find('.close-popup').on('click',function(e){
                        e.stopPropagation();
                        issuecountry1.close();
                        return false;
                    })
                }
            });

            var passportvalidity1 = myApp.calendar({
                input: '#passportvalidity-1',
                dateFormat: 'yyyy-mm-dd',
                minDate:new Date(),
                rangePicker: false,
                onClose: function () {
                    $$('#calendar-one').parents('.item-content').css('background-color', '#FFF');
                },
                onChange: function (value) {
                    // Add item text value to item-after

                    //$$('input[name="passportexpired"]').find('.item-after').text(value[0]);
                    //$$('#autocomplete-issuing-1').css('background-color', '#FFF');
                    // Add item value to input value
                    $$('input[name="passportexpired"]').find('input').val(value);
                },
                onOpen: function (p) {
                    $$('.picker-calendar').find('.toolbar-inner').append('<a href="#" id="calendardone" class="button active" style="width:60px;">OK</a>');
                    $$('#calendardone').on('click',function(){
                        passportvalidity1.close();
                    })
                },

                onMonthYearChangeStart: function (p) {
                    $$('.calendar-custom-toolbar .center').text(monthNames[p.currentMonth] + ', ' + p.currentYear);
                }
            });
/*
            var birthday1 = myApp.calendar({
                input: '#birthday',
                dateFormat: 'yyyy-mm-dd',
                minDate:'1900-01-01',
                rangePicker: false,
                onClose: function () {
                    $$('#calendar-one').parents('.item-content').css('background-color', '#FFF');
                },
                onChange: function (value) {
                    // Add item text value to item-after

                    $$('input[name="birthday"]').find('input').val(value);
                },
                onOpen: function (p) {
                    $$('.picker-calendar').find('.toolbar-inner').append('<a href="#" id="calendardone" class="button active" style="width:60px;">OK</a>');
                    $$('#calendardone').on('click',function(){
                        birthday1.close();
                    })
                },

                onMonthYearChangeStart: function (p) {
                    $$('.calendar-custom-toolbar .center').text(monthNames[p.currentMonth] + ', ' + p.currentYear);
                }
            });
*/

            $$('.modify-popup').on('click', function (e) {

                e.preventDefault();
                tjq('#info').html("");

                //use function
                //tjq('#book-submit-errors').html('');
                if (checkform('#rgneditpassengerinfo')==false) {
                    //tjq('#book-submit-errors').html('<i class="fa fa-times"></i>' + " ");
                    //return;
                    alert("@lang('language.error1')");
                }
                else {

                    //compact

                    var updatepassenger = {};
                    updatepassenger["firstname"] = tjq('#firstname').val();
                    updatepassenger["middlename"] = tjq('#middlename').val();
                    updatepassenger["lastname"] = tjq('#lastname').val();
                    updatepassenger["gender"] = tjq('select[name="gender"]').val();
                    updatepassenger["birthday"] = tjq('input[name="birthday"]').val();
                    updatepassenger["nationality"] = tjq('input[name="nationality"]').val();
                    updatepassenger["documentnumber"] = tjq('input[name="passport"]').val();
                    updatepassenger["documentvalidity"] = tjq('input[name="passportexpired"]').val();
                    updatepassenger["issuingcountry"] = tjq('input[name="passportcountry"]').val();
                    updatepassenger["passengerindex"] = tjq('#passengerindex').val();
                    console.log(updatepassenger);

                    var gender;
                    if (updatepassenger["gender"] == "M")
                        gender = "Male";
                    else gender = "Female";

                    var _token = tjq("input[name=_token]").val();


                    //ajax update
                    if (tjq('#action').val() == "update") {
                        tjq.ajax({
                            type: "POST",
                            url: "/passengerinfo?action=update",
                            data: {data: updatepassenger, _token: _token},
                            success: function (response) {
                                console.log(response);
                                var retjson=JSON.parse(response);
                                if("id" in retjson) {
                                    tjq('div[data-index=' + updatepassenger["passengerindex"] + ']').find('#name').html(updatepassenger["firstname"] + " " + updatepassenger["middlename"] + " " + updatepassenger["lastname"]);
                                    tjq('div[data-index=' + updatepassenger["passengerindex"] + ']').find('#slbirthday').html(updatepassenger["birthday"]);
                                    tjq('div[data-index=' + updatepassenger["passengerindex"] + ']').find('#gender').html(gender);
                                    tjq('div[data-index=' + updatepassenger["passengerindex"] + ']').find('#modifybutton').attr("onclick", "edit('" + retjson["id"].trim() + "')");
                                    tjq('div[data-index=' + updatepassenger["passengerindex"] + ']').find('#deletebutton').attr("onclick", "deleteinfo('" + retjson["id"].trim() + "')");
                                    tjq('div[data-index=' + updatepassenger["passengerindex"] + ']').attr('data-index', retjson["id"].trim());
                                    myApp.alert("@lang('language.uds')");
                                    myApp.closeModal(".popup");
                                }
                                else{
                                    tjq('#info').html("@lang('language.Got_problems')");
                                }
                                //tjq('#name').html(updatepassenger["firstname"]+" "+updatepassenger["middlename"]+" "+updatepassenger["lastname"]);
                                //tjq('#dateofbirth').html(updatepassenger["nationality"]);
                                //tjq('#gender').html(updatepassenger["gender"]);

                            }
                        })
                    }

                    //ajax add
                    else if (tjq('#action').val() == "add") {


                        tjq.ajax({
                            type: "POST",
                            url: "/passengerinfo?action=add",
                            data: {data: updatepassenger, _token: _token},
                            success: function (response) {
                                console.log(response);

                                var retjson=JSON.parse(response);
                                if("id" in retjson) {

                                    var template = tjq('#ft_item_template').clone();
                                    template.show();
                                    template.find('#name').html(updatepassenger["firstname"] + " " + updatepassenger["middlename"] + " " + updatepassenger["lastname"]);
                                    template.find('#slbirthday').html(updatepassenger["birthday"]);
                                    template.find('#gender').html(gender);


                                    template.find('#modifybutton').attr("onclick", "edit('" + retjson["id"].trim() + "')");
                                    template.find('#deletebutton').attr("onclick", "deleteinfo('" + retjson["id"].trim() + "')");
                                    template.attr("data-index", retjson["id"].trim());
                                    tjq('#searchresultlist').append(template);
                                    myApp.alert("@lang('language.ads')");
                                    myApp.closeModal(".popup");
                                }
                                else{
                                    tjq('#info').html("@lang('language.Got_problems')");
                                }
                            }
                        })
                    }
                }

            });

            tjq('#addpassenger').on('click', function (e){
                e.preventDefault();
                myApp.popup('.popup-passenger-1');

                tjq("#rgneditpassengerinfo").find('input').each(function(idx,item){
                    tjq(this).css('background','#fff');
                });

                tjq('#firstname').val("");
                tjq('#middlename').val("");
                tjq('#lastname').val("");
                //tjq('select[name="gender"]').filter('option[value="' + passengerinfo["gender"] + '"]');
                tjq('input[name="birthday"]').val("");
                tjq('#autocomplete-nationality-1').find('.item-after').text("");
                tjq('input[name="nationality"]').attr('value',"");

                tjq('input[name="passportexpired"]').val("");
                tjq('#passport').val("");
                tjq('#autocomplete-issuing-1').find('.item-after').text("");
                tjq('input[name="passportcountry"]').attr('value',"");

                //tjq('#passengerindex').val(passengerinfo["passengerindex"]);
                tjq('#action').val("add");
                tjq('#info').html("");
            });

            tjq('input[type="text"], input[type="email"]').on('focus', function () {
                var target = this;
                setTimeout(function(){
                    target.scrollIntoViewIfNeeded();
                    setTimeout(function(){
                        target.scrollIntoViewIfNeeded();
                        console.log('scrollIntoViewIfNeeded');
                    },400);
                },300);
                tjq('.toolbar').hide();
            });

        });

        function edit(param){
            myApp.popup('.popup-passenger-1');
            var _token = tjq("input[name=_token]").val();

            tjq.ajax({
                type: "POST",
                url:"/passengerinfo?action=getone",
                data: {data:param, _token: _token},
                success: function (response) {
                    //console.log(response);
                    //console.log(JSON.parse(response));
                    var passengerinfo=JSON.parse(response);
                    if( !('error' in passengerinfo)) {
                        tjq("#rgneditpassengerinfo").find('input').each(function(idx,item){
                            tjq(this).css('background','#fff');
                        });

                        tjq('#firstname').val(passengerinfo["firstname"]);
                        tjq('#middlename').val(passengerinfo["middlename"]);
                        tjq('#lastname').val(passengerinfo["lastname"]);
                        //tjq('select[name="gender"]').filter('option[value="' + passengerinfo["gender"] + '"]');
                        tjq('select[name="gender"]').val(passengerinfo["gender"]);
                        tjq('input[name="birthday"]').val(passengerinfo["birthday"]);
                        tjq('#autocomplete-nationality-1').find('.item-after').text(countryreverse(passengerinfo["nationality"]));
                        tjq('input[name="nationality"]').attr('value', passengerinfo["nationality"]);

                        tjq('input[name="passportexpired"]').val(passengerinfo["documentvalidity"]);
                        tjq('#passport').val(passengerinfo["documentnumber"]);
                        tjq('#autocomplete-issuing-1').find('.item-after').text(countryreverse(passengerinfo["issuingcountry"]));
                        tjq('input[name="passportcountry"]').attr('value', passengerinfo["issuingcountry"]);

                        tjq('#passengerindex').val(passengerinfo["passengerindex"]);
                        tjq('#action').val("update");
                    }
                    else{
                        myApp.alert("@lang('language.Got_problems')");
                        myApp.closeModal(".popup");
                    }
                }
            });

        }

        function deleteinfo(param){

            myApp.confirm("@lang('language.confirmdelete')", function () {
                var _token = tjq("input[name=_token]").val();

                tjq.ajax({
                    type: "POST",
                    url:"/passengerinfo?action=delete",
                    data: {data:param,_token: _token},
                    success: function (response) {
                        console.log(response);
                        var retjson=JSON.parse(response);
                        if( !('error' in retjson)) {
                            tjq('div[data-index=' + param + ']').hide();
                            tjq('#information').html("@lang('language.ds')");
                        }
                        else
                            myApp.alert("@lang('language.Got_problems')");
                    }
                });
            });
        }

        function checkform(obj){

            var firstname= trim(tjq("#firstname").val());
            var middlename= trim(tjq("#middlename").val());
            var lastname= trim(tjq("#lastname").val());
            var gender= tjq('select[name="gender"]').val();
            var birthday= tjq("#birthday").val();

            var nationality= tjq("input[name=nationality]").val();
            var passport= trim(tjq("input[name=passport]").val());
            var passportexpired= tjq("input[name=passportexpired]").val();
            var passportcountry= tjq("input[name=passportcountry]").val();


            var passchecked = true;

            if (firstname.length < 2) {
                showformerror(tjq("#firstname"));
                return false;
            }
            else {
                if (isChinese(firstname) || !isNormalName(firstname)) {
                    showformerror(tjq("#firstname"));
                    alert("@lang('language.signuperror8')");
                    return false;
                } else {
                    removeitem(tjq("#firstname"));
                }
            }

            if (lastname.length < 2) {
                showformerror(tjq("#lastname"));
                return false;
            }
            else {
                if (isChinese(lastname) || !isNormalName(lastname)) {
                    showformerror(tjq("#lastname"));
                    alert("@lang('language.signuperror8')");
                    return false;
                } else {
                    removeitem(tjq("#lastname"));
                }
            }

            if (middlename.length>0 && (isChinese(middlename) || !isNormalName(middlename) ) ) {
                showformerror(tjq("#middlename"));
                alert("@lang('language.signuperror8')");
                return false;
            } else {
                removeitem(tjq("#middlename"));
            }

            if (trim(gender)=="") {
                showformerror(tjq('select[name="gender"]'));
                return false;
            } else {
                removeitem(tjq('select[name="gender"]'));
            }
            if (trim(birthday)=="") {
                showformerror(tjq("#birthday"));
                return false;
            } else {
                removeitem(tjq("#birthday"));
            }

            removeitem(tjq("input[name=nationality]"));
            removeitem(tjq("input[name=passport]"));
            removeitem(tjq("input[name=passportexpired]"));
            removeitem(tjq("input[name=passportcountry]"));

            if(trim(nationality)!="" || trim(passport)!="" || trim(passportexpired)!="" || trim(passportcountry)!=""){
                if (trim(nationality)=="") {
                    passchecked = false;
                    showformerror(tjq("input[name=nationality]"));
                }
                if (trim(passport)=="") {
                    passchecked = false;
                    showformerror(tjq("input[name=passport]"));
                }
                if (trim(passportexpired)=="") {
                    passchecked = false;
                    showformerror(tjq("input[name=passportexpired]"));
                }
                if (trim(passportcountry)=="") {
                    passchecked = false;
                    showformerror(tjq("input[name=passportcountry]"));
                }
                if(passchecked == false)
                    alert("@lang('language.error4')");
            }
            return passchecked;
        }

        function countryreverse(obj)
        {
            //console.log(obj);
            if(obj==""){
                return "";
            }
            else {
                var country = {};
                var truecount;
                country = searchcountry(obj);

                function checkhaveornot(y) {
                    if (y.indexOf(obj) != -1)
                        return y;
                }

                truecount = country.find(checkhaveornot);
                console.log(truecount);
                return truecount;
            }
        }



        function searchcountry(query,callingcode) {
            var results = [];
            for (var i = 0; i < countryCode.length; i++) {
                var countryitem = countryCode[i];

                if (countryitem.ISO_2_Code.toLowerCase().indexOf(query.toLowerCase()) >= 0 ||
                    countryitem.ISO_3_Code.toLowerCase().indexOf(query.toLowerCase()) >= 0 ||
                    countryitem.countryName.toLowerCase().indexOf(query.toLowerCase()) >= 0 ||
                    countryitem.countryName_CH.toLowerCase().indexOf(query.toLowerCase()) >= 0 ||
                    countryitem.countryName_PY.toLowerCase().indexOf(query.toLowerCase()) >= 0
                ) {
                    if(callingcode!=undefined){
                        var tmpstr = "( "+countryitem.callingCode + " ) " + countryitem.countryName;
                        if (lang != undefined && lang == "Chinese") {
                            tmpstr = "( "+countryitem.callingCode + " ) " + countryitem.countryName_CH;
                        }
                        results.push(tmpstr);

                    }else{
                        var tmpstr = countryitem.ISO_2_Code + "-" + countryitem.countryName;
                        if (lang != undefined && lang == "Chinese") {
                            tmpstr = countryitem.ISO_2_Code + "-" + countryitem.countryName_CH;
                        }
                        results.push(tmpstr);
                    }

                }
            }
            return results;
        }


        function showformerror(item){
            tjq(item).css('background','rgba(255, 45, 85, 0.11)');
        }

        function removeitem(items){
            tjq(items).css('background','#fff');
        }


    </script>

@endsection