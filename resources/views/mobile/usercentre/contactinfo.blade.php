
@extends('mobile.layouts.mainframe')
@include('mobile.layouts.DnodeSyncClient')
<?php
$action=(isset($_GET['action'])) ? $_GET['action'] : 'read';
//echo $action;
$data=(isset($_POST['data'])) ? $_POST['data'] : "";
//var_dump($data);
$userid=session('email');

if($env== 'master')
    $url='https://mo.air.sinorama.ca/';
else
    $url='https://dev.mo.air.sinorama.ca/';

if(!isset($userid)){
    header("Location: ".$url);
    exit();
}



if($action=="update"){
    $para=array(
        array(
            "userid" => $userid,
            "firstname" => $data["firstname"],
            "lastname" => $data["lastname"],
            "email" => $data["email"],
            "callingcode" => $data["callingcode"],
            "phonenumber" => $data["phone"],
            "address" => $data["address"],
            "city" => $data["city"],
            "province" => $data["province"],
            "country" => $data["country"],
            "postcode" => $data["postcode"],
        )
    );

    $response = dnode('addUsersProfiles', $para);
    if($response[0][0]=="success"){
        echo $response[0][0];

    }

    exit;
    //var_dump($response);

}
else {

    $para = array(
        array(
            "userid" => $userid
        ));
    /*
    $para = array(
        array(
            "userid" => $userid
        ));*/
    $response = dnode('getUsersProfiles', $para);
//var_dump($response);
    if ($response[0][0] == "success") {
        $resp = $response[0][1];
        if($resp=="No_records"){
            $record="no";
        }
    }
}

function dnode($fun,$para){
    $dnode = new \DnodeSyncClient\Dnode();
    $svraddr="";

    $env=getSafeEnv();

    $svraddr = ($env=='master'? "":"dev.")  . 'search.air.sinorama.ca';

    // echo $svraddr."######" . $fun;
    if ($svraddr!=""){
        $connection = $dnode->connect($svraddr, 9998);
        $result=$connection->call($fun,$para);
        $connection->close();

        return $result;
    }else{
        echo "I do not know where am I";
        return;
    }
}



?>



@section('extracss')
    <link rel="stylesheet" href="dist/css/search-app.css">
@endsection


@section('content')
    <!-- Views -->
    <div class="views">
        <!-- Your main view, should have "view-main" class -->
        <div class="view view-main">

            @include('mobile.layouts.parts.navbar')

            <!-- Pages container, because we use fixed-through navbar and toolbar, it has additional appropriate classes-->
            <div class="pages toolbar-through navbar-through">
                <!-- Page, "data-page" contains page name -->
                <div data-page="index" class="page search">
                    <!-- Scrollable page content -->
                    <div class="page-content">

                        <div class="card travellers-card" id="contactdataform" style="margin-top: 20px;">
                            <div class="card-content">
                                <div class="list-block">
                                    <ul>
                                        <li>
                                            <div class="item-content">
                                                <div class="item-media">
                                                    <a href="#" style="font-size: 15px; text-align: center">@lang("language.Contact")
                                                    </a>
                                                </div>
                                            </div>
                                        </li>
                                        <!-- Text inputs -->
                                        <li>
                                            <div class="item-content">
                                                <div class="item-media"><i
                                                            class="icon f7-icons">person</i></div>
                                                <div class="item-inner">
                                                    <div class="item-input">
                                                        <input required name="contactfirstname" id="contactfirstname" type="text" style="height:30px;padding-left:15px;" placeholder="@lang('language.ContactFirstName')">
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="item-content">
                                                <div class="item-media"><i
                                                            class="icon f7-icons">person</i></div>
                                                <div class="item-inner">
                                                    <div class="item-input">
                                                        <input required name="contactlastname" id="contactlastname" type="text" style="height:30px;padding-left:15px;" placeholder="@lang('language.ContactLastName')">
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="item-content">
                                                <div class="item-media"><i
                                                            class="icon f7-icons">email</i></div>
                                                <div class="item-inner">
                                                    <div class="item-input">
                                                        <input name="email" id="email" required type="text" style="height:30px;padding-left:15px;" placeholder="@lang('language.Email')">
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                        <!-- Country calling code -->
                                        <li>
                                            <div class="item-content">
                                                <div class="item-media"><i
                                                            class="icon f7-icons">phone</i></div>
                                                <div class="item-inner">
                                                    <div class="item-input" style="margin-bottom: -17px;">
                                                        <a href="#" id="autocomplete-calling-code" class="item-link item-content autocomplete-opener">
                                                            <input name="callingCode" popupid="autocomplete-calling-code" required type="hidden">
                                                            <div class="item-inner">
                                                                <div class="item-title" style="color: #767676;">@lang('language.CountryCallingCode')</div>
                                                                <div class="item-after"></div>
                                                            </div>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                        <!-- phone -->
                                        <li>
                                            <div class="item-content">
                                                <div class="item-media"><i
                                                            class="icon f7-icons">phone</i></div>
                                                <div class="item-inner">
                                                    <div class="item-input">
                                                        <input name="tel1" id="tel1" required type="text" style="height:30px;padding-left:15px;" placeholder="@lang('language.Phone')">
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                        <!-- Billing address -->
                                        <li>
                                            <div class="item-content">
                                                <div class="item-media"><i
                                                            class="icon f7-icons">home</i></div>
                                                <div class="item-inner">
                                                    <div class="item-input">
                                                        <input name="address" id="address" required type="text" style="height:30px;padding-left:15px;" placeholder="@lang('language.BillingAddress')">

                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                        <!-- City -->
                                        <li>
                                            <div class="item-content">
                                                <div class="item-media"><i
                                                            class="icon f7-icons">home</i></div>
                                                <div class="item-inner">
                                                    <div class="item-input">
                                                        <input name="city" id="city" required type="text" style="height:30px;padding-left:15px;" placeholder="@lang('language.City')">

                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                        <!-- Province / State -->
                                        <li>
                                            <div class="item-content">
                                                <div class="item-media">
                                                    <i
                                                            class="icon f7-icons">home</i></div>
                                                <div class="item-inner">
                                                    <div class="item-input">
                                                        <input name="province" id="province" required type="text" style="height:30px;padding-left:15px;" placeholder="@lang('language.ProvinceState')">

                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                        <!-- Country -->
                                        <li>
                                            <div class="item-content">
                                                <div class="item-media"><i
                                                            class="icon f7-icons">home</i></div>
                                                <div class="item-inner" style="margin-left: 10px;">
                                                    <div class="item-input" style="margin-bottom: 0px; border: 0px; height: 30px;">
                                                        <a href="#" id="autocomplete-billcountry-1"
                                                           class="item-link item-content autocomplete-opener">
                                                            <input name="country" popupid="autocomplete-billcountry-1" required style="height:30px;" type="hidden">
                                                            <div class="item-inner popuptext">
                                                                <div class="item-title" style="color: #767676;">@lang('language.Country')
                                                                </div>
                                                                <div class="item-after"></div>
                                                            </div>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>


                                        <!-- Post code -->
                                        <li>
                                            <div class="item-content">
                                                <div class="item-media"><i
                                                            class="icon f7-icons">home</i></div>
                                                <div class="item-inner">
                                                    <div class="item-input">
                                                        <input name="postcode" id="postcode" required type="text" style="height:30px;padding-left:15px;" placeholder="@lang('language.PostCode')">
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="card-footer">
                                <a></a><a href="#" class="button button-fill color-blue" id="ctsubmit">@lang('language.Save')</a>
                            </div>
                        </div>

                        <div class="index-footer" style="padding-left:13px; font-size: 12px; color: grey;padding-top: 20px;padding-right:14px;">
                            <span id="info"></span>
                        </div>

                    </div>
                </div>
            </div>





            <div class="toolbar">
                <div class="toolbar-inner">
                    <!-- Toolbar links -->
                    <a href="tel:1-866-255-2188" class="external">
                        <img src="dist/img/phone_u6.png" height="12" style="padding-right: 5px">1-866-255-2188
                    </a>

                    <a href="mailto:service@sinoramagroup.com" class="external">
                        <img src="dist/img/u115.png" height="12" style="margin-left: 0px;padding-right: 5px">service@sinoramagroup.com</a>

                    <!--
                    <a href="#" class="link"><img src="../dist/img/u145.png" height="17" style="margin-right: 3px"></a>
                    -->



                </div>
            </div>
        </div>
    </div>
    <input type="hidden" name="_token" id="csrf_token" value="{{csrf_token()}}">
@endsection

@section('extrascript')


<script>
    $$(document).on('DOMContentLoaded', function(){
        var lang="@lang('language.Lang')";


        <?php if(!isset($record)){ ?>
        tjq('#contactfirstname').val("<?php echo $resp["firstname"] ?>");
        tjq('#contactlastname').val("<?php echo $resp["lastname"] ?>");
        tjq('#email').val("<?php echo $resp["email"] ?>");

        tjq('#autocomplete-calling-code').find('.item-after').text("<?php echo $resp["callingcode"] ?>");
        tjq('#autocomplete-calling-code').find('input[name="callingCode"]').attr('value',"<?php echo $resp["callingcode"] ?>");


        tjq('#tel1').val("<?php echo $resp["phonenumber"] ?>");
        tjq('#address').val("<?php echo $resp["address"] ?>");
        tjq('#city').val("<?php echo $resp["city"] ?>");
        tjq('#province').val("<?php echo $resp["province"] ?>");
        tjq('#autocomplete-billcountry-1').find('.item-after').text('<?php echo $resp["country"] ?>');
        tjq('#autocomplete-billcountry-1').find('input[name="country"]').attr('value','<?php echo $resp["country"] ?>');

        tjq('#postcode').val("<?php echo $resp["postcode"] ?>");
        <?php } ?>




        function searchcountry(query,callingcode) {
            var results = [];
            for (var i = 0; i < countryCode.length; i++) {
                var countryitem = countryCode[i];

                if (countryitem.ISO_2_Code.toLowerCase().indexOf(query.toLowerCase()) >= 0 ||
                    countryitem.ISO_3_Code.toLowerCase().indexOf(query.toLowerCase()) >= 0 ||
                    countryitem.countryName.toLowerCase().indexOf(query.toLowerCase()) >= 0 ||
                    countryitem.countryName_CH.toLowerCase().indexOf(query.toLowerCase()) >= 0 ||
                    countryitem.countryName_PY.toLowerCase().indexOf(query.toLowerCase()) >= 0
                ) {
                    if(callingcode!=undefined){
                        var tmpstr = "( "+countryitem.callingCode + " ) " + countryitem.countryName;
                        if (lang != undefined && lang == "Chinese") {
                            tmpstr = "( "+countryitem.callingCode + " ) " + countryitem.countryName_CH;
                        }
                        results.push(tmpstr);

                    }else{
                        var tmpstr = countryitem.ISO_2_Code + "-" + countryitem.countryName;
                        if (lang != undefined && lang == "Chinese") {
                            tmpstr = countryitem.ISO_2_Code + "-" + countryitem.countryName_CH;
                        }
                        results.push(tmpstr);
                    }

                }
            }
            return results;
        }

        var callingcode=myApp.autocomplete({
            openIn: 'popup', //open in popup
            opener: $$('#autocomplete-calling-code'),//link that opens autocomplete
            backOnSelect: true, //go back after we select somethin,
            source: function (autocomplete, query, render) {
                var results = [];
                if (query.length === 0) {
                    render(results);
                    return;
                }// Render items by passing array with result items
                render(searchcountry(query,1));
            },
            onChange: function (autocomplete, value) {
                // Add item text value to item-after

                $$('#autocomplete-calling-code').find('.item-after').text(value[0]);
                $$('#autocomplete-calling-code').css('background-color', '#FFF');
                // Add item value to input value

                //var tmpstr=value[0].split(' ');
                //$$('#autocomplete-calling-code').find('input').val(tmpstr[1]);
                $$('#autocomplete-calling-code').find('input').val(value[0]);
            },
            onOpen:function(p){
                tjq('.autocomplete-popup').find('.close-popup').on('click',function(e){
                    e.stopPropagation();
                    callingcode.close();
                    return false;
                })
            }
        });

        var country1 = myApp.autocomplete({
            openIn: 'popup', //open in popup
            opener: $$('#autocomplete-billcountry-1'),//link that opens autocomplete
            backOnSelect: true, //go back after we select somethin,
            source: function (autocomplete, query, render) {
                var results = [];
                if (query.length === 0) {
                    render(results);
                    return;
                }// Render items by passing array with result items
                render(searchcountry(query));
            },
            onChange: function (autocomplete, value) {
                // Add item text value to item-after

                $$('#autocomplete-billcountry-1').find('.item-after').text(value[0].substr(0, 2));
                $$('#autocomplete-billcountry-1').css('background-color', '#FFF');
                // Add item value to input value
                $$('#autocomplete-billcountry-1').find('input').val(value[0].substr(0, 2));
            },
            onOpen:function(p){
                tjq('.autocomplete-popup').find('.close-popup').on('click',function(e){
                    e.stopPropagation();
                    country1.close();
                    return false;
                })
            }
        });


        tjq('#ctsubmit').click(function (e) {
            e.preventDefault();

            //use function
            //tjq('#book-submit-errors').html('');
            if (checkform('#contactdataform')==false) {
                //tjq('#book-submit-errors').html('<i class="fa fa-times"></i>' + " ");
                //return;
                alert("@lang('language.error1')");
            }
            else {

                //compact

                var newprofile = {};
                newprofile["firstname"] = tjq('#contactfirstname').val();
                newprofile["lastname"] = tjq('#contactlastname').val();
                newprofile["email"] = tjq('#email').val();
                newprofile["callingcode"] = tjq('input[name="callingCode"]').val();
                newprofile["phone"] = tjq('#tel1').val();
                newprofile["address"] = tjq('#address').val();
                newprofile["city"] = tjq('#city').val();
                newprofile["province"] = tjq('#province').val();
                newprofile["country"] = tjq('[name="country"]').val();
                newprofile["postcode"] = tjq('#postcode').val();
                console.log(newprofile);
                var _token = tjq("input[name=_token]").val();

                tjq.ajax({
                    type: "POST",
                    url:"<?php if($env== 'master') echo 'https://mo.air.sinorama.ca/'; else echo 'https://dev.mo.air.sinorama.ca/'; ?>contactinfo?action=update" ,
                    data: {data:newprofile, _token: _token},
                    success: function (response) {
                        console.log(response);
                        tjq('#info').html("@lang('language.uds')");

                    }
                })

            }
        });

        tjq('input[type="text"], input[type="email"]').on('focus', function () {
            var target = this;
            setTimeout(function(){
                target.scrollIntoViewIfNeeded();
                setTimeout(function(){
                    target.scrollIntoViewIfNeeded();
                    console.log('scrollIntoViewIfNeeded');
                },400);
            },300);
            tjq('.toolbar').hide();
        });

    });

    function checkform(obj){

        var passchecked = true;
        //var target=tjq('form#passengers');
        var thisclass = this;
        if(obj!=undefined){
            target=obj
        }
        tjq(target).find('input').each(function(idx,item){
            var val = getValFromObj(tjq(this));
            var showerrorobject = tjq(this);
            var attr=tjq(item).attr('required');
            //console.log(tjq(item).attr("popupid"));
            if(attr){
                if(tjq(item).val()=="" || tjq(item).val()==undefined){
                    if(tjq(item).attr("popupid")){
                        showformerror(tjq('#'+tjq(item).attr("popupid")));
                    }else{
                        showformerror(tjq(item));
                    }
                    passchecked=false;
                    var dataidx=tjq(item).parents("[dataidx]").attr("dataidx");
                    if(dataidx){
                        tjq('#passenger'+dataidx).find('i').html('close_round_fill');
                    }
                }

                if (tjq(this).attr('id') == 'email') {
                    console.log("Email:");
                    console.log(thisclass.isEmail(val));
                    if (!thisclass.isEmail(val)) {
                        passchecked = false;
                        thisclass.showRequired(showerrorobject);
                        showformerror(tjq(item));

                    } else {
                        thisclass.removeRequired(showerrorobject);
                        removeitem(tjq(item));
                    }
                }

                if (tjq(this).attr('id') == 'tel1') {
                    console.log("tel1:");
                    console.log(thisclass.checkTel(val));
                    if (!thisclass.checkTel(val)) {
                        passchecked = false;
                        thisclass.showRequired(showerrorobject);
                        showformerror(tjq(item));
                    } else {
                        thisclass.removeRequired(showerrorobject);
                        removeitem(tjq(item));
                    }
                }



                if (tjq(this).attr('id') == 'firstname')  {
                    console.log("firstname:");
                    console.log(thisclass.checkFirstName(val));
                    //console.log("hello");
                    if (!thisclass.checkFirstName(val)) {
                        passchecked = false;
                        thisclass.showRequired(showerrorobject);
                        showformerror(tjq(item));
                    } else {
                        thisclass.removeRequired(showerrorobject);
                        removeitem(tjq(item));
                    }
                }



                if (tjq(this).attr('id') == 'lastname') {
                    console.log("lastname");
                    console.log(thisclass.checkLastName(val));
                    if (!thisclass.checkLastName(val)) {
                        passchecked = false;
                        thisclass.showRequired(showerrorobject);
                        showformerror(tjq(item));
                    } else {
                        thisclass.removeRequired(showerrorobject);
                        removeitem(tjq(item));
                    }
                }


                if (tjq(this).attr('id') == 'contactfirstname') {
                    console.log("contactfirstname");
                    console.log(thisclass.checkFirstName(val));
                    if (!thisclass.checkFirstName(val)) {
                        passchecked = false;
                        thisclass.showRequired(showerrorobject);
                        showformerror(tjq(item));
                    } else {
                        thisclass.removeRequired(showerrorobject);
                        removeitem(tjq(item));
                    }
                }

                if (tjq(this).attr('id') == 'contactlastname') {
                    console.log("contactlastname");
                    console.log(thisclass.checkLastName(val));
                    if (!thisclass.checkLastName(val)) {
                        passchecked = false;
                        thisclass.showRequired(showerrorobject);
                        showformerror(tjq(item));
                    } else {
                        thisclass.removeRequired(showerrorobject);
                        removeitem(tjq(item));
                    }
                }


            }

        });
        return passchecked;
    }

    function showformerror(item){
        tjq(item).css('background','rgba(255, 45, 85, 0.11)');
    }

    function removeitem(items){
        tjq(items).css('background','#fff');
    }

</script>
@endsection