@extends('mobile.layouts.mainframe')

@section('content')

    <link rel="stylesheet" href="dist/css/search-app.css">
    <div class="views">
        <!-- Your main view, should have "view-main" class -->
        <div class="view view-main">

        @include('mobile.layouts.parts.navbar')

        <!-- Pages container, because we use fixed-through navbar and toolbar, it has additional appropriate classes-->
            <div class="pages toolbar-through navbar-through">

                <!-- Page, "data-page" contains page name -->
                <div data-page="myorders" class="page">
                    <!-- Scrollable page content -->
                    <div class="page-content">

                        <!-- Scrollable page content -->
                        <div id="orderinfojson" style="display: none;">{{$orderinfo}}</div>
                        <div id="orderlanguagedata" style="display: none;">{!! $langdata !!}</div>

                        <div id="noordertoshow" class="card" style="display:none;">
                            <div class="row no-gutter">
                                <div class="col-10"></div>
                                <div class="col-90 left-block">
                                    <h3> &nbsp;@lang("language.noordertoshow")</h3>
                                </div>
                            </div>
                        </div>

                        <div class="list" id="orderslist">

                        </div>

                        <!-- Row Header order ID    -->
                        <div id="order_item_template" class="card orderscd" style="padding:0px 5px;margin-bottom: 15px;display:none;" >
                            <div class="card-header" style="font-size: 12px;">
                                <div style="white-space:nowrap; vertical-align: middle" >
                                    <span id="ordersnumbertt">@lang("language.orderid")</span>
                                    <span id="ordersnumber" >T0M100020003217</span>
                                    <span style="padding-left: 12%"></span>
                                    <!-- Row Header order status    -->
                                    <img id="orderpaystatusimg"src="/dist/img/u42.png" alt="" style="height:14px;">
                                    <span style="padding-right: 5px;" id="orderpaystatus">未付款</span>
                                    <img id="orderbookstatusimg"src="/dist/img/u54.png" alt="" style="height:14px;">
                                    <span id="orderbookstatus">已取消</span>
                                </div>
                            </div>
                            <!--Row 1 Departure time-->
                            <div id="ordersegstart" class="row no-gutter" style="font-size: 13px; padding:4px 4px;">
                                <div class="col-60 left-block">
                                    <div class="row no-gutter" style="align-items: center">
                                        <div>
                                            <img src="dist/img/user_center/orderlist_departuretime.png" alt="" style="width:23px;margin-top:3px;">
                                        </div>
                                        <div class="col-80">
                                            <span class="orderDepartureTime">2018年6月21日14:50pm</span>
                                        </div>
                                    </div>
                                </div>
                                <!--Row 1 Passenger number-->
                                <div class="row" id="pricearea" style="font-size:12px; padding:8px 3px 3px 3px;">
                                    <!-- <a id="orderpsgdtl" onclick="show_order_passenger(1);" href=""> -->
                                    <img src="dist/img/u149.png" alt="" style="height:12px; padding:2px 2px 0 0 ">
                                    <span class="color-gray"  id="ordertotalperson"> 1人</span>

                                    <!--  </a>  -->
                                </div>
                            </div>


                            <!-- city or airport-->

                            <div class="row no-gutter" style="padding:5px 0px 8px 5px; text-align: center">

                                <div class="col-40" style="font-weight: bold" id="orderdeparture">蒙特利尔</div>
                                <div class="col-15">
                                    <img id="orderflighttypeimg" src="dist/img/search_list___departure/u701.png" alt="" style="width:24px;margin-top:3px;">
                                </div>
                                <div class="col-45" style="font-weight: bold" id="orderto">加尔各答</div>

                            </div>

                            <!-- order Total Fare -->
                            <div class="row" id="tripitinary">
                            </div>
                            <div class="card-footer no-gutter" style="padding:0px 5px;">

                                <div class="row no-gutter" style="align-items: baseline;white-space:nowrap;">
                                    <div class="col-30 color-gray" style="font-size: 12px">Total:</div>
                                    <div class="col-70 color-red" id="orderTotalFare">C$1277.29</div>
                                </div>

                                <!-- button -->

                                <div class="row no-gutter" style="align-items: baseline;">
                                    <a id="orderselectbutton" onclick="show_order_detail(1);" href="#" class="button button-fill color-blue" style="font-size: 13px; height: 30px;margin-top:3px; margin-right:15px;">详情</a>
                                    <a id="ordermodifybutton" href="#" class="button button-fill color-blue" style="font-size: 13px;height: 30px;margin-top:3px;margin-right:15px;">行程单</a>
                                    <a id="orderpaybutton" href="" class="button active color-ggobutton" style="font-size: 13px;height: 30px;margin-top:3px;">付款</a>
                                </div>


                            </div>
                        </div>

                    </div>
                    <div class="toolbar" style="font-size: 13px">
                        <div class="toolbar-inner">
                            <a href="#tab-allorder" id="tab-order-all" class="tab-link"
                               style="padding-left: 15px;">@lang('language.allorders')</a>
                            <a href="#tab-nopaidorder" id="tab-order-nopaid" class="tab-link">@lang('language.nopaidorders')</a>
                            <a href="#tab-paidorder" id="tab-order-paid" class="tab-link">@lang('language.paidorders')</a>
                            <a href="#tab-tickeorder" id="tab-order-ticket" class="tab-link"
                               style="padding-right: 15px">@lang('language.ticketorders')</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <input type="hidden" name="_token" id="csrf_token" value="{{csrf_token()}}">

    @include('mobile.layouts.parts.poporderdetail')

@endsection



@section('extrascript')
    <script>
        var orderinfos;
        var segstops=[];
        var returnsegstops=[];
        var langdata;
        var preventshowdetail=0;


        function datetoshortlocal(flighttime) {
            var localm = moment(flighttime);
            var s = "";
            if (lang == "English") {
                localm.locale('en');
                s = localm.format('MMM DD YYYY hh:mm');
            }
            else if (lang == "French") {
                localm.locale('fr');
                s = localm.format('DD MMM YYYY hh:mm');
            }
            else if (lang == "Chinese") {
                localm.locale('zh-cn');
                s = localm.format('LL hh:mm');
            }
            return s;
        }

        function odtravalitinary(obj1, obj2) {
            var lastarrivetime = undefined;
            var lastcity = undefined;
            var lastteminnal = undefined;

//console.log(tjq('#itilistfinal').css("display"));
//if(tjq('#itilistfinal').css("display")=="block")

            if (obj1 == "go") {
//if(tjq('#stoptemplatefinal').css("display")=="block")

//console.log(tjq('#odsegtemplatefinal').css("display"));
                var internarytemplate = tjq('#t_oditilistfinal').clone().prop("id", 'oditilistfinal');
                var rawstoptemplatefinal = tjq(internarytemplate).find('#odstoptemplatefinal').clone();
                var rawsegtemplatefinal = tjq(internarytemplate).find('#odsegtemplatefinal').clone();
            }
            if (obj1 == "goback") {
                var internarytemplate = tjq('#t_odreturnitilistfinal').clone().prop("id", 'odreturnitilistfinal');
                ;
                var rawstoptemplatefinal = tjq(internarytemplate).find('#odreturnstoptemplatefinal').clone();
                var rawsegtemplatefinal = tjq(internarytemplate).find('#odreturnsegtemplatefinal').clone();
            }

            var totalElapsedHours = 0;
            var totalElapsedMinutes = 0;
            var segcount = 0;

//var internarytemplate = tjq(rawinternarytemplate).clone();
            tjq.each(obj2, function (idx, seg) {
//console.log(seg);

                segcount++;
                var departdate = new Date(seg.DepartureDateTime + "Z");
                if (lastarrivetime != undefined) {
                    var stoptemplatefinal = tjq(rawstoptemplatefinal).clone();
                    tjq(stoptemplatefinal).css("display", "");
                    var diff = departdate.getTime() - lastarrivetime.getTime();
                    console.log(diff);
                    var diffhours = Math.floor(diff / 1000 / 60 / 60);
                    var diffminutes = Math.floor(diff / 1000 / 60) % 60;
                    totalElapsedHours = Number(totalElapsedHours)+ Number(diffhours);
                    totalElapsedMinutes = Number(totalElapsedMinutes)+ Number(diffminutes);

                    var chanageairport = "";
                    var changeterminal = "";

                    if (lang == "English") {
                        if (lastcity != seg.From) {
                            chanageairport = "，Go to another airport";
                        }
                        if (chanageairport == "" && lastteminnal != seg.From_TerminalID) {
                            changeterminal = "，Go to another terminal";
                        }
                        tjq(stoptemplatefinal).find('#odstoptime1').html(diffhours + "H" + diffminutes + "M");
                        tjq(stoptemplatefinal).find('#odstopdesc1').html('Transit in ' + getCityNameByCode(seg.From) + chanageairport + changeterminal);
                    }
                    else if (lang == "Chinese") {
                        if (lastcity != seg.From) {
                            chanageairport = "，去另一个机场";
                        }
                        if (chanageairport == "" && lastteminnal != seg.From_TerminalID) {
                            changeterminal = "，去另一个航空站";
                        }
                        tjq(stoptemplatefinal).find('#odstoptime1').html(diffhours + "小时" + diffminutes + "分钟");
                        tjq(stoptemplatefinal).find('#odstopdesc1').html('转机在' + getAirportNameByCodebak(seg.From) + '(' + getCityNameByCode(seg.From) + ')' + chanageairport + changeterminal);
                    }
                    else if (lang == "French") {
                        if (lastcity != seg.From) {
                            chanageairport = "，Aller à un autre aéroport";
                        }
                        if (chanageairport == "" && lastteminnal != seg.From_TerminalID) {
                            changeterminal = "，Aller à un autre terminal";
                        }
                        tjq(stoptemplatefinal).find('#odstoptime1').html(diffhours + "H" + diffminutes + "M");
                        tjq(stoptemplatefinal).find('#odstopdesc1').html('Transit dans ' + getCityNameByCode(seg.From) + chanageairport + changeterminal);
                    }

                    tjq(internarytemplate).append(tjq(stoptemplatefinal));
//console.log(tjq(stoptemplatefinal));
//console.log(tjq(rawinternarytemplate)[0].outerHTML);
                }

                if (seg.Airline != seg.OperatingAirline) {
//if (language == 'Chinese') {
//var operatingAirlineDetail = "由" + seg.OperatingAirline + "运营";
//}
//else {
                    var operatingAirlineDetail = "Operated by " + seg.OperatingAirline;
//}
                } else {
                    var operatingAirlineDetail = '';
                }


                var segtemplatefinal = tjq(rawsegtemplatefinal).clone();
                tjq(segtemplatefinal).css("display", "");
//console.log(seg.From);

                totalElapsedHours = Number(totalElapsedHours)+ Number(seg.ElapsedTimeTotalHours);
                totalElapsedMinutes = Number(totalElapsedMinutes)+ Number(seg.ElapsedTimeTotalHoursLeftMins);

//console.log(seg.DepartureDateTime);
                tjq(segtemplatefinal).find('#oddeparturedate1').text((seg.DepartureDateTime).substr(0, 10));
                tjq(segtemplatefinal).find('#oddeparttime2').text((seg.DepartureDateTime).substr(-8, 8));
//tjq('#oddepartdate').text(request.formatDateYMD(departdate).substr(5,5));
//tjq('#oddeparttime').text(request.formatAMPM(departdate));
                tjq(segtemplatefinal).find('#odairinfo').text(seg.OperatingAirline + seg.OperatingFlightNumber);
                tjq(segtemplatefinal).find('#odaircabine').text(seg.MarketingCabin);
                tjq(segtemplatefinal).find('#oditinerayduration').text(seg.ElapsedTimeTotalHours + 'H' + seg.ElapsedTimeTotalHoursLeftMins + 'M');
                tjq(segtemplatefinal).find('#odairtype').text(seg.AirEquipType);
//console.log(seg.ArrivalDateTime);
                tjq(segtemplatefinal).find('#odarriveddate').text((seg.ArrivalDateTime).substr(0, 10));
                tjq(segtemplatefinal).find('#odarrivedtime').text((seg.ArrivalDateTime).substr(-8, 8));

                if (lang == "English") {
                    tjq(segtemplatefinal).find('#oddepartureterminalport').text('Terminal：' + seg.From_TerminalID);
                    tjq(segtemplatefinal).find('#odarrivalport').text(getAirportNameByCode(seg.To) + '(' + seg.To + ')');
                    tjq(segtemplatefinal).find('#odarrivalterminalport').text('Terminal：' + seg.To_TerminalID);
                    tjq(segtemplatefinal).find('#oddepartureport').text(getAirportNameByCode(seg.From) + '(' + seg.From + ')');
                }
                else if (lang == "Chinese") {
                    tjq(segtemplatefinal).find('#oddepartureterminalport').text('Terminal(航空站)：' + seg.From_TerminalID);
                    tjq(segtemplatefinal).find('#odarrivalport').text(getAirportNameByCodebak(seg.To) + '(' + seg.To + ')');
                    tjq(segtemplatefinal).find('#odarrivalterminalport').text('Terminal(航空站)：' + seg.To_TerminalID);
                    tjq(segtemplatefinal).find('#oddepartureport').text(getAirportNameByCodebak(seg.From) + '(' + seg.From + ')');
                }
                else if (lang == "French") {
                    tjq(segtemplatefinal).find('#oddepartureterminalport').text('Terminal：' + seg.From_TerminalID);
                    tjq(segtemplatefinal).find('#odarrivalport').text(getAirportNameByCode(seg.To) + '(' + seg.To + ')');
                    tjq(segtemplatefinal).find('#odarrivalterminalport').text('Terminal：' + seg.To_TerminalID);
                    tjq(segtemplatefinal).find('#oddepartureport').text(getAirportNameByCode(seg.From) + '(' + seg.From + ')');
                }

                /*tjq('#odairline').text(seg.Airline+" "+seg.Flightnumber);
                tjq('#odcabinclass').text(seg.MarketingCabin);
                tjq('#odaircarft').text(seg.AirEquipType);

                tjq('#odhours').text(seg.ElapsedTimeTotalHours);
                tjq('#odmins').text(seg.ElapsedTimeTotalHoursLeftMins);
                tjq('#odopertator').text(operatingAirlineDetail);

                //tjq(tmpseg).find('#odairlinename').text(getAirlinesNameByCode(seg.Airline));
                tjq('#odairlinelogo').attr('src','https://air.sinorama.ca/assets/images/logo/'+ tjq.trim(seg.Airline) + '.png');


                */

                var arrivedate = new Date(seg.ArrivalDateTime + "Z");
                lastarrivetime = arrivedate;
                lastcity = seg.To;
                lastteminnal = seg.To_TerminalID;

                tjq(internarytemplate).append(tjq(segtemplatefinal));

            });

            totalElapsedHours = Number(totalElapsedHours)+ Math.floor(totalElapsedMinutes / 60);
            totalElapsedMinutes = totalElapsedMinutes % 60;
            var timestr;

            if ((lang == "English") || (lang == "French")) {
                timestr = totalElapsedHours + 'H ' + totalElapsedMinutes + 'M';
            }
            else if (lang == "Chinese") {
                timestr = totalElapsedHours + '小时' + totalElapsedMinutes + '分钟';
            }
            if (obj1 == "go") {
                tjq('#odtraveltime').text(timestr);
                tjq('#odtravelstops').text(segcount);
                console.log('template internary list');
                tjq('#oditilistcontainer').html(tjq(internarytemplate));
                tjq('#oditilistfinal').show();
            }
            if (obj1 == "goback") {
                tjq('#odreturntraveltime').text(timestr);
                tjq('#odreturntravelstops').text(segcount);
                tjq('#odreturnitilistcontainer').html(tjq(internarytemplate));
                tjq('#odreturnitilistfinal').show();
            }

//console.log(tjq(internarytemplate)[0].outerHTML);
//console.log(tjq(internarytemplate));
        }

        //  ---------------------------------------------------------------------
        function show_order_detail(ordernum) {
            var detail = orderinfos[ordernum];
            var segments = JSON.parse(detail.flightsegments);

            console.log("test");
            console.log(tjq('.timeline').length);
            tjq('#oditilistfinal').remove();
            tjq('#odreturnitilistfinal').remove();

//    tjq('#odETicketLink').hide();
            show_order_passenger(ordernum);
            myApp.showTab('#odtabitinery');

//    show_order_eticket(ordernum);

//    tjq("#odpassengerinfo").hide();
//    tjq("#oditinerayinfo").show();
            tjq("#odpoptitle").text(langdata["ItiDetails"]);

//var internarytemplate=tjq(rawinternarytemplate).clone();
            if (detail.triptype == "roundtrip") {
                tjq('#odreturnitinary').css("display", "");
                tjq('#odreturnitinary2').css("display", "");

                segs = segments.slice(0, segstops[ordernum]);
                resegs = segments.slice(segstops[ordernum], segstops[ordernum] + returnsegstops[ordernum]);

                if ((parseInt(tjq('.popup-order-detail .timeline').length) < 10)) {
                    odtravalitinary("go", segs);
                    odtravalitinary("goback", resegs);
                }
                tjq('#odroundpicture1').css("display", "");
                tjq('#odroundpicture2').css("display", "");
                tjq('#odsinglepicture1').css("display", "none");
                tjq('#odsinglepicture2').css("display", "none");
            }
            else if (detail.triptype == "oneway") {
                if ((parseInt(tjq('.popup-order-detail .timeline').length) < 8)) {
                    odtravalitinary("go", segments);
                }
                tjq('#odroundpicture1').css("display", "none");
                tjq('#odroundpicture2').css("display", "none");
                tjq('#odsinglepicture1').css("display", "");
                tjq('#odsinglepicture2').css("display", "");
                tjq('#odreturnitinary').css("display", "none");
                tjq('#odreturnitinary2').css("display", "none");
            }

            if ((lang == "English") || (lang == "French")) {
                tjq('#odHeadDeparture').text(getCityNameByCode(detail.departcity));
                tjq('#odHeadArrive').text(getCityNameByCode(detail.arrivalcity));
                tjq('#odHeadDeparture2').text(getCityNameByCode(detail.departcity));
                tjq('#odHeadArrive2').text(getCityNameByCode(detail.arrivalcity));
                tjq('#odDepartureDate').text((detail.departuredate).substr(0, 10));
                tjq('#odDeparturecity').text(getCityNameByCode(detail.departcity));
                tjq('#odArrivalcity').text(getCityNameByCode(detail.arrivalcity));

                if (detail.triptype == "roundtrip") {
                    tjq('#odreturndate').text((detail.return_departuredate).substr(0, 10));
                    tjq('#odreturndeparture').text(getCityNameByCode(detail.arrivalcity));
                    tjq('#odreturnarrival').text(getCityNameByCode(detail.departcity));
                }
            }
            else if (lang == "Chinese") {
                tjq('#odHeadDeparture').text(getCityNameByCodeChinese(detail.departcity));
                tjq('#odHeadArrive').text(getCityNameByCodeChinese(detail.arrivalcity));
                tjq('#odHeadDeparture2').text(getCityNameByCodeChinese(detail.departcity));
                tjq('#odHeadArrive2').text(getCityNameByCodeChinese(detail.arrivalcity));
                tjq('#odDepartureDate').text((detail.departuredate).substr(0, 10));
                tjq('#odDeparturecity').text(getCityNameByCodeChinese(detail.departcity));
                tjq('#odArrivalcity').text(getCityNameByCodeChinese(detail.arrivalcity));

                if (detail.triptype == "roundtrip") {
                    tjq('#odreturndate').text((detail.return_departuredate).substr(0, 10));
                    tjq('#odreturndeparture').text(getCityNameByCodeChinese(detail.arrivalcity));
                    tjq('#odreturnarrival').text(getCityNameByCodeChinese(detail.departcity));
                }
            }
            var resdetailcurrency;
            if (detail.currency == 'CAD') {
                resdetailcurrency = "C$";
            } else if (detail.currency == 'USD') {
                resdetailcurrency = "US$";
            } else if (detail.currency == 'EUR') {
                resdetailcurrency = "€";
            } else if (detail.currency == 'GBP') {
                resdetailcurrency = "£";
            } else if (detail.currency == 'RMB') {
                resdetailcurrency = "Y";
            }
            else {
                resdetailcurrency = "$";
            }
            tjq('#odCurrency').text(resdetailcurrency);
            tjq('#odTotalamount').text(detail.totalprice);
//tjq('#pricetotal').text(price.totalfee);
//tjq('#airtransportation').text(price.basefee);
//tjq('#taxtotalfee').text(price.taxfee);
//tjq('#interchangefee').text('0');

//    tjq('.popup-order-detail').find('#odinternarylist').replaceWith(tjq(internarytemplate));

            myApp.popup(".popup-order-detail");
        }

        /*function show_order_eticket(ordernum){
            tjq('#odETicketFrame').attr('src',orderinfos[ordernum]['lasturl']);
            tjq('#odETicketLink').attr('href',orderinfos[ordernum]['lasturl']);
        }*/

        function click_psg_detail(idx) {
            tjq('#odpsgitem' + idx + " .c_psgdetail").show();
            tjq('#odpsgshowdetail' + idx).attr("onClick", "close_psg_detail(" + idx + ");");
            tjq('#odpsgshowdetail' + idx + " .psgtoggle").html('chevron_up');
        }

        function close_psg_detail(idx) {
            tjq('#odpsgitem' + idx + " .c_psgdetail").hide();
            tjq('#odpsgshowdetail' + idx).attr("onClick", "click_psg_detail(" + idx + ");");
            tjq('#odpsgshowdetail' + idx + " .psgtoggle").html('chevron_down');
        }

        function show_order_passenger(ordernum) {
            var passenger = orderinfos[ordernum]["Passengers"];
            var contacter = orderinfos[ordernum]["Contact"];
//    tjq("#odpassengerinfo").show();
            tjq("#odpoptitle").text(langdata['PassengerDetails']);
//    tjq("#oditinerayinfo").hide();

            if ((lang == "English") || (lang == "French")) {
                tjq('#odHeadDeparture').text((orderinfos[ordernum]["departuredate"]).substr(0, 10) + " " + getCityNameByCode(orderinfos[ordernum].departcity));
                tjq('#odHeadArrive').text(getCityNameByCode(orderinfos[ordernum].arrivalcity));
            }
            else if (lang == "Chinese") {
                tjq('#odHeadDeparture').text((orderinfos[ordernum]["departuredate"]).substr(0, 10) + " " + getCityNameByCodeChinese(orderinfos[ordernum].departcity));
                tjq('#odHeadArrive').text(getCityNameByCodeChinese(orderinfos[ordernum].arrivalcity));
            }

            if (orderinfos[ordernum].triptype == "roundtrip") {
                tjq('#odroundpicture1').css("display", "");
                tjq('#odsinglepicture1').css("display", "none");
            }
            else if (orderinfos[ordernum].triptype == "oneway") {
                tjq('#odroundpicture1').css("display", "none");
                tjq('#odsinglepicture1').css("display", "");
            }


            tjq('#odpsglistcontainer').empty();
            var count = 1;
            tjq.each(passenger, function (idx, psg) {
                var template = tjq('#t_odpsgitem').clone().prop('id', 'odpsgitem' + idx);
                tjq(template).show();
                var imgsrc = "";

                if (psg['sex'] == 'M' && psg['type'] == 'ADT')
                    imgsrc = "dist/img/male.png";
                else if (psg['sex'] == 'F' && psg['type'] == 'ADT')
                    imgsrc = "dist/img/female.png";
                else if (psg['type'] == 'CNN')
                    imgsrc = "dist/img/child.png";
                else if (psg['type'] == 'INS')
                    imgsrc = "dist/img/baby.png";
                else if (psg['type'] == 'INF')
                    imgsrc = "dist/img/inff.png";

                tjq(template).find('#odpsgidx').prop('id', 'odpsgidx' + idx).html(count++);

                tjq(template).find('#odpsgicon').prop('id', 'odpsgicon' + idx).attr('src', imgsrc);
                tjq(template).find('#odpsgttname').prop('id', 'odpsgttname' + idx).html(psg['firstname'] + " " + psg['lastname']);

                tjq(template).find('#odpsgshowdetail').prop('id', 'odpsgshowdetail' + idx).attr("onClick", "click_psg_detail(" + idx + ");");

//            tjq(template).find('#odpsgttsex').prop('id', 'odpsgttsex' + idx).html(?langdata['Male']:langdata['Female']);

                tjq(template).find('#odpsgttbirthday').prop('id', 'odpsgttbirthday' + idx).html(psg['birthday']);
                tjq(template).find('#odpsgfirstname').prop('id', 'odpsgfirstname' + idx).html(psg['firstname']);
                tjq(template).find('#odpsgmiddlename').prop('id', 'odpsgmiddlename' + idx).html(psg['middle']);
                tjq(template).find('#odpsglastname').prop('id', 'odpsglastname' + idx).html(psg['lastname']);
                tjq(template).find('#odpsgsex').prop('id', 'odpsgsex' + idx).html(psg['sex'] == 'M' ? langdata['Male'] : langdata['Female']);
                tjq(template).find('#odpsgbirthday').prop('id', 'odpsgbirthday' + idx).html(psg['birthday']);
                tjq(template).find('#odpsgnationality').prop('id', 'odpsgnationality' + idx).html(psg['nationality']);
                tjq(template).find('#odpsgpassportnumber').prop('id', 'odpsgpassportnumber' + idx).html(psg['cno']);
                tjq(template).find('#odpsgissuingcountry').prop('id', 'odpsgissuingcountry' + idx).html(psg['ccountry']);
                tjq(template).find('#odpsgvaliditypassport').prop('id', 'odpsgvaliditypassport' + idx).html(psg['cexpired']);
                tjq(template).appendTo('#odpsglistcontainer');
            });
//    tjq("#odpassengerinfo").html("<br><br><br>" + JSON.stringify(passenger) + "<br><br><br>" + JSON.stringify(contacter));

            tjq("#odcontacthead").text(contacter["firstname"] + " " + contacter["lastname"]);
            tjq("#odcttemail").text(contacter["email"]);
            tjq("#odcttphone").text(contacter["tel1"]);
            tjq("#odcttbladdress").text(contacter["address"]);
            tjq("#odcttcity").text(contacter["city"]);
            tjq("#odcttcountry").text(contacter["country"]);
            tjq("#odcttpostecode").text(contacter["postcode"]);
            // show first two passengers information
            if(count < 3){
                click_psg_detail(0);
                click_psg_detail(1);
                console.log('click_psg_detail 1');
            }

        }

        //----------------------------------------------------------------------------------------------
        //----------------------------------------------------------------------------------------------
        //   Show item in order list

        function arrange(obj, num) {
//console.log(obj["time"]);

            var template = tjq('#order_item_template').clone().prop('id', 'order_item_template' + num);
            tjq(template).show();
            tjq(template).find('#ordersnumbertt').prop('id', 'ordersnumbertt' + num).html(langdata["orderid"]);
            tjq(template).find('#ordercreatetimett').prop('id', 'ordercreatetimett' + num).html(langdata["createtime"]);
            tjq(template).find('#ordersnumber').prop('id', 'ordersnumber' + num).html(obj["orderid"]);
            tjq(template).find('#ordercreatetime').prop('id', 'ordercreatetime' + num).html(datetoshortlocal(obj["createdatetime"]));

            tjq(template).find('#orderselectbutton').html(langdata["detail"]);
            tjq(template).find('#orderselectbutton').attr("onClick", "show_order_itinery(" + num + ");");
            tjq(template).find('#orderselectbutton').hide();
            tjq(template).find('#ordermodifybutton').hide();
            tjq(template).find('#orderpaybutton').attr("onClick", "click_order_pay(" + num + ");");
//    tjq(template).find('#orderpsgdtl').attr("onClick", "show_order_passenger(" + num + ");");


            if (obj["status"] == "canceled") {
                tjq(template).find('#orderbookstatus').prop('id', 'orderbookstatus' + num).html(langdata["canceled"]);
                tjq(template).find('#orderbookstatusimg').prop('id', 'orderbookstatusimg' + num)
                    .attr("src", "/dist/img/u54.png");
                if (obj["paystatus"] == "paid") {
                    tjq(template).find('#orderpaystatus').prop('id', 'orderpaystatus' + num)
                        .prop('class', 'color-grey').html(langdata["Paid"]);
                    tjq(template).find('#orderpaystatusimg').prop('id', 'orderpaystatusimg' + num)
                        .attr("src", "/dist/img/u53.png");
                }
                else {
                    tjq(template).find('#orderpaystatus').prop('id', 'orderpaystatus' + num)
                        .html(langdata["notpaid"]);
                    tjq(template).find('#orderpaystatusimg').prop('id', 'orderpaystatusimg' + num)
                        .attr("src", "/dist/img/u42.png");
                }
                tjq(template).find('#orderpaybutton').html(langdata["invalid"]);
                tjq(template).find('#orderpaybutton').attr("class", "button button-fill color-gray");

                if (obj["reordervalidation"] == "true") {
                    tjq(template).find('#ordermodifybutton').html(langdata["Reorder"]);
                    tjq(template).find('#ordermodifybutton').click(function () {
                        var url = obj["reorderurl"];
                        redirecttourl(url);
                        preventshowdetail=1;
                    });
                    tjq(template).find('#ordermodifybutton').hide();
//            tjq(template).find('#orderpaybutton').parent().hide();
//            tjq(template).find('#ordermodifybutton').parent().show();
                }
                else if (obj["reordervalidation"] == "false") {
                    tjq(template).find('#ordermodifybutton').hide();
                }
            }
            else if (obj["ticketstatus"] == "ticketed") {
                tjq(template).find('#orderbookstatus').prop('id', 'orderbookstatus' + num).prop('class', 'color-grey').html(langdata["ticketed"]);
                tjq(template).find('#orderbookstatusimg').prop('id', 'orderbookstatusimg' + num).attr("src", "/dist/img/u43.png");
                tjq(template).find('#orderpaystatus').prop('id', 'orderpaystatus' + num).prop('class', 'color-grey').html(langdata["Paid"]);
                tjq(template).find('#orderpaystatusimg').prop('id', 'orderpaystatusimg' + num).attr("src", "/dist/img/u53.png");


                tjq(template).find('#orderpaybutton').hide();
                tjq(template).find('#orderselectbutton').hide();

                tjq(template).find('#ordermodifybutton').html(langdata["E-ticket"]);
                tjq(template).find('#ordermodifybutton').show();
                tjq(template).find('#ordermodifybutton').click(function () {
                    var url = obj["lasturl"];
                    redirecttourl(url);
                    preventshowdetail=1;
                });
            }
            else if (obj["ticketstatus"] == "booked") {
                tjq(template).find('#orderbookstatus').prop('id', 'orderbookstatus' + num).prop('class', 'color-grey').html(langdata["reserved"]);
                tjq(template).find('#orderbookstatusimg').prop('id', 'orderbookstatusimg' + num).attr("src", "/dist/img/u43.png");

                if (obj["paystatus"] == "paid") {
                    tjq(template).find('#orderpaystatus').prop('id', 'orderpaystatus' + num).prop('class', 'color-grey').html(langdata["Paid"]);
                    tjq(template).find('#orderpaystatusimg').prop('id', 'orderpaystatusimg' + num).attr("src", "/dist/img/u53.png");
                    tjq(template).find('#orderpaybutton').html(langdata["Receipt"]);
                    tjq(template).find('#orderpaybutton').click(function () {
//redirect to receipt url
                        var url = obj["receipturl"];
                        redirecttourl(url);
                        preventshowdetail=1;
                    });
//                    tjq(template).find('#reservestatus').css('display',"inline");
                }
                else {
                    tjq(template).find('#orderpaystatus').prop('id', 'orderpaystatus' + num).html(langdata["notpaid"]);
                    tjq(template).find('#orderpaystatusimg').prop('id', 'orderpaystatusimg' + num).attr("src", "/dist/img/u42.png");
                    if (obj["paymentvalidation"] == "true") {
                        tjq(template).find('#orderpaybutton').html(langdata["topay"]);
                        tjq(template).find('#orderpaybutton').click(function () {
//redirect to payment url
                            var url = obj["paymenturl"];
                            redirecttourl(url);
                            preventshowdetail=1;
                        })
                    }
                    else {
//                tjq(template).find('#orderselectbutton').html();
                        tjq(template).find('#orderpaybutton').html(langdata["invalid"]);
                        tjq(template).find('#orderpaybutton').attr("class", "button button-fill color-gray");
//                tjq(template).find('#orderselectbutton').attr("class", "button button-fill color-blue");
                    }
                }
                tjq(template).find('#ordermodifybutton').html(langdata["Itinerysheet"]);
                tjq(template).find('#ordermodifybutton').show();
                tjq(template).find('#ordermodifybutton').click(function () {
                    //direct to Itinerary url
                    var url=obj["itenerayurl"];
                    redirecttourl(url);
                    preventshowdetail=1;
                });
            }
            else {
                tjq(template).find('#orderbookstatus').prop('id', 'orderbookstatus' + num).html(langdata["notbook"]);
                tjq(template).find('#orderbookstatusimg').prop('id', 'orderbookstatusimg' + num).attr("src", "/dist/img/u54.png");
                tjq(template).find('#orderpaystatus').prop('id', 'orderpaystatus' + num).html(langdata["notpaid"]);
                tjq(template).find('#orderpaystatusimg').prop('id', 'orderpaystatusimg' + num).attr("src", "/dist/img/u42.png");
                tjq(template).find('#orderpaybutton').html(langdata["invalid"]);
                tjq(template).find('#orderpaybutton').attr("class", "button button-fill color-gray");
            }

//tjq(template).find('#arriavaldate').html(obj["return_departuredate"].substring(0,10));
            tjq(template).find('#ordertotalperson').html(obj["passengers"] + langdata["P"]);
//tjq(template).find('#arriavaltime').html(obj["return_departuredate"].substring(11,16));
            if (obj["currency"] == "CAD")
                var currency = "C$";
            else if (obj["currency"] == "USD")
                var currency = "U$";
// tjq(template).find('#currency').html(currency);
//    tjq(template).find('#orderAvgFare').html(currency + Math.round(obj["totalprice"]*100/obj["passengers"])/100);
            tjq(template).find('#orderTotalFare').html(currency + obj["totalprice"]);
//    tjq(template).find('#orderperperson').html(langdata['PP']);

            var triptype = langdata[obj["triptype"]];
            tjq(template).find('#orderflighttype').html(triptype);
            // if (obj["triptype"] != "multiple") {
                if ((lang == "English") || (lang == "French")) {
                    tjq(template).find('#orderdeparture').html(getCityNameByCode(obj["departcity"]));
                    tjq(template).find('#orderto').html(getCityNameByCode(obj["arrivalcity"]));
                }
                else if (lang == "Chinese") {
                    tjq(template).find('#orderdeparture').html(getCityNameByCodeChinese(obj["departcity"]));
                    tjq(template).find('#orderto').html(getCityNameByCodeChinese(obj["arrivalcity"]));
                }

                tjq(template).find('#orderdeparturecode').text(obj["departcity"]);
                tjq(template).find('#ordertocode').text(obj["arrivalcity"]);
            // }

//  ------------------segment  ------------------
            var airlines = [];
            var triplist = JSON.parse(obj["flightsegments"]);
//console.log(JSON.parse(obj["flightsegments"]));
//console.log(triplist);
            var departuredate = triplist[0]["DepartureDateTime"];
            var departurearrivedate;

            var istops = 0;
            var destappeared = 0;
            var citypassed = "";
            var iretstops = 0
            var orderReturnDeptTime;
            var orderReturnTime;

            tjq.each(triplist, function (segidx, segval) {
//console.log(segval);
                if (destappeared == 0) {
                    istops++;

                    if (segval.To == obj["arrivalcity"]) {

                        destappeared = 1;

                        departurearrivedate = segval["ArrivalDateTime"];
                    }
                    else {
                        if ((lang == "English") || (lang == "French")) {
                            citypassed = getCityNameByCode(segval.To);
                        }
                        else if (lang == "Chinese") {
                            citypassed = getCityNameByCodeChinese(segval.To);
                        }
                    }
                }
                else {
                    if (iretstops == 0) {
                        orderReturnDeptTime = segval["DepartureDateTime"];
                    }
                    iretstops++;

                }
                if (airlines.indexOf(segval.Airline) == '-1') {
                    airlines.push(segval.Airline);
                }
                orderReturnTime = segval["ArrivalDateTime"];
            });

//console.log(airlines);
//tjq(template).find('#flightname').find('.airimg').remove();
            segstops.push(istops);
            returnsegstops.push(iretstops);

            var aircompany = '';
            var aircompanylogo = '';
//for (var m = 0; m < airlines.length; m++) {
            if (airlines.length == 1) {
                aircompanylogo = 'https://dev.air.sinorama.ca/flight_front/assets/images/logo/' + airlines[0] + '.png';
                aircompany = '<span href="#flighttypeDetail">' + getAirlinesNameByCode(airlines[0]) + '</span>';

            } else if (airlines.length > 1) {
                aircompanylogo = 'https://dev.air.sinorama.ca/flight_front/assets/images/logo/multi.png';
                /*  for (var m = 0; m < airlines.length; m++)
                {
                aircompany +='<span href="#flighttypeDetail" onload="loadWikipedia" wkey="'+getAirlinesNameByCode(airlines[m])+'" class="soap-popupbox">'+getAirlinesNameByCode(airlines[m])+'</span>'+'<br>';
                }*/
                aircompany = 'Multiply Airlines';
            }
//console.log(aircompany);
            tjq(template).find('.airlinelogog').prop('src', aircompanylogo);
//        tjq(template).find('#aircompany').html(aircompany);

            if (istops == 1) {
                tjq(template).find('#orderstops').html("");
                tjq(template).find('#ordermidway').html(langdata["Direct"]);
            }
            else if (istops == 2) {
                if (obj["triptype"] != "multiple") {
                    if ((lang == "English") || (lang == "French")) {
                        tjq(template).find('#ordermidway').html(getCityNameByCode(citypassed));
                        tjq(template).find('#orderstops').html(langdata['stopover']);
                    }
                    else if (lang == "Chinese") {
                        tjq(template).find('#ordermidway').html(getCityNameByCodeChinese(citypassed));
                        tjq(template).find('#orderstops').html(langdata['stopover']);
                    }
                }
            }
            else {
                tjq(template).find('#orderstops').html(istops - 1);
                tjq(template).find('#ordermidway').html(langdata['Stop']);
            }

            if ((lang == "English") || (lang == "French")) {
                tjq(template).find('#ordersegend .orderReturnTime').html(orderReturnTime.toString());
                tjq(template).find('#ordersegend .orderReturnDeptTime').html(orderReturnDeptTime);
            }
            else if (lang == "Chinese") {

            }

            tjq(template).find('#ordersegstart .orderDepartureTime').html(datetoshortlocal(departuredate));
            tjq(template).find('#ordersegstart .orderArrivalTime').html(datetoshortlocal(departurearrivedate));

            if (obj["triptype"] == "roundtrip") {
                tjq(template).find("#orderflighttypeimg").prop("src", "dist/img/search_list___departure/u701.png");
                tjq(template).find("#ordersegstart .orderinddepature").html(langdata["intdepart"]);
                tjq(template).find("#ordersegend .orderinddepature").html(langdata["intreturn"]);
                tjq(template).find("#ordersegmiddle").hide();
                tjq(template).find("#ordersegend").show();
                tjq(template).find('#ordersegend .orderReturnTime').html(datetoshortlocal(orderReturnTime));
                tjq(template).find('#ordersegend .orderReturnDeptTime').html(datetoshortlocal(orderReturnDeptTime));
            }
            else if (obj["triptype"] == "oneway") {
                tjq(template).find("#orderflighttypeimg").prop("src", "dist/img/search_list___departure/u702.png");
                tjq(template).find("#ordersegstart .orderinddepature").html(langdata["intstart"]);
                tjq(template).find("#ordersegmiddle").hide();
                tjq(template).find("#ordersegend").hide();
            }
            else {
                tjq(template).find("#orderflighttypeimg").prop("src", "dist/img/search_list___departure/u704.png");
                tjq(template).find("#ordersegstart .orderinddepature").html(langdata["intstep"] + 1);
                tjq(template).find("#ordersegmiddle .orderinddepature").html(langdata["intstep"] + 2);
                tjq(template).find("#ordersegmiddle").show();
                tjq(template).find('#ordersegmiddle .orderDepartureTime').html(datetoshortlocal(departuredate));
                tjq(template).find('#ordersegmiddle .orderArrivalTime').html(datetoshortlocal(departurearrivedate));

                for (var j = 2; j < istops; j++) {
                    var tdiv = tjq(template).find("#ordersegmiddle").clone().prop("id", "ordersegmiddle" + j);
                    tjq(tdiv).find(".orderinddepature").html(langdata["intstep"] + j + 1);
                    tjq(tdiv).find(".orderDepartureTime").html(datetoshortlocal(departuredate));
                    tjq(tdiv).find(".orderArrivalTime").html(datetoshortlocal(departurearrivedate));
                    tjq(tdiv).prepend(tjq(template).find("#ordersegend"));
                }
                tjq(template).find("#ordersegend").hide();
            }

            /*
            var itineraries=JSON.parse(obj["itineraries"]);
            if(obj["itineraries"]!=null)
            {
            var tripcontent='';
            //console.log(itineraries.length);
            //console.log(itineraries);
            //console.log(itineraries.length-1);
            for(var m=0;m<itineraries.length;m++)
            {

            var semi;
            if(m==(itineraries.length-1)){
            semi="";
            }
            else{
            if(m%2==1){
            semi=", <br>"
            }
            else{
            semi=", ";
            }

            }

            if((lang=="English")||(lang=="French"))
            tripcontent += '<span id="departurecity">'+getCityNameByCode(itineraries[m]["origin"])+'('+itineraries[m]["origin"]+')'+'</span>' + '<img id="singlearrow" style="display:inline; padding-left: 5px; padding-right: 5px; " src="assets/images/uptoarrow.png">' + '<span id="arrivalcity">'+getCityNameByCode(itineraries[m]["destination"])+'('+itineraries[m]["destination"]+')'+semi+'</span>';
            else if(lang=="Chinese")
            tripcontent += '<span id="departurecity">'+getCityNameByCodeChinese(itineraries[m]["origin"])+'('+itineraries[m]["origin"]+')'+'</span>' + '<img id="singlearrow" style="display:inline; padding-left: 5px; padding-right: 5px; " src="assets/images/uptoarrow.png">' + '<span id="arrivalcity">'+getCityNameByCodeChinese(itineraries[m]["destination"])+'('+itineraries[m]["destination"]+')'+semi+'</span>';
            }
            console.log(tripcontent);
            }
            tjq(template).find('#tripitinary').html(tripcontent);
            */

            tjq(template).attr('date', obj["date"]);


            tjq(template).prependTo('#orderslist');
        }

        function click_order_pay(idx) {
            preventshowdetail=1;
        }

        function redirecttourl(url) {
            window.open(url);
        }

        tjq(document).ready(function () {
            var node = document.getElementById('orderinfojson').innerText;
            orderinfos = eval(node);
            //        console.log('node:');console.log(node);
            node = document.getElementById('orderlanguagedata').innerText;
            //        console.log('langdata ori:');console.log(node);
            langdata = JSON.parse(node);
            //        console.log('langdata');    console.log(langdata);

            tjq('#tab-order-all').click(function () {
                filterOrderList('all');
            });

            tjq('#tab-order-nopaid').click(function () {
                filterOrderList('nopaid');
            });

            tjq('#tab-order-paid').click(function () {
                filterOrderList('paid');
            });

            tjq('#tab-order-ticket').click(function () {
                filterOrderList('ticket');
            });


            if (orderinfos.length == 0) {
                tjq('#noordertoshow').show();
                tjq('order_item_template').hide();
            }
            else {
                tjq('#orderslist').html("");
                for (var i = 0; i < orderinfos.length; i++) {
                    arrange(orderinfos[i], i);
                }
            }
        });

        function filterOrderList(type) {
            var numordertoshow = 0;

            $$('#noordertoshow').hide();
            for (var i = 0; i < orderinfos.length; i++) {
                if (type == "all") {
                    tjq("#order_item_template" + i).show();
                    numordertoshow++;
                }
                else if (type == "nopaid") {
                    if (orderinfos[i]["paystatus"] != "paid") {
                        tjq("#order_item_template" + i).show();
                        numordertoshow++;
                    }
                    else {
                        tjq("#order_item_template" + i).hide();
                    }
                }
                else if (type == "paid") {
                    if (orderinfos[i]["paystatus"] == "paid") {
                        tjq("#order_item_template" + i).show();
                        numordertoshow++;
                    }
                    else {
                        tjq("#order_item_template" + i).hide();
                    }
                }
                else if (type == "ticket") {
                    if (orderinfos[i]["ticketstatus"] == "ticketed") {
                        tjq("#order_item_template" + i).show();
                        numordertoshow++;
                    }
                    else {
                        tjq("#order_item_template" + i).hide();
                    }
                }
            }
            if (numordertoshow == 0) {
                $$('#noordertoshow').show();
            }
        }

        /*
        tjq('.subnavbar .button').on('click', function(e) {
            if(tjq(this).attr('href')=="#odtabeticket"){
                tjq('#odETicketLink').show();
            }
            else
                tjq('#odETicketLink').hide();
        });
        */

        tjq('body').on('click', '.orderscd', function() {
            if(preventshowdetail==0) {
                var ss = tjq(this).attr('id');
                var len = ss.length - 19;  //('order_item_template').length
                if (len > 0) {
                    id = ss.substr(19, len);
                    var num = parseInt(id);
                    show_order_detail(num);
                }
            }
            preventshowdetail=0;
        });

        /* load script for pages use ajax load   with for test@@qiankun
        myApp.onPageInit('myorders', function (page) {
        var node=document.getElementsByTagName('script');
        for(var i=0;i<node.length;i++) {
        var s = node[i];
        var sc = document.createElement('script');
        sc.type = 'text/javascript';
        sc.async = false;
        sc.src = 'path/to/javascript'+i+'.js';
        s.parentNode.insertBefore(sc, s);
        }
        });*/

        /*  v2
        $$(document).on('pageInit', function(e) {
        var page = e.detail.page;
        $$(page.container).find("script").each(function(el) {
        if ($$(this).attr('src')) {
        jQuery.getScript($$(this).attr('src'));
        }
        } else {
        eval($$(this).text());
        }
        });*/


        /*
        js code for orders.blade view
        */

    </script>
@endsection
