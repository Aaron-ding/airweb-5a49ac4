
@extends('mobile.layouts.mainframe')
@section('extracss')
    <!-- Path to your custom app styles-->
    <link rel="stylesheet" href="/dist/css/search-app.css">
@endsection
@section('content')

    <div class="views">
        <!-- Your main view, should have "view-main" class -->
        <div class="view view-main">

        @include('mobile.layouts.parts.navbar')

        <!-- Pages container, because we use fixed-through navbar and toolbar, it has additional appropriate classes-->
            <div class="pages toolbar-through navbar-through">

                <!-- Page, "data-page" contains page name -->
                <div data-page="policy" class="page">
                    <!-- Scrollable page content -->
                    <div class="page-content" style="background: #fff">
                        <h3 class="" style="text-align: center">
                            @lang("language.policyhead")
                        </h3>

                        <div class="card">
                            <div class="card-content">
                                <div class="card-content-inner">@lang("policy.text")</div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-5"></div>
                             <a href="" id="policydetailclose" class="col-90 button button-fill color-blue">
                                    @lang("language.Close")
                                </a>
                            <div class="col-5"></div>
                        </div>
                        <br>
                    </div>
                </div>
            </div>
            <input type="hidden" name="_token" id="csrf_token" value="{{csrf_token()}}">
            <div class="toolbar">
                <div class="toolbar-inner">
                    <!-- Toolbar links -->
                    <a href="tel:1-866-255-2188" class="external">
                        <img src="dist/img/phone_u6.png" height="12" style="padding-right: 5px">1-866-255-2188
                    </a>

                    <a href="mailto:service@sinoramagroup.com" class="external">
                        <img src="dist/img/u115.png" height="12" style="margin-left: 0px;padding-right: 5px">
                        service@sinoramagroup.com
                    </a>
                </div>
            </div>
        </div>

    </div>

@endsection


@section('extrascript')
<script>
    tjq("#policydetailclose").click(function (e) {
        window.history.back();
    });

</script>

@endsection