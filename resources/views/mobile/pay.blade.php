
@include('mobile.layouts.DnodeSyncClient')
<?php

//include('ft/common.php');
//include('ft/ft-head.php');

global $lang, $currency;



if (!empty($_REQUEST['orderid'])) {
    $orderid = $_REQUEST['orderid'];

} else {
    exit('Orderid is undefined.');
}

if (!empty($_REQUEST['valid'])) {
    $valid = base64_decode($_REQUEST['valid']);
    //echo $valid;
    if (time() >= $valid + 10800) {
        exit('This payment page has already been overdue.');
    }
} else {
    exit('Valid is undefined.');
}


if (empty($_REQUEST['exp']) || $_REQUEST['exp'] != 'c4ca4238a0b923820dcc509a6f75849b') {
    $weekOfToday = date('w');
    $hourOfToday = date('G');

    if ($weekOfToday == 6 || $weekOfToday == 0) {
        $closed = true;
    } else if ($hourOfToday >= 18 || $hourOfToday <= 9) {
        $closed = true;
    }
}



$weekOfToday = date('w');
$hourOfToday = date('G');
$closed = false;
//var_dump($weekOfToday);
//var_dump($hourOfToday);

if ($weekOfToday == 6 || $weekOfToday == 0) {
    $closed = true;
} else if ($hourOfToday >= 23 || $hourOfToday <= 14) {
    $closed = true;
}

if ($closed) {
    //exit('This payment page has already been overdue.');
}

if (!empty($_REQUEST['token'])) {
    $token = $_REQUEST['token'];

    if (!checkToken($orderid, $token)) {
        exit('Token is not valid.');
    }
} else {
    exit('Token is undefined.');
}

if($env == 'master')
    $host='search.air.sinorama.ca';
else
    $host = 'dev.search.air.sinorama.ca';
$port = '9998';

$para = array(
    array(
        "orderid" => $orderid
    )
);

$dnode = new \DnodeSyncClient\Dnode();

try {
    $connection = $dnode->connect($host, $port);
    $orderInformations = $connection->call('getOrderInfo', $para);
    $connection->close();
} catch (Exception $e) {
    print "Error!: " . $e->getMessage() . PHP_EOL;
}


$order = $orderInformations[0];
if ($order[0] == 'success') {
    $orderInfo = $order[1];
    if ($orderInfo['OrderInfo']['paystatus'] == 'paid'|| $orderInfo['OrderInfo']['paystatus'] == 'processing') {
        exit('This order has already been paid.');
    } elseif ($orderInfo['OrderInfo']['ticketstatus'] != 'booked') {
        exit('This order has not been booked.');
    } elseif ($orderInfo['OrderInfo']['status'] == 'canceled') {
        exit('This order has been canceled.');
    }

    $orderInfoJSONData = json_encode($order[1]);
    //var_dump($orderInfoJSONData);
    $contact = $orderInfo['Contact'];
    $Passengers = $orderInfo['Passengers'];
    $Itineraries = $orderInfo['Itineraries'];
    $OrderInfo = $orderInfo['OrderInfo'];
    $currency=$orderInfo['OrderInfo']['currency'];

    //if(($currency!="CAD")&&($currency!="USD"))
        //exit('We only accept Canadian Doller or American Doller.');

    $adultcount = $OrderInfo["adult"];
    $childrencount = $OrderInfo["child"];
    $infantsseatcount = $OrderInfo['baby'];
    $infantslapcount = $OrderInfo['babyinlap'];
    $personcount=$adultcount+$childrencount+$infantsseatcount+$infantslapcount;
    //var_dump($contact);

    //exit;
} else {
    exit('Processing error.');
}

function checkToken($orderid, $token) {
    $key = 'sinoramaWorld_02372785@!#d_air_API';

    return md5($key . $orderid) == $token;
}

?>
<?php
$ip = '';
if (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
    $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
} elseif (!empty($_SERVER['HTTP_CLIENT_IP'])) {
    $ip = $_SERVER['HTTP_CLIENT_IP'];
} else {
    $ip = $_SERVER['REMOTE_ADDR'];
}
?>



@extends('mobile.layouts.mainframe')
@section('extracss')
    <!-- Path to your custom app styles-->
    <link rel="stylesheet" href="dist/css/search-app.css">
    <link rel="stylesheet" href="css/skeuocard/styles/skeuocard.reset.css" />
    <link rel="stylesheet" href="css/skeuocard/styles/skeuocard.css" />

    <style>
        .skeuocard.js .cc-number input, .skeuocard.js .cc-name {
            display: inline-block !important;
            height: auto !important;
            width: auto !important;
        }
        .skeuocard.js .cc-exp input {
            display: inline-block !important;
            height: auto !important;
        }
    </style>

@endsection


@section('content')
    <!-- Views -->
    <div class="views">
        <!-- Your main view, should have "view-main" class -->
        <div class="view view-main">

            @include('mobile.layouts.parts.navbar')

        <!-- Pages container, because we use fixed-through navbar and toolbar, it has additional appropriate classes-->
            <div class="pages toolbar-through navbar-through">
                <!-- Page, "data-page" contains page name -->
                <div data-page="index" class="page search">
                    <!-- Scrollable page content -->
                    <div class="page-content">

                        <!-- Search condition review  -->
                        <div class="card">
                            <div class="card-content">
                                <div class="card-footer">
                                    <h3 style="margin:0px;">
                                        <span id="headdeparture"></span>
                                        <img id="roundpicture" src="../dist/img/search_list___departure/return_u321.png"
                                             style="margin-left:20px;margin-right: 20px;display:none;">
                                        <img id="singlepicture"  src="../dist/img/baggages/u1391.png" style="margin-left:20px;margin-right: 20px;display:none;">
                                        <span id="headarrive"></span>
                                    </h3>
                                    <div class="f7-demo-icon" id="searchcard">
                                        <i class="f7-icons" style="color: #1d2088;">search</i>
                                    </div>
                                </div>
                                <div class="card-footer">
                                    <span id="daterange"></span><br>
                                    <span></span>
                                </div>
                                <!--
                                <div class="card-footer">
                                    <span id="persondesc"></span>
                                    <span></span>
                                </div>

                                <div class="card-footer">
                                    <div class="trip">Base Fee:</div>
                                    <div class="total">
                                    <h2 style="margin-top: 0px;margin-bottom: 0px;margin-left: 0px;color:#c30d23">
                                        <span style="font-size:12px;color:grey;margin-left: 0px;">$</span><span id="basefare"></span>

                                    </h2>
                                    </div>
                                    <div style="padding-left: 35%">

                                    </div>
                                </div>
                                <div class="card-footer">
                                    <div class="trip">Tax Fee:</div>
                                  <div class="total">
                                      <h2 style="margin-top: 0px;margin-bottom: 0px;color:#c30d23">
                                      <span style="font-size:12px;color:grey;">$</span><span id="taxfee"></span>

                                     </h2>
                                  </div>
                                    <div style="padding-left: 35%">

                                    </div>
                                </div>-->
                                <div class="card-footer">
                                    <div class="trip">@lang('language.TripTotal')</div>
                                    <div class="total">
                                        <h2 style="margin-top: 0px;margin-bottom: 0px;color:#c30d23">
                                            <span style="font-size:12px;color:grey;" id="currency"></span><span style="font-size:19px;" id="totalamount"></span>

                                        </h2>
                                    </div>
                                    <div style="padding-left: 15%" id="showusdetailpay">
                                        <a href="#" > @lang('language.ShowDetails')
                                            <img src="../dist/img/search_list___departure/u469.png" width="7" height="7">
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="" style="padding-left:8px; font-size: 12px; color: grey;">
                        <span>@lang('language.State')
                        </span>
                        </div>
                        <!-- departure text describe
                        <div class="content-block  desc-departure" style=" margin-top: 50px;margin-bottom: -15px;">
                            <div class="content-block-title" style="color:#1D2088;" id="selecttitle">
                                <strong id="departdesc"></strong>
                                <strong>, to </strong>
                                <strong id="destdesc"></strong>
                            </div>
                            <div class="content-block-title desc" style="padding-left:8px;">
                                <p>Prices is the average, include taxes and other fees.</p>
                            </div>
                            <div class="content-block"></div>
                        </div>

                        <div class="content-block desc-arrive" style="    margin-top: 50px;margin-bottom: -5px;">
                            <div class="content-block-title" style="color:#1D2088;" id="selecttitle">
                                <strong id="returndepartdesc"></strong>
                                <strong>, to </strong>
                                <strong id="returndestdesc"></strong>
                            </div>

                            <div class="content-block"></div>
                        </div>F
-->
                        <!-- passenger -->
                        @include('mobile.layouts.parts.internarydesc')

                        <div class="content-block " style="    margin-top: 50px;margin-bottom: -15px;">
                            <div class="content-block-title" style="color:#1D2088;" id="selecttitle">
                                <strong>@lang('language.State1') </strong>
                            </div>
                            <div class="content-block-title desc" style="padding-left:8px;">
                                <p>
                                    <span id="adultcount"></span>@lang('language.Adult'), <span id="childcount"></span> @lang('language.Children') , <span id="infantcount"></span> @lang('language.Infant')
                                </p>
                            </div>

                            <div class="content-block"></div>
                        </div>

                        <form id="passengers">

                            <div class="card" style="padding-bottom: 20px;">


                                @for($i=1;$i<=$personcount;$i++)
                                    <?php


                                    if ($i <= $adultcount) {
                                        $persontype = 'ADT';
                                        if ($i <= $infantslapcount) {
                                            $persontitle = 'Adult who carries an infant in lap';
                                        } else {
                                            $persontitle = 'Adult';
                                        }
                                    } else if (($i > $adultcount) && ($i <= (intval($adultcount) + intval($childrencount)))) {
                                        $persontype = 'CNN';
                                        $persontitle = 'Child';
                                    } else if (($i > (intval($adultcount) + intval($childrencount))) && ($i <= (intval($adultcount) + intval($childrencount) + intval($infantsseatcount)))) {
                                        $persontype = 'INS';
                                        $persontitle = 'Infant in seat';
                                    } else {
                                        $persontype = 'INF';
                                        $persontitle = 'Infant in lap';
                                    }
                                    ?>



                                    <div class="row" dataidx="{{$i}}" >
                                        <div class="col-10"></div>
                                        <div class="col-80">
                                            <a id="passenger{{$i}}" style="margin-top:20px;background: #007aff!important;border-color:#007aff;letter-spacing: 2px;" href="#" class="button button-round active passengerbutton" >Passenger {{$i}}

                                                <span style="position: absolute;right: 10px;margin-top:2px;">
                                                    <i class="f7-icons">chevron_right</i>
                                                </span>
                                            </a>
                                        </div>
                                        <div class="col-10"></div>
                                    </div>


                                    <div class="popup popup-passenger-{{$i}} person-information" style="background: #efeff4"  dataidx="{{$i}}" >
                                        <div class="page ">
                                            <div class="page-content">
                                                <div class="navbar" style="position: fixed; top: 0; left: 0; z-index: 999;">
                                                    <div class="navbar-inner">
                                                        <div class="left sliding">
                                                            <a href="#" class="link close-popup"  dataidx="{{$i}}" >
                                                                <i class="f7-icons">chevron_left</i>
                                                                <span class="">Close</span>
                                                            </a>
                                                        </div>
                                                        <div class="right sliding">
                                                            <a href="#" class="link save-popup"   dataidx="{{$i}}"  >
                                                                <span >Save</span>
                                                                <i class="f7-icons">chevron_right</i></a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="card travellers-card" style="margin-top: 50px;">
                                                    <div class="card-content">
                                                        <div class="list-block">
                                                            <ul>

                                                                <p style="margin-left: 20px;font-size: 14px;color: #55a6ff;"><?php echo $persontitle?></p>
                                                                <div  id="passengertype-{{$i}}" style="display:none;"><?php echo $persontype ?></div>


                                                                <!-- Text inputs -->
                                                                <li>
                                                                    <div class="item-content">
                                                                        <div class="item-media">
                                                                            <i class="icon f7-icons">person</i>
                                                                        </div>
                                                                        <div class="item-inner">
                                                                            <div class="item-input">
                                                                                <span name="firstname" id="firstname" type="text" required style="height:30px;padding-left:10px;" placeholder="Frist name ( * ) "></span>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </li>
                                                                <li>
                                                                    <div class="item-content">
                                                                        <div class="item-media">
                                                                            <i class="icon f7-icons">person</i>
                                                                        </div>
                                                                        <div class="item-inner">
                                                                            <div class="item-input">
                                                                                <span name="middlename" id="middlename" type="text" style="height:30px;padding-left:10px;"  placeholder="Middle name"></span>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </li>
                                                                <li>
                                                                    <div class="item-content">
                                                                        <div class="item-media">
                                                                            <i class="icon f7-icons">person</i>
                                                                        </div>
                                                                        <div class="item-inner">
                                                                            <div class="item-input">
                                                                                <span name="lastname" id="lastname" type="text" style="height:30px;padding-left:10px;" required placeholder="Last name ( * ) "></span>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </li>
                                                                <!-- gender Select -->
                                                                <li>
                                                                    <div class="item-content">
                                                                        <div class="item-media">
                                                                            <i class="icon f7-icons">person</i>
                                                                        </div>
                                                                        <div class="item-inner">
                                                                            <div class="item-input">
                                                                                <span name="gender" id="gender" required style="padding-left:10px;"></span>                                                                                                                                                                   </span>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </li>

                                                                <!-- Date -->
                                                                <li>
                                                                    <div class="item-content">
                                                                        <div class="item-media"><i class="icon f7-icons">calendar</i>
                                                                        </div>
                                                                        <div class="item-inner">
                                                                            <div class="item-title label" id="birthflag" style="color: #767676;padding-left:10px;">Birthday</div>
                                                                            <div class="item-input">
                                                                                <span name="birthday" id="birthday" type="date" required style="height:30px;padding-left:10px;" placeholder="Birth date ( * ) " value="1999-04-30"></span>
                                                                            <!--
                                                                                 readonly id="calendar-birthday-{{$i}}"
                                                                                 -->
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </li>
                                                                <!-- nationality Select -->
                                                                <style>
                                                                    .popuptext::after{
                                                                        background-color:#fff!important;
                                                                    }
                                                                </style>

                                                                <li>
                                                                    <div class="item-content">
                                                                        <div class="item-media">
                                                                            <i class="icon f7-icons">world</i>
                                                                        </div>
                                                                        <div class="item-inner" style="margin-left: 10px;">
                                                                            <div class="item-title label" style="color: #767676;padding-left:10px;">Nationality ( * )</div>
                                                                            <div class="item-input">
                                                                                <span name="nationality" type="text" id="nationality"
                                                                                      placeholder="Nationality ( * )" required style="height:30px;padding-left:10px;"></span>
                                                                            </div>
                                                                        </div>

                                                                    </div>
                                                                </li>
                                                                <!-- Passport Number-->
                                                                <li>
                                                                    <div class="item-content">
                                                                        <div class="item-media">
                                                                            <i class="icon f7-icons">document_text</i>
                                                                        </div>
                                                                        <div class="item-inner" style="margin-left: 10px;">
                                                                            <div class="item-title label" style="color: #767676;padding-left:10px;">Passport NO.</div>
                                                                            <div class="item-input">
                                                                                <span name="passport" type="text" id="passport"
                                                                                      placeholder="Passport Number ( * )" required style="height:30px;padding-left:10px;"></span>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </li>

                                                                <!-- passport validity -->
                                                                <li>
                                                                    <div class="item-content">
                                                                        <div class="item-media">
                                                                            <i class="icon f7-icons">document_text</i>
                                                                        </div>
                                                                        <div class="item-inner" style="margin-left: 10px;">
                                                                            <div class="item-title label" style="color: #767676;padding-left:10px;">Validity of passport ( * )</div>
                                                                            <div class="item-input">
                                                                                <span name="passportexpired"  type="text" id="passportexpired"
                                                                                      placeholder="Validity of passport ( * )" required style="height:30px;padding-left:10px;"></span>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </li>


                                                                <!-- Issuing country Select -->
                                                                <li>
                                                                    <div class="item-content">

                                                                        <div class="item-media">
                                                                            <i class="icon f7-icons">document_text</i>
                                                                        </div>
                                                                        <div class="item-inner" style="margin-left: 10px;">
                                                                            <div class="item-title label" style="color: #767676;padding-left:10px;">Issuing country ( * )</div>
                                                                            <div class="item-input">
                                                                                <span name="passportcountry"  type="text" id="passportcountry"
                                                                                      placeholder="Issuing country ( * )" required style="height:30px;padding-left:10px;"></span>
                                                                            </div>
                                                                        </div>

                                                                    </div>
                                                                </li>
                                                                <li id="showotheroptons" style="padding-bottom: 10px;padding-top: 10px;">
                                                                    <span style="margin-left: 20px;font-size: 14px;color: #55a6ff;">Other options ( optonal ) </span>
                                                                    <img src="../dist/img/search_list___departure/u469.png" style="margin-left: 5px">
                                                                </li>

                                                                <li class="optional" id="flyinfo" style="display: none">
                                                                    <div class="item-content" name="flyerplan" id="flyerplan" >

                                                                        <div class="item-inner">
                                                                            <div class="item-title label" style="width: 45%">POINTS PLAN</div>
                                                                            <div class="item-input">
                                                                                <span name="flyernumber" type="text" id="flyernumber"
                                                                                      placeholder="AMERICAN AIRLINES"></span>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </li>

                                                                <li class="optional" style="display: none">
                                                                    <div class="item-content">
                                                                        <div class="item-title label" style="width: 45%">MEAL REQUEST</div>
                                                                        <div class="item-inner">
                                                                            <div class="item-input">
                                                                                <span name="mealpreference" id="mealpreference" class="full-width"></span>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </li>

                                                                <li class="optional" style="display: none">
                                                                    <div class="item-content">
                                                                        <div class="item-title label" style="width: 45%">OTHER REQUEST</div>
                                                                        <div class="item-inner">
                                                                            <div class="item-input">
                                                                                <span name="ssr" id="ssr" class="full-width"></span>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </li>

                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                            @endfor

                            <!-- Contact -->
                                <div class="row">
                                    <div class="col-10"></div>
                                    <div class="col-80">
                                        <a id="passengercontact" style="margin-top:20px;background: #007aff!important;border-color:#007aff;letter-spacing: 2px;" href="#" class="button button-round active">Contact
                                            <span style="position: absolute;right: 10px;">
                                                <i class="f7-icons">chevron_right</i>
                                            </span>
                                        </a>
                                    </div>
                                    <div class="col-10"></div>

                                </div>


                                <div class="popup popup-passenger-contact person-information" style="background: #efeff4" dataidx="contact" >
                                    <div class="page ">
                                        <div class="page-content">
                                            <div class="navbar" style="position: fixed;top: 0;left: 0;z-index: 999;">
                                                <div class="navbar-inner">
                                                    <div class="left sliding">
                                                        <a href="#" dataidx="contact" class="link close-popup"><i class="f7-icons">chevron_left</i><span
                                                                    class="">Close</span></a>
                                                    </div>
                                                    <div class="right sliding">
                                                        <a href="#"  dataidx="contact" class="link save-popup"><span class="">Save</span><i class="f7-icons">chevron_right</i></a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="card travellers-card" style="margin-top: 50px;">
                                                <div class="card-content">
                                                    <div class="list-block">
                                                        <ul>
                                                            <!-- Text inputs -->
                                                            <li>
                                                                <div class="item-content">
                                                                    <div class="item-media"><i
                                                                                class="icon f7-icons">person</i></div>
                                                                    <div class="item-inner">
                                                                        <div class="item-input">
                                                                            <span required name="contactfirstname" id="contactfirstname" type="text" style="height:30px;padding-left:15px;" placeholder="Contact frist name ( * )"></span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </li>
                                                            <li>
                                                                <div class="item-content">
                                                                    <div class="item-media"><i
                                                                                class="icon f7-icons">person</i></div>
                                                                    <div class="item-inner">
                                                                        <div class="item-input">
                                                                            <span required name="contactlastname" id="contactlastname" type="text" style="height:30px;padding-left:15px;" placeholder="Contact last name ( * )"></span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </li>
                                                            <li>
                                                                <div class="item-content">
                                                                    <div class="item-media"><i
                                                                                class="icon f7-icons">email</i></div>
                                                                    <div class="item-inner">
                                                                        <div class="item-input">
                                                                            <span name="email" id="email" required type="text" style="height:30px;padding-left:15px;" placeholder="Email ( * )"></span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </li>
                                                            <!-- Country calling code -->
                                                            <!-- phone -->
                                                            <li>
                                                                <div class="item-content">
                                                                    <div class="item-media"><i
                                                                                class="icon f7-icons">phone</i></div>
                                                                    <div class="item-inner">
                                                                        <div class="item-input">
                                                                            <span name="tel1" id="tel1" required type="text" style="height:30px;padding-left:15px;" placeholder="Phone ( * )"></span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </li>
                                                            <!-- Billing address -->
                                                            <li>
                                                                <div class="item-content">
                                                                    <div class="item-media"><i
                                                                                class="icon f7-icons">home</i></div>
                                                                    <div class="item-inner">
                                                                        <div class="item-input">
                                                                            <span name="address" id="address" required type="text" style="height:30px;padding-left:15px;" placeholder="Billing address ( * )"></span>

                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </li>
                                                            <!-- City -->
                                                            <li>
                                                                <div class="item-content">
                                                                    <div class="item-media"><i
                                                                                class="icon f7-icons">home</i></div>
                                                                    <div class="item-inner">
                                                                        <div class="item-input">
                                                                            <span name="city" id="city" required type="text" style="height:30px;padding-left:15px;" placeholder="City ( * )"></span>

                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </li>
                                                            <!-- Province / State -->
                                                            <li>
                                                                <div class="item-content">
                                                                    <div class="item-media">
                                                                        <i
                                                                                class="icon f7-icons">home</i></div>
                                                                    <div class="item-inner">
                                                                        <div class="item-input">
                                                                            <span name="province" id="province" required type="text" style="height:30px;padding-left:15px;" placeholder="Province / State ( * )"></span>

                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </li>
                                                            <!-- Country -->
                                                            <li>
                                                                <div class="item-content">
                                                                    <div class="item-media"><i
                                                                                class="icon f7-icons">home</i></div>
                                                                    <div class="item-inner" style="margin-left: 10px;">
                                                                        <div class="item-input" style="margin-bottom: 0px; border: 0px; height: 30px;">
                                                                            <a href="#" id="autocomplete-billcountry-{{$i}}"
                                                                               class="item-link item-content autocomplete-opener">
                                                                                <span name="country" id="country" required style="height:30px;" type="hidden">
                                                                                <div class="item-inner popuptext">
                                                                                    <div class="item-title" style="color: #767676;">Country ( * )
                                                                                    </div>
                                                                                    <div class="item-after"></div>
                                                                                </div></span>
                                                                            </a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </li>

                                                            <!--
                                                            <li>
                                                                <div class="item-content">
                                                                    <div class="item-media">
                                                                        <i
                                                                                class="icon f7-icons">world</i>
                                                                    </div>

                                                                    <div class="item-inner" style="margin-left: 10px;">
                                                                        <div class="item-input" style="margin-bottom: 0px; border: 0px; height: 30px;">
                                                                            <a href="#" id="autocomplete-calling-code" class="item-link item-content autocomplete-opener">

                                                                                <input name="callingCode" popupid="autocomplete-calling-code" required style="height:30px;" type="hidden">
                                                                                <div class="item-inner popuptext">
                                                                                    <div class="item-title" style="color: #767676;margin-right:0px;margin-left: 0px;">Nationality ( * )
                                                                                    </div>
                                                                                    <div class="item-after"></div>
                                                                                </div>
                                                                            </a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </li>
                                                            -->
                                                            <!--   <li>
                                                                   <div class="item-content">
                                                                       <div class="item-media"><i
                                                                                   class="icon f7-icons">phone</i></div>
                                                                       <div class="item-inner">
                                                                           <div class="item-input" style="margin-bottom: -17px;">
                                                                               <a href="#" id="autocomplete-calling-code" class="item-link item-content autocomplete-opener">
                                                                                   <input name="callingCode" popupid="autocomplete-calling-code" required type="hidden">
                                                                                   <div class="item-inner">
                                                                                       <div class="item-title" style="color: #767676;">Country calling code ( * )
                                                                                       </div>
                                                                                       <div class="item-after"></div>
                                                                                   </div>
                                                                               </a>
                                                                           </div>
                                                                       </div>
                                                                   </div>
                                                               </li>

                                                           -->

                                                            <!-- Post code -->
                                                            <li>
                                                                <div class="item-content">
                                                                    <div class="item-media"><i
                                                                                class="icon f7-icons">home</i></div>
                                                                    <div class="item-inner">
                                                                        <div class="item-input">
                                                                            <span name="postcode" id="postcode" required type="text" style="height:30px;padding-left:15px;" placeholder="Post code ( * )"></span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </form>

                        <div class="demo-progressbar-inline" id="progressbar" style="display: none;">
                            <p><span data-progress="50" class="progressbar" id="probar" style="height:9px;"></span></p>
                            <p style="color:#1D2088;">The reservation is being issued, please wait with patience.</p>
                        </div>
                        <!-- button -->
                        <div class="content-block" style="margin-top:30px;margin-button:30px;display:none;">
                            <a href="#" id="submitorder" class="button button-fill" style="word-spacing: 10px;letter-spacing: 2px;" >Submit Order </a>
                        </div>

                        <!-- pay -->
                        <div id="divpayment" class="content-block"  >

                            <div class="" style="color:grey;margin-bottom:10px;">
                                <p style="color: #1D2088;"><strong>@lang('language.reserveinfo1')</strong></p>
                                <p id="pricechange" style="display:none; font-size: 12px;">@lang('language.reserveinfo2')
                                    <span id="formerfee"  style="color:red"></span>
                                    @lang('language.Too')
                                    <span id="updatefee" style="color:red"></span>.
                                    <br>@lang('language.reserveinfo3') </p>

                                <p style="color: #1D2088;">@lang('language.reserveinfo4')</p>
                            </div>
                            <div id="couponForm" style="display: inline;">
                                <hr>
                                <h3 style="margin:15px;">@lang('language.Coupon')</h3>
                                <div class="row" style="padding-bottom:10px;">
                                    <div class="col-60 "> <input type="text" id="couoncode" name="couoncode" style="width:-webkit-fill-available; height:30px; padding-left:3px;" value="" placeholder="" data-stripe="number"/>
                                    </div>
                                    <div class="col-40 " style=" padding-top: 5px;"><a href="#" id="buttonverify" class="button button-round active" style="background: #007aff!important;border-color:#007aff;">Verify coupon</a>
                                    </div>
                                </div>
                                <div class="col-70 " style="padding-bottom:15px;">
                                    <span id="coupon_info_success" style="color:green;display:none"><i class="fa fa-check"></i>@lang('language.Verifyinfo1')<span id="coupon_amt">CAD 50</span></span>
                                    <span id="coupon_info" style="color:red;display:none"><i class="fa fa-times"></i>@lang('language.Verifyinfo2')<span id="coupon_extra_error"></span></span>
                                </div>
                            </div>

                            <div class="list-block accordion-list" id="creditinformation">
                                <ul>
                                    <li class="accordion-item">
                                        <a href="#" class="item-content item-link"  style="background: azure;">
                                            <div class="item-inner">
                                                <div class="item-title" id="credittext" style="display:none;">@lang('language.CreditCard')</div>
                                                <div class="item-title" id="creditpic">@lang('language.CreditCard')
                                                    @if ($env == 'master')
                                                        <img src="https://air.sinorama.ca/assets/images/visa.png" style="margin-left:20px;"/>
                                                        <img src="https://air.sinorama.ca/assets/images/mastercard.png" />
                                                        <img src="https://air.sinorama.ca/assets/images/ae.png" />
                                                    @else
                                                        <img src="https://dev.air.sinorama.ca/flight_front/assets/images/visa.png" style="margin-left:20px;"/>
                                                        <img src="https://dev.air.sinorama.ca/flight_front/assets/images/mastercard.png" />
                                                        <img src="https://dev.air.sinorama.ca/flight_front/assets/images/ae.png" />
                                                    @endif
                                                    <!--     <p><font color="red" size="2">We have to charge 3% of transaction amount as service fee. </font></p> -->
                                                </div>
                                            </div>
                                        </a>
                                        <div id="atos" style="display:none;">
                                            <iframe id="atosIframe" src="" width="100%"></iframe>
                                            <div>
                                                <span id="paymentmsg" class="red-color"></span>
                                            </div>
                                        </div>
                                        <div id="creditcontent" class="accordion-item-content" >
                                            <div class="content-block" style="padding:20px;height:250px;!important;">

                                                <div class="credit-card-input no-js" id="skeuocard" style="position: absolute;top: 50%;left: 50%;transform: translate(-50%, -50%);">
                                                    <p class="no-support-warning">
                                                        Either you have Javascript disabled, or you're using an unsupported browser, amigo! That's why you're seeing this old-school credit card input form instead of a fancy new Skeuocard. On the other hand, at least you know it gracefully degrades...
                                                    </p>
                                                    <label for="cc_type">Card Type</label>
                                                    <select name="cc_type">
                                                        <option value="">...</option>
                                                        <option value="visa">Visa</option>
                                                        <option value="discover">Discover</option>
                                                        <option value="mastercard">MasterCard</option>
                                                        <option value="maestro">Maestro</option>
                                                        <option value="jcb">JCB</option>
                                                        <option value="unionpay">China UnionPay</option>
                                                        <option value="amex">American Express</option>
                                                        <option value="dinersclubintl">Diners Club</option>
                                                    </select>
                                                    <label for="cc_number">Card Number</label>
                                                    <input type="text" name="cc_number" id="cc_number" placeholder="XXXX XXXX XXXX XXXX" maxlength="19" size="19">
                                                    <label for="cc_exp_month">Expiration Month</label>
                                                    <input type="text" name="cc_exp_month" id="cc_exp_month" placeholder="00">
                                                    <label for="cc_exp_year">Expiration Year</label>
                                                    <input type="text" name="cc_exp_year" id="cc_exp_year" placeholder="00">
                                                    <label for="cc_name">Cardholder's Name</label>
                                                    <input type="text" name="cc_name" id="cc_name" placeholder="John Doe">
                                                    <label for="cc_cvc">Card Validation Code</label>
                                                    <input type="text" name="cc_cvc" id="cc_cvc" placeholder="123" maxlength="3" size="3">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="accordion-item-content" id="creditinfo" style="display:none">
                                            <div class="card-content">
                                                <div class="list-block">
                                                    <ul>
                                                        <li>
                                                            <div class="item-content">
                                                                <div class="item-inner">
                                                                    <div class="item-input">
                                                                        <input name="address1" id="adress1" required type="text" style="height:30px;padding-left:15px;" placeholder="@lang('language.BillingAddress1')">

                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </li>
                                                        <li>
                                                            <div class="item-content">
                                                                <div class="item-inner">
                                                                    <div class="item-input">
                                                                        <input name="address2" id="adress2"  type="text" style="height:30px;padding-left:15px;" placeholder="@lang('language.BillingAddress2')">

                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </li>
                                                        <li>
                                                            <div class="item-content">
                                                                <div class="item-inner">
                                                                    <div class="item-input">
                                                                        <input name="city1" id="city1" required type="text" style="height:30px;padding-left:15px;" placeholder="@lang('language.City')">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </li>
                                                        <li>
                                                            <div class="item-content">
                                                                <div class="item-inner">
                                                                    <div class="item-input">
                                                                        <input name="province2" id="province2" required type="text" style="height:30px;padding-left:15px;" placeholder="@lang('language.ProvinceState')">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </li>
                                                        <li>
                                                            <div class="item-content">
                                                                <div class="item-inner">
                                                                    <div class="item-input">
                                                                        <input name="country1" id="country1" required type="text" style="height:30px;padding-left:15px;" placeholder="@lang('language.Country')">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </li>
                                                        <li>
                                                            <div class="item-content">
                                                                <div class="item-inner">
                                                                    <div class="item-input">
                                                                        <input name="postcode1" id="postcode1" required type="text" style="height:30px;padding-left:15px;" placeholder="@lang('language.PostCode')">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </li>
                                                        <li>
                                                            <div class="item-content">
                                                                <div class="item-inner">
                                                                    <p id="showreminder" style="color:#1D2088;margin-top: -8px;margin-bottom: -7px;display:none;">@lang('language.error1')</p>
                                                                </div>
                                                            </div>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </li>

                                    <li class="accordion-item">
                                        <a href="#" class="item-content item-link" style="    background: azure;">
                                            <div class="item-inner" >
                                                <div class="item-title">@lang('language.Payinfo1') </div>
                                            </div>
                                        </a>
                                        <div class="accordion-item-content">
                                            <div class="content-block">
                                                <p>
                                                    <a href="#" id="printbookingreceipt" style="margin-left: 0px;">@lang('language.Payinfo2')</a> @lang('language.Or')<a href="#" id="emailbookingconfirmation" style="margin-left: 0px;"> @lang('language.Payinfo3')</a>
                                                </p>
                                                <p>
                                                    @lang('language.Payinfo4')
                                                </p>
                                                <p>
                                                <ul style="padding-left: 5px;">
                                                    <li><strong>@lang('language.Montreal')</strong> <br>998 Boulevard Saint-Laurent,suite 518,<br> Montréal, Québec H2Z 9Y9</li>
                                                    <br><li><strong>@lang('language.Schedule')</strong> <br>Mon. - Fri. 10:00 - 18:00 / <br>Sat. - Sun. 10:00 - 17:00</li>
                                                    <br><li><strong>@lang('language.Telephone')</strong> 1-866-255-2188</li>
                                                </ul>
                                                <hr>
                                                <!--  <ul>
                                                      <li>Toronto:  7077 Kennedy Road, #201, Markham ON L3R 0N8</li>
                                                      <li>Scheduel： Mon. - Fri. 10:00 - 18:00 / Sat. - Sun. 10:00 - 17:00</li>
                                                      <li>Telephone： 1-888-577-9918</li>
                                                  </ul>
                                                  <hr>
                                                  <ul>
                                                      <li>Vancouver:  8091 Westminster Hwy, #200 Richmond BC V6X 1A7</li>
                                                      <li>Scheduel： Mon. - Fri. 10:00 - 18:00 / Sat. - Sun. 10:00 - 17:00</li>
                                                      <li>Telephone： 1-866-255-2188</li>
                                                  </ul>   -->
                                                </p>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="accordion-item">
                                        <a href="#" class="item-content item-link"  style="    background: azure;">
                                            <div class="item-inner">
                                                <div class="item-title">@lang('language.Payinfo5')</div>
                                            </div></a>
                                        <div class="accordion-item-content">
                                            <div class="content-block">
                                                <p><a href="#" id="printbookingreceipt" style="margin-left: 0px;">@lang('language.Payinfo2')</a> or<a href="#" id="emailbookingconfirmation" style="margin-left: 0px;"> @lang('language.Payinfo3')</a></p>
                                                <p>@lang('language.Payinfo6') <strong>etransfer@sinoramagroup.com</strong></p>
                                                <p>@lang('language.Payinfo7') <strong>1-866-255-2188</strong>@lang('language.Payinfo8')</p>
                                                <p>@lang('language.Payinfo9')</p>

                                            </div>
                                        </div>
                                    </li>


                                </ul>
                            </div>
                        </div>

                        <div class="demo-progressbar-inline" id="progressbar1" style="display: none;">
                            <p><span data-progress="50" class="progressbar" id="probar1" style="height:9px;"></span></p>
                            <p style="color:#1D2088;">@lang('language.WaitInfo')</p>
                        </div>

                        <div class="content-block" style="margin-top:20px;margin-button:120px;">
                            <a href="#" id="submitpayment" class="button button-fill" style="word-spacing: 20px;letter-spacing: 2px;display:none;" >@lang('language.SubmitPayment') </a>
                        </div>

                        <p>&nbsp;</p>

                    </div>
                    <div class="toolbar">
                        <div class="toolbar-inner">
                            <!-- Toolbar links -->
                            <a href="tel:1-866-255-2188" class="external">
                                <img src="dist/img/phone_u6.png" height="12" style="padding-right: 5px">1-866-255-2188
                            </a>

                            <a href="mailto:service@sinoramagroup.com" class="external">
                                <img src="dist/img/u115.png" height="12" style="margin-left: 0px;padding-right: 5px">service@sinoramagroup.com</a>

                            <!--
                            <a href="#" class="link"><img src="../dist/img/u145.png" height="17" style="margin-right: 3px"></a>
                            -->



                        </div>
                    </div>
                </div>
            </div>


            <div class="popup popup-search" style="background: #efeff4">
                <div class="page">
                    <div class="page-content">
                        <div class="navbar no-shadow ">
                            <div class="navbar-inner">
                                <div class="left sliding">
                                    <a href="#" class="link close-popup" >
                                        <i class="icon icon-back"></i><span class="">@lang('language.Close')</span></a>
                                </div>
                            </div>
                        </div>
                        @include('mobile.layouts.parts.searcharea')
                    </div>
                </div>
            </div>


            <div class="popup popup-policy" style="background: #efeff4">
                <div class="page">
                    <div class="page-content">
                        <div class="navbar no-shadow ">
                            <div class="navbar-inner">
                                <div class="left sliding">
                                    <a href="#" class="link close-popup" >
                                        <i class="icon icon-back"></i><span class="">@lang('language.Close')</span></a>
                                </div>
                            </div>
                        </div>
                        @include('mobile.layouts.parts.ticketpolicy')
                    </div>
                </div>
            </div>

            <div class="popup popup-pricedetailpay" style="background: #efeff4">
                <div class="page">
                    <div class="page-content">
                        <div class="navbar no-shadow ">
                            <div class="navbar-inner">
                                <div class="left sliding">
                                    <a href="#" class="link close-popup-pdp" >
                                        <i class="icon icon-back"></i><span class="">@lang('language.Close')</span></a>
                                </div>
                            </div>
                        </div>
                        @include('mobile.layouts.parts.pricedetailpay')
                    </div>
                </div>
            </div>

            <div class="popup coupon-opacity-overlay" >

                <div class="card">
                    <div class="card-content card-content-padding">@lang('language.Couponinfo')</div>
                    <div class="card-content card-content-padding">@lang('language.Couponinfo2')</div>
                    <div class="card-footer">
                        <a href="#" class="link" id="return">@lang('language.Return')</a>
                        <a href="#" class="link" id="continue">@lang('language.Continue')</a>
                    </div>

                </div>
            </div>

            <div class="popup popup-paysuccess" style="background: #efeff4">
                <div class="page">
                    <div class="page-content">
                        <!-- navbar -->
                        <div class="navbar navbar_list">
                            <div class="navbar-inner">
                                <!-- We need cool sliding animation on title element, so we have additional "sliding" class -->
                                <div class="left logo_text">
                                    <a href="/" class="external">
                                        <img src="dist/img/logi_u4.png" style="margin-top: 5px">
                                    </a>
                                </div>
                                <!-- right
                                <div class="right  text-right">
                                <span>
                                    <a href="#" data-panel="right" class="open-panel">
                                    <img src="../dist/img/u107.png" width="17">
                                    </a>
                                </span>
                                </div>  -->
                            </div>
                        </div>
                        <!-- pay successful !-->
                        <div class="card book-card">
                            <div class="card-content">
                                <img src="dist/img/search_list___review/u1089.png" width="16" height="13">
                                <div>
                                    <p style="font-size: 16px">@lang('language.PaySuccess')</p>
                                    <p class="content-block-title desc">@lang('language.PayS1')</p>

                                    <p>@lang('language.PayS2') </p>
                                </div>
                            </div>
                        </div>
                        <!-- <div class="card" style="margin-top: 20px;">

                             <div class="card-content">
                                 <div class="content-block" style="padding-top:40px;">
                                     <div style="margin-bottom:40px;">
                                         <img src="dist/img/logi_u4.png" style="position: fixed;    left: 50%;
     transform: translate(-50%, -50%);">
                                     </div>

                                     <h3 class="">
                                         <img src="dist/img/search_list___review/u1089.png" width="16" height="13"style="margin-right:15px;">
                                         Congratulations ！ Pay Successfully !
                                     </h3>
                                     <p class="content-block-title desc">Thank you for choosing Sinorama!</p>

                                     <p class="">We will also send you a e-ticket email after we issue your ticket. </p>

                                     <p>Please check the e-ticket by ensure email.</p>
         </div>
     </div>
 </div>-->
                        <div class="card">
                            <div class="card-content">
                                <div class="card-footer">
                                    <a href="#" class="link">Your order NO.:</a>
                                    <a href="#" id="orderid" style="margin-right: 15%"></a>
                                    <a id="PNR" style="display:none"></a>
                                    <!-- <a href="#" data-popup=".popup-about" class="open-popup">
                                         <img src="dist/img/search_list___review/u1087.png">
                                     </a>-->
                                </div>
                                <div class="card-footer">
                                    <a href="#" style="color: dodgerblue">Your status:</a>
                                    <a href="#" style="color: dodgerblue">Paid</a>
                                    <a href="#" class="link"></a>
                                </div>
                            </div>
                        </div>
                        <!-- change style
                        <table style="margin:auto">
                                        <tr>
                                            <td align="right">
                                                <h3>Order number</h3>
                                            </td>
                                            <td>
                                                <span id="orderid"></span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right">
                                                <h3>Status</h3>
                                            </td>
                                            <td>
                                                Paid
                                            </td>
                                        </tr>
                                    </table>-->
                        <h3 style="padding-left: 15px; color: #1D2088;">You can also:</h3>
                        <!--
                                                            <a href="https://us.air.sinorama.ca/" class="button button-big button-round active external">Singin to check order status </a>
                                                            -->

                        <button id="gotohomepage" class="button button-round active" style="margin-top:20px;margin-bottom:20px;margin-left: 10%;width: 80%">Make a new book </button>

                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('mobile.layouts.parts.internarydetail')

@endsection



@section('extrascript')


<script src="css/skeuocard/javascripts/vendor/cssua.min.js"></script>
<script src="css/skeuocard/javascripts/skeuocard.js"></script>
<script type="text/javascript" src="https://js.stripe.com/v2/"></script>






<script>
    @if($env == 'master')
        var cleanUrl = "{{route("clean")}}";
        console.log("master:");
        console.log(cleanUrl);
    @else
        var cleanUrl = "{{route("dev_clean")}}";
        console.log("dev:");
        console.log(cleanUrl);
    @endif

    var bookingErrors = {
            'soldout': '<?php echo 'Flight tickets have been sold out. Please choose again!' ;?>',
            'systemException': '<?php echo'System is being abnormal, please contact our customer service, 1-866-255-2188';?>'
        };
    var stripeErrors = {
        'PNR_Save': "An error occurred while processing the card.",
        'PNR_Retrieve': "An error occurred while processing the card.",
        'FOP_CreateFormOfPayment': "An error occurred while obtaining credit card approval.",
        'EndTransactionLLSRQ': "An error occurred while processing the card.",
        'TravelItineraryReadRQ': "An error occurred while processing the card.",
        'CreditVerificationRQ': "An error occurred while obtaining credit card approval.",
        'AddRemarkLLSRQ_Creditcard': "An error occurred while processing the card.",
        'invalid_coupon': "The coupon is not valid.",
        'coupon_processing_error': "An error occurred while processing the coupon.",
        'invalid_number': "The card number is not a valid credit card number.",
        'invalid_expiry_month': "The card's expiration month is invalid.",
        'invalid_expiry_year': "The card's expiration year is invalid.",
        'invalid_cvc': "The card's security code is invalid.",
        'invalid_swipe_data': "The card's swipe data is invalid.",
        'incorrect_number': "The card number is incorrect.",
        'expired_card': "The card has expired.",
        'incorrect_cvc': "The card's security code is incorrect.",
        'incorrect_zip': "The card's zip code failed validation.",
        'card_declined': "The card was declined. Please try again.",
        'missing': "There is no card on a customer that is being charged.",
        'processing_error': "An error occurred while processing the card."
    };
    var ssrLangs = {
        'mealpreference_none': "<?php echo 'mealpreference_none'; ?>",
        'CHML': "<?php echo 'CHML'; ?>",
        'DBML': "<?php echo 'DBML'; ?>",
        'FPML': "<?php echo 'FPML'; ?>",
        'GFML': "<?php echo 'GFML'; ?>",
        'MOML': "<?php echo 'MOML'; ?>",
        'KSML': "<?php echo 'KSML'; ?>",
        'LCML': "<?php echo 'LCML'; ?>",
        'LFML': "<?php echo 'LFML'; ?>",
        'LSML': "<?php echo 'LSML'; ?>",
        'SFML': "<?php echo 'SFML'; ?>",
        'VJML': "<?php echo 'VJML'; ?>",
        'VLML': "<?php echo 'VLML'; ?>",
        'VGML': "<?php echo 'VGML'; ?>",
        'ssr_none': "<?php echo 'ssr_none'; ?>",
        'BLND': "<?php echo 'BLND'; ?>",
        'DEAF': "<?php echo 'DEAF'; ?>",
        'WCHS': "<?php echo 'WCHS'; ?>",
        'WCHR': "<?php echo 'WCHR'; ?>",
        'WCHC': "<?php echo 'WCHC'; ?>"
    };
    var lang_array={
        "Price_Change":"Your ticket price changed from formerfee to updatefee. The airline could not confirm the original price due to pricing or availability changes that occurred after we posted the latest prices on our site. Continue booking or look for a different flight."
    };


    var contact =<?php echo json_encode($contact); ?>;
    console.log("contact:");
    console.log(contact);
    console.log(contact["firstname"]);

    var Passengers = <?php echo json_encode($Passengers); ?>;
    console.log("passengers:");
    console.log(Passengers);
    var Itineraries = <?php echo $Itineraries; ?>;
    console.log("Itineraries:");
    console.log(Itineraries);
    var OrderInfo = <?php echo json_encode($OrderInfo); ?>;
    console.log("OrderInfo:");
    console.log(OrderInfo);

    var destcity=OrderInfo.to;
    var srccity=OrderInfo.from;

    var request = new Request();
    request.fromDBData(<?php echo $orderInfoJSONData; ?>);
    request.clientIp = '<?php echo $ip; ?>';
    var currentyear ='<?php echo date("Y"); ?>';
    var maxexpiredyear = 30;

    var options= OrderInfo;
    options["origin"]=options.from;
    options["destination"]=options.to;
    options.departuredate=(options.departuredate).substr(0,10);
    options.totalprice=parseInt(options.totalprice)/100;
    options.taxfee=parseInt(options.taxfee)/100;
    options.basefee=parseInt(options.basefee)/100;
    options.tripType=options.triptype;

    var goraw=[];
    var returnraw=[];
    var markflag;
  /*  tjq.each(Itineraries, function (idx, seg) {
        if(seg.From==destcity)
           markflag=idx;
    });

    for(var i=0;i<markflag;i++)
    {goraw.push(Itineraries[i]);}

    for(i=markflag;i<Itineraries.length;i++)
    {returnraw.push(Itineraries[i]);}*/

    var flag=true;
    tjq.each(Itineraries, function (idx, seg) {
    if(seg.From==destcity)
        flag=false;
    if(flag)
        goraw.push(seg);
    else
        returnraw.push(seg);
    })

    console.log(goraw);
    console.log(returnraw);

    request.goKey=goraw;
    request.returnKey=returnraw;

    var coupon=getCookie('coupon');
    if(coupon!=''){
       deleteCookie('coupon');
    }

    function setCookie(name,value){
        var exp = new Date();
        exp.setTime(exp.getTime() + 1*60*60*1000);//有效期1小时
        document.cookie = name + "="+ escape (value) + ";expires=" + exp.toGMTString();
    }

    function getCookie(name){
        var arr = document.cookie.match(new RegExp("(^| )"+name+"=([^;]*)(;|$)"));
        if(arr != null) return unescape(arr[2]); return null;
    }

    function deleteCookie(name){
        document.cookie = name+"=; expires=Thu, 01 Jan 1970 00:00:00 UTC;";
    }

    var departtime1=goraw[0].DepartureDateTime;
    var arrivaltime1=goraw[parseInt(goraw.length)-1].ArrivalDateTime;
    console.log(departtime1);
    console.log(arrivaltime1);
    var departdate1 = new Date(departtime1 + "Z");
    var arrivaldate1=new Date(arrivaltime1 + "Z");
    var diff1 = arrivaldate1.getTime()-departdate1.getTime();
    console.log(diff1);
    var diffhours1 = Math.floor(diff1 / 1000 / 60 / 60);
    var diffminutes1 = Math.floor(diff1 / 1000 / 60) % 60;
    console.log(diffhours1);
    console.log(diffminutes1);

    //request.goRaw.ElapsedTimeTotalHours=diffhours1;
    //request.goRaw.ElapsedTimeTotalHoursLeftMins=diffminutes1;
    var gokey=[];
    gokey.ElapsedTimeTotalHours=diffhours1;
    gokey.ElapsedTimeTotalHoursLeftMins=diffminutes1;
    gokey.stops=parseInt(goraw.length)-1;
    request.goRaw=gokey;

    if(request.tripType=="roundtrip")
    {
                var departtime2 = returnraw[0].DepartureDateTime;
                var arrivaltime2 = returnraw[parseInt(returnraw.length) - 1].ArrivalDateTime;
                var departdate2 = new Date(departtime2 + "Z");
                var arrivaldate2 = new Date(arrivaltime2 + "Z");
                var diff2 = arrivaldate2.getTime() - departdate2.getTime();
                var diffhours2 = Math.floor(diff2 / 1000 / 60 / 60);
                var diffminutes2 = Math.floor(diff2 / 1000 / 60) % 60;

                var returnkey = [];
                returnkey.ElapsedTimeTotalHours = diffhours2;
                returnkey.ElapsedTimeTotalHoursLeftMins = diffminutes2;
                returnkey.DepartureDateTime = departtime2;
                returnkey.DepartureCity = OrderInfo.destination;
                returnkey.ArrivateCity = OrderInfo.origin;
                returnkey.stops = parseInt(returnraw.length) - 1;
                request.returnRaw = returnkey;
    }

    tjq(document).ready(function() {
    {
        var param="";
        console.log(request);

        if(request.tripType=="roundtrip")
        {
            tjq('#roundpicture').css("display", "");
        }
        if(request.tripType=="oneway")
        {
            tjq('#singlepicture').css("display", "");
        }


        var adultcount=parseInt(request.adultcount);
        var childrencount=parseInt(request.childrencount);
        var infantsseatcount=parseInt(request.infantsseatcount);
        var infantslapcount=parseInt(request.infantslapcount);

        tjq('#basefare').text(request.basefee);
        tjq('#taxfee').text(request.taxfee);
        tjq('#totalamount').text(request.totalfee);
        tjq('#currency').text(request.currencyShow);


        tjq('#adultcount').text(request.adultcount);
        tjq('#childcount').text(request.childrencount);
        tjq('#infantcount').text(request.ifantcount);


        tjq('#headdeparture').html(getCityNameByCode(request.srccity));
        tjq('#headarrive').html(getCityNameByCode(request.destcity));
        //tjq('[id=destdesc]').html(getCityNameByCode(options.destination));
        //tjq('[id=returndestdesc]').html(getCityNameByCode(options.origin));

        var departdate=(request.departdate).substr(0,10);

        var fromdate = Date.parse(departdate).toString();
        //console.log(fromdate);

        if(request.tripType=="roundtrip") {
          var returndate=(request.returndate).substr(0,10);
          var todate = Date.parse(returndate).toString();
          var datedesc = fromdate.substr(0, 15) + " ~ " + todate.substr(0, 15);
          tjq('#daterange').html(datedesc);
          }
        else if(request.tripType=="oneway"){
          var datedesc = fromdate.substr(0, 15).toString();
          tjq('#daterange').html(datedesc);

          }

        var airlines = [];

        tjq.each(Itineraries, function (segidx, segval) {

                if (airlines.indexOf(segval.Airline) == '-1') {
                    airlines.push(segval.Airline);
                }
          });

        console.log(airlines);
        for (var i = 1; i <= airlines.length; i++) {
            var airline = airlines[i-1];
            var airlineName = getAirlinesNameByCode(airline);
            console.log(airlineName);
            var newflyer = tjq('#flyerplan').clone();
            tjq(newflyer).find("#flyernumber").attr('placeholder', airlineName);
            tjq(newflyer).find("#flyernumber").attr('name',"flyernumber_" + airline);
            tjq(newflyer).find("#flyernumber").attr('id', "flyernumber_" + airline);
            tjq(newflyer).attr('id',"flyerplan_" + airline);

            //tjq(newflyer).find("label[for=flyernumber]").attr("for", "flyernumber_" + airline);
            //tjq(newflyer).attr('id', "flyerplan_" + airline);
            //tjq(newflyer).show();
            tjq('#flyinfo').prepend(newflyer);
        }

        tjq('#flyerplan').remove();


        var countryCodesWithISO2Key = {};
        tjq.each(countryCode, function (i, v) {
            countryCodesWithISO2Key[v['ISO_2_Code']] = displayISO2key(v);
        });


        tjq.each(Passengers, function (key, Passenger) {
            var personIndex = Passenger['idx'];
            tjq('div[dataidx=' + personIndex + ']').find('#firstname').text(Passenger['firstname']);
            tjq('div[dataidx=' + personIndex + ']').find('#middlename').text(Passenger['middle']);
            tjq('div[dataidx=' + personIndex + ']').find('#lastname').text(Passenger['lastname']);
            if (Passenger['sex'] == 'M') {
                var gender = 'Male';
            } else {
                var gender = 'Female';
            }
            tjq('div[dataidx='+ personIndex + ']').find('#gender').text(gender);
            tjq('div[dataidx=' + personIndex + ']').find('#birthday').text(Passenger['birthday']);
            tjq('div[dataidx=' + personIndex + ']').find('#nationality').text(countryCodesWithISO2Key[Passenger['nationality']]);
            tjq('div[dataidx=' + personIndex + ']').find('#passport').text(Passenger['cno']);
            tjq('div[dataidx=' + personIndex + ']').find('#passportexpired').text(Passenger['cexpired']);
            tjq('div[dataidx=' + personIndex + ']').find('#passportcountry').text(countryCodesWithISO2Key[Passenger['ccountry']]);
            if (Passenger['flyernumbers'] != undefined && Passenger['flyernumbers'] != '') {
                Passenger['flyernumbers'].split(',').forEach(function (flyernumber) {
                    var airline = flyernumber.substr(0, 2);
                    var number = flyernumber.substr(2);
                    tjq('div[dataidx=' + personIndex + ']').find('#flyernumber_' + airline).text(number);
                });
            } else {
                tjq('div[dataidx=' + personIndex + ']').find('#flyernumber_' + airline).text(ssrLangs['ssr_none']);
            }
            if (Passenger['mealpreference'] != undefined && Passenger['mealpreference'] != '') {
                var mealpreference = ssrLangs[Passenger['mealpreference']];
            } else {
                var mealpreference = ssrLangs['mealpreference_none'];
            }
            tjq('div[dataidx=' + personIndex + ']').find('#mealpreference').text(mealpreference);
            if (Passenger['specialservice'] != undefined && Passenger['specialservice'] != '') {
                var ssr = ssrLangs[Passenger['specialservice']];
            } else {
                var ssr = ssrLangs['ssr_none'];
            }
            tjq('div[dataidx=' + personIndex + ']').find('#ssr').text(ssr);
        });

        tjq('div[dataidx="contact"]').find('#contactfirstname').text(contact['firstname']);
        tjq('div[dataidx="contact"').find('#contactlastname').text(contact['lastname']);
        tjq('div[dataidx="contact"]').find('#email').text(contact['email']);
        tjq('div[dataidx="contact"]').find('#tel1').text(contact['tel1']);
        tjq('div[dataidx="contact"]').find('#address').text(contact['address']);
        tjq('div[dataidx="contact"]').find('#city').text(contact['city']);
        tjq('div[dataidx="contact"]').find('#province').text(contact['province']);
        tjq('div[dataidx="contact"]').find('#country').text(countryCodesWithISO2Key[contact['country']]);
        tjq('div[dataidx="contact"]').find('#postcode').text(contact['postcode']);

        $$('.close-popup-pdp').on('click',function(){
            myApp.closeModal('.popup-pricedetailpay');
        });

        $$('#showusdetailpay').on('click', function () {
            myApp.popup('.popup-pricedetailpay');
            showpricedetailpay(tjq(options),tjq(request));
        })

        $$('#searchcard').on('click', function () {
            myApp.popup('.popup-search');
        })

        $$('.passengerbutton').on('click', function () {
            var dataidx=tjq(this).parents("[dataidx]").attr("dataidx");
            myApp.popup('.popup-passenger-'+dataidx);

        })

        $$('#passengercontact').on('click', function () {
            myApp.popup('.popup-passenger-contact');
        })


        function searchcountry(query,callingcode) {
            var results = [];
            for (var i = 0; i < countryCode.length; i++) {
                var countryitem = countryCode[i];

                if (countryitem.ISO_2_Code.toLowerCase().indexOf(query.toLowerCase()) >= 0 ||
                    countryitem.ISO_3_Code.toLowerCase().indexOf(query.toLowerCase()) >= 0 ||
                    countryitem.countryName.toLowerCase().indexOf(query.toLowerCase()) >= 0 ||
                    countryitem.countryName_CH.toLowerCase().indexOf(query.toLowerCase()) >= 0 ||
                    countryitem.countryName_PY.toLowerCase().indexOf(query.toLowerCase()) >= 0
                ) {
                    if(callingcode!=undefined){
                        var tmpstr = "( "+countryitem.callingCode + " ) " + countryitem.countryName;
                        if (lang != undefined && lang == "Chinese") {
                            tmpstr = "( "+countryitem.callingCode + ") " + countryitem.countryName_CH;
                        }
                        results.push(tmpstr);

                    }else{
                        var tmpstr = countryitem.ISO_2_Code + "-" + countryitem.countryName;
                        if (lang != undefined && lang == "Chinese") {
                            tmpstr = countryitem.ISO_2_Code + "-" + countryitem.countryName_CH;
                        }
                        results.push(tmpstr);
                    }

                }
            }
            return results;
        }

        tjq('[id=showotheroptons]').on('click', function () {
            if (tjq('.optional:visible').length > 0) {
                tjq('.optional:visible').hide();
            } else {
                tjq('.optional').show();
            }

        })

        tjq('#printbookingconfirmation, #printbookingreceipt').click(function (e) {
            e.preventDefault();

            if (tjq("#PNR").text().length > 0 && tjq("#orderid").text().length > 0) {
                var orderid = tjq("#orderid").text();
                FT_c_generateToken(orderid, function (rs) {
                    if (rs != undefined && rs[0] == 'success') {
                        window.open('<?php if($env == 'master') echo "https://crud.sinorama.ca";else echo "https://dev.crud.sinorama.ca" ; ?>/index.php/modules/pnrofqueue/pnrreceipt_customer/' + tjq("#PNR").text() + '/' + tjq("#orderid").text() + '/' + rs[1]);
                    } else {
                        alert("The server does not respond, please try again later, thank you");
                    }
                });
            }
        });

        tjq('#emailbookingconfirmation').click(function (e) {
            e.preventDefault();

            if (tjq("#PNR").text().length > 0 && tjq("#orderid").text().length > 0) {
                var para = {};
                para['PNR'] = tjq("#PNR").text();
                para['orderid'] = tjq("#orderid").text();
                FT_c_emailBookingConfirmation(para, function (rs) {
                    if (rs != undefined && rs[0] == 'success') {
                        alert('We have already sent the reservation confirmation to your email, please check it');
                    } else {
                        alert('The server is not respond, please try again later, thank you');
                    }
                });
            }
        });

        if(parseInt(request.totalfee)<500)
            tjq('#couponForm').hide();

        tjq('#buttonverify').click(function (e){
            e.preventDefault();
            vlidateCoupon();
        });

        function ShowFormerror(item){
            tjq(item).css('background','rgba(255, 45, 85, 0.11)');
        }

        function RemoveItem(items){
            tjq(items).css('background','#fff');
        }

        function checkFirstName(name) {
            if (name == undefined || name == "" || name.length == 0) {
                return false;
            }

            if (!/^[ a-zA-Z]+$/g.test(name)) {
                return false;
            }

            return true;
        }


        param="";
        card = new Skeuocard(tjq("#skeuocard"),{
            acceptedCardProducts: ['visa', 'amex','mastercard']
        });
        //if (msg['updateFee']) {
          //  updateTicketFee(msg);
        //}
        //console.log(request);
        var para = request;
        para['provider'] = OrderInfo['provider'];
        para['orderid'] = OrderInfo['orderid'];
        para['PNR'] = OrderInfo['PNR'];
        para['chargeamt'] = parseInt(OrderInfo['totalprice']*100);
        para['currency'] = OrderInfo['currency'];
        para["src"] = "mobile";
        para['booking'] = {};
        para['booking']["email"] = contact['email'];
        para["booking"]["tel"]=contact['tel1'];

        tjq('#PNR').text(para.PNR);
        tjq('#orderid').text(para.orderid);


        tjq('#submitpayment').show();

        //tjq('#orderid').text(request.orderid);
        //tjq('#PNR').text(request.PNR);

        if(request.currency == 'AUD'||request.currency == 'EUR'||request.currency == 'NZD'||request.currency == 'GBP'||(request.currency=='USD')){
            tjq('#credittext').show();
            tjq('#creditpic').hide();
            tjq('#creditcontent').hide();
            tjq('#creditinfo').hide();
            tjq('#atos').css('display','inline');
        }


    /*           if (tjq("#PNR").text().length > 0) {
            var atos = tjq('#atosIframe');
            if (request.currency == 'AUD' || request.currency == 'NZD'||request.currency == 'EUR'||request.currency == 'GBP'||(request.currency=='USD')) {
                //if(request.coupon=="undefined")
                //var atosSrc = '<?php if($env== 'master') echo 'https://mo.air.sinorama.ca/'; else echo 'https://dev.mo.air.sinorama.ca/'; ?>paypal?orderid=' + tjq("#orderid").text();
                //else if(request.coupon!="undefined")
                var atosSrc = '<?php if($env== 'master') echo 'https://mo.air.sinorama.ca/'; else echo 'https://dev.mo.air.sinorama.ca/'; ?>paypal?orderid=' + tjq("#orderid").text()+'&amt='+ request.coupon;
            } //else if (request.currency == 'EUR') {
              //  var atosSrc = '<?php // if($env== 'master') echo 'https://mo.air.sinorama.ca/'; else echo 'https://dev.mo.air.sinorama.ca/'; ?>ft-atos?PNR=' + tjq("#PNR").text();
            // }
            tjq(atos).attr('src', atosSrc);
        }
    */


        if (tjq("#PNR").text().length > 0) {
            var atos = tjq('#atosIframe');
            if (request.currency == 'AUD' || request.currency == 'NZD'||request.currency == 'EUR'||
                request.currency == 'GBP'||(request.currency=='USD'))        {
                var atosSrc = '<?php if($env== 'master') echo 'https://mo.air.sinorama.ca/';
                                 else echo 'https://dev.mo.air.sinorama.ca/';
                               ?>paypal?orderid=' + tjq("#orderid").text()+'&amt='+ request.coupon;
            }
            tjq(atos).attr('src', atosSrc);
        }

        <?php
            $empty=array(array());

            $response = dnode('getCreditcardPayment',$empty,$env);
            $credittype=$response[0][1];

            function dnode($fun,$para,$env){
                $dnode = new \DnodeSyncClient\Dnode();

                //$svraddr="dev.search.air.sinorama.ca";
                if ($env=="dev"){
                    $svraddr="dev.search.air.sinorama.ca";
                }
                else if ($env=="master"){
                    $svraddr="search.air.sinorama.ca";
                }


                if ($svraddr!=""){
                    $connection = $dnode->connect($svraddr, 9998);
                    $result=$connection->call($fun,$para);
                    $connection->close();
                    return $result;
                }else{
                    echo "I do not know where am I";
                    return;
                }
            }
        ?>


        var credittype='<?php echo $credittype;?>';

        if(credittype=="stripe")
            tjq('#creditinfo').show();
        else if(credittype=="airline")
            tjq('#creditinfo').hide();

        tjq('#submitpayment').on('click',function(e){
            var nametoken=true;
            var cardnumber=true;
            var expiry=true;
            var cvc=true;
            e.preventDefault();
            if(tjq('#iamdongpaying').length>0) return;

            if(card.isValid()){

                tjq(this).html('<span id="iamdongpaying" class="preloader preloader-white"></span>');

                    console.log(param);

         //       vlidateCoupon(function () {
                    // Disable the submit button to prevent repeated clicks

                    //msg['couponcode'] = "";
                if(tjq('#couoncode').val()==param) {
                    para['couoncode'] = param;
                    issueticket(credittype,para);
                }
                else          //popup
                {
                    if(tjq('#couoncode').val()==''){
                        para['couoncode'] = param;
                        issueticket(credittype,para);
                    }
                    else {
                        myApp.popup('.coupon-opacity-overlay');
                        tjq('#return').on('click', function () {
                            myApp.closeModal('.coupon-opacity-overlay');
                            tjq('#iamdongpaying').hide();
                            tjq('#submitpayment').html('<a href="#" id="submitpayment" class="button button-fill" style="word-spacing: 20px;letter-spacing: 2px;" >Submit Payment </a>');
                        });
                        tjq('#continue').on('click', function () {
                            myApp.closeModal('.coupon-opacity-overlay');
                            para['couoncode'] = param;
                            issueticket(credittype, para);
                        });
                    }
                }


                /*
                    para['couoncode']=tjq('#couoncode').val();


                    Stripe.setPublishableKey('pk_live_B5SZw46G2GUaRIWOD1s1YjdW');

                    var cardNumber = tjq('[name="cc_number"]').val().trim();
                    console.log(cardNumber);
                    var expiredyear = tjq('[name="cc_exp_year"]').val().trim();
                    var expiredmonth = tjq('[name="cc_exp_month"]').val().trim();


                    expiredmonth=ModifyToTwoDigits(expiredmonth);

                    if (!Stripe.card.validateCardNumber(cardNumber)) {
                        console.log('card is wrong');
                        cardnumber=false;
                    }

                    var creditcardType = getCreditcardType(cardNumber);
                    if (creditcardType == '') {
                        alert('wrong');
                        return;
                    }

                    if (!Stripe.card.validateExpiry(expiredmonth, tjq('[name="cc_exp_year"]').val().trim())) {
                        console.log('date is wrong');
                        expiry=false;
                    }
                    if (!Stripe.card.validateCVC(tjq('[name="cc_cvc"]').val().trim())) {
                        console.log('card is wrong');
                        cvc=false;
                    }

                    if (!checkFirstName( tjq('[name="cc_name"]').val().trim())) {
                        console.log("first name is worng");
                        nametoken=false;
                    }


                    /*
                    para['booking']["card"] = {
                        'name': tjq('[name="cc_name"]').val().trim(),
                        'number': cardNumber,
                        'last4': cardNumber.substring(cardNumber.length - 4),
                        'exp_year': expiredyear,
                        'exp_month': expiredmonth,
                        'brand': creditcardType,
                        'seccode': tjq('[name="cc_cvc"]').val().trim(),
                       /* 'address1': tjq('[name="address1"]').val().trim(),
                        'address2': tjq('[name="address2"]').val().trim(),
                        'city1': tjq('[name="city1"]').val().trim(),
                        'province2': tjq('[name="province2"]').val().trim(),
                        'country1': tjq('[name="country1"]').val().trim(),
                        'postcode1': tjq('[name="postcode1"]').val().trim(),*/
                   // }  */
                   /*

                if(credittype=="airline"){
                    para['booking']["card"] = {
                        'name': tjq('[name="cc_name"]').val().trim(),
                        'number': cardNumber,
                        'last4': cardNumber.substring(cardNumber.length - 4),
                        'exp_year': expiredyear,
                        'exp_month': expiredmonth,
                        'brand': creditcardType,
                        'seccode': tjq('[name="cc_cvc"]').val().trim(),
                        'address1': tjq('[name="address1"]').val().trim(),
                        'address2': tjq('[name="address2"]').val().trim(),
                        'city1': tjq('[name="city1"]').val().trim(),
                        'province2': tjq('[name="province2"]').val().trim(),
                        'country1': tjq('[name="country1"]').val().trim(),
                        'postcode1': tjq('[name="postcode1"]').val().trim(),

                    }
                }
                else if(credittype=="stripe"){
                    para['booking']["card"] = {
                        'name': tjq('[name="cc_name"]').val().trim(),
                        'number': cardNumber,
                        'last4': cardNumber.substring(cardNumber.length - 4),
                        'exp_year': expiredyear,
                        'exp_month': expiredmonth,
                        'brand': creditcardType,
                        'seccode': tjq('[name="cc_cvc"]').val().trim(),
                    };
                }



                    para['creditcardProviders'] = credittype;
                    console.log(para);

                    */
        /*            if ((para['booking']['card']["address1"] == "")||(para['booking']['card']["city1"] == "")||(para['booking']['card']["province2"] == "")||(para['booking']['card']["country1"] == "")||(para['booking']['card']["postcode1"] == "")||(nametoken==false)||(cvc==false)||(expiry==false)||(cardnumber==false))
                    {
                        RemoveItem(tjq('[name="address1"]'));
                        //RemoveItem(tjq('[name="address2"]'));
                        RemoveItem(tjq('[name="city1"]'));
                        RemoveItem(tjq('[name="province2"]'));
                        RemoveItem(tjq('[name="country1"]'));
                        RemoveItem(tjq('[name="postcode1"]'));

                        if(para['booking']['card']["address1"] == "")
                            ShowFormerror(tjq('[name="address1"]'));
                        //if(msg['booking']['card']["address2"] == "")
                        //ShowFormerror(tjq('[name="address2"]'));
                        if(para['booking']['card']["city1"] == "")
                            ShowFormerror(tjq('[name="city1"]'));
                        if(para['booking']['card']["province2"] == "")
                            ShowFormerror(tjq('[name="province2"]'));
                        if(para['booking']['card']["country1"] == "")
                            ShowFormerror(tjq('[name="country1"]'));
                        if(para['booking']['card']["postcode1"] == "")
                            ShowFormerror(tjq('[name="postcode1"]'));
                        if(nametoken==false) {
                            alert("your name input is not correct, please enter again, keep mind that only letters are accepted");

                            //tjq('div[class="cc-field filled valid"]').attr("class",'cc-field filled invalid');
                            //console.log(tjq('[name="cc_name"]'));
                            //console.log(tjq(".cc_name").parent());
                            tjq('input[class="cc-name"]').parent().attr("class",'cc-field filled invalid');
                        }
                        tjq('#showreminder').css('display','');
                        tjq('#iamdongpaying').hide();
                        tjq('#submitpayment').html('<a href="#" id="submitpayment" class="button button-fill" style="word-spacing: 20px;letter-spacing: 2px;" >Submit Payment </a>');
                    }
                    else{   */               /*
                        FT_c_charge_ticket(para, function (chargers) {
                            console.log(chargers);


                            if (chargers != undefined && chargers[0] != 'error') {
                                tjq.get(cleanUrl, function (data) {
                                    //tjq('#orderid').text(msg.orderid);
                                    //tjq('#PNR').text(msg.PNR);
                                    myApp.popup('.popup-paysuccess');
                                });


                            } else {
                                //tjq(this).html();

                                myApp.alert(stripeErrors[chargers[1]], "Unsuccessful");
                                //console.log(tjq(this));

                                tjq('#iamdongpaying').hide();
                                tjq('#submitpayment').html('<a href="#" id="submitpayment" class="button button-fill" style="word-spacing: 20px;letter-spacing: 2px;" >Submit Payment </a>');
                                tjq('#progressbar1').css("display", "none");
                            }
                        }, function (value) {
                            console.log(value);
                            tjq('#progressbar1').css("display", "inline");
                            tjq('#probar1').attr('data-progress', value);
                            var progress = tjq('#probar1').attr('data-progress');
                            var progressbar = tjq('#probar1');
                            myApp.setProgressbar(progressbar, progress);


                        });  */
                    //}
                    //return false;
                    //return;
            //    });

            }
        });

        function getCreditcardType(number) {
            // visa
            var re = new RegExp("^4");
            if (number.match(re) != null)
                return "VI";

            // Mastercard
            re = new RegExp("^5[1-5]");
            if (number.match(re) != null)
                return "CA";

            // AMEX
            re = new RegExp("^3[47]");
            if (number.match(re) != null)
                return "AX";

            return "";
        }

        function issueticket(credittype,para){
            Stripe.setPublishableKey('pk_live_B5SZw46G2GUaRIWOD1s1YjdW');

            var cardNumber = tjq('[name="cc_number"]').val().trim();
            console.log(cardNumber);
            var expiredyear = tjq('[name="cc_exp_year"]').val().trim();
            var expiredmonth = tjq('[name="cc_exp_month"]').val().trim();


            expiredmonth=ModifyToTwoDigits(expiredmonth);

            if (!Stripe.card.validateCardNumber(cardNumber)) {
                console.log('card is wrong');
                cardnumber=false;
            }

            var creditcardType = getCreditcardType(cardNumber);
            if (creditcardType == '') {
                alert('wrong');
                return;
            }

            if (!Stripe.card.validateExpiry(expiredmonth, tjq('[name="cc_exp_year"]').val().trim())) {
                console.log('date is wrong');
                expiry=false;
            }
            if (!Stripe.card.validateCVC(tjq('[name="cc_cvc"]').val().trim())) {
                console.log('card is wrong');
                cvc=false;
            }

            if (!checkFirstName( tjq('[name="cc_name"]').val().trim())) {
                console.log("first name is worng");
                nametoken=false;
            }


            /*
             para['booking']["card"] = {
             'name': tjq('[name="cc_name"]').val().trim(),
             'number': cardNumber,
             'last4': cardNumber.substring(cardNumber.length - 4),
             'exp_year': expiredyear,
             'exp_month': expiredmonth,
             'brand': creditcardType,
             'seccode': tjq('[name="cc_cvc"]').val().trim(),
             /* 'address1': tjq('[name="address1"]').val().trim(),
             'address2': tjq('[name="address2"]').val().trim(),
             'city1': tjq('[name="city1"]').val().trim(),
             'province2': tjq('[name="province2"]').val().trim(),
             'country1': tjq('[name="country1"]').val().trim(),
             'postcode1': tjq('[name="postcode1"]').val().trim(),*/
            // }  */


            if(credittype=="airline"){
                para['booking']["card"] = {
                    'name': tjq('[name="cc_name"]').val().trim(),
                    'number': cardNumber,
                    'last4': cardNumber.substring(cardNumber.length - 4),
                    'exp_year': expiredyear,
                    'exp_month': expiredmonth,
                    'brand': creditcardType,
                    'seccode': tjq('[name="cc_cvc"]').val().trim(),
                    'address1': tjq('[name="address1"]').val().trim(),
                    'address2': tjq('[name="address2"]').val().trim(),
                    'city1': tjq('[name="city1"]').val().trim(),
                    'province2': tjq('[name="province2"]').val().trim(),
                    'country1': tjq('[name="country1"]').val().trim(),
                    'postcode1': tjq('[name="postcode1"]').val().trim(),

                };
                para['creditcardProviders'] ='airlineSystem';

            }
            else if(credittype=="stripe"){
                para['booking']["card"] = {
                    'name': tjq('[name="cc_name"]').val().trim(),
                    'number': cardNumber,
                    'last4': cardNumber.substring(cardNumber.length - 4),
                    'exp_year': expiredyear,
                    'exp_month': expiredmonth,
                    'brand': creditcardType,
                    'seccode': tjq('[name="cc_cvc"]').val().trim(),
                };

                para['creditcardProviders'] ='stripe';
            }



            //para['creditcardProviders'] = credittype;
            console.log(para);

            /*            if ((para['booking']['card']["address1"] == "")||(para['booking']['card']["city1"] == "")||(para['booking']['card']["province2"] == "")||(para['booking']['card']["country1"] == "")||(para['booking']['card']["postcode1"] == "")||(nametoken==false)||(cvc==false)||(expiry==false)||(cardnumber==false))
             {
             RemoveItem(tjq('[name="address1"]'));
             //RemoveItem(tjq('[name="address2"]'));
             RemoveItem(tjq('[name="city1"]'));
             RemoveItem(tjq('[name="province2"]'));
             RemoveItem(tjq('[name="country1"]'));
             RemoveItem(tjq('[name="postcode1"]'));

             if(para['booking']['card']["address1"] == "")
             ShowFormerror(tjq('[name="address1"]'));
             //if(msg['booking']['card']["address2"] == "")
             //ShowFormerror(tjq('[name="address2"]'));
             if(para['booking']['card']["city1"] == "")
             ShowFormerror(tjq('[name="city1"]'));
             if(para['booking']['card']["province2"] == "")
             ShowFormerror(tjq('[name="province2"]'));
             if(para['booking']['card']["country1"] == "")
             ShowFormerror(tjq('[name="country1"]'));
             if(para['booking']['card']["postcode1"] == "")
             ShowFormerror(tjq('[name="postcode1"]'));
             if(nametoken==false) {
             alert("your name input is not correct, please enter again, keep mind that only letters are accepted");

             //tjq('div[class="cc-field filled valid"]').attr("class",'cc-field filled invalid');
             //console.log(tjq('[name="cc_name"]'));
             //console.log(tjq(".cc_name").parent());
             tjq('input[class="cc-name"]').parent().attr("class",'cc-field filled invalid');
             }
             tjq('#showreminder').css('display','');
             tjq('#iamdongpaying').hide();
             tjq('#submitpayment').html('<a href="#" id="submitpayment" class="button button-fill" style="word-spacing: 20px;letter-spacing: 2px;" >Submit Payment </a>');
             }
             else{   */
            FT_c_charge_ticket(para, function (chargers) {
                console.log(chargers);


                if (chargers != undefined && chargers[0] != 'error') {
                    tjq.get(cleanUrl, function (data) {
                        //tjq('#orderid').text(msg.orderid);
                        //tjq('#PNR').text(msg.PNR);
                        myApp.popup('.popup-paysuccess');
                    });


                } else {
                    //tjq(this).html();

                    myApp.alert(stripeErrors[chargers[1]], "Unsuccessful");
                    //console.log(tjq(this));

                    tjq('#iamdongpaying').hide();
                    tjq('#submitpayment').html('<a href="#" id="submitpayment" class="button button-fill" style="word-spacing: 20px;letter-spacing: 2px;" >Submit Payment </a>');
                    tjq('#progressbar1').css("display", "none");
                }
            }, function (value) {
                console.log(value);
                tjq('#progressbar1').css("display", "inline");
                tjq('#probar1').attr('data-progress', value);
                var progress = tjq('#probar1').attr('data-progress');
                var progressbar = tjq('#probar1');
                myApp.setProgressbar(progressbar, progress);


            });
        }


        function vlidateCoupon(f) {
            tjq('[id^=coupon_info]').hide();
            tjq('#coupon_extra_error').html("");
            tjq('#coupon_amt').html('');
            if (tjq('#couoncode').val().length > 0) {
                var obj={};
                obj.totalfee=request.totalfee;
                obj.currency=request.currency;
                obj.couponcode =tjq('#couoncode').val();
                FT_c_validateCoupon(obj, function(result) {

                    //console.log(result[1].currency);
                    //console.log(result[1].amt);

                    if (result[0] == 'success') {
                        tjq('#coupon_amt').html(result[1].currency + ' ' + result[1].amt);
                        tjq('#coupon_info_success').show();

                        if(request.currency==("CAD")){
                            var currentprice=request.totalfee;
                            var pricechange=parseFloat(currentprice)-parseFloat(result[1].amt);
                            tjq('#totalamount').text(pricechange);
                            request.coupon=result[1].amt;
                            param=tjq('#couoncode').val();
                            //return param;
                        }

                        else if(request.currency == 'AUD' || request.currency == 'NZD'||request.currency == 'EUR'||request.currency == 'GBP'||(request.currency=='USD')){
                            var currentprice = request.totalfee;
                            var pricechange = parseFloat(currentprice) - parseFloat(result[1].amt);
                            tjq('#totalamount').text(pricechange);
                            request.coupon = result[1].amt;
                            var atos = tjq('#atosIframe');
                            var atosSrc = '<?php if($env== 'master') echo 'https://mo.air.sinorama.ca/'; else echo 'https://dev.mo.air.sinorama.ca/'; ?>paypal?orderid=' + tjq("#orderid").text()+'&amt='+ request.coupon;
                            tjq(atos).attr('src', atosSrc);




                            //put couponcode into session , only in case of success
                            //var _token = $("input[name=_token]").val();
                            var couponcode=tjq('#couoncode').val();
                            var couponamt=request.coupon;
                            //console.log(couoncode);

                        /*
                            tjq.ajax({
                                url: "/order",
                                type: "get",
                                data: { couponcode:couponcode, couponamt:couponamt},
                                datatype: "json",
                                success: function (data) {

                                },
                                error: function (data) {
                                },
                                beforeSend: function () {
                                },
                                complete: function (data) {
                                    //window.location.reload(true)
                                    console.log(data);
                                }
                            });*/

                            setCookie('coupon',couponcode);

                        }


                        if (typeof f == "function") {
                            f();
                        }
                    } else {
                        tjq('#coupon_info').show();
                        if (typeof f == "function") {
                            //tjq('#submit-errors').html('<i class="fa fa-times"></i>' + "<?php// echo lang("您输入了优惠券，但验证不通过") ?>Verification failed");
                            alert("You typed the coupon, but it is not available. If you don't have a coupon, leave it blank.");
                            tjq('#submitpayment').show();
                            tjq('#iamdongpaying').hide();
                            tjq('#submitpayment').html('<a href="#" id="submitpayment" class="button button-fill" style="word-spacing: 20px;letter-spacing: 2px;" >Submit Payment </a>');
                        }
                        //var param="";
                        //return param;
                        tjq('#couoncode').val('');
                    }
                });
            } else {
                if (f == undefined) {
                    //tjq('#coupon_extra_error').html("--<?php// echo lang('请输入优惠券号') ?>");
                    alert("please enter coupon");
                    tjq('#coupon_info').show();
                } else {
                    f();
                }
            }

        }

        document.getElementById("gotohomepage").onclick = function () {
            if('<?php echo $env ?>' == 'master')
            location.href = "https://mo.air.sinorama.ca/";
            else
            location.href = "https://dev.mo.air.sinorama.ca/";
        }


         function ModifyToTwoDigits(input) {
             if(parseInt(input)<10)
                 return '0'+input;
             else return input;
         }
    });




</script>

@endsection

