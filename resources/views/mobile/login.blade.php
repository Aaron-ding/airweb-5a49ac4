@extends('mobile.layouts.mainframe')
@section('extracss')
    <!-- Path to your custom app styles-->
    <link rel="stylesheet" href="/dist/css/search-app.css">
@endsection
@section('content')

    <div class="views">
        <!-- Your main view, should have "view-main" class -->
        <div class="view view-main">

        @include('mobile.layouts.parts.navbar')

        <!-- Pages container, because we use fixed-through navbar and toolbar, it has additional appropriate classes-->
            <div class="pages toolbar-through navbar-through">

                <!-- Page, "data-page" contains page name -->
                <div data-page="mylogin" class="page search">
                    <!-- Scrollable page content -->
                    <div class="page-content" style="background: #fff">

                        <div class="list" style="padding:25px 15px 0px 20px">
                            <div id="divsigninall" class="" style="margin-left:10px; <?php if($action!="login")echo('display:none;');?>  ">
                                <div id="divsignin">
                                    <form id="submitlogin" method="POST" action="/login">
                                        {{ csrf_field() }}
                                        <h3 class="" style="text-align: center">
                                            @lang("language.WelcomeLogin")
                                        </h3>
                                        <div class="row">
                                            <div class="col-95" style="padding: 15px 0px 5px 0px">
                                                @lang("language.Emailaddress")
                                                <span class="logerrorinf" id="loginerroremail">
                                            @lang("language.signuperror4")
                                        </span>
                                                <span class="logerrorinf" id="loginerrorgen1">
                                            @lang("language.generalerror2")
                                        </span>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <input type="text" id="user_email" class="col-95" style="font-size: 15px; height: 35px; padding-left: 5px"
                                                   name="email" value="{{ old('email') }}" placeholder="email">
                                        </div>
                                        <div class="row" style="padding: 20px 0px 5px 0px">
                                            <div class="col-95">
                                                @lang("language.Password")
                                                <span class="logerrorinf" id="loginpasswordemail">
                                            @lang("language.signuperror5")
                                        </span>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <input type="password" id="user_pass" class="col-95" style="font-size: 15px; height: 35px; padding-left: 5px"
                                                   name="password" placeholder="@lang('language.Password')">
                                        </div>
                                        <div class="row" style=" padding-top: 7px;">
                                            <div class="left">
                                            <label class="checkbox">
                                                <!-- Checked by default -->
                                                <input type="checkbox" id="loginrememberme" name="remember" checked>
                                                <i class="icon-checkbox"></i>
                                            </label>@lang('language.Rememberme')
                                            </div>
                                            <div class="right text-right" style="margin-right:16px; padding-top: 4px; font-size: 12px">
                                                <a id="btnforgotpass" href="#" onclick="showForgotPassword();">@lang("language.Forgetpassword")</a>
                                            </div>
                                        </div>
                                        <div class="row" style="margin-top:30px;font-size: 17px;">
                                            <div class="col-95">
                                                <a href="" id="loginsubmit"
                                                   class="button active">@lang("language.Login")</a>
                                            </div>

                                        </div>
                                    </form>
                                </div>
                                <div id="divforgotpass" style="display: none;">
                                    <form id="submitforgotpass" method="POST" action="/password/email">
                                        {{ csrf_field() }}
                                        <div class="row">
                                            <h4>@lang("language.Forgetpassword")?</h4>
                                        </div>
                                        <div class="row">
                                            @lang("language.forgetinfo"):  <br>     <span class="logerrorinf" id="fgterroremail"> @lang("language.signuperror4")  </span>

                                        </div>
                                        <div class="row">
                                            <input id="forget_email" type="text" class="col-95"
                                                   style="font-size: 18px; height: 30px;" name="email" placeholder="email">
                                        </div>
                                        <div class="row" style="margin-top:10px">
                                            <div class="col-45">
                                                <a href="#" id="forgetbuttonsubmit" class="button button-fill color-red">@lang("language.submit")</a>
                                            </div>
                                            <div class="col-45">
                                                <a href="#" onclick="cancelForgotPassword();"
                                                   class="button button-fill color-blue">@lang("language.Cancel")</a>
                                            </div>
                                            <div class="col-10">
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <div id="divselsignup">
                                    <div class="row" style="text-align: center">
                                        <div class="col-95" style="color: grey; padding-top: 25px">------------------ Or ------------------</div>
                                        <div class="col-95" style="padding-top: 25px; font-size: 12px">
                                            @lang("language.Noaccount")
                                            <a id="showsignup" href="">@lang("language.Registerhere")</a>
                                        </div>

                                    </div>
                                </div>
                            </div>

                            <div id="divsignupall" style="<?php if($action!="register")echo('display:none;');?> " >
                                <form id="submitsignup" class="signupoptions" method="post" action="/register">
                                    {{ csrf_field() }}
                                    <h3 style="text-align: center">
                                        <strong>@lang("language.welcomesignup")</strong>
                                    </h3>
                                    <div class="list" style="padding: 20px 0px 5px 0px">
                                        <div class="row">
                                            @lang("language.inputun"):
                                        </div>
                                        <div class="row">
                                            <input id="signupusername" type="text" class="col-95" value="{{ Session::get('oldusername') }}"
                                                   style="margin-top:5px;height: 35px;" name="username"
                                                   placeholder="@lang('language.UN')"/>
                                        </div>
                                        <span class="logerrorinf" id="signuperror1">
                                            @lang('language.signuperror1')
                                        </span>
                                        <span class="logerrorinf" id="signuperror3">
                                            @lang('language.signuperror3')
                                        </span>
                                        <span class="logerrorinf" id="signuperror8">
                                            @lang('language.signuperror8')
                                         </span>
                                        <div class="row" style="margin-top:10px;">
                                            @lang('language.YEA'):
                                        </div>

                                        <div class="row">
                                            <input id="signupemail" type="email" name="email" class="col-95"
                                                   placeholder="@lang('language.Emailaddress')" required=""
                                                   value="{{ Session::get('oldemail') }}" style="margin-top:5px; height:35px;"/>
                                        </div>
                                        <span class="logerrorinf" id="signuperror4" >
                                        @lang('language.signuperror4')
                                    </span>
                                        <span class="logerrorinf" id="signuperror6">
                                        @lang('language.signuperror6')
                                    </span>

                                        <div class="row" style="margin-top:10px;">
                                            @lang('language.SetPassword'):
                                            <span class="logerrorinf" id="signuperror5">
                                            @lang('language.signuperror5')
                                        </span>
                                        </div>

                                        <div class="row">
                                            <input id="signuppass" placeholder="@lang('language.Password')" class="col-95" required=""
                                                   style="margin-top:5px;height:35px;" type="password"
                                                   name="password"/>
                                        </div>

                                        <div class="row" style="margin-top:10px;">
                                            @lang('language.Password2'):
                                            <span class="logerrorinf" id="signuperror7">
                                            @lang('language.signuperror7')
                                        </span>
                                        </div>
                                        <div class="row">
                                            <input id="signuppass2" placeholder="@lang('language.Password')" class="col-95"
                                                   required="" style="margin-top:5px;height:35px;"
                                                   type="password" name="password2"/>
                                        </div>

                                        <div class="block block-strong">
                                            <div class="block block-strong">
                                                <p style="font-size: 12px">
                                                    <!-- Single chekbox item -->
                                                    <label class="checkbox">
                                                        <!-- Checked by default -->
                                                        <input type="checkbox" id="signupreadterm" name="my-checkbox"
                                                               value="Books" checked="checked">

                                                        <i class="icon-checkbox"></i>
                                                    </label>@lang('language.agreement1')<a href="" id="loginpolicylink">@lang('language.agreement2')</a>
                                                </p>
                                            </div>

                                        </div>

                                        <div class="row" style="margin-top:15px">
                                            <div class="col-95">
                                                <a href="#" id="signupbuttonsubmit" class="button button-fill color-red">@lang("language.Register")</a>
                                            </div>

                                        </div>
                                    </div>
                                </form>
                                <div style="height:5px;"> </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <input type="hidden" name="_token" id="csrf_token" value="{{csrf_token()}}">
            <div class="toolbar">
                <div class="toolbar-inner">
                    <!-- Toolbar links -->
                    <a href="tel:1-866-255-2188" class="external">
                        <img src="dist/img/phone_u6.png" height="12" style="padding-right: 5px">1-866-255-2188
                    </a>

                    <a href="mailto:service@sinoramagroup.com" class="external">
                        <img src="dist/img/u115.png" height="12" style="margin-left: 0px;padding-right: 5px">
                        service@sinoramagroup.com
                    </a>
                </div>
            </div>
        </div>

    </div>

    <div class="popup popup-policy-detail" style="background: #efeff4">
        <div class="page">
            <div class="page-content">
                <div class="navbar" style="position: fixed; top: 0;left: 0;  z-index: 999;">
                    <div class="navbar-inner">
                        <div class="left sliding">
                            <a href="#" class="link icon-only close-popup">
                                <i class="icon icon-back"></i><span class="">@lang("language.Close")</span>
                            </a>
                        </div>
                        <div class="center sliding" style="left: -30px;">@lang("language.agreement2")</div>
                        <div class="right sliding" style="width:39px;">&nbsp;</div>
                    </div>
                </div>
                <div class="card" style="margin-top: 50px; padding:5px;">
                    <div class="card-content" id="signuppolicydetail">
                        @lang("policy.text");
                    </div>
                </div>
                <div class="row">
                    <div class="col-5"></div>
                    <a href="" id="policydetailagree" class="col-90 button button-fill color-blue">
                        @lang("language.Agree")
                    </a>
                    <div class="col-5"></div>
                </div>
                <br>
            </div>
        </div>
    </div>
    <script>
        @if (\Session::get('loginerror')=='notmatch')
        tjq('#loginerrorgen1').show();
        @endif
        @if (\Session::get('signuperror')=='usernameused')
        tjq('#signuperror3').show();
        @endif
        @if (\Session::get('signuperror')=='emailused')
        tjq('#signuperror6').show();
        @endif
    </script>
@endsection


@section('extrascript')
    <script>
        var pushbtnsignup=0;
        tjq('#showsignup').click(function(e){
            tjq('#divsignupall').show();
            tjq('#divsigninall').hide();
            pushbtnsignup=1;
            tjq('.logerrorinf').hide();
        });

        tjq('#loginpolicylink').click(function(e){
            myApp.popup(".popup-policy-detail");
        });

        // åˆ¤æ–­è¾“å…¥æ˜¯å¦æ˜¯ä¸€ä¸ªç”± 0-9 / A-Z / a-z ç»„æˆçš„å­—ç¬¦ä¸²
        function isalphanumber(str)
        {
            var result=str.match(/^[a-zA-Z]\w{2,14}$/);
            if(result==null) return false;
            return true;
        }

        tjq('#signupbuttonsubmit').click(function(e){

            if(!(tjq('#signupreadterm').prop('checked'))){
                return;
            }
//            tjq('#divsignupall').hide();
//            tjq('#divsigninall').show();
            tjq('.logerrorinf').hide();

            var username=trim(tjq("#signupusername").val());
            var email=tjq("#signupemail").val();

            var password=tjq("#signuppass").val();
            var password2=tjq("#signuppass2").val();
            if(username.length<3 || username.length>15){
                tjq('#signuperror1').show();
                return;
            }
            if(isChinese(username) || (!isalphanumber(username))){
                tjq('#signuperror8').show();
                return;
            }
            if(email=="" || (!isEmail(email)) ) {
                tjq('#signuperror4').show();
                return;
            }
            if(password.length<6){
                tjq('#signuperror5').show();
                return;
            }
            if(password2!=password){
                tjq('#signuperror7').show();
                return;
            }

            tjq('#submitsignup').submit();
        });
        tjq('#signupreadterm').change(function(){
            if(tjq('#signupreadterm').prop('checked')){
                tjq('#signupbuttonsubmit').removeClass('color-gray').addClass('color-red');
            }
            else{
                tjq('#signupbuttonsubmit').removeClass('color-red').addClass('color-gray');;
            }
        });

        tjq('#signupbuttoncancel').click(function(e){
            if(pushbtnsignup==1) {
                tjq('#divsignupall').hide();
                tjq('#divsigninall').show();
                tjq('.logerrorinf').hide();
                pushbtnsignup=0;
            }
            else
                cancelReturn();
        });

        tjq('#policydetailagree').click(function(e) {
            tjq('#signupreadterm').prop('checked','true');
            myApp.closeModal(".popup-policy-detail");
        });

        tjq('#forgetbuttonsubmit').click(function (e) {
            var email=tjq("#forget_email").val();
            tjq('.logerrorinf').hide();
            if(isEmail(email)) {
                tjq('#submitforgotpass').submit();
                tjq('#divsignin').show();
                tjq('#divforgotpass').hide();
            }
            else{
                tjq('#fgterroremail').show();
            }
        });
        /*$$('#showmyorders').click(function (e) {
            myApp.closePanel();
            mainView.router.loadPage('/orders');
        });
         */

        function showForgotPassword(){
            tjq('#forget_email').val("");
            tjq('#divforgotpass').show();
            tjq('#fgterroremail').hide();
            tjq('#divsignin').hide();
            tjq('.logerrorinf').hide();
        }

        function cancelForgotPassword(){
            tjq('#divforgotpass').hide();
            tjq('#divsignin').show();
            tjq('.logerrorinf').hide();
        }

        function cancelReturn(){
            window.history.back();
        }

        function isEmail(email) {
            var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
            return regex.test(email);
        }

        tjq('#loginsubmit').click(function(e){
            e.preventDefault();
            //initialize
            tjq('.logerrorinf').hide();

            var email=tjq("#user_email").val();

            var password=tjq("#user_pass").val();

            if(((email!="")&&(password!="")&&(isEmail(email))&&(password.length>5))) {
                tjq('#submitlogin').submit();
            }
            if(!isEmail(email)){
                tjq('#loginerroremail').show();
            }
            if(password.length<6){
                tjq('#loginpasswordemail').show();
            }
        });


        // --------------  Init -----------------------------
        tjq(document).ready(function () {
            tjq('input[type="text"], input[type="password"], input[type="email"]').on('focus', function () {
                var target = this;
                setTimeout(function(){
                    target.scrollIntoViewIfNeeded();
                    setTimeout(function(){
                        target.scrollIntoViewIfNeeded();
                        console.log('scrollIntoViewIfNeeded');
                    },400);
                },300);
                tjq('.toolbar').hide();
            });
        });
    </script>
@endsection