<?php

require 'simple_html_dom.php';

date_default_timezone_set('America/Montreal');

class Myatos {

    var $code;
    var $error;
    var $message;
    
    var $pathfile = "pathfile=/usr/local/paris_payment/param/pathfile";
    var $binhome = "/usr/local/paris_payment/bin";
    
    var $responsedata;
    var $requestdata;
    
    function buildRequest($para) {
        //echo implode(" ",$para);
        $parm = "merchant_id=053998251200011";
        
        $parm = "$parm merchant_country=fr";  
        
        $paylang="en";
        if ($para["lang"]=="french"){
            $paylang="fr";
        }else if ($para["lang"]=="spanish"){
            $paylang="sp";
        }else if ($para["lang"]=="german"){
            $paylang="ge";
        }else if ($para["lang"]=="chinese"){
            $paylang="cn";
        }
        
        $parm="$parm language=" . $paylang;
        $parm = "$parm amount=" . $para["amount"];
        $parm="$parm return_context=". $para["returncontext"];
        $parm = "$parm currency_code=978";
        $parm="$parm order_id=". $para["orderid"];
        $parm = "$parm " . $this->pathfile;
        $parm = "$parm normal_return_url=". $para["normal_return_url"];
        $parm = "$parm cancel_return_url=". $para["cancel_return_url"];
        $parm = "$parm automatic_response_url=". $para["automatic_response_url"];
       
        
        
        $path_bin = $this->binhome . "/request";
        $parm = escapeshellcmd($parm);
        $result = exec("$path_bin $parm");
        //echo $result;

        //	sortie de la fonction : $result=!code!error!buffer!
        //	    - code=0	: la fonction génère une page html contenue dans la variable buffer
        //	    - code=-1 	: La fonction retourne un message d'erreur dans la variable error
        //On separe les differents champs et on les met dans une variable tableau

        $tableau = explode("!", "$result");

        //	récupération des paramètres

        $this->code = $tableau[1];
        $this->error = $tableau[2];
        $this->message = $tableau[3];
        $rs = array();
        //echo $this->code;
        //echo $this->error;

        if ($this->isSuccess()) {
            $html = str_get_html($this->message);
            $rs["action"] = $html->find('FORM', 0)->action;
            $rs["requestdata"] = $html->find('INPUT', 0)->value;
            $this->requestdata=$rs;           
        } 
        $this->requestdata=$rs;
        //echo $this->isSuccess();
        return $this->isSuccess();
    }

    function decodeResponse($para) {
        $rs = array();
        $parm = "merchant_id=053998251200011";

        $message = "message=" . $para["DATA"];
        $pathfile = $this->pathfile;
        $path_bin = $this->binhome . "/response";
        $message = escapeshellcmd($message);
        $result = exec("$path_bin $pathfile $message");

        $tableau = explode("!", $result);

        $this->code=$rs["code"] = $tableau[1];
        $this->error=$rs["error"] = $tableau[2];
        $rs["merchant_id"] = $tableau[3];
        $rs["merchant_country"] = $tableau[4];
        $rs["amount"] = $tableau[5];
        $rs["transaction_id"] = $tableau[6];
        $rs["payment_means"] = $tableau[7];
        $rs["transmission_date"] = $tableau[8];
        $rs["payment_time"] = $tableau[9];
        $rs["payment_date"] = $tableau[10];
        $rs["response_code"] = $tableau[11];
        $rs["payment_certificate"] = $tableau[12];
        $rs["authorisation_id"] = $tableau[13];
        $rs["currency_code"] = $tableau[14];
        $rs["card_number"] = $tableau[15];
        $rs["cvv_flag"] = $tableau[16];
        $rs["cvv_response_code"] = $tableau[17];
        $rs["bank_response_code"] = $tableau[18];
        $rs["complementary_code"] = $tableau[19];
        $rs["complementary_info"] = $tableau[20];
        $rs["return_context"] = $tableau[21];
        $rs["caddie"] = $tableau[22];
        $rs["receipt_complement"] = $tableau[23];
        $rs["merchant_language"] = $tableau[24];
        $rs["language"] = $tableau[25];
        $rs["customer_id"] = $tableau[26];
        $rs["order_id"] = $tableau[27];
        $rs["customer_email"] = $tableau[28];
        $rs["customer_ip_address"] = $tableau[29];
        $rs["capture_day"] = $tableau[30];
        $rs["capture_mode"] = $tableau[31];
        $rs["data"] = $tableau[32];
        $rs["order_validity"] = $tableau[33];
        $rs["transaction_condition"] = $tableau[34];
        $rs["statement_reference"] = $tableau[35];
        $rs["card_validity"] = $tableau[36];
        $rs["score_value"] = $tableau[37];
        $rs["score_color"] = $tableau[38];
        $rs["score_info"] = $tableau[39];
        $rs["score_threshold"] = $tableau[40];
        $rs["score_profile"] = $tableau[41];
        $rs["threed_ls_code"] = $tableau[43];
        $rs["threed_relegation_code"] = $tableau[44];
        $this->responsedata=$rs;
        return $this->isSuccess();
    }
    
    function isSuccess()
    {
        if ($this->code==0) return true;
        return false;
    }

}

?>
