<!DOCTYPE html>
<html>
<head>
    <!-- Required meta tags-->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimum-scale=1, user-scalable=no, minimal-ui,">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="keywords" content="Flight,cheap flights,air flights,international flights,domestic flights,airline tickets,airplane tickets,discount flights,flight deals,discount air ticket,Air Travel,Air trip,cheap air travel,business travel,last minute flights,flight tickets,cheap plane tickets,cheap airline tickets,cheap airline">
    <meta name="author" content="Yuqing Ouyang, Kun Qian, Liang Yang, Miao Yu">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <!-- Your app title -->
    <title>Sinorama Air Tickets</title>
    <link rel="bookmark"  type="image/x-icon"  href="favicon.ico"/>
    <link rel="shortcut icon" href="favicon.ico">
    <!-- Path to Framework7 Library CSS, iOS Theme -->
    <link rel="stylesheet" href="dist/css/framework7.ios.min.css">
    <link rel="stylesheet" href="dist/css/framework7-icons.css">
    <!-- Path to Framework7 color related styles, iOS Theme -->
    <link rel="stylesheet" href="dist/css/framework7.ios.colors.min.css">
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-17008322-14"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());
        gtag('config', 'UA-17008322-14');
    </script>

    <script src="https://momentjs.com/downloads/moment-with-locales.min.js"></script>
    <script src="https://momentjs.com/downloads/moment-timezone.js"></script>
    <script src="https://momentjs.com/downloads/moment-timezone-with-data.js"></script>
    <script src="https://momentjs.com/downloads/moment-timezone-with-data-2012-2022.js"></script>

@yield('extracss')
<!-- Path to Framework7 Library JS-->

</head>
<body class="theme-red"><!--皮肤颜色-->

<script type="text/javascript" src="dist/js/framework7.js"></script>
<script type="text/javascript" src="dist/js/date.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script>
    var tjq=jQuery.noConflict();
    var lang = "@lang('language.Lang')";   // app()->getLocale();
    var myApp = new Framework7();
    var $$ = Dom7;
    var login_userid="{{Auth::ID()}}";
    var showcurrencyonnavbar=0;
    var currencyone="<?php if(!session()->has("currency")){session(['currency'=>'CAD']);}
                       echo(session("currency")); ?>";
</script>

<!-- Status bar overlay for full screen mode (PhoneGap) -->
<div class="statusbar-overlay"></div>
<!-- Panels Overlay -->
<div class="panel-overlay"></div>
<div id="langjudge" style="display: none;">@lang('language.One')</div>

<div class="panel panel-right panel-cover">
    <div data-page="home" class="page toolbar-fixed">
        <!-- page-content -->
        <div class="page-content" style="background: #eeeeee">
            <!-- Part 1 After Login, Show the Name and Logout -->
            <div class="row" style="height:75px; overflow: hidden; background:#e04040;">
                <div class="col-20">
                    <img src="/dist/img/pc.png" style="height:40px; width:40px; margin:15px 15px;">
                </div>
                <div class="col-64" style="margin-top:10px;">
                    @if(Auth::check())
                        <!--<strong style="color: #f0f0f0;">@lang("language.WelcomeBack")</strong>-->
                        <h3 style="color: #fff;">{{ucfirst(Auth::user()->username)}}</h3>
                    @else
                        <h3 style="color: #fff;">Personal Center</h3>
                    @endif
                </div>
                <div class="col-15">
                    <a href="#" class="close-panel"><i class="f7-icons size-16" style="margin:20px 0px 0px 0px; color: #fff;">close</i></a>
                </div>
            </div>
            <!-- LogIn & Register -->
            @php
                if (Auth::check()) {
            @endphp
                <div class="card-footer">
                    <div style="padding-left: 5px;"> &nbsp;</div>
                    <a href="/logout" class="external" style="padding-right: 5px;">
                        <i class="icon f7-icons">logout</i>
                        &nbsp;@lang("language.Logout")
                    </a>
                </div>
            @php
                }else{
            @endphp
                <div class="card-footer">
                    <a href="/login" class="external" style="padding-left: 5px;"> @lang("language.Login")</a>

                    <a href="/register" class="external" style="padding-right: 5px;">@lang("language.Register")</a>
                </div>
             @php }  @endphp

            <div class="list-block list-block-search searchbar-found" style="margin-top:0px;">
                <ul style="font-size: 15px">
                    <!-- Home & Order -->
                    <li class="item-divider" style="padding: 4px 8px;">@lang("language.Functions")
                    </li>
                    <li class="item-content">
                        <div class="item-media"><a href="/" class="external">
                            <i class="icon f7-icons">
                                home
                            </i></a>
                        </div>
                        <div class="item-inner">
                            <div class="item-title">
                                <a href="/" class="external">
                                    @lang("language.HP")
                                </a>
                            </div>
                        </div>
                    </li>
            @php
                if (Auth::check()) {
                    /* $headimg="undefined.png";
                    if(isset(Auth::user()->sex) && Auth::user()->sex!==""){
                        $headimg=Auth::user()->sex. ".jpeg";
                    }*/
            @endphp
                    <li class="item-content">
                        <div class="item-media">
                            <a href="/orders" id="showmyorders" class="external">
                                <i class="icon f7-icons">
                                    list
                                </i>
                            </a>
                        </div>
                        <div class="item-inner">
                            <a href="/orders" id="showmyorders" class="external">
                            <div class="item-title">
                                   @lang("language.MyOrder")
                            </div></a>
                        </div>

                    </li>
                    <li class="item-content">
                        <div class="item-media"><a href="/contactinfo" class="external">
                            <i class="icon f7-icons">
                                person
                            </i></a>
                        </div>
                        <div class="item-inner">
                            <div class="item-title">
                                <a href="/contactinfo" class="external">
                                    @lang("language.ContactInformations")
                                </a>
                            </div>
                        </div>
                    </li>
                    <li class="item-content">
                        <div class="item-media"><a href="/passengerinfo" class="external">
                            <i class="icon f7-icons">
                                persons
                            </i></a>
                        </div>
                        <div class="item-inner">
                            <div class="item-title">
                                <a href="/passengerinfo" class="external">
                                    @lang("language.PassengerInformations")
                                </a>
                            </div>
                        </div>
                    </li>
                <!-- <li class="item-content">
                        <div class="item-inner">
                            <div class="item-title">
                                <a href="/mypoints" class="external">
                                    @lang("language.MyPoints")
                                </a>
                            </div>
                        </div>
                    </li> -->
                    <li class="item-content">
                        <div class="item-media"><a href="/resetpwd" class="external">
                            <i class="icon f7-icons">
                                unlock
                            </i></a>
                        </div>
                        <div class="item-inner">
                            <div class="item-title">
                                <a href="/resetpwd" class="external">
                                    @lang("language.ChangePassword")
                                </a>
                            </div>
                        </div>
                    </li>
                    <li class="item-content">
                        <div class="item-media"><a href="/policy" class="external">
                            <i class="icon f7-icons">
                                document_text
                            </i></a>
                        </div>
                        <div class="item-inner">
                            <div class="item-title">
                                <a href="/policy" class="external">
                                    @lang("language.Privacy")
                                </a>
                            </div>
                        </div>
                    </li>
            @php
                }else{
            @endphp
        <!-- Part 2 If without Login and Sing in, Show the link to Login -->
                    <li class="item-content">
                        <div class="item-media">
                            <i class="icon f7-icons">
                                list
                            </i>
                        </div>
                        <div class="item-inner">
                            <div class="item-title">
                                <a href="/orders" id="showmyorders" class="external">
                                    @lang("language.Hisorder")
                                </a>
                            </div>
                        </div>
                    </li>
                    <li class="item-content">
                        <div class="item-media"><a href="/contactinfo" class="external">
                            <i class="icon f7-icons">
                                person
                            </i></a>
                        </div>
                        <div class="item-inner">
                            <div class="item-title">
                                <a href="/contactinfo" class="external">
                                    @lang("language.ContactInformations")
                                </a>
                            </div>
                        </div>
                    </li>
                    <li class="item-content">
                        <div class="item-media"><a href="/passengerinfo" class="external">
                            <i class="icon f7-icons">
                                persons
                            </i></a>
                        </div>
                        <div class="item-inner">
                            <div class="item-title">
                                <a href="/passengerinfo" class="external">
                                    @lang("language.PassengerInformations")
                                </a>
                            </div>
                        </div>
                    </li>
                <!-- <li class="item-content">
                        <div class="item-media">
                            <i class="icon f7-icons">
                                person
                            </i>
                        </div>
                        <div class="item-inner">
                            <div class="item-title">
                                <a href="/mypoints" class="external">
                                    @lang("language.MyPoints")
                        </a>
                    </div>
                </div>
            </li>
                    <li class="item-content">
                        <div class="item-media"><a href="/resetpwd" class="external">
                            <i class="icon f7-icons">
                                unlock
                            </i></a>
                        </div>
                        <div class="item-inner">
                            <div class="item-title">
                                <a href="/resetpwd" class="external">
                                    @lang("language.ChangePassword")
                                </a>
                            </div>
                        </div>
                    </li>  -->
                    <li class="item-content">
                        <div class="item-media">
                            <a href="/policy" class="external">
                                <i class="icon f7-icons">
                                document_text
                            </i></a>
                        </div>
                        <div class="item-inner">
                            <div class="item-title">
                                <a href="/policy" class="external">
                                    @lang("language.Privacy")
                                </a>
                            </div>
                        </div>
                    </li>

            @php }  @endphp
                <!-- currency & language -->
                <li class="item-divider" style="padding: 4px 5px;">@lang("language.Settings")
                </li>
                <!-- currency -->
                <li class="item-content">
                    <div class="item-media">
                        <i class="icon f7-icons">
                            money_dollar
                        </i>
                    </div>
                    <div class="item-inner">
                        <?php if(\session()->get("effroute")=="search") {  ?>
                        <a class="menusetcurrency" href="" onclick="show_select_currency(true);">
                            <div class="item-title">
                                @lang("language.changecurrency")&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            </div></a>
                        <a class="menusetcurrency" href="" onclick="show_select_currency(true);">
                            <div class="item-after">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <?php echo(session("currency")); ?>
                            </div>
                        </a>
                        <?php } else { ?>
                            <div class="item-title" style="color:#777777;">
                                @lang("language.changecurrency")&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            </div>
                            <div class="item-after">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <?php echo(session("currency")); ?>
                            </div>
                        <?php    }   ?>
                    </div>
                </li>

                <li class="menucurrencyselector" style="display:none; padding-left:30px;">
                    <label class="label-radio item-content">
                        <input type="radio" name="current_currency" value="CAD"
                        <?php if (session("currency") == "CAD") echo('checked="checked"');?> >
                        <div class="item-inner">
                            <div class="item-title">CAD</div>
                        </div>
                    </label>
                </li>
                <li class="menucurrencyselector" style="display:none; padding-left:30px;">
                    <label class="label-radio item-content">
                        <input type="radio" name="current_currency" value="USD"
                        <?php if (session("currency") == "USD") echo('checked="checked"');?> >
                        <div class="item-inner">
                            <div class="item-title">USD</div>
                        </div>
                    </label>
                </li>
                <li class="menucurrencyselector" style="display:none; padding-left:30px;">
                    <label class="label-radio item-content">
                        <input type="radio" name="current_currency" value="EUR"
                        <?php if (session("currency") == "EUR") echo('checked="checked"');?> >
                        <div class="item-inner">
                            <div class="item-title">EUR</div>
                        </div>
                    </label>
                </li>
                <li class="menucurrencyselector" style="display:none; padding-left:30px;">
                    <label class="label-radio item-content">
                        <input type="radio" name="current_currency" value="GBP"
                        <?php if (session("currency") == "GBP") echo('checked="checked"');?> >
                        <div class="item-inner">
                            <div class="item-title">GBP</div>
                        </div>
                    </label>
                </li>
                <li class="menucurrencyselector" style="display:none; padding-left:30px;">
                    <label class="label-radio item-content">
                        <input type="radio" name="current_currency" value="AUD"
                        <?php if (session("currency") == "AUD") echo('checked="checked"');?> >
                        <div class="item-inner">
                            <div class="item-title">AUD</div>
                        </div>
                    </label>
                </li>
                <li class="menucurrencyselector" style="display:none; padding-left:30px;">
                    <label class="label-radio item-content">
                        <input type="radio" name="current_currency" value="NZD"
                        <?php if (session("currency") == "NZD") echo('checked="checked"');?> >
                        <div class="item-inner">
                            <div class="item-title">NZD</div>
                        </div>
                    </label>
                </li>

                <!-- language -->
                <li class="item-content" id="menusetlanguage">
                    <div class="item-media">
                        <i class="icon f7-icons">
                            chats
                        </i>
                    </div>
                    <div class="item-inner">
                        <a class="menusetlanguage" href="" onclick="show_select_language(true);">
                            <div class="item-title">
                                @lang("language.changelang")&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            </div>
                        </a>
                        <a class="menusetlanguage" href="" onclick="show_select_language(true);">
                            <div class="item-after">
                                @lang("language.Lang")
                            </div>
                        </a>
                    </div>
                </li>
                <li class="menulanguageselector" style="display:none; padding-left:30px;">
                    <label class="label-radio item-content">
                        <input type="radio" name="current_language" value="French"
                        <?php if (session("locale") == "fr") echo('checked="checked"');?> >
                        <div class="item-inner">
                            <div class="item-title">French</div>
                        </div>
                    </label>
                </li>
                <li class="menulanguageselector" style="display:none; padding-left:30px;">
                    <label class="label-radio item-content">
                        <input type="radio" name="current_language" value="English"
                        <?php if (session("locale") == "en") echo('checked="checked"');?> >
                        <div class="item-inner">
                            <div class="item-title">English</div>
                        </div>
                    </label>
                </li>
                <li class="menulanguageselector" style="display:none; padding-left:30px;">
                    <label class="label-radio item-content">
                        <input type="radio" name="current_language" value="Chinese"
                        <?php if (session("locale") == "cn") echo('checked="checked"');?> >
                        <div class="item-inner">
                            <div class="item-title">Chinese</div>
                        </div>
                    </label>
                </li>

			</ul>
            </div>
        </div>
    </div>
</div>


@yield('content')



<!-- Path to your app js-->

<script type="text/javascript" src="dist/js/my-app.js"></script>


<script>
    @if (\Session::has('Error'))
    myApp.alert($$("<div/>").html("{{ \Session::get('Error') }}").text());
    @endif

    @if (\Session::has('status'))
        var sstr="{!! \Session::get('status') !!}";
        if(sstr=='@lang("language.generalerror2")'){
            tjq('#rstpwdgenerror2').show();
            sstr='@lang("language.errortitle")' +sstr;
        }
        myApp.alert(sstr);
        //myApp.addNotification({title:'Notify', message: sstr });
    @endif
</script>

<?php 
$purl=\URL::current();
if(!(substr($purl,-5)=="login" || substr($purl,-8)=="register" || substr($purl,-8)=="resetpwd" || substr($purl,-5)=="email"
               || substr($purl,-11)=="contactinfo" || substr($purl,-13)=="passengerinfo")){
	echo('<a href="' .$purl. '"> </a>');
	if (getSafeEnv() == 'master'){   ?>
    <script src="https://search.air.sinorama.ca:9999/air_api.js?v=201703303"></script>
    <script src="https://crud.sinorama.ca/getAirportCode.php?v=201807171"></script>
    <script src="https://crud.sinorama.ca/getAirlines.php?v=20170529"></script>
    <script src="https://crud.sinorama.ca/getCountryCode.php?v=201704201"></script>
    <script src="https://air.sinorama.ca/assets/js/Myrequest.js?v=20180129"></script>
	<?php }  else{  ?>
    <script src="https://dev.search.air.sinorama.ca:9999/air_api.js?v=201703303"></script>
    <script src="https://dev.crud.sinorama.ca/getAirportCode.php?v=201807171"></script>
    <script src="https://dev.crud.sinorama.ca/getAirlines.php?v=20170529"></script>
    <script src="https://dev.crud.sinorama.ca/getCountryCode.php?v=201704201"></script>
    <script src="https://dev.air.sinorama.ca/flight_front/assets/js/Myrequest.js?v=20180129"></script>
			   <?php }
			   }
else if(substr($purl,-11)=="contactinfo" || substr($purl,-13)=="passengerinfo"){   
	if (getSafeEnv() == 'master'){      ?> 
    <script src="https://search.air.sinorama.ca:9999/air_api.js?v=201703303"></script>
	<script src="https://crud.sinorama.ca/getAirlines.php?v=20170529"></script>
    <script src="https://crud.sinorama.ca/getCountryCode.php?v=201704201"></script>
    <script src="https://air.sinorama.ca/assets/js/Myrequest.js?v=20180129"></script>
	<?php }  else{  ?>
    <script src="https://dev.search.air.sinorama.ca:9999/air_api.js?v=201703303"></script>
    <script src="https://dev.crud.sinorama.ca/getAirlines.php?v=20170529"></script>
    <script src="https://dev.crud.sinorama.ca/getCountryCode.php?v=201704201"></script>
    <script src="https://dev.air.sinorama.ca/flight_front/assets/js/Myrequest.js?v=20180129"></script>
<?php }	
} ?>

<script>

    function getCityNameByCodeChinese(code){
        if (code in airportcode_array) {
            var tmpobj = airportcode_array[code];
            return tmpobj["CITY"];
        } else {
            return code;
        }
    }

    function isClosed() {
        var closed = false;

        var m = moment.tz("America/Toronto");
        var weekOfToday = m.format('d');
        var hourOfToday = m.format('H');

        if (weekOfToday == 6 || weekOfToday == 0) {
            closed = true;
        } else if (hourOfToday >= 18 || hourOfToday <= 9) {
            closed = true;
        }
        console.log('==================');
        console.log(closed);
        console.log(weekOfToday);
        console.log(hourOfToday);
        console.log('==================');
        return closed;
    }

    function isHolidays() {
        var closed = false;

        var m = moment.tz("America/Toronto");
        var monthOfToday = m.format('M');
        var dateOfToday = m.format('D');

        if ((monthOfToday == 12 && tjq.inArray(dateOfToday, [25, 26]) >= 0) || (monthOfToday == 1 && tjq.inArray(dateOfToday, [1]) >= 0)) {
            closed = true;
        }

        console.log('==================');
        console.log(closed);
        console.log(monthOfToday);
        console.log(dateOfToday);
        console.log('==================');
        return closed;
    }


    function change_language(obj) {
        console.log(obj);
        var locale = obj;
        var _token = tjq("input[name=_token]").val();
        tjq.ajax({
            url: "/language",
            type: "POST",
            data: {locale: locale, _token: _token},
            datatype: "json",
            success: function (data) {

            },
            error: function (data) {
            },
            beforeSend: function () {
            },
            complete: function (data) {
                window.location.reload(true);
                //console.log(data);
            }
        })
    }

    function show_current_lang(language) {
        if (language == "English") {
            $$('#clickEn').css('background', '#1383ff');
            $$('#clickEn').css('color', 'white');
            $$('#clickEn').css('padding', '2px');
            $$('#clickEn').css('font-weight', '600');
        }
        else if (language == "Chinese") {
            $$('#clickCn').css('background', '#1383ff');
            $$('#clickCn').css('color', 'white');
            $$('#clickCn').css('padding', '2px');
            $$('#clickCn').css('font-weight', '600');
        }
        else if (language == "French") {
            $$('#clickFr').css('background', '#1383ff');
            $$('#clickFr').css('color', 'white');
            $$('#clickFr').css('padding', '2px');
            $$('#clickFr').css('font-weight', '600');
        }
    }

    $$('.close-popup-od').on('click',function(){
        myApp.closeModal('.popup-order-detail');
    });

    $$('.panel-right').on('closed', function () {
        $$('.menulanguageselector').css("display","none");
        $$('.menucurrencyselector').css("display","none");
    });

    function changeshowncurrency(obj){
        console.log(obj);
        $$('input[type=radio][value='+obj+']').prop('checked', true);
        $$('.menusetcurrency .item-after').html('&nbsp;&nbsp;&nbsp;&nbsp;'+obj);
        myApp.closePanel();
    }

    function changecurrency(newcurrency){

        console.log(newcurrency);

        var _token = tjq("input[name=_token]").val();
        tjq.ajax({
            url: "/changecurrency",
            type: "POST",
            data: {currency: newcurrency, _token: _token },
            datatype: "json",
            success: function (data) {
                var response=JSON.parse(data);
                if(response["status"]=="Ok"){
                    if(currencyone!=response["currency"]){
                        currencyone=response["currency"];
                    }
                }
                changeshowncurrency(currencyone);
            },
            error: function (data) {
            }
        });
        console.log(currencyone);
        $$('.menucurrencyselector').css("display","none");
    }

    function stopPropagation(e) {
        if (e.stopPropagation)
            e.stopPropagation();
        else
            e.cancelBubble = true;
    }

    $$('input[type=radio][name=current_currency]').on('change', function() {
        changecurrency($$(this).val());
    });

	function show_select_currency(s){
		if(s){
			$$('.menucurrencyselector').css("display","");
			$$('.menusetcurrency').attr("onClick", "show_select_currency(false);");
            show_select_language(false);
		}
		else{
			$$('.menucurrencyselector').css("display","none");
			$$('.menusetcurrency').attr("onClick", "show_select_currency(true);");
		}
	}

    function changemenulanguage(obj){
        console.log(obj);
        $$('input[type=radio][value='+obj+']').prop('checked', true);
        $$('.menusetlanguage .item-after').html('&nbsp;&nbsp;'+obj);
        $$('.menulanguageselector').css("display","none");
    }

    function trim(str){
        return str.replace(/(^\s*)|(\s*$)/g, "");
    }

    function isChinese(str){
        var patrn=/[\u4E00-\u9FA5]|[\uFE30-\uFFA0]/gi;
        // [\u4E00-\u9FA5]è¡¨ç¤ºæ±‰å­—ï¼Œ[\uFE30-\uFFA0]è¡¨ç¤ºå…¨è§’
        if(!patrn.exec(str)){
            return false;
        }
        else{
            return true;
        }
    }

    function isNormalName(str){
        console.log(str);
        if(str.match(/^[a-z][a-z'\u002D]+$/i, str))
            return true;
        else
            return false;
    }

    $$('input[type=radio][name=current_language]').on('change', function() {
        $locale="";
        $newlanguage=$$(this).val();
        switch($newlanguage){
            case "French":
                $locale='fr';break;
            case "English":
                $locale='en';break;
            case "Chinese":
                $locale='cn';break;
        }
        change_language($locale);
        changemenulanguage($newlanguage);
    });

    function show_select_language(s){
        if(s){
            $$('.menulanguageselector').css("display","");
            $$('.menusetlanguage').attr("onClick", "show_select_language(false);");
            show_select_currency(false);
        }
        else{
            $$('.menulanguageselector').css("display","none");
            $$('.menusetlanguage').attr("onClick", "show_select_language(true);");
        }
    }
/*
	$$(document).on('click', function(e) {
		var e = e || window.event; //浏览器兼容性 
		var elem = e.target || e.srcElement;
		while (elem) { 
			if (elem.id && (elem.className == 'menucurrencyselector') ) {
				return;
			}
			elem = elem.parentNode;
		}
		$$('#menucurrencyselector').css('display', 'none');  
//		$$('#menulanguageselector').css('display', 'none');
	});
*/
    $$(document).on('DOMContentLoaded', function(){
        $$('#clickEn').on("click", function (e) {
            e.preventDefault();
            change_language("en");
        });

        $$('#clickCn').on("click", function (e) {
            e.preventDefault();
            change_language("cn");
        });
        $$('#clickFr').on("click", function (e) {
            e.preventDefault();
            change_language("fr");
        });

		changeshowncurrency(currencyone);
/*		$$('.menusetcurrency').on("click", function (e) {
            e.preventDefault();
            show_select_currency(true);
//            stopPropagation(e);
        });*/
		
        show_current_lang(lang);

        $$('#cardfacebook').on("click", function (e) {
            e.preventDefault();
            window.location.assign("https://itunes.apple.com/us/app/sinorama-travel/id1324127839?ls=1&mt=8") ;
        });

        if(showcurrencyonnavbar==1) {
            //$$('#nav_currency').show();
        }
        else{
            $$('#nav_currency').hide();
        }

    });


</script>

@yield('extrascript')

<?php logConsole(); ?>
</body>
</html>
<?php
//session_start();
if(isset($_SERVER['HTTP_REFERER']))
    Session::put('referer',$_SERVER['HTTP_REFERER']);
else
    Session::put('referer','');


?>



