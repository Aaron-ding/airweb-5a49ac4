<div class="navbar navbar_top">

    <div class="navbar-inner">
        <!-- We need cool sliding animation on title element, so we have additional "sliding" class -->
        <div class="left logo_text">
            <a href="/" class="external">
                <img src="/dist/img/logi_u4.png" style="margin-top: 3px">
            </a>
        </div>

        <div class="left text-left" style="padding-bottom: 26px;width:40px display:none;">
            <select id="nav_currency" onchange="changecurrency()" style="display:none;">
                <option value="CAD">CAD</option>
                <option value="USD">USD</option>
                <option value="EUR">EUR</option>
                <option value="GBP">GBP</option>
                <option value="AUD">AUD</option>
                <option value="NZD">NZD</option>
            </select>
        </div>

                                                                                                                                                                                                                                                                                           <!-- right-->
        <div class="left text-left" style="height:75px; padding-bottom:26px;">
            <span style="font-size:12px">
                <ul style="margin-left: -28px;">
                    <li style="display: inline-block;zoom: 1; width: 23px;">
                        <a href="#" id="clickEn" style="font-size: 13px;"> EN</a>
                    </li>
                    <li style="display: inline-block;zoom: 1; width: 20px;">
                        <a href="#" id="clickCn" style="font-size:13px"> 中</a>
                    </li>
                    <li style="display: inline-block;zoom: 1; width: 26px;">
                        <a href="#" id="clickFr" style="font-size:13px"> FR</a>
                    </li>
                </ul>
            </span>
        </div>
            @php
                if (! Auth::check()) {
            @endphp
        <div class="right text-right" style="height:75px; padding-right: 2px;">
            <a href="/login" class="external">
                <img src="../dist/img/u707.png" width="23" alt="login">
            </a>
        </div>
            @php
                }
            @endphp

        <div class="right text-right" style="height:75px; padding-right: 10px;">
            <a href="#" data-panel="right" class="open-panel">
                <img src="../dist/img/u107.png" width="22" alt="menu">
            </a>
        </div>
    </div>
</div>
<!--   part navbar ends!   -->