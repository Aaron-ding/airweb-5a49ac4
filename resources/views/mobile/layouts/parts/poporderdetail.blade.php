
<div class="popup popup-order-detail" style="background: #efeff4">
    <div class="page navbar-through_od">
        <div class="navbar navbar_top_od no-shadow">
            <div class="navbar-inner">
                <div class="left sliding">
                    <a href="#" class="link close-popup-od" >
                        <i class="icon icon-back"></i><span class="">@lang('language.Close')</span></a>
                </div>
                <div id="odpoptitle" class="center sliding" style="left: -30px;">
                    @lang("language.InternaryDetails")
                </div>
                <div class="right sliding" >&nbsp;
                <!--    <a id="odETicketLink" href="" target="_blank" style="display:none;">@lang('language.fullscreen')
                        <img src="/dist/img/fullscreen.png" height="24px" alt="Fullscr">
                    </a>   -->
                </div>

            </div>
        </div>

        <div class="page-content">
            <div class="content-block-search">
                <div class="buttons-row" style="margin: 0px 10px 30px 10px">
                    <a href="#odtabitinery" class="tab-link button color-red active">@lang('language.Itinery')</a>
                    <a href="#odtabpassenger" class="tab-link button color-red">@lang('language.Passengers')</a>
                    <a href="#odtabcontact" class="tab-link button color-red">@lang('language.Contact')</a>
                <!--
                    <a href="#odtabeticket" class="button tab-link" style="color: #C1071E">@lang('language.E-ticket')</a>
-->
                </div>
            </div>
            <div class="tabs">
                <div id="odtabitinery" class="tab active">
                    <p>
                        <img src="dist/img/u129.png"  style="padding: 0 10px 0 20px">@lang('language.RYT')
                    </p>
                    <!-- departure/return city and date -->
                    <div class="card">
                        <div class="card-content">
                            <div class="card-footer">
                                <h4 style="margin:0px 15px;">
                                    <span id="odHeadDeparture"></span>
                                    <img id="odroundpicture1" src="../dist/img/search_list___departure/return_u321.png"
                                         style="margin-left:12px;margin-right: 12px;display:none">
                                    <img id="odsinglepicture1"  src="../dist/img/baggages/u1391.png" style="margin-left:12px;margin-right: 12px;display:none;">
                                    <span id="odHeadArrive"></span>
                                </h4>
                            </div>
                        </div>
                    </div>


                    <div class="content-block"  id="oditinerayinfo">

                        <!-- departure city and date -->
                        <div class="content-block-title" style="padding: 10px 0px;">
                            <img src="dist/img/search_list___review/Departure-Grey.png" width="20" style="margin-left: 10px">
                            @lang('language.Yd')
                        </div>
                        <div class="card review-card">
                            <!-- Departure detail top -->
                            <div class="card-header">
                                <!-- Date and trip city -->
                                <div class="content-block-title">
                                    <span id="odDepartureDate"></span>
                                    <span id="odDeparturecity"> </span>
                                    <img src="dist/img/baggages/u1391.png"alt="">
                                    <span id="odArrivalcity"></span>
                                </div>
                                <!-- Time and Stop -->
                                <div class="more-title">
                                    <span id="odtraveltime"></span>   ,
                                    <span id="odtravelstops"></span> @lang('language.Stop')
                                </div>
                            </div>
                            <!-- Departure content -->
                            <div class="card-content" id="oditilistcontainer">
                            </div>
                            <div id="t_oditilistfinal" style="display:none;">
                                <!-- 1st -->
                                <div class="timeline" id="odsegtemplatefinal" style="display: none;">
                                    <!-- From -->
                                    <div class="timeline-item">
                                        <!-- Date and time -->
                                        <div class="timeline-item-date">
                                            <span><span id="oddeparturedate1"></span></span>
                                            <b><span id="oddeparttime2"></span></b>
                                        </div>
                                        <div class="timeline-item-divider"></div>
                                        <!-- City and airport -->
                                        <div class="timeline-item-content">
                                            <div class="timeline-item-time">
                                                <span id="oddepartureport"></span><br>
                                                <span id="oddepartureterminalport"></span>
                                            </div>

                                            <!-- airplant NO. and class -->
                                            <div class="timeline-item-text">
                                                <div>
                                                    <span id="odairinfo"></span>|<span id="odaircabine"></span>
                                                </div>
                                                <div>
                                                    <span id="oditinerayduration"></span>|<span id="odairtype"></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- To -->
                                    <div class="timeline-item">
                                        <!-- Date and time -->
                                        <div class="timeline-item-date">
                                            <span><span id="odarriveddate"></span></span>
                                            <b><span id="odarrivedtime"></span></b>
                                        </div>
                                        <div class="timeline-item-divider"></div>
                                        <!-- City and airport -->
                                        <div class="timeline-item-content">
                                            <div class="timeline-item-time">
                                                <span id="odarrivalport"></span>
                                            </div>
                                            <div class="timeline-item-time">
                                                <span id="odarrivalterminalport"></span>

                                            </div>
                                        </div>
                                    </div>

                                </div>

                                <div class="timeline" id="odstoptemplatefinal"  style="display:none;padding-top: 10px;padding-bottom: 10px;">

                                    <div class="timeline-item">
                                        <div class="timeline-item-date">
                                            <span id="odstoptime1" style="color:red"></span>
                                        </div>
                                        <div class="timeline-item-divider"></div>
                                        <div class="timeline-item-content">
                                            <span id="odstopdesc1"></span>
                                        </div>
                                    </div>

                                </div>
                            </div>

                        </div>

                        <!-- Return city and date -->
                        <div class="content-block-title" id="odreturnitinary" style="display: none; padding: 10px 0px;">
                            <img src="dist/img/search_list___review/Return-Grey.png" width="20" style="margin-left: 10px">
                            @lang('language.Yr')
                        </div>
                        <div class="card review-card" id="odreturnitinary2" style="display: none">
                            <!-- Return detail top -->
                            <div class="card-header">
                                <!-- Date and trip city -->
                                <div class="content-block-title">
                                    <span id="odreturndate"></span>
                                    <span id="odreturndeparture"></span>
                                    <img src="dist/img/baggages/u1391.png" alt="">
                                    <span id="odreturnarrival"></span>
                                </div>
                                <!-- Time and Stop -->
                                <div class="more-title"><span id="odreturntraveltime"></span>   ,   <span id="odreturntravelstops"></span> @lang('language.Stop')</div>
                            </div>
                            <!-- Return content -->
                            <div class="card-content" id="odreturnitilistcontainer">
                            </div>
                            <div id="t_odreturnitilistfinal" style="display: none;">
                                <!-- 1st -->
                                <div class="timeline" id="odreturnsegtemplatefinal" style="display: none;">
                                    <!-- From -->
                                    <div class="timeline-item">
                                        <!-- Date and time -->
                                        <div class="timeline-item-date">
                                            <span><span id="oddeparturedate1"></span></span>
                                            <b><span id="oddeparttime2"></span></b>
                                        </div>
                                        <div class="timeline-item-divider"></div>
                                        <!-- City and airport -->
                                        <div class="timeline-item-content">
                                            <div class="timeline-item-time">
                                                <span id="oddepartureport"></span>
                                                <span id="oddepartureterminalport"></span>
                                            </div>

                                            <!-- airplant NO. and class -->
                                            <div class="timeline-item-text">
                                                <div>
                                                    <span id="odairinfo"></span>|<span id="odaircabine"></span>
                                                </div>
                                                <div>
                                                    <span id="oditinerayduration"></span>|<span id="odairtype"></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- To -->
                                    <div class="timeline-item">
                                        <!-- Date and time -->
                                        <div class="timeline-item-date">
                                            <span><span id="odarriveddate"></span></span>
                                            <b><span id="odarrivedtime"></span></b>
                                        </div>
                                        <div class="timeline-item-divider"></div>
                                        <!-- City and airport -->
                                        <div class="timeline-item-content">
                                            <div class="timeline-item-time">
                                                <span id="odarrivalport"></span>
                                            </div>
                                            <div class="timeline-item-time">
                                                <span id="odarrivalterminalport"></span>

                                            </div>
                                        </div>
                                    </div>


                                </div>
                                <div class="timeline" id="odreturnstoptemplatefinal"  style="display:none;padding-top: 10px;padding-bottom: 10px;">

                                    <div class="timeline-item">
                                        <div class="timeline-item-date">
                                            <span id="odstoptime1" style="color:red"></span>
                                        </div>
                                        <div class="timeline-item-divider"></div>
                                        <div class="timeline-item-content">
                                            <span id="odstopdesc1"></span>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <!-- test timeline -->
                        <!--
                    <div class="timeline">

                        <div class="timeline-item">
                            <div class="timeline-item-date">21 <small>DEC</small></div>
                            <div class="timeline-item-divider"></div>
                            <div class="timeline-item-content">Plain text item</div>
                        </div>

                        <div class="timeline-item">
                            <div class="timeline-item-date">22 <small>DEC</small></div>
                            <div class="timeline-item-divider"></div>
                            <div class="timeline-item-content">
                                <div class="timeline-item-inner">Another text goes here</div>
                            </div>
                        </div>
                    </div>
        -->

                        <!-- Review your trip price -->
                        <div class="content-block-title" style="padding: 10px 0px;">
                            <img src="dist/img/u155.png" width="20" style="margin-left: 10px">
                            @lang('language.Ytp')
                        </div>
                        <div class="card sin-card">
                            <div class="card-header">

                                <div class="title-box">
                                    <div class="title" style="padding-left: 10px;">
                                        <h3 style="margin:0px;">
                                            <span id="odHeadDeparture2"></span>
                                            <img id="odroundpicture2" src="../dist/img/search_list___departure/return_u321.png"
                                                 style="margin-left:0px;margin-right: 0px;display:none;">
                                            <img id="odsinglepicture2"  src="../dist/img/baggages/u1391.png" style="margin-left:20px;margin-right: 20px;display:none;">
                                            <span id="odHeadArrive2"></span>
                                        </h3>
                                    </div>
                                </div>
                                <img src="dist/img/search_list___review/u1015.png" width="335">
                                <div class="title-box">
                                    <br><div><span id="oddaterange"></span><br></div>
                                    <div class="trip" style="padding-top: 13px; font-size: 14px;">@lang('language.TT')</div>
                                    <div class="total">
                                        <h2 style="margin-top: 0px;margin-bottom: 0px;color:#c30d23">
                                            <span style="font-size:12px;color:grey;" id="odCurrency"></span>
                                            <span style="font-size:18px; padding-right: 18px;" id="odTotalamount"></span>
                                        </h2>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!--   tabitinery  -->

                <div id="odtabpassenger" class="tab">
                <!--
            <h3 style="padding-left: 10px">@lang('language.PassengerDetails'):</h3>
            -->
                    <div class="card">
                        <div  id="odpsglistcontainer" class="list-block media-list">
                        </div>
                    </div>

                    <div style="display:none;">
                        <div id="t_odpsgitem">
                            <ul>
                                <li>
                                    <a href="" class="item-content" id="odpsgshowdetail" onclick="click_psg_detail(1);">
                                        <div class="item-inner">
                                            <div class="item-title-row">

                                                <img id="odpsgicon" src="dist/img/u149.png" height="24" width="13" alt="P"/>
                                                <div class="item-title" id="odpsgttname"  style="margin:2px 1px 1px 10px;">
                                                    Mariana Francois
                                                </div>
                                                <div class="item-after">
                                                    <span id="odpsgttbirthday" style="margin: 3px 1px 1px 1px">1987-10-23</span>
                                                    <span>&nbsp;&nbsp;</span>
                                                    <i class="f7-icons color-blue psgtoggle" style="font-size:15px; padding-top: 7px;">chevron_down</i>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                </li>
                            </ul>
                            <div class="c_psgdetail" style="display:none; padding-left:30px;">
                                <div class="list-block">
                                    <ul style="font-size: 15px">
                                        <li class="item-content">
                                            <div class="item-inner">
                                                <div class="item-title-row">
                                                    <div class="item-label">@lang('language.CFirstName')</div>
                                                    <div class="item-after">
                                                        <span id="odpsgfirstname">Mariana</span>
                                                        <span>&nbsp;&nbsp;</span>
                                                    </div>
                                                </div></div>
                                        </li>
                                        <li class="item-content">
                                            <div class="item-inner"><div class="item-title-row">
                                                    <div class="item-label">@lang('language.CMiddleName')</div>
                                                    <div class="item-after">
                                                        <span id="odpsgmiddlename"></span>
                                                        <span>&nbsp;&nbsp;</span>
                                                    </div>
                                                </div></div>
                                        </li>
                                        <li class="item-content">
                                            <div class="item-inner"><div class="item-title-row">
                                                    <div class="item-label">@lang('language.CLastName')</div>
                                                    <div class="item-after">
                                                        <span id="odpsglastname">Torsinia</span>
                                                        <span>&nbsp;&nbsp;</span>
                                                    </div>
                                                </div></div>
                                        </li>
                                        <li class="item-content">
                                            <div class="item-inner"><div class="item-title-row">
                                                    <div class="item-label">@lang('language.Sex')</div>
                                                    <div class="item-after">
                                                        <span id="odpsgsex">Female</span>
                                                        <span>&nbsp;&nbsp;</span>
                                                    </div>
                                                </div></div>
                                        </li>
                                        <li class="item-content">
                                            <div class="item-inner"><div class="item-title-row">
                                                    <div class="item-label">@lang('language.Birthday')</div>
                                                    <div class="item-after">
                                                        <span id="odpsgbirthday">1987-10-23</span>
                                                        <span>&nbsp;&nbsp;</span>
                                                    </div>
                                                </div></div>
                                        </li>
                                        <li class="item-content">
                                            <div class="item-inner"><div class="item-title-row">
                                                    <div class="item-label">@lang('language.Nationality')</div>
                                                    <div class="item-after">
                                                        <span id="odpsgnationality">Unite State</span>
                                                        <span>&nbsp;&nbsp;</span>
                                                    </div>
                                                </div></div>
                                        </li>
                                        <li class="item-content">
                                            <div class="item-inner"><div class="item-title-row">
                                                    <div class="item-label">@lang('language.PassportNumber')</div>
                                                    <div class="item-after">
                                                        <span id="odpsgpassportnumber">T34e39054</span>
                                                        <span>&nbsp;&nbsp;</span>
                                                    </div>
                                                </div></div>
                                        </li>
                                        <li class="item-content">
                                            <div class="item-inner"><div class="item-title-row">
                                                    <div class="item-label">@lang('language.ValidityPassport')</div>
                                                    <div class="item-after">
                                                        <span id="odpsgvaliditypassport">2022-08-07</span>
                                                        <span>&nbsp;&nbsp;</span>
                                                    </div>
                                                </div></div>
                                        </li>
                                        <li class="item-content">
                                            <div class="item-inner"><div class="item-title-row">
                                                    <div class="item-label">@lang('language.IssuingCountry')</div>
                                                    <div class="item-after">
                                                        <span id="odpsgissuingcountry">Unite State</span>
                                                        <span>&nbsp;&nbsp;</span>
                                                    </div></div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--  tabpassenger -->

                <div id="odtabcontact" class="tab">
                <!--
                  <h3 style="padding-left: 10px">@lang('language.ContactInf'):</h3>
                  -->

                    <div class="card ">
                        <div class="card-header">
                            <div class="row no-gap" style="width: 95%">
                                <div class="col-15">
                                    <i class="icon f7-icons">person</i>
                                </div>
                                <div class="col-85" id="odcontacthead" style="color: #007aff">Header</div>
                            </div>
                        </div>
                        <div class="card-content">

                            <div class="card-content-inner"id="odcontactdetail">
                                <div class="block" style="font-size: 13px; line-height:2.5em">
                                    <span>@lang('language.CEmail'):</span>
                                    <span id="odcttemail" style="color: grey">State@taskmanage.com</span>
                                    <br>
                                    <span>@lang('language.CPhone'):</span>
                                    <span id="odcttphone" style="color: grey">+1 479 334 5543</span>
                                    <br>
                                    <span>@lang('language.CBillingAddress'):</span>
                                    <span id="odcttbladdress" style="color: grey">T34e39054</span>
                                    <br>
                                    <span>@lang('language.CCity'):</span>
                                    <span id="odcttcity" style="color: grey">Montreal</span>
                                    <br>
                                    <span>@lang('language.CCountry'):</span>
                                    <span id="odcttcountry" style="color: grey">Unite State</span>
                                    <br>
                                    <span>@lang('language.CPostCode'):</span>
                                    <span id="odcttpostecode" style="color: grey">J3T5U7</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>  <!--  tabcontact -->
                <!--  tabeticket        <div id="odtabeticket" class="tab">
                            <div style="background-color:white; width:800px;">
                            <iframe id="odETicketFrame" src="" width="800px" height="5000px" scrolling="yes" frameborder="1" marginheight="0"
                                    marginwidth="0" allowfullscreen="false"></iframe>
                            </div>
                        </div>  -->
            </div>  <!--  tabs -->
        </div>  <!--  page-content  -->
    </div>
</div><!--  popup  -->
