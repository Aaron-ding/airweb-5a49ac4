
<!--   QQQQQQQQQQQQQQQQQQQQQQQQQQQQQQ   -->
            <div class="content-block">
                <p>
                    <img src="dist/img/u129.png"> Review your trip
                </p>

                <!-- departure/return city and date -->
                <div class="card">
                    <div class="card-content">
                        <div class="card-footer">
                            <h3 style="margin:0px;">
                                <span id="headDeparture"></span>
                                <img id="roundpicture1" src="../dist/img/search_list___departure/return_u321.png"
                                     style="margin-left:20px;margin-right: 20px;display:none">
                                <img id="singlepicture1"  src="../dist/img/baggages/u1391.png" style="margin-left:20px;margin-right: 20px;display:none;">
                                <span id="headArrive"></span>
                            </h3>
                        </div>
                    </div>
                </div>

                <!-- departure city and date -->
                <div class="content-block-title">
                    <img src="dist/img/search_list___review/Departure-Grey.png" width="20" style="margin-left: 10px">
                    Your departure
                </div>
                <div class="card review-card">
                    <!-- Departure detail top -->
                    <div class="card-header">
                        <!-- Date and trip city -->
                        <div class="content-block-title">
                            <span id="DepartureDate"></span>
                            <span id="Departurecity"> </span> <img src="dist/img/baggages/u1391.png"alt=""> <span id="Arrivalcity"></span>                                                </div>
                        <!-- Time and Stop -->
                        <div class="more-title"><span id="traveltime"></span>      <span id="travelstops"></span> Stop(s)</div>
                    </div>
                    <!-- Departure content --><?php ?>
                    <div class="card-content" id="itilistfinal">
                        <!-- 1st -->
                        <div class="timeline" id="segtemplatefinal" style="display: none;">
                            <!-- From -->
                            <div class="timeline-item">
                                <!-- Date and time -->
                                <div class="timeline-item-date">
                                    <span><span id="departuredate1"></span></span>
                                    <b><span id="departtime2"></span></b>
                                </div>
                                <div class="timeline-item-divider"></div>
                                <!-- City and airport -->
                                <div class="timeline-item-content">
                                    <div class="timeline-item-time">
                                        <span id="departureport"></span><br>
                                        <span id="departureterminalport"></span>
                                    </div>

                                    <!-- airplant NO. and class -->
                                    <div class="timeline-item-text">
                                        <div>
                                            <span id="airinfo"></span>|<span id="aircabine"></span>
                                        </div>
                                        <div>
                                            <span id="itinerayduration"></span>|<span id="airtype"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- To -->
                            <div class="timeline-item">
                                <!-- Date and time -->
                                <div class="timeline-item-date">
                                    <span><span id="arriveddate"></span></span>
                                    <b><span id="arrivedtime"></span></b>
                                </div>
                                <div class="timeline-item-divider"></div>
                                <!-- City and airport -->
                                <div class="timeline-item-content">
                                    <div class="timeline-item-time">
                                        <span id="arrivalport"></span>
                                    </div>
                                    <div class="timeline-item-time">
                                        <span id="arrivalterminalport"></span>

                                    </div>
                                </div>
                            </div>

                        </div>

                        <div class="timeline" id="stoptemplatefinal"  style="display:none;padding-top: 10px;padding-bottom: 10px;">

                            <div class="timeline-item">
                                <div class="timeline-item-date">
                                    <span id="stoptime1" style="color:red"></span>
                                </div>
                                <div class="timeline-item-divider"></div>
                                <div class="timeline-item-content">
                                    <span id="stopdesc1"></span>
                                </div>
                            </div>

                        </div>
                    </div>

                </div>

                <!-- Return city and date -->
                <div class="content-block-title" id="returnitinary" style="display: none">
                    <img src="dist/img/search_list___review/Departure-Grey.png" width="20" style="margin-left: 10px">
                    Your return
                </div>
                <div class="card review-card" id="returnitinary2" style="display: none">
                    <!-- Return detail top -->
                    <div class="card-header">
                        <!-- Date and trip city -->
                        <div class="content-block-title">
                            <span id="returndate"></span>
                            <span id="returndeparture"></span>
                            <img src="dist/img/baggages/u1391.png" alt="">
                            <span id="returnarrival"></span>
                        </div>
                        <!-- Time and Stop -->
                        <div class="more-title"><span id="returntraveltime"></span>   ,   <span id="returntravelstops"></span> Stop(s)</div>
                    </div>
                    <!-- Return content -->
                    <div class="card-content" id="returnitilistfinal">
                        <!-- 1st -->
                        <div class="timeline" id="returnsegtemplatefinal" style="display: none;">
                            <!-- From -->
                            <div class="timeline-item">
                                <!-- Date and time -->
                                <div class="timeline-item-date">
                                    <span><span id="departuredate1"></span></span>
                                    <b><span id="departtime2"></span></b>
                                </div>
                                <div class="timeline-item-divider"></div>
                                <!-- City and airport -->
                                <div class="timeline-item-content">
                                    <div class="timeline-item-time">
                                        <span id="departureport"></span>
                                        <span id="departureterminalport"></span>
                                    </div>

                                    <!-- airplant NO. and class -->
                                    <div class="timeline-item-text">
                                        <div>
                                            <span id="airinfo"></span>|<span id="aircabine"></span>
                                        </div>
                                        <div>
                                            <span id="itinerayduration"></span>|<span id="airtype"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- To -->
                            <div class="timeline-item">
                                <!-- Date and time -->
                                <div class="timeline-item-date">
                                    <span><span id="arriveddate"></span></span>
                                    <b><span id="arrivedtime"></span></b>
                                </div>
                                <div class="timeline-item-divider"></div>
                                <!-- City and airport -->
                                <div class="timeline-item-content">
                                    <div class="timeline-item-time">
                                        <span id="arrivalport"></span>
                                    </div>
                                    <div class="timeline-item-time">
                                        <span id="arrivalterminalport"></span>

                                    </div>
                                </div>
                            </div>


                        </div>
                        <div class="timeline" id="returnstoptemplatefinal"  style="display:none;padding-top: 10px;padding-bottom: 10px;">

                            <div class="timeline-item">
                                <div class="timeline-item-date">
                                    <span id="stoptime1" style="color:red"></span>
                                </div>
                                <div class="timeline-item-divider"></div>
                                <div class="timeline-item-content">
                                    <span id="stopdesc1"></span>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <!-- test timeline --><!--
                <div class="timeline">

                <div class="timeline-item">
                <div class="timeline-item-date">21 <small>DEC</small></div>
                <div class="timeline-item-divider"></div>
                <div class="timeline-item-content">Plain text item</div>
                </div>

                <div class="timeline-item">
                <div class="timeline-item-date">22 <small>DEC</small></div>
                <div class="timeline-item-divider"></div>
                <div class="timeline-item-content">
                <div class="timeline-item-inner">Another text goes here</div>
                </div>
                </div>
                </div>
                -->

                <!-- Review your trip price -->
                <div class="content-block-title">
                    <img src="dist/img/u155.png" width="20" style="margin-left: 10px">
                    Your trip price
                </div>
                <div class="card sin-card">
                    <div class="card-header">

                        <div class="title-box">
                            <div class="title">
                                <h3 style="margin:0px;">
                                    <span id="HeadDeparture"></span>
                                    <img id="roundpicture2" src="../dist/img/search_list___departure/return_u321.png"
                                         style="margin-left:0px;margin-right: 0px;display:none;">
                                    <img id="singlepicture2"  src="../dist/img/baggages/u1391.png" style="margin-left:20px;margin-right: 20px;display:none;">
                                    <span id="HeadArrive"></span>
                                </h3>
                            </div>
                        </div>
                        <img src="dist/img/search_list___review/u1015.png" width="335">
                        <div class="title-box">
                            <br><div><span id="daterange"></span><br></div>
                            <div class="trip" style="padding-top: 13px; font-size: 14px;">Trip Total:</div>
                            <div class="total">
                                <h2 style="margin-top: 0px;margin-bottom: 0px;color:#c30d23">
                                    <span style="font-size:12px;color:grey;">$</span><span style="font-size:18px; padding-right: 18px;" id="Totalamount"></span>
                                </h2>
                            </div>
                        </div>
                    </div>
                    <div class="card-content">
                        <!-- Traveller 1 -->
                        <div class="detail">
                            <div class="traveller">
                                <span>Air transportation Charges:</span><span id="airtransportation"></span><br>
                                <span>Taxes,Fees and Charges:</span><span id="taxtotalfee"></span><br>

                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <script>
                //var request = new Request();

                //var gostring='@php echo addslashes(html_entity_decode(old("selectdata"), ENT_QUOTES)); @endphp';



                function showpricedetailpay(obj,obj1) {

                    //var data=request.getDconcat(tjq(obj).attr('data'));
                    //var data=request.getDconcat(tjq(obj).attr('data'));
                    //console.log(obj);

                    var price=obj1[0];
                    console.log(price);
                    var detail=internary=obj[0];
                    if (typeof internary=="string"){
                        detail=JSON.parse(internary);
                    }
                    console.log('detail');
                    console.log(detail);


                    if(detail.tripType=="roundtrip"){
                        var goRaw=price.goRaw;
                        var returnRaw=price.returnRaw;
                        console.log("hello");
                        console.log(goRaw);
                        tjq('#returnitinary').css("display", "");
                        tjq('#returnitinary2').css("display", "");


                        console.log("goraw");
                        console.log(goRaw);
                        console.log("returnraw");
                        console.log(returnRaw);
                    }
                    else if(detail.tripType=="oneway"){
                        var goRaw=price.goRaw;


                    }

                    if(price.ValidatingCarrier=="")
                    {
                        var segs=price.goKey;
                        if (detail.tripType == "roundtrip") {
                            var resegs=price.returnKey;
                        }
                    }
                    else {
                        var segs = JSON.parse(Goraw.Segment);
                        var segcount = 0;
                        var lastarrivetime = undefined;
                        var lastcity = undefined;
                        var lastteminnal = undefined;
                        console.log("gorawseg");
                        console.log(segs);

                        if (detail.tripType == "roundtrip") {
                            var resegs = JSON.parse(Returnraw.Segment);
                            console.log("returnrawseg");
                            console.log(resegs);
                        }

                    }
                    //console.log(Goraw);

                    console.log("test");
                    console.log(tjq('.timeline').length);
                    if(detail.tripType=="roundtrip") {
                        if ((parseInt(tjq('.timeline').length) < 10)) {
                            console.log("flag");

                            travalitinarypay("go", segs);
                            travalitinarypay("goback", resegs);
                        }
                    }

                    else if(detail.tripType=="oneway") {
                        if ((parseInt(tjq('.timeline').length) < 8)) {
                            travalitinarypay("go", segs);
                        }
                    }

                    //var internarytemplate=tjq(rawinternarytemplate).clone();
                    if(detail.tripType=="roundtrip")
                    {
                        tjq('#roundpicture1').css("display", "");
                        tjq('#roundpicture2').css("display", "");
                    }
                    if(detail.tripType=="oneway")
                    {
                        tjq('#singlepicture1').css("display", "");
                        tjq('#singlepicture2').css("display", "");
                    }



                    function travalitinarypay (obj1,obj2){
                        //console.log(tjq('#itilistfinal').css("display"));
                        //if(tjq('#itilistfinal').css("display")=="block")

                        if (obj1 == "go") {
                            //if(tjq('#stoptemplatefinal').css("display")=="block")

                            //console.log(tjq('#stoptemplatefinal').css("display"));
                            //console.log(tjq('#segtemplatefinal').css("display"));
                            var rawinternarytemplate = tjq('#itilistfinal').clone();
                            var rawstoptemplatefinal = tjq(rawinternarytemplate).find('#stoptemplatefinal').clone();
                            var rawsegtemplatefinal = tjq(rawinternarytemplate).find('#segtemplatefinal').clone();
                        }
                        if (obj1 == "goback") {
                            var rawinternarytemplate = tjq('#returnitilistfinal').clone();
                            var rawstoptemplatefinal = tjq(rawinternarytemplate).find('#returnstoptemplatefinal').clone();
                            var rawsegtemplatefinal = tjq(rawinternarytemplate).find('#returnsegtemplatefinal').clone();
                        }


                        var internarytemplate = tjq(rawinternarytemplate).clone();
                        tjq.each(obj2, function (idx, seg) {
                            //console.log(seg);

                            segcount++;
                            var departdate = new Date(seg.DepartureDateTime + "Z");
                            if (lastarrivetime != undefined) {
                                var stoptemplatefinal = tjq(rawstoptemplatefinal).clone();
                                tjq(stoptemplatefinal).css("display", "");
                                var diff = departdate.getTime() - lastarrivetime.getTime();
                                console.log(diff);
                                var diffhours = Math.floor(diff / 1000 / 60 / 60);
                                var diffminutes = Math.floor(diff / 1000 / 60) % 60;


                                var chanageairport = "";
                                var changeterminal = "";
                                if (lastcity != seg.From) {
                                    chanageairport = "，Go to another airport";
                                }
                                if (chanageairport == "" && lastteminnal != seg.From_TerminalID) {
                                    changeterminal = "，Go to another terminal";
                                }
                                tjq(stoptemplatefinal).find('#stoptime1').html(diffhours + "H" + diffminutes + "M");
                                tjq(stoptemplatefinal).find('#stopdesc1').html('Transit in ' + getCityNameByCode(seg.From) + chanageairport + changeterminal);

                                tjq(internarytemplate).append(tjq(stoptemplatefinal));
                                //console.log(tjq(stoptemplatefinal));
                                //console.log(tjq(rawinternarytemplate)[0].outerHTML);


                            }


                            if (seg.Airline != seg.OperatingAirline) {
                                //if (language == 'Chinese') {
                                //var operatingAirlineDetail = "由" + seg.OperatingAirline + "运营";
                                //}
                                //else {
                                var operatingAirlineDetail = "Operated by " + seg.OperatingAirline;
                                //}
                            } else {
                                var operatingAirlineDetail = '';
                            }


                            var segtemplatefinal = tjq(rawsegtemplatefinal).clone();
                            tjq(segtemplatefinal).css("display", "");
                            //console.log(seg.From);

                            tjq(segtemplatefinal).find('#departureport').text(getAirportNameByCode(seg.From) + '(' + seg.From + ')');
                            tjq(segtemplatefinal).find('#departureterminalport').text('Terminal：' + seg.From_TerminalID);
                            //console.log(seg.DepartureDateTime);
                            tjq(segtemplatefinal).find('#departuredate1').text((seg.DepartureDateTime).substr(0, 10));
                            tjq(segtemplatefinal).find('#departtime2').text((seg.DepartureDateTime).substr(-8, 8));
                            //tjq('#departdate').text(request.formatDateYMD(departdate).substr(5,5));
                            //tjq('#departtime').text(request.formatAMPM(departdate));
                            tjq(segtemplatefinal).find('#airinfo').text(seg.OperatingAirline + seg.OperatingFlightNumber);
                            tjq(segtemplatefinal).find('#aircabine').text(seg.MarketingCabin);
                            tjq(segtemplatefinal).find('#itinerayduration').text(seg.ElapsedTimeTotalHours + 'H' + seg.ElapsedTimeTotalHoursLeftMins + 'M');
                            tjq(segtemplatefinal).find('#airtype').text(seg.AirEquipType);
                            //console.log(seg.ArrivalDateTime);
                            tjq(segtemplatefinal).find('#arriveddate').text((seg.ArrivalDateTime).substr(0, 10));
                            tjq(segtemplatefinal).find('#arrivedtime').text((seg.ArrivalDateTime).substr(-8, 8));
                            tjq(segtemplatefinal).find('#arrivalport').text(getAirportNameByCode(seg.To) + '(' + seg.To + ')');
                            tjq(segtemplatefinal).find('#arrivalterminalport').text('Terminal：' + seg.To_TerminalID);


                            /*tjq('#airline').text(seg.Airline+" "+seg.Flightnumber);
                             tjq('#cabinclass').text(seg.MarketingCabin);
                             tjq('#aircarft').text(seg.AirEquipType);

                             tjq('#hours').text(seg.ElapsedTimeTotalHours);
                             tjq('#mins').text(seg.ElapsedTimeTotalHoursLeftMins);
                             tjq('#opertator').text(operatingAirlineDetail);

                             //tjq(tmpseg).find('#airlinename').text(getAirlinesNameByCode(seg.Airline));
                             tjq('#airlinelogo').attr('src','https://air.sinorama.ca/assets/images/logo/'+ tjq.trim(seg.Airline) + '.png');


                             */

                            var arrivedate = new Date(seg.ArrivalDateTime + "Z");
                            lastarrivetime = arrivedate;
                            lastcity = seg.To;
                            lastteminnal = seg.To_TerminalID;

                            tjq(internarytemplate).append(tjq(segtemplatefinal));


                        })

                        //console.log(tjq(internarytemplate)[0].outerHTML);
                        console.log(tjq(internarytemplate));
                        if (obj1 == "go")
                            tjq('#itilistfinal').html(tjq(internarytemplate));
                        if (obj1 == "goback")
                            tjq('#returnitilistfinal').html(tjq(internarytemplate));
                        var lastarrivetime=undefined;
                        var lastcity=undefined;
                        var lastteminnal=undefined;

                    }


                    //console.log();
                    tjq('#headDeparture').text(getCityNameByCode(detail.origin));
                    tjq('#headArrive').text(getCityNameByCode(detail.destination));
                    tjq('#HeadDeparture').text(getCityNameByCode(detail.origin));
                    tjq('#HeadArrive').text(getCityNameByCode(detail.destination));
                    tjq('#DepartureDate').text(detail.departuredate);
                    tjq('#Departurecity').text(getCityNameByCode(detail.origin));
                    tjq('#Arrivalcity').text(getCityNameByCode(detail.destination));

                    //tjq('#traveltime').text(goRaw.ElapsedTimeTotalHours+'H ' + goRaw.ElapsedTimeTotalHoursLeftMins + 'M');
                    tjq('#travelstops').text(goRaw.stops);


                    if(detail.tripType=="roundtrip") {
                        tjq('#returndate').text((returnRaw.DepartureDateTime).substr(0, 10));
                        tjq('#returndeparture').text(getCityNameByCode(returnRaw.DepartureCity));
                        tjq('#returnarrival').text(getCityNameByCode(returnRaw.ArrivateCity));
                        //tjq('#returntraveltime').text(returnRaw.ElapsedTimeTotalHours + 'H ' + returnRaw.ElapsedTimeTotalHoursLeftMins + 'M');
                        tjq('#returntravelstops').text(returnRaw.stops);
                    }

                    tjq('#Totalamount').text(price.totalfee);
                    tjq('#pricetotal').text(price.totalfee);
                    tjq('#airtransportation').text(price.basefee);
                    tjq('#taxtotalfee').text(price.taxfee);
                    tjq('#interchangefee').text('0');
                }
            </script>
