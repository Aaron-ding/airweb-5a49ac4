<div id="ft_item_template" class="card" data-price="" style="display:none" xmlns="http://www.w3.org/1999/html">
    <div class="row no-gutter">
        <!-- Time and airport-->
        <div class="col-70 left-block">
            <div class="row">
                <div class="begin">
                    <span id="DepartureTime"></span>
                </div>

                <div class="when">
                    <div class="durationtime">
                        <span id="ElapsedTimeTotalHours" style="font-size: 8px"></span>
                    </div>
                    <img src="../dist/img/search_list___departure/u453.png" height="1" width="50">
                    <img src="../dist/img/search_list___departure/u457.png" height="5" width="7" class="arrow">
                </div>

                <div class="overdate" id="ArrivalDate">

                </div>
            </div>
            <div class="row" id="stopcitylist">

            </div>
        </div>
        <!-- Price and detail link-->
        <div class="col-30 right-block" id="pricearea" style=" padding: 0px;padding-top: 7px;padding-right: 2px;">
            <div class="row no-gutter" style="align-items: baseline;">

                <div class="color-blue" style="font-size: 16px;font-weight: 600;" id="TotalFareTotal"></div>
                <div class="color-gray" style="font-size: 10px">/</div>
                <div class="color-gray" style="font-size: 10px" id="totalperson"></div>
                <div class="color-gray" style="font-size: 10px">@lang('language.P')</div>
                <a href="#" class="create-popup">

                </a>
            </div>
            <div class="row no-gutter">
               <!-- <div class="color-black" style="font-size: 10px">$</div>-->
                <div id="displayprice">
                    <small>
                        <span id="TotalFare"></span>
                    </small>
                        <span class="color-gray" style="font-size: 10px">/@lang('language.PP')

                </div>
            </div>
        </div>
    </div>
    <div class="row no-gutter">
        <!-- airline and stop-->
        <div class="col-75 line-three">
            <div class="row" id="FlightName">
                <div> <span id="stops"></span></div>
                <img id="selectlink" src="../dist/img/search_list___departure/u469.png" style="margin-left: 20px">

            </div>
        </div>
        <!-- button -->
        <div class="col-25 right-btn" style="padding-right: 5px;">
            <a id="selectbutton" href="#" class="button active gobutton" style="font-size:12px; height: 25px;line-height: 25px; margin-top: 3px;">@lang('language.Select')</a>
        </div>
    </div>
</div>


<script>
    function buildTemplate(template,value){
        for (var item in value) {
            tjq(template).find('#' + item).html(value[item]);
        }

        tjq(template).find('#seatsLeft').parent().hide();

        var totalfee = parseFloat(value["TotalFare"]);
        var totalperson = parseInt(options.adultcount) + parseInt(options.childrencount) + parseInt(options.ifantcount);
        var avgfee = Math.round(totalfee * 100 / totalperson) / 100;

        tjq(template).find('#TotalFare').html(request.getCurrencyShow(value['TotalFareCurrency']) + avgfee);
        //tjq(template).find('#TotalFare').html(totalfee);
        //tjq(template).attr('data-price', avgfee);
        tjq(template).attr('data-price', totalfee);
        tjq(template).find('#TotalFareTotal').html(request.getCurrencyShow(value['TotalFareCurrency']) + value["TotalFare"]);
        tjq(template).find('#totalperson').html(totalperson);
        /*if(totalperson===1){
            tjq(template).find('#displayprice').hide();
        }else{
            tjq(template).find('#displayprice').show();
        }*/

        tjq(template).attr('data-MD5', value['MD5']);
        tjq(template).attr('data-directflights', value['stops']);
        var DepartureDateTimeDate = new Date(value['DepartureDateTime']);
        var ArricalDateTimeDate = new Date(value['ArrivalDateTime']);
        tjq(template).attr('data-time', DepartureDateTimeDate.getTime() / 1000);
        tjq(template).attr('data-duration', value['ElapsedTimeTotalMins']);
        tjq(template).attr('data-arrival',ArricalDateTimeDate.getTime()/1000 );
        tjq(template).attr('data-provider', value["provider"]);
        tjq(template).find('#provider').attr('src', 'assets/images/' + value["provider"] + '.jpg');

        if (value['stops'] == "0") {
            tjq(template).find('#stops').html("@lang('language.Direct')");
        } else {
            tjq(template).find('#stops').html(value['stops'] + "@lang('language.Stop')");
        }




        var stopcitystring = '';
        var goSeg = JSON.parse(value['Segment']);
        var i = 0;
        tjq.each(goSeg, function (idx, val) {

            if(i==0){
                if("@lang('language.Lang')"=="English")
                  stopcitystring +='<div class="departure">'+val["From"]+'</div>';
                else if("@lang('language.Lang')"=="Chinese")
                    stopcitystring +='<div class="departure">'+getCityNameByCodeChinese(val["From"])+'</div>';
                else if("@lang('language.Lang')"=="French")
                    stopcitystring +='<div class="departure">'+val["From"]+'</div>';
            }else{
                if("@lang('language.Lang')"=="English")
                    stopcitystring +='<img src="dist/img/search_list___departure/u347.png" alt=""><div class="through">'+val["From"]+'</div>';
                else if("@lang('language.Lang')"=="Chinese")
                    stopcitystring +='<img src="dist/img/search_list___departure/u347.png" alt=""><div class="through">'+getCityNameByCodeChinese(val["From"])+'</div>';
                else if("@lang('language.Lang')"=="French")
                    stopcitystring +='<img src="dist/img/search_list___departure/u347.png" alt=""><div class="through">'+val["From"]+'</div>';
            }
            if (i == (goSeg.length - 1)) {
                if("@lang('language.Lang')"=="English")
                  stopcitystring +='<img src="dist/img/search_list___departure/u347.png" alt=""><div class="departure">'+val["To"]+'</div>';
                else if("@lang('language.Lang')"=="Chinese")
                    stopcitystring +='<img src="dist/img/search_list___departure/u347.png" alt=""><div class="departure">'+getCityNameByCodeChinese(val["To"])+'</div>';
                else if("@lang('language.Lang')"=="French")
                    stopcitystring +='<img src="dist/img/search_list___departure/u347.png" alt=""><div class="departure">'+val["To"]+'</div>';
            }

            i++;
        });

        tjq(template).find('#stopcitylist').html(stopcitystring);
        if(("@lang('language.Lang')"=="English")||("@lang('language.Lang')"=="French"))
          tjq(template).find('#ElapsedTimeTotalHours').text(value["ElapsedTimeTotalHours"]+'H ' + value["ElapsedTimeTotalHoursLeftMins"] + 'M');
        else if("@lang('language.Lang')"=="Chinese")
            tjq(template).find('#ElapsedTimeTotalHours').text(value["ElapsedTimeTotalHours"]+'时' + value["ElapsedTimeTotalHoursLeftMins"] + '分');
        tjq(template).find('#ArrivalDate').html(value["ArrivalDate"]+","+value["ArrivalTime"]);

        var airCompanylist = value['airCompany'];

        if (typeof airCompanylist == "string") {
            airCompanylist = [airCompanylist];
        }



        var aircompany='';
        for (var m = 0; m < airCompanylist.length; m++) {
            aircompany +='<img class="airlinelogog" src="<?php if ($env == 'master') echo 'https://air.sinorama.ca/assets/images/logo/'; else echo 'https://dev.air.sinorama.ca/flight_front/assets/images/logo/' ?>'+ tjq.trim(airCompanylist[m]) + '.png" width="22" height="22">';

        }
        tjq(template).find('#FlightName').find('.airlinelogog').remove();
        tjq(template).find('#FlightName').prepend(aircompany);



        tjq(template).css('display','');

        tjq(template).attr('data', request.concat(JSON.stringify(value), ""));

        tjq(template).attr('return_list',value["RETURN_LIST"]);

        tjq(template).on('click',function(){
            showdetail(tjq(this));
            //showpricedetail(tjq(this));
        });
        return template;
    }


</script>