<style>
    .content-block-search {
        margin: 15px 0;
        padding: 0 10px;
        color: #6d6d72;
        box-sizing: border-box;
    }

    .list-block .item-content-traveller{
        box-sizing:border-box;
        padding-left:15px;
        min-height:40px;
        display:-webkit-box;
        display:-ms-flexbox;
        display:-webkit-flex;
        display:flex;
        -webkit-box-pack:justify;
        -ms-flex-pack:justify;
        -webkit-justify-content:space-between;
        justify-content:space-between;
        -webkit-box-align:center;
        -ms-flex-align:center;
        -webkit-align-items:center;
        align-items:center
    }

    .list-block .item-content-traveller2{
        box-sizing:border-box;
        padding-left:3px;
        min-height:28px;
        display:-webkit-box;
        display:-ms-flexbox;
        display:-webkit-flex;
        display:flex;
        -webkit-box-pack:justify;
        -ms-flex-pack:justify;
        -webkit-justify-content:space-between;
        justify-content:space-between;
        -webkit-box-align:center;
        -ms-flex-align:center;
        -webkit-align-items:center;
        align-items:center
    }

    .list-block .item-content-traveller3 {
        box-sizing: border-box;
        padding-left: 1px;
        min-height: 34px;
        display: -webkit-box;
        display: -ms-flexbox;
        display: -webkit-flex;
        display: flex;
        -webkit-box-pack: justify;
        -ms-flex-pack: justify;
        -webkit-justify-content: space-between;
        justify-content: space-between;
        -webkit-box-align: center;
        -ms-flex-align: center;
        -webkit-align-items: center;
        align-items: center;
    }

    .list-block .item-title.label-traveller {
        width: 22%;
        -webkit-flex-shrink: 0;
        -ms-flex: 0 0 auto;
        flex-shrink: 0;
    }
    .list-block .item-title.label-traveller2 {
        width: 30%;
        -webkit-flex-shrink: 0;
        -ms-flex: 0 0 auto;
        flex-shrink: 0;
    }

    .list-block .item-title.label-traveller3 {
        width: 40%;
        -webkit-flex-shrink: 0;
        -ms-flex: 0 0 auto;
        flex-shrink: 0;
    }
    .list-block .item-title.label-traveller5 {
        width: 18%;
        -webkit-flex-shrink: 0;
        -ms-flex: 0 0 auto;
        flex-shrink: 0;
    }

    .popup-traveller {
        -webkit-box-pack: center;
        -ms-flex-pack: center;
        -webkit-justify-content: center;
        justify-content: center;
        -webkit-box-align: center;
        -ms-flex-align: center;
        -webkit-align-items: center;
        align-items: center;
        width: 60%;
    }

    .button-traveller {
        border: 1px solid #D7D7D7;
        color: #D7D7D7;
        text-decoration: none;
        text-align: center;
        display: block;
        border-radius: 5px;
        line-height: 35px;
        box-sizing: border-box;
        -webkit-appearance: none;
        -moz-appearance: none;
        -ms-appearance: none;
        appearance: none;
        background: none;
        padding: 0 10px;
        margin: 0;
        height: 35px;
        white-space: nowrap;
        position: relative;
        overflow: hidden;
        text-overflow: ellipsis;
        font-size: 17px;
        font-family: inherit;
        cursor: pointer;
        outline: 0;
    }

    .list-block.inset-search{margin-top:20px;margin-bottom:20px;margin-left:1px;margin-right:1px;border-radius:7px}

    .list-block.inset-search .content-block-title{margin-left:0;margin-right:0}

    .list-block.inset-search ul{border-radius:7px}

    .list-block.inset-search ul:before{display:none}

    .list-block.inset-search ul:after{display:none}

    .list-block.inset-search li:first-child>a{border-radius:7px 7px 0 0}

    .list-block.inset-search li:last-child>a{border-radius:0 0 7px 7px}

    .list-block.inset-search li:first-child:last-child>a{
        border-radius:7px}@media all and (min-width:768px){.list-block.tablet-inset{
        margin-left:15px;
        margin-right:15px;
        border-radius:7px
    }
    }

    .row .col-451{
        margin-left:15px;
        width:45%;
        width:-webkit-calc((100% - 15px*1.2222222222222223)/ 2.2222222222222223);
        width:calc((100% - 15px*1.2222222222222223)/ 2.2222222222222223)
    }

    .row .col-452{
        margin-right:15px;
        width:45%;
        width:-webkit-calc((100% - 15px*1.2222222222222223)/ 2.2222222222222223);
        width:calc((100% - 15px*1.2222222222222223)/ 2.2222222222222223)
    }
</style>


                        <!-- Search Tab -->
                        <div class="content-block-search">
                            <p class="buttons-row">
                                <a href="#tab1" class="tab-link button color-red active">@lang('language.Round')</a>
                                <a href="#tab2" class="tab-link button color-red">@lang('language.One')</a>
                            </p>
                        </div>
                        <form id="search-form" class="list-block">
                            <!-- Tabs, tabs wrapper -->
                            <div class="tabs">
                                <!-- Tab 1, active by default -->
                                <div id="tab1" class="tab active">
                                    <!-- Tab 1 content -->
                                    <div class="content-block">
                                        <!-- Tab 1 Flying from -->

                                        <div class="list-block inset-search">
                                            <ul>
                                                <li>
                                                    <a href="#" id="departure-popup" class="item-link item-content autocomplete-opener">
                                                        <input name="src" type="hidden" >
                                                        <div class="item-inner">
                                                            <div class="item-title">@lang('language.From')</div>
                                                            <div class="item-after">@lang('language.downloading')</div>
                                                        </div>
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                        <!-- Tab 1 Flying to -->
                                        <div class="list-block inset-search">
                                            <ul>
                                                <li>
                                                    <a href="#" id="arrive-popup" class="item-link item-content autocomplete-opener">
                                                        <input name="dest" type="hidden">
                                                        <div class="item-inner">
                                                            <div class="item-title">@lang('language.To')</div>
                                                            <div class="item-after">@lang('language.downloading')</div>
                                                        </div>
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                        <div id="roundtripdate" class="list-block inset-search">
                                            <ul>
                                                <li>
                                                    <div class="item-content">
                                                        <div class="item-inner">
                                                            <div class="item-input">
                                                                <input name="departuredateroundtrip" type="text" placeholder="@lang('language.SelectRound')" readonly
                                                                       id="calendar-range">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>

                                        <div id="onewaydate" class="list-block inset-search" style="display:none;">
                                            <ul>
                                                <li>
                                                    <div class="item-content">
                                                        <div class="item-inner">
                                                            <div class="item-input">
                                                                <input name="departuredateoneway" type="text" placeholder="@lang('language.SelectSingle')" readonly
                                                                       id="calendar-one">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                        <!-- Tab 1 Class -->
                                        <div class="row">
                                            <div class="col-50">
                                                <div class="list-block inset">
                                                    <ul>
                                                        <li>
                                                            <div class="item-content">
                                                                <div class="item-inner">
                                                                    <div class="item-input">
                                                                        <a href="#" class="open-select-travel">
                                                                            <input  name="passengers" readonly id="passengers" type="text" value="@lang('language.OnePassenger')">
                                                                        </a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="col-50">
                                                <div class="list-block inset">
                                                    <ul>
                                                        <li>
                                                            <div class="item-content">
                                                                <div class="item-inner">
                                                                    <div class="item-input">
                                                                        <select id="PreferLevel">
                                                                            <option selected="" value="Y">@lang('language.EC')</option>
                                                                            <option value="S">@lang('language.PEC')</option>
                                                                            <option value="C">@lang('language.BC')</option>
                                                                            <option value="J">@lang('language.PBC')</option>
                                                                            <option value="F">@lang('language.FC')</option>
                                                                            <option value="P">@lang('language.PFC')</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>

                                        <!-- Tab 1 Button -->
                                        <p><a href="#" class="button active" id="dosearch">@lang('language.Search')<i class="f7-icons size-14" style="margin-left:10px;">search_strong</i></a></p>
                                        <span id="information" style="display:none"></span>
                                    </div>
                                </div>
                                <div id="tab2" class="tab">
                                </div>

                            </div>

                            <!-- Traveller select page - Popup -->
                            <div class="popup popup-about select-travel" style="background: #f2f2f2">
                                <div class="content-block" align="center">

                                    <!-- Adults -->
                                    <div class="row">
                                        <div class="col-20">
                                            <div class="list-block inset">
                                                <ul>
                                                    <li>
                                                        <a href="#" class="button-traveller color-gray sub" for="adult">－</a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="col-60">
                                            <div class="list-block inset">
                                                <ul>
                                                    <li>
                                                        <div class="item-content">
                                                            <input id="adult" name="adult" readonly type="text" value="@lang('language.OneAdult')" placeholder="@lang('language.Adult')" data=1 >
                                                        </div>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="col-20">
                                            <div class="list-block inset">
                                                <ul>
                                                    <li>
                                                        <a href="#" class="button-traveller color-gray add" for="adult">＋</a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- children -->
                                    <div class="row">
                                        <div class="col-20">
                                            <div class="list-block inset">
                                                <ul>
                                                    <li>
                                                        <a href="#" class="button-traveller color-gray sub" for="kids">－</a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="col-60">
                                            <div class="list-block inset">
                                                <ul>
                                                    <li>
                                                        <div class="item-content" style="text-align: center">
                                                            <input id="kids" name="kids" readonly type="text" placeholder="@lang('language.Children')" data=0>
                                                        </div>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="col-20">
                                            <div class="list-block inset">
                                                <ul>
                                                    <li>
                                                        <a href="#" class="button-traveller color-gray add" for="kids">＋</a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Infant1 -->
                                    <div class="row">
                                        <div class="col-20">
                                            <div class="list-block inset">
                                                <ul><li>
                                                        <a href="#" class="button-traveller color-gray sub"  for="infantseat">－</a>
                                                    </li></ul></div>
                                        </div>
                                        <div class="col-60">
                                            <div class="list-block inset">
                                                <ul>
                                                    <li>
                                                        <div class="item-content" style="text-align: center">
                                                            <input id="infantseat" name="infantseat" readonly type="text" placeholder="@lang('language.InfantIS')" data=0>
                                                        </div>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="col-20">
                                            <div class="list-block inset">
                                                <ul><li>
                                                        <a href="#" class="button-traveller color-gray add" for="infantseat">＋</a>
                                                    </li></ul></div>
                                        </div>
                                    </div>
                                    <!-- Infant2 -->
                                    <div class="row">
                                        <div class="col-20">
                                            <div class="list-block inset">
                                                <ul><li>
                                                        <a href="#" class="button-traveller color-gray sub" for="infant">－</a>
                                                    </li></ul></div>
                                        </div>
                                        <div class="col-60">
                                            <div class="list-block inset">
                                                <ul>
                                                    <li>
                                                        <div class="item-content" style="text-align: center">
                                                            <input id="infant" name="infant" readonly type="text" placeholder="@lang('language.InfantIL')" data=0 >
                                                        </div>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="col-20">
                                            <div class="list-block inset">
                                                <ul><li>
                                                        <a href="#" class="button-traveller color-gray add" for="infant">＋</a>
                                                    </li></ul>
                                            </div>
                                        </div>
                                    </div>

                                    <!-- Button go back to search page -->
                                    <p><a href="#" class="done-passengers button active">@lang('language.Done')</a></p>

                                </div>
                            </div>
                        </form>
        @if($env == 'master')
            <form id="tolist" action="{{route("searchlist")}}">
        @else
            <form id="tolist" action="{{route("dev_searchlist")}}">
        @endif
                <input type="hidden" name="querystring">
            </form>

        @if($env == 'master')
           <form id="topay" action="{{route("pay")}}">
        @else
            <form id="topay" action="{{route("dev_pay")}}">
        @endif
                <input type="hidden" name="orderid">
                <input type="hidden" name="token">
                <input type="hidden" name="valid">
            </form>





<script>
    @isset($respect)
            console.log(JSON.parse('@php echo $respect  @endphp'));
    @endisset

    //var lang="@lang('language.Lang')";
    //var request = new Request();
    var options=undefined;
    @isset($querystring)
        options=JSON.parse('@php echo html_entity_decode($querystring, ENT_QUOTES); @endphp');
        console.log(options);
    @endisset

    var results=getJsonFromUrl();
    console.log(results);
    var id=results["id"];
    var sourcecity = results["srccity"];
    var destcity = results["destcity"];
    var triptype = results["tripType"];
    var departdate = results["departdate"];
    var returndate = results["returndate"];
    var adultcount = results["adultcount"];
    var childrencount = results["childrencount"];
    var infantsseatcount = results["infantsseatcount"];
    var infantslapcount = results["infantslapcount"];
    var PreferLevel = results["PreferLevel"];
    var currencyonquery=results["change_currency"];
    var language=results["change_lang"];



    console.log(currencyonquery);

    if(((id==undefined)||(id=="search"))&&(sourcecity!=undefined)&&(destcity!=undefined)&&(triptype!=undefined)&&(departdate!=undefined)&&(returndate!=undefined)){

        console.log("flag");



        if (options === undefined) {
            options = {
                origin: '',
                destination: '',
                departuredate: '',
                returndate: '',
                onlineitinerariesonly: 'N',
                adultcount: 1,
                childrencount: 0,
                ifantcount: 0,
                infantsseatcount: 0,
                infantslapcount: 0,
                limit: 50,
                offset: 1,
                maxstops: 99,
                PreferLevel: "Y",
                preferredAirline: "",
                priority: "Price",
                sortby: 'totalfare',
                order: 'asc',
                sortby2: 'departuretime',
                order2: 'dsc',
                tripType: '',
                currency: '',
                rawsrcstr: '',
                rawdeststr: '',
                rawdatestr: ''
            };
        }

        options["origin"] = sourcecity;
        options["destination"] = destcity;
        //var y=searchairport("YUL");
        //console.log(y);


        options["rawsrcstr"] = sourcecity;
        options["rawdeststr"] = destcity;

        options["currency"] = "CAD";
        options["tripType"] = triptype;


        if (triptype == "roundtrip") {
            //var datesplit=formData.departuredateroundtrip.split("-");
            //options['rawdatestr']=formData.departuredateroundtrip;
            //var ddate=Date.parse(datesplit[0]).toISOString();
            //var rdate=Date.parse(datesplit[1]).toISOString();
            options["departuredate"] = departdate;
            options["returndate"] = returndate;

        }

        if (triptype == "oneway") {
            //var ddate=Date.parse(formData.departuredateoneway);
            //options['rawdatestr']=formData.departuredateoneway;
            options["departuredate"] = departdate;
            options["returndate"] = null;

        }
        options["adultcount"] = adultcount;
        options["childrencount"] = childrencount;
        options["infantsseatcount"] = infantsseatcount;
        options["infantslapcount"] = infantslapcount;
        options["ifantcount"] = parseInt(infantsseatcount) + parseInt(infantslapcount);
        //options["PreferLevel"] = PreferLevel;


        $$('[name=querystring]').val(JSON.stringify(options));

        $$('#tolist').submit();
        console.log(options);

    }


    else if(id=="pay")
    {

        $$('[name=orderid]').val(results["orderid"]);
        $$('[name=valid]').val(results["valid"]);
        $$('[name=token]').val(results["token"]);
        //console.log(JSON.stringify(result));
        $$('#topay').submit();
    }

    function searchairport(query){
//        console.log(query);

        var results = [];
        if (query.length === 0) {
            var mfuaplist=['YUL','YYZ','YVR','PEK','PVG','CAN','HKG','LAX','YYC','YOW','HGH','CTU','CDG','YEG'];
            for (var i = 0; i < airportcode.length; i++) {
                var index = tjq.inArray(airportcode[i].AIRPORTCODE.toUpperCase(), mfuaplist);
                if (index >= 0) {
                    var tmpstr="";
                    if(lang!=undefined && lang=="Chinese"){
                        tmpstr=airportcode[i].AIRPORTCODE + "-" + airportcode[i].CITY+ airportcode[i].AIRPORT+", "+ airportcode[i].COUNTRY;
                    }
                    else{
                        tmpstr=airportcode[i].AIRPORTCODE + "-" + airportcode[i].EN_CITY +" "+ airportcode[i].EN_AIRPORT+", "+ airportcode[i].EN_COUNTRY;
                    }
                    results[index]=tmpstr;
                }
            }
            return results;
        }
		var fcount=0;
        for (var i = 0; i < airportcode.length; i++) {
            var airportitem=airportcode[i];

            if (airportitem.PY_CITY.toLowerCase().indexOf(query.toLowerCase()) >= 0 ||
                airportitem.EN_CITY.toLowerCase().indexOf(query.toLowerCase()) >= 0 ||
                airportitem.CITY.toLowerCase().indexOf(query.toLowerCase()) >= 0 ||
                airportitem.AIRPORTCODE.toLowerCase().indexOf(query.toLowerCase()) >= 0 ||
                airportitem.AIRPORT.toLowerCase().indexOf(query.toLowerCase()) >= 0 ||
                airportitem.PY_AIRPORT.toLowerCase().indexOf(query.toLowerCase()) >= 0
                ||airportitem.EN_AIRPORT.toLowerCase().indexOf(query.toLowerCase()) >= 0
            ) {
                var tmpstr="";
                //var tmpstr=airportitem.AIRPORTCODE + "-" + airportitem.EN_CITY;
                if(lang!=undefined && lang=="Chinese"){
                    tmpstr=airportitem.AIRPORTCODE + "-" + airportitem.CITY+" "+ airportitem.AIRPORT+", "+ airportitem.COUNTRY;
                }
                else{
                    tmpstr=airportitem.AIRPORTCODE + "-" + airportitem.EN_CITY + airportitem.EN_AIRPORT+", "+ airportitem.EN_COUNTRY;
                }
                results.push(tmpstr);
				fcount++;
				if(fcount>=200)
					break;
            }
        }
        return results;
    }



    $$('.open-select-travel').on('click', function () {

        $$('.select-travel').on('popup:opened', function () {
            $$('.done-passengers').on('click',function(){
                myApp.closeModal('.select-travel');
            })
        });
        myApp.popup('.select-travel');

    });


    $$('#tab1').on('tab:show', function () {

        $$('#roundtripdate').show();
        $$('#onewaydate').hide();
    });

    $$('#tab2').on('tab:show', function () {
        $$(this).hide();
        $$('#tab1').show();

        $$('#roundtripdate').hide();
        $$('#onewaydate').show();
    });

    $$('.sub').on('click',function(){
        var targetstr=$$(this).attr('for');
        var min=0;
        if(targetstr=="adult"){
            min=1;
        }
        var targetdata=parseInt($$('#'+targetstr).attr('data'));
        if(targetdata>min){
            targetdata--;
        }else{
            return;
        }

        $$('#'+targetstr).attr('data',targetdata);
        if(targetdata==0){
            $$('#'+targetstr).val('');
        }else{
            $$('#'+targetstr).val(targetdata+" "+$$('#'+targetstr).attr('placeholder'));
        }
    })

    $$('.add').on('click',function(){

        var targetstr=$$(this).attr('for');
        var targetdata=parseInt($$('#'+targetstr).attr('data'));
        var total=parseInt($$('#adult').attr('data'))+parseInt($$('#kids').attr('data'))+parseInt($$('#infantseat').attr('data'))+parseInt($$('#infant').attr('data'));
        if(total>=6) return;
        var adultquantity=parseInt($$('#adult').attr('data'));
        var infantquantity=parseInt($$('#infantseat').attr('data'))+parseInt($$('#infant').attr('data'));
        if((targetstr=='infantseat')||(targetstr=='infant')) {
            if (infantquantity >= adultquantity) {
                return;
            }
        }
        targetdata++;
        $$('#'+targetstr).attr('data',targetdata);
        $$('#'+targetstr).val(targetdata+" "+$$('#'+targetstr).attr('placeholder'));


    })
    $$('.select-travel').on('popup:close', function () {
        var total=parseInt($$('#adult').attr('data'))+parseInt($$('#kids').attr('data'))+parseInt($$('#infantseat').attr('data'))+parseInt($$('#infant').attr('data'));
        $$('input#passengers').val(total+" @lang('language.Passenger')")
    });

    $$('#dosearch').on('click',function(){
        var formData = myApp.formToJSON('#search-form');
        console.log(formData);
        $$('#departure-popup').css('background-color','');
        $$('#arrive-popup').css('background-color','');
        $$('#calendar-range').parents('.item-content').css('background-color','');
        $$('#calendar-one').parents('.item-content').css('background-color','');
        var check=1;
        if(formData.src==""){

            $$('#departure-popup').css('background-color','rgba(255, 45, 85, 0.11)');
            check=0;
        }
        if(formData.dest==""){

            $$('#arrive-popup').css('background-color','rgba(255, 45, 85, 0.11)');
            check=0;
        }

        if($$('#roundtripdate').css('display')=="block" && formData.departuredateroundtrip==""){

            $$('#calendar-range').parents('.item-content').css('background-color','rgba(255, 45, 85, 0.11)');
            check=0;
        }
        if($$('#onewaydate').css('display')=="block" && formData.departuredateoneway==""){

            $$('#calendar-one').parents('.item-content').css('background-color','rgba(255, 45, 85, 0.11)');
            check=0;
        }


//        options=request.getSearchOption();


        if(check===1){
            //do search
            if(options===undefined){
                options = {
                    origin: '',
                    destination: '',
                    departuredate: '',
                    returndate: '',
                    onlineitinerariesonly: 'N',
                    adultcount:0,
                    childrencount:0,
                    ifantcount:0,
                    infantsseatcount:0,
                    infantslapcount:0,
                    limit: 50,
                    offset: 1,
                    maxstops:99,
                    PreferLevel:"Y",
                    preferredAirline:"",
                    priority:"Price",
                    sortby: 'totalfare',
                    order: 'asc',
                    sortby2: 'departuretime',
                    order2: 'dsc',
                    tripType:'',
                    currency:'',
                    rawsrcstr:'',
                    rawdeststr:'',
                    rawdatestr:''
                };
            }

            options["origin"]=formData.src;
            options["destination"]=formData.dest;

            options["rawsrcstr"]=$$('#departure-popup').find('.item-after').text();
            options["rawdeststr"]=$$('#arrive-popup').find('.item-after').text();

            //console.log(currency);
            if(typeof currencyone=="undefined")
                options["currency"]="CAD";
            else
                options["currency"]=currencyone;

            if($$('#roundtripdate').css('display')=="block"){
                var datesplit=formData.departuredateroundtrip.split("-");
                options['rawdatestr']=formData.departuredateroundtrip;
                //console.log(Date.parse(datesplit[0]));
                //console.log(formatDate(Date.parse(datesplit[0])));
                var ddate=formatDate(Date.parse(datesplit[0]));
                var rdate=formatDate(Date.parse(datesplit[1]));
                console.log(ddate);
                console.log(rdate);
                options["departuredate"]=ddate;
                options["returndate"]=rdate;
                options["tripType"]="roundtrip";
            }

            if($$('#onewaydate').css('display')=="block"){
                var ddate=formatDate(Date.parse(formData.departuredateoneway));
                options['rawdatestr']=formData.departuredateoneway;
                options["departuredate"]=ddate;
                options["returndate"]=null;
                options["tripType"]="oneway";
            }
            options["adultcount"]=$$('#adult').attr("data");
            options["childrencount"]=$$('#kids').attr("data");
            options["infantsseatcount"]=$$('#infantseat').attr("data");
            options["infantslapcount"]=$$('#infant').attr("data");
            options["ifantcount"]=parseInt(options["infantsseatcount"])+parseInt(options["infantslapcount"]);
            options["PreferLevel"]=$$('#PreferLevel').val();


            $$('[name=querystring]').val(JSON.stringify(options));

            $$('#tolist').submit();
            console.log(options);

/*
            if (options.tripType==="roundtrip") {
                FT_c_doAdvancedSearch(options, FT_p_loadRoundtripAdvancedSearchlist, FT_p_error_report);
            } else if (options.tripType==="oneway") {
                FT_c_doAdvancedSearch(options, FT_p_loadOnewayAdvancedSearchlist, FT_p_error_report);
            }

            itineraries = {};
            console.log(options);
            //FT_c_doSearch(options, FT_p_loadlist, FT_p_error_report);
*/



        }else{
            myApp.addNotification({
                title: 'Data validate',
                message: 'Please fill all required data!!'
            });
        }

    });



    function formatDate(date) {
        var d = new Date(date),
            month = '' + (d.getMonth() + 1),
            day = '' + d.getDate(),
            year = d.getFullYear();

        if (month.length < 2) month = '0' + month;
        if (day.length < 2) day = '0' + day;

        return [year, month, day].join('-');
    }


    function getJsonFromUrl() {
        var query = location.search.substr(1);
        var result = {};
        query.split("&").forEach(function(part) {
            var item = part.split("=");
            result[item[0]] = decodeURIComponent(item[1]);
        });
        return result;
    }

    function FT_p_loadRoundtripAdvancedSearchlist(departuredata, returndata) {

        if (departuredata == "NotFound" || departuredata == undefined) {
            return;
        }

        var departureDateData = [];
        var returnDateData = [];
        var advancedSearchData = {};
        var lowestPrice = '';
        for (var key in departuredata) {
            if (departuredata.hasOwnProperty(key)) {
                var departureValue = departuredata[key];
                var totalfee = parseFloat(departureValue['TotalFare']);
                var totalperson = parseInt(options.adultcount) + parseInt(options.childrencount) + parseInt(options.ifantcount);
                var avgfee = Math.round(totalfee * 100 / totalperson) / 100;
                if (!lowestPrice || lowestPrice > avgfee) {
                    lowestPrice = avgfee;
                }
            }
        }
        for (var key in departuredata) {
            if (departuredata.hasOwnProperty(key)) {
                var departureValue = departuredata[key];
                var departureDateTime = departureValue['DepartureDateTime'];
                var departureDate = departureDateTime.substring(0, 10);
                var totalfee=parseFloat(departureValue['TotalFare']);
                var totalperson = parseInt(options.adultcount) + parseInt(options.childrencount) + parseInt(options.ifantcount);
                var avgfee = Math.round(totalfee * 100 / totalperson) / 100;
                departureDateData.push(departureDate);

                //var currencyPrix = request.getCurrencyShow(departureValue['TotalFareCurrency']);

                var currencyPrix='$';
                var returnList = departureValue["RETURN_LIST"];
                for (var key_r in returnList) {
                    if (!isNaN(key_r)) {
                        var value_r = returnList[key_r];
                        var returnValue = returndata[value_r];
                        if (returnValue.hasOwnProperty('DepartureDateTime')) {
                            var returnDateTime = returnValue['DepartureDateTime'];
                            var returnDate = returnDateTime.substring(0, 10);
                            returnDateData.push(returnDate);

                            if (avgfee == lowestPrice) {
                                var dateLowestPrice = true;
                            } else {
                                var dateLowestPrice = false;
                            }

                            advancedSearchData[departureDate + '+' + returnDate] = {'price': currencyPrix + ' ' + avgfee, 'lowestPrice': dateLowestPrice};
                        }
                    }
                }
                //departureDateData = departureDateData.unique().sort();
                //returnDateData = returnDateData.unique().sort();
            }
        }

        //departureDateData = departureDateData.unique().sort();
        //returnDateData = returnDateData.unique().sort();


    }


    function FT_p_error_report(errorMsg, errorLevel, showtype) {
        tjq('.information').show();
        tjq('.information div').html(errorMsg);
        request.showProcessBar(100);
    }






if(lang=='English') {
    monthNames = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
    dayNamesShort=['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];
    }
else if(lang=="Chinese")
{
    monthNames = ['一月', '二月', '三月', '四月', '五月', '六月', '七月', '八月' , '九月' , '十月', '十一月', '十二月'];
    dayNamesShort=['日', '一', '二', '三', '四', '五', '六'];
}
else if(lang=="French")
{
    monthNames = ["Janvier", "Février", "Mars","Avril", "Mai", "Juin", "Juillet","Août", "Septembre", "Octobre","Novembre", "Décembre"];
    dayNamesShort=["DIM", "LUN", "MAR", "MER", "JEU", "VEN", "SAM"];
}


    $$(document).on('DOMContentLoaded', function(){
        var rangeselectdata=[];
        var selectdata=[];
        if(options!=undefined && options["rawdatestr"]!=undefined) {
            if(options["tripType"]=="roundtrip"){
                tjq('[name=departuredateroundtrip]').val(options["rawdatestr"]);
                rangeselectdata.push(options["departuredate"]+"T12:00:00");
                rangeselectdata.push(options["returndate"]+"T12:00:00");
            }else{
                tjq('[name=departuredateoneway]').val(options["rawdatestr"]);
                selectdata.push(options["departuredate"]+"T12:00:00");
            }

        }

        var totalperson=0;
        if(options!=undefined && options["childrencount"]!=undefined) {
            tjq('#kids').attr('data',options["childrencount"]);
            tjq('#kids').val(options["childrencount"]+" "+tjq('#kids').attr('placeholder'));
            totalperson+=parseInt(options["childrencount"]);
        }
        if(options!=undefined && options["infantsseatcount"]!=undefined) {
            tjq('#infantseat').attr('data',options["infantsseatcount"]);
            tjq('#infantseat').val(options["infantsseatcount"]+" "+tjq('#infantseat').attr('placeholder'));
            totalperson+=parseInt(options["infantsseatcount"]);
        }
        if(options!=undefined && options["infantslapcount"]!=undefined) {
            tjq('#infant').attr('data',options["infantslapcount"]);
            tjq('#infant').val(options["infantslapcount"]+" "+tjq('#infant').attr('placeholder'));
            totalperson+=parseInt(options["infantslapcount"]);
        }
        if(options!=undefined && options["adultcount"]!=undefined) {
            tjq('#adult').attr('data',options["adultcount"]);
            tjq('#adult').val(options["adultcount"]+" "+tjq('#adult').attr('placeholder'));
            totalperson+=parseInt(options["adultcount"]);
        }
        if(totalperson>0){
            tjq('input#passengers').val(totalperson+"@lang('language.Passenger')");
        }


        if(options!=undefined && options["PreferLevel"]!=undefined) {
            tjq('#PreferLevel').val(options["PreferLevel"]);
        }
/*
        if(isHolidays()) {
            alert("Dear custoemrs, thank you for visiting Sinorama air ticketing website. Our office will be closed on Dec. 25-26 and Jan. 1. During this period, we are unable to take any payment and issue ticket. Thanks for your understanding.");
        } else if (isClosed()) {
            alert("Dear cutomers, thank you for visiting Sinorama air ticketing website. Please be aware of our current working schedule is from Mon.-Fri. 10am to 6pm (EST). At any other time, you can still use our auto booking system, but we are unable to take any payment and issue ticket. Thank you for your understanding.");
        }
*/

        var today=new Date();
        var tomorrow = new Date(today.getTime() + (24 * 60 * 60 * 1000));
        var nexttomorrow=new Date(today.getTime() + (2 * 24 * 60 * 60 * 1000));

        var calendarOne= myApp.calendar({
            input: '#calendar-one',
            dateFormat: 'M dd yyyy',
            rangePicker: false,
            minDate:tomorrow,
            value:selectdata,
            onClose:function(){
                $$('#calendar-one').parents('.item-content').css('background-color','#FFF');
            },
            toolbarTemplate: '<div class="toolbar calendar-custom-toolbar">' +
                                '<div class="toolbar-inner">' +

                                        '<a href="#" class="left link icon-only"><i class="icon icon-back"></i></a>' +
                                        '<span class="center"></span>' +
                                        '<a href="#" class="right link icon-only"><i class="icon icon-forward"></i></a>' +
                                        '<a href="#" class="button active">Done</a>'+

                                '</div>' +
                            '</div>',
            onOpen: function (p) {
                $$('.calendar-custom-toolbar .center').text(monthNames[p.currentMonth] +', ' + p.currentYear);
                $$('.calendar-custom-toolbar .left.link').on('click', function () { calendarOne.prevMonth(); });
                $$('.calendar-custom-toolbar .right.link').on('click', function () { calendarOne.nextMonth(); });
                $$('.calendar-custom-toolbar .button').on('click', function () { calendarOne.close();  });

            },

            onMonthYearChangeStart: function (p) {
                $$('.calendar-custom-toolbar .center').text(monthNames[p.currentMonth] +', ' + p.currentYear);
            }
        });
        var calendarRange = myApp.calendar({
            input: '#calendar-range',
            dateFormat: 'M dd yyyy',
            rangePicker: true,
            minDate:tomorrow,
            value:rangeselectdata,
            onClose:function(){
                $$('#calendar-range').parents('.item-content').css('background-color','#FFF');
            },
            toolbarTemplate: '<div class="toolbar calendar-custom-toolbar-range">' +
                                '<div class="toolbar-inner">' +
                                    '<a href="#" class="left link icon-only"><i class="icon icon-back"></i></a>' +
                                    '<span class="center"></span>' +
                                    '<a href="#" class="right link icon-only"><i class="icon icon-forward"></i></a>' +
                                    '<a href="#" class="button active">@lang("language.Done")</a>'+
                                '</div>' +
                            '</div>'
            /*+'<div class="picker-calendar-week-days">' +
                '<div class="picker-calendar-week-day">'+dayNamesShort[0]+'</div>'+
                '<div class="picker-calendar-week-day">'+dayNamesShort[1]+'</div>'+
                '<div class="picker-calendar-week-day">'+dayNamesShort[2]+'</div>'+
                '<div class="picker-calendar-week-day">'+dayNamesShort[3]+'</div>'+
                '<div class="picker-calendar-week-day">'+dayNamesShort[4]+'</div>'+
                '<div class="picker-calendar-week-day picker-calendar-week-day-weekend">'+dayNamesShort[5]+'</div>'+
                '<div class="picker-calendar-week-day picker-calendar-week-day-weekend">'+dayNamesShort[6]+'</div>' +
            '</div>'*/,
            onOpen: function (p) {
                $$('.calendar-custom-toolbar-range .center').text(monthNames[p.currentMonth] +', ' + p.currentYear);
                //$$('.picker-calendar-week-day').text(dayNamesShort[0]);
                $$('.calendar-custom-toolbar-range .left.link').on('click', function () { calendarRange.prevMonth(); });
                $$('.calendar-custom-toolbar-range .right.link').on('click', function () { calendarRange.nextMonth(); });
                $$('.calendar-custom-toolbar-range .button').on('click', function () { tjq('.modal-overlay').click(); calendarRange.close(); });

            },

            onMonthYearChangeStart: function (p) {
                $$('.calendar-custom-toolbar-range .center').text(monthNames[p.currentMonth] +', ' + p.currentYear);
            }

        });


        var selectdata=[];
        if(options!=undefined && options["rawsrcstr"]!=undefined){
            selectdata.push(options["rawsrcstr"]);
            $$('#departure-popup').find('.item-after').text(selectdata[0]);
            $$('#departure-popup').find('input').val(selectdata[0].substr(0,3));
        }


        var departure = myApp.autocomplete({
            openIn: 'popup', //open in popup
            opener: $$('#departure-popup'),//link that opens autocomplete
            value:selectdata,
            backOnSelect: true, //go back after we select something
            popupCloseText: "@lang('language.Close')",
            searchbarPlaceholderText: "@lang('language.Clicksearch')...",
            searchbarCancelText: "@lang('language.Cancel')",
            requestSourceOnOpen: true,
            limit:200,
            notDisplayText: "@lang('language.notdisplay')",
            notFoundText: "@lang('language.notfound')",
			autoFocus: false,
            source: function (autocomplete, query, render) {
                // Render items by passing array with result items
                render(searchairport(query));
            },
            onChange: function (autocomplete, value) {
                // Add item text value to item-after
                $$('#departure-popup').find('.item-after').text(value[0]);
                $$('#departure-popup').css('background-color','#FFF');
                // Add item value to input value
                $$('#departure-popup').find('input').val(value[0].substr(0,3));
            },
            onOpen:function(p){
                tjq('.autocomplete-popup').find('.close-popup').on('click',function(e){
                    e.stopPropagation();
                    departure.close();
                    return false;
                });

            }
        });

        selectdata=[];
        if(options!=undefined && options["rawdeststr"]!=undefined){
            selectdata.push(options["rawdeststr"]);
            $$('#arrive-popup').find('.item-after').text(selectdata[0]);
            $$('#arrive-popup').find('input').val(selectdata[0].substr(0,3));
        }

        var arrive = myApp.autocomplete({
            openIn: 'popup', //open in popup
            opener: $$('#arrive-popup'),//link that opens autocomplete
            backOnSelect: true, //go back after we select somethin,
            popupCloseText: "@lang('language.Close')",
            searchbarPlaceholderText: "@lang('language.Clicksearch')...",
            searchbarCancelText: "@lang('language.Cancel')",
            value:selectdata,
            requestSourceOnOpen: true,
            limit:200,
            notDisplayText: "@lang('language.notdisplay')",
			notFoundText: "@lang('language.notfound')",
			autoFocus: false,
            source: function (autocomplete, query, render) {
                // Render items by passing array with result items
                render(searchairport(query));
            },
            onChange: function (autocomplete, value) {
                // Add item text value to item-after

                $$('#arrive-popup').find('.item-after').text(value[0]);
                $$('#arrive-popup').css('background-color','#FFF');
                // Add item value to input value
                $$('#arrive-popup').find('input').val(value[0].substr(0,3));
            },
            onOpen:function(p){
                tjq('.autocomplete-popup').find('.close-popup').on('click',function(e){
                    e.stopPropagation();
                    arrive.close();
                    return false;
                })
            }
        });

        if($$("#departure-popup .item-after").text()=="@lang('language.downloading')")
            $$("#departure-popup .item-after").text("");   //.text("@lang('language.Clicksearch')");
        if($$("#arrive-popup .item-after").text()=="@lang('language.downloading')")
            $$("#arrive-popup .item-after").text("");   //.text("@lang('language.Clicksearch')");
    });
</script>
