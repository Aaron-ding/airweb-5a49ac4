<div class="popup popup-detail" style="background: #efeff4">
    <div class="page">
        <div class="page-content">
            <div class="navbar" style="position: fixed; top: 0;left: 0;  z-index: 999;">
                <div class="navbar-inner">
                    <div class="left sliding">
                        <a href="#" class="link icon-only close-popup">
                            <i class="icon icon-back"></i><span class="">@lang("language.Close")</span>
                        </a>
                    </div>
                    <div class="center sliding" style="left: -30px;">@lang("language.InternaryDetails")</div>
                    <div class="right sliding" style="width:39px;">&nbsp;</div>
                </div>
            </div>
            <div class="card" style="margin-top: 50px;">
                <div class="card-fooder" style="padding-top: 3px;">
                    <h3>
                        <span id="departurecity" style="padding-left: 25%;"></span>
                            <img src="../dist/img/baggages/u1391.png" alt="">
                        <span id="arrivecity"></span>
                    </h3>
                    <span id="alltime"  style="padding-left: 25%"></span> ,<span id="allstops"></span> @lang("language.Stop")
                </div>
                <img src="dist/img/search_list___review/u1015.png" width="335">
                <div class="card-content" id="internarylist">
                    <div class="timeline" id="segmenttemplate" style="display:none;">
                        <div class="timeline-item">
                            <div class="timeline-item-date">
                                <span><span id="departdate"></span></span>
                                <b><span id="departtime"></span></b>
                            </div>
                            <div class="timeline-item-divider"></div>
                            <div class="timeline-item-content">
                                <div class="timeline-item-time">
                                    <span id="departureairport"></span>
                                </div>
                                <div class="timeline-item-time">
                                    <span id="departureTerminal"></span>
                                </div>
                            </div>
                        </div>
                        <div class="timeline-item">
                            <div class="timeline-item-date" style="color:red">
                                <span id="hours"  ></span>@lang('language.H')<span id="mins"></span>@lang('language.M')
                            </div>
                            <div class="timeline-item-divider"></div>
                            <div class="timeline-item-content">
                                <div class="timeline-item-text">
                                    <div>
                                        <span id="airline"></span> , <span id="aircarft"></span> , <span id="cabinclass"></span>  class
                                    </div>
                                    <div>
                                        <img id="airlinelogo" src="../dist/img/search_list___departure/delta_u355.png"
                                             width="22" height="22"><span id="airlinename"></span>
                                    </div>
                                    <div>
                                        <span id="opertator"></span>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="timeline-item">
                            <div class="timeline-item-date">
                                <span id="arrivedate"></span>
                                <b><span id="arrivetime"></span></b>
                            </div>
                            <div class="timeline-item-divider"></div>
                            <div class="timeline-item-content">
                                <div class="timeline-item-time">
                                    <span id="arriveairport"></span>
                                </div>
                                <div class="timeline-item-time">
                                    <span id="arriveTerminal"></span>
                                </div>
                            </div>
                        </div>

                    </div>

                    <div class="timeline" id="stoptemplate"  style="display:none;">
                        <!--<div class="timeline-item">
                            <div class="timeline-item-date">
                            </div>
                            <div class="timeline-item-divider"></div>
                            <div class="timeline-item-content">
                            </div>
                        </div>-->

                        <div class="timeline-item">
                            <div class="timeline-item-date">
                                <span id="stoptime" style="color:red"></span>
                            </div>
                            <div class="timeline-item-divider"></div>
                            <div class="timeline-item-content">
                                <span id="stopdesc"></span>
                            </div>
                        </div>
                        <!--
                        <div class="timeline-item">
                            <div class="timeline-item-date">
                            </div>
                            <div class="timeline-item-divider"></div>
                            <div class="timeline-item-content">
                            </div>
                        </div>-->
                    </div>
                </div>
            </div> <!--  card  -->
        </div>  <!--  page-content  -->
    </div>
</div><!--  popup  -->


<script>
    var rawinternarytemplate=tjq('.popup-detail').find('#internarylist').clone();
    var rawstoptemplate=tjq(rawinternarytemplate).find('#stoptemplate').clone();
    var segtemplate=tjq(rawinternarytemplate).find('#segmenttemplate').clone();


    function showdetail(obj){

        var data=request.getDconcat(tjq(obj).attr('data'));
        console.log(data);
        var detail=internary=data[0];
        if (typeof internary=="string"){
            detail=JSON.parse(internary);
        }
        console.log(detail);
        var segs=JSON.parse(detail.Segment);
        var segcount=0;
        var lastarrivetime=undefined;
        var lastcity=undefined;
        var lastteminnal=undefined;

        var internarytemplate=tjq(rawinternarytemplate).clone();
        if("@lang('language.Lang')"=="English") {
            tjq('.popup-detail').find('#departurecity').html(getCityNameByCode(detail.DepartureCity));
            tjq('.popup-detail').find('#arrivecity').html(getCityNameByCode(detail.ArrivateCity));
            tjq('.popup-detail').find('#alltime').html(detail.ElapsedTimeTotalHours + 'H ' + detail.ElapsedTimeTotalHoursLeftMins + 'M');
            tjq('.popup-detail').find('#allstops').html(detail.stops);
        }
        else if("@lang('language.Lang')"=="Chinese"){
            tjq('.popup-detail').find('#departurecity').html(getCityNameByCodeChinese(detail.DepartureCity));
            tjq('.popup-detail').find('#arrivecity').html(getCityNameByCodeChinese(detail.ArrivateCity));
            tjq('.popup-detail').find('#alltime').html(detail.ElapsedTimeTotalHours + '小时 ' + detail.ElapsedTimeTotalHoursLeftMins + '分钟');
            tjq('.popup-detail').find('#allstops').html(detail.stops);
        }

        else if("@lang('language.Lang')"=="French"){
            tjq('.popup-detail').find('#departurecity').html(getCityNameByCode(detail.DepartureCity));
            tjq('.popup-detail').find('#arrivecity').html(getCityNameByCode(detail.ArrivateCity));
            tjq('.popup-detail').find('#alltime').html(detail.ElapsedTimeTotalHours + 'H ' + detail.ElapsedTimeTotalHoursLeftMins + 'M');
            tjq('.popup-detail').find('#allstops').html(detail.stops);
        }

        tjq.each(segs,function(idx,seg) {
            segcount++;
            var departdate = new Date(seg.DepartureDateTime + "Z");
            if (lastarrivetime != undefined) {
                var stoptemplate = tjq(rawstoptemplate).clone();
                tjq(stoptemplate).css("display", "");
                var diff = departdate.getTime() - lastarrivetime.getTime();
                var diffhours = Math.floor(diff / 1000 / 60 / 60);
                var diffminutes = Math.floor(diff / 1000 / 60) % 60;



                var chanageairport = "";
                var changeterminal = "";

                if("@lang('language.Lang')"=="English") {
                    if (lastcity != seg.From) {
                        chanageairport = "，Go to another airport";
                    }
                    if (chanageairport == "" && lastteminnal != seg.From_TerminalID) {
                        changeterminal = "，Go to another terminal";
                    }
                    tjq(stoptemplate).find('#stoptime').text(diffhours + "H" + diffminutes + "M");
                    tjq(stoptemplate).find('#stopdesc').text('Transit in ' + getCityNameByCode(seg.From) + chanageairport + changeterminal);
                }
                else if("@lang('language.Lang')"=="Chinese"){
                    if (lastcity != seg.From) {
                        chanageairport = "，去另一个机场";
                    }
                    if (chanageairport == "" && lastteminnal != seg.From_TerminalID) {
                        changeterminal = "，去另一个航空站";
                    }
                    tjq(stoptemplate).find('#stoptime').text(diffhours + "小时" + diffminutes + "分钟");
                    tjq(stoptemplate).find('#stopdesc').text('转机在' +getAirportNameByCodebak(seg.From) +'('+getCityNameByCode(seg.From)+')' + chanageairport + changeterminal);
                }

                else if("@lang('language.Lang')"=="French"){
                    if (lastcity != seg.From) {
                        chanageairport = "，aller à un autre aéroport";
                    }
                    if (chanageairport == "" && lastteminnal != seg.From_TerminalID) {
                        changeterminal = "，aller à un autre terminal";
                    }
                    tjq(stoptemplate).find('#stoptime').text(diffhours + "H" + diffminutes + "M");
                    tjq(stoptemplate).find('#stopdesc').text('Correspondance à ' +getAirportNameByCode(seg.From) + chanageairport + changeterminal);
                }

                tjq(internarytemplate).append(tjq(stoptemplate));
            }


            if (seg.Airline != seg.OperatingAirline) {
                //if (language  == 'Chinese') {
                  //  var operatingAirlineDetail = "由" + seg.OperatingAirline + "运营";
                //} else {
                    var operatingAirlineDetail = "Operated by " + seg.OperatingAirline;
               // }
            } else {
                var operatingAirlineDetail = '';
            }


            var tmpseg=tjq(segtemplate).clone();
            tjq(tmpseg).css("display","");
            if("@lang('language.Lang')"=="English") {
                tjq(tmpseg).find('#departureairport').text(getAirportNameByCode(seg.From) + '(' + seg.From + ')');
                tjq(tmpseg).find('#departureTerminal').text('Terminal：' + seg.From_TerminalID);
            }
            else if("@lang('language.Lang')"=="Chinese"){
                tjq(tmpseg).find('#departureairport').text(getAirportNameByCodebak(seg.From) + '(' + seg.From + ')');
                tjq(tmpseg).find('#departureTerminal').text('Terminal(航空站)：' + seg.From_TerminalID);
            }
            else if("@lang('language.Lang')"=="French"){
                tjq(tmpseg).find('#departureairport').text(getAirportNameByCode(seg.From) + '(' + seg.From + ')');
                tjq(tmpseg).find('#departureTerminal').text('Terminal：' + seg.From_TerminalID);
            }
            tjq(tmpseg).find('#departdate').text(request.formatDateYMD(departdate).substr(5,5));
            tjq(tmpseg).find('#departtime').text(request.formatAMPM(departdate));
            tjq(tmpseg).find('#airline').text(seg.Airline+" "+seg.Flightnumber);
            tjq(tmpseg).find('#cabinclass').text(seg.MarketingCabin);
            tjq(tmpseg).find('#aircarft').text(seg.AirEquipType);

            tjq(tmpseg).find('#hours').text(seg.ElapsedTimeTotalHours);
            tjq(tmpseg).find('#mins').text(seg.ElapsedTimeTotalHoursLeftMins);
            tjq(tmpseg).find('#opertator').text(operatingAirlineDetail);

            //tjq(tmpseg).find('#airlinename').text(getAirlinesNameByCode(seg.Airline));
            tjq(tmpseg).find('#airlinelogo').attr('src','https://air.sinorama.ca/assets/images/logo/'+ tjq.trim(seg.Airline) + '.png');




            var arrivedate=new Date(seg.ArrivalDateTime+"Z");
            tjq(tmpseg).find('#arrivedate').text(request.formatDateYMD(arrivedate).substr(5,5));
            tjq(tmpseg).find('#arrivetime').text(request.formatAMPM(arrivedate));
            if(("@lang('language.Lang')"=="English")||("@lang('language.Lang')"=="French")) {
                tjq(tmpseg).find('#arriveairport').text(getAirportNameByCode(seg.To) + '(' + seg.To + ')');
                tjq(tmpseg).find('#arriveTerminal').text('Terminal：' + seg.To_TerminalID);
            }
            else if("@lang('language.Lang')"=="Chinese"){
                tjq(tmpseg).find('#arriveairport').text(getAirportNameByCodebak(seg.To) + '(' + seg.To + ')');
                tjq(tmpseg).find('#arriveTerminal').text('Terminal(航空站)：' + seg.To_TerminalID);
            }
            lastarrivetime=arrivedate;
            lastcity=seg.To;
            lastteminnal=seg.To_TerminalID;


            tjq(internarytemplate).append(tjq(tmpseg));


        });
        // console.log(tjq(internarytemplate)[0].outerHTML);
        tjq('.popup-detail').find('#internarylist').replaceWith(tjq(internarytemplate));


        $$('.popup-detail').on('popup:opened', function () {
            $$('.close-popup-detail').on('click',function(){
                myApp.closeModal('.popup-detail');
            });
        });
        myApp.popup('.popup-detail');

    }
</script>