
                   <!-- policy  -->
                    <div class="content-block">
                        <div class="content-block-title">
                            <p>
                                <img src="dist/img/u109.png" height="17" style="margin-right: 5px"><strong>@lang("language.FBI")</strong>
                            </p>
                        </div>
                    </div>
                    <div class="card">

                        <div class="card-content">

                            <div class="card-content-inner">

                                <div class="content-block terms">
                                    <p>
                                        @lang("language.policy1")
                                    </p>
                                    <p>
                                        @lang("language.policy2")
                                    </p>
                                    <p>
                                        @lang("language.policy3")
                                    </p>
                                    <p>
                                        @lang("language.policy4")
                                    </p>
                                    <p>
                                        @lang("language.policy5")
                                    </p>
                                    <p>
                                        @lang("language.policy6")
                                    </p>
                                    <p>
                                        @lang("language.policy7")
                                    </p>
                                    <p>
                                        @lang("language.policy8")
                                    </p>
                                    <p>
                                        @lang("language.policy9")
                                    </p>
                                    <p>
                                        @lang("language.policy10")
                                    </p>
                                </div>
                            </div>

                        </div>

                        <div class="card-footer">
                           <a href="#" class="link disagree-popup">@lang('language.Disagree')</a>
                            <a href="#" class="link agree-popup">@lang('language.Agree')</a>
                         <!--   <button id="agreeandpay"><?php echo "I agree" ?></button><button onclick="myApp.popup('.popup-policy');;"><?php echo "I dissagree" ?></button> -->
                        </div>
                    </div>



<script>


    $$('.agree-popup').on('click', function () {
        myApp.closeModal('.popup-policy');
        if(tjq('#iamdoingbook').length>0) return;



        tjq('#submitorder').html('<span id="iamdoingbook" class="preloader preloader-white"></span>');
        var rs = {};
        var personlist = new Array();
        tjq.each(tjq('.person-information'), function () {

            var dateText = tjq(this).find('input[name=birthday]').val();
            var departdate=request.getLastDepardate();
            var age=getAge(dateText,departdate);
            var persontype='ADT';
            if (age >= 12 && persontype != 'ADT') {
                var persontype = 'ADT';
            }

            if ((age>=2 && age<12) && persontype != 'CNN'){
                var persontype = 'CNN';
            }

            if (age<2 && (persontype != 'INF' && persontype != 'INS')){
                var persontype = 'INF';
                if(tjq(this).find('select[name=withseat]').val()=="yes"){
                    persontype='INS';
                }
            }



            var personindex = tjq(this).attr('dataidx');
            var personobj = tjq(this);
            var person = {};
            person['flyernumbers'] = [];
            person['index'] = personindex;
            person['passengertype'] = persontype;
            tjq.each(tjq(this).find("input,select"), function () {
                var id = tjq(this).attr('name');
                if (typeof id == 'undefined' || id == false) {
                    return;
                }

                var val = request.getValFromObj(tjq(this));

                if (id !== undefined && val != undefined) {
                    if (personindex == "contact") {
                        rs[id] = val;
                    } else {
                        person[id] = val;
                        if (id.indexOf('flyernumber_') != '-1' && val != '') {
                            var airline = id.substr(-2);
                            person['flyernumbers'].push(airline + val);
                        }
                    }
                }

            });
            if (personindex != "contact") {
                personlist.push(person);
                rs["passengers"] = personlist;
            }

        });

        var matches = rs['callingCode'].split(' ');
        //console.log(matches);
        rs['tel'] =  matches[1]+'-'+rs['tel1'];

        console.log(rs);
        request.booking = rs;

        var rsToken = getCookie('rsToken');
        if (rsToken) {
            request.booking['rsToken'] = rsToken;
        }

        request.src = 'mobile';
         console.log(request);

        //  set user email to connnect the order with user.
        //  login_userid is defined in the top of mainframe.php

        request.userid = login_userid;


        FT_c_storePassengersToSession(request, storePassengersSuccess);

        FT_c_doBooking(request, successBooking, function (value) {
         console.log(value);
         tjq('#progressbar'). css("display", "inline");
         tjq('#probar').attr('data-progress',value);
            var progress=tjq('#probar').attr('data-progress');
            var progressbar=tjq('#probar');
            myApp.setProgressbar(progressbar, progress);
            tjq('#submitorder').attr("disabled", true);
           });
    })


    $$('.disagree-popup').on('click', function () {
        myApp.closeModal('.popup-policy');


    })

    function storePassengersSuccess(rsToken) {
        console.log("show rsToken");
        console.log(rsToken);
        if (rsToken != 'error') {
            setCookie('rsToken', rsToken);
        }
    }


</script>
