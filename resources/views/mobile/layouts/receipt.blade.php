@extends('mobile.layouts.mainframe')
@section('extracss')
    <!-- Path to your custom app styles-->
    <link rel="stylesheet" href="dist/css/search-app.css">
@endsection

<?php
if (!empty($_REQUEST['orderid'])) {
    $orderid = $_REQUEST['orderid'];

} else {
    exit('Orderid is undefined.');
}
?>
@section('content')
    <!-- Views -->
    <div class="views">
        <!-- Your main view, should have "view-main" class -->
        <div class="view view-main">

            <!-- Pages container, because we use fixed-through navbar and toolbar, it has additional appropriate classes-->
            <div class="pages toolbar-through">
                <!-- Page, "data-page" contains page name -->
                <div data-page="index" class="page search">
                    <!-- Scrollable page content -->
                    <div class="page-content">
                        <div class="navbar navbar_list">
                            <div class="navbar-inner">
                                <!-- We need cool sliding animation on title element, so we have additional "sliding" class -->
                                <div class="left logo_text">
                                    <a href="/" class="external">
                                        <img src="../dist/img/logi_u4.png" style="margin-top: 5px">
                                    </a>
                                </div>
                                <!-- right
                                <div class="right  text-right">
                                    <span>
                                        <a href="#" data-panel="right" class="open-panel">
                                        <img src="../dist/img/u107.png" width="17">
                                        </a>
                                    </span>
                                </div> -->
                            </div>
                        </div>

                        <div class="page" id="page" >
                            <div class="page-content">
                                <!-- navbar -->
                                <div class="navbar navbar_list">
                                    <div class="navbar-inner">
                                        <!-- We need cool sliding animation on title element, so we have additional "sliding" class -->
                                        <div class="left logo_text">
                                            <a href="/" class="external">
                                                <img src="dist/img/logi_u4.png" style="margin-top: 5px">
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <!-- pay successful !-->
                                <div class="card book-card">
                                    <div class="card-content">
                                        <img src="dist/img/search_list___review/u1089.png" width="16" height="13">
                                        <div>
                                            <p style="font-size: 16px">@lang('language.PaySuccess')</p>
                                            <p class="content-block-title desc">@lang('language.PayS1')</p>

                                            <p>@lang('language.PayS2') </p>
                                        </div>
                                    </div>
                                </div>

                                <div class="card">
                                    <div class="card-content">
                                        <div class="card-footer">
                                            <a href="#" class="link">Your order NO.:</a>
                                            <a href="#" id="orderid" style="margin-right: 15%"></a>
                                            <a id="PNR" style="display:none"></a>
                                        </div>
                                        <div class="card-footer">
                                            <a href="#" style="color: dodgerblue">Your status:</a>
                                            <a href="#" style="color: dodgerblue">Paid</a>
                                            <a href="#" class="link"></a>
                                        </div>
                                    </div>
                                </div>
                                <h3 style="padding-left: 15px; color: #1D2088;">You can also:</h3>

                                <button id="gotohomepage" class="button button-round active" style="margin-top:20px;margin-bottom:20px;margin-left: 10%;width: 80%">Make a new book </button>

                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <div class="toolbar">
                <div class="toolbar-inner">

                    <a href="tel:1-866-255-2188" class="external">
                        <img src="dist/img/phone_u6.png" height="12" style="padding-right: 5px">1-866-255-2188
                    </a>

                    <a href="mailto:service@sinoramagroup.com" class="external">
                        <img src="dist/img/u115.png" height="12" style="margin-left: 0px; padding-right: 5px">service@sinoramagroup.com
                    </a>
                </div>
            </div>
        </div>
    </div>


@endsection

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(function () {
        tjq('#orderid').text('<?php echo $orderid; ?>');

        document.getElementById("gotohomepage").onclick = function () {
            if('<?php echo $env ?>' == 'master')
                location.href = "https://mo.air.sinorama.ca/";
            else
                location.href = "https://dev.mo.air.sinorama.ca/";
        }

    });
</script>



