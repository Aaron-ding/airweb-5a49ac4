@extends('mobile.layouts.mainframe')
@section('extracss')
    <!-- Path to your custom app styles-->
    <link rel="stylesheet" href="dist/css/my-app.css">
@endsection

@section('content')
    <!-- Views -->
    <div class="views">
        <!-- Your main view, should have "view-main" class -->
        <div class="view view-main">

            <!-- Pages container, because we use fixed-through navbar and toolbar, it has additional appropriate classes-->
            <div class="pages toolbar-through">
                <!-- Page, "data-page" contains page name -->
                <div data-page="index" class="page">
                    <!-- Scrollable page content -->
                    <div class="page-content">
                        <!-- Top Navbar-->

                    <marquee class="color-gray"> </marquee>

                        <div  class="card facebook-card" id="cardfacebook" style="display:none;">
                            <div class="card-header">
                                <div class="facebook-avatar"><img src="dist/img/sinorama_app.png" width="34" height="34"></div>
                                <div href="https://itunes.apple.com/us/app/sinorama-travel/id1324127839?ls=1&mt=8" class="facebook-name" style="color: #007EE5; padding-right:5px; ">@lang('language.APP')<b style="color: red; padding-left: 5px;">✈</b></div>
                                <div class="facebook-date">@lang("language.APP1")</div>
                            </div>

                        </div>
                    <!--<div class="card facebook-card" >
                            <div class="card-header">
                                <div class="facebook-avatar"><img src="dist/img/promotion/gift4.png" width="34" height="34"></div>
                                <div class="facebook-name" style="color: green; padding-right:5px; ">@lang('language.CP')<b style="color: red; padding-left: 5px;">sinotkt30</b></div>
                                <div class="facebook-date">@lang("language.CP1")</div>
                            </div>

                            <div class="card-content">
                                <div class="card-content-inner">
                                    <p class="color-gray" style="padding-left: 8px;"> <b style="font-size:38px; color: #007EE5;"> ☃ </b> @lang("language.CLO")</p>

                                </div>
                            </div>
                        </div>-->

                    @include('mobile.layouts.parts.searchframe')


                        <div class="index-footer" style="padding-left:8px; font-size: 12px; color: grey;">
                    <span>
                        <!--@lang('language.Claim')-->
                            <p>  © 2005-2018 Sinorama [ OPC:702569 ]</p>
                    </span>
                        </div>
                    </div>
                    998, boul. St-Laurent, Suite 518, POX.008 Montreal, QC, Canada H2Z 9Y9
                </div>
            </div>
        </div>
    </div>

    <input type="hidden" name="_token" id="csrf_token" value="{{csrf_token()}}">
@endsection
<script type="text/javascript" src="dist/js/framework7.js"></script>
<script type="text/javascript" src="dist/js/my-app.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script>
    $( document ).ready(function() {
    tjq('iframe', window.parent.document).height('450px');})
</script>
