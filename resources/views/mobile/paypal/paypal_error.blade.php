@extends('mobile.layouts.mainframe')
@section('extracss')
    <!-- Path to your custom app styles-->
    <link rel="stylesheet" href="dist/css/search-app.css">
@endsection
<?php
$token = md5('sinoramaWorld_02372785@!#d_air_API' . $orderid);
if($env== 'master'){
    $paymenturl = 'https://mo.air.sinorama.ca/pay?fullsite=yes&orderid='.$orderid.'&token='.$token.'&valid='.base64_encode(time());
    $homeurl='https://mo.air.sinorama.ca/';
}
else{
    $paymenturl = 'https://dev.mo.air.sinorama.ca/pay?fullsite=yes&orderid='.$orderid.'&token='.$token.'&valid='.base64_encode(time());
    $homeurl='https://dev.mo.air.sinorama.ca/';
}
$_SESSION["showhead"]="yes";
$fullsite="";

global $lang, $currency;

?>
@section('content')
    <!-- Views -->
    <div class="views">
        <!-- Your main view, should have "view-main" class -->
        <div class="view view-main">

            <!-- Pages container, because we use fixed-through navbar and toolbar, it has additional appropriate classes-->
            <div class="pages toolbar-through">
                <!-- Page, "data-page" contains page name -->
                <div data-page="index" class="page search">
                    <!-- Scrollable page content -->
                    <div class="page-content">
                        <div class="navbar navbar_list">
                            <div class="navbar-inner">
                                <!-- We need cool sliding animation on title element, so we have additional "sliding" class -->
                                <div class="left logo_text">
                                    <a href="/" class="external">
                                        <img src="../dist/img/logi_u4.png" style="margin-top: 5px">
                                    </a>
                                </div>
                                <!-- right
                                <div class="right  text-right">
                                    <span>
                                        <a href="#" data-panel="right" class="open-panel">
                                        <img src="../dist/img/u107.png" width="17">
                                        </a>
                                    </span>
                                </div> -->
                            </div>
                        </div>

                        <div class="card">
                            <div class="card-content card-content-padding">@lang('language.Orderfail')</div>
                            <div class="card-footer">
                                <a href="#" class="link" id="repay">@lang('language.Repay')</a>
                                <a href="#" class="link" id="reorder">@lang('language.Reorder')</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="toolbar">
                <div class="toolbar-inner">
                    <!-- Toolbar links -->
                <!--  <a href="tel:1-866-255-2188" class="external"><img src="dist/img/phone_u6.png"></a>
                <a href="mailto:service@sinoramagroup.com" class="external"><img src="dist/img/u115.png" height="15" style="margin-right: 3px"></a>
                <a href="#" class="link"><img src="../dist/img/u145.png" height="17" style="margin-right: 3px"></a>
                -->
                    <a href="tel:1-866-255-2188" class="external">
                        <img src="dist/img/phone_u6.png" height="12" style="padding-right: 5px">1-866-255-2188
                    </a>

                    <a href="mailto:service@sinoramagroup.com" class="external">
                        <img src="dist/img/u115.png" height="12" style="margin-left: 0px; padding-right: 5px">service@sinoramagroup.com
                    </a>
                </div>
            </div>
        </div>
    </div>


@endsection

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(function () {
    var orderid='<?php echo $orderid; ?>';
    console.log(orderid);
    var PNR='<?php echo $PNR; ?>';
    console.log(PNR);
    var paymenturl='<?php echo $paymenturl ?>';
    console.log(paymenturl);
    var homeurl='<?php echo $homeurl?>';
    //tjq('#redirect').css('font-size','50px');
    function redirct(){
        window.location=paymenturl;
    }
    window.setTimeout(redirct,5000);
    document.getElementById("repay").onclick = function () {
        location.href=paymenturl;
    };

    document.getElementById("reorder").onclick = function () {
        location.href = homeurl;
     }


    });
</script>