@extends('mobile.layouts.mainframe')
<?php


$_SESSION["showhead"]="no";
$fullsite="";


global $lang, $currency;


if (isset($_REQUEST) && isset($_REQUEST['orderid'])) {
    $orderid = $_REQUEST['orderid'];
} else {
    exit('Orderid is undefined.');
}

if(isset($_REQUEST['amt']))
    $amt=$_REQUEST['amt'];

if (isset($_REQUEST) && isset($_REQUEST['token'])) {
    $token = $_REQUEST['token'];

    /*
    if (!checkToken($orderid, $token)) {
        exit('Token is not valid.');
    }
    */
} else {
    //exit('Token is undefined.');
}

try {
    $db = new PDO('mysql:host=sinoramamasterdb.cugbitmg8jvq.us-west-2.rds.amazonaws.com;dbname=fts;charset=utf8', 'flight', 'flight');
} catch (PDOException $e) {
    print "Error!: " . $e->getMessage() . PHP_EOL;
    die();
}

$orderQuery = $db->prepare("SELECT * FROM t_order WHERE orderid = ?");
$orderQuery->execute(array($orderid));
$order = $orderQuery->fetch(PDO::FETCH_ASSOC);

if ($order['paystatus'] == 'paid') {
    exit('This order has already been paid.');
} elseif ($order['ticketstatus'] != 'booked') {
    exit('This order has not been booked.');
} elseif ($order['status'] == 'canceled') {
    exit('This order has been canceled.');
}

$db = null;

$optionId = 'all';
$method = 'allMethod';
$method = 'paypal';
$saleArea = '';

$newLang = strtolower($lang);
$pay_center_url = "https://asia.sinorama.ca/asia/index.php/asiatour/pay_Center/index/flight/".$orderid."_1/".$newLang;
$currency = $order['currency'];
$_SESSION["currency"] = $currency;

if($currency === 'NZD') {
    $saleArea = 'SA-0014';
} elseif($currency === 'AUD') {
    $saleArea = 'SA-0013';
}
else if($currency==='EUR'){
    $saleArea='SA-0011';
}
else if($currency==='GBP'){
    $saleArea='SA-0010';
}
else if($currency==='USD'){
    $saleArea='SA-0017';
}

function checkToken($orderid, $token) {
    $key = 'sinoramaWorld_02372785@!#d_air_API';

    return md5($key . $orderid) == $token;
}

?>
<div class="person-information ">
    <div class="form-group row">
        <div class="col-sm-6 col-md-5">
            <a class="button button-round active"  href="#" id="atosToPay">@lang('language.redirect_payment_center')</a>
        </div>
        <form id = "gotopaycenter" method = "POST" target="_parent" action = "<?php echo $pay_center_url ; ?>" >
            <input type = "hidden" name = "pay_center_optionId" value = "<?php echo $optionId; ?>" />
            <input type = "hidden" name = "pay_center_method" value = "<?php echo $method ?>" />
            <input type = "hidden" name = "pay_center_saleArea" value = "<?php echo $saleArea; ?>" />
            <input type = "hidden" name = "pay_center_currency" value = "<?php echo $currency; ?>" />
            <input type = "hidden" name = "pay_center_amount"  value = "<?php if(isset($amt)) echo number_format(floatval($order['totalprice']/100)-floatval($amt),2,'.',''); else echo number_format($order['totalprice']/100,2,'.',''); ?>"/>
            <input type = "hidden" name="pay_center_orderId" value="<?php echo $orderid; ?>" />
        </form>
    </div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
<script>
    $(document).ready(function() {
        tjq('#atosToPay').click(function (e) {
            e.preventDefault();

            //tjq(window.parent.document).find('.payment-opacity-overlay').show();
            myApp.popup('.payment-opacity-overlay');
            tjq('#gotopaycenter').submit();
        });
    });






</script>

