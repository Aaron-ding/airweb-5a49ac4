
@extends('mobile.layouts.mainframe')
@section('extracss')
    <!-- Path to your custom app styles-->
    <link rel="stylesheet" href="/dist/css/search-app.css">
@endsection
@section('content')

    <div class="views">
        <!-- Your main view, should have "view-main" class -->
        <div class="view view-main">

        @include('mobile.layouts.parts.navbar')

        <!-- Pages container, because we use fixed-through navbar and toolbar, it has additional appropriate classes-->
            <div class="pages toolbar-through navbar-through">

                <!-- Page, "data-page" contains page name -->
                <div data-page="myresetpwd" class="page">
                    <!-- Scrollable page content -->
                    <div class="page-content" style="background: #fff">

                        <div class="list" style="padding:30px 15px 0px 20px">
                            <div id="divresetpwdall" class="" style="margin-left:10px;" >
                                <form id="submitresetpwd" method="POST" action="/resetpwd">
                                    {{ csrf_field() }}
                                    <h3 class="" style="text-align: center">
                                        @lang("language.ChangePassword")
                                    </h3>
                                    <div class="row">
                                        <div class="col-95" style="padding: 15px 0px 15px 0px">
                                            @lang("language.EmailaddressUser")
                                        </div>
                                    </div>
                                    <div class="row" style="margin-left:10px;">
                                        <span id="user_email" style="font-weight:600; font-size:18px;">{{ session('email') }}</span>
                                        <span class="logerrorinf" id="rstpwderroremail">
                                            @lang("language.signuperror4")
                                            </span>
                                    </div>
                                    <input type="hidden" name="email" id="hideemail" value="{{ session('email') }}">
                                    <div class="row" style="padding: 20px 0px 5px 0px">
                                        <div class="col-95">
                                            @lang("language.oldpassword")
                                            <span class="logerrorinf" id="rstpwderrorpswd1">
                                            @lang("language.signuperror5")
                                            </span>
                                            <span class="logerrorinf" id="rstpwdgenerror2">
                                            @lang("language.generalerror2")
                                            </span>

                                        </div>
                                    </div>
                                    <div class="row">
                                        <input type="password" class="col-95" style="font-size: 15px; height: 35px; padding-left: 5px" id="old_pswd"
                                               name="oldpassword" placeholder="@lang('language.oldpassword')">
                                    </div>

                                    <div class="row" style="padding: 20px 0px 5px 0px">
                                        <div class="col-95">
                                            @lang("language.newpassword")
                                            <span class="logerrorinf" id="rstpwderrorpswd2">
                                            @lang("language.signuperror5")
                                            </span>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <input type="password" class="col-95" style="font-size: 15px; height: 35px; padding-left: 5px" id="new_pswd"
                                               name="newpassword" placeholder="@lang('language.Password')">
                                    </div>

                                    <div class="row" style="padding: 20px 0px 5px 0px">
                                        <div class="col-95">
                                            @lang("language.newpassword2")
                                            <span class="logerrorinf" id="rstpwderrorpswd3">
                                            @lang("language.signuperror7")
                                            </span>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <input type="password" class="col-95" style="font-size: 15px; height: 35px; padding-left: 5px" id="new_pswd2"
                                               name="newpassword2" placeholder="@lang('language.Password')">
                                    </div>

                                    <div class="row" style="margin-top:50px;font-size: 17px;">
                                        <div class="col-95">
                                            <a href="#" onclick="changepasswdsubmit();"
                                               class="button active">@lang("language.ChangePassword")</a>
                                        </div>
                                    </div>
                                <!--
                                    <div class="row" style="margin-top:15px;font-size: 14px;">
                                        <a href="#" onclick="changepasswdcancel();">
                                           @lang("language.Cancel")</a>
                                    </div>
                                    -->
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <input type="hidden" name="_token" id="csrf_token" value="{{csrf_token()}}">
            <div class="toolbar">
                <div class="toolbar-inner">
                    <!-- Toolbar links -->
                    <a href="tel:1-866-255-2188" class="external">
                        <img src="dist/img/phone_u6.png" height="12" style="padding-right: 5px">1-866-255-2188
                    </a>

                    <a href="mailto:service@sinoramagroup.com" class="external">
                        <img src="dist/img/u115.png" height="12" style="margin-left: 0px;padding-right: 5px">
                        service@sinoramagroup.com
                    </a>
                </div>
            </div>
        </div>

    </div>

@endsection


@section('extrascript')
    <script>

        function isEmail(email) {
            var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
            return regex.test(email);
        }

        function changepasswdsubmit(){
            tjq('#rstpwdgenerror2').hide();
            tjq('#rstpwderroremail').hide();
            tjq('#rstpwderrorpswd1').hide();
            tjq('#rstpwderrorpswd2').hide();
            tjq('#rstpwderrorpswd3').hide();


            var email=tjq("#user_email").text();
            var password=tjq("#old_pswd").val();
            var newpassword1=tjq("#new_pswd").val();
            var newpassword2=tjq("#new_pswd2").val();

            if(((email!="")&&(password!="")&&(newpassword1!="") && (newpassword2!="") &&
                (isEmail(email)) && (password.length>5) &&(newpassword1.length>5) &&(newpassword1==newpassword2))) {
                tjq('#submitresetpwd').submit();
            }
            if(!isEmail(email)){
                tjq('#rstpwderroremail').show();
            }
            if(password.length<6){
                tjq('#rstpwderrorpswd1').show();
            }
            if(newpassword1.length<6){
                tjq('#rstpwderrorpswd2').show();
                return false;
            }
            if(newpassword1!=newpassword2){
                tjq('#rstpwderrorpswd3').show();
            }
            return false;
        }

        function changepasswdcancel(){

            window.history.back();
        }

        tjq(document).ready(function () {
            @if ( $errors->first('email')=='notmatch')
            tjq('#rstpwdgenerror2').show();
            @endif

            tjq('#rstpwderroremail').hide();
            tjq('#rstpwderrorpswd1').hide();
            tjq('#rstpwderrorpswd2').hide();
            tjq('#rstpwderrorpswd3').hide();

            tjq('input[type="text"], input[type="password"], input[type="email"]').on('focus', function () {
                var target = this;
                setTimeout(function(){
                    target.scrollIntoViewIfNeeded();
                    setTimeout(function(){
                        target.scrollIntoViewIfNeeded();
                    },400);
                },300);
                tjq('.toolbar').hide();
            });
        });

    </script>
@endsection