
@extends('mobile.layouts.mainframe')
@section('extracss')
    <!-- Path to your custom app styles-->
    <link rel="stylesheet" href="dist/css/my-app.css">
@endsection

<?php
$detect = new Mobile_Detect;
if($detect->isMobile()){
if ($detect->version('iPhone')) {
    $phone = 'iphone';
}
if ($detect->version('Android'))
    $phone = 'android';}
else
    $phone='';
?>
@section('content')
    <!-- Views -->
    <div class="views">
        <!-- Your main view, should have "view-main" class -->
        <div class="view view-main">
            <!-- Top Navbar   include -->
            @include('mobile.layouts.parts.navbar')

            <!-- Pages container, because we use fixed-through navbar and toolbar, it has additional appropriate classes-->
            <div class="pages toolbar-through navbar-through">
                <!-- Page, "data-page" contains page name -->
                <div data-page="index" class="page">
                    <!-- Scrollable page content -->
                    <div class="page-content">

                    <!--<marquee class="color-gray">  </marquee>-->
                        <div  class="card facebook-card" id="cardfacebook" style="display:none;">
                            <div class="card-header">
                                <div class="facebook-avatar"><img src="dist/img/sinorama_app.png" width="34" height="34"></div>
                                <div href="https://itunes.apple.com/us/app/sinorama-travel/id1324127839?ls=1&mt=8" class="facebook-name" style="color: #007EE5; padding-right:5px; ">@lang('language.APP')<b style="color: red; padding-left: 5px;">✈</b></div>
                                <div class="facebook-date">@lang("language.APP1")</div>
                            </div>

                        </div>
                    <!--<div class="card facebook-card" >
                            <div class="card-header">
                                <div class="facebook-avatar"><img src="dist/img/promotion/gift4.png" width="34" height="34"></div>
                                <div class="facebook-name" style="color: green; padding-right:5px; ">@lang('language.CP')<b style="color: red; padding-left: 5px;">sinotkt30</b></div>
                                <div class="facebook-date">@lang("language.CP1")</div>
                            </div>

                            <div class="card-content">
                                <div class="card-content-inner">
                                    <p class="color-gray" style="padding-left: 8px;"> <b style="font-size:38px; color: #007EE5;"> ☃ </b> @lang("language.CLO")</p>

                                </div>
                            </div>
                        </div>-->

                        @include('mobile.layouts.parts.searcharea')

<!--
                        <div class="content-block">
                            <div class="chip" style="width: 100%">
                                <div class="chip-media"><img src="dist/img/sinorama_app.png"></div>
                                <div href="https://itunes.apple.com/us/app/sinorama-travel/id1324127839?ls=1&mt=8" class="chip-label">@lang('language.APP')</div><br>
                                <a href="#" class="chip-delete" style="padding-left: 15%;"></a>
                            </div>
                        </div>
                        -->
                        <div class="index-footer" style="padding-left:8px; font-size: 12px; color: grey;">
                    <span>
                        <!--@lang('language.Claim')-->
                      <p>  © 2005-2018 Sinorama [ OPC:702569 ]</p>
                    </span>
                    </div>
                    </div>
                998, boul. St-Laurent, Suite 518, POX.008 Montreal, QC, Canada H2Z 9Y9
                    <div class="toolbar">
                        <div class="toolbar-inner">
                            <!-- Toolbar links -->
                            <a href="tel:1-866-255-2188" class="external">
                                <img src="dist/img/phone_u6.png" height="12" style="padding-right: 5px">1-866-255-2188
                            </a>

                            <a href="mailto:service@sinoramagroup.com" class="external">
                                <img src="dist/img/u115.png" height="12" style="margin-left: 0px;padding-right: 5px">service@sinoramagroup.com
                            </a>

                        </div>
                    </div>
                </div>
            </div>
            <!-- Bottom Toolbar-->
        </div>
    </div>

<input type="hidden" name="_token" id="csrf_token" value="{{csrf_token()}}">
@endsection


@section('extrascript')
    <script>
        showcurrencyonnavbar=1;

        tjq(document).ready(function() {

            //console.log('');
            if('<?php echo $phone ?>'=='iphone')
                tjq('#cardfacebook').show();

            if(isHolidays()) {
                alert("Dear custoemrs, thank you for visiting Sinorama air ticketing website. Our office will be closed on Dec. 25-26 and Jan. 1. During this period, we are unable to take any payment and issue ticket. Thanks for your understanding.");
            } /*else if (isClosed()) {
            alert("Dear cutomers, thank you for visiting Sinorama air ticketing website. Please be aware of our current working schedule is from Mon.-Fri. 10am to 6pm (EST). At any other time, you can still use our auto booking system, but we are unable to take any payment and issue ticket. Thank you for your understanding.");
        }*/

        });

    </script>

@endsection






