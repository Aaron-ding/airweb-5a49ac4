@include('mobile.layouts.DnodeSyncClient')
<?php
$ip = '';
if (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
    $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
} elseif (!empty($_SERVER['HTTP_CLIENT_IP'])) {
    $ip = $_SERVER['HTTP_CLIENT_IP'];
} else {
    $ip = $_SERVER['REMOTE_ADDR'];
}

$host = ($env=='master'? "":"dev.")  . 'search.air.sinorama.ca';
$port = '9998';


if(Auth::check()) {

    $email=session('email');
    //contact info
    $para = array(
        array("userid" => $email)
    );
    $dnode = new \DnodeSyncClient\Dnode();

    try {
        $connection = $dnode->connect($host, $port);
        $contactData = $connection->call('getUsersProfiles', $para);
        $connection->close();
    } catch (Exception $e) {
        print "Error!: " . $e->getMessage() . PHP_EOL;
    }

    if ($contactData[0][0] == 'success') {
        $contactinfo = $contactData[0][1];
    }
    //var_dump($contactinfo);


    //passenger info
    $para2 = array(
        array("userid" => $email)
    );
    try {
        $connection = $dnode->connect($host, $port);
        $passengerData = $connection->call('getFrequentPassengers', $para2);
        $connection->close();
    } catch (Exception $e) {
        print "Error!: " . $e->getMessage() . PHP_EOL;
    }

    if ($passengerData[0][0] == 'success') {
        $passengerinfo = $passengerData[0][1];
    }
    //var_dump($passengerinfo);

}
?>


@extends('mobile.layouts.mainframe')
@section('extracss')
    <!-- Path to your custom app styles-->
    <link rel="stylesheet" href="dist/css/search-app.css">
    <link rel="stylesheet" href="css/skeuocard/styles/skeuocard.reset.css" />
    <link rel="stylesheet" href="css/skeuocard/styles/skeuocard.css" />

    <style>
        .skeuocard.js .cc-number input, .skeuocard.js .cc-name {
            display: inline-block !important;
            height: auto !important;
            width: auto !important;
        }
        .skeuocard.js .cc-exp input {
            display: inline-block !important;
            height: auto !important;
        }

        .payment-opacity-overlay {
            outline: none;
            position: fixed;
            left: 0;
            top: 0;
            width: 100%;
            height: 100%;
            background: rgba(0, 0, 0, 0.75);
            z-index: 10000;
            display: none;
            -webkit-backface-visibility: hidden;
        }

        .payment-opacity-overlay .container {
            height: 100%;
            position: relative;
            z-index: 10001;
        }
        .popup-wrapper {
            text-align: center;
            position: absolute;
            left: 0;
            top: 0;
            height: 100%;
            width: 100%; }
        .popup-wrapper:before {
            content: '';
            display: inline-block;
            height: 100%;
            vertical-align: middle; }
        .popup-wrapper .popup-content {
            width:100%;
            float: none;
            padding: 0;
            margin: 0 auto;
            text-align: left;
            z-index: 10003;
            position: relative;
            display: inline-block;
            vertical-align: middle; }
    </style>
@endsection


@section('content')
    <!-- Views -->
    <div class="views">
        <!-- Your main view, should have "view-main" class -->
        <div class="view view-main">

        @include('mobile.layouts.parts.navbar')

        <!-- Pages container, because we use fixed-through navbar and toolbar, it has additional appropriate classes-->
            <div class="pages toolbar-through navbar-through">
                <!-- Page, "data-page" contains page name -->
                <div data-page="index" class="page search">
                    <!-- Scrollable page content -->
                    <div class="page-content hide-toolbar-on-scroll">
                        <!-- Search condition review  -->
                        <div class="card">
                            <div class="card-content" style="padding: 0 5px">
                                <div class="card-footer">
                                    <h3 style="margin:0px;">
                                        <span id="headdeparture"></span>
                                        <img id="roundpicture" src="../dist/img/search_list___departure/return_u321.png"
                                             style="margin-left:20px;margin-right: 20px;display:none;">
                                        <img id="singlepicture"  src="../dist/img/baggages/u1391.png" style="margin-left:20px;margin-right: 20px;display:none;">
                                        <span id="headarrive"></span>
                                    </h3>
                                    <div class="f7-demo-icon" id="searchcard">
                                        <i class="f7-icons" style="color: #1d2088;">search_strong</i>
                                    </div>
                                </div>
                                <div class="card-footer">
                                    <span id="daterange"></span><br>
                                    <span></span>
                                </div>
                                <!--
                                <div class="card-footer">
                                    <span id="persondesc"></span>
                                    <span></span>
                                </div>

                                <div class="card-footer">
                                    <div class="trip">Base Fee:</div>
                                    <div class="total">
                                    <h2 style="margin-top: 0px;margin-bottom: 0px;margin-left: 0px;color:#c30d23">
                                        <span style="font-size:12px;color:grey;margin-left: 0px;">$</span><span id="basefare"></span>

                                    </h2>
                                    </div>
                                    <div style="padding-left: 35%">

                                    </div>
                                </div>
                                <div class="card-footer">
                                    <div class="trip">Tax Fee:</div>
                                  <div class="total">
                                      <h2 style="margin-top: 0px;margin-bottom: 0px;color:#c30d23">
                                      <span style="font-size:12px;color:grey;">$</span><span id="taxfee"></span>

                                     </h2>
                                  </div>
                                    <div style="padding-left: 35%">

                                    </div>
                                </div>-->
                                <div class="card-footer">
                                    <div class="trip">@lang('language.TripTotal')</div>
                                    <div class="total">
                                        <h2 style="margin-top: 0px;margin-bottom: 0px;color:#c30d23">
                                            <span style="font-size:12px;color:grey;" id="currency"></span><span style="font-size:19px;" id="totalamount"></span>

                                        </h2>
                                    </div>
                                    <div style="padding-left: 10%" id="showusdetail">
                                        <a href="#" > @lang('language.ShowDetails')
                                            <img src="../dist/img/search_list___departure/u469.png" width="7" height="7">
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="desc-grey" style="padding-left:18px;">
                        <span>@lang('language.State')
                        </span>
                        </div>
                        <!-- departure text describe
                        <div class="content-block  desc-departure" style=" margin-top: 50px;margin-bottom: -15px;">
                            <div class="content-block-title" style="color:#1D2088;" id="selecttitle">
                                <strong id="departdesc"></strong>
                                <strong>, to </strong>
                                <strong id="destdesc"></strong>
                            </div>
                            <div class="content-block-title desc" style="padding-left:8px;">
                                <p>Prices is the average, include taxes and other fees.</p>
                            </div>
                            <div class="content-block"></div>
                        </div>

                        <div class="content-block desc-arrive" style="    margin-top: 50px;margin-bottom: -5px;">
                            <div class="content-block-title" style="color:#1D2088;" id="selecttitle">
                                <strong id="returndepartdesc"></strong>
                                <strong>, to </strong>
                                <strong id="returndestdesc"></strong>
                            </div>

                            <div class="content-block"></div>
                        </div>F
-->
                        <!-- passenger -->
                        @include('mobile.layouts.parts.internarydesc')

                        <div class="content-block " style="  margin-top: 40px;margin-bottom: -15px;">
                            <div class="content-block-title" style="color:#1D2088;" id="selecttitle">
                                <strong>@lang('language.State1') </strong>
                            </div>
                            <div class="content-block-title desc" style="padding-left:8px;">
                                <p>
                                    <span id="adultcount"></span> @lang('language.Adult'),
                                    <span id="childcount"></span>@lang('language.Child') ,
                                    <span id="infantcount"></span> @lang('language.Infant')
                                </p>
                            </div>

                            <div class="content-block"></div>
                        </div>

                        <form id="passengers">

                            <div class="card" style="padding-bottom: 20px;">


                                @for($i=1;$i<=$personcount;$i++)
                                    <?php


                                    if ($i <= $adultcount) {
                                        $persontype = 'ADT';
                                        if ($i <= $infantslapcount) {
                                            $persontitle = 'Adult who carries an infant in lap';
                                        } else {
                                            $persontitle = 'Adult';
                                        }
                                    } else if (($i > $adultcount) && ($i <= (intval($adultcount) + intval($childrencount)))) {
                                        $persontype = 'CNN';
                                        $persontitle = 'Child';
                                    } else if (($i > (intval($adultcount) + intval($childrencount))) && ($i <= (intval($adultcount) + intval($childrencount) + intval($infantsseatcount)))) {
                                        $persontype = 'INS';
                                        $persontitle = 'Infant in seat';
                                    } else {
                                        $persontype = 'INF';
                                        $persontitle = 'Infant in lap';
                                    }
                                    ?>

                                    <div class="row" dataidx="{{$i}}" >
                                        <div class="col-10"></div>
                                        <div class="col-80">
                                            <a id="passenger{{$i}}" style="margin-top:20px;background: #007aff!important;border-color:#007aff;letter-spacing: 2px;" href="#" class="button button-round active passengerbutton" checkvalue="">@lang('language.Passenger') {{$i}}

                                                <span style="position: absolute;right: 10px;margin-top:2px;">
                                                    <i class="f7-icons">chevron_right</i>
                                                </span>
                                            </a>
                                        </div>
                                        <div class="col-10"></div>
                                    </div>


                                    <div class="popup popup-passenger-{{$i}} person-information" style="background: #efeff4"  dataidx="{{$i}}" >
                                        <div class="page ">
                                            <div class="page-content">
                                                <div class="navbar" style="position: fixed; top: 0; left: 0; z-index: 999;">
                                                    <div class="navbar-inner">
                                                        <div class="left sliding">
                                                            <a href="#" class="link close-popup"  dataidx="{{$i}}" >
                                                                <i class="f7-icons">chevron_left</i>
                                                                <span class="">@lang('language.Close')</span>
                                                            </a>
                                                        </div>
                                                        <div class="right sliding">
                                                            <a href="#" class="link save-popup"   dataidx="{{$i}}"  >
                                                                <span >@lang('language.Save')</span>
                                                                <i class="f7-icons">chevron_right</i></a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="card travellers-card" style="margin-top: 50px;">
                                                    <div class="card-content">
                                                        <div class="list-block">
                                                            <ul>
                                                                <li>
                                                                    <div class="item-content">
                                                                        <!-- Passenger Type-->
                                                                        <div class="item-inner">

                                                                            <p style="font-size: 14px;color: #007aff;" class="col-40"><?php echo $persontitle?></p>
                                                                            <div  id="passengertype-{{$i}}" style="display:none;"><?php echo $persontype ?></div>

                                                                        </div>
                                                                        <!--Select Passenger info. from system -->
                                                                        <div class="item-inner">
                                                                            <div class="item-input">
                                                                                <input type="text" placeholder="Select Passenger Info." readonly name="pickname" id="pickname-{{$i}}" style="display: none; text-align: right; padding-top: 10px; color: #007aff; font-weight: bold">

                                                                            </div>
                                                                        </div>
                                                                        <div class="item-media">
                                                                            <img src="../dist/img/search_list___departure/u469.png" style="padding-right: 15px; padding-top: 12px;">
                                                                        </div>
                                                                    </div>
                                                                </li>
                                                            <!--moving to the previous row
                                                                <li id="pickdiv" style="display: none;">
                                                                    <div class="item-content">
                                                                        <div class="item-inner">
                                                                            <div class="item-input">
                                                                                <input type="text" placeholder="Select information from system" readonly name="pickname" id="pickname-{{$i}}" style="display: none;">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </li>-->


                                                                <!--First Name Text inputs -->
                                                                <li>
                                                                    <div class="item-content">
                                                                        <div class="item-media">
                                                                            <i class="icon f7-icons">person</i>
                                                                        </div>
                                                                        <div class="item-inner">
                                                                            <div class="item-input">
                                                                                <input name="firstname" id="firstname" type="text" required style="height:30px;padding-left:10px;" placeholder="@lang('language.FirstName')">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </li>
                                                                <li>
                                                                    <div class="item-content">
                                                                        <div class="item-media">
                                                                            <i class="icon f7-icons">person</i>
                                                                        </div>
                                                                        <div class="item-inner">
                                                                            <div class="item-input">
                                                                                <input name="middlename" id="middlename" type="text"  style="height:30px;padding-left:10px;"  placeholder="@lang('language.MiddleName')">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </li>
                                                                <li>
                                                                    <div class="item-content">
                                                                        <div class="item-media">
                                                                            <i class="icon f7-icons">person</i>
                                                                        </div>
                                                                        <div class="item-inner">
                                                                            <div class="item-input">
                                                                                <input name="lastname" id="lastname" type="text" style="height:30px;padding-left:10px;" required placeholder="@lang('language.LastName') ">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </li>
                                                                <!-- gender Select -->
                                                                <li>
                                                                    <div class="item-content">
                                                                        <div class="item-media">
                                                                            <i class="icon f7-icons">person</i>
                                                                        </div>
                                                                        <div class="item-inner">
                                                                            <div class="item-input">
                                                                                <select name="gender" required style="padding-left:10px;">
                                                                                    <option value="M">@lang('language.Male')</option>
                                                                                    <option value="F">@lang('language.Female')</option>
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </li>

                                                                <!-- Date -->
                                                                <li>
                                                                    <div class="item-content">
                                                                        <div class="item-media"><i class="icon f7-icons">calendar</i>
                                                                        </div>
                                                                        <div class="item-inner">
                                                                            <div class="item-title label" id="birthflag" style="width: 40%; color: #767676;padding-left:10px;">@lang('language.Birthday')</div>
                                                                            <div class="item-input">
                                                                                <input name="birthday" id="birthday" type="date" required style="height:30px;padding-left:10px;" placeholder="Birth date ( * ) " value="1999-04-30">
                                                                            <!--
                                                                                 readonly id="calendar-birthday-{{$i}}"
                                                                                 -->
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </li>

                                                                <li id="showotheroptons" href="javascript:void(0)" style="padding-top: 10px;padding-left:20px;">
                                                                    <span style="font-size: 14px;color: #007aff;">@lang('language.OtherOptions') </span>
                                                                    <img src="../dist/img/search_list___departure/u469.png" style="margin-left: 5px">
                                                                </li>

                                                                <li style="padding-left: 20px;padding-right: 0px;">
                                                                    <span class="desc-grey">@lang('language.passreminder') </span>
                                                                </li>
                                                                <!-- nationality Select -->
                                                                <style>
                                                                    .popuptext::after{
                                                                        background-color:#fff!important;
                                                                    }
                                                                </style>

                                                                <div id="passportinfo">
                                                                    <li class="optional" style="display: none">
                                                                        <div class="item-content">
                                                                            <div class="item-media">
                                                                                <i class="icon f7-icons">world</i>
                                                                            </div>

                                                                            <div class="item-inner" style="margin-left: 10px;">
                                                                                <div class="item-input" style="margin-bottom: 0px; border: 0px; height: 30px;">
                                                                                    <a href="#" id="autocomplete-nationality-{{$i}}" class="item-link item-content autocomplete-opener">

                                                                                        <input name="nationality" popupid="autocomplete-nationality-{{$i}}"  style="height:30px;" type="hidden">
                                                                                        <div class="item-inner popuptext">
                                                                                            <div class="item-title" style="color: #767676;margin-right:0px;margin-left: 0px;">@lang('language.Nationality')
                                                                                            </div>
                                                                                            <div class="item-after"></div>
                                                                                        </div>
                                                                                    </a>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </li>
                                                                    <!-- Passport Number-->
                                                                    <li class="optional" style="display: none">
                                                                        <div class="item-content">
                                                                            <div class="item-media">
                                                                                <i class="icon f7-icons">document_text</i>
                                                                            </div>
                                                                            <div class="item-inner" style="margin-left: 10px;">
                                                                                <div class="item-title label" style="color: #767676;padding-left:18px;">@lang('language.PassportNumber')</div>
                                                                                <div class="item-input">
                                                                                    <input name="passport" type="text" id="passport"
                                                                                           placeholder="@lang('language.PassportNumber')" style="height:30px;padding-left:10px;">
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </li>

                                                                    <!-- passport validity -->
                                                                    <li class="optional" style="display: none">
                                                                        <div class="item-content">
                                                                            <div class="item-media">
                                                                                <i class="icon f7-icons">document_text</i>
                                                                            </div>
                                                                            <div class="item-inner" style="margin-left:10px;">
                                                                                <div class="item-title label" style="color: #767676;padding-left:18px;">@lang('language.ValidityPassport')</div>
                                                                                <div class="item-input">
                                                                                    <input name="passportexpired"  type="text" style="height:30px;padding-left:10px;"
                                                                                           placeholder="@lang('language.ValidityPassport')"
                                                                                           readonly id="passportvalidity-{{$i}}">
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </li>


                                                                    <!-- Issuing country Select -->
                                                                    <li class="optional" style="display: none">
                                                                        <div class="item-content">

                                                                            <div class="item-media">
                                                                                <i class="icon f7-icons">document_text</i>
                                                                            </div>

                                                                            <div class="item-inner" style="margin-left: 18px;">
                                                                                <div class="item-input" style="margin-bottom: 0px; border: 0px; height: 30px;">
                                                                                    <a href="#" id="autocomplete-issuing-{{$i}}"
                                                                                       class="item-link item-content autocomplete-opener">
                                                                                        <input name="passportcountry" popupid="autocomplete-issuing-{{$i}}" style="height:30px;" type="hidden">
                                                                                        <div class="item-inner popuptext">
                                                                                            <div class="item-title" style="color: #767676;">@lang('language.IssuingCountry')
                                                                                            </div>
                                                                                            <div class="item-after"></div>
                                                                                        </div>
                                                                                    </a>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </li>
                                                                </div>


                                                                <li class="optional" id="flyinfo" style="display: none">
                                                                    <div class="item-content" name="flyerplan" id="flyerplan" >
                                                                        <div class="item-media">
                                                                            <i class="icon f7-icons">card</i>
                                                                        </div>
                                                                        <div class="item-inner">
                                                                            <div class="item-title label" style="margin-left: 10px; color: #767676;">@lang('language.PointsPlan')</div>
                                                                            <div class="item-input">
                                                                                <input name="flyernumber" type="text" id="flyernumber"
                                                                                       placeholder="AMERICAN AIRLINES">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </li>

                                                                <li class="optional" style="display: none">
                                                                    <div class="item-content">
                                                                        <div class="item-media">
                                                                            <i class="icon f7-icons">compose</i>
                                                                        </div>
                                                                        <div class="item-title label" style="margin-left: 18px; width: 45%; color: #767676;">@lang('language.MealRequest')</div>
                                                                        <div class="item-inner">
                                                                            <div class="item-input">
                                                                                <select name="mealpreference" id="mealpreference" class="full-width">
                                                                                    <option value="">@lang('language.NSR')</option>
                                                                                    <option value="CHML">@lang('language.CHML')</option>
                                                                                    <option value="DBML">@lang('language.DBML')</option>
                                                                                    <option value="FPML">@lang('language.FPML')</option>
                                                                                    <option value="GFML">@lang('language.GFML')</option>
                                                                                    <option value="MOML">@lang('language.MOML')</option>
                                                                                    <option value="KSML">@lang('language.KSML')</option>
                                                                                    <option value="LCML">@lang('language.LCML')</option>
                                                                                    <option value="LFML">@lang('language.LFML')</option>
                                                                                    <option value="LSML">@lang('language.LSML')</option>
                                                                                    <option value="SFML">@lang('language.SFML')</option>
                                                                                    <option value="VJML">@lang('language.VJML')</option>
                                                                                    <option value="VLML">@lang('language.VLML')</option>
                                                                                    <option value="VGML">@lang('language.VGML')</option>
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </li>

                                                                <li class="optional" style="display: none">
                                                                    <div class="item-content">
                                                                        <div class="item-media">
                                                                            <i class="icon f7-icons">compose</i>
                                                                        </div>
                                                                        <div class="item-title label" style="margin-left: 18px; width: 45%; color: #767676;">@lang('language.OtherRequest')</div>
                                                                        <div class="item-inner">
                                                                            <div class="item-input">
                                                                                <select name="ssr" id="ssr" class="full-width">
                                                                                    <option value="">@lang('language.ssr_none')</option>
                                                                                    <option value="BLND">@lang('language.BLND')</option>
                                                                                    <option value="DEAF">@lang('language.DEAF')</option>
                                                                                    <option value="WCHS">@lang('language.WCHS')</option>
                                                                                    <option value="WCHR">@lang('language.WCHR')</option>
                                                                                    <option value="WCHC">@lang('language.WCHC')</option>
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </li>

                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                            @endfor

                            <!-- Contact -->
                                <div class="row">
                                    <div class="col-10"></div>
                                    <div class="col-80">
                                        <a id="passengercontact" style="margin-top:20px;background: #007aff!important;border-color:#007aff;letter-spacing: 2px;" href="#" class="button button-round active" checkvalue="">@lang('language.Contact')
                                            <span style="position: absolute;right: 10px;">
                                                <i class="f7-icons">chevron_right</i>
                                            </span>
                                        </a>
                                    </div>
                                    <div class="col-10"></div>

                                </div>


                                <div class="popup popup-passenger-contact person-information" style="background: #efeff4" dataidx="contact" >
                                    <div class="page ">
                                        <div class="page-content">
                                            <div class="navbar" style="position: fixed;top: 0;left: 0;z-index: 999;">
                                                <div class="navbar-inner">
                                                    <div class="left sliding">
                                                        <a href="#" dataidx="contact" class="link close-popup"><i class="f7-icons">chevron_left</i><span
                                                                    class="">@lang('language.Close')</span></a>
                                                    </div>
                                                    <div class="right sliding">
                                                        <a href="#"  dataidx="contact" class="link save-popup"><span class="">@lang('language.Save')</span><i class="f7-icons">chevron_right</i></a>
                                                    </div>
                                                </div>
                                            </div>
                                        <!-- hidden the clean area
                                            <div class="card"  xmlns="http://www.w3.org/1999/html" style="margin-top: 50px;">
                                                <div class="row no-gutter">
                                                    <div class="col-80 left-block">
                                                        <div class="row" style="padding-top:10px;padding-bottom:10px;padding-left: 10px">
                                                            <div> <a href="javascript:void(0);" id="takefromsystem" style="color: #3366CC; font-weight: 600; display: none">@lang("language.takeFromSystem")
                                                </a> </div>
                                        </div>
                                    </div>
                                    <div class="col-20">
                                        <a href="javascript:void(0);" id="clearform">
                                            <img src="../dist/img/Clear.png"   alt="Clear Passenger Info" width="25px" style="padding-top: 5px" />
                                        </a>
                                    </div>
                                </div>
                            </div>-->

                                            <div class="card travellers-card" style="margin-top: 50px;">
                                                <div class="card-content">
                                                    <div class="list-block">
                                                        <ul>
                                                            <li>
                                                                <div class="item-content">
                                                                    <div class="item-media">
                                                                        <a href="javascript:void(0);" id="takefromsystem" style="font-size: 15px;">@lang("language.Contact")
                                                                        </a>
                                                                    </div>
                                                                    <div class="item-media">
                                                                        <a href="javascript:void(0);" id="clearform">
                                                                            <img src="../dist/img/Clear.png"   alt="Clear Passenger Info" width="20px" style="padding-right: 15px" />
                                                                        </a>
                                                                    </div>
                                                                </div>
                                                            </li>
                                                            <!-- Text inputs -->
                                                            <li>
                                                                <div class="item-content">
                                                                    <div class="item-media"><i
                                                                                class="icon f7-icons">person</i>
                                                                    </div>
                                                                    <div class="item-inner">
                                                                        <div class="item-input">
                                                                            <input required name="contactfirstname" id="contactfirstname" type="text" style="height:30px;padding-left:15px;" placeholder="@lang('language.ContactFirstName')">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </li>
                                                            <li>
                                                                <div class="item-content">
                                                                    <div class="item-media"><i
                                                                                class="icon f7-icons">person</i></div>
                                                                    <div class="item-inner">
                                                                        <div class="item-input">
                                                                            <input required name="contactlastname" id="contactlastname" type="text" style="height:30px;padding-left:15px;" placeholder="@lang('language.ContactLastName')">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </li>
                                                            <li>
                                                                <div class="item-content">
                                                                    <div class="item-media"><i
                                                                                class="icon f7-icons">email</i></div>
                                                                    <div class="item-inner">
                                                                        <div class="item-input">
                                                                            <input name="email" id="email" required type="text" style="height:30px;padding-left:15px;" placeholder="@lang('language.Email')">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </li>
                                                            <!-- Country calling code -->
                                                            <li>
                                                                <div class="item-content">
                                                                    <div class="item-media"><i
                                                                                class="icon f7-icons">phone</i></div>
                                                                    <div class="item-inner">
                                                                        <div class="item-input" style="margin-bottom: -17px;">
                                                                            <a href="#" id="autocomplete-calling-code" class="item-link item-content autocomplete-opener">
                                                                                <input name="callingCode" popupid="autocomplete-calling-code" required type="hidden">
                                                                                <div class="item-inner">
                                                                                    <div class="item-title" style="color: #767676;">@lang('language.CountryCallingCode')</div>
                                                                                    <div class="item-after"></div>
                                                                                </div>
                                                                            </a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </li>
                                                            <!-- phone -->
                                                            <li>
                                                                <div class="item-content">
                                                                    <div class="item-media"><i
                                                                                class="icon f7-icons">phone</i></div>
                                                                    <div class="item-inner">
                                                                        <div class="item-input">
                                                                            <input name="tel1" id="tel1" required type="text" style="height:30px;padding-left:15px;" placeholder="@lang('language.Phone')">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </li>
                                                            <!-- Billing address -->
                                                            <li>
                                                                <div class="item-content">
                                                                    <div class="item-media"><i
                                                                                class="icon f7-icons">home</i></div>
                                                                    <div class="item-inner">
                                                                        <div class="item-input">
                                                                            <input name="address" id="address" required type="text" style="height:30px;padding-left:15px;" placeholder="@lang('language.BillingAddress')">

                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </li>
                                                            <!-- City -->
                                                            <li>
                                                                <div class="item-content">
                                                                    <div class="item-media"><i
                                                                                class="icon f7-icons">home</i></div>
                                                                    <div class="item-inner">
                                                                        <div class="item-input">
                                                                            <input name="city" id="city" required type="text" style="height:30px;padding-left:15px;" placeholder="@lang('language.City')">

                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </li>
                                                            <!-- Province / State -->
                                                            <li>
                                                                <div class="item-content">
                                                                    <div class="item-media">
                                                                        <i
                                                                                class="icon f7-icons">home</i></div>
                                                                    <div class="item-inner">
                                                                        <div class="item-input">
                                                                            <input name="province" id="province" required type="text" style="height:30px;padding-left:15px;" placeholder="@lang('language.ProvinceState')">

                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </li>
                                                            <!-- Country -->
                                                            <li>
                                                                <div class="item-content">
                                                                    <div class="item-media"><i
                                                                                class="icon f7-icons">home</i></div>
                                                                    <div class="item-inner" style="margin-left: 10px;">
                                                                        <div class="item-input" style="margin-bottom: 0px; border: 0px; height: 30px;">
                                                                            <a href="#" id="autocomplete-billcountry-{{$i}}"
                                                                               class="item-link item-content autocomplete-opener">
                                                                                <input name="country" popupid="autocomplete-billcountry-{{$i}}" required style="height:30px;" type="hidden">
                                                                                <div class="item-inner popuptext">
                                                                                    <div class="item-title" style="color: #767676;">@lang('language.Country')
                                                                                    </div>
                                                                                    <div class="item-after"></div>
                                                                                </div>
                                                                            </a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </li>

                                                            <!--
                                                            <li>
                                                                <div class="item-content">
                                                                    <div class="item-media">
                                                                        <i class="icon f7-icons">world</i>
                                                                    </div>

                                                                    <div class="item-inner" style="margin-left: 10px;">
                                                                        <div class="item-input" style="margin-bottom: 0px; border: 0px; height: 30px;">
                                                                            <a href="#" id="autocomplete-calling-code" class="item-link item-content autocomplete-opener">

                                                                                <input name="callingCode" popupid="autocomplete-calling-code" required style="height:30px;" type="hidden">
                                                                                <div class="item-inner popuptext">
                                                                                    <div class="item-title" style="color: #767676;margin-right:0px;margin-left: 0px;">Nationality ( * )
                                                                                    </div>
                                                                                    <div class="item-after"></div>
                                                                                </div>
                                                                            </a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </li>
                                                            -->
                                                            <!--   <li>
                                                                   <div class="item-content">
                                                                       <div class="item-media"><i
                                                                                   class="icon f7-icons">phone</i></div>
                                                                       <div class="item-inner">
                                                                           <div class="item-input" style="margin-bottom: -17px;">
                                                                               <a href="#" id="autocomplete-calling-code" class="item-link item-content autocomplete-opener">
                                                                                   <input name="callingCode" popupid="autocomplete-calling-code" required type="hidden">
                                                                                   <div class="item-inner">
                                                                                       <div class="item-title" style="color: #767676;">Country calling code ( * )
                                                                                       </div>
                                                                                       <div class="item-after"></div>
                                                                                   </div>
                                                                               </a>
                                                                           </div>
                                                                       </div>
                                                                   </div>
                                                               </li>

                                                           -->

                                                            <!-- Post code -->
                                                            <li>
                                                                <div class="item-content">
                                                                    <div class="item-media"><i
                                                                                class="icon f7-icons">home</i></div>
                                                                    <div class="item-inner">
                                                                        <div class="item-input">
                                                                            <input name="postcode" id="postcode" required type="text" style="height:30px;padding-left:15px;" placeholder="@lang('language.PostCode')">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </form>

                        <div class="desc-grey" style="padding-left:18px;">
                        <span>@lang('language.DW')
                        </span>
                        </div>

                        <div class="demo-progressbar-inline" id="progressbar" style="display: none;">
                            <p><span data-progress="50" class="progressbar" id="probar" style="height:9px;"></span></p>
                            <p style="color:#1D2088; padding-left: 5px;">@lang('language.waitinginfo')</p>
                        </div>
                        <!-- button -->
                        <div class="content-block" style="margin-top:18px;margin-button:30px;">
                            <a href="#" id="submitorder" class="button button-fill" style="word-spacing: 10px;letter-spacing: 2px;" >@lang('language.SubmitOrder') </a>
                        </div>

                        <!-- pay -->
                        <div id="divpayment" style="display:none;">
                            <div class="content-block">
                                <div class="" style="color:grey;margin-bottom:10px;">
                                    <p style="color: #1D2088;"><strong>@lang('language.reserveinfo1')</strong></p>
                                    <p id="pricechange" style="display:none; font-size: 12px;">@lang('language.reserveinfo2')
                                        <span id="formerfee"  style="color:red"></span>
                                        @lang('language.Too')
                                        <span id="updatefee" style="color:red"></span>.
                                        <br>@lang('language.reserveinfo3') </p>
                                </div>
                                <div id="couponForm" style="display: inline;">
                                    <hr>
                                    <h3 style="margin:15px;">@lang('language.Coupon')</h3>
                                    <div class="row" style="padding-bottom:10px;">
                                        <div class="col-60 "> <input type="text" id="couoncode" name="couoncode" style="width:-webkit-fill-available; height:30px; padding-left:3px;" value="" placeholder="" data-stripe="number"/>
                                        </div>
                                        <div class="col-40 " style=" padding-top: 5px;"><a href="#" id="buttonverify" class="button button-round active" style="background: #007aff!important;border-color:#007aff;">@lang('language.VerifyCoupon')</a>
                                        </div>
                                    </div>
                                    <div class="col-70 " style="padding-bottom:15px;">
                                        <span id="coupon_info_success" style="color:green;display:none"><i class="fa fa-check"></i>@lang('language.Verifyinfo1') <span id="coupon_amt">CAD 50</span></span>
                                        <span id="coupon_info" style="color:red;display:none"><i class="fa fa-times"></i>@lang('language.Verifyinfo2')<span id="coupon_extra_error"></span></span>
                                    </div>
                                </div>
                                <h4 style="color: #1D2088;">@lang('language.reserveinfo4')</h4>
                            </div>
                            <div class="list-block"  id="creditinformation">
                            <ul>
                                <li >
                                    <a href="#" id="rvpaybycredit" class="item-content item-link"  style="background: #90cdff;">
                                        <div class="item-inner">
                                            <div class="item-title" id="credittext" style="display:none;">1. @lang('language.CreditCard')</div>
                                            <div class="item-title" id="creditpic">1. @lang('language.CreditCard')
                                                @if ($env == 'master')
                                                    <img src="https://air.sinorama.ca/assets/images/visa.png" style="margin-left:20px;"/>
                                                    <img src="https://air.sinorama.ca/assets/images/mastercard.png" />
                                                    <img src="https://air.sinorama.ca/assets/images/ae.png" />
                                                @else
                                                    <img src="https://dev.air.sinorama.ca/flight_front/assets/images/visa.png" style="margin-left:20px;"/>
                                                    <img src="https://dev.air.sinorama.ca/flight_front/assets/images/mastercard.png" />
                                                    <img src="https://dev.air.sinorama.ca/flight_front/assets/images/ae.png" />
                                            @endif
                                            <!--     <p><font color="red" size="2">We have to charge 3% of transaction amount as service fee. </font></p> -->
                                            </div>
                                        </div>

                                    </a>
                                </li>
                                <div id="rvpaymentinfocredit">
                                    <div id="atos" style="display:none;">
                                        <iframe id="atosIframe" src="" width="100%"></iframe>
                                        <div>
                                            <span id="paymentmsg" class="red-color"></span>
                                        </div>
                                    </div>
                                    <div id="atos2" style="display:none;">
                                        <iframe id="atosIframe2" src="" width="100%"></iframe>
                                        <div>
                                            <span id="paymentmsg2" class="red-color"></span>
                                        </div>
                                    </div>
                                    <div id="creditcontent" class="card" style="display:block;">
                                        <div class="content-block" style="padding:20px;height:250px;!important;">

                                            <div class="credit-card-input no-js" id="skeuocard" style="position: absolute;top: 50%;left: 50%;transform: translate(-50%, -50%);">
                                                <p class="no-support-warning">
                                                    Either you have Javascript disabled, or you're using an unsupported browser, amigo! That's why you're seeing this old-school credit card input form instead of a fancy new Skeuocard. On the other hand, at least you know it gracefully degrades...
                                                </p>
                                                <label for="cc_type">Card Type</label>
                                                <select name="cc_type">
                                                    <option value="">...</option>
                                                    <option value="visa">Visa</option>
                                                    <option value="discover">Discover</option>
                                                    <option value="mastercard">MasterCard</option>
                                                    <option value="maestro">Maestro</option>
                                                    <option value="jcb">JCB</option>
                                                    <option value="unionpay">China UnionPay</option>
                                                    <option value="amex">American Express</option>
                                                    <option value="dinersclubintl">Diners Club</option>
                                                </select>
                                                <label for="cc_number">Card Number</label>
                                                <input type="text" name="cc_number" id="cc_number" placeholder="XXXX XXXX XXXX XXXX" maxlength="19" size="19">
                                                <label for="cc_exp_month">Expiration Month</label>
                                                <input type="text" name="cc_exp_month" id="cc_exp_month" placeholder="00">
                                                <label for="cc_exp_year">Expiration Year</label>
                                                <input type="text" name="cc_exp_year" id="cc_exp_year" placeholder="00">
                                                <label for="cc_name">Cardholder's Name</label>
                                                <input type="text" name="cc_name" id="cc_name" placeholder="John Doe">
                                                <label for="cc_cvc">Card Validation Code</label>
                                                <input type="text" name="cc_cvc" id="cc_cvc" placeholder="123" maxlength="3" size="3">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card" id="creditinfo" style="display:none">
                                        <div class="card-content">
                                            <div class="list-block">
                                                <ul style="background: #F2F2F2;">
                                                    <li>
                                                        <div class="item-content">
                                                            <div class="item-inner">
                                                                <div class="item-input">
                                                                    <input name="address1" id="adress1" required type="text" style="height:30px;padding-left:15px;" placeholder="@lang('language.BillingAddress1')">

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="item-content">
                                                            <div class="item-inner">
                                                                <div class="item-input">
                                                                    <input name="address2" id="adress2"  type="text" style="height:30px;padding-left:15px;" placeholder="@lang('language.BillingAddress2') ">

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="item-content">
                                                            <div class="item-inner">
                                                                <div class="item-input">
                                                                    <input name="city1" id="city1" required type="text" style="height:30px;padding-left:15px;" placeholder="@lang('language.City')">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="item-content">
                                                            <div class="item-inner">
                                                                <div class="item-input">
                                                                    <input name="province2" id="province2" required type="text" style="height:30px;padding-left:15px;" placeholder="@lang('language.ProvinceState')">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="item-content">
                                                            <div class="item-inner">
                                                                <div class="item-input">
                                                                    <input name="country1" id="country1" required type="text" style="height:30px;padding-left:15px;" placeholder="@lang('language.Country')">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="item-content">
                                                            <div class="item-inner">
                                                                <div class="item-input">
                                                                    <input name="postcode1" id="postcode1" required type="text" style="height:30px;padding-left:15px;" placeholder="@lang('language.PostCode')">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="item-content">
                                                            <div class="item-inner">
                                                                <p id="showreminder" style="color:#1D2088;margin-top: -8px;margin-bottom: -7px;display:none;font-size: 12px;">@lang('language.error1')</p>
                                                            </div>
                                                        </div>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <li >
                                    <a href="#" id="rvpayinstore" class="item-content item-link" style="background: #007aff;">
                                        <div class="item-inner" >
                                            <div class="item-title">2. @lang('language.Payinfo1') </div>
                                        </div>

                                    </a>
                                </li>
                                <div id="rvpaymentinfoinstore" style="display:none;">
                                    <div class="content-block">
                                        <p>
                                            <a href="#" id="printbookingreceipt" style="margin-left: 0px;">@lang('language.Payinfo2')</a> @lang('language.Or')<a href="#" id="emailbookingconfirmation" style="margin-left: 0px;">@lang('language.Payinfo3')</a>
                                        </p>
                                        <p>
                                            @lang('language.Payinfo4')
                                        </p>
                                        <p>
                                        <ul style="padding-left: 5px;">
                                            <li><strong>@lang('language.Montreal')</strong> <br>998 Boulevard Saint-Laurent,suite 518,<br> Montréal, Québec H2Z 9Y9</li>
                                            <br><li><strong>@lang('language.Schedule')</strong> <br>Mon. - Fri. 10:00 - 18:00 / <br>Sat. - Sun. 10:00 - 17:00</li>
                                            <br><li><strong>@lang('language.Telephone')</strong> 1-866-255-2188</li>
                                        </ul>
                                        <!--  <ul>
                                              <li>Toronto:  7077 Kennedy Road, #201, Markham ON L3R 0N8</li>
                                              <li>Scheduel： Mon. - Fri. 10:00 - 18:00 / Sat. - Sun. 10:00 - 17:00</li>
                                              <li>Telephone： 1-888-577-9918</li>
                                          </ul>
                                          <hr>
                                          <ul>
                                              <li>Vancouver:  8091 Westminster Hwy, #200 Richmond BC V6X 1A7</li>
                                              <li>Scheduel： Mon. - Fri. 10:00 - 18:00 / Sat. - Sun. 10:00 - 17:00</li>
                                              <li>Telephone： 1-866-255-2188</li>
                                          </ul>   -->
                                        </p>
                                    </div>
                                </div>
                                <li >
                                    <a href="#" id="rvpaybyetransfer" class="item-content item-link"  style=" background: #007aff;">
                                        <div class="item-inner">
                                            <div class="item-title">3. @lang('language.Payinfo5')</div>
                                        </div>
                                    </a>
                                </li>
                                <div id="rvpaymentinfoetransfer" style="display:none;">
                                    <div class="content-block">
                                        <p><a href="#" id="printbookingreceipt" style="margin-left: 0px;">@lang('language.Payinfo2')</a>
                                            or
                                            <a href="#" id="emailbookingconfirmation" style="margin-left: 0px;">@lang('language.Payinfo3')</a>
                                        </p>
                                        <p>@lang('language.Payinfo6') <strong>etransfer@sinoramagroup.com</strong></p>
                                        <p>@lang('language.Payinfo7') <strong>1-866-255-2188</strong>@lang('language.Payinfo8')</p>
                                        <p>@lang('language.Payinfo9')</p>

                                    </div>
                                </div>
                            </ul>
                        </div>
                    </div>

                    <div class="demo-progressbar-inline" id="progressbar1" style="display: none;">
                        <p><span data-progress="50" class="progressbar" id="probar1" style="height:9px;"></span></p>
                        <p style="color:#1D2088; font-size: 12px; padding-left: 10px;">@lang('language.WaitInfo')</p>
                    </div>

                    <div class="content-block" style="margin-top:20px;margin-button:120px;">
                        <a href="#" id="submitpayment" class="button button-fill" style="word-spacing: 20px;letter-spacing: 2px;display:none;" >@lang('language.SubmitPayment') </a>
                    </div>

                    <p>&nbsp;<br><br><br><br><br><br><br></p>

                </div>

                <div class="toolbar">
                    <div class="toolbar-inner">
                        <!-- Toolbar links -->
                        <a href="tel:1-866-255-2188" class="external">
                            <img src="dist/img/phone_u6.png" height="12" style="padding-right: 5px">1-866-255-2188
                        </a>

                        <a href="mailto:service@sinoramagroup.com" class="external">
                            <img src="dist/img/u115.png" height="12" style="margin-left: 0px;padding-right: 5px">service@sinoramagroup.com</a>

                        <!--
                        <a href="#" class="link"><img src="../dist/img/u145.png" height="17" style="margin-right: 3px"></a>
                        -->

                    </div>
                </div>
            </div>
        </div>

        <div class="popup popup-search" style="background: #efeff4">
            <div class="page">
                <div class="page-content">
                    <div class="navbar no-shadow ">
                        <div class="navbar-inner">
                            <div class="left sliding">
                                <a href="#" class="link close-popup" >
                                    <i class="icon icon-back"></i><span class="">@lang('language.Close')</span></a>
                            </div>
                        </div>
                    </div>
                    @include('mobile.layouts.parts.searcharea')
                </div>
            </div>
        </div>

        <div class="popup popup-policy" style="background: #efeff4">
            <div class="page">
                <div class="page-content">
                    <div class="navbar no-shadow ">
                        <div class="navbar-inner">
                            <div class="left sliding">
                                <a href="#" class="link close-popup" >
                                    <i class="icon icon-back"></i><span class="">@lang('language.Close')</span></a>
                            </div>
                        </div>
                    </div>
                    @include('mobile.layouts.parts.ticketpolicy')
                </div>
            </div>
        </div>

        <div class="popup popup-pricedetail" style="background: #efeff4">
            <div class="page">
                <div class="page-content">
                    <div class="navbar no-shadow ">
                        <div class="navbar-inner">
                            <div class="left sliding">
                                <a href="#" class="link close-popup-pd" >
                                    <i class="icon icon-back"></i><span class="">@lang('language.Close')</span></a>
                            </div>
                        </div>
                    </div>
                    @include('mobile.layouts.parts.pricedetail')
                </div>
            </div>
        </div>

        <div class="popup popup-paysuccess" style="background: #efeff4">
            <div class="page">
                <div class="page-content">
                    <!-- navbar -->
                    <div class="navbar navbar_list">
                        <div class="navbar-inner">
                            <!-- We need cool sliding animation on title element, so we have additional "sliding" class -->
                            <div class="left logo_text">
                                <a href="/" class="external">
                                    <img src="dist/img/logi_u4.png" style="margin-top: 5px">
                                </a>
                            </div>
                            <!-- right
                            <div class="right  text-right">
                            <span>
                                <a href="#" data-panel="right" class="open-panel">
                                <img src="../dist/img/u107.png" width="17">
                                </a>
                            </span>
                            </div>  -->
                        </div>
                    </div>
                    <!-- pay successful !-->
                    <div class="card book-card">
                        <div class="card-content">
                            <img src="dist/img/search_list___review/u1089.png" width="16" height="13">
                            <div>
                                <p style="font-size: 16px">@lang('language.PaySuccess')</p>
                                <p class="content-block-title desc">@lang('language.PayS1')</p>

                                <p>@lang('language.PayS2') </p>
                            </div>
                        </div>
                    </div>
                    <!-- <div class="card" style="margin-top: 20px;">

                         <div class="card-content">
                             <div class="content-block" style="padding-top:40px;">
                                 <div style="margin-bottom:40px;">
                                     <img src="dist/img/logi_u4.png" style="position: fixed;    left: 50%;
 transform: translate(-50%, -50%);">
                                 </div>

                                 <h3 class="">
                                     <img src="dist/img/search_list___review/u1089.png" width="16" height="13"style="margin-right:15px;">
                                     Congratulations ！ Pay Successfully !
                                 </h3>
                                 <p class="content-block-title desc">Thank you for choosing Sinorama!</p>

                                 <p class="">We will also send you a e-ticket email after we issue your ticket. </p>

                                 <p>Please check the e-ticket by ensure email.</p>
     </div>
 </div>
</div>-->
                    <div class="card">
                        <div class="card-content">
                            <div class="card-footer">
                                <a href="#" class="link">@lang('language.YoN')</a>
                                <a href="#" id="orderid" style="margin-right: 15%"></a>
                                <a id="PNR" style="display:none"></a>
                                <!-- <a href="#" data-popup=".popup-about" class="open-popup">
                                     <img src="dist/img/search_list___review/u1087.png">
                                 </a>-->
                            </div>
                            <div class="card-footer">
                                <a href="#" style="color: dodgerblue">@lang('language.Ys')</a>
                                <a href="#" style="color: dodgerblue">@lang('language.Paid')</a>
                                <a href="#" class="link"></a>
                            </div>
                        </div>
                    </div>
                    <!-- change style
                    <table style="margin:auto">
                                    <tr>
                                        <td align="right">
                                            <h3>Order number</h3>
                                        </td>
                                        <td>
                                            <span id="orderid"></span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">
                                            <h3>Status</h3>
                                        </td>
                                        <td>
                                            Paid
                                        </td>
                                    </tr>
                                </table>-->
                    <h3 style="padding-left: 15px; color: #1D2088;">@lang('language.Yca')</h3>

                    <button id="gotohomepage" class="button button-round active" style="margin-top:20px;margin-bottom:20px;margin-left: 10%;width: 80%">@lang('language.Mnb') </button>

                </div>
            </div>
        </div>
    </div>
    </div>
    @include('mobile.layouts.parts.internarydetail')


    <div class="popup payment-opacity-overlay" >
    <!-- <div class="container">
            <div class="popup-wrapper">
                <div id="paymentoptions">
                    <a href="javascript:void(0);" class="button active" id="paymentsuccess">@lang('language.Payment_successful')</a>
                    <a href="javascript:void(0);" class="button active" style="margin-left: 20px;" id="paymentfailed">@lang('language.Got_problems')</a>
                </div>
            </div>
        </div>
        -->
        <p>hello</p>
    </div>

    <div class="popup coupon-opacity-overlay" >

        <div class="card">
            <div class="card-content card-content-padding">@lang('language.Couponinfo')</div>
            <div class="card-content card-content-padding">@lang('language.Couponinfo2')</div>
            <div class="card-footer">
                <a href="#" class="link" id="return">@lang('language.Return')</a>
                <a href="#" class="link" id="continue">@lang('language.Continue')</a>
            </div>
        </div>
    </div>


    <input type="hidden" name="_token" id="csrf_token" value="{{csrf_token()}}">
@endsection



@section('extrascript')



    <script src="css/skeuocard/javascripts/vendor/cssua.min.js"></script>
    <script src="css/skeuocard/javascripts/skeuocard.js"></script>
    <script type="text/javascript" src="https://js.stripe.com/v2/"></script>


    <script>
                @if($env == 'master')
        var cleanUrl = "{{route("clean")}}";
        console.log("master:");
        console.log(cleanUrl);

                @else
        var cleanUrl = "{{route("dev_clean")}}";
        console.log("dev:");
        console.log(cleanUrl);

                @endif

        var bookingErrors = {
                'soldout':"@lang("language.soldout")",
                'systemException': "@lang('language.systemException')"
            };
        var stripeErrors = {
            'PNR_Save': "An error occurred while processing the card.",
            'PNR_Retrieve': "An error occurred while processing the card.",
            'FOP_CreateFormOfPayment': "An error occurred while obtaining credit card approval.",
            'EndTransactionLLSRQ': "An error occurred while processing the card.",
            'TravelItineraryReadRQ': "An error occurred while processing the card.",
            'CreditVerificationRQ': "An error occurred while obtaining credit card approval.",
            'AddRemarkLLSRQ_Creditcard': "An error occurred while processing the card.",
            'invalid_coupon': "The coupon is not valid.",
            'coupon_processing_error': "An error occurred while processing the coupon.",
            'invalid_number': "The card number is not a valid credit card number.",
            'invalid_expiry_month': "The card's expiration month is invalid.",
            'invalid_expiry_year': "The card's expiration year is invalid.",
            'invalid_cvc': "The card's security code is invalid.",
            'invalid_swipe_data': "The card's swipe data is invalid.",
            'incorrect_number': "The card number is incorrect.",
            'expired_card': "The card has expired.",
            'incorrect_cvc': "The card's security code is incorrect.",
            'incorrect_zip': "The card's zip code failed validation.",
            'card_declined': "The card was declined. Please try again.",
            'missing': "There is no card on a customer that is being charged.",
            'processing_error': "An error occurred while processing the card."
        };

        var lang_array={
            "Price_Change":"Your ticket price changed from formerfee to updatefee. The airline could not confirm the original price due to pricing or availability changes that occurred after we posted the latest prices on our site. Continue booking or look for a different flight."
        };


        var options;
        var request = new Request();



        var gostring='@php echo addslashes(html_entity_decode(old("selectdata"), ENT_QUOTES)); @endphp';
        var returnstring='@php echo addslashes(html_entity_decode(old("returndata"), ENT_QUOTES)); @endphp';
        //console.log(gostring);
        //console.log(returnstring);

        tjq(document).ready(function() {
            //console.log(request);

            //var goseglist = JSON.parse(request[goKey]);
            //console.log(goseglist);


            var itineraries = {};
            var param="";

            options = JSON.parse('@php echo html_entity_decode($querystring, ENT_QUOTES); @endphp');
            console.log(options);
            //request.clientIp = '207.96.163.34';
            request.clientIp = '<?php echo $ip; ?>';
            console.log(request.clientIp);
            //request.provider="sabre";


            console.log(window.location.href);
            var url=window.location.href;
            //console.log("the length of url");
            //var length=url.length;
            //console.log(length);
            var position=url.search("provider");
            //console.log(position);
            var start=Math.floor(position)+17;
            //console.log(start);
            var stringing=url.substr(start,5);
            //console.log(stringing);
            if(stringing=="sabre")
                var stringer=stringing;
            if(stringing=="amade")
                var stringer="amadeuse";
            request.provider=stringer;
            console.log(request.provider);


            //request.refer="mo.air.sinorama.ca";
            //request.refer='<?php echo ""; ?>';

            var referer = '<?php
                echo Session::get('referer'); ?>';
            if (referer != undefined && referer != '') {
                var result=parseURL(referer);
                //console.log(result);
                var host=result['host'];
                request.referer = host;
            }

            if(options.tripType=="roundtrip")
            {
                tjq('#roundpicture').css("display", "");
            }
            if(options.tripType=="oneway")
            {
                tjq('#singlepicture').css("display", "");
            }


            request.srccity = options.origin;
            request.destcity = options.destination;
            request.departdate =options.departuredate;
            request.returndate = options.returndate;
            request.adultcount = options.adultcount;
            request.childrencount = options.childrencount;
            request.ifantcount = options.ifantcount;
            request.infantsseatcount =options.infantsseatcount;
            request.infantslapcount = options.infantslapcount;
            request.tripType=options.tripType;
            //$personcount=Math.floor(options.adultcount)+Math.floor(options.childrencount)+Math.floor(options.ifantcount);
            //console.log($personcount);
            var adultcount=parseInt(options.adultcount);
            var childrencount=parseInt(options.childrencount);
            var infantsseatcount=parseInt(options.infantsseatcount);
            var infantslapcount=parseInt(options.infantslapcount);

            var selectdata = JSON.parse(gostring);
            if(options.tripType=="roundtrip"){
                if(returnstring==null)
                    alert("please select agian");
                else
                    var returndata = JSON.parse(returnstring);

            }

            else if(options.tripType=="oneway")
                var returndata=null;

            var tmpstr=request.concat(gostring,returnstring);
            request.dconcat(tmpstr);
            //console.log(request.dconcat(tmpstr));

            tjq('#basefare').text(request.basefee);
            tjq('#taxfee').text(request.taxfee);
            tjq('#totalamount').text(request.totalfee);
            var currshow=request.getCurrencyShow(options.currency);
            if(options.currency=="USD"){currshow="US$";}
            tjq('#currency').text(currshow);



            tjq('#adultcount').text(request.adultcount);
            tjq('#childcount').text(request.childrencount);
            tjq('#infantcount').text(request.ifantcount);

            if("@lang('language.Lang')"=="English") {
                tjq('#headdeparture').html(getCityNameByCode(options.origin));
                tjq('#headarrive').html(getCityNameByCode(options.destination));
                tjq('[id=destdesc]').html(getCityNameByCode(options.destination));
                tjq('[id=returndestdesc]').html(getCityNameByCode(options.origin));
            }
            else if("@lang('language.Lang')"=="Chinese"){
                tjq('#headdeparture').html(getCityNameByCodeChinese(options.origin));
                tjq('#headarrive').html(getCityNameByCodeChinese(options.destination));
                tjq('[id=destdesc]').html(getCityNameByCodeChinese(options.destination));
                tjq('[id=returndestdesc]').html(getCityNameByCodeChinese(options.origin));
            }
            else if("@lang('language.Lang')"=="French"){
                tjq('#headdeparture').html(getCityNameByCode(options.origin));
                tjq('#headarrive').html(getCityNameByCode(options.destination));
                tjq('[id=destdesc]').html(getCityNameByCode(options.destination));
                tjq('[id=returndestdesc]').html(getCityNameByCode(options.origin));
            }

            //section for airline
            console.log("gokey:");

            var goseglist = JSON.parse(request.goKey);
            if(options.tripType=="roundtrip")
                var returnlist = JSON.parse(request.returnKey);
            else if(options.tripType=="oneway")
                var returnlist=[]       ;
            var airlines = [];

            console.log(goseglist);
            console.log(returnlist);
            tjq.each(goseglist.concat(returnlist), function (segidx, segval) {
                if (airlines.indexOf(segval.Airline) == '-1') {
                    airlines.push(segval.Airline);
                }
            });
            console.log(airlines);
            console.log(airlines.length);
            for (var i = 1; i <= airlines.length; i++) {
                var airline = airlines[i-1];
                var airlineName = getAirlinesNameByCode(airline);
                console.log(airlineName);
                var newflyer = tjq('#flyerplan').clone();
                tjq(newflyer).find("#flyernumber").attr('placeholder', airlineName);
                tjq(newflyer).find("#flyernumber").attr('name',"flyernumber_" + airline);
                tjq(newflyer).find("#flyernumber").attr('id', "flyernumber_" + airline);
                tjq(newflyer).attr('id',"flyerplan_" + airline);

                //tjq(newflyer).find("label[for=flyernumber]").attr("for", "flyernumber_" + airline);
                //tjq(newflyer).attr('id', "flyerplan_" + airline);
                //tjq(newflyer).show();
                tjq('#flyinfo').prepend(newflyer);
            }
            tjq('#flyerplan').remove();
            //console.log(request);





            var persondesc = options.adultcount + ' Adult(s)';

            if (options.childrencount > 0) {
                persondesc += "," + options.childrencount + "Child(ren)";
            }
            if (options.infantsseatcount > 0) {
                persondesc += "," + options.infantsseatcount + "Infant(s)(seat)";
            }
            if (options.infantslapcount > 0) {
                persondesc += "," + options.infantslapcount + "Infant(s)";
            }

            tjq('#persondesc').html(persondesc);


            var fromdate = Date.parse(options.departuredate).toString();
            tjq('[id=departdesc]').html(fromdate.substr(0, 15));

            if(options.tripType=="roundtrip") {
                var returndate = Date.parse(options.returndate).toString();
                tjq('[id=returndepartdesc]').html(returndate.substr(0, 15));

                var todate = Date.parse(options.returndate).toString();
                var datedesc = fromdate.substr(0, 15) + " ~ " + todate.substr(0, 15);
                tjq('#daterange').html(datedesc);
            }
            else if(options.tripType=="oneway"){
                var datedesc = fromdate.substr(0, 15)
                tjq('#daterange').html(datedesc);

            }





            var gotemplate = tjq('#ft_item_template').clone();

            var departureview = buildTemplate(gotemplate, selectdata);
            tjq(departureview).find('#selectbutton').hide();
            tjq(departureview).find('#pricearea').html('');
            tjq('.desc-departure').after(tjq(departureview));

            if(options.tripType=="roundtrip") {
                var templatereturn = tjq('#ft_item_template').clone();
                var returnview = buildTemplate(templatereturn, returndata);
                tjq(returnview).find('#selectbutton').hide();
                tjq(returnview).find('#pricearea').html('');
                tjq('.desc-arrive').after(tjq(returnview));
            }


            $$('#showusdetail').on('click', function () {
                myApp.popup('.popup-pricedetail');
                showpricedetail(tjq(options),tjq(request));
            })


            $$('#searchcard').on('click', function () {
                myApp.popup('.popup-search');
            })


            $$('.passengerbutton').on('click', function () {
                var dataidx=tjq(this).parents("[dataidx]").attr("dataidx");
                myApp.popup('.popup-passenger-'+dataidx);
            })

            $$('.close-popup').on('click', function () {


                var dataidx=tjq(this).attr("dataidx");
                if(dataidx){
                    var div = document.getElementById("passengertype-"+dataidx);
                    console.log(div);


                    if(div!=null) {                  // to judge the passenger's age
                        var personType = div.textContent;
                        //var personType=tjq('.popup-passenger-'+dataidx).find('#passengertype').val();
                        //var personType=;
                        console.log("passengertype");
                        console.log(personType);

                        var dateText = tjq('.popup-passenger-' + dataidx).find('#birthday').val();
                        console.log(dateText);
                        var departdate = request.getLastDepardate();
                        console.log(departdate);
                        var checktype = true;

                        var age = getAge(dateText, departdate);
                        console.log(age);

                        if ((age >= 12) && (personType != 'ADT')) {
                            checktype = false;
                        }

                        if ((age >= 2) && (age < 12) && (personType != 'CNN')) {
                            checktype = false;
                        }

                        if ((age < 2) && (personType != 'INF') && (personType != 'INS')) {
                            checktype = false;
                        }

                        if (!checktype) {
                            checkPassengerType = false;
                            console.log("hello false");
                            request.showRequired(tjq(this).find('#birthday'));
                            tjq('.popup-passenger-' + dataidx).find('#birthflag').css('background', 'rgba(255, 45, 85, 0.11)');
                            alert("birthday selection did not match with the customer type, please select again");
                        } else {
                            request.removeRequired(tjq(this).find('#birthday'));

                        }


                        //nationality selection's judgement
                        //var passport=document.getElementById("passportinfo");
                        var nationality=tjq('#autocomplete-nationality-'+dataidx).find('input').val();
                        console.log('nationality');
                        console.log(nationality);

                        var passport=tjq('.popup-passenger-' + dataidx).find('#passport').val();
                        console.log('passport');
                        console.log(passport);

                        var passportvalidity=tjq('.popup-passenger-' + dataidx).find('#passportvalidity-'+dataidx).val();
                        console.log('passportvalidity');
                        console.log(passportvalidity);

                        var passportcountry=tjq('#autocomplete-issuing-'+dataidx).find('input').val();
                        console.log('passportcountry');
                        console.log(passportcountry);

                        if(((nationality=="")&&(passport=="")&&(passportvalidity=="")&&(passportcountry==""))||((nationality!="")&&(passport!="")&&(passportvalidity!="")&&(passportcountry!="")))
                        {
                            console.log("passport pass");
                            PassportFormCorrect(tjq('.popup-passenger-'+dataidx).find('#passportinfo'));
                        }
                        else
                        {
                            PassportFormError(tjq('.popup-passenger-'+dataidx).find('#passportinfo'));
                            checktype = false;
                            alert("@lang("language.error6")");
                        }



                        //console.log(checkpassport);
                    }
                    else checktype=true;



                    var checkformrs=(checkform(tjq('.popup-passenger-'+dataidx)))&&checktype;
                    if(checkformrs){
                        tjq('#passenger'+dataidx).find('i').html('check_round_fill');
                        tjq('#passenger'+dataidx).attr("checkvalue","true");
                    }
                    else {
                        tjq('#passenger' + dataidx).find('i').html('close_round_fill');
                        alert("@lang('language.error1')");
                        tjq('#passenger'+dataidx).attr("checkvalue","false");
                    }
                }
            })

            $$('.save-popup').on('click', function () {
                var dataidx=tjq(this).attr("dataidx");
                //console.log(tjq('.popup-passenger-'+dataidx));


                if(dataidx){
                    var div = document.getElementById("passengertype-"+dataidx);
                    if(div!=null) {
                        var personType = div.textContent;
                        //var personType=tjq('.popup-passenger-'+dataidx).attr("persontype");
                        console.log("passengertype");
                        console.log(personType);
                        var dateText = tjq('.popup-passenger-' + dataidx).find('#birthday').val();
                        console.log(dateText);
                        var departdate = request.getLastDepardate();
                        console.log(departdate);
                        var checktype = true;

                        var age = getAge(dateText, departdate);
                        console.log(age);

                        if ((age >= 12) && (personType != 'ADT')) {
                            checktype = false;
                        }

                        if ((age >= 2) && (age < 12) && (personType != 'CNN')) {
                            checktype = false;
                        }

                        if ((age < 2) && (personType != 'INF') && (personType != 'INS')) {
                            checktype = false;
                        }


                        if (!checktype) {
                            checkPassengerType = false;
                            console.log("hello false");
                            request.showRequired(tjq(this).find('#birthday'));
                            tjq('.popup-passenger-' + dataidx).find('#birthflag').css('background', 'rgba(255, 45, 85, 0.11)');
                            alert("@lang('language.birthdayerror')");
                        } else {
                            request.removeRequired(tjq(this).find('#birthday'));



                            var nationality=tjq('#autocomplete-nationality-'+dataidx).find('input').val();
                            console.log('nationality');
                            console.log(nationality);

                            var passport=tjq('.popup-passenger-' + dataidx).find('#passport').val();
                            console.log('passport');
                            console.log(passport);

                            var passportvalidity=tjq('.popup-passenger-' + dataidx).find('#passportvalidity-'+dataidx).val();
                            console.log('passportvalidity');
                            console.log(passportvalidity);

                            var passportcountry=tjq('#autocomplete-issuing-'+dataidx).find('input').val();
                            console.log('passportcountry');
                            console.log(passportcountry);

                            if(((nationality=="")&&(passport=="")&&(passportvalidity=="")&&(passportcountry==""))||((nationality!="")&&(passport!="")&&(passportvalidity!="")&&(passportcountry!="")))
                            {
                                console.log("passport pass");
                                PassportFormCorrect(tjq('.popup-passenger-'+dataidx).find('#passportinfo'));
                            }
                            else
                            {
                                PassportFormError(tjq('.popup-passenger-'+dataidx).find('#passportinfo'));
                                checktype = false;
                                alert("@lang('language.error4')");
                            }
                        }
                    }
                    else checktype=true;
                    var checkformrs=(checkform(tjq('.popup-passenger-'+dataidx)))&&checktype;
                    console.log("check");
                    console.log(checkformrs);
                    if(checkformrs){                            //if there is not any error
                        tjq('#passenger'+dataidx).find('i').html('check_round_fill');
                        myApp.closeModal('.popup-passenger-'+dataidx);
                        tjq('#passenger'+dataidx).attr("checkvalue","true");
                    }
                    else{                                      //if there are some errors
                        alert("@lang('language.error1')");
                        return;
                        tjq('#passenger'+dataidx).attr("checkvalue","false");

                    }
                }
            })

            $$('#passengercontact').on('click', function () {
                myApp.popup('.popup-passenger-contact');
                <?php
                if(isset($contactinfo)&&($contactinfo!="No_records")){
                ?>

                tjq('#contactfirstname').val("<?php echo $contactinfo["firstname"] ?>");
                tjq('#contactlastname').val("<?php echo $contactinfo["lastname"] ?>");
                tjq('#email').val("<?php echo $contactinfo["email"] ?>");

                tjq('#autocomplete-calling-code').find('.item-after').text("<?php echo $contactinfo["callingcode"] ?>");
                tjq('#autocomplete-calling-code').find('input[name="callingCode"]').attr('value',"<?php echo $contactinfo["callingcode"] ?>");


                tjq('#tel1').val("<?php echo $contactinfo["phonenumber"] ?>");
                tjq('#address').val("<?php echo $contactinfo["address"] ?>");
                tjq('#city').val("<?php echo $contactinfo["city"] ?>");
                tjq('#province').val("<?php echo $contactinfo["province"] ?>");
                tjq('#autocomplete-billcountry-{{$i}}').find('.item-after').text('<?php echo $contactinfo["country"] ?>');
                tjq('#autocomplete-billcountry-{{$i}}').find('input[name="country"]').attr('value','<?php echo $contactinfo["country"] ?>');

                tjq('#postcode').val("<?php echo $contactinfo["postcode"] ?>");




                <?php } ?>





            });

            <?php
            if(isset($contactinfo)&&($contactinfo!="No_records")){
            ?>
            tjq("#takefromsystem").show();
            tjq("#takefromsystem").on('click',function(){
                tjq('#contactfirstname').val("<?php echo $contactinfo["firstname"] ?>");
                tjq('#contactlastname').val("<?php echo $contactinfo["lastname"] ?>");
                tjq('#email').val("<?php echo $contactinfo["email"] ?>");

                tjq('#autocomplete-calling-code').find('.item-after').text("<?php echo $contactinfo["callingcode"] ?>");
                tjq('#autocomplete-calling-code').find('input[name="callingCode"]').attr('value',"<?php echo $contactinfo["callingcode"] ?>");


                tjq('#tel1').val("<?php echo $contactinfo["phonenumber"] ?>");
                tjq('#address').val("<?php echo $contactinfo["address"] ?>");
                tjq('#city').val("<?php echo $contactinfo["city"] ?>");
                tjq('#province').val("<?php echo $contactinfo["province"] ?>");
                tjq('#autocomplete-billcountry-{{$i}}').find('.item-after').text('<?php echo $contactinfo["country"] ?>');
                tjq('#autocomplete-billcountry-{{$i}}').find('input[name="country"]').attr('value','<?php echo $contactinfo["country"] ?>');

            });
            <?php } ?>


            tjq("#clearform").on('click',function(){
                tjq('#contactfirstname').val("");
                tjq('#contactlastname').val("");
                tjq('#email').val("");

                tjq('#autocomplete-calling-code').find('.item-after').text("");
                tjq('#autocomplete-calling-code').find('input[name="callingCode"]').attr('value',"");


                tjq('#tel1').val("");
                tjq('#address').val("");
                tjq('#city').val("");
                tjq('#province').val("");
                tjq('#autocomplete-billcountry-{{$i}}').find('.item-after').text('');
                tjq('#autocomplete-billcountry-{{$i}}').find('input[name="country"]').attr('value','');

                tjq('#postcode').val("");
            });

            <?php if(isset($passengerinfo)&&($passengerinfo!="No_records"))
            { ?>

            @for($i=1;$i<=$personcount;$i++)
            tjq('#pickdiv').show();
            tjq('#pickname-{{$i}}').show();
            var persontype=document.getElementById("passengertype-{{$i}}").textContent;
            var value=[];
            var information={};
            <?php foreach($passengerinfo as $passenger)
                {?>
                information["<?php echo $passenger["lastname"]." ".$passenger["middlename"]." ".$passenger["firstname"]?>"]=<?php echo json_encode($passenger) ?>;
            console.log(information["<?php echo $passenger["lastname"]." ".$passenger["middlename"]." ".$passenger["firstname"]?>"]);
            var dateText="<?php echo $passenger["birthday"]; ?>";
            var departdate=request.getLastDepardate();
            var age = getAge(dateText, departdate);
            if ((age >= 12 && persontype == 'ADT')||((age>=2 && age<12) && (persontype == 'CNN'))||(age<2 && (persontype == 'INF' || persontype == 'INS')))
            {
                //matched
                value.push("<?php echo $passenger["lastname"]." ".$passenger["middlename"]." ".$passenger["firstname"]?>");


            }

            var pickerDevice{{$i}} = myApp.picker({
                input: '#pickname-{{$i}}',
                cols: [
                    {
                        textAlign: 'center',
                        //values: ['iPhone 4', 'iPhone 4S', 'iPhone 5', 'iPhone 5S', 'iPhone 6', 'iPhone 6 Plus', 'iPad 2', 'iPad Retina', 'iPad Air', 'iPad mini', 'iPad mini 2', 'iPad mini 3']
                        values: value,
                        onChange: function(picker){
                            //console.log(picker.cols[0].value);
                            tjq('.popup-passenger-{{$i}}').find('input[name="middlename"]').val(information[picker.cols[0].value]["middlename"]);
                            tjq('.popup-passenger-{{$i}}').find('input[name="firstname"]').val(information[picker.cols[0].value]["firstname"]);
                            tjq('.popup-passenger-{{$i}}').find('input[name="lastname"]').val(information[picker.cols[0].value]["lastname"]);
                            //tjq('.popup-passenger-{{$i}}').find('select[name="gender"]').filter('option[value="' + information[picker.cols[0].value]["gender"] + '"]');
                            tjq('.popup-passenger-{{$i}}').find('select[name="gender"]').val(information[picker.cols[0].value]["gender"]);
                            tjq('.popup-passenger-{{$i}}').find('input[name="birthday"]').val(information[picker.cols[0].value]["birthday"]);
                            tjq('.popup-passenger-{{$i}}').find('#'+"autocomplete-nationality-{{$i}}").find('.item-after').text(countryreverse(information[picker.cols[0].value]["nationality"]));
                            tjq('.popup-passenger-{{$i}}').find('input[name="nationality"]').attr('value',information[picker.cols[0].value]["nationality"]);
                            tjq('.popup-passenger-{{$i}}').find('input[name="passportexpired"]').val(information[picker.cols[0].value]["documentvalidity"]);
                            tjq('.popup-passenger-{{$i}}').find('#passport').val(information[picker.cols[0].value]["documentnumber"]);
                            tjq('#autocomplete-issuing-{{$i}}').find('.item-after').text(countryreverse(information[picker.cols[0].value]["issuingcountry"]));
                            tjq('#autocomplete-issuing-{{$i}}').find('input[name="passportcountry"]').attr('value',information[picker.cols[0].value]["issuingcountry"]);
                        }
                    }
                ]
            });



            <?php }?>
            @endfor
            <?php } ?>





            function searchcountry(query,callingcode) {
                var results = [];
                for (var i = 0; i < countryCode.length; i++) {
                    var countryitem = countryCode[i];

                    if (countryitem.ISO_2_Code.toLowerCase().indexOf(query.toLowerCase()) >= 0 ||
                        countryitem.ISO_3_Code.toLowerCase().indexOf(query.toLowerCase()) >= 0 ||
                        countryitem.countryName.toLowerCase().indexOf(query.toLowerCase()) >= 0 ||
                        countryitem.countryName_CH.toLowerCase().indexOf(query.toLowerCase()) >= 0 ||
                        countryitem.countryName_PY.toLowerCase().indexOf(query.toLowerCase()) >= 0
                    ) {
                        if(callingcode!=undefined){
                            var tmpstr = "( "+countryitem.callingCode + " ) " + countryitem.countryName;
                            if (lang != undefined && lang == "Chinese") {
                                tmpstr = "( "+countryitem.callingCode + " ) " + countryitem.countryName_CH;
                            }
                            results.push(tmpstr);

                        }else{
                            var tmpstr = countryitem.ISO_2_Code + "-" + countryitem.countryName;
                            if (lang != undefined && lang == "Chinese") {
                                tmpstr = countryitem.ISO_2_Code + "-" + countryitem.countryName_CH;
                            }
                            results.push(tmpstr);
                        }

                    }
                }
                return results;
            }


            var callingcode=myApp.autocomplete({
                openIn: 'popup', //open in popup
                opener: $$('#autocomplete-calling-code'),//link that opens autocomplete
                backOnSelect: true, //go back after we select somethin,
                source: function (autocomplete, query, render) {
                    var results = [];
                    if (query.length === 0) {
                        render(results);
                        return;
                    }// Render items by passing array with result items
                    render(searchcountry(query,1));
                },
                onChange: function (autocomplete, value) {
                    // Add item text value to item-after

                    $$('#autocomplete-calling-code').find('.item-after').text(value[0]);
                    $$('#autocomplete-calling-code').css('background-color', '#FFF');
                    // Add item value to input value

                    //var tmpstr=value[0].split(' ');
                    //$$('#autocomplete-calling-code').find('input').val(tmpstr[1]);
                    $$('#autocomplete-calling-code').find('input').val(value[0]);
                },
                onOpen:function(p){
                    tjq('.autocomplete-popup').find('.close-popup').on('click',function(e){
                        e.stopPropagation();
                        callingcode.close();
                        return false;
                    })
                }
            });
            var country{{$i}} = myApp.autocomplete({
                openIn: 'popup', //open in popup
                opener: $$('#autocomplete-billcountry-{{$i}}'),//link that opens autocomplete
                backOnSelect: true, //go back after we select somethin,
                source: function (autocomplete, query, render) {
                    var results = [];
                    if (query.length === 0) {
                        render(results);
                        return;
                    }// Render items by passing array with result items
                    render(searchcountry(query));
                },
                onChange: function (autocomplete, value) {
                    // Add item text value to item-after

                    $$('#autocomplete-billcountry-{{$i}}').find('.item-after').text(value[0]);
                    $$('#autocomplete-billcountry-{{$i}}').css('background-color', '#FFF');
                    // Add item value to input value
                    $$('#autocomplete-billcountry-{{$i}}').find('input').val(value[0].substr(0, 2));
                },
                onOpen:function(p){
                    tjq('.autocomplete-popup').find('.close-popup').on('click',function(e){
                        e.stopPropagation();
                        country{{$i}}.close();
                        return false;
                    })
                }
            });

                    @for($i=1;$i<=$personcount;$i++)
            var nationality{{$i}} = myApp.autocomplete({
                    openIn: 'popup', //open in popup
                    opener: $$('#autocomplete-nationality-{{$i}}'),//link that opens autocomplete
                    backOnSelect: true, //go back after we select somethin,
                    source: function (autocomplete, query, render) {
                        var results = [];
                        if (query.length === 0) {
                            render(results);
                            return;
                        }// Render items by passing array with result items
                        render(searchcountry(query));
                    },
                    onChange: function (autocomplete, value) {
                        // Add item text value to item-after

                        $$('#autocomplete-nationality-{{$i}}').find('.item-after').text(value[0]);
                        $$('#autocomplete-nationality-{{$i}}').css('background-color', '#FFF');
                        // Add item value to input value
                        $$('#autocomplete-nationality-{{$i}}').find('input').val(value[0].substr(0, 2));
                    },
                    onOpen:function(p){
                        tjq('.autocomplete-popup').find('.close-popup').on('click',function(e){
                            e.stopPropagation();
                            nationality{{$i}}.close();
                            return false;
                        })
                    }
                });

            var issuecountry{{$i}} = myApp.autocomplete({
                openIn: 'popup', //open in popup
                opener: $$('#autocomplete-issuing-{{$i}}'),//link that opens autocomplete
                backOnSelect: true, //go back after we select somethin,
                source: function (autocomplete, query, render) {
                    var results = [];
                    if (query.length === 0) {
                        render(results);
                        return;
                    }// Render items by passing array with result items
                    render(searchcountry(query));
                },
                onChange: function (autocomplete, value) {
                    // Add item text value to item-after

                    $$('#autocomplete-issuing-{{$i}}').find('.item-after').text(value[0]);
                    $$('#autocomplete-issuing-{{$i}}').css('background-color', '#FFF');
                    // Add item value to input value
                    $$('#autocomplete-issuing-{{$i}}').find('input').val(value[0].substr(0, 2));
                },
                onOpen:function(p){
                    tjq('.autocomplete-popup').find('.close-popup').on('click',function(e){
                        e.stopPropagation();
                        issuecountry{{$i}}.close();
                        return false;
                    })
                }
            });



            var birthday{{$i}} = myApp.calendar({
                input: '#calendar-birthday-{{$i}}',
                dateFormat: 'yyyy-mm-dd',
                maxDate:new Date(),
                yearPicker: true,
                yearPickerTemplate:'<div class="picker-calendar-year-picker">'+

                '<select id="selectyear" style="    width: 80px;position: absolute;font-size:16px;left: 60%;top: 50%;-webkit-transform: translate(-50%, -50%);transform: translate(-50%, -50%);"></select>'+

                '</div>',
                rangePicker: false,
                onClose: function () {
                    $$('#calendar-one').parents('.item-content').css('background-color', '#FFF');
                },

                onOpen: function (p) {
                    var dt = new Date();
                    var maxyear= dt.getFullYear();
                    for(var i=0;i<120;i++){
                        var tmpyear=maxyear-i;
                        $$('<option>').val(tmpyear).text(tmpyear).appendTo('#selectyear');
                    }
                    var defaultyear=maxyear-18;
                    $$('#selectyear').val(maxyear-18);
                    birthday{{$i}}.setYearMonth(defaultyear);

                    $$('#selectyear').on('change',function(e){
                        birthday{{$i}}.setYearMonth($$('#selectyear').val());
                    });


                    $$('.picker-calendar').find('.toolbar-inner').append('<a href="#" id="calendardone" class="button active" style="width:60px;">Done</a>');
                    $$('#calendardone').on('click',function(){
                        birthday{{$i}}.close();
                    })
                },

                onMonthYearChangeStart: function (p) {
                    $$('.calendar-custom-toolbar .center').text(monthNames[p.currentMonth] + ', ' + p.currentYear);
                }
            });

            var passportvalidity{{$i}} = myApp.calendar({
                input: '#passportvalidity-{{$i}}',
                dateFormat: 'yyyy-mm-dd',
                minDate:new Date(),
                rangePicker: false,
                onClose: function () {
                    $$('#calendar-one').parents('.item-content').css('background-color', '#FFF');
                },
                onChange: function (value) {
                    // Add item text value to item-after

                    //$$('input[name="passportexpired"]').find('.item-after').text(value[0]);
                    //$$('#autocomplete-issuing-{{$i}}').css('background-color', '#FFF');
                    // Add item value to input value
                    $$('input[name="passportexpired"]').find('input').val(value);
                },
                onOpen: function (p) {
                    $$('.picker-calendar').find('.toolbar-inner').append('<a href="#" id="calendardone" class="button active" style="width:60px;">OK</a>');
                    $$('#calendardone').on('click',function(){
                        passportvalidity{{$i}}.close();
                    })
                },

                onMonthYearChangeStart: function (p) {
                    $$('.calendar-custom-toolbar .center').text(monthNames[p.currentMonth] + ', ' + p.currentYear);
                }
            });
            @endfor

            function showformerror(item){
                tjq(item).css('background','rgba(255, 45, 85, 0.11)');
            }




            function removeitem(items){
                tjq(items).css('background','#fff');
            }

            tjq('[id=showotheroptons]').on('click', function () {
                if (tjq('.optional:visible').length > 0) {
                    tjq('.optional:visible').hide();
                } else {
                    tjq('.optional').show();
                }

            })

            function parseURL(url) {
                var parser = document.createElement('a'),
                    searchObject = {},
                    queries, split, i;
                // Let the browser do the work
                parser.href = url;
                // Convert query string to object
                queries = parser.search.replace(/^\?/, '').split('&');
                for( i = 0; i < queries.length; i++ ) {
                    split = queries[i].split('=');
                    searchObject[split[0]] = split[1];
                }
                return {
                    protocol: parser.protocol,
                    host: parser.host,
                    hostname: parser.hostname,
                    port: parser.port,
                    pathname: parser.pathname,
                    search: parser.search,
                    searchObject: searchObject,
                    hash: parser.hash
                };
            }
            function checkform(obj){

                var passchecked = true;
                var target=tjq('form#passengers');
                var thisclass = this;
                if(obj!=undefined){
                    target=obj
                }
                tjq(target).find('input').each(function(idx,item){
                    var val = getValFromObj(tjq(this));
                    var showerrorobject = tjq(this);
                    var attr=tjq(item).attr('required');
                    //console.log(tjq(item).attr("popupid"));
                    if(attr){
                        if(tjq(item).val()=="" || tjq(item).val()==undefined){
                            if(tjq(item).attr("popupid")){
                                showformerror(tjq('#'+tjq(item).attr("popupid")));
                            }else{
                                showformerror(tjq(item));
                            }
                            passchecked=false;
                            var dataidx=tjq(item).parents("[dataidx]").attr("dataidx");
                            if(dataidx){
                                tjq('#passenger'+dataidx).find('i').html('close_round_fill');
                            }
                        }

                        if (tjq(this).attr('id') == 'email') {
                            console.log("Email:");
                            console.log(thisclass.isEmail(val));
                            if (!thisclass.isEmail(val)) {
                                passchecked = false;
                                thisclass.showRequired(showerrorobject);
                                showformerror(tjq(item));

                            } else {
                                thisclass.removeRequired(showerrorobject);
                                removeitem(tjq(item));
                            }
                        }

                        if (tjq(this).attr('id') == 'tel1') {
                            console.log("tel1:");
                            console.log(thisclass.checkTel(val));
                            if (!thisclass.checkTel(val)) {
                                passchecked = false;
                                thisclass.showRequired(showerrorobject);
                                showformerror(tjq(item));
                            } else {
                                thisclass.removeRequired(showerrorobject);
                                removeitem(tjq(item));
                            }
                        }



                        if (tjq(this).attr('id') == 'firstname')  {
                            console.log("firstname:");
                            console.log(thisclass.checkFirstName(val));
                            //console.log("hello");
                            if (!thisclass.checkFirstName(val)) {
                                passchecked = false;
                                thisclass.showRequired(showerrorobject);
                                showformerror(tjq(item));
                            } else {
                                thisclass.removeRequired(showerrorobject);
                                removeitem(tjq(item));
                            }
                        }



                        if (tjq(this).attr('id') == 'lastname') {
                            console.log("lastname");
                            console.log(thisclass.checkLastName(val));
                            if (!thisclass.checkLastName(val)) {
                                passchecked = false;
                                thisclass.showRequired(showerrorobject);
                                showformerror(tjq(item));
                            } else {
                                thisclass.removeRequired(showerrorobject);
                                removeitem(tjq(item));
                            }
                        }


                        if (tjq(this).attr('id') == 'contactfirstname') {
                            console.log("contactfirstname");
                            console.log(thisclass.checkFirstName(val));
                            if (!thisclass.checkFirstName(val)) {
                                passchecked = false;
                                thisclass.showRequired(showerrorobject);
                                showformerror(tjq(item));
                            } else {
                                thisclass.removeRequired(showerrorobject);
                                removeitem(tjq(item));
                            }
                        }

                        if (tjq(this).attr('id') == 'contactlastname') {
                            console.log("contactlastname");
                            console.log(thisclass.checkLastName(val));
                            if (!thisclass.checkLastName(val)) {
                                passchecked = false;
                                thisclass.showRequired(showerrorobject);
                                showformerror(tjq(item));
                            } else {
                                thisclass.removeRequired(showerrorobject);
                                removeitem(tjq(item));
                            }
                        }

                        /*  if((tjq(this).attr('id') == 'passport')!=""){
                             removeitem(tjq(item));
                          }

                          if((tjq(this).attr('id') == 'passportexpired')!=""){
                              removeitem(tjq(item));
                          }
                        */
                        /*
                          if (tjq(this).attr('id') == 'birthday') {
                              console.log("birthday");
                              console.log(val);
                              console.log(obj2);
                              val=val+"T00:00:00Z";
                              console.log(val);
                              var birthday=new Date(val);
                              var birth=birthday.getTime();
                              console.log(birth);

                          }
                       */



                    }
                    else{
                        if (tjq(this).attr('id') == 'middlename') {
                            console.log("middlename");
                            console.log(thisclass.checkMiddleName(val));
                            if (!thisclass.checkMiddleName(val)) {
                                passchecked = false;
                                thisclass.showRequired(showerrorobject);
                                showformerror(tjq(item));
                            } else {
                                thisclass.removeRequired(showerrorobject);
                                removeitem(tjq(item));
                            }
                        }
                    }
                    //if(passchecked){
                    //  removeitem(tjq(item));
                    //}
                });
                return passchecked;
            }


            function PassportFormError(obj){
                tjq(obj).find('input').each(function(idx,item){
                    if(tjq(item).attr("popupid")){
                        showformerror(tjq('#'+tjq(item).attr("popupid")));
                    }else{
                        showformerror(tjq(item));
                    }
                });
            }

            function PassportFormCorrect(obj){
                tjq(obj).find('input').each(function(idx,item){
                    if(tjq(item).attr("popupid")){
                        removeitem(tjq('#'+tjq(item).attr("popupid")));
                    }else{
                        removeitem(tjq(item));
                    }
                });
            }


            var rsToken = getCookie('rsToken');
            console.log("return rsToken");
            console.log(rsToken);
            if (rsToken) {
                FT_c_getPassengersFromSession(rsToken, getPassengersSuccess);
            }

            //delete cookie coupon by default
            var coupon=getCookie('coupon');
            if(coupon!=''){
                deleteCookie('coupon');
            }


            function getPassengersSuccess(resp) {
                console.log("show resp");
                console.log(resp);
                if (resp != 'error') {
                    for (var key in resp.d) {
                        if (tjq.inArray(key, [/*'contactindex',*/ 'contactfirstname', 'contactlastname', 'email', 'tel1', 'callingCode', 'address', 'city', 'province', 'country', 'postcode']) >= 0) {
                            if (key == 'country' ) {
                                tjq('div[dataidx=contact]').find('#autocomplete-billcountry-{{$i}}').find('.item-after').text(countryreverse(resp.d[key]));
                                tjq('div[dataidx=contact]').find('#autocomplete-billcountry-{{$i}}').find('input[name="country"]').attr('value',resp.d[key]);
                                //tjq('div[dataidx=contact]').find('#' + key).prev().attr('.item-after', resp.d[key]);
                                //tjq('div[dataidx=contact]').find('#' + key).typeahead('val', resp.d[key]);
                            } else if( key == 'callingCode') {
                                //console.log(searchcountry(resp.d[key]));
                                tjq('div[dataidx=contact]').find('#autocomplete-calling-code').find('.item-after').text(resp.d[key]);
                                //var tmpstr=resp.d[key].split(' ');
                                tjq('div[dataidx=contact]').find('#autocomplete-calling-code').find('input[name="callingCode"]').attr('value',resp.d[key]);
                                //tjq('div[dataidx=contact]').find('#autocomplete-calling-code').
                            }
                            else{
                                tjq('div[dataidx=contact]').find('#' + key).val(resp.d[key]);
                            }
                        } else {
                            var underlinePos = key.lastIndexOf('_');
                            if (underlinePos >= 0) {
                                var personIndex = key.substring(underlinePos + 1, key.length);
                                console.log(personIndex);
                                var inputVal = key.substring(0, underlinePos);

                                if (inputVal == 'gender') {
                                    if (personIndex > 1) {
                                        tjq('div[dataidx=' + personIndex + ']').find('select[name="gender'+ personIndex + '"]').filter('option[value="' + resp.d[key] + '"]');
                                    } else {
                                        tjq('div[dataidx=' + personIndex + ']').find('select[name="gender"]').val(resp.d[key]);

                                        //tjq('div[dataidx=' + personIndex + ']').find('select[name="gender"]').filter('option[value="' + resp.d[key] + '"]');
                                    }
                                } else if (inputVal == 'birthday' ) {

                                    tjq('div[dataidx=' + personIndex + ']').find('input[name='+inputVal+']').val(resp.d[key]);

                                }
                                else if(inputVal == 'passportexpired'){
                                    //tjq('div[dataidx=' + personIndex + ']').find('input[name="passportexpired"]').find('input').val(resp.d[key]);
                                    tjq('div[dataidx=' + personIndex + ']').find('input[name="passportexpired"]').val(resp.d[key]);
                                    //tjq('div[dataidx=' + personIndex + ']').find('input[name="passportexpired"]').datepicker('setDate', resp.d[key]);
                                    //var value=(2016,0,10);
                                    //(passportvalidity1).setValue(value);
                                }
                                else if (inputVal == 'passengertype' || inputVal == 'mealpreference' || inputVal == 'ssr') {
                                    tjq('div[dataidx=' + personIndex + ']').find('#' + inputVal).val(resp.d[key]).change();
                                }
                                else if(inputVal == 'nationality'){

                                    tjq('div[dataidx=' + personIndex + ']').find('#'+"autocomplete-nationality-"+personIndex).find('.item-after').text(countryreverse(resp.d[key]));
                                    tjq('div[dataidx=' + personIndex + ']').find('input[name='+inputVal+']').attr('value',resp.d[key]);
                                }else if(inputVal == 'passportcountry'){
                                    tjq('div[dataidx=' + personIndex + ']').find('#'+"autocomplete-issuing-"+personIndex).find('.item-after').text(countryreverse(resp.d[key]));
                                    tjq('div[dataidx=' + personIndex + ']').find('input[name='+inputVal+']').attr('value',resp.d[key]);
                                }
                                else if(inputVal=='pickname'){
                                    tjq('div[dataidx=' + personIndex + ']').find('#pickname-'+personIndex).val(resp.d[key]);
                                }
                                else {
                                    tjq('div[dataidx=' + personIndex + ']').find('#' + inputVal).val(resp.d[key]);
                                }
                            }
                        }
                    }
                }

            }

            tjq('#printbookingconfirmation, #printbookingreceipt').click(function (e) {
                e.preventDefault();

                if (tjq("#PNR").text().length > 0 && tjq("#orderid").text().length > 0) {
                    var orderid = tjq("#orderid").text();
                    FT_c_generateToken(orderid, function (rs) {
                        if (rs != undefined && rs[0] == 'success') {
                            window.open('<?php if($env == 'master') echo "https://crud.sinorama.ca";else echo "https://dev.crud.sinorama.ca" ; ?>/index.php/modules/pnrofqueue/pnrreceipt_customer/' + tjq("#PNR").text() + '/' + tjq("#orderid").text() + '/' + rs[1]);
                        } else {
                            alert("@lang('language.error2')");
                        }
                    });
                }
            });

            tjq('#emailbookingconfirmation').click(function (e) {
                e.preventDefault();

                if (tjq("#PNR").text().length > 0 && tjq("#orderid").text().length > 0) {
                    var para = {};
                    para['PNR'] = tjq("#PNR").text();
                    para['orderid'] = tjq("#orderid").text();
                    FT_c_emailBookingConfirmation(para, function (rs) {
                        if (rs != undefined && rs[0] == 'success') {
                            alert("@lang('language.error3')");
                        } else {
                            alert("@lang('language.error2')");
                        }
                    });
                }
            });

            function countryreverse(obj)
            {
                if(obj==""){
                    return "";
                }
                else {
                    var country = {};
                    var truecount;
                    country = searchcountry(obj);

                    function checkhaveornot(y) {
                        if (y.indexOf(obj) != -1)
                            return y;
                    }

                    truecount = country.find(checkhaveornot);
                    console.log(truecount);
                    return truecount;
                }

            }

            tjq('#submitorder').on('click',function(){
                //myApp.popup('.popup-policy');

                /*    if(tjq('#iamdoingbook').length>0) return;
                    request.clientIp = '207.96.163.34';
                    request.provider="sabre";
                    request.refer="mo.air.sinorama.ca";
    */
                var accum=true;
                    @for($i=1;$i<=$personcount;$i++){

                    var check=tjq('#passenger{{$i}}').attr("checkvalue");

                    accum=accum&&(check=="true");
                    console.log(accum);
                }
                @endfor
                console.log(accum);
                console.log(typeof accum);


                var checkformrs=accum&&(tjq('#passengercontact').attr("checkvalue")=="true");
                console.log(checkformrs);
                if(checkformrs){
                    //console.log(request);
                    myApp.popup('.popup-policy');           //restore after my following test

                }
                else alert("@lang('language.error5')");
            });


        });
        function setCookie(name,value){
            var exp = new Date();
            exp.setTime(exp.getTime() + 1*60*60*1000);//有效期1小时
            document.cookie = name + "="+ escape (value) + ";expires=" + exp.toGMTString();
        }

        function getCookie(name){
            var arr = document.cookie.match(new RegExp("(^| )"+name+"=([^;]*)(;|$)"));
            if(arr != null) return unescape(arr[2]); return null;
        }

        function deleteCookie(name){
            document.cookie = name+"=; expires=Thu, 01 Jan 1970 00:00:00 UTC;";
        }

        function ShowFormerror(item){
            tjq(item).css('background','rgba(255, 45, 85, 0.11)');
        }

        function RemoveItem(items){
            tjq(items).css('background','#fff');
        }
        function checkFirstName(name) {
            if (name == undefined || name == "" || name.length == 0) {
                return false;
            }

            if (!/^[ a-zA-Z]+$/g.test(name)) {
                return false;
            }

            return true;
        }

        function successBooking(msg){
            var _token = tjq("input[name=_token]").val();
            var orderid=msg.orderid;
            var PNR=msg.PNR;
            tjq.ajax({
                url: "/order",
                type: "POST",
                data: {orderid:orderid, PNR:PNR, _token: _token},
                datatype: "json",
                success: function (data) {

                },
                error: function (data) {
                },
                beforeSend: function () {
                },
                complete: function (data) {
                    //window.location.reload(true)
                    console.log(data);
                }
            });

            tjq('#progressbar').hide();
            console.log(msg);

            var formerfee=tjq('#totalamount').text();
            console.log(formerfee);
            if(msg.updateFee==true){

                tjq('#pricechange').css("display","inline");
                tjq('#totalamount').text(msg.totalfee);
                request.totalfee=msg.totalfee;
                var updatefee=tjq('#totalamount').text();
                console.log(updatefee);
                message="Attention!!!Your ticket price changed from "+formerfee+" to "+updatefee+".";
                alert(message);

                tjq('#formerfee').text(formerfee);
                tjq('#updatefee').text(updatefee);

                tjq('#basefare').text(msg.basefee);
                request.basefee=msg.basefee;

                tjq('#taxfee').text(msg.taxfee);
                request.taxfee=msg.taxfee;

                //tjq('#submitorder').after('<p>'+lang_array["Price_Change"]+'</p>');


            }


            if(parseInt(request.totalfee)<500)
                tjq('#couponForm').hide();




                <?php /*
            $empty=array(array());



            $response = dnode('getCreditcardPayment',$empty,$env);
            $credittype=$response[0][1];


            function dnode($fun,$para,$env){
            $dnode = new \DnodeSyncClient\Dnode();

            //$host="dev.search.air.sinorama.ca";
                if ($env=="dev"){
                    $svraddr="dev.search.air.sinorama.ca";
                }
                if ($env=="master"){
                    $svraddr="search.air.sinorama.ca";
                }

            if ($svraddr!=""){
            $connection = $dnode->connect($svraddr, 9998);
            $result=$connection->call($fun,$para);
            $connection->close();
            return $result;
            }else{
            echo "I do not know where am I";
            return;
            }
              }*/
                ?>


            var credittype='stripe';

            tjq('#submitorder').hide();
            if (msg!=undefined && msg[0] == "error") {
                //  tjq('#submitorder').after('<p>'+msg[1]+'</p>');

                if (msg.length >= 2) {
                    tjq('#submitorder').after('<p>'+bookingErrors[msg[1]]+'</p>');}
                tjq('#submitpayment').attr("disabled", false);


            }else{

                param="";
                card = new Skeuocard(tjq("#skeuocard"),{
                    acceptedCardProducts: ['visa', 'amex','mastercard']
                });
                if (msg['updateFee']) {
                    updateTicketFee(msg);
                }
                tjq('#orderid').text(msg.orderid);
                tjq('#PNR').text(msg.PNR);

                tjq('#divpayment').show();
                tjq('#submitpayment').show();

                if(credittype=="stripe")
                    tjq('#creditinfo').show();
                else if(credittype=="airline")
                    tjq('#creditinfo').hide();


                if(request.currency == 'AUD'||request.currency == 'EUR'||request.currency == 'NZD'||(request.currency == 'GBP')||(request.currency=='USD')){
                    tjq('#credittext').show();
                    tjq('#creditpic').hide();
                    tjq('#creditcontent').hide();
                    tjq('#creditinfo').hide();
                    tjq('#atos').css('display','inline');
                }

                if (tjq("#PNR").text().length > 0) {
                    var atos = tjq('#atosIframe');
                    if (request.currency == 'AUD' || request.currency == 'NZD'||request.currency == 'EUR'||request.currency == 'GBP'||request.currency=='USD') {
                        //if(request.coupon=="undefined")
                        //var atosSrc = '<?php if($env== 'master') echo 'https://mo.air.sinorama.ca/'; else echo 'https://dev.mo.air.sinorama.ca/'; ?>paypal?orderid=' + tjq("#orderid").text();
                        //else if(request.coupon!="undefined")
                        var atosSrc = '<?php if($env== 'master') echo 'https://mo.air.sinorama.ca/'; else echo 'https://dev.mo.air.sinorama.ca/'; ?>paypal?orderid=' + tjq("#orderid").text();
                    } //else if (request.currency == 'EUR') {
                      //  var atosSrc = '<?php // if($env== 'master') echo 'https://mo.air.sinorama.ca/'; else echo 'https://dev.mo.air.sinorama.ca/'; ?>ft-atos?PNR=' + tjq("#PNR").text();
                    // }
                    tjq(atos).attr('src', atosSrc);
                }

                if(isHolidays()) {
                    //tjq('#holidaysclosed_tips').show(100);
                    tjq('#submitpayment').hide();
                    tjq('#creditinformation').hide();
                    tjq('divpayment').hide();
                    alert("Dear custoemrs, thank you for visiting Sinorama air ticketing website. Our office will be closed on Dec. 25-26 and Jan. 1. During this period, we are unable to take any payment and issue ticket. Thanks for your understanding.");
                } /*else if (isClosed()) {
                    //tjq('#officeclosed_tips').show(100);
                    tjq('#submitpayment').hide();
                    tjq('#creditinformation').hide();
                    tjq('divpayment').hide();
                    alert("Dear cutomers, thank you for visiting Sinorama air ticketing website. Please be aware of our current working schedule is from Mon.-Fri. 10am to 6pm (EST). At any other time, you can still use our auto booking system, but we are unable to take any payment and issue ticket. Thank you for your understanding.");
                }*/


                tjq('#paymentsuccess').click(function () {
                    tjq('.payment-opacity-overlay').hide();

                    FT_c_flights_ticketing_atos(msg, function (results) {
                        if (results[0] == 'error') {
                            if (results[1] == 'noPayment') {
                                tjq('#paymentmsg').text('@lang("language.Orderfail")');
                            }
                        } else if (results[0] == 'success') {
                            tjq('#PNR').text(msg.PNR);
                            tjq('#adultcount').text(request["adultcount"]);
                            tjq('#childcount').text(request["childrencount"]);
                            tjq('#ifantcount').text(request["ifantcount"]);
                            tjq('#booking-content').show();
                            tjq('div#main').fadeOut();
                            tjq('#bookingsuccess').height(parseInt(tjq('#editInfo').height()) + 40);
                            tjq('#bookingsuccess').fadeIn();
                            tjq('#editInfo').find('a').hide();
                            tjq('.flight-booking-details').find('a').show();
                        }
                    });
                });

                tjq('#paymentfailed').click(function () {
                    tjq('.payment-opacity-overlay').hide();

                    FT_c_flights_ticketing_atos(msg, function (results) {
                        if (results[0] == 'error') {
                            if (results[1] == 'noPayment') {
                                tjq('#paymentmsg').text('@lang("language.Orderfail")');
                            }
                        } else if (results[0] == 'success') {
                            tjq('#PNR').text(msg.PNR);
                            tjq('#adultcount').text(request["adultcount"]);
                            tjq('#childcount').text(request["childrencount"]);
                            tjq('#ifantcount').text(request["ifantcount"]);
                            tjq('#booking-content').show();
                            tjq('div#main').fadeOut();
                            tjq('#bookingsuccess').height(parseInt(tjq('#editInfo').height()) + 40);
                            tjq('#bookingsuccess').fadeIn();
                            tjq('#editInfo').find('a').hide();
                            tjq('.flight-booking-details').find('a').show();
                        }
                    });
                });

                tjq('#buttonverify').click(function (e){
                    e.preventDefault();
                    vlidateCoupon(msg);
                    //vlidateCoupon();
                });

                tjq('#submitpayment').on('click',function(e){
                    var nametoken=true;
                    var cardnumber=true;
                    var expiry=true;
                    var cvc=true;
                    e.preventDefault();
                    if(tjq('#iamdongpaying').length>0) return;

                    if(card.isValid()){

                        tjq(this).html('<span id="iamdongpaying" class="preloader preloader-white"></span>');

                        console.log(param);
                        //       vlidateCoupon(function () {
                        // Disable the submit button to prevent repeated clicks

                        //msg['couoncode'] = tjq('#couoncode').val();
                        if(tjq('#couoncode').val()==param) {
                            msg['couoncode'] = param;
                            issueticket(credittype,msg);
                        }
                        else          //popup
                        {
                            if(tjq('#couoncode').val()==''){
                                msg['couoncode'] = param;
                                issueticket(credittype,msg);
                            }
                            else {
                                myApp.popup('.coupon-opacity-overlay');
                                tjq('#return').on('click', function () {
                                    myApp.closeModal('.coupon-opacity-overlay');
                                    tjq('#iamdongpaying').hide();
                                    tjq('#submitpayment').html('<a href="#" id="submitpayment" class="button button-fill" style="word-spacing: 20px;letter-spacing: 2px;" >Submit Payment </a>');
                                });
                                tjq('#continue').on('click', function () {
                                    myApp.closeModal('.coupon-opacity-overlay');
                                    msg['couoncode'] = param;
                                    issueticket(credittype, msg);
                                });
                            }
                        }

                        /*
                         Stripe.setPublishableKey('pk_live_B5SZw46G2GUaRIWOD1s1YjdW');

                         var cardNumber = tjq('[name="cc_number"]').val().trim();
                         console.log(cardNumber);
                         var expiredyear = tjq('[name="cc_exp_year"]').val().trim();
                         var expiredmonth = tjq('[name="cc_exp_month"]').val().trim();
                         expiredmonth=ModifyToTwoDigits(expiredmonth);

                         if (!Stripe.card.validateCardNumber(cardNumber)) {
                             console.log('card is wrong')
                             cardnumber=false;
                         }

                         var creditcardType = getCreditcardType(cardNumber);
                         if (creditcardType == '') {
                             alert('wrong');
                             return;
                         }


                         if (!Stripe.card.validateExpiry(expiredmonth, tjq('[name="cc_exp_year"]').val().trim())) {
                             console.log('date is wrong');
                             expiry=false;
                         }
                         if (!Stripe.card.validateCVC(tjq('[name="cc_cvc"]').val().trim())) {
                             console.log('card is wrong');
                             cvc=false;
                         }

                         if (!checkFirstName( tjq('[name="cc_name"]').val().trim())) {
                             console.log("first name is worng");
                             nametoken=false;
                         }


                         if(credittype=="airline"){
                         msg['booking']["card"] = {
                             'name': tjq('[name="cc_name"]').val().trim(),
                             'number': cardNumber,
                             'last4': cardNumber.substring(cardNumber.length - 4),
                             'exp_year': expiredyear,
                             'exp_month': expiredmonth,
                             'brand': creditcardType,
                             'seccode': tjq('[name="cc_cvc"]').val().trim(),
                              'address1': tjq('[name="address1"]').val().trim(),
                              'address2': tjq('[name="address2"]').val().trim(),
                              'city1': tjq('[name="city1"]').val().trim(),
                              'province2': tjq('[name="province2"]').val().trim(),
                              'country1': tjq('[name="country1"]').val().trim(),
                              'postcode1': tjq('[name="postcode1"]').val().trim(),

                         }
                         }
                         else if(credittype=="stripe"){
                             msg['booking']["card"] = {
                                 'name': tjq('[name="cc_name"]').val().trim(),
                                 'number': cardNumber,
                                 'last4': cardNumber.substring(cardNumber.length - 4),
                                 'exp_year': expiredyear,
                                 'exp_month': expiredmonth,
                                 'brand': creditcardType,
                                 'seccode': tjq('[name="cc_cvc"]').val().trim(),
                             };
                         }

                         msg['creditcardProviders'] = credittype;
                         console.log(msg);
/*
                         if ((msg['booking']['card']["address1"] == "")||(msg['booking']['card']["city1"] == "")||(msg['booking']['card']["province2"] == "")||(msg['booking']['card']["country1"] == "")||(msg['booking']['card']["postcode1"] == "")||(nametoken==false)||(cvc==false)||(expiry==false)||(cardnumber==false))
                         {
                             RemoveItem(tjq('[name="address1"]'));
                             //RemoveItem(tjq('[name="address2"]'));
                             RemoveItem(tjq('[name="city1"]'));
                             RemoveItem(tjq('[name="province2"]'));
                             RemoveItem(tjq('[name="country1"]'));
                             RemoveItem(tjq('[name="postcode1"]'));

                             if(msg['booking']['card']["address1"] == "")
                                 ShowFormerror(tjq('[name="address1"]'));
                             //if(msg['booking']['card']["address2"] == "")
                                 //ShowFormerror(tjq('[name="address2"]'));
                             if(msg['booking']['card']["city1"] == "")
                                 ShowFormerror(tjq('[name="city1"]'));
                             if(msg['booking']['card']["province2"] == "")
                                 ShowFormerror(tjq('[name="province2"]'));
                             if(msg['booking']['card']["country1"] == "")
                                 ShowFormerror(tjq('[name="country1"]'));
                             if(msg['booking']['card']["postcode1"] == "")
                                 ShowFormerror(tjq('[name="postcode1"]'));
                             if(nametoken==false) {
                                 alert("your name input is not correct, please enter again, keep mind that only letters are accepted");

                                 //tjq('div[class="cc-field filled valid"]').attr("class",'cc-field filled invalid');
                                 //console.log(tjq('[name="cc_name"]'));
                                 //console.log(tjq(".cc_name").parent());
                                 tjq('input[class="cc-name"]').parent().attr("class",'cc-field filled invalid');
                             }
                             tjq('#showreminder').css('display','');
                             tjq('#iamdongpaying').hide();
                             tjq('#submitpayment').html('<a href="#" id="submitpayment" class="button button-fill" style="word-spacing: 20px;letter-spacing: 2px;" >Submit Payment </a>');
                         }
                         else{    */                /*
                            FT_c_charge_ticket(msg, function (chargers) {
                                console.log(chargers);


                                if (chargers != undefined && chargers[0] != 'error') {
                                    tjq.get(cleanUrl, function (data) {
                                        //tjq('#orderid').text(msg.orderid);
                                        //tjq('#PNR').text(msg.PNR);
                                        myApp.popup('.popup-paysuccess');
                                    });


                                } else {
                                    //tjq(this).html();

                                    myApp.alert(stripeErrors[chargers[1]], "Unsuccessful");
                                    //console.log(tjq(this));

                                    tjq('#iamdongpaying').hide();
                                    tjq('#submitpayment').html('<a href="#" id="submitpayment" class="button button-fill" style="word-spacing: 20px;letter-spacing: 2px;" >Submit Payment </a>');
                                    tjq('#progressbar1').css("display", "none");
                                }
                            }, function (value) {
                                console.log(value);
                                tjq('#progressbar1').css("display", "inline");
                                tjq('#probar1').attr('data-progress', value);
                                var progress = tjq('#probar1').attr('data-progress');
                                var progressbar = tjq('#probar1');
                                myApp.setProgressbar(progressbar, progress);


                            });      */
                        //}
                        //return false;
                        //return;
                        //   });

                    }
                })


            }
        }
        function getAge(dateString,departdate) {
            var today = new Date(departdate);
            var birthDate = new Date(dateString);
            var age = today.getFullYear() - birthDate.getFullYear();
            var m = today.getMonth() - birthDate.getMonth();
            if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate()))
            {
                age--;
            }
            return age;
        }

        function updateTicketFee(para) {

        }


        function getCreditcardType(number) {
            // visa
            var re = new RegExp("^4");
            if (number.match(re) != null)
                return "VI";

            // Mastercard
            re = new RegExp("^5[1-5]");
            if (number.match(re) != null)
                return "CA";

            // AMEX
            re = new RegExp("^3[47]");
            if (number.match(re) != null)
                return "AX";

            return "";
        }

        function vlidateCoupon(f) {
            tjq('[id^=coupon_info]').hide();
            tjq('#coupon_extra_error').html("");
            tjq('#coupon_amt').html('');
            if (tjq('#couoncode').val().length > 0) {
                var obj={};
                obj.totalfee=f.totalfee;
                obj.currency=request.currency;
                obj.couponcode =tjq('#couoncode').val();
                FT_c_validateCoupon(obj, function(result) {

                    //console.log(result[1].currency);
                    //console.log(result[1].amt);

                    if (result[0] == 'success') {
                        tjq('#coupon_amt').html(result[1].currency + ' ' + result[1].amt);
                        tjq('#coupon_info_success').show();
                        console.log(request.currency);

                        if(request.currency=="CAD"){
                            var currentprice=f.totalfee;
                            var pricechange=parseFloat(currentprice)-parseFloat(result[1].amt);
                            tjq('#totalamount').text(pricechange);
                            request.coupon=result[1].amt;
                            param=tjq('#couoncode').val();
                            //return param;
                        }

                        else if(request.currency == 'AUD' || request.currency == 'NZD'||request.currency == 'EUR'||request.currency == 'GBP'||request.currency=='USD'){
                            var currentprice = f.totalfee;
                            var pricechange = parseFloat(currentprice) - parseFloat(result[1].amt);
                            console.log(pricechange);
                            tjq('#totalamount').text(pricechange);
                            request.coupon = result[1].amt;
                            var atos = tjq('#atosIframe');
                            var atosSrc = '<?php if($env== 'master') echo 'https://mo.air.sinorama.ca/'; else echo 'https://dev.mo.air.sinorama.ca/'; ?>paypal?orderid=' + tjq("#orderid").text()+'&amt='+ request.coupon;
                            tjq(atos).attr('src', atosSrc);
                            //tjq(atos).attr('src', 'https://mo.air.sinorama.test/order?couponcode=sino327tkt30&couponamt=30');


                            //put couponcode into session , only in case of success
                            //var _token = tjq("input[name=_token]").val();
                            var couponcode=tjq('#couoncode').val();
                            var couponamt=request.coupon;
                            //console.log(couoncode);
                            /*        ajax way, did not work
                           tjq.ajax({
                               url: "/order",
                               type: "get",
                               data: { couponcode:couponcode, couponamt:couponamt },
                               datatype: "json",
                               success: function (data) {

                               },
                               error: function (data) {
                               },
                               beforeSend: function () {
                               },
                               complete: function (data) {
                                   //window.location.reload(true)
                                   console.log(data);
                               }
                           });*/

                            /*     syncronous way, did not work either
                           var d = new Date();
                           var n = d.getTime();
                           var atos2 = tjq('#atosIframe2');
                           var atosSrc2 = '<?php if($env== 'master') echo 'https://mo.air.sinorama.ca/'; else echo 'https://dev.mo.air.sinorama.ca/'; ?>order?couponcode=' + couponcode +'&couponamt='+ couponamt+'&token='+n;
                            tjq(atos2).attr('src', atosSrc2);
                            */

                            //var value={};
                            //couponcode=couponcode;
                            //value.couponamt=couponamt;
                            //console.log(value);
                            setCookie('coupon',couponcode);


                        }


                        if (typeof f == "function") {
                            f();
                        }
                    } else {
                        tjq('#coupon_info').show();
                        if (typeof f == "function") {
                            //tjq('#submit-errors').html('<i class="fa fa-times"></i>' + "<?php// echo lang("您输入了优惠券，但验证不通过") ?>Verification failed");
                            alert("You typed the coupon, but it is not available. If you don't have a coupon, leave it blank.");
                            tjq('#submitpayment').show();
                            tjq('#iamdongpaying').hide();
                            tjq('#submitpayment').html('<a href="#" id="submitpayment" class="button button-fill" style="word-spacing: 20px;letter-spacing: 2px;" >Submit Payment </a>');
                        }
                        //var param="";
                        //return param;
                        tjq('#couoncode').val('');
                    }
                });
            } else {
                if (f == undefined) {
                    //tjq('#coupon_extra_error').html("--<?php// echo lang('请输入优惠券号') ?>");
                    alert("please enter coupon");
                    tjq('#coupon_info').show();
                } else {
                    f();
                }
            }

        }

        //设置cookie
        /*function setCookie(name,value){
            var exp = new Date();
            exp.setTime(exp.getTime() + 1*60*60*1000);//有效期1小时
            document.cookie = name + "="+ escape (value) + ";expires=" + exp.toGMTString();
        }*/

        document.getElementById("gotohomepage").onclick = function () {
            location.href = "<?php echo($env=='master' ? 'https://mo.air.sinorama.ca/':'https://dev.mo.air.sinorama.ca/'); ?>";
        };

        function issueticket(credittype,msg){

            Stripe.setPublishableKey('pk_live_B5SZw46G2GUaRIWOD1s1YjdW');

            var cardNumber = tjq('[name="cc_number"]').val().trim();
            console.log(cardNumber);
            var expiredyear = tjq('[name="cc_exp_year"]').val().trim();
            var expiredmonth = tjq('[name="cc_exp_month"]').val().trim();
            expiredmonth=ModifyToTwoDigits(expiredmonth);

            if (!Stripe.card.validateCardNumber(cardNumber)) {
                console.log('card is wrong')
                cardnumber=false;
            }

            var creditcardType = getCreditcardType(cardNumber);
            if (creditcardType == '') {
                alert('wrong');
                return;
            }


            if (!Stripe.card.validateExpiry(expiredmonth, tjq('[name="cc_exp_year"]').val().trim())) {
                console.log('date is wrong');
                expiry=false;
            }
            if (!Stripe.card.validateCVC(tjq('[name="cc_cvc"]').val().trim())) {
                console.log('card is wrong');
                cvc=false;
            }

            if (!checkFirstName( tjq('[name="cc_name"]').val().trim())) {
                console.log("first name is worng");
                nametoken=false;
            }


            if(credittype=="airline"){
                msg['booking']["card"] = {
                    'name': tjq('[name="cc_name"]').val().trim(),
                    'number': cardNumber,
                    'last4': cardNumber.substring(cardNumber.length - 4),
                    'exp_year': expiredyear,
                    'exp_month': expiredmonth,
                    'brand': creditcardType,
                    'seccode': tjq('[name="cc_cvc"]').val().trim(),
                    'address1': tjq('[name="address1"]').val().trim(),
                    'address2': tjq('[name="address2"]').val().trim(),
                    'city1': tjq('[name="city1"]').val().trim(),
                    'province2': tjq('[name="province2"]').val().trim(),
                    'country1': tjq('[name="country1"]').val().trim(),
                    'postcode1': tjq('[name="postcode1"]').val().trim(),

                }

                msg['creditcardProviders'] ='airlineSystem';
            }
            else if(credittype=="stripe"){
                msg['booking']["card"] = {
                    'name': tjq('[name="cc_name"]').val().trim(),
                    'number': cardNumber,
                    'last4': cardNumber.substring(cardNumber.length - 4),
                    'exp_year': expiredyear,
                    'exp_month': expiredmonth,
                    'brand': creditcardType,
                    'seccode': tjq('[name="cc_cvc"]').val().trim(),
                };

                msg['creditcardProviders'] ='stripe';
            }

            //msg['creditcardProviders'] = credittype;
            console.log(msg);
            /*
             if ((msg['booking']['card']["address1"] == "")||(msg['booking']['card']["city1"] == "")||(msg['booking']['card']["province2"] == "")||(msg['booking']['card']["country1"] == "")||(msg['booking']['card']["postcode1"] == "")||(nametoken==false)||(cvc==false)||(expiry==false)||(cardnumber==false))
             {
             RemoveItem(tjq('[name="address1"]'));
             //RemoveItem(tjq('[name="address2"]'));
             RemoveItem(tjq('[name="city1"]'));
             RemoveItem(tjq('[name="province2"]'));
             RemoveItem(tjq('[name="country1"]'));
             RemoveItem(tjq('[name="postcode1"]'));

             if(msg['booking']['card']["address1"] == "")
             ShowFormerror(tjq('[name="address1"]'));
             //if(msg['booking']['card']["address2"] == "")
             //ShowFormerror(tjq('[name="address2"]'));
             if(msg['booking']['card']["city1"] == "")
             ShowFormerror(tjq('[name="city1"]'));
             if(msg['booking']['card']["province2"] == "")
             ShowFormerror(tjq('[name="province2"]'));
             if(msg['booking']['card']["country1"] == "")
             ShowFormerror(tjq('[name="country1"]'));
             if(msg['booking']['card']["postcode1"] == "")
             ShowFormerror(tjq('[name="postcode1"]'));
             if(nametoken==false) {
             alert("your name input is not correct, please enter again, keep mind that only letters are accepted");

             //tjq('div[class="cc-field filled valid"]').attr("class",'cc-field filled invalid');
             //console.log(tjq('[name="cc_name"]'));
             //console.log(tjq(".cc_name").parent());
             tjq('input[class="cc-name"]').parent().attr("class",'cc-field filled invalid');
             }
             tjq('#showreminder').css('display','');
             tjq('#iamdongpaying').hide();
             tjq('#submitpayment').html('<a href="#" id="submitpayment" class="button button-fill" style="word-spacing: 20px;letter-spacing: 2px;" >Submit Payment </a>');
             }
             else{    */
            FT_c_charge_ticket(msg, function (chargers) {
                console.log(chargers);


                if (chargers != undefined && chargers[0] != 'error') {
                    tjq.get(cleanUrl, function (data) {
                        //tjq('#orderid').text(msg.orderid);
                        //tjq('#PNR').text(msg.PNR);
                        myApp.popup('.popup-paysuccess');
                    });


                } else {
                    //tjq(this).html();

                    myApp.alert(stripeErrors[chargers[1]], "Unsuccessful");
                    //console.log(tjq(this));

                    tjq('#iamdongpaying').hide();
                    tjq('#submitpayment').html('<a href="#" id="submitpayment" class="button button-fill" style="word-spacing: 20px;letter-spacing: 2px;" >Submit Payment </a>');
                    tjq('#progressbar1').css("display", "none");
                }
            }, function (value) {
                console.log(value);
                tjq('#progressbar1').css("display", "inline");
                tjq('#probar1').attr('data-progress', value);
                var progress = tjq('#probar1').attr('data-progress');
                var progressbar = tjq('#probar1');
                myApp.setProgressbar(progressbar, progress);


            });
        }

        function ModifyToTwoDigits(input) {
            if(parseInt(input)<10)
                return '0'+input;
            else return input;
        }

        tjq('#rvpaybycredit').on('click',function(){
            tjq('#rvpaybycredit').css("background","#90cdff");
            tjq('#rvpayinstore').css("background","#007aff");
            tjq('#rvpaybyetransfer').css("background","#007aff");
            tjq('#rvpaybycredit').css("color","#202020");
            tjq('#rvpayinstore').css("color","#ffffff");
            tjq('#rvpaybyetransfer').css("color","#ffffff");
            tjq('#rvpaymentinfocredit').show();
            tjq('#rvpaymentinfoinstore').hide();
            tjq('#rvpaymentinfoetransfer').hide();
        });

        tjq('#rvpayinstore').on('click',function(){
            tjq('#rvpaybycredit').css("background","#007aff");
            tjq('#rvpayinstore').css("background","#90cdff");
            tjq('#rvpaybyetransfer').css("background","#007aff");
            tjq('#rvpaybycredit').css("color","#ffffff");
            tjq('#rvpayinstore').css("color","#202020");
            tjq('#rvpaybyetransfer').css("color","#ffffff");
            tjq('#rvpaymentinfocredit').hide();
            tjq('#rvpaymentinfoinstore').show();
            tjq('#rvpaymentinfoetransfer').hide();
        });

        tjq('#rvpaybyetransfer').on('click',function(){
            tjq('#rvpaybycredit').css("background","#007aff");
            tjq('#rvpayinstore').css("background","#007aff");
            tjq('#rvpaybyetransfer').css("background","#90cdff");
            tjq('#rvpaybycredit').css("color","#ffffff");
            tjq('#rvpayinstore').css("color","#ffffff");
            tjq('#rvpaybyetransfer').css("color","#202020");
            tjq('#rvpaymentinfocredit').hide();
            tjq('#rvpaymentinfoinstore').hide();
            tjq('#rvpaymentinfoetransfer').show();
        });

        // $$('#qwertyui').on('click',function(){
        //var a='<?php echo(trim(config("app.env")) .", " . $env .", ". $host)?>';
        //myApp.closeModal('.popup-pricedetail');
        // });

        $$('.close-popup-pd').on('click',function(){
            myApp.closeModal('.popup-pricedetail');
        });



    </script>

@endsection