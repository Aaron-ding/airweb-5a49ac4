@extends('mobile.layouts.mainframe')
@section('extracss')
    <!-- Path to your custom app styles-->
    <link rel="stylesheet" href="dist/css/search-app.css">
@endsection

@section('content')

<?php $stopticket=true; ?>

<!-- Views -->
<div class="views">
    <!-- Your main view, should have "view-main" class -->
    <div class="view view-main">
        @include('mobile.layouts.parts.navbar')

    <!-- Pages container, because we use fixed-through navbar and toolbar, it has additional appropriate classes-->
        <div class="pages toolbar-through navbar-through">
            <!-- Page, "data-page" contains page name -->
            <div data-page="index" class="page search">
                <!-- Scrollable page content -->
                <div class="page-content hide-navbar-on-scroll" id="lstmainpagecontent">

                    <!-- Search condition review  -->
                    <div class="card" id="searchcard">
                        <div class="card-content" style="padding: 0 5px">
                            <div class="card-footer">
                                <h3 style="margin:0px;"><span id="headdeparture"></span>
                                    <img id="roundpic" src="../dist/img/search_list___departure/return_u321.png" style="margin-left:20px;margin-right: 20px;display:none;">
                                    <img id="singlepic"  src="../dist/img/baggages/u1391.png" style="margin-left:20px;margin-right: 20px;display:none;">
                                    <span id="headarrive"></span></h3>
                                <div class="f7-demo-icon" ><i class="f7-icons" style="color: #101481;">search_strong</i></div>
                            </div>
                            <div class="card-footer" >
                                <span id="daterange"></span><br>
                                <!--页面错位，调整
                                <span id="persondesc"></span>
                                -->
                                <span></span>
                            </div>
                        </div>
                    </div>


                    <div class="content-block" id="process" style="display: none;">
                        <p><span class="progressbar-infinite color-multi" style="height:9px;"></span></p>
                        <p style="color:#1D2088; font-size: 10px; padding-left:5px;">@lang('language.WaitMessage')</p>
                    </div>

                    <div class="demo-progressbar-inline" id="progressbar" style="padding-top: 10px;padding-left:14px;">
                        <div data-progress="0" class="progressbar" id="probar" style="height:9px;width:0%;background-color: #C1071E" ></div>
                        <p style="color:#1D2088; font-size: 12px;">@lang('language.WaitMessage')</p>
                    </div>

                    <div class="index-footer" style="padding-left:10px; padding-top: 10px; padding-right:10px;">
                    <span class="desc-grey">@lang('language.Claim')
                    </span>
                    </div>
                    <!-- departure text describe -->
                    <div class="content-block" style="    margin-top: 30px;margin-bottom: -15px;">
                        <div class="content-block-title" style="color:#1D2088;display:none" id="selecttitle">
                         <!--   <strong id="departdesc"></strong> -->
                            <strong id="departdesc"></strong>
                            <strong>, @lang('language.To') </strong>
                            <strong id="destdesc"></strong>
                         <!--   <span id="headarrive"></span>   -->

                        </div>
                        <div class="" style="padding-left:8px; font-size: 10px;">
                            <p>@lang('language.PriceMessage1') <span id="pricetriptype"></span>, @lang('language.PriceMessage2')</p>
                        </div>
                        <div class="content-block"></div>
                    </div>

                    <!-- filter function -->
                    <div style="padding-left: 15px">
                    <form action="#" id="extraoption" method="post">
                        <div class="selector" style="min-width:100px;padding-top: 5px;font-size: 12px">
                        <input type="checkbox" id="setnonstop" checked/><span style="margin-left: 5px;">@lang('language.NonStop')</span>
                        <input type="checkbox" id="setonestop" style="margin-left:20px;margin-right:0px;" checked/><span style="margin-left: 5px;">@lang('language.OneStop')</span>
                        <input type="checkbox" id="settwostops" style="margin-left:20px;margin-right:0px;" checked/><span style="margin-left: 5px;">@lang('language.TwoStop')</span>
                    </div>
                    </form>
                    </div>
                <!--<div class="card">
                            <div class="card-content">
                                <div class="card-footer">
                                  <ul class="sort-by-name" style="padding-left: 0px;list-style: none;">
                                   <li class="sort-by-name" id="setDepartdateLI">
                                       <div class="selector" style="min-width:70px;">
                                           <select class="full-width" id="setDepartdate">
                                              <option selected disabled>@lang('language.CDD')</option>
                                           </select>
                                       </div>
                                   </li>

                                   <li class="sort-by-name" id="setReturndateLI">
                                      <div class="selector" style="min-width:70px;">
                                          <select class="full-width" id="setReturndate">
                                             <option selected disabled>@lang('language.CRD')</option>
                                          </select>
                                      </div>
                                   </li>

                                    <li class="sort-by-name">
                                        <div class="selector" style="min-width:140px;">
                                            <select class="full-width userdefine" id="priority">
                                                <option value="Price">@lang('language.PL')</option>
                                                <option value="DirectFlights">@lang('language.SL')</option>
                                                <option value="Time">@lang('language.DE')</option>
                                            </select>
                                        </div>
                                    </li>
                                    <li class="sort-by-name">
                                        <div class="selector" style="min-width:140px;"><select class="full-width userdefine" id="maxstops"><option value="99">@lang('language.RN')</option><option value="1">@lang('language.M1S')</option><option value="2">@lang('language.M2S')</option><option value="3">@lang('language.M3S')</option><option value="4">@lang('language.M4S')</option><option value="5">@lang('language.M5S')</option></select></div>
                                    </li>

                                 </ul>
                              </div>
                           </div>

                    </div>-->


                @include('mobile.layouts.parts.internarydesc')

                    <!-- search resule -->

                    <div id="searchresultlist">
                    </div>
                </div>
                <div class="toolbar" style="font-size: 13px">
                    <div class="toolbar-inner">
                        <a href="#tab-1" id="tab-1" class="tab-link" style="padding-left: 15px">@lang('language.Tab1')</a>
                        <a href="#tab-2" id="tab-2" class="tab-link">@lang('language.Tab2')</a>

                        <a href="#tab-4" id="tab-4" class="tab-link">@lang('language.Tab4')</a>
                        <a href="#tab-3" id="tab-3" class="tab-link" style="padding-right: 15px">@lang('language.Tab3')</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@if($env == 'master')
    <form id="toreview" action="{{route("review")}}">
@else
    <form id="toreview" action="{{route("dev_review")}}">
@endif
    {{csrf_field()}}

    <input type="hidden" name="selectdata">
    <input type="hidden" name="returndata">
</form>

        <div class="popup popup-search"  style="background: #efeff4">
            <div  class="page">
                <div class="page-content">
                    <div class="navbar no-shadow ">
                        <div class="navbar-inner">
                            <div class="left sliding">
                                <a href="#" class="link close-popup"><i class="icon icon-back"></i><span class="">Close</span></a>
                            </div>
                        </div>
                    </div>
                    @include('mobile.layouts.parts.searcharea')
                </div>
            </div>
        </div>

        <div class="popup popup-return search"  style="background: #efeff4">
            <div class="navbar no-shadow " style="position: fixed;  top: 0;  left: 0;  z-index: 999;">
                <div class="navbar-inner">
                    <div class="left sliding">
                        <a href="#" class="link close-popup"><i class="icon icon-back"></i><span class="">@lang('language.Close')</span></a>
                    </div>
                    <div class="center sliding" style="left: -30px;">@lang('language.SelectReturn')</div>
                    <div class="right sliding"  style="width:39px;"></div>
                </div>
            </div>

            <div class="content-block desc-departure" style="margin-top: 80px;margin-bottom: -5px;">
                <div class="content-block-title" style="color:#1D2088;" id="selecttitle">
                    <strong id="departdesc"></strong>
                    <strong>, to </strong>
                    <strong id="destdesc"></strong>
                </div>

                <div class="content-block"></div>
            </div>


            <div class="content-block desc-arrive" style="margin-top: 50px;margin-bottom: -5px;">
                <div class="content-block-title" style="color:#1D2088;" id="selecttitle">
                    <strong id="returndepartdesc"></strong>
                    <strong>, @lang('language.To') </strong>
                    <strong id="returndestdesc"></strong>
                </div>

                <div class="content-block"></div>
            </div>
        </div>


        @include('mobile.layouts.parts.internarydetail')


            <input type="hidden" name="_token" id="csrf_token" value="{{csrf_token()}}">
@endsection


@section('extrascript')



    <script>
        console.log(getCityNameByCode("YUL"));
        console.log(getAirportNameByCodebak("YUL"));
        console.log(getAirportNameByCode("YUL"));
        console.log(getDisplayNamebak("YUL"));
        console.log(getDisplayName("YUL"));
        var itineraries= {};
        var request = new Request();
        var options=JSON.parse('@php echo html_entity_decode(old("querystring"), ENT_QUOTES); @endphp');
        console.log(options);
        if("@lang('language.Lang')"=="English") {
            tjq('#headdeparture').html(getCityNameByCode(options.origin));
            tjq('#headarrive').html(getCityNameByCode(options.destination));
            tjq('[id=destdesc]').html(getCityNameByCode(options.destination));
            tjq('[id=returndestdesc]').html(getCityNameByCode(options.origin));
        }
        else if("@lang('language.Lang')"=="Chinese"){
            tjq('#headdeparture').html(getCityNameByCodeChinese(options.origin));
            tjq('#headarrive').html(getCityNameByCodeChinese(options.destination));
            tjq('[id=destdesc]').html(getCityNameByCodeChinese(options.destination));
            tjq('[id=returndestdesc]').html(getCityNameByCodeChinese(options.origin));
        }
        else if("@lang('language.Lang')"=="French"){
            tjq('#headdeparture').html(getCityNameByCode(options.origin));
            tjq('#headarrive').html(getCityNameByCode(options.destination));
            tjq('[id=destdesc]').html(getCityNameByCode(options.destination));
            tjq('[id=returndestdesc]').html(getCityNameByCode(options.origin));
        }

        if(options.tripType=="roundtrip")
        {
            tjq('#roundpic').css("display", "");
            tjq('#pricetriptype').html("@lang('language.Round')");
        }
        if(options.tripType=="oneway")
        {
            tjq('#singlepic').css("display", "");
            tjq('#pricetriptype').html("@lang('language.One')");
        }
        var persondesc=options.adultcount+' Adult(s)';

        if(options.childrencount>0){
            persondesc+=","+options.childrencount+ "Child(ren)";
        }
        if(options.infantsseatcount>0){
            persondesc+=","+options.infantsseatcount+ "Infant(s)(seat)";
        }
        if(options.infantslapcount>0){
            persondesc+=","+options.infantslapcount+ "Infant(s)";
        }

        tjq('#persondesc').html(persondesc);


        var fromdate=Date.parse(options.departuredate).toString();
        tjq('[id=departdesc]').html(fromdate.substr(0,15));

        if(options.tripType=="roundtrip") {
            var todate = Date.parse(options.returndate).toString();
            tjq('[id=returndepartdesc]').html(todate.substr(0, 15));
            var datedesc = fromdate.substr(0, 15) + " ~ " + todate.substr(0, 15);
        }
        if(options.tripType=="oneway"){
            var datedesc = fromdate.substr(0, 15);
        }


        tjq('#daterange').html(datedesc);



        //filter function
        tjq('#setnonstop').click(function(){
            tjq(this).prop("checked");
            filterItinerary();
        });

        tjq('#setonestop').click(function(){
            tjq(this).prop("checked");
            filterItinerary();
        });

        tjq('#settwostops').click(function(){
            tjq(this).prop("checked");
            filterItinerary();
        });



        function filterItinerary() {
            var f = [false,false,false,false];
            var nonStop = tjq('#setnonstop').prop('checked');
            f[0] = nonStop;
            //console.log(f[0]);
            var oneStop = tjq('#setonestop').prop('checked');
            f[1] = oneStop;
            //console.log(f[2]);
            var twoStops = tjq('#settwostops').prop('checked');
            f[2] = twoStops;
            //console.log(twoStops);


            //var rows = tjq('#searchresultlist div');
            var rows = tjq('#searchresultlist #ft_item_template');

            rows.each(function (index, row) {
                //console.log(f[tjq(row).attr('data-directflights')]);
                if (tjq(row).attr('data-directflights') != undefined) {
                    if (f[tjq(row).attr('data-directflights')] != true) {
                        tjq(row).hide();
                    }
                    else tjq(row).show();
                }
                else tjq(row).hide();
            })
        }


        //sort function
        tjq('#tab-1').click(function(){
            sortItinerary2('Departure');
        });

        tjq('#tab-2').click(function(){
            sortItinerary2('Arrival');
        });

        tjq('#tab-3').click(function(){
            sortItinerary2('Price');
        });

        tjq('#tab-4').click(function(){
            sortItinerary2('Duration');
        });




        function FT_p_loadRoundtripAdvancedSearchlist(departuredata, returndata) {
            console.log("p_loeadround departuredata");
            console.log(departuredata);
            if (departuredata == "NotFound" || departuredata == undefined) {
                return;
            }

            var departureDateData = [];
            var returnDateData = [];
            var advancedSearchData = {};
            var lowestPrice = '';
            for (var key in departuredata) {
                if (departuredata.hasOwnProperty(key)) {
                    var departureValue = departuredata[key];
                    var totalfee = parseFloat(departureValue['TotalFare']);
                    var totalperson = parseInt(options.adultcount) + parseInt(options.childrencount) + parseInt(options.ifantcount);
                    var avgfee = Math.round(totalfee * 100 / totalperson) / 100;
                    if (!lowestPrice || lowestPrice > avgfee) {
                        lowestPrice = avgfee;
                    }
                }
            }
            for (var key in departuredata) {
                if (departuredata.hasOwnProperty(key)) {
                    var departureValue = departuredata[key];
                    var departureDateTime = departureValue['DepartureDateTime'];
                    var departureDate = departureDateTime.substring(0, 10);
                    var totalfee=parseFloat(departureValue['TotalFare']);
                    var totalperson = parseInt(options.adultcount) + parseInt(options.childrencount) + parseInt(options.ifantcount);
                    var avgfee = Math.round(totalfee * 100 / totalperson) / 100;
                    departureDateData.push(departureDate);

                    //var currencyPrix = request.getCurrencyShow(departureValue['TotalFareCurrency']);

                    var currencyPrix='$';
                    var returnList = departureValue["RETURN_LIST"];
                    for (var key_r in returnList) {
                        if (!isNaN(key_r)) {
                            var value_r = returnList[key_r];
                            var returnValue = returndata[value_r];
                            if (returnValue.hasOwnProperty('DepartureDateTime')) {
                                var returnDateTime = returnValue['DepartureDateTime'];
                                var returnDate = returnDateTime.substring(0, 10);
                                returnDateData.push(returnDate);

                                if (avgfee == lowestPrice) {
                                    var dateLowestPrice = true;
                                } else {
                                    var dateLowestPrice = false;
                                }

                                advancedSearchData[departureDate + '+' + returnDate] = {'price': currencyPrix + ' ' + avgfee, 'lowestPrice': dateLowestPrice};
                            }
                        }
                    }
                    //departureDateData = departureDateData.unique().sort();
                    //returnDateData = returnDateData.unique().sort();
                }
            }

            departureDateData = departureDateData.unique().sort();
            returnDateData = returnDateData.unique().sort();

        }

        function sortItinerary() {                 //this part will be used into sort function
            //console.log("flag");
            //var priority = tjq('#priority').val();
            var priority='Price';
            var rows = tjq('#searchresultlist #ft_item_template');
            var priorityMap = {
                'Price': ['DirectFlights', 'Time'],
                'DirectFlights': ['Price', 'Time'],
                'Time': ['Price', 'DirectFlights']
            };


            rows.sort(function(a, b) {
                var data = {
                    'Price': {},
                    'DirectFlights': {},
                    'Time': {}
                };

                data['Price']['a'] = parseFloat(tjq(a).attr('data-price'));
                data['Price']['b'] = parseFloat(tjq(b).attr('data-price'));
                data['DirectFlights']['a'] = parseInt(tjq(a).attr('data-directflights'));
                data['DirectFlights']['b'] = parseInt(tjq(b).attr('data-directflights'));
                data['Time']['a'] = parseInt(tjq(a).attr('data-time'));
                data['Time']['b'] = parseInt(tjq(b).attr('data-time'));

                if (data[priority]['a'] > data[priority]['b']) {
                    return 1;
                } else if (data[priority]['a'] < data[priority]['b']) {
                    return -1;
                }

                if (data[priorityMap[priority][0]]['a'] > data[priorityMap[priority][0]]['b']) {
                    return 1;
                } else if (data[priorityMap[priority][0]]['a'] < data[priorityMap[priority][0]]['b']) {
                    return -1;
                }

                if (data[priorityMap[priority][1]]['a'] > data[priorityMap[priority][1]]['b']) {
                    return 1;
                } else if (data[priorityMap[priority][1]]['a'] < data[priorityMap[priority][1]]['b']) {
                    return -1;
                }

                return 0;
            });
            //console.log("rows:");
            //console.log(rows);
            rows.each(function(index, row){
                rows.parent().append(row);
            });

        }

        function sortItinerary2(obj) {                 //this part will be used into sort function

            //console.log(obj);
            if(obj==undefined)
                var priority='Price';
            else
                var priority = obj;
            //var priority='Price';
            var rows = tjq('#searchresultlist #ft_item_template');
            var priorityMap = {
                'Price': ['Duration', 'Departure','Arrival'],
                'Duration': ['Price', 'Departure','Arrival'],
                'Departure': ['Price', 'Duration','Arrival'],
                'Arrival':['Price','Duration','Departure']
            };


            rows.sort(function(a, b) {
                var data = {
                    'Price': {},
                    'Duration': {},
                    'Departure': {},
                    'Arrival':{}
                };

                data['Price']['a'] = parseFloat(tjq(a).attr('data-price'));
                data['Price']['b'] = parseFloat(tjq(b).attr('data-price'));
                data['Duration']['a'] = parseInt(tjq(a).attr('data-duration'));
                data['Duration']['b'] = parseInt(tjq(b).attr('data-duration'));
                data['Departure']['a'] = parseInt(tjq(a).attr('data-time'));
                data['Departure']['b'] = parseInt(tjq(b).attr('data-time'));
                data['Arrival']['a'] = parseInt(tjq(a).attr('data-arrival'));
                data['Arrival']['b'] = parseInt(tjq(b).attr('data-arrival'));

                if (data[priority]['a'] > data[priority]['b']) {
                    return 1;
                } else if (data[priority]['a'] < data[priority]['b']) {
                    return -1;
                }

                if (data[priorityMap[priority][0]]['a'] > data[priorityMap[priority][0]]['b']) {
                    return 1;
                } else if (data[priorityMap[priority][0]]['a'] < data[priorityMap[priority][0]]['b']) {
                    return -1;
                }

                if (data[priorityMap[priority][1]]['a'] > data[priorityMap[priority][1]]['b']) {
                    return 1;
                } else if (data[priorityMap[priority][1]]['a'] < data[priorityMap[priority][1]]['b']) {
                    return -1;
                }

                if (data[priorityMap[priority][2]]['a'] > data[priorityMap[priority][2]]['b']) {
                    return 1;
                } else if (data[priorityMap[priority][2]]['a'] < data[priorityMap[priority][2]]['b']) {
                    return -1;
                }

                return 0;
            });
            //console.log("rows:");
            //console.log(rows);
            rows.each(function(index, row){
                rows.parent().append(row);
//                console.log(index+": "+tjq(row).attr('data-price')+" - "+tjq(row).attr('data-provider'));
            });

            changebackground(priority);
        }



        function changebackground(obj) {
            //console.log(obj);
            removebackground();
            var chose;
            if(obj=='Price')
                chose='#tab-3';
            else if(obj=='Duration')
                chose='#tab-4';
            else if(obj=='Departure')
                chose='#tab-1';
            else if(obj=='Arrival')
                chose='#tab-2';

            tjq(chose).css('background', '#1383ff');
            tjq(chose).css('color', 'white');
            tjq(chose).css('padding', '5px');
            tjq(chose).css('font-weight', '400');

        }

        function removebackground() {
            tjq('#tab-1').css('background', '');
            tjq('#tab-1').css('color', '');
            tjq('#tab-1').css('padding-left', '15px');
            tjq('#tab-1').css('font-weight', '');

            tjq('#tab-2').css('background', '');
            tjq('#tab-2').css('color', '');
            tjq('#tab-2').css('padding', '');
            tjq('#tab-2').css('font-weight', '');

            tjq('#tab-3').css('background', '');
            tjq('#tab-3').css('color', '');
            tjq('#tab-3').css('padding-right', '15px');
            tjq('#tab-3').css('font-weight', '');

            tjq('#tab-4').css('background', '');
            tjq('#tab-4').css('color', '');
            tjq('#tab-4').css('padding', '');
            tjq('#tab-4').css('font-weight', '');
        }








        function FT_p_error_report(errorMsg, errorLevel, showtype) {
            myApp.addNotification({
                title: 'Error',
                message: errorMsg
            });
        }

        var departuredatatemp=new Array();
        var returndatatemp=new Array();


        function FT_p_loadlist(departuredata, returndata) {


            request.currentStepIndex++;
            console.log(request.currentStepIndex);
            if (request.currentStepIndex == 1) {
                clearInterval(request.startSetIntervalId);
                var setIntervalId = setInterval(updateBar, 750);
                request.searchSetIntervalId = setIntervalId;
                console.log(request.searchSetIntervalId);

                var currentValue = parseInt(getNewProcessBarValue(tjq('#progressbar')));
                console.log(currentValue);
                var newValue = Math.floor((100 - currentValue) / 3) + currentValue;
                console.log(newValue);
                showNewProcessBar(tjq('#progressbar'), newValue);
                function updateBar() {
                    if (newValue >= 95) {
                        if (departuredata == "NotFound" || departuredata == undefined) {
                            FT_p_error_report('没有找到航班信息');
                        } else {
                            clearInterval(request.searchSetIntervalId);
                            showNewProcessBar(tjq('#progressbar'), 98);
                            setTimeout(function () {
                                //tjq('.search-button').removeAttr('disabled');
                                showNewProcessBar(tjq('#progressbar'), 100);
                            }, 1000);
                        }
                    } else {
                        newValue++;
                        showNewProcessBar(tjq('#progressbar'), newValue);
                    }
                }
            } else {
                clearInterval(request.searchSetIntervalId);
                tjq('#progressbar').hide();
            }

            if (departuredata == "NotFound" || returndata == undefined) {
                FT_p_error_report("@lang('language.FlightError')");
                return;
            }







            //var departuredatatemp=new Array();
            //var returndatatemp=new Array();

            //departuredatatemp=departuredatatemp.concat(departuredata);
            returndatatemp=returndatatemp.concat(returndata);

            //departuredata=departuredatatemp;
            //returndata=returndatatemp;

            console.log("p_load departuredata");
            console.log(departuredata);
            console.log(returndatatemp);






            tjq('#selecttitle').show();

            var listbox = tjq('#searchresultlist');
            var currprocess = 50;

            var step = Math.floor(50 / Object.keys(departuredata).length);
            //request.showProcessBar(currprocess);


                for (var key in departuredata) {
                    if (departuredata.hasOwnProperty(key)) {
                        var value = departuredata[key];

                        if (tjq("div[data-md5='" + value['MD5'] + "']").length <= 0) {            //if godata does not exist, add it directly to the result
                            console.log('addSearchResult|' + value['provider'] + '|' + value['MD5'] + '|' + value['TotalFare']);

                            var template = tjq('#ft_item_template').clone();

                            template = buildTemplate(template, value);


                            if(options.tripType=="roundtrip"){
                            var returncount=value["RETURN_LIST"].length;
                            //tjq(template).find('#selectbutton').html('Select（' + returncount + '）');
                            }
                            tjq(template).find('#selectbutton').on('click', function (event) {
                                showreturn(tjq(this), returndatatemp);
                                return false;
                                event.preventDefault();
                            });


                            tjq(listbox).append(template);

                            sortItinerary2();

                        }


                        else if (tjq("div[data-md5='" + value['MD5'] + "']").length > 0) {         //if the godata existed already
                            if (options.tripType == "roundtrip") {
                                var addReturnResultsOnly = false;

                                tjq("div[data-md5='" + value['MD5'] + "']").each(function () {
                                    var articleObject = tjq(this);
                                    var price = parseFloat(articleObject.attr('data-price'));
                                    var return_list = articleObject.attr('return_list');
                                    var return_lists = return_list.split(',');
                                    if (price > parseFloat(value['TotalFare'])) {                      //if new popup result is cheaper

                                        //console.log("price is cheeper:");
                                        //console.log(return_lists);

                                        tjq.each(return_lists, function (key_r, value_r) {
                                            //console.log("here is the return list");
                                            var returnMD5 = value_r;
                                            var returnMD5Index = value["RETURN_LIST"].indexOf(returnMD5);
                                            if (returnMD5Index != -1) {       //if new result has same returndata, keep new result and remove entire old result
                                                console.log('returnResultsHided|' + articleObject.attr('data-MD5') + '|' + returnMD5 + '|' + articleObject.attr('data-price'));
                                                return_lists.splice(key_r, 1);
                                            }
                                        });


                                        var returncount = return_lists.length;
                                        //articleObject.find('.selectbutton').html('Select（' + returncount + '）');
                                        articleObject.attr('return_list', return_lists);
                                        if (return_lists.length <= 0) {
                                            articleObject.css("display", "none");
                                        }
                                    } else if (price <= parseFloat(value['TotalFare'])) {            //if new popup price is more expensive or equal
                                        //if(price<parseFloat(value['TotalFare'])){                        //if new result has a more expensive price, show new result and remove the same returndata
                                        //  console.log("new is more expensive");
                                        //}
                                        tjq.each(return_lists, function (key_r, value_r) {
                                            var returnMD5 = value_r;
                                            var returnMD5Index = value["RETURN_LIST"].indexOf(returnMD5);
                                            if (returnMD5Index != -1) {                               //if they have same returndata
                                                console.log('spliceReturnResults|' + value['provider'] + '|' + value['MD5'] + '|' + value['TotalFare']);
                                                value["RETURN_LIST"].splice(returnMD5Index, 1);
                                            }
                                        });

                                        if (price == parseFloat(value['TotalFare'])) {             //if price is same
                                            addReturnResultsOnly = true;
                                            console.log('addReturnResults|' + value['provider'] + '|' + value['MD5'] + '|' + value['TotalFare']);
                                            console.log(value["RETURN_LIST"]);
                                            if (value["RETURN_LIST"].length > 0) {
                                                console.log(111);
                                                console.log(return_lists);
                                                var returncount = return_lists.length + value["RETURN_LIST"].length;
                                                //articleObject.find('#selectbutton').html('Select（' + returncount + '）');
                                                addReturnResults(articleObject, value["RETURN_LIST"]);


                                            }
                                        }
                                    }

                                });

                                if (!addReturnResultsOnly && value["RETURN_LIST"].length > 0) {
                                    console.log('addSearchResult111new|' + value['provider'] + '|' + value['MD5'] + '|' + value['TotalFare']);
                                    var template = tjq('#ft_item_template').clone();

                                    template = buildTemplate(template, value);



                                    var returncount=value["RETURN_LIST"].length;
                                    //tjq(template).find('#selectbutton').html('Select（' + returncount + '）');
                                    tjq(template).find('#selectbutton').on('click', function (event) {
                                        showreturn(tjq(this), returndatatemp);
                                        return false;
                                        event.preventDefault();
                                    });


                                    tjq(listbox).append(template);
                                    sortItinerary2();
                                }
                            } else if (options.tripType == "oneway") {
                                var price = parseFloat(tjq("div[data-md5='" + value['MD5'] + "']").attr('data-price'));
                                if (price > parseFloat(value['TotalFare'])) {
                                    console.log('replaceSearchResult|' + value['provider'] + '|' + value['MD5'] + '|' + value['TotalFare']);
                                    tjq("div[data-md5='" + value['MD5'] + "']").css("display", "none");
                                    sortItinerary2();
                                }
                            }
                        }


                    }

                }


            tjq('#process').hide();
            tjq('#search-button').hide();
        }



        function addReturnResults(obj1,obj2){
            var return_list=obj1.attr('return_list');
            var return_lists=return_list.split(',');
            return_lists.push(obj2);
            obj1.attr('return_list',return_lists);
        }

        var rawreturntempalte=tjq('.popup-return').clone();

        function showreturn(obj,returndatatemp){
            //console.log(obj);
            var returntempalte=tjq(rawreturntempalte).clone();



            var departureview=tjq(obj).parents('div#ft_item_template').clone(true, true);
            tjq(departureview).find('#selectbutton').hide();
            tjq(returntempalte).find('.desc-departure').after(tjq(departureview));



            if (options.tripType == "roundtrip" || options.tripType == "multiple") {


                var data=request.getDconcat(tjq(obj).parents('div#ft_item_template').attr('data'));
                var detail=internary=data[0];
                if (typeof internary=="string"){
                    detail=JSON.parse(internary);
                }
                //console.log(data);
                var returncount = 0;
                //console.log(departureview);
                var return_list=tjq(obj).parents('div#ft_item_template').attr('return_list');

                //console.log(return_list);
                var return_lists=return_list.split(',');
                //console.log(return_lists);
                tjq.each(return_lists, function (key_r, value_r) {
                    var template = tjq('#ft_item_template').clone();
                    template.find('#selectbutton').html('@lang('language.Select')');

                for(var keyindex in returndatatemp) {
                    var returndata=returndatatemp[keyindex];
                    //console.log(returndata[value_r]);
                    if (returndata[value_r] != undefined) {
                        if(returndata[value_r].provider==detail.provider){
                         //console.log(JSON.stringify(returndata[value_r]));
                         //console.log("hello");
                            var returnthings=JSON.stringify(returndata[value_r]);
                            template = buildTemplate(template, returndata[value_r]);

                            tjq(template).find('#pricearea').html('');

                            tjq(template).find('#selectbutton').on('click', function (event) {
  //  temporary stop user to book ticket online!!!!
   @if($stopticket)
       myApp.alert("<div style=\"text-align:left;\">@lang('language.techerror01')<br><br>@lang('language.techerror02')<br>@lang('language.techerror03')</div>");
   @else
                                event.preventDefault();
                                tjq('#toreview').find('input[name="selectdata"]').val(internary);
                                //tjq('#toreview').find('input[name="returndata"]').val(JSON.stringify(returndata[value_r]));
                                tjq('#toreview').find('input[name="returndata"]').val(returnthings);
                                //console.log(JSON.stringify(returndata[value_r]));
                                console.log(tjq('#toreview').find('input[name="selectdata"]').val());
                                console.log(tjq('#toreview').find('input[name="returndata"]').val());
                                tjq('#toreview').submit();
   @endif
                                return false;
                            });

                            tjq(returntempalte).append(tjq(template));
                            //sortItinerary();
                        }
                    }
                }

                });
                tjq('.popup-return').replaceWith(tjq(returntempalte));
                myApp.popup('.popup-return');

            }
            //tjq('.popup-return').replaceWith(tjq(returntempalte));
            //myApp.popup('.popup-return');
            if(options.tripType=="oneway"){
   //  temporary stop user to book ticket online!!!!
   @if($stopticket)
   myApp.alert("<div style=\"text-align:left;\">@lang('language.techerror01')<br><br>@lang('language.techerror02')<br>@lang('language.techerror03')</div>");
   @else
                var data=request.getDconcat(tjq(obj).parents('div#ft_item_template').attr('data'));
                var detail=internary=data[0];
                if (typeof internary=="string"){
                    detail=JSON.parse(internary);
                }
                console.log(internary);
                tjq('#toreview').find('input[name="selectdata"]').val(internary);
                //tjq('#toreview').find('input[name="returndata"]').val(JSON.stringify(returndata[value_r]));
                tjq('#toreview').submit();
   @endif
            }
        }


        function showNewProcessBar(object, value) {
            // console.log(value);
            var progressBar = object.children('div');
            if (value == 1) {
                //tjq(progressBar).attr('aria-valuenow', value).css('width', value + '%');
                //tjq('#probar1').attr('data-progress', value);
                var progress = progressBar.attr('data-progress');
                //var progressbar = tjq('#probar1');
                myApp.setProgressbar(progressBar, progress);
                object.show();
            }
            if (value == 100) {
                //tjq(progressBar).attr('aria-valuenow', value).css('width', value + '%');
                //tjq('#probar1').attr('data-progress', value);
                var progress = progressBar.attr('data-progress');
                //var progressbar = tjq('#probar1');
                myApp.setProgressbar(progressBar, progress);
                object.hide();
            }

            //if (object.is(':visible')) {
            //var currentValue = tjq(progressBar).attr('data-progress');
            //if (value > currentValue) {
            tjq(progressBar).attr('data-progress', value);
            tjq(progressBar).css('width', value + '%');
            // }
            //}
        }

        function getNewProcessBarValue(object) {
            var progressBar = object.children('div');
            var currentValue = tjq(progressBar).attr('data-progress');

            return currentValue;
        }


        tjq(document).ready(function() {
            $$('#searchcard').on('click',function(){
                myApp.popup('.popup-search');
            });

            var barValue = 1;
            showNewProcessBar(tjq('#progressbar'), barValue);
            var setIntervalId = setInterval(updateBar, 750);
            request.startSetIntervalId = setIntervalId;
            function updateBar() {
                if (barValue >= 95) {
                    FT_p_error_report("<?php echo "没有找到航班信息"  ?>");
                } else {
                    barValue++;
                    showNewProcessBar(tjq('#progressbar'), barValue);
                }
            }
            console.log('options');
            console.log(options);
            //FT_c_doAdvancedSearch(options, FT_p_loadRoundtripAdvancedSearchlist, FT_p_error_report);
            FT_c_doSearch(options, FT_p_loadlist, FT_p_error_report);
        });



    </script>

@endsection