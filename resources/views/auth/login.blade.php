@extends('layouts.app')
@section('extracss')
@endsection
@section('content')
    <section class="login p-fixed d-flex text-center bg-primary common-img-bg">
        <!-- Container-fluid starts -->
        <div class="container-fluid">
            <div class="row">

                <div class="col-sm-12">
                    <div class="login-card card-block">
                        <form class="md-float-material" method="POST" action="{{ route('login') }}">
                            {{ csrf_field() }}
                            <div class="text-center">
                                <img src="/assets/images/logo.png">
                            </div>
                            <h3 class="text-center txt-primary">
                                Sign In to your account
                            </h3>
                            <div class="md-input-wrapper
                                @if ($errors->has('email'))
                                        md-input-warning
                                @endif
                            ">
                                <input id="email" type="email" class="md-form-control
                               @if (null!=old('email'))
                                        md-valid
                                @endif
                                " name="email" value="{{ old('email') }}" required autofocus>
                                <label>Email</label>

                            </div>
                            <div class="md-input-wrapper
                                    @if ($errors->has('password'))
                                            md-input-warning
                                    @endif
                                ">
                                <input id="password" type="password" class="md-form-control" name="password" required>
                                <label>Password</label>
                            </div>
                            <div class="row">
                                <div class="col-sm-6 col-xs-12">
                                    <div class="rkmd-checkbox checkbox-rotate checkbox-ripple m-b-25">
                                        <label class="input-checkbox checkbox-primary">
                                            <input type="checkbox" id="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}>
                                            <span class="checkbox"></span>
                                        </label>
                                        <div class="captions">Remember Me</div>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-xs-12 text-right forgot-phone">
                                    <a class="f-w-600" href="{{ route('password.request') }}">
                                        Forgot Your Password?
                                    </a>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-10 offset-xs-1">
                                    <button type="submit" class="btn btn-primary btn-md btn-block waves-effect text-center">LOGIN</button>
                                </div>
                                <div class="col-xs-12 sign-in-with"><h6 class="text">Sign in With</h6></div>
                                <div class="col-xs-12">
                                    <div class="social-login text-center">
                                        <button type="button" class="btn btn-facebook waves-effect waves-light">
                                            <span><i class="icofont icofont-social-facebook  txt-white f-18"></i></span><span>facebook</span>
                                        </button>

                                        <button type="button" class="btn btn-twitter waves-effect waves-light"><span><i class="icofont icofont-social-twitter  txt-white f-18"></i></span><span>twitter</span>
                                        </button>

                                        <button type="button" class="btn btn-google-plus waves-effect waves-light">
                                            <i class="icofont icofont-social-google-plus txt-white f-18"></i><span>google+</span>
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <!-- <div class="card-footer"> -->
                            <div class="row">
                                <div class="col-sm-12 col-xs-12 text-center">
                                    <span class="text-muted">Don't have an account?</span>
                                    <a href="{{ route('register') }}" class="f-w-600 p-l-5">Sign up Now</a>
                                </div>
                            </div>
                            <!-- </div> -->
                        </form>
                        <!-- end of form -->
                    </div>
                    <!-- end of login-card -->
                </div>
                <!-- end of col-sm-12 -->
            </div>
            <!-- end of row -->
        </div>
        <!-- end of container-fluid -->
    </section>
@endsection

@section('extrascript')

@endsection
<script>
    console.log("hello");
</script>






