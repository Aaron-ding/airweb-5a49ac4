@extends('layouts.app')
@section('extracss')
@endsection
@section('content')
    <section class="login common-img-bg">
        <!-- Container-fluid starts -->
        <div class="container-fluid">
            <div class="row">

                <div class="col-sm-12">
                    <div class="login-card card-block bg-white">
                        <form class="md-float-material" method="POST" action="{{ route('password.request') }}">
                            {{ csrf_field() }}
                            <div class="text-center">
                                <img src="/assets/images/logo.png">
                            </div>
                            <h3 class="txt-primary text-center">Reset password</h3>
                            <div class="form-group-main">


                                <div class="md-input-wrapper">

                                    <input id="email" type="email" class="md-form-control" name="email" value="{{ old('email') }}" required>
                                    <label>Email Address</label>
                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                    @endif
                                </div>



                                <div class="md-input-wrapper">
                                    <input id="oldpassword" type="password" class="md-form-control" name="oldpassword" required>
                                    <label>Old Password</label>
                                    @if ($errors->has('oldpassword'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('oldpassword') }}</strong>
                                    </span>
                                    @endif
                                </div>

                                <div class="md-input-wrapper">
                                    <input id="newpassword" type="password" class="md-form-control" name="newpassword" required>
                                    <label>New Password</label>
                                </div>
                            </div>

                            <div class="col-sm-10 col-xs-6 offset-sm-1 col-xs-offset-0">
                                <button type="submit" class="btn btn-primary btn-md btn-block waves-effect waves-light m-b-25"> Reset Password</button>
                            </div>

                        </form>
                        <!-- end of form -->
                    </div>
                    <!-- end of login-card -->
                </div>
                <!-- end of col-sm-12 -->
            </div>
            <!-- end of row -->
        </div>
        <!-- end of container-fluid -->
    </section>
@endsection

@section('extrascript')
@endsection


