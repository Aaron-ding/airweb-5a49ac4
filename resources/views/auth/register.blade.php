@extends('layouts.app')
@section('extracss')
@endsection
@section('content')
    <section class="login common-img-bg">
        <!-- Container-fluid starts -->
        <div class="container-fluid">
            <div class="row">

                <div class="col-sm-12">
                    <div class="login-card card-block bg-white">
                        <form class="md-float-material" method="POST" action="{{ route('register') }}">
                            {{ csrf_field() }}
                            <div class="text-center">
                                <img src="/assets/images/logo.png">
                            </div>
                            <h3 class="txt-primary text-center">Create an account </h3>
                            <div class="form-group-main">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="md-input-wrapper
                                            @if ($errors->has('firstname'))
                                                md-input-warning
                                            @endif
                                        ">
                                            <input id="firstname" type="text" class="md-form-control
                                                @if (null!=old('firstname'))
                                                    md-valid
                                                @endif
                                            " name="firstname" value="{{ old('firstname') }}" required autofocus>
                                            <label>First Name</label>

                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="md-input-wrapper
                                            @if ($errors->has('lastname'))
                                                md-input-warning
                                            @endif
                                        ">

                                            <input id="lastname" type="text" class="md-form-control
                                                @if (null!=old('lastname'))
                                                    md-valid
                                                @endif
                                            " name="lastname" value="{{ old('lastname') }}" required>
                                            <label>Last Name</label>
                                            @if ($errors->has('lastname'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('lastname') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>


                                <div class="md-input-wrapper
                                    @if ($errors->has('email'))
                                        md-input-warning
                                    @endif
                                ">

                                    <input id="email" type="email" class="md-form-control
                                        @if (null!=old('email'))
                                            md-valid
                                        @endif
                                    " name="email" value="{{ old('email') }}" required>
                                    <label>Email Address</label>

                                </div>



                                <div class="md-input-wrapper
                                            @if ($errors->has('password'))
                                        md-input-warning
                                    @endif
                                ">
                                    <input id="password" type="password" class="md-form-control" name="password" required>
                                    <label>Password</label>
                                </div>


                                <div class="md-input-wrapper">
                                    <input id="password-confirm" type="password" class="md-form-control" name="password_confirmation" required>
                                    <label>Confirm Password</label>
                                </div>
                            </div>



                            <div class="col-sm-10 col-xs-6 offset-sm-1 col-xs-offset-0">
                                <button type="submit" class="btn btn-primary btn-md btn-block waves-effect waves-light m-b-25"> Sign up </button>
                            </div>
                            <div class="">
                                <div class="social-login text-center">
                                    <button type="button" class="btn btn-facebook waves-effect waves-light">
                                        <span><i class="icofont icofont-social-facebook  txt-white f-18"></i></span><span>facebook</span>
                                    </button>

                                    <button type="button" class="btn btn-twitter waves-effect waves-light"><span><i class="icofont icofont-social-twitter  txt-white f-18"></i></span><span>twitter</span>
                                    </button>

                                    <button type="button" class="btn btn-google-plus waves-effect waves-light">
                                        <i class="icofont icofont-social-google-plus txt-white f-18"></i><span>google+</span>
                                    </button>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-xs-12 text-center">
                                    <span class="text-muted">Already have an account?</span>
                                    <a href="{{ route('login') }}" class="f-w-600 p-l-5"> Sign In Here</a>

                                </div>
                            </div>
                        </form>
                        <!-- end of form -->
                    </div>
                    <!-- end of login-card -->
                </div>
                <!-- end of col-sm-12 -->
            </div>
            <!-- end of row -->
        </div>
        <!-- end of container-fluid -->
    </section>
@endsection

@section('extrascript')
@endsection

