@extends('layouts.mainframe')

@section('extracss')
    <!-- Date Picker css -->
    <link rel="stylesheet"
          href="/bower_components/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css"/>

    <!-- Bootstrap Date-Picker css -->
    <link rel="stylesheet" href="/assets/plugins/bootstrap-datepicker/css/bootstrap-datetimepicker.css"/>
    <link rel="stylesheet" type="text/css" href="/bower_components/bootstrap-daterangepicker/daterangepicker.css"/>
    <link rel="stylesheet" href="/bower_components/jquery-confirm2/css/jquery-confirm.css">


@endsection

@section('content')

    <div class="content-wrapper">
        <!-- Container-fluid starts -->
        <!-- Main content starts -->
        <div class="container-fluid">
            <div class="row" style="margin-bottom:20px;">
                <div class="main-header">
                    <ol class="breadcrumb breadcrumb-title breadcrumb-arrow">
                        <li class="breadcrumb-item"><a href="/home"><i class="icofont icofont-home"></i></a>
                        </li>
                        <li class="breadcrumb-item"><a href="#!">Agent Menu</a>
                        </li>

                        <li class="breadcrumb-item"><a href="#!">Agent application list</a>
                        </li>

                    </ol>
                </div>
            </div>


            <nav class="navbar navbar-light bg-faded m-b-30 p-10">
                <h3>Total : {{count($result)}} application(s)</h3>
            </nav>

            <div class="row">
                @foreach($result as $item)
                    <div class="col-xl-4 col-md-12">
                        <div class="card b-l-primary">
                            <div class="card-header">
                                <a href="#!" class="card-title">#{{$item->id}}. {{$item->email}} </a>
                                <span class="label label-default f-right m-t-5"> {{$item->created_at}} </span>
                            </div>
                            <div class="card-block">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <h6>License</h6>
                                    </div>
                                    <div class="col-sm-6">
                                        {{$item->license}}
                                    </div>
                                    <!-- end of col-sm-8 -->
                                </div>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <h6>Company</h6>
                                    </div>
                                    <div class="col-sm-6">
                                        {{$item->company}}
                                    </div>
                                    <!-- end of col-sm-8 -->
                                </div>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <h6>Company phone</h6>
                                    </div>
                                    <div class="col-sm-6">
                                        {{$item->companyphone}}
                                    </div>
                                    <!-- end of col-sm-8 -->
                                </div>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <h6>Company address</h6>
                                    </div>
                                    <div class="col-sm-6">
                                        {{$item->companyaddress}}
                                    </div>
                                    <!-- end of col-sm-8 -->
                                </div>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <h6>Company province</h6>
                                    </div>
                                    <div class="col-sm-6">
                                        {{$item->companyprovince}}
                                    </div>
                                    <!-- end of col-sm-8 -->
                                </div>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <h6>Company Country</h6>
                                    </div>
                                    <div class="col-sm-6">
                                        {{$item->companycountry}}
                                    </div>
                                    <!-- end of col-sm-8 -->
                                </div>
                                <!-- end of row -->
                            </div>
                            <div class="card-block">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <h6>Name</h6>
                                    </div>
                                    <div class="col-sm-6">
                                        {{$item->firstname . " " . $item->lastname}}
                                    </div>
                                    <!-- end of col-sm-8 -->
                                </div>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <h6>Gender</h6>
                                    </div>
                                    <div class="col-sm-6">
                                        {{$item->sex}}
                                    </div>
                                    <!-- end of col-sm-8 -->
                                </div>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <h6>Phone</h6>
                                    </div>
                                    <div class="col-sm-6">
                                        {{$item->phone1}}
                                    </div>
                                    <!-- end of col-sm-8 -->
                                </div>
                                <div class="row">
                                    <div class="col-sm-6">
                                         <h6>Address</h6>
                                    </div>
                                    <div class="col-sm-6" style="overflow-wrap: break-word;">
                                        {{$item->address1 . "," . $item->city . "," .$item->province. "," . $item->country}}
                                    </div>
                                    <!-- end of col-sm-8 -->
                                </div>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <h6>Prefer Language</h6>
                                    </div>
                                    <div class="col-sm-6">
                                        {{$item->lang1}}
                                    </div>
                                    <!-- end of col-sm-8 -->
                                </div>

                                <!-- end of row -->
                            </div>
                            <!-- end of card-block -->

                            <div class="card-footer">

                                <div class="task-board">
                                    <form action="{{route("applicationlist")}}"  style="float:left;">
                                        <input type="hidden" name="approve" value="yes">
                                        <input type="hidden" name="email" value="{{$item->email}}">
                                        <button class="btn btn-primary btn-mini  waves-effect waves-light" type="button"  onclick="dealapplication(this,'approve');" >Approve</button>
                                    </form>
                                    <form action="{{route("applicationlist")}}" style="float:left;margin-left:10px;">
                                        <input type="hidden" name="deny" value="yes">
                                        <input type="hidden" name="email" value="{{$item->email}}">
                                        <button class="btn btn-warning btn-mini  waves-effect waves-light" type="button"  onclick="dealapplication(this,'deny');"  >Deny</button>
                                    </form>

                                    <!-- end of seconadary -->
                                </div>
                                <!-- end of pull-right class -->
                            </div>
                            <!-- end of card-footer -->
                        </div>
                        <!-- end of card -->
                    </div>
                @endforeach
            </div>
            <!-- Main content ends -->


            <!-- Container-fluid ends -->
        </div>
    @endsection



    @section('extrascript')

        <!-- Date picker.js -->
            <script src="/assets/plugins/datepicker/js/moment-with-locales.min.js"></script>
            <script src="/bower_components/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js"></script>

            <script src="/bower_components/jquery-confirm2/js/jquery-confirm.js"></script>

            <!-- Bootstrap Datepicker js -->
            <script type="text/javascript"
                    src="/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
            <script src="/assets/plugins/bootstrap-datepicker/js/bootstrap-datetimepicker.min.js"></script>

            <!-- bootstrap range picker -->
            <script type="text/javascript"
                    src="/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
            <script>
                function dealapplication(ref,actionname){

                    $.confirm({
                        title: 'Confirm',
                        alignMiddle:true,
                        content: 'Are your sure to '+actionname+' this user?',
                        buttons: {
                            confirm: function () {
                                $(ref).parent('form').submit();
                            },
                            cancel: function () {

                            }
                        }
                    });
                }

            </script>





@endsection
