@extends('layouts.mainframe')

@section('extracss')
    <!-- Date Picker css -->
    <link rel="stylesheet"
          href="/bower_components/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css"/>

    <!-- Bootstrap Date-Picker css -->
    <link rel="stylesheet" href="/assets/plugins/bootstrap-datepicker/css/bootstrap-datetimepicker.css"/>
    <link rel="stylesheet" type="text/css" href="/bower_components/bootstrap-daterangepicker/daterangepicker.css"/>

@endsection

@section('content')

    <div class="content-wrapper">
        <!-- Container-fluid starts -->
        <!-- Main content starts -->
        <div class="container-fluid">
            <div class="row" style="margin-bottom:20px;">
                <div class="main-header">
                    <ol class="breadcrumb breadcrumb-title breadcrumb-arrow">
                        <li class="breadcrumb-item"><a href="/home"><i class="icofont icofont-home"></i></a>
                        </li>
                        <li class="breadcrumb-item"><a href="#!">Agent Menu</a>
                        </li>

                        <li class="breadcrumb-item"><a href="#!">Calculate Commission of GDS's PNR</a>
                        </li>

                    </ol>
                </div>
            </div>




            <div class="row">

                    <div id="inputpnr" class="col-xl-4">
                        <div class="card">
                            <div class="card-header" style="padding:10px;padding-bottom:0px;background:#5cb85c">
                                <div class="row">
                                    <div class="col-lg-12 col-xs-12">
                                        <h3 class="card-header-text"  style="color:#FFF;font-size: 24px;font-weight: 600;">STEP 1: Enter a PNR</h3>
                                    </div>
                                </div>
                            </div>

                            <form id="searchorder" class="md-float-material" method="GET" action="{{ route('calculatecommission') }}">
                                {{ csrf_field() }}
                                <div class="cd-pricing-body">
                                    <ul class="basic-list" style="padding-left:10px;padding-right: 10px;">
                                        <li>
                                            <div class="row">
                                                <div class="col-lg-12 col-xs-12">
                                                    <div class="form-radio">
                                                        <div class="radio radio-inline">
                                                            <label>
                                                                <input type="radio" name="provider" value="sabre"
                                                                    @if ("sabre"==old('provider'))
                                                                        checked
                                                                    @endif
                                                                >
                                                                <i class="helper"></i>SABRE
                                                            </label>
                                                        </div>

                                                        <div class="radio radio-inline">
                                                            <label>
                                                                <input type="radio" name="provider" value="amadeus"
                                                                    @if ("amadeus"==old('provider'))
                                                                        checked
                                                                    @endif
                                                                ><i class="helper">
                                                                </i>AMADEUS
                                                            </label>
                                                        </div>

                                                        <div class="radio radio-inline">
                                                            <label>
                                                                <input type="radio" name="provider" value="travelport"
                                                                        @if ("travelport"==old('provider'))
                                                                            checked
                                                                        @endif
                                                                >
                                                                <i class="helper"></i>Travel Port
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-lg-12 col-xs-12">
                                                    <div class="md-input-wrapper">
                                                        <input type="text" name="pnrcode" class="md-form-control
                                                            @if (null!=old('pnrcode'))
                                                                md-valid
                                                            @endif" value="{{old("pnrcode")}}"/>
                                                        <label>PNR code</label>
                                                    </div>
                                                </div>
                                                <div class="col-lg-12 col-xs-12" align="right">
                                                    <div class="md-input-wrapper">
                                                        <button type="submit"  class="save_btn btn btn-primary" ><span class="m-r-10">Calculate Commission</span><i class="icofont icofont-bubble-right"></i></button>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="row">
                                                <div class="col-lg-12 col-xs-12">
                                                    <p style="color:red">Please make sure you have send the pnr to our queue.</p>
                                                    <p style="color:red">If not, please execute below commands on your GDS terminal.</p>
                                                    <h6 style="margin-top:20px">for amadeus</h6>
                                                    <p>
                                                        SQ PNR CODE
                                                    </p>
                                                    <h6 style="margin-top:20px">for sabre</h6>
                                                    <p>
                                                        SQ PNR CODE
                                                    </p>
                                                    <h6 style="margin-top:20px">for traveport</h6>
                                                    <p>
                                                        SQ PNR CODE
                                                    </p>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </form>

                        </div>
                    </div>



            </div>
            <!-- Main content ends -->


            <!-- Container-fluid ends -->
        </div>
    @endsection



    @section('extrascript')

        <!-- Date picker.js -->
            <script src="/assets/plugins/datepicker/js/moment-with-locales.min.js"></script>
            <script src="/bower_components/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js"></script>

            <!-- Bootstrap Datepicker js -->
            <script type="text/javascript"
                    src="/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
            <script src="/assets/plugins/bootstrap-datepicker/js/bootstrap-datetimepicker.min.js"></script>

            <!-- bootstrap range picker -->
            <script type="text/javascript"
                    src="/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>

            <script>
                @if (null!=old("pnrcode") && null!=old("provider"))
                    $(document).ready(function () {
                        $.ajaxSetup(
                            {
                                headers:
                                    {
                                        'X-CSRF-Token': $('input[name="_token"]').val()
                                    }
                            });
                        $.post(
                            '{{route("gdscalculatecommission")}}',
                            {
                                "pnr": '{{old("pnrcode")}}',
                                "provider": '{{old("provider")}}',
                            },
                            function( data ) {
                                var htmlarray=JSON.parse(data);
                                $('#inputpnr').after(htmlarray["commission"]);
                            },
                            'html'
                        );
                    });
                @endif


            </script>


@endsection
