<!DOCTYPE html>
<html lang="en">
<head>
    <title>Active sinorama accout</title>
    <!-- HTML5 Shim and Respond.js IE9 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!-- Meta -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="description" content="Phoenixcoded">
    <meta name="keywords" content=", Responsive, Landing, Bootstrap, App, Template, Mobile, iOS, Android, apple, creative app">
    <meta name="author" content="Phoenixcoded">

    <!-- Favicon icon -->
    <link rel="shortcut icon" href="/assets/images/favicon.png" type="image/x-icon">
    <link rel="icon" href="/assets/images/favicon.ico" type="image/x-icon">

    <!-- Google font-->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">

    <!--ico Fonts-->
    <link rel="stylesheet" type="text/css" href="/assets/icon/icofont/css/icofont.css">

    <!-- Required Fremwork -->
    <link rel="stylesheet" type="text/css" href="/bower_components/bootstrap/dist/css/bootstrap.min.css">

    <!-- waves css -->
    <link rel="stylesheet" type="text/css" href="/bower_components/Waves/dist/waves.min.css">

    <!-- Style.css -->
    <link rel="stylesheet" type="text/css" href="/assets/css/main.css">

    <!-- Responsive.css-->
    <link rel="stylesheet" type="text/css" href="/assets/css/responsive.css">

    <!--color css-->
    <link rel="stylesheet" type="text/css" href="/assets/css/color/color-1.min.css" id="color"/>

</head>
<body>

<div class="error-403">
    <!-- Container-fluid start -->
    <div class="container-fluid">
        <!-- Row start -->
        <div class="row">
            <div class="text-uppercase col-xs-12">

                <h5>You have signed up successfully!!!</h5>
                <p>Please check your email box to receive the email from 'no-replay@sinoramagroup.com'</p>
                <p>Click the link in the email to active your Sinorama account</p>
                <a href="/logout" class="btn btn-error btn-lg waves-effect" style="margin-right:20px;">back to home page</a><a href="#!" onclick="return resend();" class="btn btn-success btn-lg waves-effect">Resend the active email</a>
            </div>
        </div>
        <!-- Row end -->
    </div>
    <!-- Container-fluid end -->
</div>

<!-- Warning Section Starts -->
<!-- Older IE warning message -->
<!--[if lt IE 9]>
<div class="ie-warning">
    <h1>Warning!!</h1>
    <p>You are using an outdated version of Internet Explorer, please upgrade <br/>to any of the following web browsers to access this website.</p>
    <div class="iew-container">
        <ul class="iew-download">
            <li>
                <a href="http://www.google.com/chrome/">
                    <img src="assets/images/browser/chrome.png" alt="Chrome">
                    <div>Chrome</div>
                </a>
            </li>
            <li>
                <a href="https://www.mozilla.org/en-US/firefox/new/">
                    <img src="assets/images/browser/firefox.png" alt="Firefox">
                    <div>Firefox</div>
                </a>
            </li>
            <li>
                <a href="http://www.opera.com">
                    <img src="assets/images/browser/opera.png" alt="Opera">
                    <div>Opera</div>
                </a>
            </li>
            <li>
                <a href="https://www.apple.com/safari/">
                    <img src="assets/images/browser/safari.png" alt="Safari">
                    <div>Safari</div>
                </a>
            </li>
            <li>
                <a href="http://windows.microsoft.com/en-us/internet-explorer/download-ie">
                    <img src="assets/images/browser/ie.png" alt="">
                    <div>IE (9 & above)</div>
                </a>
            </li>
        </ul>
    </div>
    <p>Sorry for the inconvenience!</p>
</div>
<![endif]-->
<!-- Warning Section Ends -->

<!-- Required Jqurey -->
<script src="/bower_components/jquery/dist/jquery.min.js"></script>
<script src="/bower_components/jquery-ui/jquery-ui.min.js"></script>
<script src="/bower_components/tether/dist/js/tether.min.js"></script>


<!-- Required Fremwork -->
<script src="/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

<!-- waves effects.js -->
<script src="/bower_components/Waves/dist/waves.min.js"></script>

<!-- Scrollbar JS-->
<script src="/bower_components/jquery-slimscroll/jquery.slimscroll.js"></script>
<script src="/bower_components/jquery.nicescroll/dist/jquery.nicescroll.min.js"></script>

<!--classic JS-->
<script src="/bower_components/classie/classie.js"></script>

<!-- notification -->
<script src="/assets/plugins/notification/js/bootstrap-growl.min.js"></script>



<!-- custom js -->
<script type="text/javascript" src="/assets/js/main.min.js"></script>
<script type="text/javascript" src="/assets/pages/elements.js"></script>
<script src="/assets/js/menu-horizontal.min.js"></script>


<script>

    function resend(){
        jQuery( document ).ready( function( $ ) {
        $.ajaxSetup(
        {
            headers:
            {
            'X-CSRF-Token': $('input[name="_token"]').val()
            }
        });
        $.post(
            '{{route("email-verification.resend")}}',
            {
                "_token":'{{ csrf_token() }}'
            },
            function( data ) {
                var htmlarray=JSON.parse(data);
                if(htmlarray[0]==="success"){
                    notify('The email have been sent successfully!!!','top','center','fa fa-comments','success','','');
                }else{
                    notify('System error','top','center','fa fa-comments','danger','','');
                }
            },
            'html'
        );


        //prevent the form from actually submitting in browser
        return false;
        } );

    }



</script>
</body>
</html>
