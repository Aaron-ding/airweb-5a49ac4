<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('firstname');
            $table->string('lastname');
            $table->string('email')->unique();
            $table->string('password');
            $table->string('sex')->nullable();
            $table->string('address1')->nullable();
            $table->string('address2')->nullable();
            $table->string('city')->nullable();
            $table->string('province')->nullable();
            $table->string('ountry')->nullable();
            $table->string('postcode')->nullable();
            $table->string('phone1')->nullable();
            $table->string('phone2')->nullable();
            $table->string('remain1')->nullable();
            $table->string('remain2')->nullable();
            $table->smallInteger('remain3')->nullable();
            $table->smallInteger('remain4')->nullable();
            $table->string('memo')->nullable();
            $table->dateTime('createtime')->nullable();
            $table->dateTime('modifytime')->nullable();
            $table->string('ip')->nullable();
            $table->string('ipcountry')->nullable();
            $table->string('ipcity')->nullable();
            $table->string('lang1')->nullable();
            $table->string('lang2')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
