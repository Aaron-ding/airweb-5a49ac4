<?php

// Call by The Laraval Auto Loader

// check both the server host and the env file to get the env setting

// 检查是否定义，避免冲突
if( ! function_exists('getSafeEnv') ){
    function getSafeEnv(){
        if (stripos($_SERVER["HTTP_HOST"],'localhost')===FALSE
            && strpos($_SERVER["HTTP_HOST"],'192.168.')===FALSE
            && strpos($_SERVER["HTTP_HOST"],'127.0.0.1')===FALSE){
            $env = 'master';
        } else {
            $env = 'dev';
        }
        $d=substr(trim(config('app.env')),0,6);
        if($d!='master'){
            $env = 'dev';
        }
        return $env;
    }
}

if( ! function_exists('logConsole') ) {
    function logConsole($log = '')
    {
        if( !array_key_exists('savconsolelog', $GLOBALS))
            $GLOBALS['savconsolelog'] = '';
        switch (empty($log)) {
            case False:
                $out = json_encode($log);
                $GLOBALS['savconsolelog'] .= 'console.log(' . $out . ');';
                break;

            default:
                echo '<script type="text/javascript">' . $GLOBALS['savconsolelog'] . '</script>';
        }
    }
}
?>