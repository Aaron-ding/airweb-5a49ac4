<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();


Route::group(['domain' => 'mo.air.sinorama.ca'], function (){
    Route::get('', 'Mobile\HomeController@index');
    Route::get('/paypal', 'Mobile\HomeController@paypal');
    Route::get('/ft-atos', 'Mobile\HomeController@ftatos');
    Route::post('/atos', 'Mobile\HomeController@atos')->name("atos");
    Route::get('/searchlist', 'Mobile\HomeController@searchlist')->name("searchlist");
    Route::get('/review', 'Mobile\HomeController@review')->name("review");
    Route::get('/clean', 'Mobile\HomeController@clean')->name("clean");
    Route::get('/pay', 'Mobile\HomeController@pay')->name("pay");
    Route::get('/search_iframe', 'Mobile\HomeController@search_iframe');
    Route::get('/receipt', 'Mobile\HomeController@receipt');
    Route::get('/paypal_success', 'Mobile\HomeController@paypalsuccess');
    Route::get('/paypal_error', 'Mobile\HomeController@paypalerror');
    Route::post('/language-chooser', 'LanguageController@changeLanguage');

    Route::post('/language',array(
        'before'=>'csrf',
        'as'=>'language-chooser',
        'uses'=>'LanguageController@changeLanguage',
    )
    );
    Route::post('/changecurrency', 'Mobile\HomeController@changecurrency');
    Route::post('/order','OrderidController@loadID');
    Route::get('/order', 'OrderidController@loadcoupon');
    Route::get('/orders', 'Mobile\OrderController@orders')->middleware('auth.custom');
    Route::get('/contactinfo', 'Mobile\HomeController@contactinfo')->middleware('auth.custom');
    Route::post('/contactinfo', 'Mobile\HomeController@contactinfo')->middleware('auth.custom');
    Route::get('/passengerinfo', 'Mobile\HomeController@passengerinfo')->middleware('auth.custom');
    Route::post('/passengerinfo', 'Mobile\HomeController@passengerinfo')->middleware('auth.custom');
    Route::get('/mypoints', 'Mobile\HomeController@mypoints')->middleware('auth.custom');
    Route::post('/login', 'Auth\LoginController@login');
    Route::get('/logout', 'Auth\LoginController@logout');
    Route::get('/resetpwd', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
    Route::post('/resetpwd', 'Auth\ResetPasswordController@reset');
    Route::get('/orders/psginfo/{ID}', 'Mobile\OrderController@orderspsginfo')->middleware('auth.custom');
    Route::get('/policy', 'Mobile\HomeController@policy');
});


$appDevRoutes = function() {
    Route::get('', 'Mobile\HomeController@index');
    Route::get('/paypal', 'Mobile\HomeController@paypal');
    Route::get('/ft-atos', 'Mobile\HomeController@ftatos');
    Route::post('/atos', 'Mobile\HomeController@atos')->name("dev_atos");
    Route::get('/searchlist', 'Mobile\HomeController@searchlist')->name("dev_searchlist");
    Route::get('/review', 'Mobile\HomeController@review')->name("dev_review");
    Route::get('/clean', 'Mobile\HomeController@clean')->name("dev_clean");
    Route::get('/pay', 'Mobile\HomeController@pay')->name("dev_pay");
    Route::get('/search_iframe', 'Mobile\HomeController@search_iframe');
    Route::get('/receipt', 'Mobile\HomeController@receipt');
    Route::get('/paypal_success', 'Mobile\HomeController@paypalsuccess');
    Route::get('/paypal_error', 'Mobile\HomeController@paypalerror');
    Route::post('/language-chooser', 'LanguageController@changeLanguage');
    Route::post('/language',array(
            'before'=>'csrf',
            'as'=>'language-chooser',
            'uses'=>'LanguageController@changeLanguage',
        )
    );
    Route::post('/changecurrency', 'Mobile\HomeController@changecurrency');
    Route::post('/order','OrderidController@loadID');
    Route::get('/order', 'OrderidController@loadcoupon');
    Route::get('/orders', 'Mobile\OrderController@orders')->middleware('auth.custom');
    Route::get('/contactinfo', 'Mobile\HomeController@contactinfo')->middleware('auth.custom');
    Route::post('/contactinfo', 'Mobile\HomeController@contactinfo')->middleware('auth.custom');
    Route::get('/passengerinfo', 'Mobile\HomeController@passengerinfo')->middleware('auth.custom');
    Route::post('/passengerinfo', 'Mobile\HomeController@passengerinfo')->middleware('auth.custom');
    Route::get('/mypoints', 'Mobile\HomeController@mypoints')->middleware('auth.custom');
    Route::post('/login', 'Auth\LoginController@login');
    Route::get('/logout', 'Auth\LoginController@logout');
    Route::get('/resetpwd', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
    Route::post('/resetpwd', 'Auth\ResetPasswordController@reset');
    Route::get('/orders/psginfo/{ID}', 'Mobile\OrderController@orderspsginfo')->middleware('auth.custom');
    Route::get('/policy', 'Mobile\HomeController@policy');
};

if($_SERVER["HTTP_HOST"] == 'dev.mo.air.sinorama.ca') {
    Route::group(['domain' => 'dev.mo.air.sinorama.ca'], $appDevRoutes);
}

if($_SERVER["HTTP_HOST"] == 'localhost') {
    Route::group(['domain' => 'localhost'], $appDevRoutes);
}

if($_SERVER["HTTP_HOST"] == '192.168.0.149') {
    Route::group(['domain' => '192.168.0.149'], $appDevRoutes);
}

Route::group(['domain' => 'de.air.sinorama.ca'], function (){
    Route::get('', 'Desktop\HomeController@index');

    Route::match(['get', 'post'],'/booking', 'Desktop\HomeController@booking')->name("booking");
});

Route::group(['middleware'=>'csrf'],function() {
    Route::post('/atos_cancel', 'Mobile\HomeController@atoscancel');
    Route::post('/atos_success','Mobile\HomeController@atossuccess');
    Route::post('/atos_return', 'Mobile\HomeController@atosreturn');

});

/*Route::group(['domain' => 'mo.air.sinorama.test'], function (){
    Route::get('', 'Mobile\HomeController@index');
    Route::get('/paypal', 'Mobile\HomeController@paypal');
    Route::get('/ft-atos', 'Mobile\HomeController@ftatos');
    Route::post('/atos', 'Mobile\HomeController@atos')->name("dev_atos");
    Route::get('/searchlist', 'Mobile\HomeController@searchlist')->name("dev_searchlist");
    Route::get('/review', 'Mobile\HomeController@review')->name("dev_review");
    Route::get('/clean', 'Mobile\HomeController@clean')->name("dev_clean");
    Route::get('/pay', 'Mobile\HomeController@pay')->name("dev_pay");
    Route::get('/search_iframe', 'Mobile\HomeController@search_iframe');
    Route::get('/receipt', 'Mobile\HomeController@receipt');
    Route::get('/paypal_success', 'Mobile\HomeController@paypalsuccess');
    Route::get('/paypal_error', 'Mobile\HomeController@paypalerror');
    Route::post('/language-chooser', 'LanguageController@changeLanguage');
    Route::post('/language',array(
            'before'=>'csrf',
            'as'=>'language-chooser',
            'uses'=>'LanguageController@changeLanguage',
        )
    );
    Route::post('/order','OrderidController@loadID');
    Route::get('/order', 'OrderidController@loadcoupon');
    Route::get('/orders', 'Mobile\OrderController@orders');
    Route::get('/contactinfo', 'Mobile\HomeController@contactinfo');
    Route::post('/contactinfo', 'Mobile\HomeController@contactinfo');
    Route::get('/passengerinfo', 'Mobile\HomeController@passengerinfo');
    Route::post('/passengerinfo', 'Mobile\HomeController@passengerinfo');
    Route::get('/mypoints', 'Mobile\HomeController@mypoints');
    Route::post('/login', 'Auth\LoginController@login');
    Route::get('/logout', 'Auth\LoginController@logout');
});*/

/*
Route::group(['middleware' => ['isVerified']], function () {
    Route::get('/', 'HomeController@index')->name('index');
    Route::get('/home', 'HomeController@index')->name('home');
    //accounts

    Route::get('/account/cashpage', 'AccountController@cashpage')->name('cashpage');
    Route::get('/account/pointpage', 'AccountController@pointpage')->name('pointpage');
    Route::get('/account/profile', 'AccountController@profile')->name('profile');
    Route::get('/account/becomeingagent', 'AccountController@profile')->name('becomeingagent');
    //orders
    Route::get('/order/flightorder', 'OrderController@recentorders')->name('flightrecentorder');
    Route::get('/order/flighthistoryorder', 'OrderController@historyorders')->name('flighthistoryorder');
    Route::get('/order/orderdetail', 'OrderController@orderdetail')->name('orderdetail');
    Route::get('/order/payit', 'OrderController@orderdetail')->name('payit');
    Route::post('/order/chargepointsaccount', 'OrderController@chargeAccountPoints')->name('changeaccountpoints');

    Route::post('/order/paywithpoints', 'OrderController@querypaypointsinfo')->name('querypaypointsinfo');
    Route::post('/order/paywithaccount', 'OrderController@querypayaccountinfo')->name('querypayaccountinfo');

    Route::get('/order/invoice', 'OrderController@orderdetail')->name('invoice');

    Route::get('/contract/search', 'ContractController@searchContract')->name('searchcontract');
    Route::get('/contract/commission', 'ContractController@getCommissionOfContract')->name('commission');

    //GDS

    Route::post('/gds/pnrpassengerCard', 'GdsController@pnrPassngerCard')->name('passengercard');
    Route::post('/gds/calculatecommission', 'GdsController@calculatePNRCommission')->name('gdscalculatecommission');

    //AGENT

    Route::get('/agent/calculatecommission', 'AgentController@calculateCommission')->name('calculatecommission');
    Route::get('/agent/issueairticekt', 'AgentController@issueAirTicekt')->name('issueairticekt');

    Route::get('/agent/applicationlist','AgentController@application')->name('applicationlist');

});


Route::get('/logout', 'HomeController@logout')->name('logout');

Route::group( ['middleware' => ['auth']], function() {
    Route::resource('users', 'UserController');
    Route::resource('roles', 'RoleController');
    Route::resource('agents', 'AgentController');
});

Route::group([
    'middleware' => 'web'
], function () {
    Route::get('email-verification/error', 'Auth\RegisterController@getVerificationError')
        ->name('email-verification.error');
    Route::get('email-verification/check/{token}', 'Auth\RegisterController@getVerification')
        ->name('email-verification.check');
    Route::post('email-verification/resend', 'Auth\RegisterController@resendEmail')
        ->name('email-verification.resend');

});
*/
/*
public function auth()
{
    // Authentication Routes...
    $this->get('login', 'Auth\LoginController@showLoginForm')->name('login');
    $this->post('login', 'Auth\LoginController@login');
    $this->post('logout', 'Auth\LoginController@logout')->name('logout');

    // Registration Routes...
    $this->get('register', 'Auth\RegisterController@showRegistrationForm')->name('register');
    $this->post('register', 'Auth\RegisterController@register');

    // Password Reset Routes...
    $this->get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
    $this->post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
    $this->get('/resetpwd', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
    $this->post('/resetpwd', 'Auth\ResetPasswordController@reset');
}
*/



